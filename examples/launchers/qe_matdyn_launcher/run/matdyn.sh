#!/bin/bash

MPIRUN="mpirun -np 4"
EXECUTABLE="/home/fgoudreault/Workspace/q-e/bin/matdyn.x -npool 4"
INPUT=/home/fgoudreault/Workspace/abilaunch/examples/launchers/qe_matdyn_launcher/matdyn.in
LOG=/home/fgoudreault/Workspace/abilaunch/examples/launchers/qe_matdyn_launcher/matdyn.log
STDERR=/home/fgoudreault/Workspace/abilaunch/examples/launchers/qe_matdyn_launcher/matdyn.stderr


$MPIRUN $EXECUTABLE < $INPUT > $LOG 2> $STDERR
