from abilaunch import QEMatdynLauncher as Launcher


variables = {
        "asr": "simple",
        "q_in_band_form": True,
        "q_points": {"qpts" : [[0.5, 0.5, 0.5],
                               [0.0, 0.0, 0.0],
                               [1.0, 0.0, 0.0]],
                     "nqpts": [20, 20, 20]},
        }

launcher = Launcher("matdyn")
launcher.workdir = "."
launcher.input_variables = variables
launcher.link_calculation("../qe_q2rx_launcher")
launcher.command_arguments = "-npool 4"
launcher.mpi_command = "mpirun -np 4"
launcher.write()
launcher.run()
