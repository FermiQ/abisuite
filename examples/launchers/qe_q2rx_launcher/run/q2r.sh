#!/bin/bash

MPIRUN="mpirun -np 4"
EXECUTABLE="/home/fgoudreault/Workspace/q-e/bin/q2r.x -npool 4"
INPUT=/home/fgoudreault/Workspace/abilaunch/examples/launchers/qe_q2rx_launcher/q2r.in
LOG=/home/fgoudreault/Workspace/abilaunch/examples/launchers/qe_q2rx_launcher/q2r.log
STDERR=/home/fgoudreault/Workspace/abilaunch/examples/launchers/qe_q2rx_launcher/q2r.stderr


$MPIRUN $EXECUTABLE < $INPUT > $LOG 2> $STDERR
