#!/bin/bash

MPIRUN=""
EXECUTABLE="/home/fgoudreault/Workspace/q-e/bin/pw.x"
INPUT=/home/fgoudreault/Workspace/abilaunch/examples/launchers/qe_pwx_launcher/silicon.in
LOG=/home/fgoudreault/Workspace/abilaunch/examples/launchers/qe_pwx_launcher/silicon.log
STDERR=/home/fgoudreault/Workspace/abilaunch/examples/launchers/qe_pwx_launcher/silicon.stderr


$MPIRUN $EXECUTABLE < $INPUT > $LOG 2> $STDERR
