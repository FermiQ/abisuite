from abilaunch import AbinitLauncher as Launcher


# tbase1_1 variables
variables = {"acell": (10, 10, 10),
             "ntypat": 1,
             "znucl": 1,
             "natom": 2,
             "typat": (1, 1),
             "xcart": ((-0.7, 0.0, 0.0), (0.7, 0.0, 0.0)),
             "ecut": 10.0,
             "kptopt": 0,
             "nkpt": 1,
             "nstep": 10,
             "toldfe": 1.0e-6,
             "diemac": 2.0,
             "optforces": 1}

l = Launcher("example")
l.workdir = "."
l.pseudos = ["../../../pseudos/01h.pspgth"]
l.input_variables = variables
l.command = "/home/fgoudreault/Workspace/abinit/build/master/vanilla/src/98_main/abinit"
l.write(overwrite=True)
l.run()
