from abilaunch import QEMassLauncher


# very simple example
qe_input_vars = {"calculation": "scf",
                 "ibrav": 2,
                 "pseudo_dir": "../../pseudos",
                 "celldm(1)": 10.28,
                 "nat": 2,
                 "ntyp": 1,
                 "atomic_species": [{"atom": "Si", "atomic_mass": 28.086,
                                     "pseudo": "Si.pz-vbc.UPF"}],
                 "atomic_positions": {"parameter": "alat",
                                      "positions": {"Si": [[0.00, 0.00, 0.00],
                                                           [0.25, 0.25, 0.25]],
                                                    }},
                 "k_points": {"automatic": [4, 4, 4, 1, 1, 1]}}


launcher = QEMassLauncher(["ecutwfc5", "ecutwfc10"])
launcher.workdir = "."
launcher.command = "/home/fgoudreault/Workspace/q-e/bin/pw.x"
launcher.common_input_variables = qe_input_vars
launcher.specific_input_variables = [{"ecutwfc": 5.0}, {"ecutwfc": 10.0}]
launcher.write(overwrite=True)
launcher.run()
