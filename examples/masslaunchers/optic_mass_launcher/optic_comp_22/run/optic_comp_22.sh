#!/bin/bash


MPIRUN=""
EXECUTABLE=/home/fgoudreault/Workspace/abinit/build/master/vanilla/src/98_main/optic
INPUT=/home/fgoudreault/Workspace/abilaunch/examples/optic_mass_launcher/optic_comp_22/run/optic_comp_22.files
LOG=/home/fgoudreault/Workspace/abilaunch/examples/optic_mass_launcher/optic_comp_22/optic_comp_22.log
STDERR=/home/fgoudreault/Workspace/abilaunch/examples/optic_mass_launcher/optic_comp_22/optic_comp_22.stderr

$MPIRUN $EXECUTABLE < $INPUT > $LOG 2> $STDERR
