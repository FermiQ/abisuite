#!/bin/bash


MPIRUN=""
EXECUTABLE=/home/fgoudreault/Workspace/abinit/build/master/vanilla/src/98_main/abinit
INPUT=/home/fgoudreault/Workspace/abilaunch/examples/masslauncher/ecut5/run/ecut5.files
LOG=/home/fgoudreault/Workspace/abilaunch/examples/masslauncher/ecut5/ecut5.log
STDERR=/home/fgoudreault/Workspace/abilaunch/examples/masslauncher/ecut5/ecut5.stderr

$MPIRUN $EXECUTABLE < $INPUT > $LOG 2> $STDERR
