from abilaunch import AbinitMassLauncher as MassLauncher


tbase1_1_vars = {"acell": (10, 10, 10),
                 "ntypat": 1,
                 "znucl": 1,
                 "natom": 2,
                 "typat": (1, 1),
                 "xcart": ((-0.7, 0.0, 0.0), (0.7, 0.0, 0.0)),
                 # "ecut": 10.0,
                 "kptopt": 0,
                 "nkpt": 1,
                 "nstep": 10,
                 "toldfe": 1.0e-6,
                 "diemac": 2.0,
                 "optforces": 1}

launcher = MassLauncher(["ecut5", "ecut10"])  # job name for each job
launcher.workdir = "."
launcher.common_input_variables = tbase1_1_vars
launcher.common_pseudos = ["../../../pseudos/01h.pspgth"]
launcher.specific_input_variables = [{"ecut": 5}, {"ecut": 10}]
launcher.command = "/home/fgoudreault/Workspace/abinit/build/master/vanilla/src/98_main/abinit"
launcher.write(overwrite=True)
launcher.run()
