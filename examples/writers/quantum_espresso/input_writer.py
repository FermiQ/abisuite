from abilaunch.writers import QEInputFileWriter


path = "example.in"
variables = {"calculation": "scf",
             "prefix": "silicon",
             "pseudo_dir": "./",
             "outdir": "./",
             "ibrav": 2,
             "celldm(1)": 10.28,
             "nat": 2,
             "ntyp": 1,
             "ecutwfc": 18.0,
             "atomic_positions": {"parameter": "alat",
                                  "positions": {"Si": [[0.00, 0.00, 0.00],
                                                       [0.25, 0.25, 0.25]]}},
             "atomic_species": [{"atom": "Si",
                                 "atomic_mass": 28.086,
                                 "pseudo": "Si.pz-vbc.UPF"}],
             "k_points": {"automatic": [4, 4, 4, 1, 1, 1]},
            }
writer = QEInputFileWriter()
writer.path = path
writer.input_variables = variables
writer.write(overwrite=True,  # to overwrite previously existing file
                              # (default is False)
             )
