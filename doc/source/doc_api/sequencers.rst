Sequencers Module
=================

.. contents::
    :local:

This module contains the sequencers objects which are used to easily produce
series of coherent calculations.
