Launchers Module
================

.. contents::
    :local:

This module contains all the necessary Launcher classes to execute ab initio
tasks.

:mod:`~abisuite.launchers.launchers`
------------------------------------

.. automodule:: abisuite.launchers.launchers.bases
    :members:
    :show-inheritance:


:mod:`~abisuite.launchers.launchers.abinit_launchers`
-----------------------------------------------------

.. automodule:: abisuite.launchers.launchers.abinit_launchers.abinit_launcher
    :members:
    :show-inheritance:
