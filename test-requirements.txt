pytest != 4.2.1  # for testing
pytest-cov       # for test code coverage
pytest-xdist     # for parallel test execution
pytest-order     # for test ordering
flake8           # for code quality testing
flake8-bugbear   # for extra code quality testing
