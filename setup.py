from setuptools import setup
import os


def post_install():
    # prepend PATH variable with scripts directory
    here = os.path.dirname(os.path.abspath(__file__))
    scripts = os.path.join(here, "abisuite", "scripts")
    PATH = os.environ["PATH"].split(":")
    if scripts not in PATH:
        # append path to scripts in bashrc
        print(  # noqa: T001
          "The $PATH environment variable has been extended to include"
          " 'abisuite' scripts.")  # noqa: T001
        print("See '~/.bashrc'")  # noqa: T001
        bashrc = os.path.expanduser("~/.bashrc")
        with open(bashrc, "a") as f:
            f.write("\n")
            f.write("# The following line has been added by the 'abisuite'"
                    " setup script.\n")
            f.write(f'export PATH="{scripts}:$PATH"')
            # add autocompletes for directories only for some scripts
            f.write("complete -d abirm")
            f.write("complete -d abistatus")


with open("requirements.txt") as f:
    requirements = f.read().splitlines()


setup(name="abisuite",
      python_requires=">=3.6",
      licence="MIT",
      install_requires=requirements,
      )

post_install()
