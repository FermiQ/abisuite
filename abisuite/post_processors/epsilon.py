from .bases import BasePostProcClass
# from ..parsers import QEEpsilonParser
from ..routines import is_list_like
import numpy as np


# FIXME: probably doesn't work anymore with QE
class Epsilon(BasePostProcClass):
    """Class that represents a dielectric tensor.

    For now it represent only one element of the dielectric tensor.
    """
    _loggername = "Epsilon"

    def __init__(self, **kwargs):
        super().__init__(**kwargs)
        self._energies = None
        self._epsilon_real = None
        self._epsilon_imag = None

    @property
    def energies(self):
        if self._energies is not None:
            return self._energies
        raise ValueError("Set energies before using them (x-axis).")

    @energies.setter
    def energies(self, energies):
        if not is_list_like(energies):
            raise TypeError(f"Energies should be a list but got {energies}.")
        if not isinstance(energies, np.ndarray):
            energies = np.array(energies)
        self._energies = energies

    @property
    def epsilon_real(self):
        if self._epsilon_real is not None:
            return self._epsilon_real
        raise ValueError("Set epsilon_real before using it.")

    @epsilon_real.setter
    def epsilon_real(self, epsilon):
        if not isinstance(epsilon, np.ndarray):
            epsilon = np.array(epsilon)
        self._epsilon_real = epsilon

    @property
    def epsilon_imag(self):
        if self._epsilon_imag is not None:
            return self._epsilon_imag
        raise ValueError("Set epsilon_imag before using it.")

    @epsilon_imag.setter
    def epsilon_imag(self, epsilon):
        if not isinstance(epsilon, np.ndarray):
            epsilon = np.array(epsilon)
        self._epsilon_imag = epsilon

    def get_plot(self, **kwargs):
        plot = super().get_plot(ylabel=r"$\epsilon(\omega)$",  # noqa
                                xlabel=r"$\omega$", **kwargs)
        plot.add_curve(self.energies, self.epsilon_real,
                       label=r"$\Re[\epsilon(\omega)]$",  # noqa
                       color="r")
        plot.add_curve(self.energies, self.epsilon_imag,
                       label=r"$\Im[\epsilon(\omega)]$",  # noqa
                       color="b")
        plot.add_hline(0, linestyle="--", color="k")
        return plot

    # @classmethod
    # def from_qeeps(
    #       cls, path_real, comp_real, path_imag, comp_imag, **kwargs):
    #     """Create the Epsilon class from Quantum Espresso Epsilon files
    #     produced by the 'epsilon.x' script.
    #
    #     Parameters
    #     ----------
    #     path_real : str
    #                 The path to the file containing the real part of epsilon.
    #     path_imag : str
    #                 The path to the file continaing the imag part of epsilon.
    #     comp_real : str
    #                 States the component of the real part of epsilon
    #                 to consider
    #     comp_imag : str
    #                 States the component of the imaginary part of epsilon
    #                 to consider
    #     """
    #     parser_real = QEEpsilonParser(path_real, **kwargs)
    #     parser_imag = QEEpsilonParser(path_imag, **kwargs)
    #
    #     if not all(parser_real["energies"] == parser_imag["energies"]):
    #         raise ValueError(
    #                   "Energie grids are not the same on both imag and"
    #                   " real part of epsilon...")
    #     epsilon = cls(**kwargs)
    #     epsilon.energies = parser_real["energies"]
    #     epsilon.epsilon_real = parser_real[comp_real]
    #     epsilon.epsilon_imag = parser_imag[comp_imag]
    #     return epsilon
