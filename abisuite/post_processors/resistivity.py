import numpy as np

from .bases import BasePostProcClass
from ..handlers import QEEPWResistivityFile
from ..routines import is_list_like


class Resistivity(BasePostProcClass):
    """Process the spectral function alpha^2F(w) in order to compute rho(T).
    """
    _loggername = "Resistivity"

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self._temperatures = None
        self._resistivity = None

    @property
    def resistivity(self):
        if self._resistivity is not None:
            return self._resistivity
        raise ValueError("Need to set 'resistivity'.")

    @resistivity.setter
    def resistivity(self, res):
        self._resistivity = res

    @property
    def temperatures(self):
        if self._temperatures is not None:
            return self._temperatures
        raise ValueError("Need to set temperatures.")

    @temperatures.setter
    def temperatures(self, temperatures):
        if not is_list_like(temperatures):
            raise TypeError("temperatures must be list-like.")
        self._temperatures = np.array(temperatures)

    def get_plot(
            self, xlabel="Temperature", xunits="K", ylabel="Resistivity",
            yunits=r"$\mu\Omega$ cm", temperatures_converter=1,
            resistivity_converter=1, **kwargs):
        title = kwargs.pop("title", "")
        plot = super().get_plot(
                xunits=xunits, yunits=yunits, xlabel=xlabel, ylabel=ylabel,
                title=title)
        plot.add_curve(self.temperatures * temperatures_converter,
                       self.resistivity * resistivity_converter, **kwargs)
        return plot

    @classmethod
    def from_file(cls, path, *args, **kwargs):
        with QEEPWResistivityFile.from_file(
                path) as res:
            return cls.from_handler(res, *args, **kwargs)

    @classmethod
    def from_calculation(cls, path, *args, **kwargs):
        """Create the Resistivity object from a calculation. For now, it only
        supports 'qe_epw' calculation.
        """
        with QEEPWResistivityFile.from_calculation(
                path) as resistivity:
            return cls.from_handler(resistivity, *args, **kwargs)

    @classmethod
    def from_handler(cls, handler, *args, **kwargs):
        """Create a resistivity object from a handler object.
        """
        res = cls(*args, **kwargs)
        with handler:
            res.resistivity = handler.resistivity
            res.temperatures = handler.temperatures
        return res
