import numpy as np

from .band_structure import BandStructure
from ..handlers import (
        AbinitFatbandFile, AbinitProcarFile, CalculationDirectory,
        )
from ..plotters import Plot


class FatBand(BandStructure):
    """Post processing class for a fat band structure.
    """
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        # projection data
        self.m = None
        self.l_number = None
        self.atom_name = None
        self.atom_number = None
        self.orbital = None
        self._characters = None

    def get_plot(self, *args, bands=None, fat_band_color="r",
                 coeff=1.0, **kwargs):
        """Plot a normal band structure and superpose on it the fat bands for
        each wfc.

        Parameters
        ----------
        fat_band_color : str, optional
            The color of the fat band.
        coeff : float, optional
            Multiplies the width of the fat band structure with this number.
            Used to adjust the general width of the fat band structure in case
            it is too big or too small.
        """
        band_structure_plot = super().get_plot(*args, bands=bands, **kwargs)
        fat_band_plot = Plot(loglevel=self._loglevel)
        band_characters = self._get_considered_bands(
                                self.characters, bands)
        bands = self._get_considered_bands(self.bands, bands)
        for band, band_character in zip(bands, band_characters):
            band -= self.fermi_energy
            if len(band_character) != len(band):
                raise ValueError(
                        "Band characters don't match band eigenvalues.")
            chars = band_character * coeff
            xs = band_structure_plot.curves[0].xdata  # should be all the same
            fat_band_plot.add_fill_between(xs, band + chars, band - chars,
                                           color=fat_band_color)
        # set same parameters to make sure we can both plots
        for attr in ("ylabel", "xlabel", ):
            setattr(fat_band_plot, attr, getattr(band_structure_plot, attr))
        # add the 2 plots and return it
        final_plot = fat_band_plot + band_structure_plot
        if self.orbital is None:
            final_plot.title = (
                    "Projection on atom " + self.atom_name +
                    str(self.atom_number) + f" l={self.l_number}, m={self.m}")
        else:
            final_plot.title = (
                    "Projection on atom " + str(self.atom_number) +
                    f" {self.orbital}")
        return final_plot

    @property
    def characters(self):
        if self._characters is not None:
            return self._characters
        raise ValueError("Need to set 'characters'.")

    @characters.setter
    def characters(self, chars):
        # check shape
        chars = np.array(chars)
        nbands = chars.shape[0]
        if nbands != self.nbands:
            raise ValueError("Wrong number of bands.")
        nkpts = chars.shape[1]
        if nkpts != self.nkpts:
            raise ValueError("Wrong number of kpts.")
        self._characters = chars

    @classmethod
    def from_abinit_calculation(cls, calc, *args, **kwargs):
        """Returns an initiated FatBand object from a CalculationDirectory
        instance.
        """
        with calc.input_file as inp:
            # check how fatbands was computed.
            if inp.input_variables.get("pawfatbnd", 0) > 0:
                return cls.from_abinit_paw_calculation(
                        calc, *args, **kwargs)
            if inp.input_variables.get("prtprocar", 0) > 0:
                return cls.from_abinit_procar_calculation(
                        calc, *args, **kwargs)
        raise ValueError(f"Not a Fat band calculation: {calc.path}.")

    @classmethod
    def from_abinit_paw_calculation(
            cls, calc, *args, atom_number=None, l_number=None, m=None,
            **kwargs):
        """Returns an initiated FatBand object from a CalculationDirectory
        instance.

        Parameters
        ----------
        calc: CalculationDirectory instance
            The calc dir object for the given abinit calculation.
        atom_number: int, optional
            The atom number of which we want to plot the fatband.
            If None, all atom_numbers are considered.
        l_number: int, optional
            The l quantum number. If None, all l_numbers are considered.
        m: int, optional
            The m quantum number. If None, all m are considered.
        """
        files = AbinitFatbandFile.from_calculation(calc.path)
        bs = BandStructure.from_abinit_calculation(calc.path)
        instances = []
        for key, handler in files.items():
            # key = at0001_Si_is1_l0_m+0
            # get atom number
            at = int(key.split("at")[1].split("_")[0])
            if atom_number is not None:
                if at != atom_number:
                    continue
            # get l
            _l = int(key.split("_")[3][-1])
            if l_number is not None:
                if _l != l_number:
                    continue
            # get m
            _m = int(key.split("_")[-1][-2:])
            if m is not None:
                if _m != m:
                    continue
            # from here we got our file
            atom_name = key.split("_")[1]
            instance = cls(*args, **kwargs)
            instance.l_number = _l
            instance.m = _m
            instance.atom_number = at
            instance.atom_name = atom_name
            instance.kpts = bs.kpts
            instance.energies = bs.energies
            instance.fermi_energy = bs.fermi_energy
            instance.fermi_band = bs.fermi_band
            instances.append(instance)
            with handler:
                instance.characters = handler.characters
        if not instances:
            raise LookupError(
                    "Could not find fatband file for atom_numer="
                    f"{atom_number}, l_number={l_number}, m={m}")
        return instances

    @classmethod
    def from_abinit_procar_calculation(
            cls, calc, *args, atom_number=None, orbital=None, **kwargs):
        """Instantiate a FatBand object from a procar file.

        Parameters
        ----------
        calc: CalculationDirectory instance
            The calc dir object for the given abinit calculation.
        atom_number: int, optional
            The atom number of which we want to plot the fatband.
            If None, all atom_numbers are considered.
        orbital: str, optional
            The orbital to consider. If None, all orbitals are considered.
        """
        orb_order = ["s", "py", "pz", "px", "dxy", "dyz", "dz2", "dxz",
                     "dx2-y2"]
        instances = []
        bs = BandStructure.from_abinit_calculation(calc.path)
        with AbinitProcarFile.from_calculation(calc.path) as procar:
            characters = procar.characters
        # characters is a nbands x nkpts x natoms x norb array
        for iat, at_chars in enumerate(characters.transpose((2, 3, 0, 1))):
            if atom_number is not None:
                if iat != atom_number:
                    continue
            for orb, orb_chars in zip(orb_order, at_chars):
                if orbital is not None:
                    if orb != orbital:
                        continue
                instance = cls(*args, **kwargs)
                instance.atom_number = iat
                instance.kpts = bs.kpts
                instance.energies = bs.energies
                instance.fermi_energy = bs.fermi_energy
                instance.fermi_band = bs.fermi_band
                instance.orbital = orb
                instance.characters = orb_chars
                instances.append(instance)
        return instances

    @classmethod
    def from_calculation(cls, path, *args, **kwargs):
        """Returns a FatBand object from a calculation directory.
        """
        with CalculationDirectory.from_calculation(path) as calc:
            if calc.calctype == "abinit":
                return cls.from_abinit_calculation(calc, *args, **kwargs)
            else:
                raise NotImplementedError(calc.calctype)
