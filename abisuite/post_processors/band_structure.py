import logging
import os
import traceback as tb

import numpy as np

from .bases import BasePostProcClass_w_Fermi
from ..constants import HARTREE_TO_EV
from ..handlers import (
        AbinitEIGFile, AbinitGSRFile, AbinitLogFile, AbinitOutputFile,
        CalculationDirectory, MetaDataFile, QEPWLogFile, QEEPWBandEigFile,
        Wannier90BandDatFile, Wannier90BandkptFile, Wannier90InputFile,
        )
from ..routines import is_list_like, is_vector, is_2d_arr


HIGH_SYM_PTS = {"cubic": {r"$\Gamma$": (0.0, 0.0, 0.0),
                          "R": (0.5, 0.5, 0.5),
                          "X": (0.5, 0.0, 0.0),
                          "M": (0.5, 0.5, 0.0)},
                "fcc": {"L": (0.5, 0.5, 0.5),
                        r"$\Gamma$": (0.0, 0.0, 0.0),
                        "X": (0.0, 0.5, 0.5),
                        "$X_2$": (1.0, 0.0, 0.0),  # considered as x in QE
                        "W": (0.25, 0.75, 0.5),
                        "U": (0.25, 0.625, 0.625),
                        "K": (0.375, 0.75, 0.375),
                        },
                "none": {},
                "rectangle": {r"$\Gamma$": (0.0, 0.0, 0.0),
                              "X": (0.5, 0.0, 0.0),
                              "Y": (0.0, 0.5, 0.0),
                              "M": (0.5, 0.5, 0.0)},
                "hexagonal": {r"$\Gamma$": (0.0, 0.0, 0.0),
                              "M": (0.5, 0.0, 0.0),
                              "K": (0.6667, 0.3333, 0.0),
                              "A": (0.0, 0.0, 0.5),
                              "L": (0.5, 0.0, 0.5),
                              "H": (0.6667, 0.3333, 0.5),
                              },
                "tetragonal": {r"$\Gamma$": (0.0, 0.0, 0.0),
                               "M": (0.5, 0.5, 0.0),
                               "X": (0.5, 0.0, 0.0),
                               "Z": (0.0, 0.0, 0.5),
                               "R": (0.5, 0.0, 0.5),
                               "A": (0.5, 0.5, 0.5),
                               },
                }


def get_all_k_labels(labels, density, dtype=tuple):
    """Function that returns what the BandStructure.get_plot 'all_k_labels'
    parameter needs to work based on a list of labels and a kpoints path
    density.

    Parameters
    ----------
    labels : list
        The list of kpoint labels.
    density : int
        The kpoint path density.
    dtype : callable
        e.g.: tuple, list, etc.
    """
    if not is_list_like(labels):
        raise TypeError("'labels' need to be a list-like object.")
    return [
        dtype((label, coord))
        for label, coord in zip(
            labels, range(0, len(labels) * (density + 1), density))]


# TODO: should multi class this with a plot class
class BandStructure(BasePostProcClass_w_Fermi):
    """Class that represents a Band Structure.
    """
    _loggername = "BandStructure"

    def __init__(self, **kwargs):
        super().__init__(**kwargs)
        self._kpts = None
        self._nkpts = None
        self._energies = None
        self._fermiband = None
        self._bands = None
        self._nbands = None
        self._software = None
        self._bandgap = None

    @property
    def bandgap(self):
        if self._bandgap is not None:
            return self._bandgap
        raise ValueError("Plot data to compute band gap.")

    @property
    def fermi_band(self):
        if self._fermiband is not None:
            return self._fermiband
        # if fermi energy is set and eigenvalues, we can compute the fermi band
        if self._energies is None or self._fermienergy is None:
            raise ValueError("Set fermi_band before using it.")
        for iband, band in enumerate(self.bands):
            # if fermi energy is within the band, this is it
            if self.fermi_energy <= max(band) and self.fermi_energy >= min(
                    band):
                self._fermiband = iband
                return self.fermi_band
        raise RuntimeError("Could not compute fermi band.")

    @fermi_band.setter
    def fermi_band(self, fermi_band):
        if not isinstance(fermi_band, int):
            raise TypeError(f"Expected an integer but got: {fermi_band}")
        if self._fermienergy is not None:
            self._logger.warning("Resetting Fermi energy. Overwritting "
                                 " existing value using the fermi band index.")
        self._fermiband = fermi_band
        self.fermi_energy = max(self.bands[self.fermi_band])

    @property
    def kpts(self):
        if self._kpts is not None:
            return self._kpts
        raise ValueError("Need to set kpts/qpts before using them.")

    @kpts.setter
    def kpts(self, kpts):
        # kpts must be a Nkpts x 3 array / list
        if is_vector(kpts):
            kpts = [kpts]
        if not is_2d_arr(kpts):
            raise TypeError(f"Expected 2d array but got: {kpts}")
        kpts = np.array(kpts)
        if kpts.shape[-1] != 3:
            raise TypeError(f"Expected Nx3 array but got: {kpts}")
        self._kpts = kpts
        self._nkpts = len(self.kpts)
        # check nkpts vs number of pts for energies
        try:
            # if energies not set it will raise an error
            if self.energies.shape[0] != self.nkpts:
                raise TypeError(f"npts ({self.nkpts}) not commensurable with "
                                f"energies ({self.energies.shape[0]})")
        except ValueError:
            # energies not set, bypass the check
            pass

    @property
    def nkpts(self):
        if self._nkpts is not None:
            return self._nkpts
        raise ValueError("Need to set kpts/qpts before getting nb of pts.")

    @property
    def energies(self):
        if self._energies is not None:
            return self._energies
        raise ValueError("Need to set energies before using them.")

    @energies.setter
    def energies(self, energies):
        if is_vector(energies):
            # only one point
            energies = [energies]
        if not is_2d_arr(energies):
            raise ValueError(f"Expected 2D array but got: {energies}")
        energies = np.array(energies)
        # check size with number of kpts
        try:
            # if kpts aren't set, this will raise an error
            if self.energies.shape[0] != self.nkpts:
                raise TypeError(f"nb of energies ({self.energies.shape[0]}) "
                                f" not commensurable with number of points "
                                f"({self.nkpts}).")
        except ValueError:
            # bypass check
            pass
        self._energies = energies
        # set nkpts
        self._nkpts = energies.shape[0]
        # set nbands
        self._nbands = energies.shape[1]

    @property
    def bands(self):
        return self.energies.T

    @property
    def nbands(self):
        if self._nbands is not None:
            return self._nbands
        raise ValueError("Set energies to get nbands")

    def compute_bandgap(self):
        """Computes the band gap.
        """
        return min(self.bands[self.fermi_band + 1]) - self.fermi_energy

    def _get_sym_pts_labels(self, symmetry, other_k_labels=None):
        labels = []
        labels_loc = []
        if symmetry is None:
            symmetry = "none"
        high_sym_coordinates = HIGH_SYM_PTS[symmetry].copy()
        if other_k_labels is not None:
            high_sym_coordinates.update(other_k_labels)
        for index, kpt in enumerate(self.kpts):
            for label, high_sym_kpt in high_sym_coordinates.items():
                # round the kpt coordinates to 4th decimal
                kpt = [round(x, 4) for x in kpt]
                high_sym_kpt = list(high_sym_kpt)
                if kpt == high_sym_kpt:
                    labels_loc.append(index)
                    if label == "Gamma":
                        labels.append(r"$\Gamma$")
                    else:
                        labels.append(label)
                    break
        return labels, labels_loc

    def _get_considered_bands(self, array, bands):
        """Returns the considered bands of an array if needed.
        """
        considered_bands = array
        if bands is not None:
            considered_bands = array[range(bands[0], bands[1] + 1), :]
        return considered_bands

    def get_plot(self, bands=None, symmetry="none",
                 line_at_zero=True,
                 adjust_axis=True,
                 high_sym_vlines=False,
                 other_k_labels=None,
                 all_k_labels=None,
                 ylabel="Energy",
                 yunits=None,
                 show_bandgap=False,
                 fermi_at_zero=True, color="k",
                 **kwargs):
        """Plot the bandstructure.

        Parameters
        ----------
        symmetry: str, optional, {"none", "rectangle", "FCC"}
                  Gives the crystal symmetry of the structure. This will
                  able the labelling of the high symmetry points in the
                  Brillouin Zone.
        adjust_axis : bool, optional
            If True, a new kpt/qpt axis is computed so that the distance
            between points is proportional to the length of the reciprocal
            vectors.
        other_k_label : dict, optional
                        If not None, it is a dictionary whose keys are
                        k-pts labels and values are the coordinates.
        all_k_labels : list, optional
                       If not None, gives the entirety of the kpt labels
                       and positions in the form of tuples (label, label_loc).
                       Overwrite all labels that are retrieved via other means.
        line_at_zero: bool, optional
                      If True, a line is drawn at 0 energy.
        bands: list, optional
               Selects the range of bands to plot (starting from 0).
               If set to None, all bands are shown.
               (tuple: (min, max))
        high_sym_vlines : bool, optional
                          If True, plain vertical lines will be shown
                          at the high symetry points.
        ylabel : str, optional
                 ylabel for the plot.
        yunits : str, optional
                 If not None, it is the units for the y axis.
        show_bandgap : bool, optional
            If True, the band gap value will be shown in a label and dots
            will appear on the band structure where the min of the conduction
            band is located and where the max of the valence band is.
            Not compatible with 'adjust_axis'.
        fermi_at_zero: bool, optional
            If True (default), the fermi level is placed at the 0 energy.
            Otherwise, a line is just drawn at the fermi level.
        color: str, optional
            Band structure color.
        other kwargs are passed as arguments for the bands (e.g.:
        linewidth, linestyle, etc.) or to create xlabels, xunits title
        """
        if show_bandgap and adjust_axis:
            raise NotImplementedError(
                    "Showing band gap not compatible yet with adjust axis.")
        considered_bands = self._get_considered_bands(self.bands, bands)
        if self._fermienergy is None:
            self._logger.warning("Fermi energy not set, assuming it is 0...")
            self.fermi_energy = 0
        okl = other_k_labels
        labels, labels_loc = self._get_sym_pts_labels(symmetry,
                                                      other_k_labels=okl)
        if all_k_labels is not None and all_k_labels != "none":
            labels = [x[0] for x in all_k_labels]
            labels_loc = [x[1] for x in all_k_labels]
        if all_k_labels == "none" or all_k_labels is None:
            # special value TODO: find a better way to do this
            labels = ["", ""]
            labels_loc = None
        if fermi_at_zero:
            ys = considered_bands - self.fermi_energy
        else:
            ys = considered_bands
        if labels_loc is None:
            labels_loc = (0, self.nkpts - 1)
        if adjust_axis:
            xs, labels_loc = self._get_kpts_axis(labels_loc)
        else:
            # just normalize by nb of points the axis
            xs = [i/self.nkpts for i in range(self.nkpts)]
            labels_loc = [i/self.nkpts for i in labels_loc]
        plot = super().get_plot(ylabel=ylabel, yunits=yunits,
                                xlabel=kwargs.pop("xlabel", ""),
                                xunits=kwargs.pop("xunits", None),
                                title=kwargs.pop("title", ""))
        # compute band gap
        self._bandgap = self.compute_bandgap()
        gap = self._bandgap
        plot.xtick_labels = [(pos, label) for pos, label in
                             zip(labels_loc, labels)]
        if line_at_zero:
            plot.add_hline(0, linestyle="--")
        if not fermi_at_zero and self.fermi_energy != 0.0:
            # draw fermi energy line
            plot.add_hline(self.fermi_energy, linestyle="--")
        if high_sym_vlines:
            for pos in labels_loc:
                plot.add_vline(pos)
        for band in ys:
            plot.add_curve(xs, band, color=color, **kwargs)
        # add gap label if needed
        if show_bandgap:
            if fermi_at_zero:
                gap -= self.fermi_energy
            # add dots where the min of conduction band and max of valence band
            # are located.
            min_ = np.min(self.bands[self.fermi_band + 1])
            where_min_ = np.where(
                    self.bands[self.fermi_band + 1] == min_)[0] / self.nkpts
            mins_ = [min_] * len(where_min_)
            max_ = self.fermi_energy
            where_max_ = np.where(
                    self.bands[self.fermi_band] == max_)[0] / self.nkpts
            maxs_ = [max_] * len(where_max_)
            if fermi_at_zero:
                mins_ = (np.array(mins_) - self.fermi_energy).tolist()
                maxs_ = np.array([0] * len(maxs_)).tolist()
            plot.add_scatter(
                    list(where_min_) + list(where_max_), mins_ + maxs_,
                    color=color, s=50)
            plot.curves[0].label = (
                    r"$E_{gap}=$" + str(round(self._bandgap, 3)) + " eV")
            # plot.add_hline(gap, linestyle="--", color=color)
            # plot.title = f"$E_g={gap:.3f}$"
            # if yunits is not None:
            #     plot.title += f" {yunits}"
        self._plot = plot
        return plot

    @classmethod
    def from_abinit_calculation(cls, path, **kwargs):
        """Classmethod to read kpts and eigenvalues directly from an
        Abinit calculation.

        Sets the kpts and the energies but still need to set fermi_energy
        or fermi_band manually after that.

        Parameters
        ----------
        path : The abinit calculation path.
        """
        bandstructure = cls(**kwargs)
        with AbinitEIGFile.from_calculation(
                path, loglevel=bandstructure._logger.level) as eig:
            bandstructure.kpts = eig.k_points
            bandstructure.energies = np.array(eig.eigenvalues) * HARTREE_TO_EV
        # check the fermi level in the output file
        with AbinitOutputFile.from_calculation(
                path, loglevel=bandstructure._loglevel) as output:
            try:
                fermi_energy = output.dtsets[0]["fermi_energy"] * HARTREE_TO_EV
            except IndexError as e:
                bandstructure._logger.error(
                        f"Couldn't parse any dataset:\n{output}")
                bandstructure._logger.exception(e)
                raise e
            bandstructure._logger.debug(
                "Automatically got fermi energy from parent calculation: "
                f"{fermi_energy} eV")
            bandstructure.fermi_energy = fermi_energy
        # try to set fermi band as well
        with AbinitLogFile.from_calculation(
                path, loglevel=bandstructure._loglevel) as log:
            bandstructure._logger.debug(
                    "Trying to get occupations from log file to set fermi "
                    "band automatically.")
            occupations = log.occ
        # if cannot get occupations from log file, check GSR file
        if occupations is None:
            with AbinitGSRFile.from_calculation(
                    path, loglevel=bandstructure._loglevel) as gsr:
                bandstructure._logger.debug(
                        "Trying to get occupations from GSR file.")
                # 1st 0 is for the first spin (assume same occ for all spins)
                # 2nd 0 is for the kpt index. assume same occ for all kpts
                occupations = gsr.occupations[0][0]
        if occupations is None:
            raise LookupError("Could not get occupations...")
        for band, occ in enumerate(occupations):
            # first occ to be 0 is the first conduction band.
            if not occ:
                bandstructure._logger.debug(
                        "Automatically found fermi band to be: "
                        f"{band - 1}.")
                bandstructure.fermi_band = band - 1
                break
        else:
            bandstructure._logger.debug(
                    "Couldn't detect fermi band from occupations: '"
                    f"{occupations}'.")
        return bandstructure

    @classmethod
    def from_qe_pw_calculation(cls, path, *args, **kwargs):
        """Classmethod that instanciate a BandStructure object
        from a QEPW Bandstructure calculation.

        Parameters
        ----------
        path: str
            The path to the QEPW calculation.
        """
        loglevel = kwargs.get("loglevel", logging.INFO)
        # get log file
        with CalculationDirectory.from_calculation(path) as calc:
            if calc.calctype != "qe_pw":
                raise TypeError("Not a qe_pw calculation.")
            meta = calc.meta_data_file
            with QEPWLogFile.from_meta_data_file(
                    meta, loglevel=loglevel) as log:
                bs = cls.from_qelog(log, *args, **kwargs)
                # try to get fermi level from parent calculation
            parents = meta.parents
            if len(parents) != 1:
                bs._logger.warning("Could not get fermi level"
                                   " from parent "
                                   "calculation.")
                return bs
            try:
                with QEPWLogFile.from_calculation(
                        parents[0], loglevel=loglevel) as log:
                    log.read()
                    bs.fermi_energy = log.fermi_energy
                    bs._logger.debug(f"Setting fermi level from parent "
                                     f"calculation: {log.fermi_energy}")
            except Exception as e:
                bs._logger.error("An error occured while trying to get"
                                 " fermi energy from parent "
                                 "calculation: "
                                 f"{tb.print_tb(e.__traceback__)}:"
                                 f" {e}")
            return bs

    @classmethod
    def from_qelog(cls, path, *args, **kwargs):
        """Class method to read kpts/eigenvalues directly from a QE log file.

        Sets the kpts/qpts and the energies but still need to set fermi_energy
        or fermi_band manually if necessary after that.

        Parameters
        ----------
        path : The log file path or QEPWLogFile object.
        """
        bandstructure = cls(*args, **kwargs)
        if isinstance(path, str):
            log = QEPWLogFile.from_file(path, loglevel=bandstructure._loglevel)
        else:
            log = path
        if not isinstance(log, QEPWLogFile):
            raise TypeError("Was expecting either Log file object or path to "
                            f"log file but got: {log}")
        log.read()
        bandstructure.kpts = np.array(log.eigenvalues["k_points"])
        bandstructure.energies = np.array(log.eigenvalues["eigenvalues"])
        return bandstructure

    # TODO: rebase this class method with the 'from_wannier90' one.
    @classmethod
    def from_epw(cls, path, *args, **kwargs):
        """Get bandstructure from a wannier90 calculation.
        """
        bs = cls(*args, **kwargs)
        calc = CalculationDirectory.from_calculation(
                path, loglevel=bs._loglevel)
        # check if bands_plot=True flag has been set in input file
        with calc.meta_data_file as meta:
            wannier_input = os.path.join(meta.rundir, meta.jobname + ".win")
            with Wannier90InputFile.from_file(wannier_input) as input_file:
                bands_plot = input_file.input_variables.get(
                        "bands_plot", False)
                if not bands_plot:
                    raise ValueError(
                            "EPW must be run with wdata(X)='bands_plot=True'.")
        # check if 'filkf' has been defined. If yes, use the kpts from this
        with calc.input_file as inf:
            filkf = inf.input_variables.get("filkf", None).value
        if filkf is not None:
            # get kpts from the band.eig file
            # if not os.path.isabs(filkf):
            #     # filkf is relative to rundir.
            #     filkf = full_abspath(os.path.join(
            #         calc.run_directory.path, filkf))
            # # load kpts (skip first line which is irrelevent)
            # bs.kpts = np.loadtxt(filkf, skiprows=1, usecols=(0, 1, 2))
            # read eigs from the bands.eig file
            bs = cls.from_qeepw_bandeig(path, *args, **kwargs)
        else:
            # read kpts and eigs from the W90 files
            with Wannier90BandDatFile.from_calculation(
                    calc, loglevel=bs._loglevel) as banddat:
                bs.energies = np.array(banddat.bands).T
            with Wannier90BandkptFile.from_calculation(
                    calc, loglevel=bs._loglevel) as bandkpt:
                bs.kpts = bandkpt.kpts
        # get fermi level
        with calc.meta_data_file as meta:
            # first get the nscf preparation run calculation
            for parent in meta.parents:
                with MetaDataFile.from_calculation(parent) as meta_parent:
                    if meta_parent.calctype == "qe_pw":
                        # found it
                        parent_calc = parent
                        break
            else:
                raise FileNotFoundError("Could not find nscf parent "
                                        "calculation.")
        with QEPWLogFile.from_calculation(
                parent_calc, loglevel=bs._loglevel) as log:
            if log.fermi_energy is not None:
                bs.fermi_energy = log.fermi_energy
                return bs
        bs._logger.debug("Could not find fermi energy... trying parent calc")
        with MetaDataFile.from_calculation(
                parent_calc, loglevel=bs._loglevel) as meta:
            parent_calc = meta.parents[0]
        with QEPWLogFile.from_calculation(
                parent_calc, loglevel=bs._loglevel) as log:
            if log.fermi_energy is not None:
                bs.fermi_energy = log.fermi_energy
                return bs
        raise ValueError("Could not find fermi energy.")

    @classmethod
    def from_qeepw_bandeig(cls, filepath, *args, **kwargs):
        """Create a BandStructure object from a band.eig file created by the
        epw.x script from Quantum Espresso.

        Parameters
        ----------
        filepath : str
            Path to the band.eig file or calculation.

        Returns
        -------
        BandStructure object with kpts and eigenvalues read from the file.
        """
        bs = cls(*args, **kwargs)
        if CalculationDirectory.is_calculation_directory(filepath):
            classmeth = QEEPWBandEigFile.from_calculation
        else:
            classmeth = QEEPWBandEigFile.from_file
        with classmeth(
                filepath, loglevel=bs._loglevel) as eigs:
            bs.kpts = eigs.coordinates
            bs.energies = eigs.eigenvalues
        return bs

    @classmethod
    def from_wannier90(cls, path, *args, **kwargs):
        """Get bandstructure from a wannier90 calculation.
        """
        bs = cls(*args, **kwargs)
        calc = CalculationDirectory.from_calculation(
                path, loglevel=bs._loglevel)
        # check if bands_plot=True flag has been set in input file
        with calc.input_file as input_file:
            bands_plot = input_file.input_variables.get("bands_plot", False)
            if not bands_plot:
                raise ValueError(
                        "Wannier90 must be run with 'bands_plot'=True.")
        # get bands and kpts
        with Wannier90BandDatFile.from_calculation(
                calc, loglevel=bs._loglevel) as banddat:
            bs.energies = np.array(banddat.bands).T
        with Wannier90BandkptFile.from_calculation(
                calc, loglevel=bs._loglevel) as bandkpt:
            bs.kpts = bandkpt.kpts
        # get fermi level
        with calc.meta_data_file as meta:
            # first get the nscf preparation run calculation
            for parent in meta.parents:
                with MetaDataFile.from_calculation(parent) as meta_parent:
                    if meta_parent.calctype == "qe_pw":
                        # found it
                        parent_calc = parent
                        break
                    if meta_parent.calctype == "qe_pw2wannier90":
                        # should find parent in this calc
                        with MetaDataFile.from_calculation(
                                meta_parent.calc_workdir) as meta_parent_p2w:
                            for parent_p2w in meta_parent_p2w.parents:
                                with MetaDataFile.from_calculation(
                                        parent_p2w,
                                        loglevel=bs._loglevel) as meta_parent2:
                                    if meta_parent2.calctype == "qe_pw":
                                        # found it
                                        parent_calc = parent_p2w
                                        break
                            else:
                                raise ValueError(
                                        "Could not find nscf parent.")
                        break
            else:
                raise FileNotFoundError("Could not find nscf parent "
                                        "calculation.")
        with QEPWLogFile.from_calculation(
                parent_calc, loglevel=bs._loglevel) as log:
            if log.fermi_energy is not None:
                bs.fermi_energy = log.fermi_energy
                return bs
        bs._logger.debug("Could not find fermi energy... trying parent calc")
        with MetaDataFile.from_calculation(
                parent_calc, loglevel=bs._loglevel) as meta:
            parent_calc = meta.parents[0]
        with QEPWLogFile.from_calculation(
                parent_calc, loglevel=bs._loglevel) as log:
            if log.fermi_energy is not None:
                bs.fermi_energy = log.fermi_energy
                return bs
        raise ValueError("Could not find fermi energy.")

    @classmethod
    def from_calculation(cls, path, *args, **kwargs):
        """Returns a Bandstructure object from a calculation directory.

        This method reads the metadata file and deduces which output file
        to read.
        """
        lvl = kwargs.get("loglevel", logging.INFO)
        with MetaDataFile.from_calculation(path, loglevel=lvl) as meta:
            calctype = meta.calctype
            if calctype == "qe_pw":
                return cls.from_qe_pw_calculation(path, *args, **kwargs)
            elif calctype == "abinit":
                return cls.from_abinit_calculation(path, *args, **kwargs)
            elif calctype == "wannier90":
                return cls.from_wannier90(path, *args, **kwargs)
            elif calctype == "qe_epw":
                return cls.from_epw(path, *args, **kwargs)
            else:
                raise NotImplementedError(calctype)

    def _get_kpts_axis(self, labels_loc):
        # return list(range(len(self.kpts)))
        # labels loc are the locations of the high symmetry points defining
        # sub paths in the kpts path
        # we want to get the xaxis pts such that the lengths of the subpaths in
        # reciprocal space are proportional to the lengths of corresponding
        # sections on band structure.
        # first get all subpaths
        assert labels_loc is not None
        if 0 not in labels_loc:
            raise LookupError(f"First kpt label not at 0: {labels_loc}")
        if len(self.kpts) - 1 not in labels_loc:
            raise LookupError(f"Last kpt don't have a label: "
                              f"{len(self.kpts) - 1} not in {labels_loc}")
        subpaths = []
        subpath = None
        for ikpt in range(len(self.kpts)):
            if ikpt in labels_loc:
                if subpath is None:
                    subpath = []
                    subpath.append(ikpt)
                    continue
                else:
                    subpath.append(ikpt)
                    subpaths.append(subpath)
                    subpath = [ikpt]
                    continue
            else:
                subpath.append(ikpt)
        # there should be n_labels - 1 kpts subpaths
        assert len(subpaths) == len(labels_loc) - 1
        # get the lengths in reciprocal space of each subpaths
        lengths = []
        nkpts_per_subpath = []
        for subpath in subpaths:
            k_initial = self.kpts[subpath[0]]
            k_final = self.kpts[subpath[-1]]
            length = np.linalg.norm(k_final - k_initial)
            if length <= 1e-6:
                # very small length => probably same point
                raise RuntimeError(
                        "Missing labels in kpt path => subpaths have 0 length!"
                        )
            lengths.append(np.linalg.norm(k_final - k_initial))
            nkpts_per_subpath.append(len(subpath))
        total_length = sum(lengths)
        # check number of kpts in nkpts_per_subpath matches total nb of kpts
        assert (sum(nkpts_per_subpath) - len(nkpts_per_subpath) + 1 ==
                len(self.kpts))
        # get the first and final positions of
        # the subpath on the band structure
        x_finals = []
        x_initials = []
        for j in range(len(nkpts_per_subpath)):
            x_finals.append(sum(lengths[:j+1]) / total_length)
            x_initials.append(sum(lengths[:j]) / total_length)
        xs = []
        for j, (x_initial, x_final, nkpts) in enumerate(
                zip(x_initials, x_finals, nkpts_per_subpath)):
            if j == len(nkpts_per_subpath) - 1:
                xs += np.linspace(
                        x_initial, x_final, num=nkpts, endpoint=True).tolist()
                continue
            xs += np.linspace(
                    x_initial, x_final, num=nkpts - 1, endpoint=False).tolist()
        # need to update the position of the labels location
        new_labels_loc = [labels_loc[0]]  # first label loc is 0 anyways
        new_labels_loc += x_finals
        # check that number of generated points matches number of kpts
        assert len(xs) == len(self.kpts)
        return xs, new_labels_loc
