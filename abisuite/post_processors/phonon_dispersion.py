import logging
import numpy as np
import os

from .band_structure import BandStructure
from ..handlers import (
        AbinitAnaddbInputFile, AbinitAnaddbPhfrqFile, MetaDataFile,
        QEEPWPHBandFreqFile, QEMatdynFreqFile,
        )
from ..handlers.file_parsers import AbinitAnaddbPhfrqParser


# special case of band structure with fermi energy = 0
class PhononDispersion(BandStructure):
    """Post processing tool that reads phonon frequencies files and plots
    a phonon dispersion.
    """
    _loggername = "PhononDispersion"

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self._fermienergy = 0.0

    @property
    def fermi_energy(self):
        return 0.0

    @property
    def frequencies(self):
        return self.energies

    @frequencies.setter
    def frequencies(self, freqs):
        self.energies = freqs

    # alias
    @property
    def qpoints(self):
        return self.qpts

    @qpoints.setter
    def qpoints(self, qpoints):
        self.qpts = qpoints

    @property
    def qpts(self):
        # samething as kpts
        return self.kpts

    @qpts.setter
    def qpts(self, qpts):
        # use kpts for qpts
        self.kpts = qpts

    @property
    def nqpoints(self):
        return self.nqpts

    @property
    def nqpts(self):
        return self.nkpts

    def compute_bandgap(self):
        # irrelevent for this class
        return 0.0

    @classmethod
    def from_calculation(cls, path, *args, **kwargs):
        """Returns a Bandstructure object from a calculation directory.

        This method reads the metadata file and deduces which output file
        to read.
        """
        lvl = kwargs.get("loglevel", logging.INFO)
        with MetaDataFile.from_calculation(path, loglevel=lvl) as meta:
            calctype = meta.calctype
            if calctype == "abinit_anaddb":
                filepath = AbinitAnaddbPhfrqParser._filepath_from_meta(meta)
                freqs = cls._load_freqs_from_abinit_anaddb_phfrq(
                        filepath, *args, **kwargs)
                qpts = cls._load_qpts_from_abinit_anaddb_input(
                        meta.input_file_path, *args, **kwargs)
                phdisp = cls(*args, **kwargs)
                phdisp.frequencies = freqs
                phdisp.qpts = qpts
                return phdisp
            elif calctype == "qe_matdyn":
                filepath = os.path.join(meta.output_data_dir,
                                        meta.jobname + ".freq")
                return cls.from_qe_matdyn_freq(filepath, *args, **kwargs)
            elif calctype == "qe_epw":
                # definite file name for EPW
                filepath = os.path.join(meta.rundir, "phband.freq")
                return cls.from_qe_epw_phbandfreq(filepath, *args, **kwargs)
            else:
                raise NotImplementedError(calctype)

    @classmethod
    def from_qe_matdyn_freq(cls, path, **kwargs):
        """Class method to read qpts and frequencies directly from a QE
        matdyn freq file.

        Parameters
        ----------
        path : The .freq file path.
        """
        return cls._from_any_qe_freq_file(path, QEMatdynFreqFile, **kwargs)

    @classmethod
    def from_qe_epw_phbandfreq(cls, path, **kwargs):
        """Class method to read qpts and frequencies directly from a QE
        phband.freq file produced by epw.x.

        Parameters
        ----------
        path : The .freq file path.
        """
        return cls._from_any_qe_freq_file(path, QEEPWPHBandFreqFile, **kwargs)

    @classmethod
    def _from_any_qe_freq_file(cls, path, filehandler, **kwargs):
        """Return a PhononDispersion object from any Quantum Espresso
        frequencies file path.
        """
        if not os.path.exists(path):
            raise FileNotFoundError(path)
        phdisp = cls(**kwargs)
        with filehandler.from_file(
                    path, loglevel=phdisp._loglevel) as freq:
            qpts = np.array(freq.coordinates)
            eigs = np.array(freq.eigenvalues)
        phdisp.qpts = qpts
        phdisp.frequencies = eigs
        return phdisp

    @classmethod
    def _load_freqs_from_abinit_anaddb_phfrq(cls, path, *args, **kwargs):
        """Returns the phonon frequencies from an anaddb phfrq file.
        """
        with AbinitAnaddbPhfrqFile.from_file(
                path, loglevel=kwargs.get("loglevel", logging.INFO)) as phfrq:
            return phfrq.frequencies

    @classmethod
    def _load_qpts_from_abinit_anaddb_input(cls, path, *args, **kwargs):
        """Returns the list of qpts from the anaddb input file.
        """
        with AbinitAnaddbInputFile.from_file(
                path, loglevel=kwargs.get(
                    "loglevel", logging.INFO)) as inputfile:
            # discard the last column as it is a 'weight'
            return np.array(inputfile.input_variables["qph1l"].value)[:, :-1]
