import os
import subprocess

from .exceptions import TooManyCalculationsInQueueError
from .. import USER_CONFIG
from ..linux_tools import cd


__SUBMIT_SCRIPT_FROM_QUEUING_SYSTEM__ = {
        "local": "bash",
        "slurm": "sbatch",
        "torque": "qsub",
        "grid_engine": "qsub",
        "pbs_professional": "qsub",
        }


def submit_or_launch(pbs_file, background=False):
    """Submits or launch a calculation.

    Parameters
    ----------
    pbs_file: PBSFile object
        The PBS file that we want to submit or launch.
    background : bool, optional
                 If True, all jobs will be submitted in the background
                 (with subprocess.Popen instead of subprocess.run).
                 This parameter is not considered for jobs that need
                 to be submitted through a queuing system.
    """
    runfunc = subprocess.run if not background else subprocess.Popen
    exe = __SUBMIT_SCRIPT_FROM_QUEUING_SYSTEM__[pbs_file.queuing_system]
    command = [exe, pbs_file.path]
    if pbs_file.queuing_system != "local":
        try:
            check_queue()
        except TooManyCalculationsInQueueError as e:
            pbs_file._logger.error(
                f"Cannot launch '{pbs_file.jobname}' ('{pbs_file.path}')"
                )
            raise e
    with cd(os.path.dirname(pbs_file.path)):
        runfunc(command)


def check_queue(max_jobs_in_queue=None):
    """Checks if the number of jobs in queue exceeds a user given threshold.

    Parameters
    ----------
    max_jobs_in_queue: int, optional
        Gives the maximum amount of jobs allowed in queue. If maximum is
        reached, an error is raised. If None, takes the attribute set in
        the config file. If this one is None as well, no limit is set.
    """
    if max_jobs_in_queue is None:
        if USER_CONFIG.SYSTEM.max_jobs_in_queue is not None:
            max_jobs_in_queue = USER_CONFIG.SYSTEM.max_jobs_in_queue
    if max_jobs_in_queue is None:
        return
    # there is a maximum defined. check we do not overload it
    from abisuite import SYSTEM_QUEUE
    with SYSTEM_QUEUE as queue:
        if queue.njobs >= max_jobs_in_queue:
            # maximum jobs reached. raise an error
            raise TooManyCalculationsInQueueError(
                    f"There are {queue.njobs} running jobs already"
                    f", which is >= than the maximum number of "
                    f"jobs allowed ("
                    f"{max_jobs_in_queue}).")


def find_cmd_from_config_entries(software, calctype):
    # try to locate the script name in the config file
    script = calctype.split("_")[-1]
    return USER_CONFIG[software.upper()][script + "_path"]
