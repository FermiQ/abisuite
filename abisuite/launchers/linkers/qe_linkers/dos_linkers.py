from .bases import BasePostQEPWLinker


class QEDOSfromQEPWLinker(BasePostQEPWLinker):
    """Linker class to link a qe_pw calculation to a qe_dos calculation.
    """
    _calctype = "qe_dos"
    _link_calctype = "qe_pw"
    _loggername = "QEDOSfromQEPWLinker"
