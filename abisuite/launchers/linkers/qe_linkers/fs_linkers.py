from .bases import BasePostQEPWLinker


class QEFSfromQEPWLinker(BasePostQEPWLinker):
    """Linker class to link a qe_pw calculation to a qe_fs calculation.
    """
    _calctype = "qe_fs"
    _link_calctype = "qe_pw"
    _loggername = "QEFSfromQEPWLinker"
