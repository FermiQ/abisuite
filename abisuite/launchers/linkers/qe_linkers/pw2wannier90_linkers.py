import os

from .bases import BasePostQEPWLinker
from ....handlers import PBSFile


class QEPW2Wannier90fromQEPWLinker(BasePostQEPWLinker):
    """Linker class to link a qe_pw calculation to a qe_pw2wanier90 calculation.
    """
    _calctype = "qe_pw2wannier90"
    _link_calctype = "qe_pw"
    _loggername = "QEPW2Wannier90fromQEPWLinker"


class QEPW2Wannier90fromWannier90Linker(BasePostQEPWLinker):
    """Linker class to link a wannier90 calculation to a qe_pw2wannier90
    calculation from Quantum Espresso.
    """
    _calctype = "qe_pw2wannier90"
    _link_calctype = "wannier90"
    _loggername = "QEPW2Wannier90fromWannier90Linker"

    def _link_files_from_calculation(self, meta_file):
        # this wannier90 run should have been run with the '-pp' flag
        # check it is the case
        with PBSFile.from_meta_data_file(
                meta_file, loglevel=self._loglevel) as pbs:
            pbs.read()
            if "-pp" not in pbs.command_arguments:
                raise ValueError("")
        # link 'nnkp' file which is located in same dir as input file
        self.add_input_file(os.path.join(meta_file.calc_workdir, "*.nnkp"))
        # also link parent calculation of this 'pp' run
        self.launcher.link_calculation(meta_file.parents[0])
