import os
import tempfile

from .bases import BaseQELinker
from ....handlers import SymLinkFile
from ....linux_tools import mkdir


class QEPWfromQEPWLinker(BaseQELinker):
    """Linker class to link a pw.x calculation to another pw.x calculation
    of Quantum Espresso.
    """
    _calctype = "qe_pw"
    _link_calctype = "qe_pw"
    _loggername = "QEPWfromQEPWLinker"

    def _link_files_from_calculation(self, metadata):
        """Link a previous pw calculation to this launcher.
        The relevant files will be linked.
        """
        # TODO: probably a better way to do this
        # need to link everything except for the 'wfc*.dat' files.
        # these files are located in the ".save" directory in output_data_dir
        # copy xml files though to prevent overwriting
        # (=> done in add_input_file method)
        outdir = metadata.output_data_dir
        # to achieve this, we need to create a temporary directory which will
        # contain the only links we need
        # we do this in order to keep the input in a .save subdir
        # NOTE: create temp to launcher attribute as we want the tempdir to be
        # destroyed only when launcher is destroyed.
        self.launcher._temp = tempfile.TemporaryDirectory()
        tempdir = self.launcher._temp.name
        # find the name of the 'save' directory
        savedirpath = self._search_for_save_dir(outdir)
        savename = os.path.basename(savedirpath)
        # make the same dir in the temp dir
        savepath = os.path.join(tempdir, savename)
        mkdir(savepath)
        # now symlink all non ".dat" files except for charge-density
        for subfile in os.listdir(savedirpath):
            if subfile.endswith(".dat") and subfile.startswith("wfc"):
                # don't symlink it
                continue
            symlinkfile = SymLinkFile(loglevel=self._loglevel)
            symlinkfile.path = os.path.join(savepath, subfile)
            symlinkfile.source = os.path.join(outdir, savename, subfile)
            symlinkfile.validate()
            symlinkfile.write()
        # add the temporary directory to the list of input files
        # copy data-file-scheme.xml since it is modified by new pw run.
        self.launcher.add_input_file(
                savepath, copy_file_with_suffix=["data-file-schema.xml"],
                keep_original_filename=True)
