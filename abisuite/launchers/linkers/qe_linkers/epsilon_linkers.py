from .bases import BasePostQEPWLinker


class QEEpsilonfromQEPWLinker(BasePostQEPWLinker):
    """Linker class to link a qe_pw calculation to a qe_epsilon calculation.
    """
    _calctype = "qe_epsilon"
    _link_calctype = "qe_pw"
    _loggername = "QEEpsilonfromQEPWLinker"
