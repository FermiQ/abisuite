from .bases import BasePostQEPWLinker


class QEProjwfcfromQEPWLinker(BasePostQEPWLinker):
    """Linker class to link a qe_pw calculation to a qe_projwfc calculation.
    """
    _calctype = "qe_projwfc"
    _link_calctype = "qe_pw"
    _loggername = "QEProjwfcfromQEPWLinker"
