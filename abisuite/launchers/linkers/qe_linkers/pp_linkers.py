from .bases import BasePostQEPWLinker


class QEPPfromQEPWLinker(BasePostQEPWLinker):
    """Linker class to link a qe_pw calculation to a qe_pp calculation.
    """
    _calctype = "qe_pp"
    _link_calctype = "qe_pw"
    _loggername = "QEPPfromQEPWLinker"
