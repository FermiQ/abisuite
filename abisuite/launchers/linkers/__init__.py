from .abinit_linkers import (
        AbinitAnaddbfromAbinitMrgddbLinker, AbinitfromAbinitLinker,
        AbinitMrgddbfromAbinitLinker, AbinitOpticfromAbinitLinker,
        )
from .qe_linkers import (
        QEDOSfromQEPWLinker,
        QEDynmatfromQEPHLinker,
        QEEPWfromQEEPWLinker, QEEPWfromQEMatdynLinker,
        QEEPWfromQEPHLinker,
        QEEPWfromQEPWLinker,
        QEEPWfromQEQ2RLinker, QEEpsilonfromQEPWLinker,
        QEFSfromQEPWLinker,
        QEMatdynfromQEQ2RLinker,
        QEPHfromQEPWLinker,
        QEPPfromQEPWLinker,
        QEProjwfcfromQEPWLinker,
        QEPWfromQEPWLinker,
        QEPW2Wannier90fromQEPWLinker, QEPW2Wannier90fromWannier90Linker,
        QEQ2RfromQEPHLinker,
        )
from .wannier90_linkers import (
        Wannier90fromQEPWLinker,
        Wannier90fromQEPW2Wannier90Linker,
        Wannier90fromWannier90Linker,
        )


__ALL_LINKERS__ = {
        "abinit": {
            "abinit": AbinitfromAbinitLinker,
            },
        "abinit_anaddb": {
            "abinit_mrgddb": AbinitAnaddbfromAbinitMrgddbLinker,
            },
        "abinit_mrgddb": {
            "abinit": AbinitMrgddbfromAbinitLinker,
            },
        "abinit_optic": {
            "abinit": AbinitOpticfromAbinitLinker,
            },
        "qe_dos": {
            "qe_pw": QEDOSfromQEPWLinker,
            },
        "qe_dynmat": {
            "qe_ph": QEDynmatfromQEPHLinker,
            },
        "qe_epw": {
            "qe_epw": QEEPWfromQEEPWLinker,
            "qe_matdyn": QEEPWfromQEMatdynLinker,
            "qe_ph": QEEPWfromQEPHLinker,
            "qe_pw": QEEPWfromQEPWLinker,
            "qe_q2r": QEEPWfromQEQ2RLinker,
            },
        "qe_epsilon": {
            "qe_pw": QEEpsilonfromQEPWLinker,
            },
        "qe_fs": {
            "qe_pw": QEFSfromQEPWLinker,
            },
        "qe_matdyn": {
            "qe_q2r": QEMatdynfromQEQ2RLinker,
            },
        "qe_ph": {
            "qe_pw": QEPHfromQEPWLinker,
            },
        "qe_pp": {
            "qe_pw": QEPPfromQEPWLinker,
            },
        "qe_projwfc": {
            "qe_pw": QEProjwfcfromQEPWLinker,
            },
        "qe_pw": {
            "qe_pw": QEPWfromQEPWLinker,
            },
        "qe_pw2wannier90": {
            "qe_pw": QEPW2Wannier90fromQEPWLinker,
            "wannier90": QEPW2Wannier90fromWannier90Linker,
            },
        "qe_q2r": {
            "qe_ph": QEQ2RfromQEPHLinker,
            },
        "wannier90": {
            "qe_pw": Wannier90fromQEPWLinker,
            "qe_pw2wannier90": Wannier90fromQEPW2Wannier90Linker,
            "wannier90": Wannier90fromWannier90Linker,
            },
        }
