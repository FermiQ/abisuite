from ..bases import BaseLinker


class BaseWannier90Linker(BaseLinker):
    """Base class for all wannier90 linkers.
    """
    _calctype = "wannier90"
