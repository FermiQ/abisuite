from ..bases import BaseLinker


class AbinitMrgddbfromAbinitLinker(BaseLinker):
    """Linker to link an abinit calculation to a mrgddb calculation.
    """
    _calctype = "abinit_mrgddb"
    _link_calctype = "abinit"
    _loggername = "AbinitMrgddbfromAbinitLinker"

    def _link_files_from_calculation(self, *args, **kwargs):
        # link DDB files only
        nfiles = 0
        with self.calculation_to_link.output_data_directory as odd:
            for handler in odd:
                if handler.path.endswith("_DDB"):
                    self.add_input_file(handler.path)
                    nfiles += 1
            if not nfiles:
                raise FileNotFoundError(
                        "Could not find any DDB files to link in '{odd.path}'."
                        )
