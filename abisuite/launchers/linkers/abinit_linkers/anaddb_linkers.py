import os

from ..bases import BaseLinker


class AbinitAnaddbfromAbinitMrgddbLinker(BaseLinker):
    """Linker to link an mrgddb calculation to an anaddb calculation.
    """
    _calctype = "abinit_anaddb"
    _link_calctype = "abinit_mrgddb"
    _loggername = "AbinitAnaddbfromAbinitLinker"

    def _link_files_from_calculation(self, *args, **kwargs):
        # link the final DDB file whose path is stated in the input file.
        with self.calculation_to_link.input_file as input_file:
            ddb_path = input_file.output_file_path
        if not os.path.isabs(ddb_path):
            # join calculation directory
            ddb_path = os.path.join(self.calculation_to_link.path, ddb_path)
        if not os.path.isfile(ddb_path):
            raise FileNotFoundError(
                    f"Could not find DDB file at '{ddb_path}'."
                    )
        self.add_input_file(ddb_path)
