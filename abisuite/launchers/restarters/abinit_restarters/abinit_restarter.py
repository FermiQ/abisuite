from ..bases import BaseRestarter


class AbinitRestarter(BaseRestarter):
    """Abinit calculation restarter.
    """
    _calctype = "abinit"
    _loggername = "AbinitRestarter"

    def recover_calculation(self, *args, use_last_geometry=False, **kwargs):
        """Recovers an abinit calculation that either errored or
        succeeded. Useful if we want to restart from a converged
        calculation with new parameters.

        Parameters
        ----------
        use_last_geometry: bool, optional
            If True, the last atomic / cell geometry is used in case
            the recovered calculation was a relaxation run.
        """
        super().recover_calculation(*args, **kwargs)
        if not use_last_geometry:
            # nothing else to do
            return
        # need to dig calc for last geometry
        # first make sure last calculation was a relaxation one
        # that means ionmov >= 2
        ionmov = self.launcher.input_variables.get("ionmov", 0)
        if ionmov < 2:
            raise ValueError(
                    "Recovered calculation not a relaxation one. "
                    f"('ionmov'={ionmov}).")
        with self.calculation_to_recover.log_file as log:
            # read and set last geometry according to the ones
            # defined in the input file
            # abinit prints xcart, xred, etc but we only need one of them
            # check in the input file which was defined and take this one
            outs = log.output_variables
            for varname in self.launcher._geometry_variables:
                if varname not in outs:
                    continue
                for vargroup in (
                        self.launcher._one_from_these_geometry_variables):
                    if varname not in vargroup:
                        continue
                    else:
                        break
                else:
                    # variable was not in __ABINIT_ONE_FROM_THESE_...
                    # it is thus mandatory => add it to the input vars
                    self.launcher.input_variables[varname] = outs[varname]
                    continue
                # varname is in a __ONE_FROM_THESE__ group. check if it is
                # defined in the input file. if not skip
                if varname not in self.launcher.input_variables:
                    continue
                self.launcher.input_variables[varname] = outs[varname]
