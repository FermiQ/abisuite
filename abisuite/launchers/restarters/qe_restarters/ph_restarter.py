import os

from .bases import BaseQERestarter


class QEPHRestarter(BaseQERestarter):
    """Restarter class for a qe_ph calculation for the ph.x script of
    Quantum Espresso.
    """
    _calctype = "qe_ph"
    _loggername = "QEPHRestarter"

    def recover_calculation(self):
        """Sets variables in order to recover a previous ph.x
        calculation. Can be used for example if a calculation crashed or
        needs to be completed after a specific parallelization scheme
        splitted the representations in different parts and needs to be
        merged.

        This method will dig the calculation dir to link and recover
        all the input parameters and set the 'recover' flag to True if
        not already done. Also, all the parameters from the batch file
        will be reused.
        """
        # set this as the validation process will be a bit different
        # TODO: find a better way to do this that does not rely directly
        # on setting launchers properties
        self.launcher.is_recover_run = True
        super().recover_calculation()
        with self.calculation_to_recover.meta_data_file as meta:
            # # set input variables
            # ifcls = self._input_file_handler_class
            # inputfile = ifcls.from_meta_data_file(meta,
            #                                       loglevel=self._loglevel)
            # inputfile.read()
            # # remove file paths that depends on workdir and/or jobname
            # for var in ("fildyn", "prefix", "outdir", "title"):
            #     inputfile.input_variables.pop(var, None)
            # self.input_variables = inputfile.input_variables.copy()
            self.launcher.input_variables["recover"] = True
            # link everything in output data dir
            self.launcher.add_input_file(
                    os.path.join(meta.output_data_dir, "*"))
            # # set the pbs file parameters
            # pbs = PBSFile.from_meta_data_file(meta, loglevel=self._loglevel)
            # pbs.read()
            # set some properties by default to the same as it was in previous
            # run
            # for attr in pbs.structure.get_relevant_properties():
            #     if "path" in attr or attr == "jobname":
            #         # depends on workdir and/or jobname
            #         continue
            #     setattr(self.pbs_file, attr, getattr(pbs, attr))
            # meta.add_child(self.workdir)
        # only need one image if initially more than one
        # remove 'nimage' if present but keep number of pools
        # FIXME: this might only be useful for a recovering
        # TODO: this might be fixed by checking if 'nimage' was present in
        #       previous calculation and that the calculation is finished
        self.launcher.command_arguments.pop("-nimage", None)
        # self.meta_data_file.add_parent(path)
