from .bases import BaseQERestarter


class QEProjwfcRestarter(BaseQERestarter):
    """Restarter class for a qe_projwfc calculation for the projwfc.x script
    of Quantum Espresso.
    """
    _calctype = "qe_projwfc"
    _loggername = "QEProjwfcRestarter"
