import os

from .bases import BaseQERestarter
from ....handlers import CalculationDirectory
from ....routines import full_abspath, is_list_like


class QEEPWRestarter(BaseQERestarter):
    """Restarter class for a qe_epw calculation from the epw.x script from
    Quantum Espresso.
    """
    _calctype = "qe_epw"
    _loggername = "QEEPWRestarter"

    def recover_calculation(self, *args, pop_variables=None, **kwargs):
        """Recover from a previous epw calc.
        """
        # special case if 'filqf' and/or 'filkf' are defined. We need to give
        # full path (in case they are defined as relative path)
        # if update_variables is None:
        #     update_variables = {}
        # self._update_filkqf(update_variables)
        if pop_variables is None:
            pop_variables = []
        if is_list_like(pop_variables) and not isinstance(pop_variables, list):
            # make it a list that we can append
            pop_variables = list(pop_variables)
        # before linking anything, check if we need to pop some variables
        # that will be automatically added when linking
        with self.calculation_to_recover.meta_data_file as meta:
            for parent in meta.parents:
                with CalculationDirectory.from_calculation(
                        parent, loglevel=self._loglevel) as calc:
                    if calc.calctype == "qe_matdyn":
                        # if defined filqf, a new file will be written
                        # by linker
                        pop_variables.append("filqf")
                    # samething for band structure calc
                    elif calc.calctype == "qe_pw":
                        with calc.input_file as ifile:
                            calculation = (
                                    ifile.input_variables["calculation"].value)
                            if calculation == "bands":
                                # pop filkf if defined
                                pop_variables.append("filkf")
        super().recover_calculation(
                *args,
                pop_variables=pop_variables,
                _add_parent=False, **kwargs)
        # need to link the previous calculations
        iv = self.launcher.input_variables
        epbread = iv.get("epbread", False).value is True
        epwread = iv.get("epwread", False).value is True
        # link previous epw file if necessary
        if any([epbread, epwread]):
            # since this is a recovering, allow failed calculation
            # when linking recovered calculation with epb files.
            self.launcher.link_calculation(
                    self.calculation_to_recover.path,
                    allow_calculation_failed=True)
            # also need to link fc file if needed
            lifc = iv.get("lifc", False).value
            if lifc:
                # iterate over parents and link the q2r file
                for parent in self.calculation_to_recover.parents:
                    with CalculationDirectory.from_calculation(parent) as calc:
                        if calc.calctype == "qe_q2r":
                            self.launcher.link_calculation(parent)
                            break
                else:
                    raise NotADirectoryError(
                            "'lifc' is True but could not find q2r calc in "
                            "parents.")
        else:
            # else we need to link all previous calculations
            with self.calculation_to_recover.meta_data_file as meta:
                for parent in meta.parents:
                    self.launcher.link_calculation(parent)

    def _update_filkqf(self, update_variables):
        with self.calculation_to_recover.input_file as input_file:
            for fil in ("filqf", "filkf"):
                if fil not in input_file.input_variables:
                    continue
                if fil in update_variables:
                    # don't reupdate it
                    continue
                # fil(k,q)f is defined. if not absolute path, change to abs
                filpath = input_file.input_variables[fil].value
                if not os.path.isabs(filpath):
                    filpath = full_abspath(
                            os.path.join(
                                self.calculation_to_recover.run_directory.path,
                                filpath))
                update_variables[fil] = filpath
                # need to link file to new launcher
                # self.launcher.add_input_file(
                #         update_variables[fil], keep_original_filename=True,
                #         copy=True)
