from .bases import BaseQERestarter


class QEPPRestarter(BaseQERestarter):
    """Restarter class for a qe_pp calculation for the pp.x script
    of Quantum Espresso.
    """
    _calctype = "qe_pp"
    _loggername = "QEPPRestarter"
