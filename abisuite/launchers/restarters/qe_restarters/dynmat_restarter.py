from .bases import BaseQERestarter


class QEDynmatRestarter(BaseQERestarter):
    """Restarter class for a qe_dynmat calculation for the dynmat.x script
    of Quantum Espresso.
    """
    _calctype = "qe_dynmat"
    _loggername = "QEDynmatRestarter"
