from .bases import BaseQERestarter


class QEEpsilonRestarter(BaseQERestarter):
    """Restarter class for a qe_epsilon calculation for the epsilon.x script
    of Quantum Espresso.
    """
    _calctype = "qe_epsilon"
    _loggername = "QEEpsilonRestarter"
