from .bases import BaseQERestarter


class QEFSRestarter(BaseQERestarter):
    """Restarter class for a qe_fs calculation for the fs.x script
    of Quantum Espresso.
    """
    _calctype = "qe_fs"
    _loggername = "QEFSRestarter"
