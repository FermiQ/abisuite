from ...bases import BaseCalctypedUtility
from ...handlers import CalculationDirectory, PBSFile
from ...routines import full_abspath


class BaseRestarter(BaseCalctypedUtility):
    """Base class for a Restarter object, The main purpose of these objects is
    to restart a specific calculation.
    """

    def __init__(self, launcher, *args, **kwargs):
        """Restarted init method.

        Parameters
        ----------
        launcher : Launcher object
            The Launcher object onto which the Restarter acts.
        """
        super().__init__(*args, **kwargs)
        self.launcher = launcher
        self._calculation_to_recover = None

    @property
    def calculation_to_recover(self):
        return self._calculation_to_recover

    @calculation_to_recover.setter
    def calculation_to_recover(self, calc):
        if not isinstance(calc, CalculationDirectory):
            if not isinstance(calc, str):
                raise TypeError(f"Expected a path but got: '{calc}'.")
            calc = full_abspath(calc)
            if not CalculationDirectory.is_calculation_directory(calc):
                raise NotADirectoryError(
                        f"Not a calculation directory: '{calc}'.")
            calc = CalculationDirectory.from_calculation(
                    calc, loglevel=self._loglevel)
        with calc:
            if calc.calctype != self._calctype:
                raise TypeError(
                        "Calctype not good: '{calc.calctype}'.")
        self._calculation_to_recover = calc

    def recover_calculation(
            self, update_variables=None, pop_variables=None,
            keep_pbs_file_parameters=True, _add_parent=True):
        """Recover from a previous calculation.

        This method sets the same input variables and batch file parameters
        as the previous calculation. Also add parent/child in meta files if
        calculation directory is different.

        Parameters
        ----------
        pop_variables : list, optional
            Name of variables to remove from the original calculation.
        update_variables : dict, optional
            If not None, it is a dict that will update the input variables.
        keep_pbs_file_parameters: bool, optional
            If True (default), the pbs file attributes are copied from the
            recovered calculation.
        """
        # _add_parent = True means the recovered calc will be added to the
        # list of parents of the present calc
        with self.calculation_to_recover.meta_data_file as meta:
            # set input variables
            ifcls = self.launcher._input_file_handler_class
            with ifcls.from_meta_data_file(
                    meta, loglevel=self._loglevel) as inputfile:
                inputs = inputfile.input_variables.copy().todict()
                if update_variables is not None:
                    inputs.update(update_variables)
                if pop_variables is not None:
                    for var in pop_variables:
                        inputs.pop(var, None)
                self.launcher.input_variables = inputs
            # set the pbs file parameters
            # set some properties by default to the same as it was in previous
            # run
            if keep_pbs_file_parameters:
                with PBSFile.from_meta_data_file(
                        meta, loglevel=self._loglevel) as pbs:
                    for attr in pbs.structure.get_relevant_properties():
                        if "path" in attr or attr == "jobname":
                            # depends on workdir and/or jobname
                            continue
                        setattr(self.launcher.pbs_file,
                                attr, getattr(pbs, attr))
            if self.launcher.workdir != meta.calc_workdir:
                # add children and parents
                # try except here in case meta to recover is read only mode
                # if can't write child it does't really matter
                # but tell user
                try:
                    meta.add_child(self.launcher.workdir)
                    # reset state in order to prevent write when leaving
                    # the context manager
                    meta.set_state()
                    # need to avoid updating last modified property which is
                    # normally updated when writing meta data file.
                    meta.write(bypass_last_modified_update=True)
                except OSError:
                    # read only mode most likely
                    self._logger.warning(
                            "Can't add child to calculation to "
                            f"recover from: {self.calculation_to_recover.path}"
                            )
                else:
                    # if can write, redo write to update last_modified property
                    meta.write()
                if _add_parent:
                    self.launcher.meta_data_file.add_parent(
                            self.calculation_to_recover.path)
