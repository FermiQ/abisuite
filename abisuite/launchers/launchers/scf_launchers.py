from .bases import BaseLauncher
from ...exceptions import DevError
from ...handlers import CalculationDirectory
from ...routines import is_list_like


class BaseSCFLauncher(BaseLauncher):
    """Base class for a SCF  calculation launcher. Implements some usegul
    functions common to all scf calculations.
    """
    _geometry_variables = None
    _one_from_these_geometry_variables = None  # can be None
    _kpts_variables = None
    _one_from_these_kpts_variables = None

    def __init__(self, *args, **kwargs):
        if self._geometry_variables is None:
            raise DevError(
                    "Need to set '_geometry_variables' for Launcher class "
                    f" '{self.__class__}'.")
        if self._one_from_these_geometry_variables is not None:
            for sublist in self._one_from_these_geometry_variables:
                if not is_list_like(sublist):
                    raise DevError(
                            "'_one_from_these_geometry_variables' should "
                            "only contain lists.")
                for varname in sublist:
                    if varname not in self._geometry_variables:
                        raise DevError(
                                f"'{varname}' should be in "
                                "'_geometry_variables'.")
        if self._kpts_variables is None:
            raise DevError(
                    "Need to set '_kpts_variables' for Launcher class "
                    f" '{self.__class__}'.")
        if self._one_from_these_kpts_variables is not None:
            for sublist in self._one_from_these_kpts_variables:
                if not is_list_like(sublist):
                    raise DevError(
                            "'_one_from_these_kpts_variables' should "
                            "only contain lists.")
                for varname in sublist:
                    if varname not in self._kpts_variables:
                        raise DevError(
                                f"'{varname}' should be in "
                                "'_kpts_variables'.")
        super().__init__(*args, **kwargs)

    def load_geometry_from(self, calc):
        """Use the geometry of another calculation.

        Note
        ----
        The calculation must be of the same 'calctype'.

        Parameters
        ----------
        calc : path
               The path to calculation where we load the geometry from.
        """
        self._load_variables_from(
                calc, self._geometry_variables,
                one_from_them=self._one_from_these_geometry_variables)

    def load_kpts_from(self, calc):
        """Use the kpt grid from another calculation.

        Note
        ----
        The calculation must be of the same 'calctype'.

        Parameters
        ----------
        calc : path
               The path to calculation where we load the geometry from.
        """
        self._load_variables_from(
                calc, self._kpts_variables,
                one_from_them=self._one_from_these_kpts_variables)

    def _load_variables_from(self, calc, variables, one_from_them=None):
        """Loads the required variables from a calculation.

        Parameters
        ----------
        calc: str
            The path to the calculation to load the variables from.
        variables: list-like
            The list of variables to load from.
        one_from_them: list of list-like
            Lists of lists. Each list is a list of varnames from which
            one variable must be loaded. E.g.: [xcart, xred] one of
            them must be loaded but the others might not be present.
        """
        # actually load the variables from another calculation
        if one_from_them is None:
            one_from_them = [[]]
        try:
            self.input_variables
        except ValueError:
            raise ValueError("Put the other variables first.")
        self._logger.info(f"Loading variables from: '{calc}'.")
        calc = CalculationDirectory.from_calculation(
                calc, loglevel=self._loglevel)
        # load from input file
        with calc.input_file as input_file:
            # make check for variables in 'one_of_them'. Make sure that
            # there is one variable in the input file from each sublist
            for sublist in one_from_them:
                if not sublist:
                    # empty sublist
                    continue
                npresent = 0
                for subvar in sublist:
                    # make sure one of them is in the invars
                    if subvar in input_file.input_variables:
                        npresent += 1
                if npresent > 1:
                    raise LookupError(
                            f"More than one variables from '{sublist}' is "
                            "present in input file.")
            # from here, everything is ok.
            for var in variables:
                if "*" in var:
                    # need to decompose variable name and
                    # loop over all variables
                    # in the input file
                    start = var.split("*")[0]
                    end = var.split("*")[-1]
                    for varname, value in input_file.input_variables.items():
                        if varname.startswith(start) and varname.endswith(end):
                            self.input_variables.update({varname: value})
                    continue
                if any([var in sublist for sublist in one_from_them]):
                    # varname is part of a one_from_them sublist
                    # if not in input_variables, just skip it
                    if var not in input_file.input_variables:
                        continue
                self.input_variables.update(
                        {var: input_file.input_variables[var]})
