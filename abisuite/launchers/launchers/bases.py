import abc
import datetime
import glob
import os

from abisuite import USER_CONFIG
from ..linkers import __ALL_LINKERS__
from ..restarters import __ALL_RESTARTERS__
from ..routines import find_cmd_from_config_entries, submit_or_launch
from ...bases import BaseCalctypedUtility, BaseUtility
from ...exceptions import DevError
from ...databases import AbiDB
from ...handlers import CalculationDirectory, MetaDataFile, PBSFile
from ...linux_tools import mkdir
from ...routines import full_abspath, is_list_like, return_list


class BaseLauncherPBSOnly(BaseCalctypedUtility, abc.ABC):
    """Base class for Launchers that only implements the PBS file.

    Useful for scripts like 'cut3d' that only process a single file.
    An input file is also implemented.

    Basically this does the same as the BaseLauncher class except
    that there is less structure (does not use the CalculationDirectory
    class).
    """
    _input_file_handler_class = None

    def __init__(self, *args, **kwargs):
        """BaseLauncherPBSOnly init method.
        """
        if self._input_file_handler_class is None:
            raise DevError("Need to set input file handler class.")
        self._executables_directory = None
        self._build = None
        self._pbs_file = None
        self._input_file = None
        super().__init__(*args, **kwargs)

    @property
    def build(self):
        """Build directory entry name as specified in the config file.
        """
        if self._build is not None:
            return self._build
        # set the default build
        self._logger.debug("Setting the default build for the calculation.")
        self.build = getattr(USER_CONFIG, self.software.upper()).default_build
        return self.build

    @build.setter
    def build(self, build):
        """Set a build instead of a command + executables directory
        """
        if build not in USER_CONFIG:
            raise ValueError(f"{build} is not defined in config file.")
        entry = USER_CONFIG[build]
        # set executables directory
        self.executables_directory = entry.build_path
        # set modules
        self.modules_to_use += entry.modules_to_use
        self.modules_to_load += entry.modules_to_load
        self.modules_to_swap += entry.modules_to_swap
        self.modules_to_unload += entry.modules_to_unload
        self._build = build
        # set default lines before if needed
        if not len(self.lines_before):
            self.lines_before = entry.default_lines_before
        # do nothing if there are already lines before to not overwrite them
        # or to conflict them

    @property
    def command(self):
        try:
            return self.pbs_file.command
        except ValueError:
            # need to set command using default command
            cmd = find_cmd_from_config_entries(self.software, self.calctype)
            self._process_command(cmd)
        return self.pbs_file.command

    @command.setter
    def command(self, command):
        # check that script exist and is coherant with Launcher
        if self._expected_script not in command:
            raise ValueError(f"Expected {self._expected_script} in command")
        self._process_command(command)

    @property
    def command_arguments(self):
        return self.pbs_file.command_arguments

    @command_arguments.setter
    def command_arguments(self, command_arguments):
        self.pbs_file.command_arguments = command_arguments

    @property
    def executables_directory(self):
        if self._executables_directory is not None:
            return self._executables_directory
        # if None, return the default dir stated in CONFIG
        self._logger.debug("Setting default executables directory "
                           "from config file.")
        self.build = USER_CONFIG[self.software.upper()]["default_build"]
        return self.executables_directory

    @executables_directory.setter
    def executables_directory(self, directory):
        directory = full_abspath(directory)
        if not os.path.isdir(directory):
            raise NotADirectoryError(f"{directory}")
        self._executables_directory = directory

    @property
    def _expected_script(self):
        return self.calctype.split("_")[-1]

    @property
    def input_file(self):
        if self._input_file is not None:
            return self._input_file
        self._input_file = self._input_file_handler_class(
                loglevel=self._loglevel)
        return self.input_file

    @property
    def jobname(self):
        try:
            return self.pbs_file.jobname
        except ValueError as e:
            self._logger.exception("An error occured while getting jobname.")
            raise e

    @jobname.setter
    def jobname(self, jobname):
        self.pbs_file.jobname = jobname

    @property
    def lines_after(self):
        return self.pbs_file.lines_after

    @lines_after.setter
    def lines_after(self, lines_after):
        self.pbs_file.lines_after = lines_after

    @property
    def lines_before(self):
        return self.pbs_file.lines_before

    @lines_before.setter
    def lines_before(self, lines_before):
        self.pbs_file.lines_before = lines_before

    @property
    def memory(self):
        return self.pbs_file.memory

    @memory.setter
    def memory(self, memory):
        self.pbs_file.memory = memory

    @property
    def memory_per_cpu(self):
        return self.pbs_file.memory_per_cpu

    @memory_per_cpu.setter
    def memory_per_cpu(self, mem):
        self.pbs_file.memory_per_cpu = mem

    # just an alias
    @property
    def modules(self):
        return self.modules_to_load

    @modules.setter
    def modules(self, modules):
        self.modules_to_load = modules

    @property
    def modules_to_load(self):
        return self.pbs_file.modules_to_load

    @modules_to_load.setter
    def modules_to_load(self, modules):
        self.pbs_file.modules_to_load = return_list(modules)

    @property
    def modules_to_unload(self):
        return self.pbs_file.modules_to_unload

    @modules_to_unload.setter
    def modules_to_unload(self, modules):
        self.pbs_file.modules_to_unload = return_list(modules)

    @property
    def modules_to_use(self):
        return self.pbs_file.modules_to_use

    @modules_to_use.setter
    def modules_to_use(self, modules):
        self.pbs_file.modules_to_use = return_list(modules)

    @property
    def modules_to_swap(self):
        return self.pbs_file.modules_to_swap

    @modules_to_swap.setter
    def modules_to_swap(self, modules):
        self.pbs_file.modules_to_swap = return_list(modules)

    @property
    def mpi_command(self):
        return self.pbs_file.mpi_command

    @mpi_command.setter
    def mpi_command(self, mpi_command):
        self.pbs_file.mpi_command = mpi_command

    @property
    def mpi_command_arguments(self):
        return self.pbs_file.mpi_command_arguments

    @mpi_command_arguments.setter
    def mpi_command_arguments(self, mpi_command_arguments):
        self.pbs_file.mpi_command_arguments = mpi_command_arguments

    @property
    def nodes(self):
        return self.pbs_file.nodes

    @nodes.setter
    def nodes(self, nodes):
        self.pbs_file.nodes = nodes

    @property
    def ntasks(self):
        return self.pbs_file.ntasks

    @ntasks.setter
    def ntasks(self, n):
        self.pbs_file.ntasks = n

    @property
    def pbs_file(self):
        if self._pbs_file is not None:
            return self._pbs_file
        self._pbs_file = PBSFile(loglevel=self._loglevel)
        return self.pbs_file

    @property
    def ppn(self):
        return self.pbs_file.ppn

    @ppn.setter
    def ppn(self, ppn):
        self.pbs_file.ppn = ppn

    @property
    def project_account(self):
        return self.pbs_file.project_account

    @project_account.setter
    def project_account(self, project_account):
        self.pbs_file.project_account = project_account

    @property
    def project_code(self):
        return self.pbs_file.project_code

    @project_code.setter
    def project_code(self, project_code):
        self.pbs_file.project_code = project_code

    # just an alias
    @property
    def qos(self):
        return self.quality_of_service

    @qos.setter
    def qos(self, qos):
        self.quality_of_service = qos

    @property
    def quality_of_service(self):
        return self.pbs_file.quality_of_service

    @quality_of_service.setter
    def quality_of_service(self, qos):
        self.pbs_file.quality_of_service = qos

    @property
    def queue(self):
        try:
            return self.pbs_file.queue
        except ValueError:
            raise ValueError("Need to set 'queue'.")

    @queue.setter
    def queue(self, queue):
        self.pbs_file.queue = queue

    @property
    def queuing_system(self):
        try:
            return self.pbs_file.queuing_system
        except ValueError:
            # set default
            self.queuing_system = USER_CONFIG.SYSTEM.queuing_system
        return self.queuing_system

    @queuing_system.setter
    def queuing_system(self, queuing_system):
        self.pbs_file.queuing_system = queuing_system

    @property
    def walltime(self):
        try:
            return self.pbs_file.walltime
        except ValueError as err:
            self._logger.error("Need to set 'walltime'.")
            raise err

    @walltime.setter
    def walltime(self, walltime):
        self.pbs_file.walltime = walltime

    def launch(self, *args, **kwargs):
        """Alias for the 'run' method.
        """
        self.run(*args, **kwargs)

    @abc.abstractmethod
    def run(self, *args, **kwargs):
        pass

    def _process_command(self, command):
        # process the command to make sure it is well set
        # copy arguments to make sure they are still set after processing cmd
        args_already_set = self.command_arguments.copy()
        split = command.split(" ")
        cmd = split[0]
        if not os.path.isfile(cmd):
            # try appending the executables directory
            cmd = os.path.join(self.executables_directory, cmd)
        if len(split) - 1:
            # reset arguments based on new cmd string
            cmd = " ".join([cmd] + split[1:])
            args_already_set = {}
        self.pbs_file.command = cmd
        if len(args_already_set):
            self.command_arguments = args_already_set


class BaseLauncher(BaseLauncherPBSOnly, abc.ABC):
    """Base class for any launchers. User should not use this class directly.
    User should either use the already implemented Launcher classes or
    subclass this class.

    A launcher object will collect file
    handlers to generate a calculation. It will take input variables and
    make some checks using Input Approvers objects to make sure input
    variables are consistent and make sense.

    workdir/ -> input file, log file, out file, stderr goes there
    ----input_data/ -> all input data from another calculation goes there
    ----run/ -> files file, pbs file some output files goes there
    -------output_data/ -> other outputs will go there
    """
    _paral_approver_class = None
    _parent_approver_class = None

    def __init__(self, jobname, *args, **kwargs):
        """Base launcher class.

        Parameters
        ----------
        jobname : str
                  This is the jobname of the calculation. All files will
                  have this name.
        """
        super().__init__(*args, **kwargs)
        self._calculation_directory = CalculationDirectory(
                self._input_file_handler_class,
                loglevel=self._loglevel
                )
        # set default queuing system from the start
        self._set_defaults()
        self.meta_data_file.calctype = self._calctype
        # set jobname at the end (at least after calculation directory created)
        self.jobname = jobname
        # project name for database
        self.project = ""
        self.allow_connection_to_database = True
        # create a list of files to delete after writing the job
        # (for temporary files)
        self._files_to_delete = []

    # def __del__(self):
    #     # delete temp files if they were not deleted for some reason
    #     # (e.g.: write was not called)
    #     self._delete_temporary_files()

    @property
    def calculation_directory(self):
        return self._calculation_directory

    @calculation_directory.setter
    def calculation_directory(self, calc):
        if not isinstance(calc, CalculationDirectory):
            raise TypeError(
                    "Expected a CalculationDirectory instance but got "
                    f"'{calc}' instead.")
        # make sure jobname is synced
        calc.jobname = self.jobname
        self._calculation_directory = calc

    @property
    def calctype(self):
        return self.meta_data_file.calctype

    @property
    def files_to_copy(self):
        return self.meta_data_file.copied_files

    @property
    def files_to_link(self):
        return self.meta_data_file.linked_files

    @property
    def input_data_dir(self):
        return self.calculation_directory.input_data_directory

    @property
    def input_file(self):
        return self.calculation_directory.input_file

    @property
    def input_variables(self):
        try:
            return self.input_file.input_variables
        except ValueError:
            raise ValueError("Need to set the input variables!")

    @input_variables.setter
    def input_variables(self, input_variables):
        self.input_file.input_variables = input_variables

    @input_variables.deleter
    def input_variables(self):
        del self.input_file.input_variables

    @property
    def jobname(self):
        try:
            return self.meta_data_file.jobname
        except Exception:
            return BaseLauncherPBSOnly.jobname.fget(self)

    @jobname.setter
    def jobname(self, jobname):
        BaseLauncherPBSOnly.jobname.fset(self, jobname)
        self.meta_data_file.jobname = jobname

    @property
    def output_data_dir(self):
        # output dir in run dir
        return self.calculation_directory.run_directory.output_data_directory

    @property
    def pbs_file(self):
        return self.calculation_directory.pbs_file

    # the pseudos property is not relevant to all launchers and thus it may
    # be just None
    @property
    def pseudos(self):
        return self.input_file.pseudos

    @pseudos.setter
    def pseudos(self, pseudos):
        self.input_file.pseudos = pseudos

    @property
    def rundir(self):
        return self.calculation_directory.run_directory

    @property
    def workdir(self):
        return self.calculation_directory.path

    @workdir.setter
    def workdir(self, workdir):
        workdir = full_abspath(workdir)
        self.calculation_directory.path = workdir
        self.meta_data_file.path = os.path.join(self.workdir,
                                                "." + self.jobname + ".meta")
        self.input_file.path = os.path.join(
                self.workdir,
                self.jobname + self.input_file._parser_class._expected_ending)
        self._set_pbs_file_paths()

    @property
    def meta_data_file(self):
        return self.calculation_directory.meta_data_file

    def add_input_directory(self, dirpath, *args, **kwargs):
        """Add a whole directory to link in the input data.

        This methods just recursively calls 'add_input_file'.
        """
        if is_list_like(dirpath):
            for path in dirpath:
                self.add_input_directory(path)
            return
        dirpath = full_abspath(dirpath)
        if "*" in dirpath:
            # stardef, call glob and add recursively
            self._logger.debug(f"Stardef found in: {dirpath}")
            alldirs = glob.glob(dirpath)
            self.add_input_directory(alldirs, *args,
                                     **kwargs)
            return
        if not os.path.isdir(dirpath):
            raise NotADirectoryError(dirpath)
        self._do_add_input_directory(dirpath, *args, **kwargs)

    def _do_add_input_directory(self, dirpath, *args, subdir=None,
                                keep_original_dirname=False, **kwargs):
        # at this point, dirpath SHOULD be a directory
        newsubdir = os.path.basename(dirpath)
        if not keep_original_dirname:
            # new subdirname is also changed
            newsubdir = self._get_target_name(newsubdir)
        if subdir is not None:
            subdir = os.path.join(subdir, newsubdir)
        else:
            subdir = newsubdir
        for name in os.listdir(dirpath):
            subpath = os.path.join(dirpath, name)
            self.add_input_file(subpath, *args, keep_original_dirname=False,
                                subdir=subdir, **kwargs)

    def add_input_file(self, filepath, *args, copy_file_with_suffix=None,
                       keep_original_dirname=False, **kwargs):
        """Add an input file to link to the present calculation.
        A symbolic link will be created to this file once the files
        are written (unless copy is True in which case the files are copied).

        Supports '*' notation.
        E.g.: 'file.in*' will add each file that starts with 'file.in'

        Parameters
        ----------
        filepath : str, list
                   The path to the file to link. Can be a list.
        copy : bool, optional
               If True, the file will be copied instead of linked via
               a symlink.
        copy_file_with_suffix : str, list-like, optional
                                If not None, states the file suffix to copy
                                instead of linking. Useful for softwares
                                who overwrite certain files to preserve
                                previous calculations.
        keep_original_filename : bool, optional
                                 If False, a target name for input file is
                                 computed from the 'Launcher's jobname'.
                                 Else the same filename is used.
        keep_original_dirname : bool, optional
                                Same as 'keep_original_filename but for
                                directories in case subdirectories happen.
        subdir : str, optional
                 If not None, this is a prefix to the filename inside the
                 input data directory that will be added to the target.
        """
        if is_list_like(filepath):
            for path in filepath:
                self.add_input_file(
                        full_abspath(path), *args,
                        copy_file_with_suffix=copy_file_with_suffix,
                        keep_original_dirname=keep_original_dirname,
                        **kwargs,
                        )
            return
        path = full_abspath(filepath)
        if "*" in path:
            self._logger.debug(f"Stardef found in: {path}")
            allfiles = glob.glob(path)
            self.add_input_file(allfiles, *args,
                                copy_file_with_suffix=copy_file_with_suffix,
                                keep_original_dirname=keep_original_dirname,
                                **kwargs)
            return
        # if a directory, append recursively and force subdir
        if os.path.isdir(path):
            self.add_input_directory(
                            path, *args,
                            copy_file_with_suffix=copy_file_with_suffix,
                            keep_original_dirname=keep_original_dirname,
                            **kwargs)
            return
        if copy_file_with_suffix is not None:
            if not is_list_like(copy_file_with_suffix):
                copy_file_with_suffix = (copy_file_with_suffix, )
        else:
            copy_file_with_suffix = []
        if not isinstance(copy_file_with_suffix, list):
            copy_file_with_suffix = list(copy_file_with_suffix)
        # from here we know its a file to link/copy
        self._do_add_input_file(
                        path,
                        copy_file_with_suffix, *args,
                        **kwargs)

    def _do_add_input_file(self, path,
                           copy_file_with_suffix,
                           copy=False,
                           keep_original_filename=False,
                           subdir=None):
        # From here, path SHOULD be a file and copy_file_with_suffix SHOULD
        # be a list
        for fileend in copy_file_with_suffix:
            if path.endswith(fileend):
                copy = True
                break
        if keep_original_filename:
            targetname = os.path.basename(path)
        else:
            targetname = self._get_target_name(path)
        target_path = self._get_target_path(targetname)
        if subdir is not None:
            target_path = os.path.join(os.path.dirname(target_path),
                                       subdir,
                                       os.path.basename(target_path))
        if copy:
            self._add_copied_file_to_calcdir(target_path, path)
        else:
            self._add_symlink_file_to_calcdir(target_path, path)

    def add_input_variables(self, new_variables):
        """Adds input variables to the input variables dictionary.

        Also updates variables if they were already there.
        """
        self.input_file.add_input_variables(new_variables)

    def clean_workdir(self):
        """Delete everything in the working directory.
        """
        self.calculation_directory.delete()

    def clear_linked_calculations(self):
        """Remove all traces of a linked calculation.
        """
        self.meta_data_file.parents = []  # reset parents in meta file
        # remove all input files
        self.clear_input_files()

    def clear_input_files(self):
        """Remove all input files to link and to copy.
        """
        self.input_data_dir.clear_content()
        self.meta_data_file.copied_files = []
        self.meta_data_file.linked_files = []

    def clear_input_variables(self):
        """Remove all input variables.
        """
        self.input_file.clear_input_variables()

    def clear_workdir(self):
        """Delete all files in workdir."""
        self.calculation_directory.delete()

    def link_calculation(
            self, directory, database=None,
            allow_calculation_not_started=False,
            allow_calculation_failed=False):
        """Link a calculation to the Launcher.

        Parameters
        ----------
        allow_calculation_failed : bool, optional
            If True, allows the calculation to link to have failed.
            Otherwise an error is raised.
        allow_calculation_not_started : bool, optional
            If True, the linked calculation is allowed to not been started.
            Useful for parallel calculations which don't rely on a result
            but on a specific input.
        directory : str or int
                    The path to the calculation directory.
                    (Must contain a '.meta' file).
                    Can also be an integer. In that case, it is the ID of a
                    calculation in the database given as a kwarg.
        database : str, optional
            If 'directory' is an integer, it will link against a calculation
            stored in the SQLite database given by this argument.
            If None, it tries to use the one given in the config file.
        """
        if self.calctype not in __ALL_LINKERS__:
            self._logger.error(
                    f"Cannot link a calculation because no linker have been "
                    f"defined for this calctype: '{self.calctype}'.")
            raise NotImplementedError(self.calctype)
        if isinstance(directory, int):
            # link agains a calculation in a SQLite database
            if database is None:
                database = USER_CONFIG.DATABASE.path
                if database is None:
                    raise ValueError(
                            "No database defined in config file.")
            abidb = AbiDB.from_file(database, loglevel=self._loglevel)
            directory = abidb[directory]
        directory = full_abspath(directory)
        if "*" in directory:
            self._logger.debug(f"Stardef found in: {directory}")
            alldirs = glob.glob(directory)
            for path in alldirs:
                self.link_calculation(path)
            return
        self._logger.info(f"Linking calculation with {directory}")
        if not CalculationDirectory.is_calculation_directory(directory):
            raise NotADirectoryError(
                    f"Not a calculation directory: {directory}")
        # if already linked, just don't care
        if directory in self.meta_data_file.parents:
            return
        # from this point we need to link using a linker
        # first get linker
        with CalculationDirectory.from_calculation(
                directory, loglevel=self._loglevel) as calc:
            if calc.calctype not in __ALL_LINKERS__[self.calctype]:
                self._logger.error(
                        f"Cannot link calculation of type '{calc.calctype}' "
                        f"to a calculation of type '{self.calctype}'. "
                        f"Either because it is not allower or either it is not"
                        f" implemented.")
                raise NotImplementedError(calc.calctype)
            linker_cls = __ALL_LINKERS__[self.calctype][calc.calctype]
            # instanciate linker class and add what we need
            linker = linker_cls(self, loglevel=self._loglevel)
            acns = allow_calculation_not_started
            linker.allow_calculation_not_started = acns
            linker.allow_calculation_failed = allow_calculation_failed
            linker.calculation_to_link = calc.path
            linker.link_calculation()

    # TODO: rebase this method with the 'link_calculation' one.
    def recover_from(self, directory, *args, database=None, **kwargs):
        """Recover a (possibly failed) calculation.

        Parameters
        ----------
        directory : str or int
            The path (or database id) to the calculation to recover from.
        database : str, optional
            If not None, it is the path to the sqlite database file to use tio
            get the recovered calculation path from.

        Other args and kwargs are passed to he Restarter objects when calling
        their 'recover_calculation' method.
        """
        if self.calctype not in __ALL_RESTARTERS__:
            self._logger.error(
                    f"Cannot recover a calculation because no restarter"
                    " have been "
                    f"defined for this calctype: '{self.calctype}'.")
            raise NotImplementedError(self.calctype)
        if isinstance(directory, int):
            # link agains a calculation in a SQLite database
            if database is None:
                database = USER_CONFIG.DATABASE.path
                if database is None:
                    raise ValueError(
                            "No database defined in config file.")
            abidb = AbiDB.from_file(database, loglevel=self._loglevel)
            directory = abidb[directory]
        directory = full_abspath(directory)
        self._logger.info(f"Recover from calculation: {directory}")
        if not CalculationDirectory.is_calculation_directory(directory):
            raise NotADirectoryError(
                    f"Not a calculation directory: {directory}")
        # from this point we need to invoke the restarted object.
        with CalculationDirectory.from_calculation(
                directory, loglevel=self._loglevel) as calc:
            if calc.calctype != self.calctype:
                raise ValueError(
                        "Cannot recover from a different calctype: "
                        f"'{calc.calctype}'.")
            restarter_cls = __ALL_RESTARTERS__[self.calctype]
            # instanciate restarter class and add what we need
            restarter = restarter_cls(self, loglevel=self._loglevel)
            restarter.calculation_to_recover = calc
            restarter.recover_calculation(*args, **kwargs)

    def validate(self, bypass_validation=False, bypass_pbs_validation=False):
        """Validates the input variables with the appropriate Approver class.

        Parameters
        ----------
        bypass_validation : bool, optional
                            If True, the input file is not validated.
        bypass_pbs_validation : bool, optional
            If True, the pbs file is not validated.
        """
        # call self.command here to atomatically set command if needed
        self.command
        if not bypass_pbs_validation:
            self.pbs_file.validate()
        if bypass_validation:
            return
        # check jobname length
        maxlen = USER_CONFIG.SYSTEM.max_jobname_len
        if maxlen is not None and self.queuing_system != "local":
            if len(self.jobname) > maxlen:
                raise ValueError(
                        f"Jobname '{self.jobname}' too long according to "
                        f"user config file (max {maxlen} chars.)")
        self.input_file.validate()
        # TODO: find a way to integrate this into file handlers
        # Note for this todo: I don't remember why we should do that (26/03/19)
        if self._paral_approver_class is not None:
            self._logger.debug("Cross validating input with mpi parameters.")
            cls = self._paral_approver_class
            inst = cls.from_approvers(self.input_file.approver,
                                      self.pbs_file.mpi_approver,
                                      loglevel=self._logger.level)
            input_paral_approver = inst
            input_paral_approver.validate()
            if not input_paral_approver.is_valid:
                input_paral_approver.raise_errors()
        if self._parent_approver_class is not None:
            self._logger.debug("Cross validating input variables with parent"
                               " calculation.")
            cls = self._parent_approver_class
            inst = cls.from_approver(
                    self.input_file.approver,
                    self.meta_data_file.parents,
                    loglevel=self._loglevel)
            inst.validate()
            if not inst.is_valid:
                inst.raise_errors()
        # check that all input files to link exist
        for path in self.meta_data_file.linked_files:
            if not os.path.exists(path):
                raise FileNotFoundError(f"File to link not found: {path}.")
        self._logger.info("Validation: OK -> ready for launching!")

    def run(self, **kwargs):
        """Run or submit the calculation (if on a supercluster).
        """
        # check that batch file is there
        if not os.path.isfile(self.pbs_file.path):
            raise FileNotFoundError(f"Cannot run calculation without batch"
                                    f" file: {self.pbs_file.path} use write()")
        if self.queuing_system not in (None, "local"):
            self._logger.info(f"Submitting {self.jobname}")
        else:
            self._logger.info(f"Launching {self.jobname}")
        submit_or_launch(self.pbs_file, **kwargs)
        with self.meta_data_file as meta:
            meta.submit_time = str(datetime.datetime.now())
        # NOTE: we link to database here mostly because of unittests.
        # If a database is defined in config file (path not None below) we
        # don't want to put test calculations in it automatically. Thus, since
        # unittest never call 'launch()' (for now 10/05/2019) we link here.
        # this goes the same for Sequencers.
        allow = self.allow_connection_to_database
        if USER_CONFIG.DATABASE.path is not None and allow:
            self.calculation_directory.connect_to_database()
            if self.calculation_directory.is_in_database:
                # reset status and monitored (used in restarts)
                self._logger.info(
                        "Calculation already in database. "
                        "Updating status and monitored attribute.")
                self.calculation_directory.reset_status()
                self.calculation_directory.update_database_monitored(True)
            else:
                # add calculation to db
                if self.project:
                    self._logger.info(
                            "Adding calculation to database under project: "
                            f"'{self.project}'.")
                else:
                    self._logger.info("Adding calculation to database.")
                self.calculation_directory.add_to_database()
                self.calculation_directory.update_database_project(
                    self.project)

    def write(self, force_queuing_system=None,
              bypass_validation=False, bypass_pbs_validation=False,
              overwrite=False, **kwargs):
        """Writes the files fot the calculation.

        Validates input parameters before writing files.

        Parameters
        ----------
        force_queuing_system : str, optional, {"local", "torque",
                                               "grid_engine", "slurm"}
                               Used to force the queuing system for the pbs
                               file. If None, the default system is used as
                               defined in the config file.
        bypass_validation : bool, optional
                            If True, the validation step for the input file
                            is skipped.
                            (Mainly used for testing purposes)
        bypass_pbs_validation : bool, optional
            Same as 'bypass_validation' but for pbs file only.
        overwrite : bool, optional
                    If False, an error is raised when trying to write the
                    files in an already existing calculation directory.

        Other kwargs are passed directly to all the writers write methods
        """
        # set the following before validating
        if force_queuing_system is not None:
            self.pbs_file.queuing_system = force_queuing_system
        # call self.command here to atomatically set command if needed
        self.command
        self.validate(
                bypass_validation=bypass_validation,
                bypass_pbs_validation=bypass_pbs_validation)
        # give submit time even if we don't use the 'run' method afterwards
        # (it will be updated anyways)
        self.meta_data_file.submit_time = str(datetime.datetime.now())
        if CalculationDirectory.is_calculation_directory(
                self.workdir) and not overwrite:
            raise IsADirectoryError(f"Is already a calculation directory: "
                                    f"{self.workdir}")
        self.calculation_directory.write(overwrite=overwrite, **kwargs)
        # append children to all parents
        for parent in self.meta_data_file.parents:
            with MetaDataFile.from_calculation(parent) as meta:
                meta.add_child(self.workdir)
        self._delete_temporary_files()

    @classmethod
    def restart_from(cls, path, *args, clear_output=False, relink=False,
                     run=True, new_workdir=None, **kwargs):
        """Set the meta data file and the pbs file from 'path', then
        immediately calls run().

        Parameters
        ----------
        path : str
               Which calculation needs to be restarted.
        clear_output : bool, optional
                       If True, all the content of the directory is cleared.
                       Then everything is rewritten.
        relink : bool, optional
                 If True, all parents are relinked.
        run : bool, optional
              If True, the calculation is immediately launched.
        new_workdir : str, optional
                      If not None, specifies a new workdir for the restarted
                      calculation. Everything is relinked after that.
        All other args and kwargs are passed to the new Launcher instance.
        """
        new = cls(*args, **kwargs)
        new.workdir = path
        new.meta_data_file.read()
        new.pbs_file.read()
        new.input_file.read()
        if new_workdir is not None:
            new.workdir = new_workdir
            # reset input variables if needed
            new.input_variables = new.input_variables
        if clear_output:
            new.clear_workdir()
            new.input_data_dir.clear_content()
            new.rundir.clear_content()
        # relink parents if needed
        if clear_output or relink or new_workdir is not None:
            for parent in new.meta_data_file.parents:
                new.link_calculation(parent)
        if run:
            new.run()
        return new

    def _add_copied_file_to_calcdir(self, target_path, path):
        if os.path.islink(path):
            path = os.readlink(path)
        if os.path.basename(
                self.input_data_dir.path
                ) in target_path:
            # add file in input data directory
            self.input_data_dir.add_copied_file(
                    target_path, path)
            # need to add manually to meta data file
            if path not in self.meta_data_file.copied_files:
                self.meta_data_file.copied_files.append(path)
        else:
            self.calculation_directory.add_copied_file(target_path, path)

    def _add_symlink_file_to_calcdir(self, target_path, path):
        """Adds a symlink that points to a certain input file.

        Parameters
        ----------
        target_path: str
            Where the symlinked will be written (it's final path).
        path: str
            The symlink source (where it points to).
        """
        if target_path.startswith(self.input_data_dir.path):
            # the symlink will be located into the input data dir
            if target_path not in self.input_data_dir:
                # only add if it does not exist already
                self.input_data_dir.add_symlink(
                    target_path, path)
            # need to add manually to meta data file
            if path not in self.meta_data_file.linked_files:
                self.meta_data_file.linked_files.append(path)
        else:
            self.calculation_directory.add_symlink(target_path, path)

    def _delete_temporary_files(self):
        # delete temporary files if necessary
        for file_to_delete in self._files_to_delete:
            if not isinstance(file_to_delete, str):
                # it's a file object
                name = file_to_delete.name
                file_to_delete.close()
                del file_to_delete
                file_to_delete = name
            if os.path.exists(file_to_delete):
                # else remove manually
                os.remove(file_to_delete)
            with self.meta_data_file as meta:
                # also delete from meta data file if they are present
                if file_to_delete in meta.copied_files:
                    meta.copied_files.remove(file_to_delete)
                if file_to_delete in meta.linked_files:
                    meta.linked_files.remove(file_to_delete)
        del self._files_to_delete
        self._files_to_delete = []  # reset

    def _get_target_name(self, target):
        return os.path.basename(target)

    def _get_target_path(self, targetname):
        return os.path.join(self.input_data_dir.path, targetname)

    def _set_defaults(self):
        # set default attributes according to config file
        self.queuing_system = USER_CONFIG.SYSTEM.queuing_system
        # set default parameters if relevant
        if USER_CONFIG.SYSTEM.default_memory not in (0, None):
            self.memory = USER_CONFIG.SYSTEM.default_memory
        if USER_CONFIG.SYSTEM.default_project_account is not None:
            self.project_account = USER_CONFIG.SYSTEM.default_project_account
        if USER_CONFIG.SYSTEM.default_project_code is not None:
            self.project_code = USER_CONFIG.SYSTEM.default_project_code
        if USER_CONFIG.SYSTEM.default_mpi_command is not None:
            self.mpi_command = USER_CONFIG.SYSTEM.default_mpi_command
        if USER_CONFIG.SYSTEM.default_lines_before:
            self.lines_before += USER_CONFIG.SYSTEM.default_lines_before

    def _set_pbs_file_paths(self):
        # set the pbs file when 'workdir' is set/modified
        self.pbs_file.path = os.path.join(self.rundir.path,
                                          self.jobname + ".sh")
        self.pbs_file.input_file_path = self.input_file.path
        self.pbs_file.log_path = os.path.join(self.workdir,
                                              self.jobname + ".log")
        self.pbs_file.stderr_path = os.path.join(self.workdir,
                                                 self.jobname + ".stderr")


class BaseMassLauncher(BaseUtility, abc.ABC):
    """The base class for mass launchers."""
    _launcher_class = None

    def __init__(self, jobnames, **kwargs):
        """Base mass launcher init method.

        Parameters
        ----------
        jobnames : list
                   The list of jobnames for each job.
        """
        super().__init__(**kwargs)
        if not is_list_like(jobnames):
            raise TypeError(f"jobnames must be list but received {jobnames}")
        self.jobnames = jobnames
        self._logger.debug(f"MassLauncher jobnames: {self.jobnames}")
        self.n_jobs = len(self.jobnames)
        self.launchers = [self._launcher_class(x, loglevel=self._logger.level)
                          for x in self.jobnames]
        self._workdir = None
        self._common_input_variables = None
        self._specific_input_variables = None

    @property
    def allow_connection_to_database(self):
        # same property for all sublaunchers.
        return self.launchers[0].allow_connection_to_database

    @allow_connection_to_database.setter
    def allow_connection_to_database(self, allow):
        for launcher in self.launchers:
            launcher.allow_connection_to_database = allow

    @property
    def command(self):
        # since this is the same property to each launcher, return the one
        # from the first launcher
        return self.launchers[0].command

    @command.setter
    def command(self, command):
        for launcher in self.launchers:
            launcher.command = command

    @property
    def common_input_variables(self):
        if self._common_input_variables is not None:
            return self._common_input_variables
        raise ValueError("Need to set the common input variables.")

    @common_input_variables.setter
    def common_input_variables(self, common_input_variables):
        self._logger.debug("Adding common input variables:"
                           f" {common_input_variables}")
        if not isinstance(common_input_variables, dict):
            raise TypeError("Variables must be a dict but received:"
                            f" {common_input_variables}.")
        self._common_input_variables = common_input_variables
        for launcher in self.launchers:
            launcher.input_variables = self.common_input_variables.copy()

    @property
    def memory(self):
        return self.launchers[0].memory

    @memory.setter
    def memory(self, memory):
        for launcher in self.launchers:
            launcher.memory = memory

    @property
    def memory_per_cpu(self):
        return self.launchers[0].memory_per_cpu

    @memory_per_cpu.setter
    def memory_per_cpu(self, mem):
        for launcher in self.launchers:
            launcher.memory_per_cpu = mem

    # just an alias
    @property
    def modules(self):
        return self.modules_to_load

    @modules.setter
    def modules(self, modules):
        self.modules_to_load = modules

    @property
    def modules_to_load(self):
        return self.launchers[0].modules_to_load

    @modules_to_load.setter
    def modules_to_load(self, modules):
        for launcher in self.launchers:
            launcher.modules_to_load = modules

    @property
    def modules_to_unload(self):
        return self.launchers[0].modules_to_unload

    @modules_to_unload.setter
    def modules_to_unload(self, modules):
        for launcher in self.launchers:
            launcher.modules_to_unload = modules

    @property
    def modules_to_use(self):
        return self.launchers[0].modules_to_use

    @modules_to_use.setter
    def modules_to_use(self, modules):
        for launcher in self.launchers:
            launcher.modules_to_use = modules

    @property
    def modules_to_swap(self):
        return self.launchers[0].modules_to_swap

    @modules_to_swap.setter
    def modules_to_swap(self, modules):
        for launcher in self.launchers:
            launcher.modules_to_swap = modules

    @property
    def mpi_command(self):
        # since this command is the same for each launcher, return the one
        # from the first launcher
        return self.launchers[0].mpi_command

    @mpi_command.setter
    def mpi_command(self, mpi_command):
        for launcher in self.launchers:
            launcher.mpi_command = mpi_command

    @property
    def nodes(self):
        return self.launchers[0].nodes

    @nodes.setter
    def nodes(self, nodes):
        for launcher in self.launchers:
            launcher.nodes = nodes

    @property
    def project(self):
        return self.launchers[0].project

    @project.setter
    def project(self, project):
        for launcher in self.launchers:
            launcher.project = project

    @property
    def project_account(self):
        return self.launchers[0].project_account

    @project_account.setter
    def project_account(self, project_account):
        for launcher in self.launchers:
            launcher.project_account = project_account

    @property
    def project_code(self):
        return self.launchers[0].project_code

    @project_code.setter
    def project_code(self, project_code):
        for launcher in self.launchers:
            launcher.project_code = project_code

    @property
    def ppn(self):
        return self.launchers[0].ppn

    @ppn.setter
    def ppn(self, ppn):
        for launcher in self.launchers:
            launcher.ppn = ppn

    # just an alias
    @property
    def qos(self):
        return self.quality_of_service

    @qos.setter
    def qos(self, qos):
        self.quality_of_service = qos

    @property
    def quality_of_service(self):
        # return the first launcher (assume same QOS for every launchers)
        return self.launchers[0].quality_of_service

    @quality_of_service.setter
    def quality_of_service(self, qos):
        for launcher in self.launchers:
            launcher.quality_of_service = qos

    @property
    def queuing_system(self):
        return self.launchers[0].queuing_system

    @queuing_system.setter
    def queuing_system(self, queuing_system):
        for launcher in self.launchers:
            launcher.queuing_system = queuing_system

    @property
    def walltime(self):
        return self.launchers[0].walltime

    @walltime.setter
    def walltime(self, walltime):
        for launcher in self.launchers:
            launcher.walltime = walltime

    @property
    def specific_input_variables(self):
        if self._specific_input_variables is not None:
            return self._specific_input_variables
        raise ValueError("Need to set specific variables.")

    @specific_input_variables.setter
    def specific_input_variables(self, specific_input_variables):
        self._logger.debug(f"Adding specific variables:"
                           f" {specific_input_variables}")
        if not is_list_like(specific_input_variables):
            raise TypeError(f"Specific variables should be a list but received"
                            f": {specific_input_variables}")
        if len(specific_input_variables) != self.n_jobs:
            raise ValueError(f"len(specific variables) "
                             f"({len(specific_input_variables)} != n_jobs "
                             f"({self.n_jobs})")
        self._specific_input_variables = specific_input_variables
        for launcher, spec_vars in zip(self.launchers,
                                       self.specific_input_variables):
            launcher.add_input_variables(spec_vars)

    @property
    def workdir(self):
        if self._workdir is not None:
            return self._workdir
        raise ValueError("Need to set the workdir.")

    @workdir.setter
    def workdir(self, workdir):
        self._workdir = full_abspath(workdir)
        for launcher in self.launchers:
            launcher.workdir = os.path.join(self.workdir, launcher.jobname)

    def add_input_file(self, *args, **kwargs):
        """Add input file to each launcher.
        """
        for launcher in self.launchers:
            launcher.add_input_file(*args, **kwargs)

    def clear_input_files(self, *args, **kwargs):
        """Clear input files for each launchers.
        """
        for launcher in self.launchers:
            launcher.clear_input_files(*args, **kwargs)

    def link_calculation(self, *args, **kwargs):
        """Link the same calculation to each launchers.
        """
        for launcher in self.launchers:
            launcher.link_calculation(*args, **kwargs)

    def link_calculations(self, calculations, *args, **kwargs):
        """Link one different calculation for each launcher.

        Parameters
        ----------
        calculations : list-like
                       The list of calculations to link. One for each launcher.
        All other args and kwargs are passed to the 'link_calculation'
        method of all sublaunchers.
        """
        if not is_list_like(calculations):
            if self.n_jobs == 1:
                calculations = (calculations, )
            else:
                raise TypeError(f"calculations must be a list but got "
                                f"{calculations}")
        if len(calculations) != self.n_jobs:
            raise ValueError(f"Was expecting {self.n_jobs} calculations but "
                             f"received {len(calculations)}")
        for calc, launcher in zip(calculations, self.launchers):
            launcher.link_calculation(calc, *args, **kwargs)

    def run(self, **kwargs):
        """Run all calculations. By default, all calculations
        are run in the foreground. The reason is to not overload
        the CPU if there is more jobs than number of processors
        available. Define jobs in parallel for optimization or use
        the background=True parameter to put all jobs in the background.
        """
        self._logger.info(f"Launching {self.n_jobs} different calculations.")
        for launcher in self.launchers:
            launcher.run(**kwargs)

    def validate(self, *args, **kwargs):
        """Validate all launchers.
        """
        for launcher in self.launchers:
            launcher.validate(*args, **kwargs)

    def write(self, **kwargs):
        """Write all the calculations.

        All kwargs are passed directly to each sublauncher.
        """
        self._logger.debug("Creating working directory for mass launcher.")
        mkdir(self.workdir)
        for launcher in self.launchers:
            launcher.write(**kwargs)
