import os

from ..bases import BaseLauncherPBSOnly
from ...routines import submit_or_launch
from ....handlers import CalculationDirectory, AbinitCut3DInputFile
from ....routines import full_abspath
from ....status_checkers.exceptions import CalculationNotFinishedError


class AbinitCut3DLauncher(BaseLauncherPBSOnly):
    """Launcher for the cut3d utility of Abinit.
    """
    _calctype = "abinit_cut3d"
    _loggername = "AbinitCut3DLauncher"
    _input_file_handler_class = AbinitCut3DInputFile

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self._workdir = None
        self.calculation_directory = None
        # set pbs file
        self.pbs_file.queuing_system = "local"
        self.pbs_file.command = self.command

    @property
    def workdir(self):
        if self.calculation_directory is None:
            raise ValueError("Need to set 'workdir'.")
        return self.calculation_directory.path

    @workdir.setter
    def workdir(self, workdir):
        self.calculation_directory = CalculationDirectory.from_calculation(
                full_abspath(workdir))
        # write next to the DEN file
        self.pbs_file.path = os.path.join(
                self.calculation_directory.output_data_directory.path,
                "cut3d.sh")
        self.input_file.path = os.path.join(
                self.calculation_directory.output_data_directory.path,
                "cut3d.in")
        self.pbs_file.input_file_path = self.input_file.path
        self.pbs_file.log_path = os.path.join(
                self.calculation_directory.output_data_directory.path,
                "cut3d.log")
        self.pbs_file.stderr_path = os.path.join(
                self.calculation_directory.output_data_directory.path,
                "cut3d.stderr")
        # get DEN filename
        for path in os.listdir(
                self.calculation_directory.output_data_directory.path):
            if "DEN" in path:
                self.input_file.data_file_path = path
                break
        else:
            raise FileNotFoundError("Could not find DEN file.")
        self.input_file.output_file_path = "density.xsf"
        self.input_file.option = 9
        self.input_file.post_option = ["n", 0]

    def write(self):
        self.pbs_file.write()
        self.input_file.write()

    def run(self):
        # only local run for now
        self._logger.info(f"Launching cut3d on calculation: '{self.workdir}'.")
        # if calculation not finished raise error
        with self.calculation_directory as calc:
            if calc.status["calculation_finished"] is False:
                raise CalculationNotFinishedError(self.workdir)
        submit_or_launch(self.pbs_file, background=False)
