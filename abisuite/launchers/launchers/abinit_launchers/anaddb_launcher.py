import os

from .bases import BaseAbinitLauncher
from ....handlers import (
        AbinitAnaddbFilesFile, AbinitAnaddbInputFile,
        )


class AbinitAnaddbLauncher(BaseAbinitLauncher):
    """Launcher class for an anaddb calculation.
    """
    _calctype = "abinit_anaddb"
    _files_file_handler_class = AbinitAnaddbFilesFile
    _input_file_handler_class = AbinitAnaddbInputFile
    _loggername = "AbinitAnaddbLauncher"

    @property
    def band_structure_file_path(self):
        return self.files_file.band_structure_file_path

    @band_structure_file_path.setter
    def band_structure_file_path(self, band_structure_file_path):
        self.files_file.band_structure_file_path = band_structure_file_path

    @property
    def ddb_file_path(self):
        return self.files_file.ddb_file_path

    @ddb_file_path.setter
    def ddb_file_path(self, ddb_file_path):
        self.files_file.ddb_file_path = ddb_file_path

    @property
    def ddk_file_path(self):
        return self.files_file.ddk_file_path

    @ddk_file_path.setter
    def ddk_file_path(self, ddk_file_path):
        self.files_file.ddk_file_path = ddk_file_path

    @property
    def eph_data_prefix(self):
        return self.files_file.eph_data_prefix

    @eph_data_prefix.setter
    def eph_data_prefix(self, eph_data_prefix):
        self.files_file.eph_data_prefix = eph_data_prefix

    @property
    def gkk_file_path(self):
        return self.files_file.gkk_file_path

    @gkk_file_path.setter
    def gkk_file_path(self, gkk_file_path):
        self.files_file.gkk_file_path = gkk_file_path

    @BaseAbinitLauncher.workdir.setter
    def workdir(self, workdir):
        BaseAbinitLauncher.workdir.fset(self, workdir)
        # put input DDB into input data directory
        self.ddb_file_path = os.path.join(
                self.input_data_dir.path,
                self._add_prefixes_to_target("DDB"))
        self.band_structure_file_path = os.path.join(
                self.output_data_dir.path,
                "phonon_band_structure.dat")
        self.gkk_file_path = os.path.join(
                self.input_data_dir.path,
                self.jobname + "_gkk")
        self.eph_data_prefix = os.path.join(
                self.output_data_dir.path,
                self.jobname + ".eph")
        self.ddk_file_path = os.path.join(
                self.input_data_dir.path,
                self.jobname + "_ddk")

    def validate(self, *args, **kwargs):
        super().validate(*args, **kwargs)
        # check that a DDB file was linked
        # # check that a mrgddb calculation was linked.
        # for parent in self.meta_data_file.parents:
        #     with CalculationDirectory.from_calculation(
        #             parent, loglevel=self._loglevel) as calc:
        #         if calc.calctype == "abinit_mrgddb":
        #             break
        for file_ in self.input_data_dir:
            if file_.path.endswith("DDB"):
                break
        else:
            raise FileNotFoundError(
                    "Need to link anaddb calculation against a mrgddb "
                    "calculation or to a DDB file!"
                    )

    def _add_prefixes_to_target(self, end):
        return "idat_" + self.jobname + "_" + end
