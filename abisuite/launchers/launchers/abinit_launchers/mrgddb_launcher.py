import os

from .bases import BaseAbinitLauncherNoFilesFile
from ....handlers import AbinitMrgddbInputFile


class AbinitMrgddbLauncher(BaseAbinitLauncherNoFilesFile):
    """Launcher class for an Abinit mrgddb calculation.
    """
    _calctype = "abinit_mrgddb"
    _loggername = "AbinitMrgddbLauncher"
    _input_file_handler_class = AbinitMrgddbInputFile

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        # set title of mgrddb as jobname
        self.input_file.title = self.jobname

    @property
    def input_variables(self):
        return {}

    @input_variables.setter
    def input_variables(self, input_vars):
        if input_vars in ({}, None):
            # nothing to do for real
            return
        raise AttributeError(
                "mgrddb doesn't have any input variables to set.")

    @BaseAbinitLauncherNoFilesFile.workdir.setter
    def workdir(self, workdir):
        # need to set more stuff when setting workdir
        BaseAbinitLauncherNoFilesFile.workdir.fset(self, workdir)
        self.input_file.output_file_path = os.path.join(
                self.output_data_dir.path, "odat_" + self.jobname + "_DDB")

    def _do_add_input_file(self, filename, *args, **kwargs):
        # Important: add input file to input data dir before adding it to the
        # input file. this is because we append a number to the filename which
        # corresponds to the number of DDB to merge.
        super()._do_add_input_file(filename, *args, **kwargs)
        if os.path.isfile(filename):
            # subdir (in kwargs) should be None... is that true?
            if filename.endswith("DDB"):
                # also append file to DDB paths
                path = os.path.join(
                        self.input_data_dir.path,
                        self._get_target_name(filename)
                        )
                self.input_file.ddb_paths.append(path)

    def _add_prefixes_to_target(self, end):
        nddb_already_linked = self.input_file.nddb
        return f"idat_{self.jobname}_{nddb_already_linked + 1}_{end}"
