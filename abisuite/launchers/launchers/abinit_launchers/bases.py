import abc
import os

from ..bases import BaseLauncher
from ....exceptions import DevError


DATA_FILE_ENDINGS = ("DEN", "1WF", "WFQ", "WFK", "EIG", "KSS",
                     "GEO", "DDB", "DOS", "POT")


class BaseAbinitLauncherNoFilesFile(BaseLauncher):
    """Base class for any Abinit launchers that does not require a files file.
    """

    def _get_target_name(self, path):
        # path is the source file to point to. return the link destination
        filename = os.path.basename(path)
        end = filename.split("_")[-1]
        # loop over all possible endings (instead of just if) to take into
        # account _1WFX files where X is a number.
        for ending in DATA_FILE_ENDINGS:
            if ending in end:
                break
        else:
            # executed only if no ending are in the end of the filename.
            raise NotImplementedError(f"file ending {end} not supported.")
        newname = self._add_prefixes_to_target(end)
        return newname

    @abc.abstractmethod
    def _add_prefixes_to_target(self, *args, **kwargs):
        pass


class BaseAbinitLauncher(BaseAbinitLauncherNoFilesFile, abc.ABC):
    """Base class for Abinit launchers that requires a files file.
    """
    _files_file_handler_class = None

    def __init__(self, *args, **kwargs):
        """Base Launcher class init method.

        Created the directories and various files needed for the calculation.

        Parameters
        ----------
        kwargs : other kwargs goes to the PBS file.
        """
        super().__init__(*args, **kwargs)
        if self._files_file_handler_class is None:
            raise DevError("Need to set FilesFile handler class.")
        lvl = self._loglevel
        self.files_file = self._files_file_handler_class(loglevel=lvl)

    @property
    def output_file_path(self):
        return self.files_file.output_file_path

    @output_file_path.setter
    def output_file_path(self, output_file_path):
        self.files_file.output_file_path = output_file_path

    @BaseAbinitLauncherNoFilesFile.workdir.setter
    def workdir(self, workdir):
        # override this method because abinit pbs files need files file
        # call base class setter:
        # https://stackoverflow.com/questions/39133072/override-abstract-setter-of-property-in-python-3
        BaseAbinitLauncherNoFilesFile.workdir.fset(self, workdir)
        self.files_file.path = os.path.join(self.rundir.path,
                                            self.jobname + ".files")
        self.output_file_path = os.path.join(self.workdir,
                                             self.jobname + ".out")
        self.files_file.input_file_path = self.input_file.path
        self.pbs_file.input_file_path = self.files_file.path
        # for meta data file: reset script file path as it is the files file
        self.meta_data_file.script_input_file_path = self.files_file.path

    def write(self, *args, **kwargs):
        """Write the files for the calculation.
        """
        super().write(
                *args,
                bypass_validation=kwargs.pop("bypass_validation", False),
                bypass_pbs_validation=kwargs.pop(
                    "bypass_pbs_validation", False),
                **kwargs)
        self.files_file.write(**kwargs)
