import os

from .bases import BaseAbinitLauncher
from ..scf_launchers import BaseSCFLauncher
from ....approvers import AbinitInputParalApprover
from ....exceptions import DevError
from ....handlers import AbinitFilesFile, AbinitInputFile, CalculationDirectory


__ALL_ABINIT_WFK_KPTS_VARIABLES__ = (
        "ngkpt", "nshiftk", "shiftk", "ecut",  "nband",
        "ixc", "nbdbuf", "kptrlatt", "occ",
        "occopt", "tsmear", "nbdbuf", "ecutsm", "pawecutdg",
        )

__ABINIT_ONE_FROM_THESE_KPTS_VARIABLES__ = (
        ("ngkpt", "kptrlatt"),
        # not mandatory variables
        ("nband", ), ("tsmear", ), ("ecutsm", ), ("pawecutdg", ),
        ("shiftk", ),
        ("nshiftk", ),
        ("nbdbuf", ),
        ("occopt", ), ("occ", ),
        ("ixc", ),
        )
__ALL_ABINIT_GEOMETRY_VARIABLES__ = (
        "acell", "rprim", "ntypat", "znucl", "natom", "typat",
        "xcart", "xred",
        )
__ABINIT_ONE_FROM_THESE_GEOMETRY_VARIABLES__ = (
        ("xred", "xcart"),
        )
# variables to keep when linking density
__ALL_ABINIT_DEN_KPTS_VARIABLES__ = (
        "ecut", "pawecutdg", "occopt", "occ", "ixc", "tsmear",
        )
__ABINIT_ONE_FROM_THESE_DEN_KPTS_VARIABLES__ = (
        ("pawecutdg", ),  # not mandatory
        )


class AbinitLauncher(BaseAbinitLauncher, BaseSCFLauncher):
    """Launcher class for an abinit calculation.
    """
    _calctype = "abinit"
    _files_file_handler_class = AbinitFilesFile
    _input_file_handler_class = AbinitInputFile
    _loggername = "AbinitLauncher"
    _paral_approver_class = AbinitInputParalApprover
    _geometry_variables = __ALL_ABINIT_GEOMETRY_VARIABLES__
    _one_from_these_geometry_variables = (
            __ABINIT_ONE_FROM_THESE_GEOMETRY_VARIABLES__)
    _kpts_variables = __ALL_ABINIT_WFK_KPTS_VARIABLES__
    _one_from_these_kpts_variables = __ABINIT_ONE_FROM_THESE_KPTS_VARIABLES__
    # (
    #         "ngkpt", "nshiftk", "shiftk",
    #         )

    def __init__(self, *args, **kwargs):
        BaseAbinitLauncher.__init__(self, *args, **kwargs)
        BaseSCFLauncher.__init__(self, *args, **kwargs)

    @property
    def input_data_prefix(self):
        return self.files_file.input_data_prefix

    @input_data_prefix.setter
    def input_data_prefix(self, input_data_prefix):
        self.files_file.input_data_prefix = input_data_prefix

    @property
    def output_data_prefix(self):
        return self.files_file.output_data_prefix

    @output_data_prefix.setter
    def output_data_prefix(self, output_data_prefix):
        self.files_file.output_data_prefix = output_data_prefix

    @property
    def pseudos(self):
        return self.files_file.pseudos

    @pseudos.setter
    def pseudos(self, pseudos):
        self.files_file.pseudos = pseudos
        self.input_file.pseudos = pseudos

    @property
    def tmp_data_prefix(self):
        return self.files_file.tmp_data_prefix

    @tmp_data_prefix.setter
    def tmp_data_prefix(self, tmp_data_prefix):
        self.files_file.tmp_data_prefix = tmp_data_prefix

    @BaseAbinitLauncher.workdir.setter
    def workdir(self, workdir):
        BaseAbinitLauncher.workdir.fset(self, workdir)
        self.files_file.output_data_dir = self.output_data_dir.path
        self.output_data_prefix = os.path.join(self.output_data_dir.path,
                                               "odat_" +
                                               self.jobname)
        self.tmp_data_prefix = os.path.join(self.output_data_dir.path,
                                            "tmp_" + self.jobname)
        self.input_data_prefix = os.path.join(self.input_data_dir.path,
                                              "idat_" +
                                              self.jobname)
        self.files_file.input_data_dir = self.input_data_dir.path
        self.files_file.output_data_dir = self.output_data_dir.path
        self.files_file.tmp_data_dir = self.output_data_dir.path

    def load_geometry_from(self, calc, *args, **kwargs):
        super().load_geometry_from(calc, *args, **kwargs)
        # meed to link pseudos as well
        with AbinitFilesFile.from_calculation(
                calc, loglevel=self._loglevel) as files:
            self.pseudos = files.pseudos

    def link_den_from(self, calc):
        """Link DEN file from a previous calculation.

        Parameters
        ----------
        calc : path to calculation.
        """
        # use contains instead of endswith since the file could be in netcdf
        # format and thus endswith .nc
        self._link_output_file_from_calculation(calc, contains="_DEN")
        # manually set irdwfk to 1 if not already
        irdden = self.input_variables.get("irdden", 0)
        if irdden != 1:
            self._logger.info(
                    "Automatically setting 'irdden' to 1 since we link a "
                    "DEN file.")
            self.input_variables["irdden"] = 1

        # if "irdden" not in self.input_variables:
        #     raise ValueError("Need to set 'irdden' to 1.")
        # else:
        #     if self.input_variables["irdden"] != 1:
        #         raise ValueError("Need to set 'irdden' to 1.")
        with CalculationDirectory.from_calculation(calc) as calcdir:
            with calcdir.input_file as input_file:
                for varname in __ALL_ABINIT_DEN_KPTS_VARIABLES__:
                    if varname in input_file.input_variables:
                        # update input variables with this one
                        # if it is not equal or
                        # it is is not there, just set it but warn user
                        value = input_file.input_variables[varname]
                        if varname not in self.input_variables:
                            self._logger.info(
                                    f"Setting '{varname}' to '{value.value}'. "
                                    "Because of input DEN file."
                                    )
                            self.input_variables[varname] = value.value
                        else:
                            if value != self.input_variables[varname]:
                                self._logger.warning(
                                        f"Overwriting input variable "
                                        f"'{varname}' to '{value.value}' "
                                        "because of input DEN file.")
                                self.input_variables[varname] = value.value

    def link_wfk_from(self, calc):
        """Link a WFK file from a previous calculation.

        Parameters
        ----------
        calc : path to calculation.
        """
        self._link_output_file_from_calculation(calc, contains="_WFK")
        # Input file must have same kpt parameters than the one used to compute
        # the WFK.
        # unless it is a nscf calculation then number of bands can change
        # for now I just implemented for iscf = -2 but I think it's safe for
        # any value of iscf. I am lazy to change all the unittests....
        # FIXME: (above)
        iscf = self.input_variables.get("iscf", 7)
        with CalculationDirectory.from_calculation(calc) as calcdir:
            with calcdir.input_file as input_file:
                for varname in __ALL_ABINIT_WFK_KPTS_VARIABLES__:
                    if iscf == -2 and varname in (
                            "nband", "nbdbuf", "ngkpt", "kptrlatt"):
                        # skip this as this can change
                        continue
                    if varname in input_file.input_variables:
                        # update input variables with this one
                        # if it is not equal raise an error
                        # it is is not there, just set it but warn user
                        value = input_file.input_variables[varname]
                        if varname not in self.input_variables:
                            self._logger.info(
                                    f"Setting '{varname}' to '{value.value}'. "
                                    "Because of input WFK file."
                                    )
                            self.input_variables[varname] = value.value
                        else:
                            if value != self.input_variables[varname]:
                                self._logger.warning(
                                        f"Overwriting input variable "
                                        f"'{varname}' to '{value.value}' "
                                        "because of input WFK file.")
                                self.input_variables[varname] = value.value
        # manually set irdwfk to 1 if not already
        irdwfk = self.input_variables.get("irdwfk", 0)
        if irdwfk != 1:
            self._logger.info(
                    "Automatically setting 'irdwfk' to 1 since we link a "
                    "WFK file.")
            self.input_variables["irdwfk"] = 1

        # if "irdwfk" not in self.input_variables:
        #     raise ValueError("Need to set 'irdwfk' to 1.")
        # else:
        #     if self.input_variables["irdwfk"] != 1:
        #         raise ValueError("Need to set 'irdwfk' to 1.")
        #     # else nothing to do

    def link_ddk_from(self, calc):
        """Link 1WF* files from a previous calculation.

        Parameters
        ----------
        calc : path to calculation.
        """
        self._link_output_file_from_calculation(calc, contains="_1WF")
        if "irdddk" not in self.input_variables:
            raise ValueError("Need to set 'irdddk' to 1.")
        else:
            if self.input_variables["irdddk"] != 1:
                raise ValueError("Need to set 'irdddk' to 1.")

    def _link_output_file_from_calculation(
            self, calc, endswith=None, contains=None):
        nfiles_found = 0
        if endswith is None and contains is None:
            raise DevError("Need to specify either 'endswith' or 'contains'.")
        if not CalculationDirectory.is_calculation_directory(calc):
            raise NotADirectoryError(calc)
        with CalculationDirectory.from_calculation(calc) as calcdir:
            with calcdir.output_data_directory as out_dir:
                for filename in out_dir:
                    if endswith is not None:
                        if not filename.path.endswith(endswith):
                            continue
                    if contains is not None:
                        if contains not in filename.path:
                            continue
                    already_copied = filename.path in self.files_to_copy
                    already_linked = filename.path in self.files_to_link
                    if already_copied or already_linked:
                        continue
                    self.add_input_file(filename.path)
                    nfiles_found += 1
        # Put this in validation because we can link more than 1 calc
        # if not nfiles_found:
        #     if endswith is None:
        #         raise FileNotFoundError(
        #                 f"Didn't find any input files that contains "
        #                 f"'{contains}' in '{calc}'.")
        #     elif contains is None:
        #         raise FileNotFoundError(
        #                 f"Didn't find any input files that ends with "
        #                 f"'{endswith}' in '{calc}'.")
        #     else:
        #         raise FileNotFoundError(
        #                 f"Didn't find any input files that ends with "
        #                 f"'{endswith}' and contains '{contains}' "
        #                 f"in '{calc}'.")
        # add calculation to parents
        self.meta_data_file.add_parent(calc)

    def validate(self, *args, **kwargs):
        super().validate(*args, **kwargs)
        # check that all input files have been added
        self._cross_check_input_files_and_ird_variables()
        if self.input_variables.get("irdwfk", 0).value != 0:
            self._check_input_file_added(contains="_WFK")

    def _check_input_file_added(self, endswith=None, contains=None):
        if endswith is None and contains is None:
            raise DevError("endswith and contains cannot be None at same time")
        if endswith is not None and contains is not None:
            raise DevError(
                    "endswith and contains cannot be not None at same time.")
        # check that an input file has been linked
        for fil in self.input_data_dir:
            if endswith is not None:
                if fil.path.endswith(endswith):
                    return
            if contains is not None:
                if contains in fil.path:
                    return
        # nothing found raise an error
        raise FileNotFoundError(f"'*{endswith or contains}*'")

    def _cross_check_input_files_and_ird_variables(self):
        irdwfk = self.input_variables.get("irdwfk", 0).value
        if irdwfk != 0:
            # check that a "WFK" file has been linked
            try:
                self._check_input_file_added(contains="_WFK")
            except FileNotFoundError:
                raise FileNotFoundError(
                        "irdwfk is not 0 but no WFK file linked.")
        irdddk = self.input_variables.get("irdddk", 0).value
        if irdddk != 0:
            # check that _1WF* files have been linked
            try:
                self._check_input_file_added(contains="_1WF")
            except FileNotFoundError:
                raise FileNotFoundError(
                        "irdddk is not 0 but not 1WF files linked.")

    def _add_prefixes_to_target(self, end):
        return self.input_data_prefix + "_" + end
