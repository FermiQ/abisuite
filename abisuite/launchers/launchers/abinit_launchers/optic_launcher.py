import os

from .bases import BaseAbinitLauncher
from ....handlers import AbinitOpticFilesFile, AbinitOpticInputFile
from ....routines import is_list_like


__LINK_VARS__ = ("ddkfile_1", "ddkfile_2", "ddkfile_3", "wfkfile")


class AbinitOpticLauncher(BaseAbinitLauncher):
    """Launcher class for an optic calculation.

        workdir/
            input_data/ -> automatically links all the input files from
                           optic input variables.
            run/  -> files file, pbs file goes there.
                output_data/ -> all output from optic will go there.
    """
    _calctype = "abinit_optic"
    _loggername = "AbinitOpticLauncher"
    _input_file_handler_class = AbinitOpticInputFile
    _files_file_handler_class = AbinitOpticFilesFile

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self._ddk_files_to_link = {}

    @property
    def output_data_prefix(self):
        return self.files_file.output_data_prefix

    @output_data_prefix.setter
    def output_data_prefix(self, output_data_prefix):
        self.files_file.output_data_prefix = output_data_prefix

    @BaseAbinitLauncher.workdir.setter
    def workdir(self, workdir):
        BaseAbinitLauncher.workdir.fset(self, workdir)
        self.files_file.output_data_dir = self.output_data_dir.path
        self.output_data_prefix = os.path.join(self.output_data_dir.path,
                                               "odat_" +
                                               self.jobname)
        self.files_file.output_data_dir = self.output_data_dir.path

    # def write(self, *args, **kwargs):
    #     # link input files to input_data
    #     self.add_input_file([self.input_variables[x].value
    #                          for x in __LINK_VARS__])
    #     # reset optic variables with symlink target before creating inputfile
    #     for x in __LINK_VARS__:
    #         iv = self.input_variables
    #         iv[x] = os.path.join(self.input_data_dir.path,
    #                              self._get_target_name(iv[x].value))
    #     super().write(*args, **kwargs)

    def add_input_file(self, filepath, ddk_idx=None, *args, **kwargs):
        """Adds an input file to the Optic calculation.

        This method is an override of the base one in order to also modify
        the input variables on the fly.

        Parameters
        ----------
        filepath: str
            The path to the file to link.
        ddk_idx: int, optional
            In case of the 1WF files to link (ddk files). This would be the
            index. It is mandatory for the ddk files.
        """
        if is_list_like(filepath):
            for path in filepath:
                self.add_input_file(path, ddk_idx=ddk_idx, *args, **kwargs)
            return
        if "_1WF" in filepath or "_WFK" in filepath:
            if "_1WF" in filepath:
                if ddk_idx is None:
                    raise ValueError(
                        "Need to specify 'ddk_idx' when linking ddk file.")
                if ddk_idx not in (1, 2, 3, "1", "2", "3"):
                    raise ValueError(
                            f"Invalid 'ddk_idx' ('{ddk_idx}').")
                varname = f"ddkfile_{ddk_idx}"
            else:
                # the wfk file to link
                varname = "wfkfile"
            self.input_variables[varname] = os.path.join(
                    self.input_data_dir.path,
                    self._get_target_name(filepath))
        super().add_input_file(filepath, *args, **kwargs)

    def _add_prefixes_to_target(self, end):
        return "idat_" + self.jobname + "_" + end
