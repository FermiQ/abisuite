from .bases import BaseQELauncher_for_outdir_prefix, BasePostPWLauncher
from ....handlers import QEEpsilonInputFile


class QEEpsilonLauncher(BaseQELauncher_for_outdir_prefix, BasePostPWLauncher):
    """Launcher class for a Quantum Espresso Calculation using epsilon.x script.
    """
    _loggername = "QEEpsilonLauncher"
    _input_file_handler_class = QEEpsilonInputFile
    _calctype = "qe_epsilon"
