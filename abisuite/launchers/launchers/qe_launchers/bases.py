import abc
import os

from ..bases import BaseLauncher
from ....handlers import (
        GenericDirectory, QEPHDyn0File, SymLinkFile
        )
from ....routines import full_abspath


# TODO: All these inputs preprocessing could be turned into specific properties
class BaseQELauncher(BaseLauncher, abc.ABC):
    """Base class for a Quantum Espresso Launcher.
    """
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        # change the input file redirection in pbs files to a more stable one
        self.pbs_file.command_line = (
                "$MPIRUN $EXECUTABLE -input $INPUT > $LOG 2> $STDERR"
                )

    def _do_add_input_file(self, path, copy_file_with_suffix, *args,
                           keep_original_filename=False, **kwargs):
        # copy '.xml' files since they are overwritten during calculation
        copy_file_with_suffix.append(".xml")
        # if file endswith ".xml", and it's in a 'save' directory, don't change
        # its name!!!
        if os.path.dirname(path).endswith("save"):
            keep_original_filename = True
        super()._do_add_input_file(
                        path,
                        copy_file_with_suffix, *args,
                        keep_original_filename=keep_original_filename,
                        **kwargs)

    def write(self, *args, **kwargs):
        """Write the files for a calculation.
        """
        super().write(*args, **kwargs)
        # QE calculations read input data links from the output data dir
        # just symlink everything that is input data dir in the output data dir
        for path in [x.path for x in self.input_data_dir]:
            outdirpath = os.path.join(self.output_data_dir.path,
                                      os.path.basename(path))
            self.output_data_dir.add_symlink(outdirpath, path)
        self.output_data_dir.write()

    @BaseLauncher._expected_script.getter
    def _expected_script(self):
        # need to override this as Quantum espresso scripts ends with ".x"
        return self.calctype.split("_")[-1] + ".x"

    @BaseLauncher.input_variables.setter
    def input_variables(self, value):
        # need to preprocess input variables
        value = self._preprocess_inputs(value)
        BaseLauncher.input_variables.fset(self, value)

    @abc.abstractmethod
    def _preprocess_inputs(self, inputs):
        return inputs

    def _get_target_name(self, path):
        filename = os.path.basename(path)
        end = filename.split(".")[-1]
        return self.jobname + "." + end

    def _check_for_input_file(self, suffix, check_in=None):
        if check_in is None:
            check_in = self.input_data_dir
        # check that a certain file has been asked to be linked.
        # don't use walk() here as we don't want to force read the input data
        # directory as files may not have been written yet.
        for item in check_in:
            # check for subdirectories too
            fil = item.path
            if isinstance(item, SymLinkFile):
                # check the source at the end of the link
                if item.exists and os.path.islink(item.path):
                    with item:
                        fil = item.source
                else:
                    fil = item.source
                if os.path.isdir(fil):
                    # link is a directory, create a base directory from source
                    # and search in it
                    subdir = GenericDirectory(loglevel=item.loglevel)
                    subdir.path = fil
                    try:
                        with subdir:
                            self._check_for_input_file(suffix, check_in=subdir)
                    except FileNotFoundError:
                        # not in this subdir
                        continue
                    else:
                        # found it
                        return
            elif item.isdir:
                try:
                    self._check_for_input_file(suffix, check_in=item)
                except FileNotFoundError:
                    # not in this subdir
                    continue
                else:
                    # if nothing raised, that means file has been found
                    return
            if suffix in fil:
                return
        # # if no files there, check that there is one already in the
        # # output data dir with the same name as the jobfile
        # filename = self.jobname + suffix
        # path = os.path.join(self.output_data_dir.path, filename)
        # if not os.path.exists(path):
        #     raise FileNotFoundError(f"Need '{suffix}' for {self._calctype}"
        #                             " calculation.")
        raise FileNotFoundError(
            f"Need '{suffix}' in '{check_in.path}' for "
            f"'{self._calctype}' calculation.")

    def _overwrite_input_var(self, inputs, varname, replacement):
        if varname in inputs:
            # make string before making log call because of possibility
            # to have sets which are non-hashable.
            value = str(inputs[varname])
            self._logger.debug(f"Overwriting '{varname}'={value}"
                               f" variable to {replacement}")
        inputs[varname] = replacement
        return inputs


class BaseQELauncher_for_outdir_prefix(BaseQELauncher):
    """Just an intermediate base class for the pw.x and ph.x scripts.
    This class is implemented only to have a DRY code because of a common
    input variable preprocessing.
    """

    def _preprocess_inputs(self, inputs):
        outdir = self.output_data_dir.path
        # FIXME: find a better way to do this (using the variables module)
        # if not outdir.endswith("/"):
        #     outdir += "/"  # can cause errors with EPW
        inputs = self._overwrite_input_var(inputs, "outdir",
                                           outdir)
        inputs = self._overwrite_input_var(inputs, "prefix", self.jobname)
        return inputs


class BaseQELauncher_for_fildyn(BaseQELauncher):
    """Just an intermediate base class for the dynmat.x and ph.x scripts.
    This class is implemented only to have a DRY code because of a common
    input variable preprocessing.
    """

    def _preprocess_inputs(self, inputs):
        fildyn = os.path.join(self.output_data_dir.path, self.jobname + ".dyn")
        inputs = self._overwrite_input_var(inputs, "fildyn", fildyn)
        return inputs


class BasePostPWLauncher(BaseQELauncher):
    """Just an intermediate base class that checks that a ".save"
    file has been added as input. This is common to post processing scripts.
    """

    def validate(self, *args, bypass_validation=False, **kwargs):
        super().validate(*args, bypass_validation=bypass_validation, **kwargs)
        # check for a ".save" file has been linked to the calculation
        if bypass_validation:
            return
        self._check_for_input_file(".save")
        self._check_for_input_file("charge-density.dat")


class BasePostPHLauncher(BaseQELauncher):
    """Just an intermediate base class that checks that a ".save"
    file has been added as input. This is common to post processing scripts.
    """

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self._dyn0file = None

    @property
    def dyn0file(self):
        if self._dyn0file is not None:
            return self._dyn0file
        # check for .dyn* files. There should be nqpt+1 file to link.
        # check first for the .dyn0 file
        self._check_for_input_file(".dyn0")
        # if its there, open it and read number of qpts then assert all
        # files are there
        # self.input_data_dir.read()
        for item in self.input_data_dir:
            path = item.path
            if path.endswith(".dyn0"):
                break
        else:
            raise FileNotFoundError("*.dyn0")
        if isinstance(item, SymLinkFile):
            if item.exists and os.path.islink(item.path):
                with item:
                    path = item.source
                    # if not absolute path, make it absolute
                    if not os.path.isabs(path):
                        path = full_abspath(
                                os.path.join(os.path.dirname(item.path), path))
            else:
                path = item.source
        with QEPHDyn0File.from_file(path, loglevel=self._loglevel) as dyn0:
            self._dyn0file = dyn0
        return self.dyn0file

    def validate(self, *args, **kwargs):
        super().validate(*args, **kwargs)
        self._check_for_dynfiles()

    def _check_for_dynfiles(self):
        for i in range(1, self.dyn0file.nqpt + 1):
            self._check_for_input_file(f".dyn{i}")

    def _do_add_input_file(self, path, *args, **kwargs):
        # add an exception in case the ph run was splitted in many calculations
        # (one for each qpt for exemple) in order to accept multiple dyn0 files
        # and multiple patterns.*.xml
        if path.endswith(".xml"):
            basename = os.path.basename(path)
            if basename.startswith(
                    "patterns"
                    ) or basename.startswith(
                            "status") or basename.startswith("control_ph"):
                # check if file is already in inputs
                # don't allow to read content as we walk over files that are
                # not yet written.
                for item in self.input_data_dir.walk(paths_only=True,
                                                     allow_read_content=False):
                    if os.path.basename(item) == basename:
                        # do nothing else since its the same file
                        return
        if not path.endswith(QEPHDyn0File._parser_class._expected_ending):
            super()._do_add_input_file(path, *args, **kwargs)
            return
        # else its a dyn0 file. check if one already exists in the input data
        try:
            self._check_for_input_file(".dyn0")
        except FileNotFoundError:
            # file not already linked so link it
            super()._do_add_input_file(path, *args, **kwargs)
        else:
            # file exists in input, thus check if files are equal
            with QEPHDyn0File.from_file(path, loglevel=self._loglevel) as dyn0:
                for attr in dyn0.structure.get_relevant_attributes():
                    if attr == "path":
                        continue
                    if getattr(dyn0, attr) != getattr(self.dyn0file, attr):
                        raise LookupError(f".dyn0 files don't match:\n"
                                          f"{dyn0}\n!=\n"
                                          f"{self.dyn0file}")
                # if files matches, don't add the new one just skip it.
