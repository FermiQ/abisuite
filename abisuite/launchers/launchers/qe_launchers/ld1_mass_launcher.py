from .ld1_launcher import QELD1Launcher
from ..bases import BaseMassLauncher


class QELD1MassLauncher(BaseMassLauncher):
    """The mass launcher class for Quantum Espresso calculations
    using the ld1.x script.
    """
    _launcher_class = QELD1Launcher
    _loggername = "QELD1MassLauncher"
