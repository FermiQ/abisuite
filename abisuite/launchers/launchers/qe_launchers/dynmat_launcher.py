from .bases import BaseQELauncher_for_fildyn
from ....handlers import QEDynmatInputFile


class QEDynmatLauncher(BaseQELauncher_for_fildyn):
    """Launcher class for a Quantum Espresso calculation using the
    dynmat.x script.
    """
    _loggername = "QEDynmatLauncher"
    _input_file_handler_class = QEDynmatInputFile
    _calctype = "qe_dynmat"

    def validate(self, *args, **kwargs):
        super().validate(*args, **kwargs)
        # check that the '.dyn' file is there or has been put in the files
        # to link
        self._check_for_input_file(".dyn")
