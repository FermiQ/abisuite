from .pw_launcher import QEPWLauncher
from ..bases import BaseMassLauncher


class QEPWMassLauncher(BaseMassLauncher):
    """The mass launcher class for Quantum Espresso calculations
    using the pw.x script.
    """
    _launcher_class = QEPWLauncher
    _loggername = "QEPWMassLauncher"

    def load_geometry_from(self, *args, **kwargs):
        """Load the same geometry to all the sub launchers.
        """
        for launcher in self.launchers:
            launcher.load_geometry_from(*args, **kwargs)
