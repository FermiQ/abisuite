import os

from .bases import BasePostPHLauncher, BaseQELauncher_for_outdir_prefix
from ....handlers import (
        QEEPWInputFile,
        )
from ....routines import full_abspath


# TODO: put this limit in epw input variables directly
__DVSCF_MAX_LEN__ = 100  # taken from EPW source code
__FILKQF_MAX_LEN__ = 80  # taken from EPW source code as well


class QEEPWLauncher(BasePostPHLauncher, BaseQELauncher_for_outdir_prefix):
    """Launcher class for a Quantum Espresso calculation using
    the epw.x script.
    """
    _loggername = "QEEPWLauncher"
    _input_file_handler_class = QEEPWInputFile
    _calctype = "qe_epw"

    def write(self, *args, **kwargs):
        # make sure filkf and filqf variables are not too long
        for name in ("filqf", "filkf"):
            if name not in self.input_variables:
                continue
            oldvar = self.input_variables.get(name).value
            if len(oldvar) > __FILKQF_MAX_LEN__:
                # store as relative value
                newvar = os.path.relpath(oldvar, self.rundir.path)
                self.input_variables[name] = newvar
        super().write(*args, **kwargs)

    def validate(self, *args, **kwargs):
        super().validate(*args, **kwargs)
        # TODO: implement the check for all the other dvscf1 files
        # only need this if we don't read epb files
        if self.input_variables.get("epbread", False) is False:
            self._check_for_input_file(".dvscf1")
        # check qpts fine mesh if necessary
        # for varname in ("filqf", "filkf"):
        #     if varname in self.input_variables:
        #         filqf = self.input_variables[varname].value
        #         # get the link path
        #         filqf = self.input_data_dir[os.path.basename(filqf)].source
        #         self._check_filqf_filkf(filqf)

    # def _check_filqf_filkf(self, filqf):
    #     if not os.path.isfile(filqf):
    #         raise FileNotFoundError(filqf)
    #     # TODO: check content of this file
    #     # TODO: put this in Approver

    def _check_for_dynfiles(self):
        # don't check if we do a restart run after interpolation as we
        # don't need it.
        epbread = self.input_variables.get("epbread", False).value is True
        epwread = self.input_variables.get("epwread", False).value is True
        if any([epbread, epwread]):
            return
        super()._check_for_dynfiles()

    def _do_add_input_file(self, *args, **kwargs):
        # use the one from POSTPH CAlC
        BasePostPHLauncher._do_add_input_file(self, *args, **kwargs)

    def _get_target_name(self, path):
        target = os.path.basename(path)
        if target.startswith("filqf.txt") and not target == "filqf.txt":
            # in case tempfile looks like this 'filqf.txt032832239'
            return "filqf.txt"
        if target.startswith("filkf.txt") and not target == "filkf.txt":
            # in case tempfile looks like this 'filkf.txt032832239'
            return "filkf.txt"
        # need to override this since some file extensions need to be renamed
        target = super()._get_target_name(path)
        if ".dyn" in target and not target.endswith(".dyn0"):
            # change for .dyn_q except for the .dyn0 file (not needed)
            return target.replace(".dyn", ".dyn_q")
        if ".dvscf" in target:
            # change for .dvscf_q
            # need to know the qpt number.
            # if first base dir is _ph0, label = 1
            if os.path.basename(os.path.dirname(path)) == "_ph0":
                label = "1"
            # else, the number is in the dir's name
            elif ".q_" in os.path.basename(os.path.dirname(path)):
                label = os.path.dirname(path).split("_")[-1]
            else:
                raise LookupError(f"Could not find label for {path}")
            return target.replace(".dvscf1", ".dvscf_q" + label)
        if target.endswith(".fc"):
            # force constant file must have a very specific name
            return "ifc.q2r"
        return target

    def _get_target_path(self, targetname):
        # overwride this as some files need to be linked directly in run
        # directory...
        inrundir = os.path.join(
                self.calculation_directory.run_directory.path, targetname)
        if targetname in (
                "epwdata.fmt", "crystal.fmt", "dmedata.fmt", "vmedata.fmt", ):
            return inrundir
        ends_in_run = (".kgmap", ".kmap", ".ukk", ".mmn", )
        for end in ends_in_run:
            if targetname.endswith(end):
                return inrundir
        return super()._get_target_path(targetname)

    def _preprocess_inputs(self, inputs):
        inputs = BaseQELauncher_for_outdir_prefix._preprocess_inputs(
                self, inputs)
        inputs = self._overwrite_input_var(inputs, "title", self.jobname)
        dvscf = self.input_data_dir.path
        if not dvscf.endswith("/"):
            dvscf += "/"
        # there is a string limit to dvscf_dir of 100 chars.
        # see QEROOT/EPW/src/epwcom.f90 line 302 (12/04/2019)
        # if it's greater, we need to raise error
        if len(dvscf) > __DVSCF_MAX_LEN__:
            # try making it relative
            dvscf = os.path.relpath(dvscf, self.rundir.path)
            if len(dvscf) > __DVSCF_MAX_LEN__:
                self._logger.error("dvscf_dir is too long for EPW.")
                raise ValueError(
                        "Cannot make a 'dvscf_dir' input variable less than "
                        "it's maximum length value.")
            self._logger.debug("dvscf_dir too long. Using it's relative value "
                               f"instead: '{dvscf}'.")
        inputs = self._overwrite_input_var(inputs, "dvscf_dir",
                                           dvscf)
        # get qgrid and set it in the inputs
        # only check if we need to
        epbread = inputs.get("epbread", False) is False
        epwread = inputs.get("epwread", False) is False
        if all([epbread, epwread]):
            try:
                self._check_for_input_file(".dyn0")
            except FileNotFoundError:
                # dyn0 not set yet do nothing
                pass
            else:
                for i, nq in enumerate(self.dyn0file.qgrid):
                    inputs = self._overwrite_input_var(inputs, f"nq{i+1}", nq)
                qpts = {"parameter": "cartesian",
                        "q_points": [[
                            round(x, 10) for x in qpt]
                            for qpt in self.dyn0file.qpts],
                        "weights": [1] * len(self.dyn0file.qpts)}
                inputs = self._overwrite_input_var(inputs, "q_points", qpts)
        # link filqf if necessary
        # TODO: put this check in approver
        if "filqf" in inputs:
            # get full path
            filqf = inputs["filqf"]
            if not os.path.isabs(filqf):
                # file is elsewhere on disk with a relative path
                # filqf = full_abspath(os.path.join(self.rundir.path, filqf))
                filqf = full_abspath(filqf)
            basefil = os.path.basename(filqf)
            if not os.path.isfile(
                    filqf) and basefil not in self.input_data_dir:
                raise FileNotFoundError(filqf)
            # if not linked do it
            if basefil not in self.input_data_dir:
                self.add_input_file(filqf, keep_original_filename=True)
            newfilqf = os.path.join(self.input_data_dir.path,
                                    os.path.basename(filqf))
            self._overwrite_input_var(inputs, "filqf", newfilqf)
        return inputs
