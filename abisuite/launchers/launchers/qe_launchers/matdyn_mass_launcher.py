from .matdyn_launcher import QEMatdynLauncher
from ..bases import BaseMassLauncher


class QEMatdynMassLauncher(BaseMassLauncher):
    """The mass launcher class for Quantum Espresso calculations
    using the matdyn.x script.
    """
    _launcher_class = QEMatdynLauncher
    _loggername = "QEMatdynMassLauncher"
