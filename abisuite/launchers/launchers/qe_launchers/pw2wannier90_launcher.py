import os

from .bases import BaseQELauncher_for_outdir_prefix, BasePostPWLauncher
from ....approvers import QEPW2Wannier90InputParalApprover
from ....handlers import QEPW2Wannier90InputFile


class QEPW2Wannier90Launcher(BaseQELauncher_for_outdir_prefix,
                             BasePostPWLauncher):
    """Launcher class for a Quantum Espresso Calculation using
    pw2wannier90.x script.
    """
    _loggername = "QEPW2Wannier90Launcher"
    _input_file_handler_class = QEPW2Wannier90InputFile
    _calctype = "qe_pw2wannier90"
    _paral_approver_class = QEPW2Wannier90InputParalApprover

    def _get_target_path(self, targetname):
        # if linking a nnkp file, add it to next to the batch script
        if targetname.endswith(".nnkp"):
            return os.path.join(
                    self.calculation_directory.run_directory.path,
                    targetname)
        return super()._get_target_path(targetname)

    def _preprocess_inputs(self, inputs):
        inputs = super()._preprocess_inputs(inputs)
        # overwrite the seedname to be consistent with the jobname
        inputs = self._overwrite_input_var(inputs, "seedname", self.jobname)
        return inputs

    def validate(self, *args, **kwargs):
        super().validate(*args, **kwargs)
        # check for a ".save" file has been linked to the calculation
        self._check_for_input_file(".save")
        self._check_for_input_file(".nnkp", check_in=self.rundir)
