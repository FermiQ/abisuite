from .bases import (
                BaseQELauncher_for_outdir_prefix,
                BasePostPWLauncher,
                )
from ....handlers import QEFSInputFile


# Watchout deadly diamond of death with the class below!!
class QEFSLauncher(BaseQELauncher_for_outdir_prefix,
                   BasePostPWLauncher):
    """Launcher class for a Quantum Espresso Calculation using the fs.x script.
    """
    _loggername = "QEFSLauncher"
    _input_file_handler_class = QEFSInputFile
    _calctype = "qe_fs"
