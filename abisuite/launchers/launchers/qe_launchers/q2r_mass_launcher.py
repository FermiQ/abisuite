from .q2r_launcher import QEQ2RLauncher
from ..bases import BaseMassLauncher


class QEQ2RMassLauncher(BaseMassLauncher):
    """The mass launcher class for Quantum Espresso calculations
    using the q2r.x script.
    """
    _launcher_class = QEQ2RLauncher
    _loggername = "QEQ2RMassLauncher"
