import os

from ..bases import BaseLauncher
from ....approvers import Wannier90ParentApprover
from ....handlers import (
            CalculationDirectory, Wannier90InputFile,
            )


# NOTE:
# SPECIAL CLASS that don't support run directory cause we can't 'pipe' an
# input file to the wannier90.x script. Thus, everything is located inside the
# main directory.
class Wannier90Launcher(BaseLauncher):
    """Launcher class for the wannier90.x script from Wannier90.
    """
    _calctype = "wannier90"
    _loggername = "Wannier90Launcher"
    _input_file_handler_class = Wannier90InputFile
    _parent_approver_class = Wannier90ParentApprover

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        pf = self.pbs_file
        # don't need to write STDERR and STDOUT as wannier90 don't use them
        # instead it generates it's own output file and error files
        # pf.command_line = f"$EXECUTABLE {self.jobname}"
        pf.command_line = (f"$MPIRUN $EXECUTABLE ../{self.jobname + '.win'} "
                           f"> $LOG 2> $STDERR")

    @property
    def input_data_dir(self):
        # override this as the concept of an input data directory does not work
        # with wannier90
        return self.calculation_directory

    def restart_from(self, calc):
        """Restart a calculation using exactly the same input as the calc.
        """
        if not CalculationDirectory.is_calculation_directory(calc):
            raise ValueError(f"Not a calc dir: {calc}")
        calc = CalculationDirectory.from_calculation(
                calc, loglevel=self._loglevel)
        if calc.meta_data_file.calctype != "wannier90":
            raise ValueError("Impossible to restart from a calculation which "
                             "is not a 'wannier90' calculation.")
        self.input_variables = calc.input_file.input_variables
        self.add_input_file(os.path.join(calc.path, "*.amn"), copy=True)
        self.add_input_file(os.path.join(calc.path, "*.chk"), copy=True)
        self.add_input_file(os.path.join(calc.path, "*.eig"), copy=True)
        self.add_input_file(os.path.join(calc.path, "*.mmn"), copy=True)
        self.add_input_file(os.path.join(calc.path, "*.nnkp"), copy=True)

    @BaseLauncher._expected_script.getter
    def _expected_script(self):
        # need to override this as Quantum espresso scripts ends with ".x"
        return self.calctype.split("_")[-1] + ".x"

    def _get_target_name(self, target):
        # depending of filetype, either change the target name or not
        basename = os.path.basename(target)
        if basename.startswith("UNK"):
            # return same name
            return basename
        # remove extension and change the filename with the jobname
        split = basename.split(".")
        ext = split[-1]
        return self.jobname + "." + ext

    def _get_target_path(self, targetname):
        # instead of returning a path in input data dir return one in main dir
        # it's the only way to link a file with 'wannier90.x' ...
        # RIP workdir readability...
        return os.path.join(self.workdir, targetname)
