from ..bases import BaseInputApprover
from ...handlers.file_structures import QEQ2RInputStructure
from ...variables import ALL_QEQ2R_VARIABLES


class QEQ2RInputApprover(BaseInputApprover):
    """Class that checks the input variables for a q2r.x calculation.
    """
    _loggername = "QEQ2RInputApprover"
    _variables_db = ALL_QEQ2R_VARIABLES
    _structure_class = QEQ2RInputStructure
