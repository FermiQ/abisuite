from ..bases import BaseInputApprover
from ...handlers.file_structures import QEDynmatInputStructure
from ...variables import ALL_QEDYNMAT_VARIABLES


class QEDynmatInputApprover(BaseInputApprover):
    """Class that checks the input variables for a dynmat.x calculation.
    """
    _loggername = "QEDynmatInputApprover"
    _variables_db = ALL_QEDYNMAT_VARIABLES
    _structure_class = QEDynmatInputStructure
