from ..bases import BaseInputApprover
from ...handlers.file_structures import QEFSInputStructure
from ...variables import ALL_QEFS_VARIABLES


class QEFSInputApprover(BaseInputApprover):
    """Class that checks the input variables for a fs.x calculation.
    """
    _loggername = "QEFSInputApprover"
    _variables_db = ALL_QEFS_VARIABLES
    _structure_class = QEFSInputStructure
