from ..bases import BaseInputApprover, BaseInputParalApprover
from ...handlers.file_structures import QEPW2Wannier90InputStructure
from ...variables import ALL_QEPW2WANNIER90_VARIABLES


class QEPW2Wannier90InputApprover(BaseInputApprover):
    """Class that checkes the input variables for a pw2wannier90.x calculation.
    """
    _loggername = "QEPW2Wannier90InputApprover"
    _variables_db = ALL_QEPW2WANNIER90_VARIABLES
    _structure_class = QEPW2Wannier90InputStructure


class QEPW2Wannier90InputParalApprover(BaseInputParalApprover):
    """Class that checks the paralellization scheme of a pw2wannier90.x
    script from Quantum Espresso.
    """
    _loggername = "QEPW2Wannier90InputParalApprover"
    _input_approver_class = QEPW2Wannier90InputApprover
    _structure_class = QEPW2Wannier90InputStructure

    def validate(self, *args, **kwargs):
        super().validate(*args, **kwargs)
        self._check_npools()

    def _check_npools(self):
        if "-npool" in self.command_arguments:
            self.errors.append("pooling not supported for pw2wannier90.x")
