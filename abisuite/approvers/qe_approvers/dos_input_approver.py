from .bases import BaseQEInputParalApprover
from ..bases import BaseInputApprover
from ...handlers.file_structures import QEDOSInputStructure
from ...variables import ALL_QEDOS_VARIABLES


class QEDOSInputApprover(BaseInputApprover):
    """Class that checkes the input variables for a dos.x calculation.
    """
    _loggername = "QEDOSInputApprover"
    _variables_db = ALL_QEDOS_VARIABLES
    _structure_class = QEDOSInputStructure


class QEDOSInputParalApprover(BaseQEInputParalApprover):
    """Class that just raise an error if one tries to run dos.x script in
    parallel (not implemented).
    """
    _loggername = "QEDOSInputParalApprover"
    _input_approver_class = QEDOSInputApprover
    _structure_class = QEDOSInputStructure

    def validate(self, *args, **kwargs):
        if self.mpi_command:
            self.errors.append(
                    "mpi implementation not supported for dos.x")
        if self.command_arguments:
            self.errors.append(
                    "no parallelism schemes implemented for dos.x")
        self._has_been_validated = True
