from ..bases import BaseInputApprover
from ...handlers.file_structures import QELD1InputStructure
from ...variables import ALL_QELD1_VARIABLES


class QELD1InputApprover(BaseInputApprover):
    """Class that checks the input variables for a ld1.x calculation.
    """
    _loggername = "QELD1InputApprover"
    _variables_db = ALL_QELD1_VARIABLES
    _structure_class = QELD1InputStructure
