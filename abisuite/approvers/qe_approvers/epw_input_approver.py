from .bases import BaseQEInputParalApprover
from ..bases import BaseInputApprover
from ...handlers.file_structures import QEEPWInputStructure
from ...routines import is_list_like
from ...variables import ALL_QEEPW_VARIABLES


class QEEPWInputApprover(BaseInputApprover):
    """Class that checks the input variables for a epw.x calculation.
    """
    _loggername = "QEEPWInputApprover"
    _variables_db = ALL_QEEPW_VARIABLES
    _structure_class = QEEPWInputStructure

    def validate(self):
        self._check_bands()
        self._check_elph_interpolation()
        self._check_qpts()
        self._check_scattering()
        self._check_a2f()
        self._check_self_energies()
        self._check_fine_grids()
        self._check_phonon_spectral_function()
        self._check_ibte_input_variables()
        self._check_degaussw_vs_fsthick()
        super().validate()

    def _check_bands(self):
        if "bands_skipped" in self.input_variables:
            bands_skipped = self.input_variables["bands_skipped"].value
            # bands_skipped should be written something like:
            # "exclude_bands = 1:5" or "exclude_bands = 1-2,23-40"
            if not bands_skipped.strip(" ").startswith("exclude_bands"):
                self.errors.append(
                        "'bands_skipped' should be set like 'exclude_bands = "
                        f"1:5' but it is '{bands_skipped}'.")

    def _check_degaussw_vs_fsthick(self):
        degaussw = self.input_variables.get("degaussw", 0.0).value
        fsthick = self.input_variables.get("fsthick", 0.0).value
        if 4 * degaussw > fsthick:
            self.errors.append(
                    f"'degaussw' ({degaussw}) must be less than "
                    f"0.25 * 'fsthick' ({fsthick})")

    def _check_elph_interpolation(self):
        if self.input_variables.get("epbwrite", False).value:
            if not self.input_variables.get("elph", False).value:
                self.errors.append(
                        "epbwrite is set to True but elph is set to False.")

    def _check_ibte_input_variables(self):
        # check ibte input variables validity
        ibte = self.input_variables.get("iterative_bte", False).value
        if not ibte:
            return
        for i in range(1, 4):
            # for IBTE, each nkfi must be equal to nqfi
            nkfi = self.input_variables.get(f"nkf{i}").value
            nqfi = self.input_variables.get(f"nqf{i}").value
            if nqfi != nkfi:
                self.errors.append(
                        f"For IBTE, we must have nkf = nqf but we got "
                        f"'nkf{i}'={nkfi}!={nqfi}='nqf{i}'"
                        )

    def _check_a2f(self):
        # if a2f is True, phonselfen should be true as well
        a2f = self.input_variables.get("a2f", False).value
        if a2f is False:
            return
        phonselfen = self.input_variables.get("phonselfen", False).value
        if phonselfen is False:
            self.errors.append(
                    "If 'a2f' is set to True, 'phonselfen' should be set to "
                    "True as well.")
        # adaptative smearing is not implemented with phon selfen
        degaussw = self.input_variables.get("degaussw", 0.0)
        if degaussw == 0.0:
            self.errors.append(
                    "Adaptative smearing (degaussw = 0.0) is not implemented "
                    "when computing phonon self energy.")

    def _check_fine_grids(self):
        # if fine grids are defined and necessary, one needs to check they
        # are commensurate.
        # it is needed for iterative BTE at least.
        ibte = self.input_variables.get("iterative_bte", False).value
        if ibte is False:
            return
        allfines = []
        for var in ("nkf", "nqf"):
            for index in (1, 2, 3):
                name = f"{var}{index}"
                if name not in self.input_variables:
                    self.errors.append(
                            f"Need to define '{name}' if doing ibte."
                            )
                    return
                allfines.append(
                        self.input_variables.get(name).value)
        nkf1, nkf2, nkf3 = allfines[0], allfines[1], allfines[2]
        nqf1, nqf2, nqf3 = allfines[3], allfines[4], allfines[5]
        if nkf1 % nqf1:
            self.errors.append(
                    f"nqf1={nqf1} must be commensurate with nkf1={nkf1}.")
        if nkf2 % nqf2:
            self.errors.append(
                    f"nqf2={nqf2} must be commensurate with nkf2={nkf2}.")
        if nkf3 % nqf3:
            self.errors.append(
                    f"nqf3={nqf3} must be commensurate with nkf3={nkf3}.")

    def _check_phonon_spectral_function(self):
        specfun_ph = self.input_variables.get("specfun_ph", False).value
        if not specfun_ph:
            return
        for var in ("wmin_specfun", "wmax_specfun", "nw_specfun"):
            if var not in self.input_variables:
                self.errors.append(
                        f"Need to define '{var}' for spectral function "
                        "calculations.")
        wmin = self.input_variables.get("wmin_specfun").value
        wmax = self.input_variables.get("wmax_specfun").value
        if wmin >= wmax:
            self.errors.append(
                    f"wmin_specfun ({wmin}) should be < wmax_specfun ({wmax})")

    def _check_qpts(self):
        if "q_points" not in self.input_variables:
            self.errors.append("Need to set q_points.")
            return
        qpts = self.input_variables["q_points"]
        if qpts["parameter"] != "cartesian":
            self.errors.append("Only parameter possible for qpts is "
                               "'cartesian'")
        if len(qpts["q_points"]) == 0:
            self.errors.append("Need at least one qpt!")
        if self.input_variables.get("rand_q", False).value is True:
            return
        # check fine qpt grid
        nqs = ("nqf1", "nqf2", "nqf3")
        how_many_nq = 0
        for nq in nqs:
            if nq in self.input_variables:
                how_many_nq += 1
        if how_many_nq != 3 and how_many_nq != 0:
            self.errors.append("Missing one or more nqf*")
            return
        # if no nqf, filqf must be defined if we need fine grids for something
        do_a2f = self.input_variables.get("a2f", False)
        if do_a2f:
            if how_many_nq == 0 and "filqf" not in self.input_variables:
                self.errors.append(
                        "No 'nqf*' in inputs and 'filqf' not defined.")

    def _check_scattering(self):
        if self.input_variables.get("scattering", False).value is False:
            return
        # scattering run => check temperatures are defined
        for varname in ("nstemp", "temps"):
            if varname not in self.input_variables:
                self.errors.append(
                        f"Scattering run: need to define '{varname}'.")
        nstemp = self.input_variables["nstemp"]
        temps = self.input_variables["temps"]
        if nstemp == 1 and not is_list_like(temps):
            # only a single value given to temps, translate as array
            self.input_variables["temps"] = [temps]
        else:
            if nstemp != len(temps):
                self.errors.append(
                        f"'nstemp'={nstemp} should equal the len of 'temps'.")

    def _check_self_energies(self):
        # selfenergies (ph or elecs) + spectral functions calculations must be
        # executed on uniform meshes and on the full (no symmetries)
        phonselfen = self.input_variables.get("phonselfen", False).value
        elecselfen = self.input_variables.get("elecselfen", False).value
        specfun_el = self.input_variables.get("specfun_el", False).value
        specfun_ph = self.input_variables.get("specfun_ph", False).value
        if not any([phonselfen, elecselfen, specfun_el, specfun_ph]):
            # nothing to check
            return
        # get meshes
        mp_mesh_k = self.input_variables.get("mp_mesh_k", False).value
        mp_mesh_q = self.input_variables.get("mp_mesh_q", False).value
        if any([mp_mesh_k, mp_mesh_q]):
            self.errors.append(
                    "Cannot use 'phonselfen', 'elecselfen', 'specfun_el', "
                    "'specfun_ph' in conjunction with irr. meshes (with "
                    "'mp_mesh_k or mp_mesh_q').")
        # get nqs
        if not specfun_el and not specfun_ph:
            return
        for i in (1, 2, 3):
            for varname in (f"nkf{i}", ):
                # not mandatory to specify nqf* (we can give path e.g.)
                if varname not in self.input_variables:
                    self.errors.append(
                        "When using 'phonselfen', 'elecselfen', 'specfun_el', "
                        f"'specfun_ph' we need to specify {varname}'.")

        # nk1 = self.input_variables.get("nkf1").value
        # nk2 = self.input_variables.get("nkf2").value
        # nk3 = self.input_variables.get("nkf3").value
        # if nk2 != nk1 or nk3 != nk1 or nk2 != nk3:
        #     self.errors.append(
        #         "When using 'phonselfen', 'elecselfen', 'specfun_el',"
        #         f"'specfun_ph' we need to use homogenous nkf grid.")
        # nq1 = self.input_variables.get("nqf1", 1).value
        # nq2 = self.input_variables.get("nqf2", 1).value
        # nq3 = self.input_variables.get("nqf3", 1).value
        # if nq2 != nq1 or nq3 != nq1 or nq2 != nq3:
        #     self.errors.append(
        #         "When using 'phonselfen', 'elecselfen', 'specfun_el', "
        #         f"'specfun_ph' we need to use homogenous nqf grid.")


class QEEPWInputParalApprover(BaseQEInputParalApprover):
    """Approver that cross checks the input parameter of the epw.x script
    with the mpi settings.
    """
    _loggername = "QEEPWInputParalApprover"
    _input_approver_class = QEEPWInputApprover
    _structure_class = QEEPWInputStructure

    def validate(self, *args, **kwargs):
        super().validate(*args, **kwargs)
        self._check_enough_npools()

    def _check_enough_npools(self):
        # for epw, npool must be equal to the number of processes
        npools = self.command_arguments.get("-npool", 1)
        if not self.mpi_command and npools > 1:
            self.errors.append("npools must be equal to number of processors.")
            return
        if self.total_ncpus != npools:
            self.errors.append("npools must be equal to number of processors.")
