from ..bases import BaseInputApprover
from ...handlers.file_structures import QEMatdynInputStructure
from ...variables import ALL_QEMATDYN_VARIABLES


class QEMatdynInputApprover(BaseInputApprover):
    """Class that checks the input variables for a matdyn.x calculation.
    """
    _loggername = "QEMatdynInputApprover"
    _variables_db = ALL_QEMATDYN_VARIABLES
    _structure_class = QEMatdynInputStructure
