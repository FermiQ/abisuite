import numpy as np

from ..bases import BaseCrossApprover, BaseInputApprover
from ...handlers.file_structures import Wannier90InputStructure
from ...variables import ALL_WANNIER90_VARIABLES


class Wannier90InputApprover(BaseInputApprover):
    """Class that checks the input variables for a wannier90.x calculation.
    """
    _loggername = "Wannier90InputApprover"
    _variables_db = ALL_WANNIER90_VARIABLES
    _structure_class = Wannier90InputStructure

    def validate(self):
        super().validate()
        try:
            self._check_bands_vs_wannier_functions()
            self._check_kpts_vs_mpgrid()
            self._check_projections()
        except Exception as e:
            if len(self.errors):
                # there were errors, just accept the exceptions and solve
                # the errors.
                return
            # if no errors, something bad happened during the validation.
            # reraise the exception
            raise e

    def _check_bands_vs_wannier_functions(self):
        bands = self.input_variables["num_bands"].value
        nwann = self.input_variables["num_wann"].value
        if bands < nwann:
            self.errors.append(
                f"num_wann ({nwann}) must be >= number of bands ({bands}).")

    def _check_kpts_vs_mpgrid(self):
        kpts = self.input_variables["kpoints"]
        mpgrid = self.input_variables["mp_grid"]
        nkpts = len(kpts)
        gridsize = mpgrid[0] * mpgrid[1] * mpgrid[2]
        if nkpts != gridsize:
            self.errors.append(f"nkpts={nkpts}!={gridsize}=mp grid size")

    def _check_projections(self):
        # there should be the same amount of projections as there are
        # wannier functions to compute
        # one line per atom too
        iv = self.input_variables
        allatoms = []
        total_nproj = 0
        for proj in iv["projections"]:
            # Al:l=0;l=1
            atom = proj.split(":")[0].strip()
            # check if atom is present in the system
            if atom not in iv["atomic_positions"]["positions"].keys():
                self.errors.append(f"Atom {atom} in projections not defined "
                                   f"in atomic positions.")
                return
            # if atom in allatoms:
            #     # one line per atom
            #     self.errors.append(f"Atom {atom} appears twice in list of "
            #                        "projections.")
            #     return
            allatoms.append(atom)
            # get number of atoms of this kind
            natom_of_this_kind = len(
                            iv["atomic_positions"]["positions"][atom])
            # compute number of projections
            projections = proj.split(atom + ":")[-1].strip().split(";")
            for projection in projections:
                projection = projection.strip()
                if projection.startswith("l"):
                    l_proj = int(projection.split("=")[-1])
                    total_nproj += (2 * l_proj + 1) * natom_of_this_kind
                    if l_proj < 0:
                        self.errors("projections on 'l' orbitals cannot "
                                    "have negative 'l' xD...")
                    continue
                elif projection.startswith("sp3"):
                    # sp3 = 4 hybrid orbitals of 1s and 3p orbitals
                    total_nproj += 4 * natom_of_this_kind
                    continue
                elif projection.startswith("s"):
                    total_nproj += 1 * natom_of_this_kind
                    continue
                elif projection.startswith("p"):
                    total_nproj += 3 * natom_of_this_kind
                    continue
                elif projection.startswith("d"):
                    total_nproj += 5 * natom_of_this_kind
                    continue
                else:
                    raise NotImplementedError(projection)
        nwann = self.input_variables["num_wann"].value
        if nwann < total_nproj:
            self.errors.append(f"total # of projections ({total_nproj}) is >"
                               f" than # of wannier functions to compute "
                               f"({nwann})")


class Wannier90ParentApprover(BaseCrossApprover):
    """Cross approver to validate a wannier90 calculation with it's parent.
    """
    _input_approver_class = Wannier90InputApprover
    _loggername = "Wannier90ParentApprover"
    _structure_class = Wannier90InputApprover._structure_class

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self._parent = None

    @property
    def parent(self):
        # we want the handler for the parent calculation
        if self._parent is not None:
            return self._parent
        raise ValueError("need to set parent log file.")

    @parent.setter
    def parent(self, parents):
        from ...handlers import CalculationDirectory
        if isinstance(parents, str):
            parents = (parents, )
        for parent in parents:
            # expect a path to a parent calculation
            calc = CalculationDirectory.from_calculation(
                    parent, loglevel=self._loglevel)
            if calc.meta_data_file.calctype == "qe_pw2wannier90":
                # a suitable parent is the parent of this one as pw2wannier90
                # needs a nscf calculation to launch
                self.parent = calc.meta_data_file.parents
                return
            if calc.meta_data_file.calctype == "qe_pw":
                # found parent of good type
                self._parent = calc
                break
        else:
            raise ValueError(
                    f"Could not find a suitable parent calculation in "
                    f"{parents}")

    def validate(self):
        self._check_windows()
        super().validate()

    @classmethod
    def from_approver(cls, approver, parents, *args, **kwargs):
        instance = super().from_approver(approver, *args, **kwargs)
        instance.parent = parents
        return instance

    def _check_windows(self):
        # check that the disentanglement window contains enough bands for
        # the number of Wannier functions to compute
        # check if verbosity was set to high
        with self.parent.input_file as input_file:
            if input_file.input_variables.get(
                    "verbosity", "default") != "high":
                self._logger.warning(
                        "verbosity of parent calculation is not set to high.."
                        ". Cannot check disantanglement window."
                        )
                return
        # get eigenvalues and bands and kpts
        with self.parent.log_file as log:
            eigenvalues = np.array(log.eigenvalues["eigenvalues"])
            nwann = self.input_variables["num_wann"].value
            dis_win_min = self.input_variables.get(
                    "dis_win_min", min(eigenvalues[:, 0]))
            dis_win_max = self.input_variables.get(
                    "dis_win_max", max(eigenvalues[:, -1]))
            for eigs_this_kpt in eigenvalues:
                nbands = len(np.where(
                    np.logical_and(
                        eigs_this_kpt >= dis_win_min,
                        eigs_this_kpt <= dis_win_max))[0])
                if nbands < nwann:
                    self.errors.append(
                            f"Distanganglement window ([{dis_win_min.value}, "
                            f"{dis_win_max.value}]) is too small for number "
                            f"of wannier functions to compute ({nwann}). The "
                            "number of bands included in window for each kpt "
                            "must be >= than number of wannier functions.")
                    return
