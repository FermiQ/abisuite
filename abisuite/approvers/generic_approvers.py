from .bases import BaseInputApprover
from ..handlers.file_structures import GenericInputStructure
from ..variables import GENERIC_INPUT_VARIABLES_DB


class GenericInputApprover(BaseInputApprover):
    """Generic input approver class. Has the basic properties of any input
    approver but doesn't check anything.
    """
    _loggername = "GenericInputApprover"
    _variables_db = GENERIC_INPUT_VARIABLES_DB
    _structure_class = GenericInputStructure
