import os

import numpy as np

from ..bases import BaseInputApprover, BaseInputParalApprover
from ...handlers.file_structures import AbinitInputStructure
from ...routines import is_vector, is_2d_arr
from ...variables import ALL_ABINIT_VARIABLES

# there is utilities in abipy to validate input files but the only thing they
# do is to call abinit in dry run and check for errors. This approver class
# is more passive as it does not call abinit.

TOLERANCES_VARS = ("toldfe", "tolwfr", "toldff", "tolrff", "tolvrs")
ATOMIC_POSITION_VARS = ("xcart", "xred")
PARAL_VARS = ("npfft", "npkpt", "npband", "npspinor", "nphf")


class AbinitInputApprover(BaseInputApprover):
    """Class that checks if a set of input variable is
     valid for an abinit calculation. The 'valid' attribute states
     if the input should be good. If it is False, abinit will raise an error
     if it is launched with these variables.
    """
    _loggername = "AbinitInputApprover"
    _variables_db = ALL_ABINIT_VARIABLES
    _structure_class = AbinitInputStructure

    def validate(self):
        ndtset = self.input_variables.get("ndtset", 1)
        if ndtset > 1:
            self._logger.warning("Input approval is not implemented"
                                 " for multidtset.")
            return
        self._check_cell()
        self._check_relax()
        self._check_nscf_ok()
        self._check_basics()
        self._check_kpt_grids()
        self._check_nbands()
        self._check_dfpt()
        self._check_printing()
        self._check_iscf()
        self._check_occupations()
        super().validate()

    def _validate_pseudos(self):
        super()._validate_pseudos()
        self._check_nspinor()
        self._check_znucl()
        self._check_nbands()
        # if pseudos are paw ones, check input variables have been set correct
        self._check_paw()
        self._check_vdw()

    def _check_cell(self):
        acell_in = "acell" in self.input_variables
        scalecart_in = "scalecart" in self.input_variables
        if not acell_in and not scalecart_in:
            self.errors.append(
                    "Either one in 'acell' and 'scalecart' should be defined.")
        if "rprim" in self.input_variables:
            # check that primitive vectors give a non-zero unit cell volume
            rprim = np.array(self.input_variables["rprim"])
            if acell_in:
                acell = self.input_variables["acell"]
                rprim[0] *= acell[0]
                rprim[1] *= acell[1]
                rprim[2] *= acell[2]
            if scalecart_in:
                scalecart = self.input_variables["scalecart"]
                rprim[:, 0] *= scalecart[0]
                rprim[:, 1] *= scalecart[1]
                rprim[:, 2] *= scalecart[2]
            if rprim[0].dot(np.cross(rprim[1], rprim[2])) == 0.0:
                # volume is zero, raise error
                self.errors.append(
                        "The choice of 'rprim+acell+scalecart' gives a unit "
                        "cell volume of 0.0. Rectify this it is an ordah!")
        if "tnons" in self.input_variables:
            # check that tnons is a 3xN array
            tnons = np.array(self.input_variables["tnons"])
            if len(tnons.shape) != 2 or tnons.shape[-1] != 3:
                self.errors.append(
                        f"'tnons' must be a 3xN array but we got: '{tnons}'.")

    def _check_dfpt(self):
        # checks for response function calculations
        rfphon = self.input_variables.get("rfphon", 0).value
        rfelfd = self.input_variables.get("rfelfd", 0).value
        if not rfphon and not rfelfd:
            # not a dfpt calc
            return
        qpt = self.input_variables.get("qpt", [0.0, 0.0, 0.0]).value
        qpt = list(qpt)
        if qpt != [0.0, 0.0, 0.0]:
            if rfelfd:
                self.errors.append(
                        "Cannot compute electric field response function for "
                        "non-gamma qpts => "
                        f"rfelfd = '{rfelfd}' and qpt = '{qpt}'.")
        # rfatpol (len=2) should match number of atoms
        # that means rfatpol(:) >= 1 and <= natom
        # also rfatpol(1) <= rfatpol(2)
        rfatpol = self._get_var("rfatpol")
        natom = self._get_var("natom")
        if rfatpol[0] > rfatpol[1]:
            self.errors.append("rfatpol[0] should be <= rfatpol[1]")
        if rfatpol[0] > natom or rfatpol[1] > natom:
            self.errors.append("rfatpol[:] should be <= natom")
        if rfatpol[0] < 1 or rfatpol[1] < 1:
            self.errors.append("rfatpol[:] should be >= 1")
        # if doing DFPT + PAW + GGA calc, pawxcdev must be set to 0
        # it is also recommended to use it for GS run.
        usepaw = self._get_var("usepaw")
        if usepaw != 1:
            return
        ixc = self._get_var("ixc")
        pawxcdev = self._get_var("pawxcdev")
        if pawxcdev == 0:
            return
        # get xclevel (from 56_xc/m_xclevel.F90)
        # if xclevel == 2 => we use GGA
        xclevel = 1
        if isinstance(ixc, str):
            if ixc.lower() == "pbe":
                xclevel = 2
        else:
            if (ixc >= 11 and ixc <= 19) or (ixc >= 23 and ixc <= 29) or (
                    ixc == 1402000):
                xclevel = 2
            else:
                # I tried to determine the algorithm but abinit
                # calls libxc at this
                # point and since my knowledge of C is limited...
                # worst case is that abinit crashes on start
                self._logger.info(
                        "Could not analyze ixc type to get xclevel...")
                return
        if xclevel >= 2:
            self.errors.append(
                    "Need to set 'pawxcdev'=0 because. N.B.: It is "
                    "recommended to use pawxcdev=0 in GS run as well. "
                    "See: https://docs.abinit.org/variables/paw/#pawxcdev")
            return

    def _check_iscf(self):
        # check iscf
        iscf = self._get_var("iscf")
        kptopt = self._get_var("kptopt")
        if kptopt < 0 and iscf != -2:
            # iscf must be set to -2
            self.errors.append("If kptopt < 0, iscf must be set to -2.")

    def _check_nbands(self):
        # there must be enough bands to contain electrons.
        if "nband" not in self.input_variables:
            occopt = self.input_variables.get("occopt", 1)
            if occopt not in (0, 2):
                # nband can be ommitted
                # https://docs.abinit.org/variables/basic/#nband
                self._logger.debug(
                        f"Bypass nband check because it is not set"
                        f" and not mandatory given occopt={occopt.value}.")
                return
            # else, nband must be set
            self.errors.append(
                    f"nband must be set because occopt={occopt.value}.")
            return
        nband = self.input_variables["nband"]
        # result will depend if there is SO coupling
        nspinor = self.input_variables.get("nspinor", 1)
        # need to compute total number of electrons in the system
        if "ntypat" not in self.input_variables:
            self.errors.append("Need to set 'ntypat'.")
            return
        ntypat = self.input_variables["ntypat"]
        nval_per_pseudos = [pseudo.zion for pseudo in self.pseudo_files]
        if len(nval_per_pseudos) != ntypat:
            raise ValueError(
                    f"Number of pseudos ({len(nval_per_pseudos)}) does not "
                    f"match number of types of atoms ({ntypat}).")
        nelec = 0
        if "typat" not in self.input_variables:
            self.errors.append("Need to set 'typat'.")
            return
        typat = self.input_variables["typat"]
        for at in typat:
            # fortran indices start at 1
            nelec += nval_per_pseudos[at - 1]
        # nelec is the total number of electrons in the system.
        # the number of bands needed must half this number at least
        # so add an error if it is less.
        # multiply nelec by nspinor if so coupling
        if nband < nspinor * nelec / 2:
            if nspinor == 1:
                self.errors.append(f"Number of bands {nband} insufficient for"
                                   f"number of electrons in the"
                                   f" system {nelec}.")
            else:
                self.errors.append(f"Number of bands {nband} insufficient for"
                                   f"number of electrons in the"
                                   f" system {nelec} with SO coupling.")

    def _check_occupations(self):
        # check occupations, if defined, are well defined
        occopt = self._get_var("occopt")
        if occopt == 0:
            # occupations should be a single list with length = number of bands
            occ = self._get_var("occ")
            nband = self._get_var("nband")
            if is_2d_arr(occ):
                if len(occ) != 1:
                    self.errors.append("occupations should be a single list.")
                    return
                occ = occ[0]
            if len(occ) != nband:
                self.errors.append(
                        "Length of occ should match number of bands.")
        if occopt in (0, 2):
            iscf = self._get_var("iscf")
            if iscf < 0 and iscf != -3:
                self.errors.append(
                        "occ is defined but iscf < 0 and != -3. for iscf < 0, "
                        "it needs to be set to -3 in order to read the occ!")

    def _check_paw(self):
        # check if all pseudos are paw ones
        # all should be paw or all should be non-paw
        is_paw = [pseudo.is_paw for pseudo in self.pseudo_files]
        if any(is_paw) and not all(is_paw):
            # at least one of the pseudo is paw but not all of them
            self.errors.append(
                    "At least one of the pseudos is PAW but not all of them.")
            return
        if not all(is_paw):
            # not a paw calculation
            return
        # paw calculations, check variables have been set
        if "pawecutdg" not in self.input_variables:
            self.errors.append(
                    "Pseudos are PAW: 'pawecutdg' must be defined.")
            return
        pawecutdg = self.input_variables["pawecutdg"].value
        # pawecutdg must be > ecut
        if "ecut" not in self.input_variables:
            self.errors.append("'ecut' should be present in input file.")
            return
        ecut = self.input_variables["ecut"].value
        if pawecutdg < ecut:
            self.errors.append(
                f"'pawecutdg'='{pawecutdg}' should be > than 'ecut'='{ecut}'.")

    def _check_printing(self):
        if self.input_variables.get("prtprocar", 0) > 0:
            if self.input_variables.get("prtdos", 0) != 3:
                self.errors.append(
                        "'prtprocar' != 0 => 'prtdos' must be set to 3.")

    def _check_relax(self):
        ionmov = self.input_variables.get("ionmov", 0)
        if ionmov == 0:
            return
        optcell = self.input_variables.get("optcell", 0)
        if optcell > 0:
            if "ecutsm" not in self.input_variables:
                self.errors.append("'optcell > 0 => need to set 'ecutsm'.")

    def _check_znucl(self):
        # check that znucl given in input matches znucl in pseudo files
        if "znucl" not in self.input_variables:
            self.errors.append("znucl must be defined.")
            return
        znucl = self.input_variables["znucl"].value
        pseudos = self.pseudo_files
        if isinstance(znucl, int) or isinstance(znucl, float):
            znucl = (znucl, )
        znucl_per_pseudos = [pseudo.zatom for pseudo in pseudos]
        if len(znucl) != len(znucl_per_pseudos):
            self.errors.append(f"Number of pseudos ({len(znucl_per_pseudos)})"
                               f" does not match znucl ({znucl}).")
            return
        for i, (in_z, pseudo_z) in enumerate(zip(znucl, znucl_per_pseudos)):
            if in_z != pseudo_z:
                self.errors.append(f"Pseudo #{i + 1} with Z={pseudo_z} does"
                                   f" not match corresponding"
                                   f" znucl={repr(in_z)}.")

    def _check_kpt_grids(self):
        # check nshiftk is consistent with shiftk
        nshiftk = self.input_variables.get("nshiftk", 1)
        if nshiftk < 1 or nshiftk > 210:
            self.errors.append(f"nshiftk = {nshiftk} while it should be >= 1"
                               " and <= 210.")
        if "shiftk" not in self.input_variables:
            # default is 1 shift of [0.5, 0.5, 0.5]
            if nshiftk != 1:
                self.errors.append(f"asked for {nshiftk} shiftk but shiftk not"
                                   "defined and default is only 1 shift.")
        else:
            shiftk = self.input_variables["shiftk"]
            if is_vector(shiftk.value):
                shiftk = [shiftk.value]
            if len(shiftk) != nshiftk:
                self.errors.append(
                        f"nshiftk = {nshiftk.value} but there is"
                        f" {len(shiftk)} shifts given.")
        if "kptbounds" in self.input_variables:
            kptbounds = self.input_variables["kptbounds"]
            for ikpt, kpt in enumerate(kptbounds):
                # try except with raise to continue the outer loop when
                # an error is found
                try:
                    for kcomponent in kpt:
                        if kcomponent > 1 or kcomponent < -1:
                            self.errors.append(
                                    f"kpts #{ikpt} components must be between "
                                    "-1 and 1: "
                                    f"{kpt} in kptbounds is wrong.")
                            raise StopIteration
                except StopIteration:
                    continue

    def _check_nscf_ok(self):
        iscf = self.input_variables.get("iscf", 0)
        if iscf < 0 and iscf != -3:
            # iscf < 0 and iscf != -3 => nscf calculation
            # check that tolwfr > 0
            tolwfr = self.input_variables.get("tolwfr", 0.0)
            if tolwfr <= 0.0:
                self.errors.append("for iscf < 0 and != -3, tolwfr"
                                   "  must be > 0.")

    def _check_basics(self):
        keys = list(self.input_variables.keys())
        check2, presents2 = self._only_one_in(TOLERANCES_VARS, keys)
        check3, presents3 = self._only_one_in(ATOMIC_POSITION_VARS, keys)
        for check, presents in zip([check2, check3], [presents2, presents3]):
            if not check:
                self.errors.append("%s are presents in the input file but"
                                   " there should be one from %s." %
                                   (str(presents), str(TOLERANCES_VARS)))
        # check that ntypat is equal to number of elements in znucl
        ntypat = self.input_variables.get("ntypat", 1).value
        if "znucl" not in self.input_variables:
            self.errors.append("znucl must be defined.")
            return
        znucl = self.input_variables["znucl"].value
        if isinstance(znucl, int) or isinstance(znucl, float):
            znucl = (znucl, )
        if ntypat != len(znucl):
            self.errors.append(f"Number of atom types ({repr(ntypat)}) doesn't"
                               f" match the znucl variable ({repr(znucl)}).")
        # check that natom = len(typat)
        natom = self.input_variables.get("natom", 1)
        natrd = self.input_variables.get("natrd", None)
        typat = self.input_variables.get("typat", [])
        # typat default is [] same reason as znucl above
        if isinstance(typat.value, int):
            typat.value = (typat.value, )
        if natrd.value is None:
            # compare natom to length of typat
            if natom != len(typat):
                self.errors.append(f"Number of atoms ({natom.value}) "
                                   "does not match the typat variable "
                                   f"({typat.value}).")
        else:
            # compare typat to natrd
            if len(typat) != natrd:
                self.errors.append(
                        f"Length of typat ({typat.value}) does not match "
                        f"natrd ({natrd.value}).")
            if natrd != natom and ("spgroup" not in self.input_variables and
                                   "nsym" not in self.input_variables):
                self.errors.append(
                        f"Number of atoms ({natom.value}) must match the "
                        f"number of atoms to read natrd ({natrd.value})")

    def _only_one_in(self, lst1, lst2):
        # check that there is exactly one item of list 1 in list 2
        presents = []
        result = True
        for item in lst1:
            if item in lst2:
                if len(presents):
                    result = False
                presents.append(item)
        if not len(presents):
            # no item is present in list2
            return False, presents
        return result, presents

    def _at_least_one_in(self, lst1, lst2):
        # check that at least one item of list 1 is in list 2
        for item in lst1:
            if item in lst2:
                return True
        return False

    def _check_nspinor(self):
        # check spinors vs pseudos
        nspinor = self.input_variables.get("nspinor", 1).value
        if nspinor == 1:
            # all pseudos should support this mode a priori?
            return
        # nspinor is 2, all pseudos should support SOC
        for pseudo in self.pseudo_files:
            if not pseudo.has_so:
                self.errors.append(
                        f"pseudo '{os.path.basename(pseudo.path)}' does not "
                        "support SOC while 'nspinor' is set to 2.")

    def _get_var(self, var):
        # return the variable value. if it is not defined in the input
        # variables, the default value is computed
        if var in self.input_variables:
            return self.input_variables.get(var).value
        # from here, var is not defined, compute default value
        if var == "iscf":
            # default is 17 if %usepaw == 1, 0 if usewvl == 1, 7 otherwise
            usepaw = self._get_var("usepaw")
            if usepaw == 1:
                return 17
            usewvl = self._get_var("usewvl")
            if usewvl == 1:
                return 0
            return 7
        if var == "ixc":
            # the default is set from the pseudo potential files
            # all functionals should be the same between pseudos
            ixcs = [pseudo.ixc for pseudo in self.pseudo_files]
            if len(ixcs) > 1:
                for ixc, pseudo in zip(ixcs[1:], self.pseudo_files[1:]):
                    if ixc != ixcs[0]:
                        self.errors.append(
                                f"pseudo '{os.path.basename(pseudo.path)}' ixc"
                                " is not the same as the others!")
            return ixcs[0]
        if var == "kptopt":
            # default is 4 if nspden == 4, 1 otherwise
            nspden = self._get_var("nspden")
            if nspden == 4:
                return 4
            return 1
        if var == "nband":
            # default is computed from abinit
            # which is total number of valence electrons + 1
            # (actually possibly a more complicated formula)
            # from pseudos compute total number of valence electrons
            # count number of each type of atoms
            # only for occopt != 2 that nband may be ommitted
            if self._get_var("occopt") == 2:
                raise ValueError("nband should be provided.")
            natom_per_type = [0] * self._get_var("ntypat")
            for typat in self._get_var("typat"):
                natom_per_type[typat - 1] += 1
            nval = 0
            for pseudo, natom in zip(self.pseudo_files, natom_per_type):
                nval += natom * pseudo.zion
            return nval // 2 + 1
        if var == "nkpt":
            # 1 if kptopt == 0, 0 otherwise
            if self._get_var("kptopt") == 0:
                return 1
            return 0
        if var == "nspden":
            # default is nsppol
            nsppol = self._get_var("nsppol")
            return nsppol
        if var == "nsppol":
            # default is 1
            return 1
        if var == "occ":
            # default is a 2d array whose size depends of occopt
            occopt = self._get_var("occopt")
            nband = self._get_var("nband")
            if occopt == 0:
                # occ is a 1 x nband array
                return [0] * nband
            elif occopt == 2:
                # occ is a nkpt x nband x nsppol array
                nsppol = self._get_var("nsppol")
                nkpt = self._get_var("nkpt")
                return [[0] * nband] * (nkpt * nsppol)
            else:
                # this var is ignored in other cases
                return 0
        if var == "occopt":
            # default is 1
            return 1
        if var == "pawxcdev":
            # https://docs.abinit.org/variables/paw/#pawxcdev
            return 1
        if var == "rfatpol":
            # default is [1, 1]
            return [1, 1]
        if var == "usepaw":
            # this variable is set when reading the pseudopotentials
            # for paw pseudos, it is set to 1 otherwise 0
            for pseudo in self.pseudo_files:
                if pseudo.is_paw:
                    return 1
            return 0
        if var == "usewvl":
            # default is 0
            return 0
        if var == "vdw_xc":
            # default is 0
            return 0
        raise NotImplementedError(var)

    def _check_vdw(self):
        # vdw functionals are only available for certain types of xc.
        ixc = self._get_var("ixc")
        vdw_xc = self._get_var("vdw_xc")
        if vdw_xc == 0:
            # no vdw correction
            return
        if vdw_xc in (6, 7):
            # DFT-D correction os only available for certain types of xc
            # the ixc availables are enumerated in abinit src files
            if ixc not in (
                    11, -101130, -130101, 18, -106131, -131106, 19, -106132,
                    -132106, -202231, -231202, 14, -102130, -130102, -170,
                    41, -406):
                self.errors.append(
                        f"vdw_xc={vdw_xc} is not implemented for xc functional"
                        f" ixc={ixc}.")


class AbinitInputParalApprover(BaseInputParalApprover):
    """Class that compares the parallelization variables of Abinit with
    the mpi settings.
    """
    _loggername = "AbinitInputParalApprover"
    _input_approver_class = AbinitInputApprover
    _structure_class = AbinitInputStructure

    def validate(self):
        super().validate()
        self._compare_nprocs_with_paralvars()

    def _compare_nprocs_with_paralvars(self):
        paralvars = {var: int(self.input_variables.get(var, 1).value)
                     for var in PARAL_VARS}
        product = np.prod(list(paralvars.values()))
        if product > self.total_ncpus:
            self.errors.append(f"Parallelism scheme ask for too much proc:"
                               f" {paralvars} => {product}")
