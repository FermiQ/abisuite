from ..bases import BaseApprover
from ..exceptions import InputFileError
from ...handlers.file_structures import AbinitMrgddbInputStructure


class AbinitMrgddbInputApprover(BaseApprover):
    """Approver class for an abinit mrgddb input file.
    """
    _loggername = "AbinitMrgddbInputApprover"
    _exception = InputFileError
    _structure_class = AbinitMrgddbInputStructure

    def validate(self):
        super().validate()
        if self.nddb <= 1:
            self.errors.append(
                    "Need to merge at least 2 ddbs!"
                    )
