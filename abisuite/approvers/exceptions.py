# This file contains custom exceptions for approvers


class PseudosError(Exception):
    pass


class InputFileError(Exception):
    pass


class MetaDataError(Exception):
    pass


class MPIError(Exception):
    pass


class PBSError(Exception):
    pass


class SymLinkError(Exception):
    pass
