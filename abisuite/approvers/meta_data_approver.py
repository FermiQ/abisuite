from .bases import BaseApprover
from .exceptions import MetaDataError
from ..handlers.file_structures import MetaDataStructure


class MetaDataApprover(BaseApprover):
    """Approver for meta data file.
    """
    _exception = MetaDataError
    _loggername = "MetaDataApprover"
    _structure_class = MetaDataStructure

    def validate(self):
        """Validate meta data file.
        """
        for attr in self.structure.all_attributes:
            try:
                getattr(self, attr)
            except ValueError:
                self.errors.append("'{attr}' not set.")
        super().validate()
