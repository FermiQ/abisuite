from .bases import BaseApprover
from .exceptions import SymLinkError
from ..handlers.file_structures import SymLinkStructure
import os


class SymLinkApprover(BaseApprover):
    """Approver for a symlink file.
    """
    _exception = SymLinkError
    _loggername = "SymLinkApprover"
    _structure_class = SymLinkStructure

    def validate(self):
        """Validates the symlink file.
        """
        if not os.path.exists(self.source):
            self.errors.append(f"Source not found: {self.source}")
        super().validate()
