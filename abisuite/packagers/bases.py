import abc
import os

from .routines import compute_packaging_path, get_packaging_roots
from ..bases import BaseCalctypedUtility


class BasePackager(BaseCalctypedUtility, abc.ABC):
    """Base class for all Packagers objects.
    """

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self._calculation_directory = None
        self.nfiles_saved = 0

    @property
    def calculation_directory(self):
        """The packager's CalculationDirectory.
        """
        if self._calculation_directory is not None:
            return self._calculation_directory
        raise ValueError("Need to set the 'calculation_directory'.")

    @calculation_directory.setter
    def calculation_directory(self, calc):
        if isinstance(calc, str):
            from ..handlers import CalculationDirectory
            calc = CalculationDirectory.from_calculation(
                    calc, loglevel=self._loglevel)
        self._calculation_directory = calc

    @abc.abstractmethod
    def keep_file(self, handler):
        """Tells if a file must be packaged or not.

        Parameters
        ----------
        handler: FileHandler object
            The file handler to check.

        Returns
        -------
        bool: True if we package file, False if not.
        """
        name = handler.basename
        if name.endswith(".in"):
            return True
        if name.endswith(".log"):
            return True
        if name.endswith(".meta"):
            return True
        if name.endswith(".sh"):
            return True
        return False

    def package(self, overwrite=False, **kwargs):
        """Packages the calculation directory attached to the packager object.
        It basically copies relevant files useful for safekeeping and
        ignores everything else.

        It also keeps the directory tree intact. That means it will keep the
        subdirectories hierarchy of the src w.r.t. a given root and create/keep
        the same hierarchy w.r.t. the dest root.

        Parameters
        ----------
        overwrite: bool, optional
            If True and files with the same names are present at packaging
            destination, those files will be overwritten.

        all other kwargs are passed to the `get_packaging_roots` method.

        Raises
        ------
        ValueError:
            - If the calculation directory to package lies outside the
            src root.
        """
        src_root, dest_root = get_packaging_roots(
                _logger=self._logger, **kwargs)
        # compare src root with calculation directory
        calcdirrelpath = os.path.relpath(
                self.calculation_directory.path, src_root)
        if calcdirrelpath.startswith(".."):
            raise ValueError(
                    f"Directory '{self.calculation_directory.path}' "
                    f"is outside package src root: '{src_root}'.")
        for handler in self.calculation_directory.walk(paths_only=False):
            if self.keep_file(handler):
                newpath = compute_packaging_path(
                        handler.path, src_root, dest_root)
                handler.copy_file(newpath, overwrite=overwrite)
                self.nfiles_saved += 1
