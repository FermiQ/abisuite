import os

from .. import __USER_CONFIG__
from ..routines import full_abspath


# FG: 2021/05/05 I moved these methods that were originally inside
# the BasePackager class outside of it since I use them
# in the 'abipackage' script.
def compute_packaging_path(og_path, src_root, dest_root):
    """Computes the destination path of a given file/directory.

    Parameters
    ----------
    og_path: str
        The original path of the object.
    src_root: str
        The src_root of the packaging process.
    dest_root: str
        The dest_root of the packaging process.

    Returns
    -------
    str: The full absolute destination path of the file.
    """
    rel_to_src = os.path.relpath(og_path, src_root)
    return full_abspath(os.path.join(dest_root, rel_to_src))


def get_packaging_roots(
        src_root=None, dest_root=None, _logger=None):
    """Gets the packaging roots.

    Parameters
    ----------
    src_root : str, optional
        The src root where the hierarchy is computed from. If None,
        the root package from the config file is taken by default.
        If the calculation directory has a relative path
        'above' or 'outside' the root, an error is thrown.
    dest_root : str, optional
        The dest root where the files will be packaged. If None,
        the dest root package from the config file is taken by default.
    _logger: logging.Logger object, optional
        If not None, debugging info about the packaging roots will be
        logged with this logger object.

    Returns
    -------
    tuple: (src_root :: str, dest_root :: str)
        The src_root and dest_root of the packaging process.

    Raises
    ------
    ValueError:
        - If no src_root/dest_root is given and the default values in
        the user config file are not set.
    """
    if src_root is None:
        src_root = __USER_CONFIG__.DATABASE["package_src_root"]
        if _logger is not None:
            _logger.debug(
                "Taking default packaging src root from config file.")
        if src_root is None:
            raise ValueError(
                    "The 'package_src_root' in config file is set to "
                    "None.")
    if _logger is not None:
        _logger.debug(f"Packaging src root is '{src_root}'.")
    if dest_root is None:
        dest_root = __USER_CONFIG__.DATABASE["package_dest_root"]
        if _logger is not None:
            _logger.debug(
                "Taking default packaging dest root from config file.")
        if dest_root is None:
            raise ValueError(
                    "The 'package_dest_root' in config file is set to "
                    "None.")
    if _logger is not None:
        _logger.debug(f"Packaging dest root is '{dest_root}'.")
    return src_root, dest_root
