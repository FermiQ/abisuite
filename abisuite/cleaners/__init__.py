from .abinit_cleaners import AbinitCleaner
from .qe_cleaners import (
        QEEPWCleaner, QEMatdynCleaner, QEPHCleaner, QEPWCleaner, QEQ2RCleaner,
        )


CALCTYPES_TO_CLEANER_CLS = {
        "abinit": AbinitCleaner,
        "qe_epw": QEEPWCleaner,
        "qe_matdyn": QEMatdynCleaner,
        "qe_ph": QEPHCleaner,
        "qe_pw": QEPWCleaner,
        "qe_q2r": QEQ2RCleaner,
        }
