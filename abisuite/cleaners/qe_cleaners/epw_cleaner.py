from .bases import BaseQECleaner


class QEEPWCleaner(BaseQECleaner):
    """Cleaner class for a 'qe_epw' calculation.
    """
    _calctype = "qe_epw"
    _loggername = "QEEPWCleaner"
