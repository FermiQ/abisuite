from ..bases import BaseCleaner


class BaseQECleaner(BaseCleaner):
    """Base class for QE cleaners.
    """

    def delete_this_file(self, handler):
        if ".wfc" in handler.basename:
            return True
        if handler.basename.startswith("wfc") and (
                handler.basename.endswith(".dat")):
            return True
        return super().delete_this_file(handler)
