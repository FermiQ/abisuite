import os

import numpy as np

from .bases import BaseWorkflow, SequencersList
from ..exceptions import DevError
from ..handlers import CalculationDirectory
from ..launchers.launchers.qe_launchers.pw_launcher import (
        __ALL_QE_GEOMETRY_VARIABLES__,
        )
from ..plotters import Plot, MultiPlot, rand_cmap
from ..post_processors import ConductivityTensor
from ..routines import is_list_like
from ..sequencers import (
        QEBandStructureSequencer,
        QEEcutConvergenceSequencer, QEEcutPhononConvergenceSequencer,
        QEEPWWithPhononInterpolationSequencer, QEFermiSurfaceSequencer,
        QEIBTESequencer, QEKgridConvergenceSequencer,
        QEPhononDispersionSequencer,
        QERelaxationSequencer, QESCFSequencer, QESmearingConvergenceSequencer,
        QESmearingPhononConvergenceSequencer,
        QEThermalExpansionSequencer,
        )


class QEWorkflow(BaseWorkflow):
    """Workflow for Espresso. For now supports:

    - Fermi Surface calculation
    - Lattice expansion calculation
    - EPW interpolation (regular)
    - EPW interpolation with lattice thermal expansion
    - IBTE (regular)
    - IBTE with lattice thermal expansion
    - Plus all workflows implemented in the base class (see BaseWorkflow)
    """
    _all_geometry_variables = __ALL_QE_GEOMETRY_VARIABLES__
    _band_structure_sequencer_cls = QEBandStructureSequencer
    _loggername = "QEWorkflow"
    _gs_sequencer_cls = QESCFSequencer
    _gs_ecut_convergence_sequencer_cls = QEEcutConvergenceSequencer
    _gs_kgrid_convergence_sequencer_cls = QEKgridConvergenceSequencer
    _gs_smearing_convergence_sequencer_cls = QESmearingConvergenceSequencer
    _phonon_ecut_convergence_sequencer_cls = QEEcutPhononConvergenceSequencer
    _phonon_dispersion_sequencer_cls = QEPhononDispersionSequencer
    _phonon_smearing_convergence_sequencer_cls = (
            QESmearingPhononConvergenceSequencer)
    _relaxation_sequencer_cls = QERelaxationSequencer
    workflow = (
            "gs_ecut_convergence", "gs_kgrid_convergence",
            "gs_smearing_convergence", "relaxation",
            "phonon_ecut_convergence", "phonon_smearing_convergence", "gs",
            "band_structure", "fermi_surface",
            "phonon_dispersion", "phonon_dispersion_qgrid_convergence",
            "lattice_expansion", "epw_interpolation", "ibte",
            "epw_interpolation_with_thermal_expansion",
            "ibte_with_thermal_expansion",
            )

    def __init__(
            self,
            epw_interpolation=False,
            epw_interpolation_with_thermal_expansion=False,
            fermi_surface=False,
            ibte=False,
            ibte_with_thermal_expansion=False,
            lattice_expansion=False,
            phonon_dispersion_qgrid_convergence=False,
            *args, **kwargs):
        """QE Workflow init method.

        Parameters
        ----------
        epw_interpolation: bool, optional
            If True, the Wannier function interpolation with the EPW module
            will be executed.
        epw_interpolation_with_thermal_expansion: bool, optional
            If True, the interpolations using wanner function will be done
        fermi_surface: bool, optional
            If True, fermi surface calculation will be done in order to
            plot fermi surface.
        ibte: bool, optional
            If True, IBTE transport calculation is executed.
            'epw_interpolation' must be set to True as well.
        ibte_with_thermal_expansion: bool, optional
            If True, IBTE transport data is computed considering thermal
            expansion as well. 'epw_interpolation_with_thermal_expansion'
            must be set to True as well.
        lattice_expansion: bool, optional
            If True, the lattice expansion is computed.
            considering lattice expansion.
        phonon_dispersion_qgrid_convergence: bool, optional
            If True, the phonon dispersion qgrid convergence is added to the
            workflow.
        """
        super().__init__(*args, **kwargs)
        # FERMI SURFACE PART
        self._compute_fermi_surface = fermi_surface
        self._fermi_surface_sequencer = None
        # PHONON DISPERSION QGRID CONVERGENCE PART
        self._compute_phonon_dispersion_qgrid_convergence = (
                phonon_dispersion_qgrid_convergence)
        self._phonon_dispersion_qgrid_convergence_sequencer = None
        # LATTICE THERMAL EXPANSION PART
        self._compute_lattice_expansion = lattice_expansion
        self._lattice_expansion_sequencer = None
        # EPW INTERPOLATION PART
        self._compute_epw_interpolation = epw_interpolation
        self._epw_interpolation_sequencer = None
        # EPW INTERPOLATION WITH THERMAL EXPANSION PART
        eiwte = epw_interpolation_with_thermal_expansion
        self._compute_epw_interpolation_with_thermal_expansion = eiwte
        self._epw_interpolation_with_thermal_expansion_sequencer = None
        # IBTE PART
        self._compute_ibte = ibte
        self._ibte_sequencer = None
        # IBTE WITH THERMAL EXPANSION PART
        self._compute_ibte_with_thermal_expansion = ibte_with_thermal_expansion
        self._ibte_with_thermal_expansion_sequencer = None

    @property
    def epw_interpolation_sequencer(self):
        if self._epw_interpolation_sequencer is not None:
            return self._epw_interpolation_sequencer
        self._epw_interpolation_sequencer = (
                QEEPWWithPhononInterpolationSequencer(loglevel=self._loglevel))
        return self._epw_interpolation_sequencer

    @property
    def epw_interpolation_with_thermal_expansion_sequencer(self):
        _ = self._epw_interpolation_with_thermal_expansion_sequencer
        if _ is not None:
            return self._epw_interpolation_with_thermal_expansion_sequencer
        if not self._compute_lattice_expansion:
            raise DevError(
                    "Need to set "
                    "'epw_interpolation_with_thermal_expansion_sequencer' "
                    "manually.")
        if self._lattice_expansion_sequencer is None:
            raise RuntimeError(
                    "Please set lattice_expansion calculation first.")
        ncalcs = len(self.lattice_expansion_sequencer.temperatures)
        list_ = SequencersList([
                QEEPWWithPhononInterpolationSequencer(loglevel=self._loglevel)
                for i in range(ncalcs)])
        self._epw_interpolation_with_thermal_expansion_sequencer = list_
        return self.epw_interpolation_with_thermal_expansion_sequencer

    @property
    def fermi_surface_sequencer(self):
        if self._fermi_surface_sequencer is not None:
            return self._fermi_surface_sequencer
        self._fermi_surface_sequencer = QEFermiSurfaceSequencer(
                loglevel=self._loglevel)
        return self._fermi_surface_sequencer

    @property
    def ibte_sequencer(self):
        if self._ibte_sequencer is not None:
            return self._ibte_sequencer
        if not self._compute_epw_interpolation:
            raise ValueError(
                    "Need to set 'epw_interpolation' to True upon init.")
        self._ibte_sequencer = QEIBTESequencer(loglevel=self._loglevel)
        return self._ibte_sequencer

    @property
    def ibte_with_thermal_expansion_sequencer(self):
        if self._ibte_with_thermal_expansion_sequencer is not None:
            return self._ibte_with_thermal_expansion_sequencer
        if self._epw_interpolation_with_thermal_expansion_sequencer is None:
            raise RuntimeError(
                    "Please set epw with thermal expansion calculations first."
                    )
        ncalcs = len(self.epw_interpolation_with_thermal_expansion_sequencer)
        self._ibte_with_thermal_expansion_sequencer = SequencersList([
                QEIBTESequencer(loglevel=self._loglevel)
                for i in range(ncalcs)])
        return self.ibte_with_thermal_expansion_sequencer

    @property
    def lattice_expansion_sequencer(self):
        if self._lattice_expansion_sequencer is not None:
            return self._lattice_expansion_sequencer
        self._lattice_expansion_sequencer = QEThermalExpansionSequencer(
                loglevel=self._loglevel)
        return self.lattice_expansion_sequencer

    @property
    def phonon_dispersion_qgrid_convergence_sequencer(self):
        if self._phonon_dispersion_qgrid_convergence_sequencer is not None:
            return self._phonon_dispersion_qgrid_convergence_sequencer
        self._phonon_dispersion_qgrid_convergence_sequencer = (
                SequencersList([], loglevel=self._loglevel))
        return self.phonon_dispersion_qgrid_convergence_sequencer

    def run_epw_interpolation(self):
        self.epw_interpolation_sequencer.run()

    def run_epw_interpolation_with_thermal_expansion(self):
        if self._compute_lattice_expansion:
            if not self.lattice_expansion_sequencer.sequence_completed:
                return
            self._logger.info(
                    "Thermal lattice expansion sequence completed. "
                    "Now running epw with thermal "
                    "lattice expansion sequences.")
        self.epw_interpolation_with_thermal_expansion_sequencer.run()
        seqs = self.epw_interpolation_with_thermal_expansion_sequencer
        if seqs.sequence_completed:
            self._plot_final_results_epw_interpolation_with_thermal_expansion()

    def run_fermi_surface(self):
        """Runs the fermi_surface part of the workflow.
        """
        self.fermi_surface_sequencer.run()

    def run_ibte(self):
        self.ibte_sequencer.run()

    def run_ibte_with_thermal_expansion(self):
        seqs = self.epw_interpolation_with_thermal_expansion_sequencer
        if not seqs.sequence_completed:
            return
        for seq in self.ibte_with_thermal_expansion_sequencer:
            # don't show plot (only 1 point plots are uninteresting)
            show = seq.plot_calculation_parameters.pop("show", True)
            seq.plot_show = False
            seq.run()
        if self.ibte_with_thermal_expansion_sequencer.sequence_completed:
            self._logger.info("Plotting final IBTE results.")
            self._plot_final_results_ibte_with_thermal_expansion(show)
        else:
            self._logger.info(
                    "Waiting for IBTE sequence to complete to plot final "
                    "results.")

    def run_lattice_expansion(self):
        deltas = self.lattice_expansion_sequencer.deltas_volumes
        zero = 0 in deltas or 0.0 in deltas
        completed = self.gs_sequencer.sequence_completed
        if self._compute_gs and zero and not completed:
            # wait for gs
            return
        self.lattice_expansion_sequencer.run()

    def run_phonon_dispersion_qgrid_convergence(self):
        self.phonon_dispersion_qgrid_convergence_sequencer.run()
        seqs = self.phonon_dispersion_qgrid_convergence_sequencer
        if not seqs.sequence_completed:
            return
        # plot final results
        all_plots = []
        colors = rand_cmap(len(seqs))
        for iseq, sequencer in enumerate(seqs):
            plot = Plot.load_plot(sequencer.plot_save_pickle)
            plot.curves[0].label = (
                    r"$N_q = $" + " ".join(
                        [str(x) for x in sequencer.phonons_qpoint_grid]))
            for curve in plot.curves:
                curve.color = colors(iseq / len(seqs))
            all_plots.append(plot)
        final_plot = sum(all_plots[1:], all_plots[0])
        show = sequencer.plot_calculation_parameters.get("show", True)
        final_plot.plot(show=show)
        results_dir = os.path.join(
                os.path.dirname(
                    os.path.dirname(
                        sequencer.q2r_workdir)),
                "results")
        final_plot.save(
                os.path.join(
                    results_dir,
                    "phonon_dispersion_qpoint_grid_convergence.pdf"),
                overwrite=True,
                )
        final_plot.save_pickle(
                os.path.join(
                    results_dir,
                    "phonon_dispersion_qpoint_grid_convergence.pickle"),
                overwrite=True)

    def set_band_structure(
            self, *args, use_gs=False,
            band_structure_input_variables=None,
            **kwargs):
        """Sets the band structure part of the workflow.

        Parameters
        ----------
        use_gs: bool, optional
            If True, the gs part of the workflow is used to make the
            band_structure part of the calculation.
        band_structure_input_variables: dict, optional
            Dict of the input variables for the band structure calculation.
            If None and the 'use_gs' flag is set to True, the input variables
            are taken to be the one from the gs calculation. The appropriate
            band structure variables will be applied as well (through the
            sequencer).
        other kwargs and args are passed to the Base class method.
        """
        if use_gs:
            if band_structure_input_variables is None:
                band_structure_input_variables = (
                    self.gs_sequencer.scf_input_variables.copy())
            else:
                real_vars = self.gs_sequencer.scf_input_variables.copy()
                real_vars.update(band_structure_input_variables)
                band_structure_input_variables = real_vars
        super().set_band_structure(
                *args, use_gs=use_gs,
                band_structure_input_variables=band_structure_input_variables,
                **kwargs)

    def set_epw_interpolation(
            self,
            root_workdir=None,
            asr=None,
            scf_input_variables=None,
            scf_calculation_parameters=None,
            nscf_kgrid=None,
            nscf_input_variables=None,
            nscf_calculation_parameters=None,
            band_structure_input_variables=None,
            band_structure_calculation_parameters=None,
            band_structure_kpoint_path=None,
            band_structure_kpoint_path_density=None,
            phonons_input_variables=None,
            phonons_calculation_parameters=None,
            phonons_qpoint_grid=None,
            q2r_calculation_parameters=None,
            matdyn_calculation_parameters=None,
            qpoint_path=None,
            qpoint_path_density=None,
            epw_input_variables=None,
            epw_calculation_parameters=None,
            plot_calculation_parameters=None,
            use_gs=False,
            use_gs_converged_ecut=False,
            use_gs_converged_kgrid=False,
            use_gs_converged_smearing=False,
            use_phonon_converged_ecut=False,
            use_phonon_converged_smearing=False,
            use_phonon_dispersion=False,
            use_band_structure=False,
            ):
        """Sets the epw interpolation part of the workflow.

        Parameters
        ----------
        root_workdir: str
            The root workdir for the whole sequence.
        asr: str, optional
            Needed for the matdyn calculations to get the phonon dispersion.
            Ignored if 'use_phonon_dispersion' is True.
        scf_input_variables: dict, optional
            The dict of scf input variables. Ignored if 'use_gs' is True.
        scf_calculation_parameters: dict, optional
            The dict of scf calculation parameters. Ignored
            if 'use_gs' is True.
        nscf_kgrid: list-like
            The kgrid for the nscf calculation.
        nscf_input_variables: dict, optional
            The input variables for the NSCF calculations.
            If 'use_gs' is True, these variables will update the variables
            from the gs run. If None, the gs run input variables are used.
            If None but 'use_gs' is False, an error is thrown.
        nscf_calculation_parameters: dict
            The calculation parameters for the nscf calculations.
        band_structure_kpoint_path: list-like, optional
            The kpoint path for the band structure calculation.
            Ignored if 'use_band_structure' is True.
        band_structure_kpoint_path_density: int, optional
            The number of points between points in the band structure.
            Ignored if 'use_band_structure' is True.
        band_structure_input_variables: dict, optional
            The dict of the band structures input variables.
            Ignored if 'use_band_structure' is True.
        band_structure_calculation_parameters: dict, optional
            The calculation parameters for the band structure calculations.
            Ignored if 'use_band_structure' is True.
        phonons_input_variables: dict, optional
            The phonons input variables.
            Ignored if 'use_phonon_dispersion' is True.
        phonons_calculation_parameters: dict, optional
            The calculation parameters for the phonons calculations.
            Ignored if 'use_phonon_dispersion' is True.
        phonons_qpoint_grid: list, optional
            The phonon qpoint grid.
            Ignored if 'use_phonon_dispersion' is True.
        q2r_calculation_parameters: dict, optional
            The q2r calculation parameters.
            Ignored if 'use_phonon_dispersion' is True.
        matdyn_calculation_parameters: dict, optional
            The matdyn calculation parmeters.
            Ignored if 'use_phonon_dispersion' is True.
        qpoint_path: list-like
            The qpoint path for the phonon dispersion.
            Ignored if 'use_phonon_dispersion' is True.
        qpoint_path_density: int
            The qpoint path density for the phonon dispersion.
            Ignored if 'use_phonon_dispersion' is True.
        epw_input_variables: dict
            The epw input variables for the interpolation.
        epw_calculation_parameters: dict, optional
            The epw calculation parameters for the interpolation.
        plot_calculation_parameters: dict, optional
            Plot parameters for the interpolations.
        use_gs: bool, optional
            If True, the gs part of the workflow is used as the scf calculation
            for this sequence.
        use_gs_converged_ecut: bool, optional
            If True and 'use_gs' is not, the gs converged ecut is used for the
            scf calculation.
        use_gs_converged_kgrid: bool, optional
            If True and 'use_gs' is not, the gs converged kgrid is used for the
            scf calculation.
        use_gs_converged_smearing: bool, optional
            If True and 'use_gs' is not, the gs converged smearing
            is used for the scf calculation.
        use_phonon_converged_ecut: bool, optional
            If True and 'use_gs' is not, the phonon converged ecut
            is used for the scf calculation.
        use_phonon_converged_smearing: bool, optional
            If True and 'use_gs' is not, the phonon converged smearing
            is used for the scf calculation.
        use_band_structure: bool, optional
            If True, the band structure part of the workflow is used as the
            band structure part of the sequence.
        use_phonon_dispersion: bool, optional
            If True, the phonon dispersion part of the workflow is used as
            the phonon + q2r + matdyn part of the sequence.
        """
        sequencer = self.epw_interpolation_sequencer
        if root_workdir is None:
            raise ValueError("Need to set 'root_workdir'.")
        # ####################### SCF PART ####################################
        self._use_converged_calculations(
                sequencer,
                use_gs=use_gs,
                scf_input_variables=scf_input_variables,
                scf_calculation_parameters=scf_calculation_parameters,
                scf_workdir=os.path.join(root_workdir, "scf_run"),
                use_gs_converged_ecut=use_gs_converged_ecut,
                use_gs_converged_kgrid=use_gs_converged_kgrid,
                use_gs_converged_smearing=use_gs_converged_smearing,
                use_phonon_converged_ecut=use_phonon_converged_ecut,
                use_phonon_converged_smearing=use_phonon_converged_smearing,
                )
        # ######################## NSCF PART ##################################
        sequencer.nscf_workdir = os.path.join(root_workdir, "nscf_run")
        if nscf_kgrid is None:
            raise ValueError("Need to set 'nscf_kgrid'.")
        sequencer.nscf_kgrid = nscf_kgrid
        if nscf_input_variables is None:
            if use_gs:
                nscf_input_variables = (
                        self.gs_sequencer.scf_input_variables.copy())
            else:
                raise ValueError("Need to set 'nscf_input_variables'.")
        else:
            if use_gs:
                nscf_vars = self.gs_sequencer.scf_input_variables.copy()
                nscf_vars.update(nscf_input_variables)
                nscf_input_variables = nscf_vars
        sequencer.nscf_input_variables = nscf_input_variables
        self._add_calculation_parameters(
                "nscf_", nscf_calculation_parameters, sequencer)
        # ###################### BAND STRUCTURE PART ##########################
        if use_band_structure:
            if not self.band_structure_sequencer.sequence_completed:
                self.stop_at_workflow = "band_structure"
                return
            bsseq = self.band_structure_sequencer
            sequencer.band_structure_workdir = bsseq.band_structure_workdir
            sequencer.band_structure_kpoint_path = (
                    bsseq.band_structure_kpoint_path)
            sequencer.band_structure_kpoint_path_density = (
                    bsseq.band_structure_kpoint_path_density)
            sequencer.band_structure_input_variables = (
                    bsseq.band_structure_input_variables)
        else:
            sequencer.band_structure_workdir = os.path.join(
                    root_workdir, "band_structure_run")
            if band_structure_input_variables is None:
                if use_gs:
                    band_structure_input_variables = (
                        self.gs_sequencer.scf_input_variables.copy())
                else:
                    raise ValueError(
                            "Need to set 'band_structure_input_variables'.")
            else:
                if use_gs:
                    # update vars
                    real_vars = self.gs_sequencer.scf_input_variables.copy()
                    real_vars.update(band_structure_input_variables)
                    band_structure_input_variables = real_vars
            sequencer.band_structure_input_variables = (
                    band_structure_input_variables)
            if band_structure_kpoint_path is None:
                raise ValueError(
                        "Need to set 'band_structure_kpoint_path'.")
            sequencer.band_structure_kpoint_path = band_structure_kpoint_path
            if band_structure_kpoint_path_density is None:
                raise ValueError(
                        "Need to set 'band_structure_kpoint_path_density'.")
            sequencer.band_structure_kpoint_path_density = (
                    band_structure_kpoint_path_density)
            self._add_calculation_parameters(
                    "band_structure_", band_structure_calculation_parameters,
                    sequencer)
        # ####################### PHONONS DISPERSION PART #####################
        if use_phonon_dispersion:
            if not self.phonon_dispersion_sequencer.sequence_completed:
                self.stop_at_workflow = "phonon_dispersion"
                return
            # phonons part
            phseq = self.phonon_dispersion_sequencer
            sequencer.phonons_workdir = phseq.phonons_workdir
            sequencer.phonons_qpoint_grid = phseq.phonons_qpoint_grid
            sequencer.phonons_input_variables = (
                    phseq.phonons_input_variables.copy())
            # q2r part
            sequencer.q2r_workdir = phseq.q2r_workdir
            sequencer.q2r_input_variables = phseq.q2r_input_variables.copy()
            # matdyn part
            sequencer.matdyn_workdir = phseq.matdyn_workdir
            sequencer.qpoint_path = phseq.qpoint_path
            sequencer.qpoint_path_density = (
                    phseq.qpoint_path_density)
            sequencer.matdyn_input_variables = (
                    phseq.matdyn_input_variables.copy())
        else:
            # ###################### PHONONS PART #############################
            sequencer.phonons_workdir = os.path.join(
                    root_workdir, "phonons_runs")
            if phonons_qpoint_grid is None:
                raise ValueError("Need to set 'phonons_qpoint_grid'.")
            sequencer.phonons_qpoint_grid = phonons_qpoint_grid
            if phonons_input_variables is None:
                raise ValueError("Need to set 'phonons_input_variables'.")
            sequencer.phonons_input_variables = phonons_input_variables
            self._add_calculation_parameters(
                    "phonons_", phonons_calculation_parameters, sequencer)
            # ######################## Q2R PART ###############################
            sequencer.q2r_workdir = os.path.join(root_workdir, "q2r_run")
            sequencer.q2r_input_variables = {}
            self._add_calculation_parameters(
                    "q2r_", q2r_calculation_parameters, sequencer)
            # ######################### MATDYN PART ###########################
            sequencer.matdyn_workdir = os.path.join(root_workdir, "matdyn_run")
            if qpoint_path is None:
                raise ValueError("Need to set 'qpoint_path'.")
            sequencer.qpoint_path = qpoint_path
            if qpoint_path_density is None:
                raise ValueError("Need to set 'qpoint_path_density'.")
            sequencer.qpoint_path_density = qpoint_path_density
            if asr is None:
                raise ValueError("Need to set 'asr'.")
            sequencer.matdyn_input_variables = {
                    "asr": asr,
                    "q_in_cryst_coord": True}
            self._add_calculation_parameters(
                    "matdyn_", matdyn_calculation_parameters, sequencer)
        # ######################### EPW PART ##################################
        sequencer.epw_workdir = os.path.join(
                root_workdir, "epw_interpolation_run")
        if epw_input_variables is None:
            raise ValueError("Need to set 'epw_input_variables'.")
        sequencer.epw_input_variables = epw_input_variables
        self._add_calculation_parameters(
                "epw_", epw_calculation_parameters, sequencer)
        # ####################### PLOTTING PART ###############################
        if plot_calculation_parameters is None:
            plot_calculation_parameters = {}
        plot_calculation_parameters["save"] = os.path.join(
                root_workdir, "results", "epw_interpolation.pdf")
        plot_calculation_parameters["save_pickle"] = os.path.join(
                root_workdir, "results", "epw_interpolation.pickle")
        # add title to plots
        self._add_calculation_parameters(
                "plot_", plot_calculation_parameters, sequencer)

    # TODO: cleanup this (too) very long method...
    def set_epw_interpolation_with_thermal_expansion(
            self, root_workdir=None,
            lattice_parameters=None,
            temperatures=None,
            asr=None,
            scf_input_variables=None,
            scf_calculation_parameters=None,
            nscf_kgrid=None, nscf_input_variables=None,
            nscf_calculation_parameters=None,
            band_structure_kpoint_path=None,
            band_structure_kpoint_path_density=None,
            band_structure_input_variables=None,
            band_structure_calculation_parameters=None,
            phonons_input_variables=None,
            phonons_calculation_parameters=None,
            phonons_qpoint_grid=None,
            q2r_calculation_parameters=None,
            matdyn_calculation_parameters=None,
            qpoint_path=None,
            qpoint_path_density=None,
            epw_input_variables=None,
            epw_calculation_parameters=None,
            plot_parameters=None,
            ):
        """Sets the corresponding sequencers for the EPW calculation
        for interpolation that
        takes into account the lattice expansion.

        The SCF input variables are taken from the Thermal Expansion Sequencer.
        The temperatures are taken from this sequencer as well.
        Same as the phonons, q2r and matdyn input variables.

        Parameters
        ----------
        root_workdir: str
            The root workdir for these Sequencers.
        lattice_parameters: list, optional
            Gives the list of lattice parameters to use for the lattice
            expansion. If None, the ones from the lattice expansion
            calculation are taken. This computation option must thus
            be set to True otherwise an error is thrown.
        temperatures: list, optional
            Gives the temperatures corresponding to the lattice parameters.
            If None, they are taken from the lattice expansion calculations.
        asr: str, optional
            The acoustic sum rule to impose (matdyn input variables).
            If None, the one from the lattice expansion calculation is
            taken if it is requested.
        scf_input_variables: dict, optional
            The scf input variables.
        scf_calculation_parameters: dict, optional
            If not None, sets the calculation parameters for the scf
            calculations. If None, takes the same calculation parameters
            as the Thermal expansion part.
        nscf_kgrid: list-like
            The kgrid for the nscf calculation.
        nscf_input_variables: dict
            The input variables for the NSCF calculations.
        nscf_calculation_parameters: dict, optional
            The calculation parameters for the nscf calculations.
        band_structure_kpoint_path: list-like
            The kpoint path for the band structure calculation.
        band_structure_kpoint_path_density: int
            The number of points between points in the band structure.
        band_structure_input_variables: dict, optional
            If not None, states the input variables for the band structure
            run. If None, the nscf input variables are taken by default.
        band_structure_calculation_parameters: dict, optional
            The calculation parameters for the band structure calculations.
        phonons_input_variables: dict, optional
            The phonons input variables.
        phonons_calculation_parameters: dict, optional
            The calculation parameters for the phonons calculations.
            If None, the
            calculation parameters from the thermal expansion sequencer are
            taken by default.
        phonons_qpoint_grid: list, optional
            The phonon qpoint grid.
        q2r_calculation_parameters: dict, optional
            The q2r calculation parameters.
            If None, the q2r calculation parameters
            from the lattice expansion sequencer are taken by default.
        matdyn_calculation_parameters: dict, optional
            The matdyn calculation parmeters.
            If None, the matdyn calculation parameters
            from the lattice expansion sequencer are taken by default.
        qpoint_path: list-like
            The qpoint path for the phonon dispersion.
        qpoint_path_density: int
            The qpoint path density for the phonon dispersion.
        epw_input_variables: dict
            The epw input variables for the interpolation.
        epw_calculation_parameters: dict, optional
            The epw calculation parameters for the interpolation.
        plot_parameters: dict, optional
            Plot parameters for the interpolations.
        """
        if root_workdir is None:
            raise ValueError("Need to set 'root_workdir'.")
        if lattice_parameters is None:
            if not self._compute_lattice_expansion:
                raise ValueError(
                        "No lattice_parameters given and computation of "
                        "lattice parameters expansion not requested.")
            if self._compute_gs:
                if not self.gs_sequencer.sequence_completed:
                    # need to wait for gs computation to complete
                    self._logger.info(
                            "Waiting for gs sequencer to be done.")
                    self.stop_at_workflow = "gs"
                    return
            # if thermal expansion is completed, load data from the
            # lattice_expansion plot.
            if not self.lattice_expansion_sequencer.sequence_completed:
                self._logger.info("Waiting for lattice exp to be done")
                self.stop_at_workflow = "lattice_expansion"
                return
            pickle = self.lattice_expansion_sequencer.plot_save_pickle
            lattice_plot_path = pickle
            if not os.path.exists(lattice_plot_path):
                # run results
                self.run_lattice_expansion()
            # if something is wrong the plot won't show up
            if not os.path.exists(lattice_plot_path):
                self._logger.info(
                        "No data found for lattice thermal expansion."
                        " Nothing done"
                        " for epw with thermal expansion. "
                        "Waiting for relaunch...")
                self.stop_at_workflow = "lattice_expansion"
                return
            lattice_plot = Plot.load_plot(lattice_plot_path)
            temperatures = lattice_plot.curves[0].xdata
            acells = lattice_plot.curves[0].ydata
            lat_seq = self.lattice_expansion_sequencer
            asr = lat_seq.asr
            scf_input_variables = lat_seq.scf_input_variables.copy()
            if scf_calculation_parameters is None:
                scp = lat_seq.scf_calculation_parameters.copy()
                scf_calculation_parameters = scp
                scp.pop("jobname", None)  # automatically set
            phonons_input_variables = lat_seq.phonons_input_variables.copy()
            phonons_qpoint_grid = lat_seq.phonons_qpoint_grid
            if phonons_calculation_parameters is None:
                pcp = lat_seq.phonons_calculation_parameters.copy()
                phonons_calculation_parameters = pcp
                pcp.pop("jobname", None)
            if q2r_calculation_parameters is None:
                qcp = lat_seq.q2r_calculation_parameters.copy()
                q2r_calculation_parameters = qcp
                qcp.pop("jobname", None)  # automatically set
            if matdyn_calculation_parameters is None:
                mcp = lat_seq.matdyn_calculation_parameters.copy()
                matdyn_calculation_parameters = mcp
                mcp.pop("jobname", None)  # automatically set
        else:
            if not is_list_like(lattice_parameters):
                raise TypeError("'lattice_parameters' must be a list.")
            acells = lattice_parameters
            if temperatures is None:
                raise ValueError("Need to set 'temperatures'.")
            if not is_list_like(temperatures):
                raise TypeError("'temperatures' must be a list.")
            if len(acells) != len(lattice_parameters):
                raise ValueError("Length mismatch between 'temperatures' "
                                 "and 'lattice_parameters'.")
            list_ = SequencersList([
                QEEPWWithPhononInterpolationSequencer(loglevel=self._loglevel)
                for i in range(len(temperatures))])
            self._epw_interpolation_with_thermal_expansion_sequencer = list_
            if asr is None:
                raise ValueError("Need to set 'asr'.")
            if scf_calculation_parameters is None:
                raise ValueError("Need to set 'scf_calculation_parameters'.")
            if scf_input_variables is None:
                raise ValueError("Need to set 'scf_input_variables'.")
            if phonons_input_variables is None:
                raise ValueError("Need to set 'phonons_input_variables'.")
            if phonons_calculation_parameters is None:
                raise ValueError(
                        "Need to set 'phonons_calculation_parameters'.")
            if q2r_calculation_parameters is None:
                raise ValueError(
                        "Need to set 'q2r_calculation_parameters'.")
            if matdyn_calculation_parameters is None:
                raise ValueError(
                        "Need to set 'matdyn_calculation_parameters'.")
        for sequencer, acell, temperature in zip(
                self.epw_interpolation_with_thermal_expansion_sequencer,
                acells,
                temperatures):
            # SCF part
            sequencer.scf_workdir = os.path.join(
                    root_workdir, "scf_runs", f"{temperature}K")
            invars = scf_input_variables.copy()
            invars.update({"celldm(1)": acell})
            sequencer.scf_input_variables = invars
            self._add_calculation_parameters(
                    "scf_", scf_calculation_parameters, sequencer)

            # nscf part
            sequencer.nscf_workdir = os.path.join(
                    root_workdir, "nscf_runs", f"{temperature}K")
            if nscf_kgrid is None:
                raise ValueError("Need to set 'nscf_kgrid'.")
            sequencer.nscf_kgrid = nscf_kgrid
            if nscf_input_variables is None:
                raise ValueError("Need to set 'nscf_input_variables'.")
            sequencer.nscf_input_variables = nscf_input_variables
            self._add_calculation_parameters(
                    "nscf_", nscf_calculation_parameters, sequencer)

            # band structure part
            sequencer.band_structure_workdir = os.path.join(
                    root_workdir, "band_structure_runs", f"{temperature}K")
            if band_structure_kpoint_path is None:
                raise ValueError("Need to set 'band_structure_kpoint_path'.")
            sequencer.band_structure_kpoint_path = band_structure_kpoint_path
            if band_structure_kpoint_path_density is None:
                raise ValueError(
                        "Need to set 'band_structure_kpoint_path_density'.")
            bskpd = band_structure_kpoint_path_density
            sequencer.band_structure_kpoint_path_density = bskpd
            if band_structure_input_variables is None:
                band_structure_input_variables = nscf_input_variables.copy()
            bsiv = band_structure_input_variables
            sequencer.band_structure_input_variables = bsiv
            self._add_calculation_parameters(
                    "band_structure_", band_structure_calculation_parameters,
                    sequencer)

            # phonons part
            # (almost everything the same as the thermal lattice part)
            sequencer.phonons_workdir = os.path.join(
                    root_workdir, "phonons_runs", f"{temperature}K", "ph")
            sequencer.phonons_qpoint_grid = phonons_qpoint_grid
            sequencer.phonons_input_variables = phonons_input_variables.copy()
            self._add_calculation_parameters(
                    "phonons_", phonons_calculation_parameters, sequencer)

            # q2r part
            sequencer.q2r_workdir = os.path.join(
                    root_workdir, "q2r_runs", f"{temperature}K")
            sequencer.q2r_input_variables = {}
            self._add_calculation_parameters(
                    "q2r_", q2r_calculation_parameters, sequencer)

            # matdyn part
            sequencer.matdyn_workdir = os.path.join(
                    root_workdir, "matdyn_runs", f"{temperature}K")
            if qpoint_path is None:
                raise ValueError("Need to set 'qpoint_path'.")
            sequencer.qpoint_path = qpoint_path
            if qpoint_path_density is None:
                raise ValueError("Need to set 'qpoint_path_density.'")
            sequencer.qpoint_path_density = qpoint_path_density
            sequencer.matdyn_input_variables = {
                    "asr": asr,
                    "q_in_cryst_coord": True}
            self._add_calculation_parameters(
                    "matdyn_", matdyn_calculation_parameters, sequencer)

            # set epw part
            sequencer.epw_workdir = os.path.join(
                    root_workdir, "epw_interpolation_runs", f"{temperature}K")
            if epw_input_variables is None:
                raise ValueError("Need to set 'epw_input_variables'.")
            sequencer.epw_input_variables = epw_input_variables.copy()
            self._add_calculation_parameters(
                    "epw_", epw_calculation_parameters, sequencer)

            # set plot part
            sequencer.plot_save = os.path.join(
                    root_workdir, "results",
                    f"{temperature}K", "interpolation.pdf")
            sequencer.plot_save_pickle = os.path.join(
                    root_workdir, "results", f"{temperature}K",
                    "interpolation.pickle")
            if plot_parameters is None:
                plot_parameters = {}
            # add title to plots
            plot_parameters.update({"title": f"{temperature}K"})
            self._add_calculation_parameters(
                    "plot_", plot_parameters, sequencer)

    def set_fermi_surface(
            self,
            fermi_surface_k_z=None,
            root_workdir=None,
            scf_input_variables=None,
            scf_calculation_parameters=None,
            nscf_input_variables=None,
            nscf_calculation_parameters=None,
            nscf_kgrid=None,
            plot_calculation_parameters=None,
            use_gs=False,
            use_phonon_converged_ecut=False,
            use_phonon_converged_smearing=False,
            use_relaxed_geometry=False,
            ):
        """Sets the fermi_surface part of the workflow.

        Parameters
        ----------
        fermi_surface_k_z: list-like, optional
            If given, slices of the fermi surfaces will be generated for these
            k_z. These k_z must be part of the 'nscf_kgrid'.
        root_workdir: str
            The root workdir where the calculations will be run.
        scf_input_variables: dict, optional
            The dict of scf input variables.
        scf_calculation_parameters: dict, optional
            The dict of scf calculation parameters.
        nscf_input_variables: dict
            The dict of nscf input variables.
        nscf_calculation_parameters: dict
            The dict of nscf calculation parameters.
        nscf_kgrid: list-like
            The nscf k-point grid used to interpolate the fermi surface.
        plot_calculation_parameters: dict, optional
            The dict of plot calculations parameters.
        use_phonon_converged_ecut: bool, optional
            If True, the phonon converged ecut will be used. The
            'phonon_ecut_convergence' part of the workflow must be activated
            or the 'converged_phonon_ecut' must be set upon init.
        use_phonon_converged_smearing: bool, optional
            If True, the phonon converged smearing will be used. The
            'phonon_smearing_convergence' part of the workflow must be
            activated or the 'converged_phonon_smearing' must be set upon init.
        use_gs: bool, optional
            If True, the 'gs' part of the workflow is used. This option is
            incompatible with 'use_*' flags and will thus override them.
        use_relaxed_geometry: bool, optional
            If True, the relaxed geometry will be used. The 'relaxation' part
            of the workflow must be activated or the 'relaxed_geometry'
            attribute must be set upon init.
        """
        if root_workdir is None:
            raise ValueError("Need to set the 'root_workdir'.")
        sequencer = self.fermi_surface_sequencer
        # SCF PART
        self._use_converged_calculations(
                sequencer, use_gs=use_gs,
                scf_input_variables=scf_input_variables,
                scf_calculation_parameters=scf_calculation_parameters,
                scf_workdir=os.path.join(root_workdir, "scf_run"),
                use_phonon_converged_ecut=use_phonon_converged_ecut,
                use_phonon_converged_smearing=use_phonon_converged_smearing,
                use_relaxed_geometry=use_relaxed_geometry)
        # NSCF PART
        sequencer.nscf_workdir = os.path.join(root_workdir, "nscf_run")
        if nscf_kgrid is None:
            raise ValueError("Need to set 'nscf_kgrid'.")
        sequencer.nscf_kgrid = nscf_kgrid
        if nscf_input_variables is None:
            raise ValueError("Need to set 'nscf_input_variables'.")
        sequencer.nscf_input_variables = nscf_input_variables
        self._add_calculation_parameters(
                "nscf_", nscf_calculation_parameters, sequencer)
        if plot_calculation_parameters is None:
            plot_calculation_parameters = {}
        plot_calculation_parameters["save"] = os.path.join(
                root_workdir, "results", "fermi_surface.pdf")
        plot_calculation_parameters["save_pickle"] = os.path.join(
                root_workdir, "results", "fermi_surface.pickle")
        self._add_calculation_parameters(
                "plot_", plot_calculation_parameters, sequencer)

    def set_ibte(
            self, root_workdir=None, ibte_input_variables=None,
            ibte_calculation_parameters=None,
            plot_calculation_parameters=None):
        """Set the ibte calculation.

        Parameters
        ----------
        ibte_workdir: str
            The root directory where the ibte calculation will run and results
            will be stored.
        ibte_input_variables: dict
            The dictionary of the EPW input variables for the IBTE run.
        ibte_calculation_parameters: dict
            The dictionary of the EPW calculation parameters for the
            IBTE run.
        plot_calculation_parameters: dict, optional
            The plot parameters for the ibte plots.
        """
        sequencer = self.ibte_sequencer
        if not self.epw_interpolation_sequencer.sequence_completed:
            self.stop_at_workflow = "epw_interpolation"
            return
        if root_workdir is None:
            raise ValueError("Need to set 'root_workdir'.")
        sequencer.ibte_workdir = os.path.join(
                root_workdir, "ibte_run")
        sequencer.epw_interpolation_calculation = (
                self.epw_interpolation_sequencer.epw_workdir)
        if ibte_input_variables is None:
            raise ValueError("Need to set 'ibte_input_variables'.")
        sequencer.ibte_input_variables = ibte_input_variables
        self._add_calculation_parameters(
                "ibte_", ibte_calculation_parameters, sequencer)
        if plot_calculation_parameters is None:
            plot_calculation_parameters = {}
        plot_calculation_parameters["save"] = os.path.join(
                root_workdir, "results", "ibte.pdf")
        plot_calculation_parameters["save_pickle"] = os.path.join(
                root_workdir, "results", "ibte.pickle")
        self._add_calculation_parameters(
                "plot_", plot_calculation_parameters, sequencer)

    def set_ibte_with_thermal_expansion(
            self, ibte_workdir=None, ibte_input_variables=None,
            ibte_calculation_parameters=None, plot_parameters=None):
        """Set the ibte calculations that considers the lattice expansion.

        Parameters
        ----------
        ibte_workdir: str
            The root directory where the ibte calculations will run.
        ibte_input_variables: dict
            The dictionary of the EPW input variables for the IBTE runs.
        ibte_calculation_parameters: dict
            The dictionary of the EPW calculation parameters for the
            IBTE runs.
        plot_parameters: dict, optional
            The plot parameters for the ibte plots.
        """
        seqs = self.epw_interpolation_with_thermal_expansion_sequencer
        if not seqs.sequence_completed:
            self._logger.info(
                    "EPW with lattice expansion not completed. Nothing done "
                    "for IBTE with lattice expansion.")
            self.stop_at_workflow = "epw_interpolation_with_thermal_expansion"
            return
        results_dir = os.path.dirname(seqs[0].plot_save)
        if not os.path.exists(results_dir):
            # sequence finished but no results => run epw to get them
            self.run_epw_interpolation_with_thermal_expansion()
        if not os.path.exists(results_dir):
            # something happend don't do anything
            self._logger.info("Somthing happened for epw results do nothing.")
            return
        if ibte_workdir is None:
            raise ValueError("Need to set 'ibte_workdir'.")
        if ibte_input_variables is None:
            raise ValueError("Need to set 'ibte_input_variables'.")
        if ibte_calculation_parameters is None:
            raise ValueError("Need to set 'ibte_calculation_parameters'.")
        inp = ibte_input_variables.copy()
        if self._compute_lattice_expansion:
            temperatures = self.lattice_expansion_sequencer.temperatures
        else:
            # get temperatures from dirnames of epw sequencers
            temperatures = []
            # keep as strings for the dirname since we can be confused
            # with floats and integers
            for seq in self.epw_interpolation_with_thermal_expansion_sequencer:
                scf_dirname = os.path.basename(seq.scf_workdir)
                temperatures.append(scf_dirname.split("K")[0])
        for sequencer, epw_seq, temperature in zip(
                self.ibte_with_thermal_expansion_sequencer,
                self.epw_interpolation_with_thermal_expansion_sequencer,
                temperatures):
            sequencer.ibte_workdir = os.path.join(
                    ibte_workdir, "ibte_runs", f"{temperature}K")
            sequencer.epw_interpolation_calculation = epw_seq.epw_workdir
            # update temperature in input variables
            inp = ibte_input_variables.copy()
            # convert temperature to float from here
            inp.update({"temps": [float(temperature)], "nstemp": 1})
            sequencer.ibte_input_variables = inp
            self._add_calculation_parameters(
                    "ibte_", ibte_calculation_parameters, sequencer)
            sequencer.plot_save = os.path.join(
                    ibte_workdir, "results", f"ibte_{temperature}K.pdf")
            sequencer.plot_save_pickle = os.path.join(
                    ibte_workdir, "results", f"ibte_{temperature}K.pickle")
            self._add_calculation_parameters(
                    "plot_", plot_parameters, sequencer)

    def set_lattice_expansion(
            self,
            deltas_volumes=None,
            asr=None,
            temperatures=None,
            bulk_modulus_initial_guess=None,
            root_workdir=None,
            scf_input_variables=None,
            scf_calculation_parameters=None,
            phonons_input_variables=None,
            phonons_calculation_parameters=None,
            phonons_qpoint_grid=None,
            q2r_input_variables=None,
            q2r_calculation_parameters=None,
            matdyn_calculation_parameters=None,
            plot_calculation_parameters=None,
            use_relaxed_geometry=False):
        """Sets the ThermalExpansionSequencer.

        One needs to set scf, phonons, deltas_volumes. q2r, matdyn and
        plot attributes.

        Parameters
        ----------
        deltas_volumes: list-like
            The percentage of volumes of change to operate from
            the lattice parameter given in the scf_input_variables.
        asr: str
            The acoustic sum rule to fulfill in the matdyn calculation.
        temperatures: list-like
            The list of temperatures to compute the lattice expansion to.
        bulk_modulus_initial_guess: float
            Initial guess for the bulk modulus to make the fit work with the
            equation of state.
        root_workdir: str
            Root worikdir for the sequencer and all the calculations in it.
        scf_input_variables: dict, optional
            The scf input variables. This parameter can be None. In that
            case, the input variables from the SCFSequencer from the gs run
            will be used instead.
        scf_calculation_parameters: dict, optional
            The scf calculation parameters. This parameter can be None. In that
            case, the calculation parameters from the SCFSequencer
            from the gs run  will be used instead.
        phonons_input_variables: dict
            The phonons input variables.
        phonons_calculation_parameters:
            The phonons calculation parameters
        phonons_qpoint_grid: list
            The phonon qpoint grid.
        q2r_input_variables: dict
            The q2r input variables.
        q2r_calculation_parameters: dict
            The q2r calculation parameters.
        matdyn_calculation_parameters: dict
            The matdcyn calculation parameters.
        plot_calculation_parameters: dict, optional
            The plot parameters for the sequencer.
        use_relaxed_geometry: bool, optional
            If True, the relxed geometry will be used in the
            scf_input_variables and as the 'relaxed parameter' for benchmarks.

        N.B.: there are no matdyn input variables because they are
              automatically computed by the sequencer.
        """
        sequencer = self.lattice_expansion_sequencer
        if deltas_volumes is None:
            raise ValueError("Need to set 'deltas_volumes'.")
        sequencer.deltas_volumes = deltas_volumes
        if temperatures is None:
            raise ValueError("Need to set 'temperatures'.")
        sequencer.temperatures = temperatures
        if bulk_modulus_initial_guess is None:
            raise ValueError("Need to set 'bulk_modulus_initial_guess'.")
        sequencer.bulk_modulus_initial_guess = bulk_modulus_initial_guess

        # manage the scf part
        if root_workdir is None:
            raise ValueError("Need to set 'root_workdir'.")
        sequencer.scf_workdir = os.path.join(root_workdir, "scf_runs", "scf")
        if scf_input_variables is None or scf_calculation_parameters is None:
            # take the scf input variables from the scf sequencer if a
            # gs run is executed.
            if not self._compute_gs:
                if scf_input_variables is None:
                    raise ValueError("Need to set 'scf_input_variables'.")
                if scf_calculation_parameters is None:
                    raise ValueError(
                            "Need to set 'scf_calculation_parmeters'.")
            if self.gs_sequencer.scf_input_variables is None:
                raise ValueError(
                        "Need to setup gs_calculation if using these input "
                        "variables.")
            # use copy to make sure we don't overwrite by mistake the variables
            scf = self.gs_sequencer
            scf_input_variables = scf.scf_input_variables.copy()
            if scf_calculation_parameters is None:
                params = scf.scf_calculation_parameters.copy()
                scf_calculation_parameters = params
        else:
            self._use_converged_quantities(
                    scf_input_variables,
                    use_relaxed_geometry=use_relaxed_geometry)
        sequencer.scf_input_variables = scf_input_variables
        sequencer.relaxed_lattice_parameter = scf_input_variables["celldm(1)"]
        self._add_calculation_parameters("scf_", scf_calculation_parameters,
                                         sequencer)
        # manage the phonon part
        sequencer.phonons_workdir = os.path.join(
                root_workdir, "phonons_runs", "phonons")
        if phonons_qpoint_grid is None:
            raise ValueError("Need to set 'phonons_qpoint_grid'.")
        sequencer.phonons_qpoint_grid = phonons_qpoint_grid
        if phonons_input_variables is None:
            raise ValueError("Need to set 'phonons_input_variables'.")
        sequencer.phonons_input_variables = phonons_input_variables
        self._add_calculation_parameters(
                "phonons_", phonons_calculation_parameters, sequencer)

        # manage q2r part
        sequencer.q2r_workdir = os.path.join(root_workdir, "q2r_runs", "q2r")
        if q2r_input_variables is None:
            raise ValueError("Need to set 'q2r_input_variables'.")
        sequencer.q2r_input_variables = q2r_input_variables
        self._add_calculation_parameters(
                "q2r_", q2r_calculation_parameters, sequencer)

        # manage matdyn part
        sequencer.matdyn_workdir = os.path.join(
                root_workdir, "matdyn_runs", "matdyn")
        sequencer.asr = asr
        self._add_calculation_parameters(
                "matdyn_", matdyn_calculation_parameters, sequencer)

        # manage plot parameters
        if plot_calculation_parameters is None:
            plot_calculation_parameters = {}
        plot_calculation_parameters["save"] = os.path.join(
                root_workdir, "results", "lattice_expansion.pdf")
        plot_calculation_parameters["save_pickle"] = os.path.join(
                root_workdir, "results", "lattice_expansion.pickle")
        self._add_calculation_parameters(
                "plot_", plot_calculation_parameters, sequencer)

    def set_phonon_dispersion(
            self, *args,
            root_workdir=None,
            q2r_calculation_parameters=None,
            q2r_input_variables=None,
            matdyn_calculation_parameters=None,
            matdyn_input_variables=None,
            **kwargs,
            ):
        """Sets the phonon dispersion part of the workflow.

        Parameters
        ----------
        q2r_calculation_parameters: dict
            The dict of q2r calculation parameters.
        q2r_input_variables: dict
            The dict of q2r input variables.
        matdyn_calculation_parameters: dict
            The dict of matdyn calculation parameters.
        matdyn_input_variables: dict
            The dict of matdyn input variables.

        Other parameters are passed to the mother's class method.
        """
        sequencer = self.phonon_dispersion_sequencer
        super().set_phonon_dispersion(
                *args, root_workdir=root_workdir, **kwargs)
        # Q2R
        sequencer.q2r_workdir = os.path.join(root_workdir, "q2r_run")
        if q2r_input_variables is None:
            raise ValueError("Need to set 'q2r_input_variables'.")
        sequencer.q2r_input_variables = q2r_input_variables
        self._add_calculation_parameters(
                "q2r_", q2r_calculation_parameters, sequencer)
        # MATDYN
        sequencer.matdyn_workdir = os.path.join(root_workdir, "matdyn_run")
        if matdyn_input_variables is None:
            raise ValueError("Need to set 'matdyn_input_variables'.")
        sequencer.matdyn_input_variables = matdyn_input_variables
        self._add_calculation_parameters(
                "matdyn_", matdyn_calculation_parameters, sequencer)

    def set_phonon_dispersion_qgrid_convergence(
            self, root_workdir=None,
            scf_calculation_parameters=None,
            scf_input_variables=None,
            phonons_calculation_parameters=None,
            phonons_input_variables=None,
            phonons_qpoint_grids=None,
            q2r_calculation_parameters=None,
            q2r_input_variables=None,
            matdyn_calculation_parameters=None,
            matdyn_input_variables=None,
            qpoint_path=None,
            qpoint_path_density=None,
            plot_calculation_parameters=None,
            use_gs=False,
            use_phonon_converged_ecut=False,
            use_phonon_converged_smearing=False,
            use_relaxed_geometry=False,
            ):
        """Sets the phonon dispersion qpoint grid convergence part of the
        workflow. Launches a bunch of phonon dispersion sequencers
        to test out many qpoint grids.

        Parameters
        ----------
        root_workdir: str
            The root workdir where the calculations will run.
        scf_calculation_parameters: dict, optional
            The dict of scf calculation parameters.
        scf_input_variables: dict, optional
            The dict of scf input variables.
        phonons_calculation_parameters: dict
            The dict of phonons calculation parameters.
        phonons_input_variables: dict
            The dict of phonons input variables.
        phonons_qpoint_grids: list
            List of qpoint grids to test.
        q2r_calculation_parameters: dict
            The dict of q2r calculation parameters.
        q2r_input_variables: dict
            The dict of q2r input variables.
        matdyn_calculation_parameters: dict
            The dict of matdyn calculation parameters.
        matdyn_input_variables: dict
            The dict of matdyn input variables.
        qpoint_path: list
            The list of qpoints that forms the phonon dispersion.
        qpoint_path_density: int
            The number of qpoints between the points given in the qpoint path.
        plot_calculation_parameters: dict, optional
            The dict of plot parameters.
        use_phonon_converged_ecut: bool, optional
            If True, the phonon converged ecut will be used. The
            'phonon_ecut_convergence' part of the workflow must be activated
            or the 'converged_phonon_ecut' must be set upon init.
        use_phonon_converged_smearing: bool, optional
            If True, the phonon converged smearing will be used. The
            'phonon_smearing_convergence' part of the workflow must be
            activated or the 'converged_phonon_smearing' must be set upon init.
        use_gs: bool, optional
            If True, the 'gs' part of the workflow is used. This option is
            incompatible with 'use_*' flags and will thus override them.
        use_relaxed_geometry: bool, optional
            If True, the relaxed geometry will be used. The 'relaxation' part
            of the workflow must be activated or the 'relaxed_geometry'
            attribute must be set upon init.
        """
        # need to add Sequencers here
        if phonons_qpoint_grids is None:
            raise ValueError("Need to set 'phonons_qpoint_grids'.")
        if not is_list_like(phonons_qpoint_grids):
            raise ValueError(
                    "'phonons_qpoint_grids' must be a list of qpoint grids.")
        if phonons_input_variables is None:
            raise ValueError("Need to set 'phonons_input_variables'.")
        if q2r_input_variables is None:
            raise ValueError("Need to set 'q2r_input_variables'.")
        if qpoint_path is None:
            raise ValueError("Need to set 'qpoint_path'.")
        if qpoint_path_density is None:
            raise ValueError("Need to set 'qpoint_path_density'.")
        if matdyn_input_variables is None:
            raise ValueError("Need to set 'matdyn_input_variables'.")
        for phonons_qpoint_grid in phonons_qpoint_grids:
            sequencer = QEPhononDispersionSequencer(loglevel=self._loglevel)
            # SCF
            subroot_workdir = os.path.join(
                    root_workdir, "qgrid_" + "_".join(
                        [str(x) for x in phonons_qpoint_grid]))
            self._use_converged_calculations(
                    sequencer, use_gs=use_gs,
                    scf_input_variables=scf_input_variables,
                    scf_calculation_parameters=scf_calculation_parameters,
                    scf_workdir=os.path.join(subroot_workdir, "scf_run"),
                    use_phonon_converged_ecut=use_phonon_converged_ecut,
                    use_phonon_converged_smearing=(
                        use_phonon_converged_smearing),
                    use_relaxed_geometry=use_relaxed_geometry)
            # PHONONS
            sequencer.phonons_workdir = os.path.join(
                    subroot_workdir, "phonons_runs", "ph_q")
            sequencer.phonons_qpoint_grid = phonons_qpoint_grid
            # copy very important here since setting input vars will modify
            # the dict (internal sequencers setting methods)"
            sequencer.phonons_input_variables = phonons_input_variables.copy()
            self._add_calculation_parameters(
                    "phonons_", phonons_calculation_parameters, sequencer)
            # Q2R
            sequencer.q2r_workdir = os.path.join(subroot_workdir, "q2r_run")
            sequencer.q2r_input_variables = q2r_input_variables
            self._add_calculation_parameters(
                    "q2r_", q2r_calculation_parameters, sequencer)
            # MATDYN
            sequencer.matdyn_workdir = os.path.join(
                    subroot_workdir, "matdyn_run")
            sequencer.qpoint_path = qpoint_path
            sequencer.qpoint_path_density = qpoint_path_density
            sequencer.matdyn_input_variables = matdyn_input_variables
            self._add_calculation_parameters(
                    "matdyn_", matdyn_calculation_parameters, sequencer)
            # PLOT
            if plot_calculation_parameters is None:
                plot_calculation_parameters = {}
            plot_calculation_parameters["save"] = os.path.join(
                    subroot_workdir, "results", "phonon_dispersion.pdf")
            plot_calculation_parameters["save_pickle"] = os.path.join(
                    subroot_workdir, "results", "phonon_dispersion.pickle")
            self._add_calculation_parameters(
                    "plot_", plot_calculation_parameters, sequencer)
            self.phonon_dispersion_qgrid_convergence_sequencer.append(
                    sequencer)

    def set_relaxation(
            self, *args,
            maximum_relaxations=None, **kwargs):
        """Sets the relaxation part of the workflow.

        Parameters
        ----------
        maximum_relaxations: int
            Maximum number cell relaxations to do in case relax_cell is set
            to True.
        other args and kwargs are passed to the mother class' method with the
        same name.
        """
        super().set_relaxation(*args, **kwargs)
        if maximum_relaxations is None:
            if self.relaxation_sequencer.relax_cell:
                raise ValueError("Need to set 'maximum_relaxations'.")
            maximum_relaxations = 0  # dummy
        self.relaxation_sequencer.maximum_relaxations = maximum_relaxations

    def write_epw_interpolation(self):
        self.epw_interpolation_sequencer.write()

    def write_epw_interpolation_with_thermal_expansion(self):
        if self._compute_lattice_expansion:
            if not self.lattice_expansion_sequencer.sequence_completed:
                return
        self.epw_interpolation_with_thermal_expansion_sequencer.write()

    def write_fermi_surface(self):
        self.fermi_surface_sequencer.write()

    def write_ibte(self):
        self.ibte_sequencer.write()

    def write_ibte_with_thermal_expansion(self):
        if self._compute_lattice_expansion:
            if not self.lattice_expansion_sequencer.sequence_completed:
                return
        seqs = self.epw_interpolation_with_thermal_expansion_sequencer
        if not seqs.sequence_completed:
            return
        self.ibte_with_thermal_expansion_sequencer.write()

    def write_lattice_expansion(self):
        """Writes the lattice expansion sequencer.
        """
        deltas = self.lattice_expansion_sequencer.deltas_volumes
        zero = 0 in deltas or 0.0 in deltas
        completed = self.gs_sequencer.sequence_completed
        if self._compute_gs and not completed and zero:
            # wait for gs
            return
        self.lattice_expansion_sequencer.write()

    def write_phonon_dispersion_qgrid_convergence(self):
        self.phonon_dispersion_qgrid_convergence_sequencer.write()

    def _plot_final_results_epw_interpolation_with_thermal_expansion(self):
        """Plot final results for the epw with thermal expansion calcs.

        Basically makes a multiplot with all interpolations to easily see
        results.
        """
        final_plot_bs = MultiPlot()
        final_plot_bs.title = "Band Structures"
        final_plot_ph = MultiPlot()
        final_plot_ph.title = "Phonon dispersions"
        ncolumns = 2
        # the final plots will be ncolumns max.
        row_ph = 0
        row_bs = 0
        seqs = self.epw_interpolation_with_thermal_expansion_sequencer
        for sequencer in seqs:
            # we need to get ph path and bs path
            results_dir = os.path.dirname(sequencer.plot_save)
            temperature = os.path.basename(results_dir).split("K")[0]
            for filename in os.listdir(results_dir):
                if not filename.endswith(".pickle"):
                    continue
                plot = Plot.load_plot(os.path.join(results_dir, filename))
                plot.title = f"{temperature}K"
                if "phonon_dispersion" in filename:
                    final_plot_ph.add_plot(plot, row=row_ph // ncolumns)
                    row_ph += 1
                elif "band_structure" in filename:
                    final_plot_bs.add_plot(plot, row=row_bs // ncolumns)
                    row_bs += 1
        final_plot_bs.plot(
                show=sequencer.plot_calculation_parameters.get("show", True),
                show_legend_on=[[0, 0]],
                )
        final_plot_ph.plot(
                show=sequencer.plot_calculation_parameters.get("show", True),
                show_legend_on=[[0, 0]],
                )
        # save plot in the dirname of the dirname
        dirname = os.path.dirname(os.path.dirname(
            sequencer.plot_save_pickle))
        nameph = "final_results_ph_disp_with_wannier_interpolation.pdf"
        namebs = "final_results_bs_with_wannier_interpolation.pdf"
        final_plot_ph.save(os.path.join(dirname, nameph))
        final_plot_bs.save(os.path.join(dirname, namebs))

    def _plot_final_results_ibte_with_thermal_expansion(self, show):
        conductivity_tensor = ConductivityTensor()
        conductivity_tensor.conductivity_tensor = []
        conductivity_tensor.serta_conductivity_tensor = []
        conductivity_tensor.temperatures = []
        for sequencer in self.ibte_with_thermal_expansion_sequencer:
            # get temperatures from directory names
            temperature = float(
                    os.path.basename(sequencer.ibte_workdir).split("K")[0])
            with CalculationDirectory.from_calculation(
                    sequencer.ibte_workdir) as calc:
                logpath = calc.log_file.path
            this_cond = ConductivityTensor.from_file(logpath)
            conductivity_tensor.conductivity_tensor.append(
                    this_cond.conductivity_tensor[0])
            conductivity_tensor.serta_conductivity_tensor.append(
                    this_cond.serta_conductivity_tensor[0])
            conductivity_tensor.temperatures.append(temperature)
        conductivity_tensor.conductivity_tensor = np.array(
                conductivity_tensor.conductivity_tensor)
        conductivity_tensor.serta_conductivity_tensor = np.array(
                conductivity_tensor.serta_conductivity_tensor)
        elements = sequencer.plot_calculation_parameters.get("elements", None)
        dirsave = os.path.dirname(sequencer.plot_save)
        for serta in [True, False]:
            for resistivity, factor, unit, title, name in zip(
                    [True, False], [1e6, 1e-6],
                    [r"$\mu\Omega$cm", r"$\mu\Omega$cm$^{-1}$"],
                    ["Resistivity tensor", "Conductivity tensor"],
                    ["resistivity", "conductivity"]):
                if serta:
                    title += " (SERTA)"
                    name += "_serta"
                else:
                    title += " (IBTE)"
                    name += "_ibte"
                plot = conductivity_tensor.get_plot(
                        resistivity=resistivity, conversion_factor=factor,
                        xunits="K", yunits=unit, linestyle="-", title=title,
                        linewidth=2, elements=elements)
                plot.plot(show=show)
                plot.save(os.path.join(dirsave, name + ".pdf"), overwrite=True)
                plot.save_plot(os.path.join(dirsave, name + ".pickle"),
                               overwrite=True)
