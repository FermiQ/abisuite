import os

from .bases import BaseWorkflow, SequencersList
from ..constants import EV_TO_HARTREE
from ..launchers.launchers.abinit_launchers.abinit_launcher import (
        __ALL_ABINIT_GEOMETRY_VARIABLES__,
        __ABINIT_ONE_FROM_THESE_GEOMETRY_VARIABLES__,
        )
from ..routines import is_list_like
from ..sequencers import (
        AbinitBandStructureSequencer, AbinitBandStructureComparatorSequencer,
        AbinitEcutConvergenceSequencer, AbinitEcutPhononConvergenceSequencer,
        AbinitKgridConvergenceSequencer, AbinitPhononDispersionSequencer,
        AbinitSmearingConvergenceSequencer,
        AbinitSmearingPhononConvergenceSequencer,
        AbinitSCFSequencer,
        AbinitOpticSequencer, AbinitRelaxationSequencer,
        )


def is_geometry_defined(input_vars):
    """Returns True if geometry is fully defined in this dict of
    input variables.
    """
    for var in __ALL_ABINIT_GEOMETRY_VARIABLES__:
        if any([var in sublist
                for sublist in __ABINIT_ONE_FROM_THESE_GEOMETRY_VARIABLES__]):
            # variable in a sublist for 'one_of_them'. deal with it later
            continue
        if var not in input_vars:
            return False
    for sublist in __ABINIT_ONE_FROM_THESE_GEOMETRY_VARIABLES__:
        npresent = 0
        for var in sublist:
            if var in input_vars:
                npresent += 1
        if npresent != 1:
            return False
    return True


class AbinitWorkflow(BaseWorkflow):
    """Workflow for Abinit.

    Workflow order:
        - band_structure convergence / comparison
        - optic
        - ++ all other workflows implemented in base class (see BaseWorkflow).
    """
    _all_geometry_variables = __ALL_ABINIT_GEOMETRY_VARIABLES__
    _band_structure_sequencer_cls = AbinitBandStructureSequencer
    _loggername = "AbinitWorkflow"
    _gs_sequencer_cls = AbinitSCFSequencer
    _gs_ecut_convergence_sequencer_cls = AbinitEcutConvergenceSequencer
    _gs_kgrid_convergence_sequencer_cls = AbinitKgridConvergenceSequencer
    _gs_smearing_convergence_sequencer_cls = AbinitSmearingConvergenceSequencer
    _phonon_ecut_convergence_sequencer_cls = (
            AbinitEcutPhononConvergenceSequencer)
    _phonon_dispersion_sequencer_cls = AbinitPhononDispersionSequencer
    _phonon_smearing_convergence_sequencer_cls = (
            AbinitSmearingPhononConvergenceSequencer)
    _relaxation_sequencer_cls = AbinitRelaxationSequencer
    workflow = (
            "gs_ecut_convergence", "gs_kgrid_convergence",
            "gs_smearing_convergence", "relaxation",
            "phonon_ecut_convergence", "phonon_smearing_convergence",
            "gs", "band_structure", "phonon_dispersion",
            "band_structure_convergence", "optic")

    def __init__(
            self, *args,
            band_structure_convergence=False,
            optic=False, pseudos=None,
            **kwargs):
        """AbinitWorkflow init method.

        Parameters
        ----------
        band_structure_convergence: bool, optional
            If True, will compute a band structure convergence
            with respect a given input variable parameter.
        optic: bool, optional
            If True, will compute the optic response of the meterial.
        pseudos: list-like
            The list of pseudos to use throughout the workflow.
        """
        super().__init__(*args, **kwargs)
        # BAND STRUCTURE CONVERGENCE PART
        self._compute_band_structure_convergence = band_structure_convergence
        self._band_structure_convergence_sequencer = None
        # OPTIC PART
        self._compute_optic = optic
        self._optic_sequencer = None
        if not is_list_like(pseudos):
            raise TypeError("Pseudos should be set as a list.")
        self.pseudos = pseudos

    @property
    def band_structure_convergence_sequencer(self):
        if self._band_structure_convergence_sequencer is not None:
            return self._band_structure_convergence_sequencer
        bs = AbinitBandStructureComparatorSequencer(loglevel=self._loglevel)
        self._band_structure_convergence_sequencer = bs
        return self.band_structure_convergence_sequencer

    @property
    def optic_sequencer(self):
        if self._optic_sequencer is not None:
            return self._optic_sequencer
        self._optic_sequencer = AbinitOpticSequencer(loglevel=self._loglevel)
        return self.optic_sequencer

    def run_band_structure(self):
        if self._compute_optic:
            # optic can use the bandgap if requested
            # it needs to run the sequencer
            # don't rerun if already run
            if self.band_structure_sequencer.has_been_run:
                return
        super().run_band_structure()

    def run_band_structure_convergence(self):
        self.band_structure_convergence_sequencer.run()

    def run_optic(self):
        self.optic_sequencer.run()

    def set_band_structure(
            self, fatbands=False,
            band_structure_input_variables=None,
            **kwargs):
        """Sets the BandStructureSequencer

        Parameters
        ----------
        band_structure_input_variables: dict
            The dict of the band structure input variables.
        fatbands: bool, optional
            If True, the orbital projections will be computed in order to draw
            the fatbands structure.
        all kwargs are passed to the mother class method of the same name.
        """
        if band_structure_input_variables is None:
            raise ValueError("Need to set 'band_structure_input_variables'.")
        if fatbands:
            if "pawecutdg" in band_structure_input_variables:
                # paw calc, use pawfatbnd
                band_structure_input_variables["pawfatbnd"] = 2
            else:
                band_structure_input_variables["prtprocar"] = 1
                band_structure_input_variables["prtdos"] = 3
        super().set_band_structure(
                band_structure_input_variables=band_structure_input_variables,
                **kwargs)

    def set_band_structure_convergence(
            self, root_workdir=None,
            scf_input_variables=None,
            scf_calculation_parameters=None,
            scf_specific_input_variables=None,
            band_structure_input_variables=None,
            band_structure_calculation_parameters=None,
            band_structure_kpoint_path=None,
            band_structure_kpoint_path_density=None,
            use_gs_converged_ecut=False,
            use_gs_converged_kgrid=False,
            use_relaxed_geometry=False,
            plot_calculation_parameters=None):
        """Sets the BandStructureComparatorSequencer

        Parameters
        ----------
        root_workdir: str
            The root workdir for the band structure sequence.
        scf_input_variables: dict, optional
            The scf input variables.
        scf_calculation_parameters: dict
            The scf calculation parameters.
        scf_specific_input_variables: dict, optional
            The specific scf input variables that differ for each scf calc.
        band_structure_input_variables: dict, optional
            The dict of the band structure input variables.
        band_structure_calculation_parameters: dict, optional
            The dict of the calculation parameters for the band structure
            calculation.
        band_structure_kpoint_path: list, optional
            The list of kpoints that forms the band structure.
        band_structure_kpoint_path_density: int, optional
            The number of points between kpoints in the list of
            the kpoints that forms the band structure.
        use_gs_converged_ecut: bool, optional
            If True, will use the converged ecut from the convergence study
            for the scf calculations.
            'gs_ecut_convergence' must be set to True at init.
        use_gs_converged_kgrid: bool, optional
            If True, will use the converged kgrid from the convergence study
            for the scf calculations.
            'gs_kgrid_convergence' must be set to True at init.
        use_relaxed_geometry: bool, optional
            If True, will use the relaxed geometry as computed by the
            relaxation run for the scf calculations.
            'relaxation' must be set to True at init.
        plot_calculation_parameters: dict, optional
            The dict of plot parameters.
        """
        bsseq = self.band_structure_convergence_sequencer
        if root_workdir is None:
            raise ValueError("Need to set 'root_workdir'.")
        # SCF PART
        bsseq.scf_workdir = os.path.join(
                root_workdir, "scf_runs", "scf")
        self._use_converged_quantities(
                scf_input_variables,
                use_gs_converged_ecut=use_gs_converged_ecut,
                use_gs_converged_kgrid=use_gs_converged_kgrid,
                use_relaxed_geometry=use_relaxed_geometry,
                )
        bsseq.scf_input_variables = scf_input_variables
        self._add_calculation_parameters(
                "scf_", scf_calculation_parameters, bsseq)
        if scf_specific_input_variables is None:
            raise ValueError(
                    "Need to set 'scf_specific_input_variables'.")
        bsseq.scf_specific_input_variables = scf_specific_input_variables

        # BAND STRUCTURE PART
        bsseq.band_structure_workdir = os.path.join(
                root_workdir, "band_structure_runs", "band_structure")
        if band_structure_kpoint_path is None:
            raise ValueError(
                    "Need to set 'band_structure_kpoint_path'.")
        bsseq.band_structure_kpoint_path = band_structure_kpoint_path
        if band_structure_kpoint_path_density is None:
            raise ValueError(
                    "Need to set 'band_structure_kpoint_path_density'.")
        bskpd = band_structure_kpoint_path_density
        bsseq.band_structure_kpoint_path_density = bskpd
        if band_structure_input_variables is None:
            raise ValueError("Need to set 'band_structure_input_variables'.")
        bsseq.band_structure_input_variables = band_structure_input_variables
        self._add_calculation_parameters(
                "band_structure_",
                band_structure_calculation_parameters, bsseq)
        # PLOTTING PART
        if plot_calculation_parameters is None:
            plot_calculation_parameters = {}
        plot_calculation_parameters["title"] = "Band Structure"
        plot_calculation_parameters["save"] = os.path.join(
                root_workdir, "results", "band_structure.pdf")
        plot_calculation_parameters["save_pickle"] = os.path.join(
                root_workdir, "results", "band_structure.pickle")
        self._add_calculation_parameters(
                "plot_", plot_calculation_parameters, bsseq)

    def set_gs_ecut_convergence(
            self, *args,
            scf_pawecutdg=None,
            scf_pawecutdg_convergence_criterion=None,
            **kwargs):
        """Sets the gs_ecut_convergence workflow.

        Parameters
        ----------
        scf_calculation_parameters: dict, optional
            The scf calculation parameters.
        scf_pawecutdg_convergence_criterion: float, optional
            The convergence criterion for the pawecutdg convergence test
            in meV/at.
        scf_pawecutdg: list-like, optional
            The list of pawecutdg to use for the second convergence test.
        plot_parameters: dict, optional
            The dict of plot parameters.
        """
        # start with the ecut convergence sequencer
        # do ecutdg afterwards in case of PAW
        seq1 = self.gs_ecut_convergence_sequencer
        seq1.ecuts_input_variable_name = "ecut"
        super().set_gs_ecut_convergence(*args, **kwargs)
        # create another sequencer and make a list of sequencers
        # so that all convergence sequencers are executed
        if not self._is_paw_calculation(seq1.scf_input_variables):
            return
        if not seq1.sequence_completed:
            return
        list_ = SequencersList([seq1])
        # calc is paw, add another convergence calculation for pawecutdg
        if scf_pawecutdg is None:
            raise ValueError("Need to set 'scf_pawecutdg'.")
        # make sure pawecutdg is present (test case for ecut) in first seq
        if "pawecutdg" not in seq1.scf_input_variables:
            raise ValueError(
                    "'pawecutdg' must be present as a test case for the "
                    "'ecut' convergence.")
        # pawecutdg must be > than all ecut checked
        if seq1.scf_input_variables["pawecutdg"] < max(seq1.scf_ecuts):
            raise ValueError("'pawecutdg' must be > than all ecuts checked.")
        seq2 = AbinitEcutConvergenceSequencer(loglevel=self._loglevel)
        seq2.ecuts_input_variable_name = "pawecutdg"
        # only consider pawecutdg that are higher than the converged ecut
        converged_ecut = seq1.scf_converged_ecut
        scf_pawecutdg = ([converged_ecut] +
                         [x for x in scf_pawecutdg if x > converged_ecut])
        if not len(scf_pawecutdg):
            raise ValueError(
                    "Converged ecut is higher than all proposed pawecutdg.")
        pawecutdg_vars = seq1.scf_input_variables.copy()
        self._use_converged_quantities(
                pawecutdg_vars, use_gs_converged_ecut=True)
        super().set_gs_ecut_convergence(
                root_workdir=os.path.dirname(seq1.scf_workdir),
                scf_calculation_parameters=seq1.scf_calculation_parameters,
                scf_ecuts=scf_pawecutdg,
                scf_convergence_criterion=scf_pawecutdg_convergence_criterion,
                scf_input_variables=pawecutdg_vars,
                _sequencer=seq2,
                plot_calculation_parameters=(
                    seq1.plot_calculation_parameters.copy()))
        list_.append(seq2)
        self._gs_ecut_convergence_sequencer = list_

    def set_gs_kgrid_convergence(
            self, *args, scf_kgrids_input_variable_name=None, **kwargs):
        """Sets the gs_kgrid_convergence part of the workflow.

        Parameters
        ----------
        scf_kgrids_input_variable_name: str
            The kgrid input variable name to test.
        All other kwargs and args are passed to the mother's class method with
        the same name.
        """
        sequencer = self.gs_kgrid_convergence_sequencer
        if scf_kgrids_input_variable_name is None:
            raise ValueError(
                    "Need to set 'scf_kgrids_input_variable_name'.")
        sequencer.kgrids_input_variable_name = scf_kgrids_input_variable_name
        super().set_gs_kgrid_convergence(*args, **kwargs)

    def set_gs_smearing_convergence(
            self, *args, scf_kgrids_input_variable_name=None, **kwargs):
        """Sets the gs_smearing_convergence part of the workflow.

        Parameters
        ----------
        scf_kgrids_input_variable_name: str
            The kgrid input variable name to test.
        All other kwargs and args are passed to the mother's class method with
        the same name.
        """
        sequencer = self.gs_smearing_convergence_sequencer
        if scf_kgrids_input_variable_name is None:
            raise ValueError(
                    "Need to set 'scf_kgrids_input_variable_name'.")
        sequencer.kgrids_input_variable_name = scf_kgrids_input_variable_name
        super().set_gs_smearing_convergence(*args, **kwargs)

    def set_optic(
            self, root_workdir=None, experimental_bandgap=None,
            compute_non_linear_optical_response=None,
            scf_input_variables=None,
            scf_calculation_parameters=None, nscf_input_variables=None,
            nscf_calculation_parameters=None,
            nscffbz_calculation_parameters=None,
            ddk_input_variables=None,
            ddk_calculation_parameters=None,
            optic_input_variables=None, optic_calculation_parameters=None,
            optic_force_kptopt=False,
            use_gs=False,
            use_gs_converged_ecut=False,
            use_gs_converged_kgrid=False,
            use_relaxed_geometry=False,
            plot_calculation_parameters=None):
        """Sets the optic calculations sequencer.

        Parameters
        ----------
        root_workdir: str
            The workdir where the sequencer will operate.
        experimental_bandgap: float, optional
            The experimental band gap in eV. If given, the scissor shift in
            optic will be automaticallyu computed if the band structure is
            computed.
        compute_non_linear_optical_response: bool
            If True, the non linear optical response will be computed.
        scf_input_variables: dict, optional
            The scf input variables. If None, the ones from the GS calculation
            will be used.
        scf_calculation_parameters: dict, optional
            The scf calculation parameters. If None, the ones from the GS
            calculation will be used.
        nscf_input_variables: dict
            The dict of nscf input variables.
        nscf_calculation_parameters: dict
            The dict of nscf calculation parameters.
        nscffbz_calculation_parameters: dict
            The dict of calculation parameters for the fbz nscf run.
        ddk_input_variables: dict
            The dict of ddk input variables.
        ddk_calculation_parameters: dict
            The dict of ddk calculation parameters.
        optic_input_variables: dict
            The dict of optic input variables.
        optic_calculation_parameters: dict
            The dict of optic calculation parameters.
        optic_force_kptopt: bool, optional
            If False (default), kptopt is automatically set if wrongly set.
            Otherwise, it is set only if it is not defined. Useful to
            test kpt generation using something else than regular grids.
        use_gs: bool, optional
            If True, the gs part of the workflow is used.
        use_gs_converged_ecut: bool, optional
            If True, will use the converged ecut from the convergence study.
            'gs_ecut_convergence' must be set to True at init.
        use_gs_converged_kgrid: bool, optional
            If True, will use the converged kgrid from the convergence study.
            'gs_kgrid_convergence' must be set to True at init.
        use_relaxed_geometry: bool, optional
            If True, will use the relaxed geometry as computed by the
            relaxation run. 'relaxation' must be set to True at init.
        plot_calculation_parameters: dict, optional
            The dict of plot parameters.
        """
        if root_workdir is None:
            raise ValueError("Need to set 'root_workdir'.")
        optic_seq = self.optic_sequencer
        if compute_non_linear_optical_response is None:
            raise ValueError(
                    "Need to set 'compute_non_linear_optical_response'.")
        cnlor = compute_non_linear_optical_response
        optic_seq.compute_non_linear_optical_response = cnlor
        optic_seq.force_kptopt = optic_force_kptopt
        # SCF PART
        self._use_converged_calculations(
                optic_seq, use_gs=use_gs,
                use_gs_converged_ecut=use_gs_converged_ecut,
                use_gs_converged_kgrid=use_gs_converged_kgrid,
                use_relaxed_geometry=use_relaxed_geometry,
                scf_input_variables=scf_input_variables,
                scf_calculation_parameters=scf_calculation_parameters,
                scf_workdir=os.path.join(root_workdir, "scf_run"))
        optic_seq.nscf_workdir = os.path.join(root_workdir, "nscf_run")
        if nscf_input_variables is None:
            raise ValueError("Need to set 'nscf_input_variables'.")
        optic_seq.nscf_input_variables = nscf_input_variables
        if nscf_calculation_parameters is None:
            raise ValueError("Need to set 'nscf_calculation_parameters'.")
        self._add_calculation_parameters(
                "nscf_", nscf_calculation_parameters, optic_seq)
        # nscf_fbz part
        optic_seq.nscffbz_workdir = os.path.join(root_workdir, "nscf_fbz_run")
        if nscffbz_calculation_parameters is None:
            raise ValueError("Need to set 'nscffbz_calculation_parameters'.")
        self._add_calculation_parameters(
                "nscffbz_", nscffbz_calculation_parameters, optic_seq)
        # ddk part
        optic_seq.ddk_workdir = os.path.join(root_workdir, "ddk_run")
        if ddk_input_variables is None:
            raise ValueError("Need to set 'ddk_input_variables'.")
        optic_seq.ddk_input_variables = ddk_input_variables
        if ddk_calculation_parameters is None:
            raise ValueError("Need to set 'ddk_calculation_parameters'.")
        self._add_calculation_parameters(
                "ddk_", ddk_calculation_parameters, optic_seq)
        # optic part
        optic_seq.optic_workdir = os.path.join(root_workdir, "optic_run")
        if optic_input_variables is None:
            raise ValueError("Need to set 'optic_input_variables'.")
        optic_seq.optic_input_variables = optic_input_variables
        if experimental_bandgap is not None:
            if not self._compute_band_structure:
                raise ValueError(
                        "Need to compute band structure in order to compute "
                        "scissor shift.")
            if not self.band_structure_sequencer.sequence_completed:
                self.stop_at_workflow = "band_structure"
                return
            computed_bandgap = self.band_structure_sequencer.bandgap
            scissor = experimental_bandgap - computed_bandgap
            scissor *= EV_TO_HARTREE
            optic_seq.optic_input_variables["scissor"] = scissor
        if optic_calculation_parameters is None:
            raise ValueError("Need to set 'optic_calculation_parameters'.")
        self._add_calculation_parameters(
                "optic_", optic_calculation_parameters, optic_seq)
        if plot_calculation_parameters is None:
            plot_calculation_parameters = {}
        plot_calculation_parameters["save"] = os.path.join(
                root_workdir, "results", "optic.pdf")
        plot_calculation_parameters["save_pickle"] = os.path.join(
                root_workdir, "results", "optic.pickle")
        plot_calculation_parameters["legend"] = True
        plot_calculation_parameters["legend_outside"] = True
        plot_calculation_parameters["show_legend_on"] = [[0, 0]]
        self._add_calculation_parameters(
                "plot_", plot_calculation_parameters, optic_seq)

    def set_phonon_ecut_convergence(
            self, *args,
            compute_electric_field_response=None,
            ddk_input_variables=None,
            ddk_calculation_parameters=None,
            phonons_pawecutdg_convergence_criterion=None,
            root_workdir=None,
            scf_pawecutdg=None,
            **kwargs):
        """Sets the phonon_ecut_convergence workflow.

        Parameters
        ----------
        compute_electric_field_response: bool
            If True, the electric field response (for LO-TO splitting) will be
            computed (Irrelevent for metals sor set to False).
        ddk_input_variables: dict, optional
            In the case where 'compute_electric_field_response' is True,
            this specifies the dict of ddk input variables.
        ddk_calculation_parameters: dict, optional
            In the case where 'compute_electric_field_response' is True,
            this specifies the dict of ddk calculation_parameters.
        phonons_pawecutdg_convergence_criterion: float, optional
            The convergence criterion for the pawecutdg convergence test
            in cm-1.
        root_workdir: str
            The root workdir where all calculations will be run.
        scf_pawecutdg: list-like, optional
            The list of pawecutdg to use for the second convergence test.
        Other args and kwargs are passed to the base class method.
        """
        if compute_electric_field_response is None:
            raise ValueError("Need to set 'compute_electric_field_response'.")
        # start with the ecut convergence sequencer
        # do ecutdg afterwards in case of PAW
        seq1 = self.phonon_ecut_convergence_sequencer
        seq1.ecuts_input_variable_name = "ecut"
        seq1.compute_electric_field_response = compute_electric_field_response
        super().set_phonon_ecut_convergence(
                *args, root_workdir=root_workdir, **kwargs)
        if compute_electric_field_response:
            seq1.ddk_workdir = os.path.join(root_workdir, "ddk_runs")
            if ddk_input_variables is None:
                raise ValueError("Need to set 'ddk_input_variables'.")
            seq1.ddk_input_variables = ddk_input_variables
            if ddk_calculation_parameters is None:
                raise ValueError("Need to set 'ddk_calculation_parameters'.")
            self._add_calculation_parameters(
                    "ddk_", ddk_calculation_parameters, seq1)
        # create another sequencer and make a list of sequencers
        # so that all convergence sequencers are executed
        if not self._is_paw_calculation(seq1.scf_input_variables):
            return
        if not seq1.sequence_completed:
            # if first calc not finished, wait for it
            return
        list_ = SequencersList([seq1])
        # calc is paw, add another convergence calculation for pawecutdg
        if scf_pawecutdg is None:
            raise ValueError("Need to set 'scf_pawecutdg'.")
        # make sure pawecutdg is present (test case for ecut) in first seq
        if "pawecutdg" not in seq1.scf_input_variables:
            raise ValueError(
                    "'pawecutdg' must be present as a test case for the "
                    "'ecut' convergence.")
        if phonons_pawecutdg_convergence_criterion is None:
            raise ValueError(
                    "Must set 'phonons_pawecutdg_convergence_criterion'."
                    )
        # pawecutdg must be > than all ecut checked
        if seq1.scf_input_variables["pawecutdg"] < max(seq1.scf_ecuts):
            raise ValueError("'pawecutdg' must be > than all ecuts checked.")
        seq2 = AbinitEcutPhononConvergenceSequencer(loglevel=self._loglevel)
        seq2.ecuts_input_variable_name = "pawecutdg"
        seq2.compute_electric_field_response = compute_electric_field_response
        converged_ecut = seq1.scf_converged_ecut
        scf_pawecutdg = ([converged_ecut] +
                         [x for x in scf_pawecutdg
                          if x > converged_ecut[
                                seq1.ecuts_input_variable_name]])
        if not len(scf_pawecutdg):
            raise ValueError(
                    "Converged ecut is higher than all proposed pawecutdg.")
        # use converged ecut for pawecutdg convergence test
        pawecutdg_input_vars = seq1.scf_input_variables.copy()
        self._use_converged_quantities(
                pawecutdg_input_vars, use_phonon_converged_ecut=True)
        # pop out old variables
        kwargs.pop("scf_input_variables")
        kwargs.pop("phonons_convergence_criterion")
        super().set_phonon_ecut_convergence(
                *args, root_workdir=root_workdir,
                scf_input_variables=pawecutdg_input_vars,
                phonons_convergence_criterion=(
                    phonons_pawecutdg_convergence_criterion),
                _sequencer=seq2,
                **kwargs)
        if compute_electric_field_response:
            seq2.ddk_workdir = os.path.join(root_workdir, "ddk_runs")
            seq2.ddk_input_variables = ddk_input_variables
            self._add_calculation_parameters(
                    "ddk_", ddk_calculation_parameters, seq2)
        list_.append(seq2)
        self._phonon_ecut_convergence_sequencer = list_

    def set_phonon_dispersion(
            self, *args,
            root_workdir=None,
            anaddb_input_variables=None,
            anaddb_calculation_parameters=None,
            compute_electric_field_response=None,
            ddk_input_variables=None,
            ddk_calculation_parameters=None,
            mrgddb_calculation_parameters=None,
            **kwargs,
            ):
        """Sets the phonon_dispersion part of the workflow.

        Parameters
        ----------
        root_workdir: str
            Where all the calculations will be executed.
        compute_electric_field_response: bool
            If True, the electric field response (ddk) will be computed.
            Used to compute the LO-TO splitting.
        anaddb_input_variables: dict
            The dict of input variables for anaddb script.
        anaddb_calculation_parameters: dict
            The dict of calculation parameters for the anaddb script.
        ddk_input_variables: dict, optional
            In the case where 'compute_electric_field_response' is True,
            this specifies the dict of ddk input variables.
        ddk_calculation_parameters: dict, optional
            In the case where 'compute_electric_field_response' is True,
            this specifies the dict of ddk calculation_parameters.

        Other kwargs are passed to the mother's class method.
        """
        super().set_phonon_dispersion(
                *args, root_workdir=root_workdir, **kwargs)
        if compute_electric_field_response is None:
            raise ValueError("Need to set 'compute_electric_field_response'.")
        sequencer = self.phonon_dispersion_sequencer
        # DDK PART
        sequencer.compute_electric_field_response = (
                compute_electric_field_response)
        if compute_electric_field_response:
            sequencer.ddk_workdir = os.path.join(root_workdir, "ddk_runs")
            if ddk_input_variables is None:
                raise ValueError("Need to set 'ddk_input_variables'.")
            sequencer.ddk_input_variables = ddk_input_variables
            self._add_calculation_parameters(
                    "ddk_", ddk_calculation_parameters, sequencer)
        # MRGDDB PART
        sequencer.mrgddb_workdir = os.path.join(root_workdir, "mrgddb_run")
        self._add_calculation_parameters(
                "mrgddb_", mrgddb_calculation_parameters, sequencer)
        # ANADDB PART
        sequencer.anaddb_workdir = os.path.join(root_workdir, "anaddb_run")
        if anaddb_input_variables is None:
            raise ValueError("Need to set 'anaddb_input_variables'.")
        sequencer.anaddb_input_variables = anaddb_input_variables
        self._add_calculation_parameters(
                "anaddb_", anaddb_calculation_parameters, sequencer)

    def set_phonon_smearing_convergence(
            self, *args, scf_kgrids_input_variable_name=None, **kwargs):
        """Sets the phonon_smearing_convergence part of the workflow.

        Parameters
        ----------
        scf_kgrids_input_variable_name: str
            The kgrid input variable name to test.
        All other kwargs and args are passed to the mother's class method with
        the same name.
        """
        sequencer = self.phonon_smearing_convergence_sequencer
        if scf_kgrids_input_variable_name is None:
            raise ValueError(
                    "Need to set 'scf_kgrids_input_variable_name'.")
        sequencer.kgrids_input_variable_name = scf_kgrids_input_variable_name
        super().set_phonon_smearing_convergence(*args, **kwargs)

    def write_band_structure_convergence(self):
        self.band_structure_convergence_sequencer.write()

    def write_optic(self):
        self.optic_sequencer.write()

    def _add_calculation_parameters(self, prefix, parameters, sequencer):
        # add pseudos
        if parameters is not None:
            parameters["pseudos"] = self.pseudos
        return super()._add_calculation_parameters(
                prefix, parameters, sequencer)

    def _is_paw_calculation(self, input_variables):
        """Returns True if the input_variables given corresponds to a PAW
        calculation.
        """
        if "pawecutdg" in input_variables:
            return True
        return False
