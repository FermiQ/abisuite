import abc
import os

import matplotlib
import numpy as np

from ..bases import BaseUtility
from ..routines import (
        full_abspath, is_list_like, is_scalar_or_str,
        sort_data as fsort, suppress_warnings,
        )


class BaseCurve(BaseUtility, abc.ABC):
    """Base class for curves.
    """
    equal_xdata_and_ydata = True

    def __init__(self, xdata, ydata, zdata=None, color="k", offset=None,
                 zdir=None, alpha=1.0, label=None,
                 **kwargs):
        super().__init__(**kwargs)
        self._xdata, self._ydata, self._zdata = None, None, None
        self.xdata, self.ydata = xdata, ydata
        if self.equal_xdata_and_ydata:
            if len(self.xdata) != len(self.ydata):
                raise ValueError("xdata and ydata must be same length!")
        self.alpha = alpha
        if zdata is not None:
            self.zdata = zdata
        self.offset = offset
        self.zdir = zdir
        self._color = None
        self.color = color
        self.label = label
        self.lines = None  # data returned by matplotlib

    @property
    def color(self):
        return self._color

    @color.setter
    def color(self, color):
        self._color = color

    @property
    def xdata(self):
        return self._xdata

    @xdata.setter
    def xdata(self, xdata):
        self._xdata = self._check_list(xdata)

    @property
    def ydata(self):
        return self._ydata

    @ydata.setter
    def ydata(self, ydata):
        self._ydata = self._check_list(ydata)

    @property
    def zdata(self):
        return self._zdata

    @zdata.setter
    def zdata(self, zdata):
        # NOTE: don't do check as data might be more than 2D (color img)
        zdata = self._check_list(zdata)
        if len(zdata) != len(self.xdata):
            raise ValueError("zdata must be same len as x and y data!")
        self._zdata = zdata

    @abc.abstractmethod
    def plot_on_axis(self, *args, **kwargs):
        # must be overidden
        pass

    def copy(self):
        zdata = None
        if self.zdata is not None:
            zdata = self.zdata.copy()
        new = self.__class__(
                self.xdata.copy(), self.ydata.copy(), zdata,
                loglevel=self._loglevel)
        new.color = self.color
        new.alpha = self.alpha
        new.label = self.label
        new.offset = self.offset
        new.zdir = self.zdir
        return new

    def clear_label(self):
        """Sets the curve label to None.
        """
        self.label = None

    def reset(self):
        del self.lines
        self.lines = None

    @abc.abstractmethod
    def update(self):
        """Update axis data with current stored data.
        """
        raise NotImplementedError()

    def _check_list(self, data):
        # check if arguments are array-like and return numpy arrays
        if not is_list_like(data):
            raise TypeError("Argument should be array-like.")
        if isinstance(data, np.ndarray):
            if len(data.shape) >= 2:
                raise TypeError("Argument is > 1D data!")
        return np.array(data, dtype=float)  # transform all to floats


class Curve(BaseCurve):
    """Class that represents a curve."""
    _loggername = "Curve"

    def __init__(
            self, *args, linestyle="-",
            linewidth=1.0, normalize=False, semilogy=False,
            semilogx=False, loglog=False, marker=None,
            sort_data=False, pickradius=5, fillstyle="full", markersize=1,
            markerfacecolor="k",
            **kwargs):
        """Curve init method.

        Parameters
        ----------
        xdata : array-like
                 x-data points.
        ydata : array-like
                 y-data points.
        linestyle : str, optional
                    Curve linestyle.
        alpha : float, optional
                The alpha value of the curve.
        color : str, optional
                Curve color.
        label : str, optional
                Curve label. None = no label.
        linewidth : float, optional
                    Sets the width of the curves.
        normalize : bool, optional
                    If True, the ydata is normalized by its maximum value.
        semilogy : bool, optional
                   If True, the ydata is plot on a semilog axis instead of a
                   normal axis.
        semilogx : bool, optional
                   Same as semilogy but for xaxis.
        loglog : bool, optional
                 Same as semilogy but for both axis. If semilogx and semilogy
                 are both True at the same time: loglog will be set to True.
        marker : str, optional
                 The marker for the ydata. If None, no marker is applied.
        markersize : float, optional
            The marker size.
        markerfacecolor : str, optional
            The marker face color.
        sort_data : bool, optional
                    If True, xdata will be sorted and ydata too according to
                   the sorting of xdata.
        pickradius : None or float,
            If A float, it tells pyplot how large the 'pickable' area is
            for a curve.
        """
        super().__init__(*args, **kwargs)
        if sort_data:
            self.sort_data()
        if linestyle is None:
            # matplotlib only accepts strings...
            linestyle = "None"
        self.linestyle = linestyle
        self.linewidth = linewidth
        self.semilogy = semilogy
        self.semilogx = semilogx
        self.loglog = loglog
        if semilogy and semilogx:
            # use loglog instead
            self.loglog = True
        self.pickradius = pickradius
        self.fillstyle = fillstyle
        self.marker = marker
        self.markersize = markersize
        self.markerfacecolor = markerfacecolor
        if normalize:
            self.ydata = self.ydata / self.max

    def __truediv__(self, value):
        self._check_scalar(value)
        self.ydata /= value
        return self

    def __add__(self, value):
        self._check_scalar(value)
        self.ydata += value
        return self

    def __mul__(self, value):
        self._check_scalar(value)
        self.ydata *= value
        return self

    def __sub__(self, value):
        self._check_scalar(value)
        self.ydata -= value
        return self

    @property
    def max(self):
        return np.max(self.ydata)

    @property
    def min(self):
        return np.min(self.ydata)

    @property
    def wheremax(self):
        return self.xdata[np.where(self.ydata == self.max)[0]]

    @property
    def wheremin(self):
        return self.xdata[np.where(self.ydata == self.min)[0]]

    def copy(self):
        new = super().copy()
        new.linestyle = self.linestyle
        new.semilogx = self.semilogx
        new.semilogy = self.semilogy
        new.loglog = self.loglog
        new.marker = self.marker
        new.markerfacecolor = self.markerfacecolor
        new.markersize = self.markersize
        new.linewidth = self.linewidth
        new.pickradius = self.pickradius
        return new

    @suppress_warnings
    def plot_on_axis(self, axis):
        args = [self.xdata,
                self.ydata]
        if self.zdata is not None:
            args.append(self.zdata)
        kwargs = {"linestyle": self.linestyle,
                  "color": self.color,
                  "label": self.label,
                  "linewidth": self.linewidth,
                  "marker": self.marker,
                  "alpha": self.alpha,
                  # "pickradius": self.pickradius,
                  "fillstyle": self.fillstyle,
                  "markersize": self.markersize,
                  "markerfacecolor": self.markerfacecolor}
        if self.loglog:
            # important to call this first because if semilogx-y are both True,
            # loglog wil be set to True
            self.lines = axis.loglog(*args, **kwargs)
        elif self.semilogy and not self.loglog:
            self.lines = axis.semilogy(*args, **kwargs)
        elif self.semilogx and not self.loglog:
            self.lines = axis.semilogx(*args, **kwargs)
        else:
            self.lines = axis.plot(*args, **kwargs)
        return self.lines

    def sort_data(self):
        self.xdata, self.ydata = fsort(self.xdata, self.ydata)

    def update(self):
        line = self.lines[0]
        for prop in ("xdata", "ydata", "color", "label", "linewidth",
                     "marker", "alpha",  # "pickradius",
                     "fillstyle",
                     "markersize", "markerfacecolor"):
            getattr(line, f"set_{prop}")(getattr(self, prop))
        if self.zdata is not None:
            line.set_zdata(self.zdata)

    def _check_scalar(self, value):
        if not isinstance(value, int) and not isinstance(value, float):
            raise TypeError("Arithmetic operations only with int or floats.")


class Text(BaseCurve):
    """Class that represents a text object.
    """
    _loggername = "Text"

    def __init__(self, text, xdata, ydata, zdata=None, **kwargs):
        if is_scalar_or_str(xdata):
            xdata = [xdata]
        else:
            if len(xdata) > 1:
                raise ValueError(
                        "xdata must be an array of length 1 or scalar.")
        if is_scalar_or_str(ydata):
            ydata = [ydata]
        else:
            if len(ydata) > 1:
                raise ValueError(
                        "ydata must be an array of length 1 or scalar.")
        if zdata is not None:
            if is_scalar_or_str(zdata):
                zdata = [zdata]
            else:
                if len(zdata) > 1:
                    raise ValueError(
                            "zdata must be an array of length 1 or scalar.")
        super().__init__(xdata, ydata, zdata=zdata, **kwargs)
        self.text = text

    def plot_on_axis(self, axis):
        args = [self.xdata[0], self.ydata[0]]
        if self.zdata is not None:
            args.append(self.zdata[0])
        args.append(self.text)
        args.append(self.zdir)
        kwargs = {"color": self.color}
        self.lines = axis.text(*args, **kwargs)
        return self.lines

    def update(self):
        if self.zdata is None:
            self.lines.set_position((self.xdata[0], self.ydata[0]))
        else:
            self.lines.set_position(
                    (self.xdata[0], self.ydata[0], self.zdata[0]))
        self.lines.set_color(self.color)
        self.lines.set_text(self.text)


class FillBetween(BaseCurve):
    """Class that represents a filled area between two curves.
    """
    _loggername = "FillBetween"

    def __init__(
            self, xdata, ydata1, ydata2, color="r", zorder=None, **kwargs):
        """FillBetween area init method.

        Parameters
        ----------
        xdata : array-like
                x-data points.
        ydata1, ydata2 : array-like
                         The y coordinates of the two curves to fill between.
        color : str, optional
                The color of the filled area.
        """
        super().__init__(xdata, ydata1, **kwargs)
        self._ydata2 = None
        self.ydata2 = ydata2
        self.color = color

    @property
    def ydata2(self):
        return self._ydata2

    @ydata2.setter
    def ydata2(self, ydata2):
        self._ydata2 = self._check_list(ydata2)

    def plot_on_axis(self, axis):
        self.lines = axis.fill_between(
                self.xdata, self.ydata, self.ydata2, color=self.color,
                alpha=self.alpha)
        return self.lines

    def update(self):
        # FIXME: THIS IS NOT WORKING PROPERLY
        return
        # delete the collection directly from the figure since
        # deleting the reference is not sufficient
        # loop over all axes in case we have a multiplot
        to_del = None
        axis_idx = None
        for iax, axis in enumerate(self.lines.get_figure().axes):
            for ic, collection in enumerate(axis.collections):
                if collection.get_label() == self.lines.get_label():
                    # delete this one
                    to_del = ic
                    axis_idx = iax
                    break
            if to_del is not None:
                break
        else:
            raise LookupError()
        axis = self.lines.get_figure().axes[axis_idx]
        del axis.collections[to_del]
        # redraw
        self.plot_on_axis(axis)
        # swap collection to previous position in order to keep ordering
        (axis.collections[ic],
         axis.collections[-1]) = (axis.collections[-1], axis.collections[ic])


class Histogram(BaseCurve):
    """Class that represents a Histogram.

    xdata is the bins and ydata is the data to put in a histogram.
    If xdata is an array, the bins are that array. Can be a single integer
    where it would correspond to the number of bins.
    """
    equal_xdata_and_ydata = False
    _loggername = "Historam"

    @property
    def xdata(self):
        return BaseCurve.xdata.fget(self)

    @xdata.setter
    def xdata(self, xdata):
        if isinstance(xdata, int):
            self.xdata = [xdata]
            return
        BaseCurve.xdata.fset(self, xdata)

    def plot_on_axis(self, axis):
        bins = self.xdata
        if len(self.xdata) == 1:
            bins = int(self.xdata[0])
        self.lines = axis.hist(self.ydata, bins=bins)

    def update(self):
        raise NotImplementedError()


class Image(BaseCurve):
    """Class that represents an image in order to plot it on an axis.

    xdata and ydata are only the boundaries of the actual x and y data.
    """
    _loggername = "Image"

    def __init__(self, xdata, ydata, zdata, cmap="hot", interpolation="none",
                 origin="lower", **kwargs):
        if len(xdata) != len(ydata):
            # take only boundaries
            xdata = [xdata[0], xdata[-1]]
            ydata = [ydata[0], ydata[-1]]
            super().__init__(xdata, ydata, **kwargs)
            self._logger.info(
                    "Different arrays have been passed to Image. "
                    "Taking only boundaries.")
        else:
            super().__init__(xdata, ydata, **kwargs)
        self.cmap = cmap
        self.interpolation = interpolation
        self.zdata = zdata
        self.origin = origin

    @property
    def ax_image(self):
        # alias
        return self.lines

    @ax_image.setter
    def ax_image(self, image):
        self.lines = image

    @BaseCurve.zdata.setter
    def zdata(self, zdata):
        # NOTE: don't do check as data might be more than 2D (color img)
        self._zdata = zdata  # self._check_list(zdata)

    def plot_on_axis(self, axis):
        self.ax_image = axis.imshow(
                self.zdata, cmap=self.cmap, origin=self.origin,
                extent=[self.xdata[0], self.xdata[-1],
                        self.ydata[0], self.ydata[-1]],
                interpolation=self.interpolation)
        return self.ax_image


class PNGImage(Image):
    """Class that represents a png image.
    """
    _loggername = "PNGImage"

    def __init__(
            self, path, *args,
            xmin=0, xmax=1, ymin=0, ymax=1,
            origin="lower", reverse=True,
            **kwargs):
        """PNGImage init method.

        Parameters
        ----------
        path : str
            the path to the png image.
        xmin, xmax, ymin, ymax : float, optional
            The boundaries of the data. Each pixel coordinates will be rescaled
            according to these values.
        reverse : bool, optional
            if True, the image is reversed after being read.
        """
        self.path = full_abspath(path)
        if not os.path.isfile(self.path):
            raise FileNotFoundError(self.path)
        img = matplotlib.image.imread(self.path)
        shape = img.shape
        n_xs = shape[0]
        n_ys = shape[1]
        if reverse:
            img = np.flip(img, axis=0)
        super().__init__(
                np.linspace(xmin, xmax, n_xs),
                np.linspace(ymin, ymax, n_ys),
                img, *args, origin=origin, **kwargs)

    def update(self):
        raise NotImplementedError()


class Scatter(BaseCurve):
    """Class that represents a scatter plot to plot on an axis.
    """
    _loggername = "Scatter"

    def __init__(self, *args, s=1, edgecolors="face", **kwargs):
        super().__init__(*args, **kwargs)
        self.s = s  # marker size
        self.edgecolors = edgecolors

    def copy(self):
        new = super().copy()
        new.s = self.s
        new.edgecolors = self.edgecolors
        return new

    def plot_on_axis(self, axis):
        args = [self.xdata, self.ydata]
        if self.zdata is not None:
            args.append(self.zdata)
        self.lines = axis.scatter(
                *args, s=self.s, color=self.color, label=self.label,
                edgecolors=self.edgecolors,
                )
        return self.lines

    def update(self):
        line = self.lines
        line.set_lw(self.s)
        line.set_ec(self.edgecolors)
        line.set_fc(self.color)
        line.set_label(self.label)
        if self.zdata is not None:
            line.set_offsets(np.array([self.xdata, self.ydata, self.zdata]).T)
        else:
            line.set_offsets(np.array([self.xdata, self.ydata]).T)


class Surface(Image):
    """Class that represents a 3D surface.
    """
    # ALL DATA MUST BE NxM (xdata, ydata, zdata)
    _loggername = "Surface"

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        assert self.xdata.shape == self.ydata.shape
        if len(self.xdata.shape) == 1:
            # 1D data => create meshgrid
            self.xdata, self.ydata = np.meshgrid(self.xdata, self.ydata)
        # zdata must be same shape as this meshgrid
        assert self.xdata.shape == self.zdata.shape

    def plot_on_axis(self, axis):
        self.lines = axis.plot_surface(
                self.xdata, self.ydata, self.zdata, cmap=self.cmap)
        return self.lines

    def _check_list(self, data):
        # check if arguments are array-like and return numpy arrays
        if type(data) not in (np.ndarray, list, tuple):
            raise TypeError("Argument should be array-like.")
        if isinstance(data, np.ndarray):
            if len(data.shape) >= 3:
                raise TypeError("Argument is > 2D data!")
        return np.array(data, dtype=float)  # transform all to floats


class Contour(Surface):
    """Class that represents a contour plot on a 3D surface.
    """
    def __init__(
            self, xdata, ydata, zdata, levels, *args, linewidths=1.0,
            linestyles="-", **kwargs):
        """Contour init method.

        Parameters
        ----------
        xdata, ydata, zdata : arrays
                              Must be 2D shaped.
        linewidths : float, optional
            The coutour plot linewidths.
        levels : int or array-like
                 If one int, draws n+1 contour lines with automatic height
                 (by pyplot). If array-like, specifies the levels heights
                 manually for each contour line.
        """
        super().__init__(xdata, ydata, zdata, *args, **kwargs)
        self._levels = None
        self.levels = levels
        self.linewidths = linewidths
        self.linestyles = linestyles

    @property
    def levels(self):
        return self._levels

    @levels.setter
    def levels(self, levels):
        if isinstance(levels, int):
            self._levels = levels
            return
        if is_list_like(levels):
            self._levels = levels
            return
        raise TypeError(f"Expected int or array-like but got: {levels}")

    @property
    def contour(self):
        # alias
        return self.lines

    @contour.setter
    def contour(self, contour):
        self.lines = contour

    def plot_on_axis(self, axis):
        args = [self.xdata, self.ydata, self.zdata, self.levels]
        kwargs = {"linewidths": self.linewidths,
                  "linestyles": self.linestyles,
                  "label": self.label}
        if self.color is not None:
            self.contour = axis.contour(*args, colors=self.color, **kwargs)
            return self.contour
        self.contour = axis.contour(*args, cmap=self.cmap, **kwargs)
        return self.contour

    def update(self):
        raise NotImplementedError()


# TODO: rebase with BaseCurve
class StraightLine(BaseUtility, abc.ABC):
    def __init__(self, pos, linestyle="-", color="k", linewidth=1.0,
                 label=None, **kwargs):
        super().__init__(**kwargs)
        self.position = pos
        self.linestyle = linestyle
        self.color = color
        self.linewidth = linewidth
        self.label = label
        self.lines = None

    @abc.abstractmethod
    def plot_on_axis(self, *args, **kwargs):
        pass

    def reset(self):
        del self.lines
        self.lines = None

    def update(self):
        self.lines.set_linestyle(self.linestyle)
        self.lines.set_linewidth(self.linewidth)
        self.lines.set_color(self.color)
        self.lines.set_label(self.label)


class VLine(StraightLine):
    _loggername = "VLine"

    def plot_on_axis(self, axis):
        self.lines = axis.axvline(
                self.position, linestyle=self.linestyle, color=self.color,
                linewidth=self.linewidth, label=self.label)
        return self.lines

    def update(self):
        self.lines.set_xdata(self.position)
        super().update()


class HLine(StraightLine):
    _loggername = "HLine"

    def plot_on_axis(self, axis):
        self.lines = axis.axhline(
                self.position, linestyle=self.linestyle, color=self.color,
                linewidth=self.linewidth, label=self.label)
        return self.lines

    def update(self):
        self.lines.set_ydata(self.position)
        super().update()
