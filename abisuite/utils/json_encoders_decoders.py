import json


class AbisuiteJSONEncoder(json.JSONEncoder):
    """Overrides json default encoder to allow things we need for abisuite.
    """
    def default(self, obj):
        if isinstance(obj, set):
            return {"__isset__": True, "data": list(obj)}
        # else return default value
        return super().default(obj)


class AbisuiteJSONDecoder(json.JSONDecoder):
    """Overrides json default decoder to allow things we need for abisuite.
    """
    def decode(self, obj):
        obj = super().decode(obj)
        if not isinstance(obj, dict):
            return obj
        for k, v in obj.items():
            if not isinstance(v, dict):
                continue
            if "__isset__" in v:
                obj[k] = set(v["data"])
        return obj
