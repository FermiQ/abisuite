import numpy as np
from tabulate import tabulate

from ..bases import BaseUtility
from ..colors import Colors
from ..routines import is_list_like


class TerminalTable(BaseUtility):
    """Class that serves as template to print a table in terminal.
    """
    _loggername = "TerminalTable"

    def __init__(
            self, column_names, column_alignments=None,
            bold_column_headers=True,
            **kwargs):
        """Table init method.

        Parameters
        ----------
        bold_column_headers: bool, optional
            If True, the column headers are set to Bold text upon printing.
        column_names : list
            The list of column names. The length of this list
            dictates the number of columns.
        column_alignements: list, optional
            Gives the column alignement for each columns. If None,
            the 'center' alignement is given to all columns.
        """
        super().__init__(**kwargs)
        self.column_names = column_names
        self.bold_column_headers = bold_column_headers
        if column_alignments is None:
            self.column_alignments = ["center"] * len(self.column_names)
        else:
            if not is_list_like(column_alignments):
                raise TypeError(
                        "Expected 'column_alignments' to be a list but got: "
                        f"'{column_alignments}'.")
            if len(column_alignments) != len(column_names):
                raise ValueError(
                        "Length of 'column_alignments' doesn't match the "
                        "number of columns.")
            self.column_alignments = column_alignments
        self.rows = []

    def add_column(self, column_header, column, index=None,
                   alignment="center"):
        """Add a column to the table.

        Paramters
        ----------
        alignment: str, optional
                   Specifies the alignement of the column.
        column_header : str
                        The title of the column.
        column : list
                 The data list of the column. Must match the
                 number of rows.
        index : int, optional
                If not None, insert column before that index.
        """
        if len(column) != len(self.rows) and len(self.rows) > 0:
            raise ValueError("Column length does not match number of rows.")
        if index is None:
            if not len(self.rows):
                if len(column):
                    # something wrong where do we put data??
                    self._logger.error(f"Trying to append a column "
                                       f"'{column_header}': {column}")
                    raise LookupError("No rows to append data to??")
                # no rows but also no column data. weird but not impossible
                # index is just zero then (in fact any index do the job)
                index = 0
            else:
                index = len(self.rows[0])  # append at the end
        self.column_names.insert(index, column_header)
        self.column_alignments.insert(index, alignment)
        for i, data in enumerate(column):
            self.rows[i].insert(index, data)

    def add_row(self, row):
        """Add a row to the table.

        Parameters
        ----------
        row : list
              The row of data to add. Must be the same length
              as the number of columns.
        """
        if len(row) != len(self.column_names):
            raise ValueError("Row to add is not the same length"
                             " as the others.")
        self.rows.append(row)

    def _check_column_exists(self, column_title):
        if column_title not in self.column_names:
            raise KeyError(f"{column_title} not in the column headers.")

    def is_column_sortable(self, column_title):
        """Check if a column is sortable or not.

        Parameters
        ----------
        column_title : str
                       The column header.

        Returns
        -------
        bool : True if the column is (at least partially) sortable.
        """
        self._check_column_exists(column_title)
        sortable_rows = self._sortable_rows_indices(column_title)
        if len(sortable_rows) == 0:
            return False
        return True

    def _sortable_rows_indices(self, column_title):
        # find sortable values rows by checking value in the column
        # if value is not a float nor an int, row is not sortable
        column_index = self.get_column_index(column_title)
        sortable_rows = [i for i, x in enumerate(self.rows)
                         if type(x[column_index]) in (int, float)]
        return sortable_rows

    def sortby(self, column_title):
        """Sort the table.

        Parameters
        ----------
        column_title : str
            Sort in increasing order according to this column.
        """
        self._check_column_exists(column_title)
        if not self.is_column_sortable(column_title):
            self._logger.warning(f"{column_title} not sortable...")
        self._logger.info(f"Sorting table according to {column_title}.")
        sortable_rows = self._sortable_rows_indices(column_title)
        non_sortable_rows = [i for i in range(len(self.rows))
                             if i not in sortable_rows]
        # sort the sortable rows
        sort_indices = np.argsort(self.get_column(column_title,
                                                  indices=sortable_rows))
        newrows = [self.rows[x] for x in sort_indices]
        # add non sortable rows at the end
        for i in non_sortable_rows:
            newrows.append(self.rows[i])
        # just a little checkup that everything went well
        assert len(newrows) == len(self.rows)
        self.rows = newrows

    def get_column(self, column_title, indices=None):
        """Returns the data of a column.

        Parameters
        ----------
        column_title : str
                        The column header.
        indices : list, optional
                  The list if index to keep.
        """
        self._check_column_exists(column_title)
        column_index = self.column_names.index(column_title)
        toreturn = [row[column_index] for row in self.rows]
        # remove indices if needed
        if indices is not None:
            toret = []
            for i in indices:
                toret.append(toreturn[i])
            toreturn = toret
        return toreturn

    def get_column_index(self, column_title):
        """Returns the index of the column.

        Parameters
        ----------
        column_title : str
                       The column header.
        """
        self._check_column_exists(column_title)
        return self.column_names.index(column_title)

    def print(self):  # noqa: T002
        headers = self.column_names
        if self.bold_column_headers:
            headers = [Colors.color_text(header, "bold") for header in headers]
        if not self.rows:
            print(Colors.color_text("NO DATA TO SHOW", "bold"))
            return
        print()  # noqa: T001
        print(tabulate(  # noqa: T001
                self.rows,
                headers=headers,
                colalign=self.column_alignments,
                )
              )
