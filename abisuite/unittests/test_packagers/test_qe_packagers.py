import os
import tempfile

from ..bases import TestCase
from ..routines_for_tests import TemporaryCalculation
from ..variables_for_tests import QEPWCalc
from ...packagers import QEPWPackager


class TestQEPWPackager(TestCase):
    """Test case for the QEPWPackager object.
    """

    def setUp(self):
        self.calc = TemporaryCalculation(QEPWCalc, copy_on_creation=False)
        self.packager = QEPWPackager()
        self.dest = tempfile.TemporaryDirectory()

    def tearDown(self):
        self.calc.cleanup()
        self.dest.cleanup()

    def test_package(self):
        self.calc.copy_calculation()
        self.packager.calculation_directory = self.calc.path
        self.packager.package(
                src_root=self.calc.path,
                dest_root=self.dest.name)
        # check that all files needed to copy were copied
        # and that all files copied needed to be copy (1:1 correspondance)
        for handler in self.packager.calculation_directory.walk(
                paths_only=False):
            if self.packager.keep_file(handler):
                # make sure this file appears in the dest
                relpath = os.path.relpath(handler.path, self.calc.path)
                newpath = os.path.join(self.dest.name, relpath)
                self.assertTrue(os.path.isfile(newpath))
        for root, _, files in os.walk(self.dest.name):
            for filename in files:
                relpath = os.path.relpath(
                        os.path.join(root, filename), self.dest.name)
                oldpath = os.path.join(self.calc.path, relpath)
                self.assertIn(
                        oldpath,
                        self.packager.calculation_directory.walk(
                            paths_only=True))
