import os
import tempfile

from ..bases import TestCase
from ...databases.bases import DB
from ...linux_tools import touch


class TestDB(TestCase):
    """Generic DB unittests.
    """
    def setUp(self):
        self.tempdir = tempfile.TemporaryDirectory()
        self.path = os.path.join(self.tempdir.name, "test.db")
        touch(self.path)
        self.db = DB.from_file(self.path)

    def tearDown(self):
        self.tempdir.cleanup()

    def test_delete_column(self):
        # create a table and try to delete a column
        table = "tablename"
        tablecols = {"col_int": int,
                     "col_float": float,
                     "col_str": str,
                     "primary_key": "col_int",
                     }
        self.db.create_table(table, **tablecols)
        self.assertEqual(self.db.primary_key[table], tablecols["primary_key"])
        todelete = "col_float"
        self.db.delete_column(table, todelete)
        self.assertNotIn(todelete, self.db.columns[table])
        self.assertEqual(self.db.primary_key[table], tablecols["primary_key"])

    def test_rename_table(self):
        # create a table and rename it
        oldname = "tablename"
        newname = "newtablename"
        tablecols = {"col_int": int,
                     "col_float": float,
                     "col_str": str,
                     }
        self.db.create_table(oldname, **tablecols)
        self.db.rename_table(oldname, newname)
        self.assertIn(newname, self.db.tables)
        self.assertNotIn(oldname, self.db.tables)
        for k in tablecols:
            self.assertIn(k, self.db.columns[newname])
        with self.assertRaises(ValueError):
            self.db.rename_table("this_table_does_not_exist", "new_table")
        with self.assertRaises(ValueError):
            self.db.rename_table(newname, newname)
        another_table = "another_table"
        self.db.create_table(another_table, **tablecols)
        with self.assertRaises(ValueError):
            self.db.rename_table(newname, another_table)

    def test_adding_columns(self):
        tablename = "tablename"
        tablecols = {"col_int": int,
                     "col_float": float,
                     "col_str": str,
                     }
        self.db.create_table(tablename, **tablecols)
        for key in tablecols:
            self.assertIn(key, self.db.columns[tablename])
        newcols = {"newcol1": int, "newcol2": str}
        self.db.add_columns(tablename, **newcols)
        for key in newcols:
            self.assertIn(key, self.db.columns[tablename])
        # check adding a column to a non existent table raise an error
        with self.assertRaises(ValueError):
            self.db.add_columns("nonexistenttable", newcolumn=int)
        # check adding nothing raise an error
        with self.assertRaises(ValueError):
            self.db.add_columns(tablename)
        # check addind an already existing column raise an error
        with self.assertRaises(ValueError):
            self.db.add_columns(tablename, **newcols)

    def test_insert(self):
        tablename = "tablename"
        tablecols = {"col_int": int,
                     "col_float": float,
                     "col_str": str,
                     }
        self.db.create_table(tablename, **tablecols)
        self.db.insert(tablename, **{k: v(1.0) for k, v in tablecols.items()})
        selection = self.db.select("*", tablename)
        self.assertEqual(len(selection), 1)
        selection = selection[0]
        self.assertEqual(len(selection), len(tablecols))
        for i, value in enumerate(selection):
            self.assertEqual(value, tablecols[list(tablecols.keys())[i]](1.0))
        # check that trying to get column from a non-existant column raise err
        with self.assertRaises(ValueError):
            self.db.select("unknown_column", tablename)
        # check the raise from an unknown tablename
        for k in tablecols:
            with self.assertRaises(ValueError):
                self.db.select(k, "unknown_table")

    def test_table_creation(self):
        tablename = "tablename"
        tablecols = {"col_int": int,
                     "col_float": float,
                     "col_str": str,
                     }
        self.assertEqual(self.db.tables, [])
        self.db.create_table(tablename, **tablecols)
        self.assertIn(tablename, self.db.tables)
        cols = self.db.columns[tablename]
        for colname, colcast in cols.items():
            self.assertIn(colname, tablecols)
            self.assertIs(colcast, tablecols[colname])
        # check the table deletion
        self.db.delete_table(tablename)
        self.assertEqual(len(self.db.tables), 0)

    def test_db_creation(self):
        os.remove(self.path)
        self.db.create_database(self.path)
        self.assertTrue(os.path.isfile(self.path))
        self.assertEqual(self.path, self.db.db)
        # check the db deletion
        self.db.delete_database()
        self.assertFalse(os.path.isfile(self.path))
