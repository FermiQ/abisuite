import os

from abisuite.linux_tools import touch


def replace_file(path, **kwargs):
    if os.path.islink(path):
        # compute relative path and replace link
        og_source = os.readlink(path)
        dirname = os.path.dirname(path)
        relpath = os.path.relpath(og_source, start=dirname)
        assert og_source == os.path.abspath(os.path.join(dirname, relpath))
        os.remove(path)
        os.symlink(relpath, path, **kwargs)
    else:
        os.remove(path)
        touch(path)


def is_file_to_replace(path):
    if os.path.islink(path):
        if os.path.isabs(os.readlink(path)):
            return True
        return False
    if not os.path.isfile(path):
        return False
    path = os.path.basename(path)
    if ".wfc" in path:
        return True
    if path.startswith("wfc") and path.endswith(".dat"):
        return True
    if "charge-density.dat" in path:
        return True
    if path.endswith("_WFK") or "_1WF" in path or path.endswith("_WFK.nc"):
        return True
    if path.endswith("_DEN") or path.endswith("_DEN.nc"):
        return True
    return False


for dirname, _dirpaths, filepaths in os.walk("."):
    # check if there are symlinks of directories
    for _dirpath in _dirpaths:
        fullpath = os.path.abspath(os.path.join(dirname, _dirpath))
        if os.path.islink(fullpath):
            og_source = os.readlink(fullpath)
            if not os.path.isabs(og_source):
                continue
            replace_file(fullpath, target_is_directory=True)
    for filepath in filepaths:
        fullpath = os.path.abspath(os.path.join(dirname, filepath))
        if is_file_to_replace(fullpath):
            replace_file(fullpath)
