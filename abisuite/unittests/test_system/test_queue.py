import os
import tempfile
import unittest

from ...system import Queue


output_examples = {
    "slurm": (
        "=             JOBID     NAME     USER ST       TIME  NODES "
        "NODELIST(REASON) WORK_DIR\n=           2107431      epw oums1149 PD  "
        "0:00      8 (QOSGrpNodeLimit) /scratch/oums1149/epw/run\n=           "
        "2107430 kgrid12_ oums1149 PD       0:00     20 (Priority) "
        "/data/fgiustino/oums1149/Sr2RuO4/PBE_NC/noSOC/gs/nscf/ecutwfc90/kgrid"
        "12_12_12_20nodes/run\n"
        )
    }


# FIXME: for now only test the slurm queuing system
class TestQueue(unittest.TestCase):
    """Test case for the different Queue parsers.
    """

    def setUp(self):
        self.queue = Queue()
        self.queue_cmd_tempfile = tempfile.NamedTemporaryFile()
        self.queue_cmd = self.queue_cmd_tempfile.name

    def tearDown(self):
        if os.path.isfile(self.queue_cmd):
            self.queue_cmd_tempfile.close()

    # TODO: use mock instead of artificially setting temporary files...
    def test_queue_parsing(self):
        # NOTE: see fixme above for this loop
        for queuing_system in ("slurm", ):
            # manual setting
            self.queue.queuing_system = queuing_system
            parser = self.queue.queue_parser  # get parser cls
            parser.queue_command = self.queue_cmd  # artificially set cmd
            parser = parser()  # initialize parser obj
            # test the analyse output method
            parser.analyse_queue_output(output_examples[queuing_system])
            # data is manually extracted from example
            self.assertEqual(parser.njobs, 2)
            paths = (
                "/data/fgiustino/oums1149/Sr2RuO4/PBE_NC/noSOC/gs/nscf/ecutwfc90/kgrid12_12_12_20nodes/run",  # noqa
                "/scratch/oums1149/epw/run")
            for job in paths:
                self.assertIn(job, [x.workdir for x in parser.jobs])
