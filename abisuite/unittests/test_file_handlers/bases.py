# from ..variables_for_tests import (
#         abinit_vars, optic_vars, qe_dosx_vars, qe_dynmatx_vars,
#         qe_epsilonx_vars, qe_fsx_vars, qe_matdynx_vars, qe_phx_vars,
#         qe_ppx_vars, qe_projwfcx_vars, qe_pwx_vars, qe_q2rx_vars,
#         PWCalc, PHCalc, Q2RCalc
#         )
# from ...launchers import ALL_LAUNCHERS
import abc
import os
import shutil
import tempfile

from ...exceptions import DevError
from ...routines import is_list_like

# # order match the list in launchers/__init__.py
# ALL_TEST_VARS = [abinit_vars, optic_vars, qe_dosx_vars, qe_dynmatx_vars,
#                  qe_epsilonx_vars, qe_fsx_vars, qe_matdynx_vars, qe_phx_vars,
#                  qe_ppx_vars, qe_projwfcx_vars, qe_pwx_vars, qe_q2rx_vars]
# ALL_TEST_LINKS = [None, None, PWCalc, PHCalc, PWCalc, PWCalc, Q2RCalc,
#                   PWCalc, PWCalc, PWCalc, None, PHCalc]


class ManualTestException(Exception):
    """Defined this to be a really specific exception that is raised
    manually in a try except statement. Use it to make sure other exceptions
    are raised in the appropriate way.
    """
    pass


class BaseFileHandlerTest(abc.ABC):
    _file_handler_class = None
    _example_file = None
    maxDiff = None  # to see long diffs

    def setUp(self):
        if self._file_handler_class is None:
            raise DevError("Need to set '_file_handler_class'.")
        self.tempdir = tempfile.TemporaryDirectory()
        self.handler = self._file_handler_class()
        if self._example_file is not None:
            newpath = os.path.join(self.tempdir.name,
                                   os.path.basename(self._example_file))
            shutil.copy2(self._example_file, newpath)
            self.handler.path = newpath
            self.handler.read()

    def tearDown(self):
        self.tempdir.cleanup()
        del self.tempdir
        del self.handler

    def test_copy(self):
        copy = self.handler.copy()
        self.assertEqual(copy, self.handler)

    def test_copy_file(self):
        newfilepath = os.path.join(self.tempdir.name, "test_copy_file")
        self.handler.copy_file(newfilepath, overwrite=True)
        self.assertTrue(os.path.isfile(newfilepath))

    def test_delete(self):
        self.handler.delete()
        self.assertFalse(os.path.isfile(self.handler.path))

    def test_move(self):
        newfilepath = os.path.join(self.tempdir.name, "test_move_file")
        oldpath = self.handler.path
        self.handler.move(newfilepath, overwrite=True)
        self.assertTrue(os.path.isfile(newfilepath))
        self.assertFalse(os.path.isfile(oldpath))

    def test_read(self):
        # we expect that this should not raise an error
        self.handler.read()

    # this test does not work on all file handlers....
    # def test_init_from_calculation(self):
    #     for (launcher_class, input_vars, links) in zip(ALL_LAUNCHERS,
    #                                                    ALL_TEST_VARS,
    #                                                    ALL_TEST_LINKS):
    #         classname = str(launcher_class).split(".")[-1].split("'")[0]
    #         workdir = os.path.join(self.tempdir.name,
    #                                classname)
    #         jobname = "test"
    #         launcher = launcher_class(jobname, loglevel=1)
    #         # create fake script
    #         suffix = launcher.calctype
    #         if "qe_" in suffix:
    #             suffix += ".x"
    #         script = tempfile.NamedTemporaryFile(suffix=suffix)
    #         launcher.workdir = workdir
    #         launcher.command = script.name
    #         # check that trying to get meta file from
    #         # non existent calculation
    #         # raises an error
    #         with self.assertRaises(NotADirectoryError):
    #             self._file_handler_class.from_calculation(launcher.workdir)
    #         launcher.init_calculation_tree()
    #         with self.assertRaises(FileNotFoundError):
    #             self._file_handler_class.from_calculation(launcher.workdir)
    #         # write calculation
    #         launcher.input_variables = input_vars.copy()
    #         if links is not None:
    #             launcher.link_calculation(links)
    #         launcher.queuing_system = "local"
    #         launcher.write(bypass_validation=True)
    #         wd = launcher.workdir
    #         with self._file_handler_class.from_calculation(wd) as handler:
    #             self.assertTrue(isinstance(handler,
    #                             self._file_handler_class))


class BaseWritableFileHandlerTest(BaseFileHandlerTest, abc.ABC):
    """Base test case class for handlers that can be written.
    """

    def setUp(self, write=True):
        super().setUp()
        self.path = os.path.join(self.tempdir.name, "test")
        self.handler.path = self.path
        if write:
            self.handler.write()

    def test_copy(self):
        super().test_copy()
        prev = self.handler.copy()
        self._change_random_property(self.handler)
        self.assertNotEqual(self.handler, prev)

    def test_context_manager(self):
        # test that nothing changed if nothing changed
        previous = self.handler.copy()
        with self._file_handler_class.from_file(self.handler.path) as handler:
            handler.read()
            self.assertEqual(previous, handler)
        # test something changed
        with self.handler as handler:
            self._change_random_property(handler)
        # read new
        newhandler = self._file_handler_class.from_file(self.handler.path)
        newhandler.read()
        self.assertNotEqual(previous, newhandler)
        # test nothing changed if an error is raised inside the context manager
        previous = self.handler.copy()
        try:
            with self.handler as handler:
                self._change_random_property(handler)
                raise ManualTestException
        except ManualTestException:
            pass
        newhandler = self._file_handler_class.from_file(self.handler.path)
        newhandler.read()
        self.assertEqual(previous, newhandler,
                         msg=f"{previous}\n!=\n{newhandler}")

    def test_write(self):
        with self._file_handler_class.from_file(self.handler.path) as handler:
            handler.read()
            self.assertEqual(handler, self.handler,
                             msg=f"{handler}\n!=\n{self.handler}")

    @abc.abstractmethod
    def _change_random_property(self, handler):  # pragma: no cover
        pass


class BasePathConvertibleFileHandlerTest(BaseWritableFileHandlerTest):
    """Base test case class for path convertible file handlers.
    """
    def test_path_conversion(self):
        """Checks that path conversion works as expected.
        """
        def check_conversion(absolute):
            for attr in self.handler.structure.convertible_path_attributes:
                value = getattr(self.handler, attr)
                if is_list_like(value):
                    for path in value:
                        if absolute:
                            self.assertTrue(os.path.isabs(path), msg=path)
                        else:
                            self.assertFalse(os.path.isabs(path), msg=path)
                else:
                    if absolute:
                        self.assertTrue(os.path.isabs(value), msg=value)
                    else:
                        self.assertFalse(os.path.isabs(value), msg=value)
        # test that paths conversion works
        self.handler.convert_to_relative_paths()
        check_conversion(False)
        relcopy = self.handler.copy()
        self.handler.convert_to_absolute_paths()
        check_conversion(True)
        # convert to relative path and compare
        # convert back and check everything is the same
        self.handler.convert_to_relative_paths()
        self.assertEqual(relcopy, self.handler)


class BaseInputFileHandlerTest(BaseWritableFileHandlerTest):
    _inputs_vars = None

    def setUp(self):
        super().setUp(write=False)
        self.handler.input_variables = self._input_vars
        self.handler.write()
