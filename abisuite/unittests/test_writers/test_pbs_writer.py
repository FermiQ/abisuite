import unittest

from ... import __IMPLEMENTED_QUEUING_SYSTEMS__
from .bases import BaseWriterTest
from ..routines_for_tests import create_pbs_test_writer
from ...handlers.file_parsers import PBSParser
from ...handlers.file_writers import PBSWriter


class TestPBSWriter(BaseWriterTest, unittest.TestCase):
    """Test case for the PBS file writer.
    """
    _parser_class = PBSParser
    _writer_class = PBSWriter

    # TODO: rebase this test with the meta data writer test
    def setUp(self):
        super().setUp(write=False)
        create_pbs_test_writer(self.writer)
        self.writer.write()

    def test_writing(self):
        super().test_writing()
        # try writing it using all possible queuing systems
        for queuing_system in __IMPLEMENTED_QUEUING_SYSTEMS__:
            self.writer.queuing_system = queuing_system
            self.writer.write(overwrite=True)
            # compare result
            newpbs = PBSParser.from_file(self.writer.path)
            newpbs.read()
            self.assertEqual(
                    self.writer.structure, newpbs.structure,
                    msg=(
                        f"{queuing_system}:\n{self.writer.structure}\n!="
                        f"\n{newpbs.structure}"
                        )
                    )

    def test_wrong_queuing_system(self):
        with self.assertRaises(ValueError):
            self.writer.queuing_system = "wrong queuing system"
            self.writer.write(overwrite=True)

    def test_grid_engine_raise_error_when_no_queue(self):
        with self.assertRaises(ValueError):
            self.writer.queue = None
            self.writer.queuing_system = "grid_engine"
            self.writer.write(overwrite=True)
