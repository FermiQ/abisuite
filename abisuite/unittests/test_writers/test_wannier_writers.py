from .bases import BaseInputWriterTest
from ..variables_for_tests import (
        wannier90_vars,
        )
from ...handlers.file_parsers import (
        Wannier90InputParser,
        )
from ...handlers.file_writers import (
        Wannier90InputWriter,
        )
import unittest


class TestWannier90InputWriter(BaseInputWriterTest, unittest.TestCase):
    _input_vars = wannier90_vars.copy()
    _parser_class = Wannier90InputParser
    _writer_class = Wannier90InputWriter
