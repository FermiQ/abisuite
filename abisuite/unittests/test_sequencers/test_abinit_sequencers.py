import os
import pytest
import shutil
import tempfile

from abisuite import (
        AbinitBandStructureSequencer, AbinitBandStructureComparatorSequencer,
        AbinitEcutConvergenceSequencer, AbinitEcutPhononConvergenceSequencer,
        AbinitKgridConvergenceSequencer, AbinitKgridPhononConvergenceSequencer,
        AbinitIndividualPhononSequencer, AbinitOpticSequencer,
        AbinitPhononDispersionSequencer, AbinitRelaxationSequencer,
        AbinitSmearingConvergenceSequencer,
        AbinitSmearingPhononConvergenceSequencer,
        )
from .bases import (
        BaseBandStructureSequencerTest,
        BasePhononSequencerTest, BaseSCFSequencerTest,
        BasePhononConvergenceSequencerTest,
        BaseEcutPhononConvergenceSequencerTest,
        BaseEcutConvergenceSequencerTest, BaseConvergenceSequencerTest,
        BaseKgridConvergenceSequencerTest,
        BaseKgridPhononConvergenceSequencerTest, BaseRelaxationSequencerTest,
        BaseSmearingConvergenceSequencerTest,
        BaseSmearingPhononConvergenceSequencerTest,
        )
from ..bases import TestCase
from ..routines_for_tests import copy_calculation
from ...exceptions import DevError
from ...handlers import (
        CalculationDirectory
        )


here = os.path.dirname(os.path.abspath(__file__))


class BaseAbinitSCFSequencerTest(BaseSCFSequencerTest):
    """Base class for SCF sequencer test cases with abinit.
    """
    _scf_script_name = "abinit"
    _pseudos = None

    def setUp(self):
        BaseSCFSequencerTest.setUp(self)
        if self._pseudos is None:
            raise DevError(
                    f"Need to set '_pseudos' in '{self.__class__}'.")
        self.sequencer.scf_pseudos = self._pseudos


# #############################################################################
# ################# Relaxation Sequencer Test #################################
# #############################################################################
ABINIT_RELAX_EXAMPLE = os.path.join(
        here, "files", "abinit_AlAs_relaxation", "relax_run")
ABINIT_RELAX_INPUT_VARIABLES = {
        "autoparal": 1,
        "toldfe": 1e-10,
        "acell": [10.61] * 3,
        "rprim": [[0.0, 0.5, 0.5], [0.5, 0.0, 0.5], [0.5, 0.5, 0.0]],
        "ntypat": 2,
        "znucl": [13, 33],
        "natom": 2,
        "typat": [1, 2],
        "xred": [[0.0, 0.0, 0.0], [0.25, 0.25, 0.25]],
        "nband": 4,
        "ixc": 1,
        "ngkpt": [4, 4, 4],
        "nshiftk": 4,
        "shiftk": [[0.0, 0.0, 0.5], [0.0, 0.5, 0.0],
                   [0.5, 0.0, 0.0], [0.5, 0.5, 0.5]],
        "nstep": 25,
        "diemac": 9.0,
        "tolmxf": 1e-5,
        "ntime": 20,
        "ionmov": 22,
        "ecutsm": 0.5,
        "ecut": 20.0,
        }
PSEUDOS = [
        os.path.join(here, "files", "pseudos", "13al.981214.fhi"),
        os.path.join(here, "files", "pseudos", "33as.pspnc"),
        ]


@pytest.mark.order("first")
class TestAbinitRelaxationSequencer(
        BaseAbinitSCFSequencerTest, BaseRelaxationSequencerTest, TestCase):
    """Tests the AbinitRelaxationSequencer class.
    """
    _scf_example = ABINIT_RELAX_EXAMPLE
    _scf_input_variables = ABINIT_RELAX_INPUT_VARIABLES.copy()
    _sequencer_class = AbinitRelaxationSequencer
    _pseudos = PSEUDOS

    def _set_relax_atoms_done(self):
        copy_calculation(
                os.path.join(self._scf_example, "relax_atoms_only"),
                os.path.join(self.sequencer.scf_workdir, "relax_atoms_only"),
                new_pseudos=self._pseudos)

    def _set_relax_cell_done(self):
        if self.sequencer.relax_atoms:
            new_parents = [os.path.join(
                self.sequencer.scf_workdir, "relax_atoms_only")]
        else:
            new_parents = []
        copy_calculation(
                os.path.join(self._scf_example, "relax_full_cell_geometry"),
                os.path.join(
                    self.sequencer.scf_workdir, "relax_full_cell_geometry"),
                new_pseudos=self._pseudos,
                new_parents=new_parents)


class BaseAbinitConvergenceSequencerTest(
        BaseConvergenceSequencerTest, BaseAbinitSCFSequencerTest):
    """Base class for convergence sequencer unittests using abinit.
    """
    def setUp(self):
        BaseConvergenceSequencerTest.setUp(self)
        BaseAbinitSCFSequencerTest.setUp(self)

    def _set_scf_done(self):
        BaseConvergenceSequencerTest._set_scf_done(self)


class BaseAbinitPhononSequencerTest(
        BasePhononSequencerTest, BaseAbinitSCFSequencerTest):
    """Base class for phonons sequencer test cases with abinit.
    """
    _phonons_qpoint_grid = None
    _phonons_script_name = "abinit"

    def setUp(self, set_qpts=True):
        if self._phonons_qpoint_grid is None:
            raise DevError(
                    f"Need to set '_phonons_qpoint_grid' in "
                    f"'{self.__class__}'.")
        BasePhononSequencerTest.setUp(self)
        BaseAbinitSCFSequencerTest.setUp(self)
        if set_qpts:
            self.sequencer.phonons_qpoint_grid = self._phonons_qpoint_grid

    def _set_phonons_except_gamma_done(self):
        dirname = os.path.dirname(self._phonons_example)
        for calc in os.listdir(dirname):
            if calc.endswith("1"):
                # gamma is first phonon skip it
                continue
            copy_calculation(
                    os.path.join(dirname, calc),
                    os.path.join(
                        os.path.dirname(self.sequencer.phonons_workdir), calc),
                    new_pseudos=self._pseudos,
                    new_parents=[self.scf_workdir],
                    )

    def _set_phonons_done(self):
        ex_dirname = os.path.dirname(self._phonons_example)
        new_dirname = os.path.dirname(self.sequencer.phonons_workdir)
        for calc in os.listdir(ex_dirname):
            source = os.path.join(ex_dirname, calc)
            target = os.path.join(new_dirname, calc)
            new_parents = [self.scf_workdir]
            if calc.endswith("1") and (
                    self.sequencer.compute_electric_field_response):
                # gamma
                new_parents.append(self.ddk_workdir)
            if os.path.isdir(target):
                continue
            copy_calculation(
                    source, target,
                    new_pseudos=self._pseudos,
                    new_parents=new_parents)


class BaseAbinitPhononConvergenceSequencerTest(
        BasePhononConvergenceSequencerTest, BaseAbinitPhononSequencerTest):
    """Base test case class for phonons convergence sequencers with abinit.
    """
    _phonons_qpoint_grid = [[0.0, 0.0, 0.0]]

    def setUp(self):
        BasePhononConvergenceSequencerTest.setUp(self)
        BaseAbinitPhononSequencerTest.setUp(self, set_qpts=False)


class BaseAbinitPhononConvergenceWithDDKSequencerTest(
        BaseAbinitPhononConvergenceSequencerTest):
    """Base test case class for phonons convergence sequencers with abinit.
    """

    def _check_ddk_written(self):
        name = self.sequencer._parameter_to_converge_input_variable_name
        self.assertEqual(
                len(os.listdir(os.path.join(self.ddk_workdir, name))),
                len(self.sequencer._parameters_to_converge))

    def _check_ddk_not_written(self):
        self.assertFalse(os.path.isdir(self.ddk_workdir))

    def _set_ddk_tempdir(self):
        if hasattr(self, "ddk_tempdir"):
            return
        self.ddk_tempdir = tempfile.TemporaryDirectory()

    def _set_ddk(self):
        if self._ddk_input_variables is None:
            raise DevError(
                    f"Need to set '_ddk_input_variables' in "
                    f"'{self.__class__}'.")
        self.sequencer.ddk_input_variables = self._ddk_input_variables

    def _set_ddk_done(self):
        name = self.sequencer._parameter_to_converge_input_variable_name
        for example in self._ddk_example:
            basename = os.path.basename(example)
            dest = os.path.join(self.ddk_workdir, name, basename)
            copy_calculation(
                    example, dest, new_pseudos=self._pseudos,
                    new_parents=[
                        os.path.join(self.scf_workdir, name, basename)])

    def _set_phonons_done(self):
        name = self.sequencer._parameter_to_converge_input_variable_name
        for example in self._phonons_example:
            basename = os.path.basename(example)
            dest = os.path.join(self.phonons_workdir, name, basename)
            copy_calculation(
                    example, dest, new_pseudos=self._pseudos,
                    new_parents=[
                        os.path.join(self.scf_workdir, name, basename),
                        os.path.join(self.ddk_workdir, name, basename)],
                        )


QPTS = [
        [0.0, 0.0, 0.0],
        [0.25, 0.0, 0.0],
        [0.5, 0.0, 0.0],
        [0.25, 0.25, 0.0],
        [0.5, 0.25, 0.0],
        [-0.25, 0.25, 0.0],
        [0.5, 0.5, 0.0],
        [-0.25, 0.5, 0.25],
        ]
NPHONONS = len(QPTS)
SCF_EXAMPLE = os.path.join(
        here, "files", "abinit_Si_phonons", "scf_run")
PHONONS_EXAMPLE = os.path.join(
        here, "files", "abinit_Si_phonons", "ph_runs", "ph_run")
DDK_EXAMPLE = os.path.join(
        here, "files", "abinit_Si_phonons", "ddk")
SCF_INPUT_VARIABLES = {
        "tolvrs": 1.0e-18,
        "acell": [10.61] * 3,
        "rprim": [[0.0, 0.5, 0.5], [0.5, 0.0, 0.5], [0.5, 0.5, 0.0]],
        "ntypat": 2,
        "znucl": [13, 33],
        "natom": 2,
        "typat": [1, 2],
        "xred": [[0.0, 0.0, 0.0], [0.25, 0.25, 0.25]],
        "nband": 4,
        "ixc": 1,
        "ecut": 3.0,
        "ngkpt": [4, 4, 4],
        "nshiftk": 4,
        "shiftk": [[0.0, 0.0, 0.5], [0.0, 0.5, 0.0],
                   [0.5, 0.0, 0.0], [0.5, 0.5, 0.5]],
        "nstep": 25,
        "diemac": 9.0
        }
DDK_INPUT_VARIABLES = {
        "tolwfr": 1.0e-22,
        "nstep": 25,
        "diemac": 9.0,
        "rfdir": [1, 1, 1],
        }
PHONONS_INPUT_VARIABLES = {
            "rfatpol": [1, 2],
            "tolvrs": 1.0e-8,
            "diemac": 9.0,
            "nstep": 25,
            }

# #############################################################################
# ############### abinit convergence sequencer test cases #####################
# #############################################################################

ECUTS = [3.0, 5.0]
root_example = os.path.join(
        here, "files", "abinit_AlAs_ecut_convergence_noddk", "scf_run", "ecut")
SCF_ECUTS_EXAMPLES = [
        os.path.join(root_example, x) for x in os.listdir(root_example)]
SCF_NO_ECUT_INPUT_VARIABLES = {
        "tolvrs": 1.0e-18,
        "acell": [10.61] * 3,
        "rprim": [[0.0, 0.5, 0.5], [0.5, 0.0, 0.5], [0.5, 0.5, 0.0]],
        "ntypat": 2,
        "znucl": [13, 33],
        "natom": 2,
        "typat": [1, 2],
        "xred": [[0.0, 0.0, 0.0], [0.25, 0.25, 0.25]],
        "nband": 4,
        "ixc": 1,
        "ngkpt": [4, 4, 4],
        "nshiftk": 4,
        "shiftk": [[0.0, 0.0, 0.5], [0.0, 0.5, 0.0],
                   [0.5, 0.0, 0.0], [0.5, 0.5, 0.5]],
        "nstep": 25,
        "diemac": 9.0
        }


@pytest.mark.order("first")
class TestAbinitEcutConvergenceSequencer(
        BaseEcutConvergenceSequencerTest,
        BaseAbinitConvergenceSequencerTest, TestCase):
    """Test case for the abinit ecut convergence sequencer.
    """
    _ecuts = ECUTS
    _scf_input_variables = SCF_NO_ECUT_INPUT_VARIABLES.copy()
    _scf_example = SCF_ECUTS_EXAMPLES
    _pseudos = PSEUDOS
    _sequencer_class = AbinitEcutConvergenceSequencer

    def setUp(self):
        BaseEcutConvergenceSequencerTest.setUp(self)
        BaseAbinitConvergenceSequencerTest.setUp(self)
        self.sequencer.ecuts_input_variable_name = "ecut"

    def _set_scf_done(self):
        BaseAbinitConvergenceSequencerTest._set_scf_done(self)


root_example = os.path.join(
        here, "files", "abinit_AlAs_ecut_convergence_noddk", "ph_run", "ecut")
PHONONS_ECUT_CONVERGENCE_NO_DDK_EXAMPLES = [
        os.path.join(root_example, x) for x in os.listdir(root_example)]
root_example = os.path.join(
        here, "files", "abinit_AlAs_ecut_convergence", "ph_run", "ecut")
PHONONS_ECUT_CONVERGENCE_WITH_DDK_EXAMPLES = [
        os.path.join(root_example, x) for x in os.listdir(root_example)]
PHONONS_CONVERGENCE_INPUT_VARIABLES = {
            "rfatpol": [1, 2],
            "tolvrs": 1.0e-8,
            "diemac": 9.0,
            "nstep": 25,
            }


@pytest.mark.order("first")
class TestAbinitEcutPhononConvergenceSequencerWithoutDDK(
        BaseEcutPhononConvergenceSequencerTest,
        BaseAbinitPhononConvergenceSequencerTest, TestCase):
    """Test case for the Abinit ecut phonons convergence sequencer without
    electric field response.
    """
    _ecuts = ECUTS
    _phonons_example = PHONONS_ECUT_CONVERGENCE_NO_DDK_EXAMPLES
    _phonons_input_variables = PHONONS_CONVERGENCE_INPUT_VARIABLES.copy()
    _scf_input_variables = SCF_NO_ECUT_INPUT_VARIABLES.copy()
    _scf_example = SCF_ECUTS_EXAMPLES
    _pseudos = PSEUDOS
    _sequencer_class = AbinitEcutPhononConvergenceSequencer

    def setUp(self):
        BaseEcutPhononConvergenceSequencerTest.setUp(self)
        BaseAbinitPhononConvergenceSequencerTest.setUp(self)
        self.sequencer.ecuts_input_variable_name = "ecut"
        self.sequencer.compute_electric_field_response = False

    def _set_scf_done(self):
        BaseAbinitConvergenceSequencerTest._set_scf_done(self)


root_example = os.path.join(
        here, "files", "abinit_AlAs_ecut_convergence", "ddk", "ecut")
PHONONS_ECUT_CONVERGENCE_DDK_EXAMPLES = [
        os.path.join(root_example, x) for x in os.listdir(root_example)]


@pytest.mark.order("first")
class TestAbinitEcutPhononConvergenceSequencerWithDDK(
        BaseEcutPhononConvergenceSequencerTest,
        BaseAbinitPhononConvergenceWithDDKSequencerTest, TestCase):
    """Test case for the Abinit ecut phonons convergence sequencer with
    electric field response.
    """
    _ecuts = ECUTS
    _phonons_example = PHONONS_ECUT_CONVERGENCE_WITH_DDK_EXAMPLES
    _phonons_input_variables = PHONONS_CONVERGENCE_INPUT_VARIABLES.copy()
    _scf_input_variables = SCF_NO_ECUT_INPUT_VARIABLES.copy()
    _scf_example = SCF_ECUTS_EXAMPLES
    _pseudos = PSEUDOS
    _ddk_example = PHONONS_ECUT_CONVERGENCE_DDK_EXAMPLES
    _ddk_input_variables = DDK_INPUT_VARIABLES.copy()
    _sequencer_class = AbinitEcutPhononConvergenceSequencer

    def setUp(self):
        BaseEcutPhononConvergenceSequencerTest.setUp(self)
        BaseAbinitPhononConvergenceSequencerTest.setUp(self)
        self.sequencer.compute_electric_field_response = True
        self.sequencer.ecuts_input_variable_name = "ecut"
        self._set_ddk_tempdir()
        self.ddk_workdir = os.path.join(self.ddk_tempdir.name, "ddk_run")
        self.sequencer.ddk_workdir = self.ddk_workdir
        self.sequencer.ddk_command = self.phonons_command
        self.sequencer.ddk_queuing_system = "local"

    def tearDown(self):
        super().tearDown()
        if os.path.isdir(self.ddk_tempdir.name):
            self.ddk_tempdir.cleanup()

    def test_whole_sequence(self):
        self._set_scf()
        self._set_ddk()
        self._set_phonons()
        self.sequencer.write()
        self._check_scf_written()
        self._check_ddk_not_written()
        self._check_phonons_not_written()
        self.assertFalse(self.sequencer.sequence_completed)
        # continue after scf calc
        self.scf_tempdir.cleanup()
        self._set_scf_done()
        self.sequencer.write()
        self._check_ddk_written()
        self._check_phonons_not_written()
        self.assertFalse(self.sequencer.sequence_completed)
        # continue after ddk calc and non gamma phonons
        self.ddk_tempdir.cleanup()
        self.phonons_tempdir.cleanup()
        self._set_ddk_done()
        self._set_phonons_done()
        self.sequencer.launch()
        self.assertTrue(self.sequencer.sequence_completed)
        self._check_plot_written()

    def test_starting_from_ddk(self):
        # Test that starting will all phonons and ddk but no gamma works
        # this is tested since gamma phonons with electric response function
        # calculation relies on the ddk calculation
        self._set_scf()
        self._set_ddk()
        self._set_scf_done()
        self._set_ddk_done()
        self._set_phonons()
        self.sequencer.write()
        self._check_phonons_written()
        self.assertFalse(self.sequencer.sequence_completed)

    def test_starting_from_scf(self):
        self._set_scf()
        self._set_scf_done()
        self._set_ddk()
        self._set_phonons()
        self.sequencer.write()
        # when computing electric field response, all phonons except gamma
        # should be written because gamma relies on the ddks
        self._check_scf_written()
        self._check_phonons_not_written()
        self._check_ddk_written()
        self.assertFalse(self.sequencer.sequence_completed)

    def test_starting_from_scratch(self):
        self._set_scf()
        self.sequencer.write()
        self._check_scf_written()
        self._check_ddk_not_written()
        self._check_phonons_not_written()
        self.assertFalse(self.sequencer.sequence_completed)

    def _set_scf_done(self):
        BaseAbinitConvergenceSequencerTest._set_scf_done(self)


KGRIDS = [[4, 4, 4], [5, 5, 5]]
root_example = os.path.join(
        here, "files", "abinit_AlAs_kgrid_convergence_noddk", "scf_run",
        "ngkpt")
SCF_KGRIDS_EXAMPLES = [
        os.path.join(root_example, x) for x in os.listdir(root_example)]
SCF_NO_KGRID_INPUT_VARIABLES = {
        "tolvrs": 1.0e-18,
        "acell": [10.61] * 3,
        "rprim": [[0.0, 0.5, 0.5], [0.5, 0.0, 0.5], [0.5, 0.5, 0.0]],
        "ntypat": 2,
        "znucl": [13, 33],
        "natom": 2,
        "typat": [1, 2],
        "xred": [[0.0, 0.0, 0.0], [0.25, 0.25, 0.25]],
        "nband": 4,
        "ixc": 1,
        "ecut": 3.0,
        "nshiftk": 4,
        "shiftk": [[0.0, 0.0, 0.5], [0.0, 0.5, 0.0],
                   [0.5, 0.0, 0.0], [0.5, 0.5, 0.5]],
        "nstep": 25,
        "diemac": 9.0
        }


@pytest.mark.order("first")
class TestAbinitKgridConvergenceSequencer(
        BaseKgridConvergenceSequencerTest,
        BaseAbinitConvergenceSequencerTest, TestCase):
    """Test case for the abinit kgrid convergence sequencer.
    """
    _kgrids = KGRIDS
    _scf_input_variables = SCF_NO_KGRID_INPUT_VARIABLES.copy()
    _scf_example = SCF_KGRIDS_EXAMPLES
    _sequencer_class = AbinitKgridConvergenceSequencer
    _pseudos = PSEUDOS

    def setUp(self):
        BaseKgridConvergenceSequencerTest.setUp(self)
        BaseAbinitConvergenceSequencerTest.setUp(self)
        self.sequencer.kgrids_input_variable_name = "ngkpt"

    def _set_scf_done(self):
        BaseAbinitConvergenceSequencerTest._set_scf_done(self)


root_example = os.path.join(
        here, "files", "abinit_AlAs_kgrid_convergence", "ddk",
        "ngkpt")
PHONONS_KGRID_CONVERGENCE_DDK_EXAMPLES = [
        os.path.join(root_example, x) for x in os.listdir(root_example)]
root_example = os.path.join(
        here, "files", "abinit_AlAs_kgrid_convergence_noddk", "ph_run",
        "ngkpt")
PHONONS_KGRID_CONVERGENCE_NO_DDK_EXAMPLES = [
        os.path.join(root_example, x) for x in os.listdir(root_example)]
root_example = os.path.join(
        here, "files", "abinit_AlAs_kgrid_convergence", "ph_run",
        "ngkpt")
PHONONS_KGRID_CONVERGENCE_WITH_DDK_EXAMPLES = [
        os.path.join(root_example, x) for x in os.listdir(root_example)]


# TODO: rebase this with TestAbinitEcutPhononConvergenceSequencerWithDDK
@pytest.mark.order("first")
class TestAbinitKgridPhononConvergenceSequencerWithDDK(
        BaseKgridPhononConvergenceSequencerTest,
        BaseAbinitPhononConvergenceWithDDKSequencerTest, TestCase,
        ):
    """Test case for the abinit kgrids phonon convergence sequencer."""
    _kgrids = KGRIDS
    _phonons_input_variables = PHONONS_CONVERGENCE_INPUT_VARIABLES.copy()
    _phonons_example = PHONONS_KGRID_CONVERGENCE_WITH_DDK_EXAMPLES
    _scf_input_variables = SCF_NO_KGRID_INPUT_VARIABLES.copy()
    _scf_example = SCF_KGRIDS_EXAMPLES
    _sequencer_class = AbinitKgridPhononConvergenceSequencer
    _pseudos = PSEUDOS
    _ddk_example = PHONONS_KGRID_CONVERGENCE_DDK_EXAMPLES
    _ddk_input_variables = DDK_INPUT_VARIABLES.copy()

    def setUp(self):
        BaseKgridPhononConvergenceSequencerTest.setUp(self)
        BaseAbinitPhononConvergenceSequencerTest.setUp(self)
        self.sequencer.compute_electric_field_response = True
        self.sequencer.kgrids_input_variable_name = "ngkpt"
        self._set_ddk_tempdir()
        self.ddk_workdir = os.path.join(self.ddk_tempdir.name, "ddk_run")
        self.sequencer.ddk_workdir = self.ddk_workdir
        self.sequencer.ddk_command = self.phonons_command
        self.sequencer.ddk_queuing_system = "local"

    def tearDown(self):
        super().tearDown()
        if os.path.isdir(self.ddk_tempdir.name):
            self.ddk_tempdir.cleanup()

    def test_whole_sequence(self):
        self._set_scf()
        self._set_ddk()
        self._set_phonons()
        self.sequencer.write()
        self._check_scf_written()
        self._check_ddk_not_written()
        self._check_phonons_not_written()
        self.assertFalse(self.sequencer.sequence_completed)
        # continue after scf calc
        self.scf_tempdir.cleanup()
        self._set_scf_done()
        self.sequencer.write()
        self._check_ddk_written()
        self._check_phonons_not_written()
        self.assertFalse(self.sequencer.sequence_completed)
        # continue after ddk calc and non gamma phonons
        self.ddk_tempdir.cleanup()
        self.phonons_tempdir.cleanup()
        self._set_ddk_done()
        self._set_phonons_done()
        self.sequencer.launch()
        self.assertTrue(self.sequencer.sequence_completed)
        self._check_plot_written()

    def test_starting_from_ddk(self):
        # Test that starting will all phonons and ddk but no gamma works
        # this is tested since gamma phonons with electric response function
        # calculation relies on the ddk calculation
        self._set_scf()
        self._set_ddk()
        self._set_scf_done()
        self._set_ddk_done()
        self._set_phonons()
        self.sequencer.write()
        self._check_phonons_written()
        self.assertFalse(self.sequencer.sequence_completed)

    def test_starting_from_scf(self):
        self._set_scf()
        self._set_scf_done()
        self._set_ddk()
        self._set_phonons()
        self.sequencer.write()
        # when computing electric field response, all phonons except gamma
        # should be written because gamma relies on the ddks
        self._check_scf_written()
        self._check_phonons_not_written()
        self._check_ddk_written()
        self.assertFalse(self.sequencer.sequence_completed)

    def test_starting_from_scratch(self):
        self._set_scf()
        self.sequencer.write()
        self._check_scf_written()
        self._check_ddk_not_written()
        self._check_phonons_not_written()
        self.assertFalse(self.sequencer.sequence_completed)

    def _set_scf_done(self):
        BaseAbinitConvergenceSequencerTest._set_scf_done(self)


@pytest.mark.order("first")
class TestAbinitKgridPhononConvergenceSequencerWithoutDDK(
        BaseKgridPhononConvergenceSequencerTest,
        BaseAbinitPhononConvergenceSequencerTest, TestCase,
        ):
    """Test case for the abinit kgrids phonon convergence sequencer."""
    _kgrids = KGRIDS
    _phonons_input_variables = PHONONS_CONVERGENCE_INPUT_VARIABLES.copy()
    _phonons_example = PHONONS_KGRID_CONVERGENCE_NO_DDK_EXAMPLES
    _scf_input_variables = SCF_NO_KGRID_INPUT_VARIABLES.copy()
    _scf_example = SCF_KGRIDS_EXAMPLES
    _sequencer_class = AbinitKgridPhononConvergenceSequencer
    _pseudos = PSEUDOS

    def setUp(self):
        BaseKgridPhononConvergenceSequencerTest.setUp(self)
        BaseAbinitPhononConvergenceSequencerTest.setUp(self)
        self.sequencer.compute_electric_field_response = False
        self.sequencer.kgrids_input_variable_name = "ngkpt"

    def _set_scf_done(self):
        BaseAbinitConvergenceSequencerTest._set_scf_done(self)


# ################# ABINIT smearing convergence sequencer tests ##############
ABINIT_SMEARING_CONVERGENCE_INPUT_VARS = {
            "acell": [7.6] * 3,
            "rprim": [[0, 0.5, 0.5], [0.5, 0, 0.5], [0.5, 0.5, 0]],
            "ntypat": 1,
            "znucl": [13],
            "natom": 1,
            "typat": [1],
            "xred": [0, 0, 0],
            "ecut": 6,
            "nshiftk": 4,
            "shiftk": [[0.5, 0.5, 0.5], [0.5, 0.0, 0.0],
                       [0, 0.5, 0], [0, 0, 0.5]],
            "nstep": 10,
            "toldfe": 1e-6,
            "occopt": 4,
            }
ABINIT_SMEARING_CONVERGENCE_SMEARINGS = [1, 0.1, 0.01]
ABINIT_SMEARING_CONVERGENCE_KGRIDS = [[x, x, x] for x in [2, 5, 10, 20]]
ABINIT_SMEARING_CONVERGENCE_SCF_EXAMPLE = os.path.join(
            here, "files", "abinit_Al_smearing_convergence",
            "scf_runs", "tsmear")
ABINIT_SMEARING_CONVERGENCE_PSEUDOS = [
            os.path.join(here, "files", "pseudos", "Al.psp8"),
            ]


@pytest.mark.order("first")
class TestAbinitSmearingConvergenceSequencer(
        BaseSmearingConvergenceSequencerTest,
        BaseAbinitConvergenceSequencerTest, TestCase):
    """Test case for the AbinitSmearingConvergenceSequencer class.
    """
    _kgrids = ABINIT_SMEARING_CONVERGENCE_KGRIDS
    _pseudos = ABINIT_SMEARING_CONVERGENCE_PSEUDOS
    _scf_example = ABINIT_SMEARING_CONVERGENCE_SCF_EXAMPLE
    _scf_input_variables = ABINIT_SMEARING_CONVERGENCE_INPUT_VARS
    _sequencer_class = AbinitSmearingConvergenceSequencer
    _smearings = ABINIT_SMEARING_CONVERGENCE_SMEARINGS

    def setUp(self):
        BaseSmearingConvergenceSequencerTest.setUp(self)
        BaseAbinitConvergenceSequencerTest.setUp(self)
        self.sequencer.kgrids_input_variable_name = "ngkpt"

    def _set_scf_done(self):
        BaseSmearingConvergenceSequencerTest._set_scf_done(
                self, new_pseudos=self._pseudos)


ABINIT_SMEARING_CONVERGENCE_PHONONS_EXAMPLE = os.path.join(
            here, "files", "abinit_Al_smearing_convergence",
            "phonons_runs", "tsmear")


@pytest.mark.order("first")
class TestAbinitSmearingPhononConvergenceSequencer(
        BaseSmearingPhononConvergenceSequencerTest,
        BaseAbinitPhononConvergenceSequencerTest, TestCase):
    """Test case for the Abinit smearing convergence sequencer.
    """
    _kgrids = ABINIT_SMEARING_CONVERGENCE_KGRIDS
    _smearings = ABINIT_SMEARING_CONVERGENCE_SMEARINGS
    _nphonons = 1
    _phonons_example = ABINIT_SMEARING_CONVERGENCE_PHONONS_EXAMPLE
    _phonons_input_variables = {
        "rfatpol": [1, 1],
        "tolvrs": 1e-8,
        "diemac": 9.0,
        "nstep": 25,
        }
    _scf_input_variables = ABINIT_SMEARING_CONVERGENCE_INPUT_VARS.copy()
    _scf_example = ABINIT_SMEARING_CONVERGENCE_SCF_EXAMPLE
    _pseudos = ABINIT_SMEARING_CONVERGENCE_PSEUDOS
    _sequencer_class = AbinitSmearingPhononConvergenceSequencer

    def setUp(self):
        BaseSmearingPhononConvergenceSequencerTest.setUp(self)
        BaseAbinitPhononConvergenceSequencerTest.setUp(self)
        self.sequencer.kgrids_input_variable_name = "ngkpt"

    def _set_scf_done(self):
        BaseSmearingPhononConvergenceSequencerTest._set_scf_done(
                self, new_pseudos=self._pseudos)

    def _set_phonons_done(self):
        BaseSmearingPhononConvergenceSequencerTest._set_phonons_done(
                self, new_pseudos=self._pseudos)


# #############################################################################
# ############### abinit individual phonon sequencer test case ################
# #############################################################################
SCF_INPUT_VARIABLES = {
        "ecut": 5.0,
        "tolvrs": 1.0e-18,
        "acell": [10.61] * 3,
        "rprim": [[0.0, 0.5, 0.5], [0.5, 0.0, 0.5], [0.5, 0.5, 0.0]],
        "ntypat": 1,
        "znucl": [14],
        "natom": 2,
        "typat": [1, 1],
        "xred": [[0.0, 0.0, 0.0], [0.25, 0.25, 0.25]],
        "ngkpt": [4, 4, 4],
        "nshiftk": 4,
        "shiftk": [[0.0, 0.0, 0.5], [0.0, 0.5, 0.0],
                   [0.5, 0.0, 0.0], [0.5, 0.5, 0.5]],
        "nstep": 25,
        "diemac": 12.0
        }
DDK_INPUT_VARIABLES = {
        "tolwfr": 1.0e-22,
        "nstep": 25,
        "diemac": 12.0,
        "rfdir": [1, 1, 1],
        }
PHONONS_INPUT_VARIABLES = {
        "rfatpol": [1, 2],
        "tolvrs": 1.0e-8,
        "diemac": 12.0,
        "nstep": 25,
        }
PSEUDOS = [os.path.join(here, "files", "pseudos", "Si.psp8")]


@pytest.mark.order("first")
class TestAbinitIndividualPhononSequencerWithDDK(
        BaseAbinitPhononSequencerTest, TestCase):
    """Test case for the AbinitIndividualPhononSequencer class.
    """
    _nphonons = NPHONONS
    _phonons_example = PHONONS_EXAMPLE
    _phonons_input_variables = PHONONS_INPUT_VARIABLES.copy()
    _phonons_qpoint_grid = QPTS
    _pseudos = PSEUDOS
    _ddk_example = DDK_EXAMPLE
    _ddk_input_variables = DDK_INPUT_VARIABLES.copy()
    _scf_example = SCF_EXAMPLE
    _scf_input_variables = SCF_INPUT_VARIABLES.copy()
    _sequencer_class = AbinitIndividualPhononSequencer

    def setUp(self):
        BaseAbinitPhononSequencerTest.setUp(self)
        self.sequencer.compute_electric_field_response = True
        self._set_ddk_tempdir()
        self.ddk_workdir = os.path.join(self.ddk_tempdir.name, "ddk_run")
        self.sequencer.ddk_workdir = self.ddk_workdir
        self.sequencer.ddk_command = self.phonons_command
        self.sequencer.ddk_queuing_system = "local"

    def tearDown(self):
        super().tearDown()
        if os.path.isdir(self.ddk_tempdir.name):
            self.ddk_tempdir.cleanup()

    def test_whole_sequence(self):
        self._set_scf()
        self._set_ddk()
        self._set_phonons()
        self.sequencer.write()
        self._check_scf_written()
        self._check_ddk_not_written()
        self._check_phonons_not_written()
        self.assertFalse(self.sequencer.sequence_completed)
        # continue after scf calc
        self.scf_tempdir.cleanup()
        self._set_scf_done()
        self.sequencer.write()
        self._check_ddk_written()
        self._check_phonons_except_gamma_written()
        self.assertFalse(self.sequencer.sequence_completed)
        # continue after ddk calc and non gamma phonons
        self.ddk_tempdir.cleanup()
        self.phonons_tempdir.cleanup()
        self._set_ddk_done()
        self._set_phonons_except_gamma_done()
        self.sequencer.write()
        self._check_phonons_gamma_written()
        self.assertFalse(self.sequencer.sequence_completed)
        # continue for gamma phonon
        # I use rmtree here cause cleanup does not seem to remove subdir
        # I don't know why...
        shutil.rmtree(self.phonons_tempdir.name)
        # self.phonons_tempdir.cleanup()
        self._set_phonons_done()
        self.sequencer.write()
        self.assertTrue(self.sequencer.sequence_completed)

    def test_starting_from_ddk_and_phonons_except_gamma(self):
        # Test that starting will all phonons and ddk but no gamma works
        # this is tested since gamma phonons with electric response function
        # calculation relies on the ddk calculation
        self._set_scf_done()
        self._set_ddk_done()
        self._set_phonons_except_gamma_done()
        self._set_phonons()
        self.sequencer.write()
        self._check_scf_written()
        self._check_ddk_written()
        self._check_phonons_written()
        self.assertFalse(self.sequencer.sequence_completed)

    def test_starting_from_ddk_no_other_phonons(self):
        # check that scf and ddk are done without any phonons
        self._set_scf_done()
        self._set_ddk_done()
        self._set_phonons()
        self.sequencer.write()
        self._check_scf_written()
        self._check_ddk_written()
        self._check_phonons_written()
        self.assertFalse(self.sequencer.sequence_completed)

    def test_starting_from_scf(self):
        self._set_scf_done()
        self._set_ddk()
        self._set_phonons()
        self.sequencer.write()
        # when computing electric field response, all phonons except gamma
        # should be written because gamma relies on the ddks
        self._check_scf_written()
        self._check_phonons_except_gamma_written()
        self._check_phonons_gamma_not_written()
        self._check_ddk_written()
        self.assertFalse(self.sequencer.sequence_completed)

    def test_starting_from_scratch(self):
        self._set_scf()
        self.sequencer.write()
        self._check_scf_written()
        self._check_ddk_not_written()
        self._check_phonons_not_written()
        self.assertFalse(self.sequencer.sequence_completed)

    def _check_ddk(self, func):
        func(CalculationDirectory.is_calculation_directory(
            self.ddk_workdir))

    def _check_ddk_written(self):
        self._check_ddk(self.assertTrue)

    def _check_ddk_not_written(self):
        self._check_ddk(self.assertFalse)

    def _check_phonons_except_gamma(self, func):
        for i in range(2, self.sequencer.nphonons + 1):
            # gamma is first phonon just skip it
            phdir = self.sequencer.phonons_workdir + str(i)
            func(CalculationDirectory.is_calculation_directory(phdir))

    def _check_phonons_except_gamma_written(self):
        self._check_phonons_except_gamma(self.assertTrue)

    def _check_phonons_gamma(self, func):
        # gamma is first phonon
        phdir = self.sequencer.phonons_workdir + str(1)
        func(CalculationDirectory.is_calculation_directory(phdir))

    def _check_phonons_gamma_not_written(self):
        self._check_phonons_gamma(self.assertFalse)

    def _check_phonons_gamma_written(self):
        self._check_phonons_gamma(self.assertTrue)

    def _set_ddk_tempdir(self):
        if hasattr(self, "ddk_tempdir"):
            return
        self.ddk_tempdir = tempfile.TemporaryDirectory()

    def _set_ddk(self):
        if self._ddk_input_variables is None:
            raise DevError(
                    f"Need to set '_ddk_input_variables' in "
                    f"'{self.__class__}'.")
        self.sequencer.ddk_input_variables = self._ddk_input_variables

    def _set_ddk_done(self):
        copy_calculation(
                self._ddk_example, self.ddk_workdir,
                new_pseudos=self._pseudos,
                new_parents=[self.scf_workdir],
                )


PHONONS_NO_DDK_EXAMPLE = os.path.join(
        here, "files", "abinit_Si_phonons_noddk", "ph_runs", "ph_run")
SCF_NO_DDK_EXAMPLE = os.path.join(
        here, "files", "abinit_Si_phonons_noddk", "scf_run")


@pytest.mark.order("first")
class TestAbinitIndividualPhononSequencerWithoutDDK(
        BaseAbinitPhononSequencerTest, TestCase):
    """Test case for the AbinitIndividualPhononSequencer class without the
    computation of the electric field response.
    """
    _nphonons = NPHONONS
    _phonons_example = PHONONS_NO_DDK_EXAMPLE
    _phonons_input_variables = PHONONS_INPUT_VARIABLES.copy()
    _phonons_qpoint_grid = QPTS
    _pseudos = PSEUDOS
    _scf_example = SCF_NO_DDK_EXAMPLE
    _scf_input_variables = SCF_INPUT_VARIABLES.copy()
    _sequencer_class = AbinitIndividualPhononSequencer

    def setUp(self):
        BaseAbinitPhononSequencerTest.setUp(self)
        self.sequencer.compute_electric_field_response = False


# #############################################################################
# ######### abinit phonon dispersion sequencer test cases #####################
# #############################################################################
PHONONS_QPOINT_GRID = [4, 4, 4]

ANADDB_INPUT_VARIABLES = {
        "ifcflag": 1,
        "ifcout": 0,
        "brav": 2,
        "nqshft": 1,
        "q1shft": [0.0] * 3,
        "chneut": 1,
        "dipdip": 1,
        "eivec": 4,
        "nph2l": 1,
        "qph2l": [1.0, 0.0, 0.0, 0.0],
        }
ANADDB_NO_DDK_INPUT_VARIABLES = {
        "ifcflag": 1,
        "ifcout": 0,
        "brav": 2,
        "nqshft": 1,
        "q1shft": [0.0] * 3,
        "chneut": 1,
        "dipdip": 1,
        "eivec": 4,
        }
QPOINT_PATH_DENSITY = 10
QPOINT_PATH = [
        {r"$\Gamma$": [0.0, 0.0, 0.0]},
        {"K": [0.3750, 0.3750, 0.7500]},
        {"X": [0.5, 0.5, 1.0]},
        {r"$\Gamma$": [1.0, 1.0, 1.0]},
        {"L": [0.5, 0.5, 0.5]},
        {"X": [0.5, 0.0, 0.5]},
        {"W": [0.5, 0.25, 0.75]},
        {"L": [0.5, 0.5, 0.5]},
        ]


class BaseAbinitPhononDispersionSequencerTest(
        BaseAbinitPhononSequencerTest):
    """Base class for abinit phonon dispersion sequencer test cases.
    """
    _nphonons = None
    _anaddb_input_variables = None

    def setUp(self):
        if self._anaddb_input_variables is None:
            raise DevError(
                    "Need to set '_anaddb_input_variables'.")
        # need to set mrgddb and anaddb calculations afterwards
        self._set_mrgddb_tempdir()
        self.mrgddb_workdir = os.path.join(
                self.mrgddb_tempdir.name, "mrgddb_run")
        self.mrgddb_command_file = tempfile.NamedTemporaryFile(suffix="mrgddb")
        self.sequencer.mrgddb_workdir = self.mrgddb_workdir
        self.sequencer.mrgddb_command = self.mrgddb_command_file.name
        self.sequencer.mrgddb_queuing_system = "local"
        self._set_anaddb_tempdir()
        self.anaddb_workdir = os.path.join(
                self.anaddb_tempdir.name, "anaddb_run")
        self.anaddb_command_file = tempfile.NamedTemporaryFile(suffix="anaddb")
        self.sequencer.anaddb_workdir = self.anaddb_workdir
        self.sequencer.anaddb_command = self.anaddb_command_file.name
        self.sequencer.anaddb_queuing_system = "local"
        self.sequencer.qpoint_path_density = QPOINT_PATH_DENSITY
        self.sequencer.qpoint_path = QPOINT_PATH
        self.sequencer.plot_show = False
        self.sequencer.plot_save = os.path.join(
                self.anaddb_workdir, "plot.pdf")

    def tearDown(self):
        super().tearDown()
        if os.path.isdir(self.mrgddb_tempdir.name):
            self.mrgddb_tempdir.cleanup()
        if os.path.isfile(self.mrgddb_command_file.name):
            self.mrgddb_command_file.close()
        if os.path.isdir(self.anaddb_tempdir.name):
            self.anaddb_tempdir.cleanup()
        if os.path.isfile(self.anaddb_command_file.name):
            self.anaddb_command_file.close()

    def _check_phonons(self, *args, **kwargs):
        # override this to take into account the 'qgrid_gen'
        BaseAbinitPhononSequencerTest._check_phonons(
                self, *args, exceptions="qgrid_gen", **kwargs)

    def _check_mrgddb(self, func):
        func(CalculationDirectory.is_calculation_directory(
            self.mrgddb_workdir))

    def _check_mrgddb_written(self):
        self._check_mrgddb(self.assertTrue)

    def _check_mrgddb_not_written(self):
        self._check_mrgddb(self.assertFalse)

    def _check_anaddb(self, func):
        func(CalculationDirectory.is_calculation_directory(
            self.anaddb_workdir))

    def _check_anaddb_written(self):
        self._check_anaddb(self.assertTrue)

    def _check_anaddb_not_written(self):
        self._check_anaddb(self.assertFalse)

    def _set_mrgddb_tempdir(self):
        if hasattr(self, "mrgddb_tempdir"):
            return
        self.mrgddb_tempdir = tempfile.TemporaryDirectory()

    def _set_mrgddb(self):
        # nothing to do
        return

    def _set_mrgddb_done(self):
        dirname = os.path.dirname(self.sequencer.phonons_workdir)
        copy_calculation(
                self._mrgddb_example, self.mrgddb_workdir,
                new_parents=[
                    os.path.join(dirname, x)
                    for x in os.listdir(dirname) if "qgrid_gen" not in x
                    ]
                )

    def _set_phonons(self):
        BaseAbinitPhononSequencerTest._set_phonons(self)
        # also copy the qpt generation run
        if os.path.isdir(self.sequencer.phonons_workdir + "_qgrid_generation"):
            return
        copy_calculation(
                self._phonons_example + "_qgrid_generation",
                self.sequencer.phonons_workdir + "_qgrid_generation",
                new_pseudos=self._pseudos)

    def _set_anaddb_tempdir(self):
        if hasattr(self, "anaddb_tempdir"):
            return
        self.anaddb_tempdir = tempfile.TemporaryDirectory()

    def _set_anaddb(self):
        if self._anaddb_input_variables is None:
            raise DevError(
                    f"Need to set '_anaddb_input_variables' in "
                    f"'{self.__class__}'.")
        self.sequencer.anaddb_input_variables = self._anaddb_input_variables

    def _set_anaddb_done(self):
        copy_calculation(
                self._anaddb_example, self.anaddb_workdir,
                new_parents=[self.mrgddb_workdir])

    def _check_plot(self, func):
        func(os.path.isfile(self.sequencer.plot_save))

    def _check_plot_written(self):
        self._check_plot(self.assertTrue)

    def _check_plot_not_written(self):
        self._check_plot(self.assertFalse)

    def test_whole_sequence(self):
        self._set_scf_done()
        self._set_ddk_done()
        self._set_phonons()
        self._set_phonons_done()
        self._set_mrgddb()
        self._set_anaddb()
        self.sequencer.write()
        self._check_mrgddb_written()
        self._check_anaddb_not_written()
        self.assertFalse(self.sequencer.sequence_completed)
        # continue for mrgddb
        self.mrgddb_tempdir.cleanup()
        self._set_mrgddb_done()
        self.sequencer.write()
        self._check_anaddb_written()
        self.assertFalse(self.sequencer.sequence_completed)
        # set anaddb done and check plot is created
        self.anaddb_tempdir.cleanup()
        self._set_anaddb_done()
        self.sequencer.launch()
        self.assertTrue(self.sequencer.sequence_completed)
        self._check_plot_written()

    def test_starting_from_mrgddb(self):
        self._set_scf_done()
        self._set_ddk_done()
        self._set_phonons()
        self._set_phonons_done()
        self._set_mrgddb_done()
        self._set_anaddb()
        self.sequencer.write()
        self._check_anaddb_written()
        self.assertFalse(self.sequencer.sequence_completed)

    def test_starting_from_phonons(self):
        self._set_scf_done()
        self._set_ddk_done()
        self._set_phonons()
        self._set_phonons_done()
        self._set_mrgddb()
        self._set_anaddb()
        self.sequencer.write()
        self._check_mrgddb_written()
        self._check_anaddb_not_written()
        self.assertFalse(self.sequencer.sequence_completed)

    def test_starting_from_ddk_no_other_phonons(self):
        # check that scf and ddk are done without any phonons
        self._set_scf_done()
        self._set_ddk_done()
        self._set_phonons()
        self.sequencer.write()
        self._check_phonons_written()
        self._check_mrgddb_not_written()
        self._check_anaddb_not_written()
        self.assertFalse(self.sequencer.sequence_completed)

    def test_starting_from_scf(self):
        self._set_scf_done()
        self._set_ddk()
        self._set_phonons()
        self.sequencer.write()
        # when computing electric field response, all phonons except gamma
        # should be written because gamma relies on the ddks
        self._check_scf_written()
        self._check_phonons_except_gamma_written()
        self._check_phonons_gamma_not_written()
        self._check_ddk_written()
        self._check_mrgddb_not_written()
        self._check_anaddb_not_written()
        self.assertFalse(self.sequencer.sequence_completed)

    def test_starting_from_scratch(self):
        self._set_scf()
        self.sequencer.write()
        self._check_scf_written()
        self._check_ddk_not_written()
        self._check_phonons_not_written()
        self._check_mrgddb_not_written()
        self._check_anaddb_not_written()
        self.assertFalse(self.sequencer.sequence_completed)


SCF_EXAMPLE = os.path.join(
        here, "files", "abinit_Si_phonons", "scf_run")
SCF_NO_DDK_EXAMPLE = os.path.join(
        here, "files", "abinit_Si_phonons_noddk", "scf_run")
PHONONS_EXAMPLE = os.path.join(
        here, "files", "abinit_Si_phonons", "ph_runs/ph_run")
PHONONS_NO_DDK_EXAMPLE = os.path.join(
        here, "files", "abinit_Si_phonons_noddk", "ph_runs/ph_run")
DDK_EXAMPLE = os.path.join(
        here, "files", "abinit_Si_phonons", "ddk")
MRGDDB_EXAMPLE = os.path.join(
        here, "files", "abinit_Si_phonons", "mrgddb")
ANADDB_EXAMPLE = os.path.join(
        here, "files", "abinit_Si_phonons", "anaddb")
MRGDDB_NO_DDK_EXAMPLE = os.path.join(
        here, "files", "abinit_Si_phonons_noddk", "mrgddb")
ANADDB_NO_DDK_EXAMPLE = os.path.join(
        here, "files", "abinit_Si_phonons_noddk", "anaddb")


@pytest.mark.order("first")
class TestAbinitPhononDispersionWithDDKSequencer(
        TestAbinitIndividualPhononSequencerWithDDK,
        BaseAbinitPhononDispersionSequencerTest, TestCase):
    """Test case for the abinit phonon dispersion sequencer with electric
    field response calculation.
    """
    _nphonons = None
    _phonons_example = PHONONS_EXAMPLE
    _phonons_qpoint_grid = PHONONS_QPOINT_GRID
    _phonons_input_variables = PHONONS_INPUT_VARIABLES.copy()
    _pseudos = PSEUDOS
    _ddk_example = DDK_EXAMPLE
    _ddk_input_variables = DDK_INPUT_VARIABLES.copy()
    _scf_example = SCF_EXAMPLE
    _scf_input_variables = SCF_INPUT_VARIABLES.copy()
    _mrgddb_example = MRGDDB_EXAMPLE
    _anaddb_example = ANADDB_EXAMPLE
    _anaddb_input_variables = ANADDB_INPUT_VARIABLES.copy()
    _sequencer_class = AbinitPhononDispersionSequencer

    def setUp(self):
        TestAbinitIndividualPhononSequencerWithDDK.setUp(self)
        BaseAbinitPhononDispersionSequencerTest.setUp(self)
        self.sequencer.mrgddb_queuing_system = "local"
        self.sequencer.anaddb_queuing_system = "local"

    def test_starting_from_ddk_and_phonons_except_gamma(self):
        # Test that starting will all phonons and ddk but no gamma works
        # this is tested since gamma phonons with electric response function
        # calculation relies on the ddk calculation
        self._set_scf_done()
        self._set_ddk_done()
        self._set_phonons_except_gamma_done()
        self._set_phonons()
        self.sequencer.write()
        self._check_scf_written()
        self._check_ddk_written()
        self._check_phonons_written()
        self._check_mrgddb_not_written()
        self._check_anaddb_not_written()
        self.assertFalse(self.sequencer.sequence_completed)

    def test_whole_sequence(self):
        BaseAbinitPhononDispersionSequencerTest.test_whole_sequence(self)


@pytest.mark.order("first")
class TestAbinitPhononDispersionWithoutDDKSequencer(
        TestAbinitIndividualPhononSequencerWithoutDDK,
        BaseAbinitPhononDispersionSequencerTest, TestCase):
    """Test case for the abinit phonon dispersion sequencer without electric
    field response calculation.
    """
    _nphonons = None
    _phonons_example = PHONONS_NO_DDK_EXAMPLE
    _phonons_qpoint_grid = PHONONS_QPOINT_GRID
    _phonons_input_variables = PHONONS_INPUT_VARIABLES.copy()
    _pseudos = PSEUDOS
    _scf_example = SCF_NO_DDK_EXAMPLE
    _scf_input_variables = SCF_INPUT_VARIABLES.copy()
    _mrgddb_example = MRGDDB_NO_DDK_EXAMPLE
    _anaddb_example = ANADDB_NO_DDK_EXAMPLE
    _anaddb_input_variables = ANADDB_NO_DDK_INPUT_VARIABLES.copy()
    _sequencer_class = AbinitPhononDispersionSequencer

    def setUp(self):
        TestAbinitIndividualPhononSequencerWithoutDDK.setUp(self)
        BaseAbinitPhononDispersionSequencerTest.setUp(self)

    def _set_ddk(self):
        # do nothing
        pass

    def _set_ddk_done(self):
        # just do nothing
        pass

    def _check_ddk_written(self):
        # do nothing
        pass

    def _check_ddk_not_written(self):
        # do nothing
        pass

    def _check_phonons_except_gamma_written(self):
        # irrelevent here
        pass

    def _check_phonons_gamma_not_written(self):
        # irrelevent here
        pass

    def test_whole_sequence(self):
        BaseAbinitPhononDispersionSequencerTest.test_whole_sequence(self)


# ########################################################################### #
# ########################## Optic sequencers ############################### #
# ########################################################################### #


OPTIC_SCF_EXAMPLE = os.path.join(
        here, "files", "abinit_optic_GaAs", "scf_run")
OPTIC_SCF_INPUT_VARIABLES = {
        "nband": 4,
        "nstep": 25,
        "kptopt": 1,
        "nbdbuf": 0,
        "nshiftk": 4,
        "shiftk": [[0.5, 0.5, 0.5],
                   [0.5, 0.0, 0.0],
                   [0.0, 0.5, 0.0],
                   [0.0, 0.0, 0.5]],
        "ngkpt": [4, 4, 4],
        "acell": [10.60] * 3,
        "amu": [69.72, 74.9216],
        "diemac": 10.0,
        "ecut": 2.00,
        "ixc": 3,
        "natom": 2,
        "ntypat": 2,
        "rprim": [[0.0, 0.5, 0.5],
                  [0.5, 0.0, 0.5],
                  [0.5, 0.5, 0.0]],
        "xred": [[0.0, 0.0, 0.0],
                 [0.25, 0.25, 0.25]],
        # "tnons": [[0.0] * 3] * 24,
        "tnons": [[0.0] * 3] * 24,
        "typat": [1, 2],
        "tolwfr": 1e-20,
        "znucl": [31, 33],
        }
OPTIC_PSEUDOS = [
        os.path.join(here, "files", "pseudos", "31ga.pspnc"),
        os.path.join(here, "files", "pseudos", "33as.pspnc"),
        ]
OPTIC_NSCF_EXAMPLE = os.path.join(
        here, "files", "abinit_optic_GaAs", "nscf_run")
OPTIC_NSCF_INPUT_VARIABLES = OPTIC_SCF_INPUT_VARIABLES.copy()
OPTIC_NSCF_INPUT_VARIABLES.update({"nband": 20, "nbdbuf": 2})
OPTIC_NSCF_FBZ_EXAMPLE = os.path.join(
        here, "files", "abinit_optic_GaAs", "nscf_fbz_run")
OPTIC_NSCF_FBZ_INPUT_VARIABLES = {}
OPTIC_DDK_EXAMPLE = os.path.join(
        here, "files", "abinit_optic_GaAs", "ddk_run")
OPTIC_DDK_INPUT_VARIABLES = {
        "rfdir": [1, 1, 1],
        }
OPTIC_OPTIC_EXAMPLE = os.path.join(
        here, "files", "abinit_optic_GaAs", "optic_run")
OPTIC_OPTIC_INPUT_VARIABLES = {
        "broadening": 0.002,
        "domega": 0.0003,
        "maxomega": 0.3,
        "scissor": 0.000,
        "tolerance": 0.002,
        "num_lin_comp": 1,
        "lin_comp": 11,
        "num_nonlin_comp": 2,
        "nonlin_comp": [123, 222],
        "num_linel_comp": 0,
        "num_nonlin2_comp": 0,
        }


@pytest.mark.order("first")
class TestAbinitOpticSequencer(
        BaseAbinitSCFSequencerTest,
        TestCase):
    """class for the AbinitOpticSequencer test cases.
    """
    _ddk_example = OPTIC_DDK_EXAMPLE
    _ddk_input_variables = OPTIC_DDK_INPUT_VARIABLES.copy()
    _nscf_example = OPTIC_NSCF_EXAMPLE
    _nscf_input_variables = OPTIC_NSCF_INPUT_VARIABLES.copy()
    _nscffbz_example = OPTIC_NSCF_FBZ_EXAMPLE
    _nscffbz_input_variables = OPTIC_NSCF_FBZ_INPUT_VARIABLES.copy()
    _optic_example = OPTIC_OPTIC_EXAMPLE
    _optic_input_variables = OPTIC_OPTIC_INPUT_VARIABLES.copy()
    _pseudos = OPTIC_PSEUDOS
    _scf_example = OPTIC_SCF_EXAMPLE
    _scf_input_variables = OPTIC_SCF_INPUT_VARIABLES.copy()
    _sequencer_class = AbinitOpticSequencer

    def setUp(self):
        BaseAbinitSCFSequencerTest.setUp(self)
        if self._optic_input_variables is None:
            raise DevError(
                    "Need to set '_optic_input_variables'.")
        self.sequencer.compute_non_linear_optical_response = True

        self._set_nscf_tempdir()
        self.nscf_workdir = os.path.join(
                self.nscf_tempdir.name, "nscf_run")
        self.nscf_command_file = self.scf_command_file
        self.sequencer.nscf_workdir = self.nscf_workdir
        self.sequencer.nscf_command = self.nscf_command_file.name
        self.sequencer.nscf_queuing_system = "local"

        self._set_nscffbz_tempdir()
        self.nscffbz_workdir = os.path.join(
                self.nscffbz_tempdir.name, "nscf_fbz_run")
        self.nscffbz_command_file = self.scf_command_file
        self.sequencer.nscffbz_workdir = self.nscffbz_workdir
        self.sequencer.nscffbz_command = self.nscffbz_command_file.name
        self.sequencer.nscffbz_queuing_system = "local"

        self._set_ddk_tempdir()
        self.ddk_workdir = os.path.join(
                self.ddk_tempdir.name, "ddk_run")
        self.ddk_command_file = self.scf_command_file
        self.sequencer.ddk_workdir = self.ddk_workdir
        self.sequencer.ddk_command = self.ddk_command_file.name
        self.sequencer.ddk_queuing_system = "local"

        self._set_optic_tempdir()
        self.optic_workdir = os.path.join(
                self.optic_tempdir.name, "optic_run")
        self.optic_command_file = tempfile.NamedTemporaryFile(suffix="optic")
        self.sequencer.optic_workdir = self.optic_workdir
        self.sequencer.optic_command = self.optic_command_file.name
        self.sequencer.optic_queuing_system = "local"
        self.sequencer.plot_show = False
        self.sequencer.plot_save = os.path.join(
                self.optic_workdir, "plot.pdf")

    def tearDown(self):
        super().tearDown()
        if os.path.isdir(self.optic_tempdir.name):
            self.optic_tempdir.cleanup()
        if os.path.isfile(self.optic_command_file.name):
            self.optic_command_file.close()

    def _check_ddk(self, func):
        func(CalculationDirectory.is_calculation_directory(
            self.ddk_workdir))

    def _check_ddk_written(self):
        self._check_ddk(self.assertTrue)

    def _check_ddk_not_written(self):
        self._check_ddk(self.assertFalse)

    def _set_ddk_tempdir(self):
        if hasattr(self, "ddk_tempdir"):
            return
        self.ddk_tempdir = tempfile.TemporaryDirectory()

    def _set_ddk(self):
        # also need to set nscf input vars
        self._set_nscf()
        self.sequencer.ddk_input_variables = self._ddk_input_variables

    def _set_ddk_done(self):
        copy_calculation(
                self._ddk_example, self.ddk_workdir,
                new_pseudos=self._pseudos,
                new_parents=[self.sequencer.nscffbz_workdir])

    def _check_nscf(self, func):
        func(CalculationDirectory.is_calculation_directory(
            self.nscf_workdir))

    def _check_nscf_written(self):
        self._check_nscf(self.assertTrue)

    def _check_nscf_not_written(self):
        self._check_nscf(self.assertFalse)

    def _set_nscf_tempdir(self):
        if hasattr(self, "nscf_tempdir"):
            return
        self.nscf_tempdir = tempfile.TemporaryDirectory()

    def _set_nscf(self):
        self.sequencer.nscf_input_variables = self._nscf_input_variables

    def _set_nscf_done(self):
        copy_calculation(
                self._nscf_example, self.nscf_workdir,
                new_pseudos=self._pseudos,
                new_parents=[self.sequencer.scf_workdir])

    def _check_nscffbz(self, func):
        func(CalculationDirectory.is_calculation_directory(
            self.nscffbz_workdir))

    def _check_nscffbz_written(self):
        self._check_nscffbz(self.assertTrue)

    def _check_nscffbz_not_written(self):
        self._check_nscffbz(self.assertFalse)

    def _set_nscffbz_tempdir(self):
        if hasattr(self, "nscffbz_tempdir"):
            return
        self.nscffbz_tempdir = tempfile.TemporaryDirectory()

    def _set_nscffbz(self):
        # just set the nscf input vars
        self._set_nscf()

    def _set_nscffbz_done(self):
        copy_calculation(
                self._nscffbz_example, self.nscffbz_workdir,
                new_pseudos=self._pseudos,
                new_parents=[self.sequencer.nscf_workdir,
                             self.sequencer.scf_workdir])

    def _check_optic(self, func):
        func(CalculationDirectory.is_calculation_directory(
            self.optic_workdir))

    def _check_optic_written(self):
        self._check_optic(self.assertTrue)

    def _check_optic_not_written(self):
        self._check_optic(self.assertFalse)

    def _set_optic_tempdir(self):
        if hasattr(self, "optic_tempdir"):
            return
        self.optic_tempdir = tempfile.TemporaryDirectory()

    def _set_optic(self):
        self.sequencer.optic_input_variables = self._optic_input_variables

    def _set_optic_done(self):
        copy_calculation(
                self._optic_example, self.optic_workdir,
                new_parents=[self.sequencer.nscffbz_workdir,
                             self.sequencer.ddk_workdir])

    def _check_plot(self, func):
        func(os.path.isfile(self.sequencer.plot_save))

    def _check_plot_written(self):
        self.assertGreater(
                len(os.listdir(os.path.dirname(self.sequencer.plot_save))), 1)

    def _check_plot_not_written(self):
        self._check_plot(self.assertFalse)

    def test_whole_sequence(self):
        self._set_scf_done()
        self._set_nscf_done()
        self._set_nscffbz_done()
        self._set_ddk_done()
        self._set_optic()
        self.sequencer.write()
        self._check_optic_written()
        self.assertFalse(self.sequencer.sequence_completed)
        # continue for optic
        self.optic_tempdir.cleanup()
        self._set_optic_done()
        self.sequencer.launch()
        self.assertTrue(self.sequencer.sequence_completed)
        self._check_plot_written()

    def test_starting_from_nscffbz(self):
        self._set_scf_done()
        self._set_nscf_done()
        self._set_nscffbz_done()
        self._set_ddk()
        self.sequencer.write()
        self._check_ddk_written()
        self._check_optic_not_written()
        self.assertFalse(self.sequencer.sequence_completed)

    def test_starting_from_nscf(self):
        self._set_scf_done()
        self._set_nscf_done()
        self._set_nscffbz()
        self.sequencer.write()
        self._check_nscffbz_written()
        self._check_ddk_not_written()
        self._check_optic_not_written()
        self.assertFalse(self.sequencer.sequence_completed)

    def test_starting_from_scf(self):
        self._set_scf_done()
        self._set_nscf()
        self.sequencer.write()
        self._check_nscf_written()
        self._check_nscffbz_not_written()
        self._check_ddk_not_written()
        self._check_optic_not_written()
        self.assertFalse(self.sequencer.sequence_completed)

    def test_starting_from_scratch(self):
        self._set_scf()
        self.sequencer.write()
        self._check_scf_written()
        self._check_nscf_not_written()
        self._check_nscffbz_not_written()
        self._check_ddk_not_written()
        self._check_optic_not_written()
        self.assertFalse(self.sequencer.sequence_completed)


# ######################## TEST CASE FOR BAND STRUCTURE SEQUENCER #############
@pytest.mark.order("first")
class TestAbinitBandStructureSequencer(
        BaseAbinitSCFSequencerTest,
        BaseBandStructureSequencerTest, TestCase):
    """Test case for the AbinitBandStructureSequencer class.
    """
    _band_structure_example = os.path.join(
            here, "files", "abinit_Si_band_structure", "band_structure",
            "bs_ecut10")
    _band_structure_input_variables = {
        "ecut": 10,
        "iscf": -2,
        "nband": 8,
        "diemac": 12.0,
        "tolwfr": 1e-12,
        "autoparal": 1,
        }
    _band_structure_kpoint_path = [
            {"L": [0.5, 0.0, 0.0]},
            {r"$\Gamma$": [0.0, 0.0, 0.0]},
            {"X": [0.0, 0.5, 0.5]},
            {r"$\Gamma_2$": [1.0, 1.0, 1.0]}]
    _band_structure_kpoint_path_density = 20
    _scf_example = os.path.join(
            here, "files", "abinit_Si_band_structure", "scf_run", "scf_ecut10")
    _scf_input_variables = {
        "ecut": 10,
        "kptopt": 1,
        "ngkpt": [6, 6, 6],
        "nshiftk": 4,
        "shiftk": [[0.5, 0.5, 0.5], [0.5, 0.0, 0.0],
                   [0.0, 0.5, 0.0], [0.0, 0.0, 0.5]],
        "diemac": 12.0,
        "nstep": 10,
        "toldfe": 1e-6,
        "acell": [7.2078778121] * 3,
        "natom": 2,
        "ntypat": 1,
        "rprim": [[0, 0.70710678119, 0.70710678119],
                  [0.70710678119, 0, 0.70710678119],
                  [0.70710678119, 0.70710678119, 0]],
        "typat": [1, 1],
        "xred": [[0, 0, 0], [0.25, 0.25, 0.25]],
        "znucl": 14.0,
        "autoparal": 1,
        }
    _sequencer_class = AbinitBandStructureSequencer
    _pseudos = [os.path.join(here, "files", "pseudos", "Si.psp8")]

    def setUp(self):
        BaseAbinitSCFSequencerTest.setUp(self)
        BaseBandStructureSequencerTest.setUp(self)


@pytest.mark.order("first")
class TestAbinitBandStructureComparatorSequencer(
        BaseAbinitSCFSequencerTest,
        BaseBandStructureSequencerTest, TestCase):
    """Test case for the AbinitBandStructureSequencer class.
    """
    _band_structure_example = os.path.join(
            here, "files", "abinit_Si_band_structure", "band_structure")
    _band_structure_input_variables = {
        "iscf": -2,
        "nband": 8,
        "diemac": 12.0,
        "tolwfr": 1e-12,
        "autoparal": 1,
        }
    _band_structure_kpoint_path = [
            {"L": [0.5, 0.0, 0.0]},
            {r"$\Gamma$": [0.0, 0.0, 0.0]},
            {"X": [0.0, 0.5, 0.5]},
            {r"$\Gamma_2$": [1.0, 1.0, 1.0]}]
    _band_structure_kpoint_path_density = 20
    _scf_example = os.path.join(
            here, "files", "abinit_Si_band_structure", "scf_run")
    _scf_input_variables = {
        "kptopt": 1,
        "ngkpt": [6, 6, 6],
        "nshiftk": 4,
        "shiftk": [[0.5, 0.5, 0.5], [0.5, 0.0, 0.0],
                   [0.0, 0.5, 0.0], [0.0, 0.0, 0.5]],
        "diemac": 12.0,
        "nstep": 10,
        "toldfe": 1e-6,
        "acell": [7.2078778121] * 3,
        "natom": 2,
        "ntypat": 1,
        "rprim": [[0, 0.70710678119, 0.70710678119],
                  [0.70710678119, 0, 0.70710678119],
                  [0.70710678119, 0.70710678119, 0]],
        "typat": [1, 1],
        "xred": [[0, 0, 0], [0.25, 0.25, 0.25]],
        "znucl": 14.0,
        "autoparal": 1,
        }
    _sequencer_class = AbinitBandStructureComparatorSequencer
    _pseudos = [os.path.join(here, "files", "pseudos", "Si.psp8")]

    def setUp(self):
        BaseAbinitSCFSequencerTest.setUp(self)
        BaseBandStructureSequencerTest.setUp(self)
        self.sequencer.scf_specific_input_variables = [
                {"ecut": 10}, {"ecut": 20}]
        self.sequencer.scf_workdir = os.path.join(
                self.sequencer.scf_workdir, "scf")
        self.sequencer.band_structure_workdir = os.path.join(
                self.sequencer.band_structure_workdir,
                "bs")

    def _check_band_structure(self, func):
        dirname = os.path.dirname(self.sequencer.band_structure_workdir)
        calcs = [os.path.join(dirname, calc) for calc in os.listdir(dirname)]
        for calc in calcs:
            func(CalculationDirectory.is_calculation_directory(calc))

    def _check_scf(self, func):
        dirname = os.path.dirname(self.sequencer.scf_workdir)
        calcs = [os.path.join(dirname, calc) for calc in os.listdir(dirname)]
        for calc in calcs:
            func(CalculationDirectory.is_calculation_directory(calc),
                 msg=calc)

    def _set_band_structure_done(self):
        dirname = os.path.dirname(self.sequencer.band_structure_workdir)
        for calc in os.listdir(self._band_structure_example):
            ext = calc.split("_")[-1]
            copy_calculation(
                    os.path.join(self._band_structure_example, calc),
                    os.path.join(dirname, calc),
                    new_parents=[self.sequencer.scf_workdir + "_" + ext],
                    new_pseudos=self._pseudos)

    def _set_scf_done(self):
        dirname = os.path.dirname(self.sequencer.scf_workdir)
        for calc in os.listdir(self._scf_example):
            copy_calculation(
                    os.path.join(self._scf_example, calc),
                    os.path.join(dirname, calc),
                    new_pseudos=self._pseudos)
