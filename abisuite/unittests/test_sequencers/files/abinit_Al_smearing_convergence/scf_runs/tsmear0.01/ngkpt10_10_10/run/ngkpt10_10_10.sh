#!/bin/bash

MPIRUN="mpirun -np 4"
EXECUTABLE="/home/fgoudreault/Workspace/abinit/abinit/build/develop/2021-04-16/src/98_main/abinit"
INPUT=/home/fgoudreault/Workspace/abisuite/abisuite/unittests/test_sequencers/files/abinit_Al_smearing_convergence/scf_runs/tsmear0.01/ngkpt10_10_10/run/ngkpt10_10_10.files
LOG=/home/fgoudreault/Workspace/abisuite/abisuite/unittests/test_sequencers/files/abinit_Al_smearing_convergence/scf_runs/tsmear0.01/ngkpt10_10_10/ngkpt10_10_10.log
STDERR=/home/fgoudreault/Workspace/abisuite/abisuite/unittests/test_sequencers/files/abinit_Al_smearing_convergence/scf_runs/tsmear0.01/ngkpt10_10_10/ngkpt10_10_10.stderr


$MPIRUN $EXECUTABLE < $INPUT > $LOG 2> $STDERR
