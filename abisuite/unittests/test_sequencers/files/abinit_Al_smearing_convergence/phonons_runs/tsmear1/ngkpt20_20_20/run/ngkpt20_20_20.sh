#!/bin/bash

MPIRUN="mpirun -np 4"
EXECUTABLE="/home/fgoudreault/Workspace/abinit/abinit/build/develop/2021-04-16/src/98_main/abinit"
INPUT=/home/fgoudreault/Workspace/abisuite/abisuite/unittests/test_sequencers/files/abinit_Al_smearing_convergence/phonons_runs/tsmear1/ngkpt20_20_20/run/ngkpt20_20_20.files
LOG=/home/fgoudreault/Workspace/abisuite/abisuite/unittests/test_sequencers/files/abinit_Al_smearing_convergence/phonons_runs/tsmear1/ngkpt20_20_20/ngkpt20_20_20.log
STDERR=/home/fgoudreault/Workspace/abisuite/abisuite/unittests/test_sequencers/files/abinit_Al_smearing_convergence/phonons_runs/tsmear1/ngkpt20_20_20/ngkpt20_20_20.stderr


$MPIRUN $EXECUTABLE < $INPUT > $LOG 2> $STDERR
