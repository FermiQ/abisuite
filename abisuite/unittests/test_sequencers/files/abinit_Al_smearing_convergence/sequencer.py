from abisuite import AbinitSmearingConvergenceSequencer


sequencer = AbinitSmearingConvergenceSequencer()
# scf part
sequencer.scf_workdir = "scf_runs"
sequencer.scf_pseudos = "../pseudos/Al.psp8"
sequencer.scf_input_variables = {
        "acell": [7.6] * 3,
        "rprim": [[0, 0.5, 0.5], [0.5, 0, 0.5], [0.5, 0.5, 0]],
        "ntypat": 1,
        "znucl": 13,
        "natom": 1,
        "typat": 1,
        "xred": [0, 0, 0],
        "ecut": 6,
        "nshiftk": 4,
        "shiftk": [[0.5, 0.5, 0.5], [0.5, 0.0, 0.0], [0, 0.5, 0], [0, 0, 0.5]],
        "nstep": 10,
        "toldfe": 1e-6,
        "occopt": 4,
        }
sequencer.kgrids_input_variable_name = "ngkpt"
sequencer.scf_convergence_criterion = 10
sequencer.scf_kgrids = [[x, x, x] for x in [2, 5, 10, 20]]
sequencer.scf_smearings = [1, 0.1, 0.01]
sequencer.scf_mpi_command = "mpirun -np 4"
sequencer.launch()
print(sequencer.scf_converged_parameter)
