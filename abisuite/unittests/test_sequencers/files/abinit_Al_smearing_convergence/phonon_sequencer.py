from abisuite import AbinitSmearingPhononConvergenceSequencer


sequencer = AbinitSmearingPhononConvergenceSequencer()
# scf part
sequencer.scf_workdir = "scf_runs"
sequencer.scf_pseudos = "../pseudos/Al.psp8"
sequencer.scf_input_variables = {
        "acell": [7.6] * 3,
        "rprim": [[0, 0.5, 0.5], [0.5, 0, 0.5], [0.5, 0.5, 0]],
        "ntypat": 1,
        "znucl": 13,
        "natom": 1,
        "typat": 1,
        "xred": [0, 0, 0],
        "ecut": 6,
        "nshiftk": 4,
        "shiftk": [[0.5, 0.5, 0.5], [0.5, 0.0, 0.0], [0, 0.5, 0], [0, 0, 0.5]],
        "nstep": 10,
        "toldfe": 1e-6,
        "occopt": 4,
        }
sequencer.kgrids_input_variable_name = "ngkpt"
sequencer.scf_convergence_criterion = 10
sequencer.scf_kgrids = [[x, x, x] for x in [2, 5, 10, 20]]
sequencer.scf_smearings = [1, 0.1, 0.01]
sequencer.scf_mpi_command = "mpirun -np 4"

# phonon part
sequencer.phonons_workdir = "phonons_runs"
sequencer.phonons_qpt = [0.0, 0.0, 0.0]
sequencer.phonons_input_variables = {
        "rfatpol": [1, 1],
        "tolvrs": 1e-8,
        "diemac": 9.0,
        "nstep": 25,
        }
sequencer.phonons_mpi_command = "mpirun -np 4"
sequencer.phonons_convergence_criterion = 10

sequencer.launch()
print(sequencer.scf_converged_parameter)
