#!/bin/bash

MPIRUN="mpirun -np 4"
EXECUTABLE="/home/fgoudreault/Workspace/abinit/abinit/build/develop/2021-04-16/src/98_main/abinit"
INPUT=/home/fgoudreault/Workspace/abisuite/abisuite/unittests/test_sequencers/files/abinit_AlAs_kgrid_convergence/ddk/ngkpt/ngkpt_5_5_5/run/ngkpt_5_5_5.files
LOG=/home/fgoudreault/Workspace/abisuite/abisuite/unittests/test_sequencers/files/abinit_AlAs_kgrid_convergence/ddk/ngkpt/ngkpt_5_5_5/ngkpt_5_5_5.log
STDERR=/home/fgoudreault/Workspace/abisuite/abisuite/unittests/test_sequencers/files/abinit_AlAs_kgrid_convergence/ddk/ngkpt/ngkpt_5_5_5/ngkpt_5_5_5.stderr


$MPIRUN $EXECUTABLE < $INPUT > $LOG 2> $STDERR
