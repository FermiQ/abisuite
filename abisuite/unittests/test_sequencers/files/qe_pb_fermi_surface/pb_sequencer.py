from abisuite import QEFermiSurfaceSequencer as Sequencer


sequencer = Sequencer()
# set scf part
sequencer.scf_workdir = "scf_run"
sequencer.scf_input_variables = {
    "calculation": "scf",
    "ibrav": 2,
    "ntyp": 1,
    "nat": 1,
    "celldm(1)": 9.2225583816,
    "ecutwfc": 40,
    "pseudo_dir": "../pseudos",
    "occupations": "smearing",
    "conv_thr": 1.0e-6,
    "atomic_species": [
        {"atom": "Pb",
         "atomic_mass": 207.2,
         "pseudo": "pb_s.UPF",
         }],
    "atomic_positions": {"parameter": "crystal",
                         "positions": {"Pb": [0.0, 0.0, 0.0]}},
    "k_points": {"parameter": "automatic",
                 "k_points": [10, 10, 10, 1, 1, 1]},
    "smearing": "methfessel-paxton",
    "degauss": 0.01,
    }
sequencer.scf_mpi_command = "mpirun -np 4"
sequencer.scf_command_arguments = "-npool 4"

sequencer.nscf_workdir = "nscf_run"
sequencer.nscf_kgrid = [30, 30, 30]
sequencer.nscf_input_variables = {
    "ibrav": 2,
    "ntyp": 1,
    "nat": 1,
    "celldm(1)": 9.2225583816,
    "ecutwfc": 40,
    "pseudo_dir": "../pseudos",
    "occupations": "smearing",
    "conv_thr": 1.0e-6,
    "atomic_species": [
        {"atom": "Pb",
         "atomic_mass": 207.2,
         "pseudo": "pb_s.UPF",
         }],
    "atomic_positions": {"parameter": "crystal",
                         "positions": {"Pb": [0.0, 0.0, 0.0]}},
    "k_points": {"parameter": "automatic",
                 "k_points": [30, 30, 30, 0, 0, 0]},
    "smearing": "methfessel-paxton",
    "degauss": 0.01,
    }
sequencer.nscf_mpi_command = "mpirun -np 4"
sequencer.nscf_command_arguments = "-npool 4"
sequencer.fermi_surface_k_z = [0.0]
sequencer.plot_save = "fermi_surfaces.pdf"
sequencer.launch()
