from abisuite import QESmearingConvergenceSequencer as Sequencer


sequencer = Sequencer()
# set scf part
sequencer.scf_workdir = "scf_runs"
sequencer.scf_input_variables = {
    "calculation": "scf",
    "ibrav": 2,
    "ntyp": 1,
    "nat": 1,
    "celldm(1)": 9.2225583816,
    "ecutwfc": 40,
    "pseudo_dir": "../pseudos",
    "occupations": "smearing",
    "conv_thr": 1.0e-6,
    "atomic_species": [
        {"atom": "Pb",
         "atomic_mass": 207.2,
         "pseudo": "pb_s.UPF",
         }],
    "atomic_positions": {"parameter": "crystal",
                         "positions": {"Pb": [0.0, 0.0, 0.0]}},
    }
sequencer.scf_convergence_criterion = 10
sequencer.scf_kgrids = [[x, x, x, 1, 1, 1]
                        for x in [2, 4, 8]]
sequencer.scf_smearings = [0.1, 0.01]
sequencer.scf_mpi_command = "mpirun -np 4"
sequencer.scf_command_arguments = "-npool 4"
sequencer.launch()
print(sequencer.scf_converged_parameter)
