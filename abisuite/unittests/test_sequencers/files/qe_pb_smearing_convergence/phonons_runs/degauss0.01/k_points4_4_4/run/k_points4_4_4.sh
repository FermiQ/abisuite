#!/bin/bash

MPIRUN="mpirun -np 4"
EXECUTABLE="/home/fgoudreault/Workspace/q-e/bin/ph.x -npool 4"
INPUT=/home/fgoudreault/Workspace/abisuite/abisuite/unittests/test_sequencers/files/qe_pb_smearing_convergence/phonons_runs/degauss0.01/k_points4_4_4/k_points4_4_4.in
LOG=/home/fgoudreault/Workspace/abisuite/abisuite/unittests/test_sequencers/files/qe_pb_smearing_convergence/phonons_runs/degauss0.01/k_points4_4_4/k_points4_4_4.log
STDERR=/home/fgoudreault/Workspace/abisuite/abisuite/unittests/test_sequencers/files/qe_pb_smearing_convergence/phonons_runs/degauss0.01/k_points4_4_4/k_points4_4_4.stderr


$MPIRUN $EXECUTABLE -input $INPUT > $LOG 2> $STDERR
