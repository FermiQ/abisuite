import os


def convert_link_to_relative_link(fullpath, source):
    relsource = os.path.relpath(source, os.path.dirname(fullpath))
    os.remove(fullpath)
    os.symlink(relsource, fullpath)


for root, dirs, files in os.walk("."):
    for dirname in dirs:
        # check if dir is a link towards another directory
        fullpath = os.path.join(root, dirname)
        if os.path.islink(fullpath):
            source = os.readlink(fullpath)
            if os.path.isabs(source):
                # convert to a relative path
                convert_link_to_relative_link(fullpath, source)
    for filename in files:
        fullpath = os.path.join(root, filename)
        if os.path.islink(fullpath):
            source = os.readlink(fullpath)
            if os.path.isabs(source):
                convert_link_to_relative_link(fullpath, source)
