#!/bin/bash

MPIRUN="mpirun -np 4"
EXECUTABLE="/home/fgoudreault/Workspace/q-e/bin/ph.x -npool 4"
INPUT=/home/fgoudreault/Workspace/abisuite/abisuite/unittests/test_sequencers/files/qe_pb/phonons_run/ph_q3/ph_q3.in
LOG=/home/fgoudreault/Workspace/abisuite/abisuite/unittests/test_sequencers/files/qe_pb/phonons_run/ph_q3/ph_q3.log
STDERR=/home/fgoudreault/Workspace/abisuite/abisuite/unittests/test_sequencers/files/qe_pb/phonons_run/ph_q3/ph_q3.stderr


$MPIRUN $EXECUTABLE < $INPUT > $LOG 2> $STDERR
