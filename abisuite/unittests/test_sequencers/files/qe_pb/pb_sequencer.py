from abisuite import QEEPWSequencer as Sequencer


sequencer = Sequencer()
# set scf part
sequencer.scf_workdir = "scf_run"
sequencer.scf_input_variables = {
    "calculation": "scf",
    "ibrav": 2,
    "ntyp": 1,
    "nat": 1,
    "celldm(1)": 9.2225583816,
    "ecutwfc": 40,
    "pseudo_dir": "../pseudos",
    "occupations": "smearing",
    "degauss": 0.1,
    "conv_thr": 1.0e-6,
    "atomic_species": [
        {"atom": "Pb",
         "atomic_mass": 207.2,
         "pseudo": "pb_s.UPF",
         }],
    "k_points": {"parameter": "automatic",
                 "k_points": [4, 4, 4, 1, 1, 1],
                 },
    "atomic_positions": {"parameter": "crystal",
                         "positions": {"Pb": [0.0, 0.0, 0.0]}},
    }
sequencer.scf_mpi_command = "mpirun -np 4"
sequencer.scf_command_arguments = "-npool 4"

# set nscf part
sequencer.nscf_kgrid = [8, 8, 8]
sequencer.nscf_workdir = "nscf_run"
sequencer.nscf_input_variables = {
    "tprnfor": True,
    "tstress": True,
    "ecutwfc": 40,
    "occupations": "smearing",
    "degauss": 0.1,
    "nbnd": 10,
    "conv_thr": 1.0e-8,
    }
sequencer.nscf_mpi_command = "mpirun -np 4"
sequencer.nscf_command_arguments = "-npool 4"

# set band structure part
sequencer.band_structure_workdir = "band_structure_run"
sequencer.band_structure_kpoint_path = [
        {"L": [0.0, 0.5, 0.0]},
        {r"$\Gamma$": [0.0, 0.0, 0.0]},
        {"X": [0.0, 0.5, 0.5]},
        {"U": [0.0, 0.625, 0.375]},
        {"K": [-0.375, 0.375, 0.0]},
        {r"$\Gamma$": [0.0, 0.0, 0.0]},
        ]
sequencer.band_structure_kpoint_path_density = 20
sequencer.band_structure_input_variables = {
        "conv_thr": 1e-8,
        "diagonalization": "cg",
        "diago_full_acc": True,
        "ecutwfc": 40.0,
        "nbnd": 10,
        }
sequencer.band_structure_mpi_command = "mpirun -np 4"
sequencer.band_structure_command_arguments = "-npool 4"

# set phonons part
sequencer.phonons_workdir = "phonons_run/ph_q"
sequencer.nphonons = 3
sequencer.phonons_input_variables = {
        "ldisp": True,
        "nq1": 2,
        "nq2": 2,
        "nq3": 2,
        "tr2_ph": 1.0e-8,
        }
sequencer.phonons_mpi_command = "mpirun -np 4"
sequencer.phonons_command_arguments = "-npool 4"

# set epw part
sequencer.epw_workdir = "epw_run"
sequencer.epw_mpi_command = "mpirun -np 4"
sequencer.epw_command_arguments = "-npool 4"
sequencer.epw_input_variables = {
        "amass(1)": 207.2,
        "elph": True,
        "kmaps": False,
        "epbwrite": True,
        "epbread": False,
        "epwwrite": True,
        "epwread": False,
        "nbndsub": 4,
        "bands_skipped": "exclude_bands = 1:5",
        "wannierize": True,
        "num_iter": 300,
        "dis_win_max": 21,
        "dis_win_min": -3,
        "dis_froz_min": -3,
        "dis_froz_max": 13.5,
        "proj(1)": "PB:sp3",
        "iverbosity": 0,
        "elecselfen": False,
        "phonselfen": True,
        "fsthick": 6,
        "degaussw": 0.1,
        "a2f": True,
        "nkf1": 10,
        "nkf2": 10,
        "nkf3": 10,
        "nqf1": 10,
        "nqf2": 10,
        "nqf3": 10,
        "nk1": 8,
        "nk2": 8,
        "nk3": 8,
        }
sequencer.launch()
