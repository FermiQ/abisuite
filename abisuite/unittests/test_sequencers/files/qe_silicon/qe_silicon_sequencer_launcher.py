from abisuite import WannierInterpolationSequencer as Sequencer


sequencer = Sequencer("qe")

# set scf parameters
sequencer.scf_input_variables = {
    "ibrav": 2,
    "celldm(1)": 10.3472,
    "ntyp": 1,
    "nat": 2,
    "ecutwfc": 35.0,
    "conv_thr": 1e-8,
    "pseudo_dir": ".",
    "atomic_species": [{"atom": "Si",
                        "pseudo": "Si.pbe-mt_fhi.UPF",
                        "atomic_mass": 28.085, }],
    "atomic_positions": {
        "parameter": "crystal",
        "positions": {"Si": [[0.0, 0.0, 0.0], [0.25, 0.25, 0.25]]}},
    "k_points": {
        "parameter": "automatic",
        "k_points": [4, 4, 4, 0, 0, 0],
        },
    }
sequencer.scf_workdir = "scf_run"
sequencer.scf_command_arguments = "-npool 4"
sequencer.scf_mpi_command = "mpirun -np 4"

# set nscf parameters
sequencer.nscf_workdir = "nscf_run"
sequencer.nscf_kgrid = [4, 4, 4]
sequencer.nscf_input_variables = {
        "ecutwfc": 35.0,
        "conv_thr": 1e-12,
        "nbnd": 12,
        "diagonalization": "cg",
        "diago_full_acc": True,
        "verbosity": "high",
        }
sequencer.nscf_command_arguments = "-npool 4"
sequencer.nscf_mpi_command = "mpirun -np 4"

# set band structure

sequencer.band_structure_workdir = "band_structure_run"
sequencer.band_structure_kpoint_path = [
        {"L": [0.0, 0.5, 0.0]},
        {r"$\Gamma$": [0.0, 0.0, 0.0]},
        {"X": [0.0, 0.5, 0.5]},
        {"U": [0.0, 0.625, 0.375]},
        {"K": [-0.375, 0.375, 0.0]},
        {r"$\Gamma$": [0.0, 0.0, 0.0]},
        ]
sequencer.band_structure_kpoint_path_density = 20
sequencer.band_structure_input_variables = {
        "conv_thr": 1e-12,
        "diagonalization": "cg",
        "diago_full_acc": True,
        "ecutwfc": 35.0,
        "nbnd": 12,
        }
sequencer.band_structure_mpi_command = "mpirun -np 4"
sequencer.band_structure_command_arguments = "-npool 4"

# set wannier parameters
sequencer.wannier90_workdir = "wannier_run"
sequencer.wannier90_mpi_command = "mpirun -np 4"
sequencer.wannier90_input_variables = {
        "num_wann": 8,
        "bands_plot": True,
        "dis_win_max": 17.5,
        "dis_froz_max": 6.9,
        "dis_num_iter": 1000,
        "num_iter": 2000,
        "num_print_cycles": 20,
        "conv_window": 3,
        "projections": ["Si:sp3"],
        }
# set pw2wannier90 parameters
sequencer.pw2wannier90_workdir = "pw2wannier90_run"
sequencer.pw2wannier90_input_variables = {
        "spin_component": "none",
        }
sequencer.pw2wannier90_mpi_command = "mpirun -np 4"

sequencer.launch()
