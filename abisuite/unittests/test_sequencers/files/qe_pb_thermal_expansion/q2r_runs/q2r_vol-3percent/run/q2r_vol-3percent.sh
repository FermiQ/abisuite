#!/bin/bash

MPIRUN="mpirun -np 4"
EXECUTABLE="/home/fgoudreault/Workspace/q-e/bin/q2r.x"
INPUT=/home/fgoudreault/Workspace/abisuite/abisuite/unittests/test_sequencers/files/qe_pb_thermal_expansion/q2r_runs/q2r_vol-3percent/q2r_vol-3percent.in
LOG=/home/fgoudreault/Workspace/abisuite/abisuite/unittests/test_sequencers/files/qe_pb_thermal_expansion/q2r_runs/q2r_vol-3percent/q2r_vol-3percent.log
STDERR=/home/fgoudreault/Workspace/abisuite/abisuite/unittests/test_sequencers/files/qe_pb_thermal_expansion/q2r_runs/q2r_vol-3percent/q2r_vol-3percent.stderr


$MPIRUN $EXECUTABLE -input $INPUT > $LOG 2> $STDERR
