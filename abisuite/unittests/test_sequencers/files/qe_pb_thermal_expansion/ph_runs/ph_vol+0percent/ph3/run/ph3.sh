#!/bin/bash

MPIRUN="mpirun -np 4"
EXECUTABLE="/home/fgoudreault/Workspace/q-e/bin/ph.x -npool 4"
INPUT=/home/fgoudreault/Workspace/abisuite/abisuite/unittests/test_sequencers/files/qe_pb_thermal_expansion/ph_runs/ph_vol+0percent/ph3/ph3.in
LOG=/home/fgoudreault/Workspace/abisuite/abisuite/unittests/test_sequencers/files/qe_pb_thermal_expansion/ph_runs/ph_vol+0percent/ph3/ph3.log
STDERR=/home/fgoudreault/Workspace/abisuite/abisuite/unittests/test_sequencers/files/qe_pb_thermal_expansion/ph_runs/ph_vol+0percent/ph3/ph3.stderr


$MPIRUN $EXECUTABLE -input $INPUT > $LOG 2> $STDERR
