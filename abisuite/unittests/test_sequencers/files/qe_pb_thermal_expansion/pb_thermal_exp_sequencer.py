from abisuite import QEThermalExpansionSequencer as Sequencer


sequencer = Sequencer()
# set scf part
sequencer.scf_workdir = "scf_runs/scf"
sequencer.scf_input_variables = {
    "calculation": "scf",
    "ibrav": 2,
    "ntyp": 1,
    "nat": 1,
    "celldm(1)": 9.2225583816,
    "ecutwfc": 40,
    "pseudo_dir": "../pseudos",
    "occupations": "smearing",
    "degauss": 0.1,
    "conv_thr": 1.0e-8,
    "atomic_species": [
        {"atom": "Pb",
         "atomic_mass": 207.2,
         "pseudo": "pb_s.UPF",
         }],
    "k_points": {"parameter": "automatic",
                 "k_points": [6, 6, 6, 1, 1, 1],
                 },
    "atomic_positions": {"parameter": "crystal",
                         "positions": {"Pb": [0.0, 0.0, 0.0]}},
    }
sequencer.deltas_volumes = [0, -1, 1, -3, 3, -5, 5]
sequencer.scf_mpi_command = "mpirun -np 4"
sequencer.scf_command_arguments = "-npool 4"

sequencer.phonons_workdir = "ph_runs/ph"
sequencer.phonons_input_variables = {
        "ldisp": True,
        "tr2_ph": 1e-12,
        "nq1": 2, "nq2": 2, "nq3": 2,
        }
sequencer.nphonons = 3
sequencer.phonons_mpi_command = "mpirun -np 4"
sequencer.phonons_command_arguments = "-npool 4"

sequencer.q2r_workdir = "q2r_runs/q2r"
sequencer.q2r_mpi_command = "mpirun -np 4"
sequencer.q2r_command_arguements = "-npool 4"
sequencer.q2r_input_variables = {}

sequencer.matdyn_workdir = "matdyn_runs/matdyn"
sequencer.matdyn_mpi_command = "mpirun -np 4"
sequencer.matdyn_command_arguments = "-npool 4"
sequencer.asr = "crystal"

sequencer.bulk_modulus_initial_guess = 37
sequencer.temperatures = [10, 50, 100, 150, 300, 400, 500, 600]
sequencer.launch()
