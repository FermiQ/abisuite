#!/bin/bash

MPIRUN="mpirun -np 4"
EXECUTABLE="/home/fgoudreault/Workspace/abinit/abinit/build/develop/2021-04-16/src/98_main/abinit"
INPUT=/home/fgoudreault/Workspace/abisuite/abisuite/unittests/test_sequencers/files/abinit_Si_band_structure/scf_run/scf_ecut20/run/scf_ecut20.files
LOG=/home/fgoudreault/Workspace/abisuite/abisuite/unittests/test_sequencers/files/abinit_Si_band_structure/scf_run/scf_ecut20/scf_ecut20.log
STDERR=/home/fgoudreault/Workspace/abisuite/abisuite/unittests/test_sequencers/files/abinit_Si_band_structure/scf_run/scf_ecut20/scf_ecut20.stderr


$MPIRUN $EXECUTABLE < $INPUT > $LOG 2> $STDERR
