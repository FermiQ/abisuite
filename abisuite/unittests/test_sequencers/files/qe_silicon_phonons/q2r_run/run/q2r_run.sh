#!/bin/bash

MPIRUN="mpirun -np 4"
EXECUTABLE="/home/fgoudreault/Workspace/q-e/bin/q2r.x -npool 4"
INPUT=/home/fgoudreault/Workspace/abisuite/abisuite/unittests/test_sequencers/files/qe_silicon_phonons/q2r_run/q2r_run.in
LOG=/home/fgoudreault/Workspace/abisuite/abisuite/unittests/test_sequencers/files/qe_silicon_phonons/q2r_run/q2r_run.log
STDERR=/home/fgoudreault/Workspace/abisuite/abisuite/unittests/test_sequencers/files/qe_silicon_phonons/q2r_run/q2r_run.stderr


$MPIRUN $EXECUTABLE < $INPUT > $LOG 2> $STDERR
