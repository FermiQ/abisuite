#!/bin/bash

MPIRUN=""
EXECUTABLE="/home/fgoudreault/Workspace/q-e/bin/ph.x"
INPUT=/home/fgoudreault/Workspace/abisuite/abisuite/unittests/test_sequencers/files/qe_silicon_phonons/ph_run_q3/ph_run_q3.in
LOG=/home/fgoudreault/Workspace/abisuite/abisuite/unittests/test_sequencers/files/qe_silicon_phonons/ph_run_q3/ph_run_q3.log
STDERR=/home/fgoudreault/Workspace/abisuite/abisuite/unittests/test_sequencers/files/qe_silicon_phonons/ph_run_q3/ph_run_q3.stderr


$MPIRUN $EXECUTABLE < $INPUT > $LOG 2> $STDERR
