from abisuite import AbinitOpticSequencer as Sequencer


sequencer = Sequencer()
sequencer.scf_workdir = "scf_run"
sequencer.scf_input_variables = {
        "nband": 4,
        "nstep": 25,
        "kptopt": 1,
        "nbdbuf": 0,
        "nshiftk": 4,
        "shiftk": [[0.5, 0.5, 0.5],
                   [0.5, 0.0, 0.0],
                   [0.0, 0.5, 0.0],
                   [0.0, 0.0, 0.5]],
        "ngkpt": [4, 4, 4],
        "acell": [10.60] * 3,
        "amu": [69.72, 74.9216],
        "diemac": 10.0,
        "ecut": 2.00,
        "ixc": 3,
        "natom": 2,
        "ntypat": 2,
        "rprim": [[0.0, 0.5, 0.5],
                  [0.5, 0.0, 0.5],
                  [0.5, 0.5, 0.0]],
        "xred": [[0.0, 0.0, 0.0],
                 [0.25, 0.25, 0.25]],
        # "tnons": [[0.0] * 3] * 24,
        "tnons": [[0.0] * 3] * 24,
        "typat": [1, 2],
        "tolwfr": 1e-20,
        "znucl": [31, 33],
        }
sequencer.scf_pseudos = ["../pseudos/31ga.pspnc", "../pseudos/33as.pspnc"]
sequencer.scf_mpi_command = "mpirun"
sequencer.scf_mpi_command_arguments = "-np 4"

sequencer.nscf_workdir = "nscf_run"
sequencer.compute_non_linear_optical_response = True
nscfvars = sequencer.scf_input_variables.copy()
nscfvars["nband"] = 20
nscfvars["nbdbuf"] = 2
sequencer.nscf_input_variables = nscfvars
sequencer.nscf_mpi_command = "mpirun"
sequencer.nscf_mpi_command_arguments = "-np 4"

sequencer.nscffbz_workdir = "nscf_fbz_run"

sequencer.ddk_workdir = "ddk_run"
sequencer.ddk_input_variables = {
        "rfdir": [1, 1, 1],
        }
sequencer.ddk_mpi_command = "mpirun"
sequencer.ddk_mpi_command_arguments = "-np 4"

sequencer.optic_workdir = "optic_run"
sequencer.optic_input_variables = {
        "broadening": 0.002,
        "domega": 0.0003,
        "maxomega": 0.3,
        "scissor": 0.000,
        "tolerance": 0.002,
        "num_lin_comp": 1,
        "lin_comp": 11,
        "num_nonlin_comp": 2,
        "nonlin_comp": [123, 222],
        "num_linel_comp": 0,
        "num_nonlin2_comp": 0,
        }
sequencer.optic_mpi_command = "mpirun"
sequencer.optic_mpi_command_arguments = "-np 4"
sequencer.plot_save = "dielectric_tensor.pdf"
sequencer.plot_show = True

sequencer.launch()
