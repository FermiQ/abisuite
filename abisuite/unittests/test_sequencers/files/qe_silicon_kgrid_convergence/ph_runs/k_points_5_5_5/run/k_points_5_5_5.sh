#!/bin/bash

MPIRUN="mpirun -np 4"
EXECUTABLE="/home/fgoudreault/Workspace/q-e/bin/ph.x -npool 4"
INPUT=/home/fgoudreault/Workspace/abisuite/abisuite/unittests/test_sequencers/files/qe_silicon_kgrid_convergence/ph_runs/k_points_5_5_5/k_points_5_5_5.in
LOG=/home/fgoudreault/Workspace/abisuite/abisuite/unittests/test_sequencers/files/qe_silicon_kgrid_convergence/ph_runs/k_points_5_5_5/k_points_5_5_5.log
STDERR=/home/fgoudreault/Workspace/abisuite/abisuite/unittests/test_sequencers/files/qe_silicon_kgrid_convergence/ph_runs/k_points_5_5_5/k_points_5_5_5.stderr


$MPIRUN $EXECUTABLE -input $INPUT > $LOG 2> $STDERR
