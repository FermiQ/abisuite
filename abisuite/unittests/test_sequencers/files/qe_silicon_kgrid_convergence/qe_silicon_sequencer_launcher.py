from abisuite import QEKgridPhononConvergenceSequencer as Sequencer


sequencer = Sequencer()

# set scf parameters
sequencer.scf_input_variables = {
    "ibrav": 2,
    "celldm(1)": 10.3472,
    "ntyp": 1,
    "nat": 2,
    "conv_thr": 1e-8,
    "ecutwfc": 10.0,
    "pseudo_dir": "../pseudos",
    "atomic_species": [{"atom": "Si",
                        "pseudo": "Si.pbe-mt_fhi.UPF",
                        "atomic_mass": 28.085, }],
    "atomic_positions": {
        "parameter": "crystal",
        "positions": {"Si": [[0.0, 0.0, 0.0], [0.25, 0.25, 0.25]]}},
    }
sequencer.scf_workdir = "scf_run"
sequencer.scf_kgrids = [[4, 4, 4, 0, 0, 0], [5, 5, 5, 0, 0, 0]]
sequencer.scf_command_arguments = "-npool 4"
sequencer.scf_mpi_command = "mpirun -np 4"

sequencer.phonons_workdir = "ph_runs"
sequencer.phonons_input_variables = {
        "amass(1)": 28.085,
        }
sequencer.phonons_qpt = [0.0, 0.0, 0.0]  # gamma
sequencer.phonons_command_arguments = "-npool 4"
sequencer.phonons_mpi_command = "mpirun -np 4"

sequencer.launch()
