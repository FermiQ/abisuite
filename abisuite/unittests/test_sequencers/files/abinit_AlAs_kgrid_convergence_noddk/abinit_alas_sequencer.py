from abisuite import AbinitKgridPhononConvergenceSequencer as Sequencer


sequencer = Sequencer()
sequencer.scf_workdir = "scf_run"
sequencer.kgrids_input_variable_name = "ngkpt"
sequencer.scf_input_variables = {
        "tolvrs": 1.0e-18,
        "acell": [10.61] * 3,
        "rprim": [[0.0, 0.5, 0.5], [0.5, 0.0, 0.5], [0.5, 0.5, 0.0]],
        "ntypat": 2,
        "znucl": [13, 33],
        "natom": 2,
        "typat": [1, 2],
        "xred": [[0.0, 0.0, 0.0], [0.25, 0.25, 0.25]],
        "nband": 4,
        "ixc": 1,
        "ngkpt": [4, 4, 4],
        "nshiftk": 4,
        "ecut": 3.0,
        "shiftk": [[0.0, 0.0, 0.5], [0.0, 0.5, 0.0],
                   [0.5, 0.0, 0.0], [0.5, 0.5, 0.5]],
        "nstep": 25,
        "diemac": 9.0
        }
sequencer.scf_kgrids = [[4, 4, 4], [5, 5, 5]]
sequencer.scf_pseudos = ["13al.981214.fhi", "33as.pspnc"]
sequencer.scf_mpi_command = "mpirun"
sequencer.scf_mpi_command_arguments = "-np 4"

sequencer.compute_electric_field_response = False

sequencer.phonons_qpt = [0.0, 0.0, 0.0]
sequencer.phonons_workdir = "ph_run"
sequencer.phonons_input_variables = {
            "rfatpol": [1, 2],
            "tolvrs": 1.0e-8,
            "diemac": 9.0,
            "nstep": 25,
            }
sequencer.phonons_mpi_command = "mpirun -np 4"
sequencer.phonons_convergence_criterion = 10

sequencer.launch()
