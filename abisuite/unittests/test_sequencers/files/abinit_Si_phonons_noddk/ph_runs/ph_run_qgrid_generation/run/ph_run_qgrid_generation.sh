#!/bin/bash

MPIRUN=""
EXECUTABLE="/home/fgoudreault/Workspace/abinit/abinit/build/develop/2021-04-20/src/98_main/abinit"
INPUT=/home/fgoudreault/Workspace/abisuite/abisuite/unittests/test_sequencers/files/abinit_Si_phonons_noddk/ph_runs/ph_run_qgrid_generation/run/ph_run_qgrid_generation.files
LOG=/home/fgoudreault/Workspace/abisuite/abisuite/unittests/test_sequencers/files/abinit_Si_phonons_noddk/ph_runs/ph_run_qgrid_generation/ph_run_qgrid_generation.log
STDERR=/home/fgoudreault/Workspace/abisuite/abisuite/unittests/test_sequencers/files/abinit_Si_phonons_noddk/ph_runs/ph_run_qgrid_generation/ph_run_qgrid_generation.stderr


$MPIRUN $EXECUTABLE < $INPUT > $LOG 2> $STDERR
