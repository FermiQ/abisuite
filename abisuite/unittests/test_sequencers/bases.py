import os
import shutil
import tempfile

from ..routines_for_tests import copy_calculation
from ...exceptions import DevError
from ...handlers import (
        CalculationDirectory,
        )
from ...routines import is_list_like


class BaseSCFSequencerTest:
    """Base class for SCF sequencers test cases.
    """
    _scf_script_name = None
    _scf_example = None
    _scf_input_variables = None
    _sequencer_class = None
    _sequencer_class_args = []
    _sequencer_class_kwargs = {}
    _pseudos = None

    def setUp(self):
        if self._sequencer_class is None:
            raise DevError("Need to set '_sequencer_class'.")
        if self._scf_example is None:
            raise DevError("Need to set '_scf_example'.")
        if self._scf_script_name is None:
            raise DevError("Need to set '_scf_script_name'.")
        # create sequencer obj
        self._set_sequencer()
        # set temporary directories and files
        self._set_scf_tempdir()
        self._set_scf_command_file()
        self.scf_workdir = os.path.join(self.scf_tempdir.name, "scf_run")
        self.scf_command = self.scf_command_file.name
        # setting SCF sequencer parameters
        self.sequencer.scf_workdir = self.scf_workdir
        self.sequencer.scf_queuing_system = "local"
        self.sequencer.scf_command = self.scf_command

    def tearDown(self):
        if os.path.isdir(self.scf_workdir):
            self.scf_tempdir.cleanup()
        if os.path.isfile(self.scf_command):
            self.scf_command_file.close()

    def test_starting_from_scratch(self):
        """Test that starting from scratch works.
        """
        self._set_scf()
        self.sequencer.write()
        self._check_scf_written()
        self.assertFalse(self.sequencer.sequence_completed)
        # cleanup and set done
        self.scf_tempdir.cleanup()
        self._set_scf_done()
        self.sequencer.write()
        self.assertTrue(self.sequencer.sequence_completed)

    def _check_scf(self, func):
        func(CalculationDirectory.is_calculation_directory(self.scf_workdir))

    def _check_scf_written(self):
        self._check_scf(self.assertTrue)

    def _check_scf_not_written(self):
        self._check_scf(self.assertFalse)

    def _set_scf(self):
        if self._scf_input_variables is None:
            raise DevError("Need to set '_scf_input_variables'.")
        self.sequencer.scf_input_variables = self._scf_input_variables

    def _set_scf_done(self, **kwargs):
        copy_calculation(
                self._scf_example, self.scf_workdir,
                new_pseudos=self._pseudos,
                **kwargs)

    def _set_scf_tempdir(self):
        if hasattr(self, "scf_tempdir"):
            return
        self.scf_tempdir = tempfile.TemporaryDirectory()

    def _set_scf_command_file(self):
        if hasattr(self, "scf_command_file"):
            return
        self.scf_command_file = tempfile.NamedTemporaryFile(
                suffix=self._scf_script_name)

    def _set_sequencer(self):
        if hasattr(self, "sequencer"):
            return
        self.sequencer = self._sequencer_class(
                *self._sequencer_class_args,
                **self._sequencer_class_kwargs,
                )


class BaseRelaxationSequencerTest(BaseSCFSequencerTest):
    """Base test class for all RelaxationSequencers.
    """
    def test_relax_atoms_only(self):
        self.sequencer.relax_atoms = True
        self.sequencer.relax_cell = False
        # start from scratch
        self._set_scf()
        self.sequencer.write()
        self.assertEqual(len(os.listdir(self.sequencer.scf_workdir)), 1)
        self.assertFalse(self.sequencer.sequence_completed)
        shutil.rmtree(self.sequencer.scf_workdir)
        self._set_relax_atoms_done()
        self.sequencer.run()
        self.assertTrue(self.sequencer.sequence_completed)

    def test_relax_cell_only(self):
        self.sequencer.relax_cell = True
        self.sequencer.relax_atoms = False
        self._set_scf()
        self.sequencer.write()
        self.assertEqual(len(os.listdir(self.sequencer.scf_workdir)), 1)
        self.assertFalse(self.sequencer.sequence_completed)
        shutil.rmtree(self.sequencer.scf_workdir)
        self._set_relax_cell_done()
        self.sequencer.run()
        self.assertTrue(self.sequencer.sequence_completed)

    def test_relax_atoms_and_cell(self):
        self.sequencer.relax_atoms = True
        self.sequencer.relax_cell = True
        self._set_scf()
        self.sequencer.write()
        self.assertEqual(len(os.listdir(self.sequencer.scf_workdir)), 1)
        self.assertFalse(self.sequencer.sequence_completed)
        shutil.rmtree(self.sequencer.scf_workdir)
        self._set_relax_atoms_done()
        self.sequencer.write()
        self.assertEqual(len(os.listdir(self.sequencer.scf_workdir)), 2)
        self.assertFalse(self.sequencer.sequence_completed)
        shutil.rmtree(self.sequencer.scf_workdir)
        self._set_relax_atoms_done()
        self._set_relax_cell_done()
        self.sequencer.run()
        self.assertTrue(self.sequencer.sequence_completed)

    def test_starting_from_scratch(self):
        # this test is overridden by the ones above
        pass


class BaseConvergenceSequencerTest(BaseSCFSequencerTest):
    """Base test class for convergence sequencers.
    """
    _scf_convergence_criterion = 1

    def setUp(self):
        BaseSCFSequencerTest.setUp(self)
        self.sequencer.scf_convergence_criterion = (
                self._scf_convergence_criterion)
        self.sequencer.plot_show = False
        self.sequencer.plot_save = os.path.join(
                self.sequencer.scf_workdir, "plot.pdf")
        # don't save pickle as this is bugged somehow inside unittests...
        self.sequencer.plot_save_plot = False

    def test_whole_sequence(self):
        self._set_scf()
        self._set_scf_done()
        self.sequencer.launch()
        self._check_plot_written()

    def _check_plot_written(self):
        self.assertTrue(
                os.path.isfile(self.sequencer.plot_save),
                msg=self.sequencer.plot_save)

    def _set_scf_done(self, **kwargs):
        name = self.sequencer._parameter_to_converge_input_variable_name
        for calc in self._scf_example:
            basename = os.path.basename(calc)
            copy_calculation(
                    calc,
                    os.path.join(
                        self.scf_workdir, name,
                        basename),
                    new_pseudos=self._pseudos,
                    **kwargs)

    def _check_scf_written(self):
        name = self.sequencer._parameter_to_converge_input_variable_name
        self.assertEqual(
                len(os.listdir(os.path.join(self.scf_workdir, name))),
                len(self.sequencer._parameters_to_converge))


class BaseEcutConvergenceSequencerTest(BaseConvergenceSequencerTest):
    """Base test class for ecut convergence sequencers.
    """
    _ecuts = None

    def setUp(self):
        BaseConvergenceSequencerTest.setUp(self)
        if self._ecuts is None:
            raise DevError("Need to set '_ecuts'.")
        self.sequencer.scf_ecuts = self._ecuts

    def _check_scf_written(self):
        name = self.sequencer._parameter_to_converge_input_variable_name
        self.assertEqual(
                len(os.listdir(
                    os.path.join(self.scf_workdir, name))), len(self._ecuts))


class BaseKgridConvergenceSequencerTest(BaseConvergenceSequencerTest):
    """Base test class for kgrid convergence sequencers.
    """
    _kgrids = None

    def setUp(self):
        BaseConvergenceSequencerTest.setUp(self)
        if self._kgrids is None:
            raise DevError("Need to set '_kgrids'.")
        self.sequencer.scf_kgrids = self._kgrids


class BaseSmearingConvergenceSequencerTest(BaseKgridConvergenceSequencerTest):
    """Base test class for smearing convergence sequencers.
    """
    _smearings = None

    def setUp(self):
        BaseKgridConvergenceSequencerTest.setUp(self)
        if self._smearings is None:
            raise DevError("Need to set '_smearings'.")
        self.sequencer.scf_smearings = self._smearings

    def test_whole_sequence(self):
        super().test_whole_sequence()
        self.assertIn(
                self.sequencer.scf_converged_parameter[
                    self.sequencer.smearings_input_variable_name],
                self.sequencer.scf_smearings)
        self.assertIn(
                self.sequencer.scf_converged_parameter[
                    self.sequencer.kgrids_input_variable_name],
                self.sequencer.scf_kgrids)

    def _check_plot_written(self):
        self.assertEqual(
                len([x for x in os.listdir(self.scf_workdir)
                     if x.endswith(".pdf")]), 3,
                msg=self.sequencer.plot_save)

    def _check_scf_written(self):
        self.assertEqual(
                len(os.listdir(self.scf_workdir)),
                len(self.sequencer.scf_smearings))
        for subdir in os.listdir(self.scf_workdir):
            self.assertEqual(
                    len(os.listdir(os.path.join(self.scf_workdir, subdir))),
                    len(self._kgrids))

    def _set_scf_done(self, **kwargs):
        # override this since there are calcs inside subdirs
        rootdir = os.path.dirname(self._scf_example)
        for subdir in os.listdir(rootdir):
            if not subdir.startswith(os.path.basename(self._scf_example)):
                # ignore other files/dirs
                continue
            # this subdir contains the kgrid calcs
            for calc in os.listdir(os.path.join(rootdir, subdir)):
                copy_calculation(
                        os.path.join(rootdir, subdir, calc),
                        os.path.join(self.scf_workdir, subdir, calc),
                        **kwargs)


class BasePhononSequencerTest(BaseSCFSequencerTest):
    """Base test class for sequencers with phonons.
    """
    _phonons_script_name = None
    _nphonons = None
    _phonons_example = None
    _phonons_input_variables = None

    def setUp(self):
        if self._phonons_example is None:
            raise DevError("Need to set '_phonons_example'.")
        if self._phonons_script_name is None:
            raise DevError("Need to set '_phonons_script_name'.")
        super().setUp()
        self._set_phonons_tempdir()
        self._set_phonons_command_file()
        self.phonons_workdir = os.path.join(
                self.phonons_tempdir.name, "ph_runs/ph_run")
        self.phonons_command = self.phonons_command_file.name
        # set sequencer object
        self.sequencer.phonons_workdir = self.phonons_workdir
        self.sequencer.phonons_command = self.phonons_command
        self.sequencer.phonons_queuing_system = "local"
        if self._nphonons is not None:
            self.sequencer.nphonons = self._nphonons

    def tearDown(self):
        super().tearDown()
        if os.path.isdir(self.phonons_tempdir.name):
            self.phonons_tempdir.cleanup()
        if os.path.isfile(self.phonons_command):
            self.phonons_command_file.close()

    def test_whole_sequence(self):
        self._set_scf()
        self._set_phonons()
        self.sequencer.write()
        self._check_scf_written()
        self._check_phonons_not_written()
        self.assertFalse(self.sequencer.sequence_completed)
        self.scf_tempdir.cleanup()
        self._set_scf_done()
        # continue sequence
        self.sequencer.write()
        self._check_phonons_written()
        self.assertFalse(self.sequencer.sequence_completed)
        # copy rest of sequence
        self.phonons_tempdir.cleanup()
        self._set_phonons_done()
        self.sequencer.write()
        self.assertTrue(self.sequencer.sequence_completed)

    def test_starting_from_scf(self):
        self._set_scf()
        self._set_scf_done()
        self._set_phonons()
        self.sequencer.write()
        self._check_phonons_written()
        self.assertFalse(self.sequencer.sequence_completed)

    def test_starting_from_scratch(self):
        self._set_scf()
        self.sequencer.write()
        self._check_scf_written()
        self._check_phonons_not_written()
        self.assertFalse(self.sequencer.sequence_completed)

    def _check_phonons(self, func, exceptions=None):
        # check that there are as many phonon calcs written as there are
        # phonon calcs in the sequence.
        # exceptions: optional, list,
        #   list of eceptions strings. if a sequence.workdir contains these
        #   strings, it is ignored.
        if exceptions is not None:
            if not is_list_like(exceptions):
                exceptions = (exceptions, )
        basename = self.sequencer.phonons_workdir

        def _contain_exception(workdir):
            if exceptions is None:
                return False
            for exception in exceptions:
                if exception in workdir:
                    return True
            return False

        for calc in self.sequencer.sequence:
            if basename not in calc.workdir or _contain_exception(
                    calc.workdir):
                continue
            func(CalculationDirectory.is_calculation_directory(calc.workdir))

    def _check_phonons_written(self):
        self._check_phonons(self.assertTrue)

    def _check_phonons_not_written(self):
        self._check_phonons(self.assertFalse)

    def _set_phonons(self):
        if self._phonons_input_variables is None:
            raise DevError("Need to set '_phonons_input_variables'.")
        self.sequencer.phonons_input_variables = self._phonons_input_variables

    def _set_phonons_done(self):
        basename = os.path.basename(self._phonons_example)
        dirname = os.path.dirname(self._phonons_example)
        for calcdir in os.listdir(dirname):
            if basename not in calcdir:
                continue
            ph_idx = calcdir.split(basename)[-1]
            phdir = self.sequencer.phonons_workdir + ph_idx
            copy_calculation(
                    os.path.join(dirname, calcdir), phdir,
                    new_parents=[self.scf_workdir],
                    new_pseudos=self._pseudos)

    def _set_phonons_command_file(self):
        if hasattr(self, "phonons_command_file"):
            return
        self.phonons_command_file = tempfile.NamedTemporaryFile(
                suffix=self._phonons_script_name)

    def _set_phonons_tempdir(self):
        if hasattr(self, "phonons_tempdir"):
            return
        self.phonons_tempdir = tempfile.TemporaryDirectory()


class BasePhononConvergenceSequencerTest(
        BaseConvergenceSequencerTest,
        BasePhononSequencerTest):
    """Base test case for phonon convergence sequencers.
    """
    _nphonons = 1
    _phonons_convergence_criterion = 10

    def setUp(self):
        BaseConvergenceSequencerTest.setUp(self)
        BasePhononSequencerTest.setUp(self)
        self.sequencer.phonons_qpt = [0.0, 0.0, 0.0]  # just do gamma
        self.sequencer.phonons_convergence_criterion = (
                self._phonons_convergence_criterion)

    def test_whole_sequence(self):
        self._set_scf()
        self._set_phonons()
        self.sequencer.write()
        self.assertFalse(self.sequencer.sequence_completed)
        self._check_scf_written()
        self._check_phonons_not_written()
        self.scf_tempdir.cleanup()
        self._set_scf_done()
        self.sequencer.write()
        self.assertFalse(self.sequencer.sequence_completed)
        self._check_phonons_written()
        self.phonons_tempdir.cleanup()
        self._set_phonons_done()
        self.sequencer.launch()
        self._check_plot_written()

    def test_starting_from_scf(self):
        self._set_scf()
        self._set_scf_done()
        self._set_phonons()
        self.sequencer.write()
        self._check_phonons_written()
        self.assertFalse(self.sequencer.sequence_completed)
        # set scf input variables, and restart. check its good
        self.sequencer.write()
        self._check_phonons_written()
        self.assertFalse(self.sequencer.sequence_completed)

    def _check_scf_written(self):
        BaseConvergenceSequencerTest._check_scf_written(self)

    def _check_phonons_written(self):
        name = self.sequencer._parameter_to_converge_input_variable_name
        self.assertEqual(
                len(os.listdir(os.path.join(self.scf_workdir, name))),
                len(os.listdir(os.path.join(self.phonons_workdir, name))))

    def _check_phonons_not_written(self):
        self.assertFalse(os.path.isdir(self.phonons_workdir))

    def _set_phonons_done(self):
        name = self.sequencer._parameter_to_converge_input_variable_name
        for calc in self._phonons_example:
            basename = os.path.basename(calc)
            dest = os.path.join(self.phonons_workdir, name, basename)
            copy_calculation(
                    calc, dest,
                    new_parents=[
                        os.path.join(self.scf_workdir, name, basename)],
                    new_pseudos=self._pseudos,
                    )


class BaseEcutPhononConvergenceSequencerTest(
        BasePhononConvergenceSequencerTest,
        BaseEcutConvergenceSequencerTest):
    """Base test class for ecut phonons convergence sequencers.
    """
    _ecuts = None

    def setUp(self):
        BasePhononConvergenceSequencerTest.setUp(self)
        if self._ecuts is None:
            raise DevError("Need to set '_ecuts'.")
        self.sequencer.scf_ecuts = self._ecuts

    def _check_scf_written(self):
        BaseEcutConvergenceSequencerTest._check_scf_written(self)


class BaseKgridPhononConvergenceSequencerTest(
        BasePhononConvergenceSequencerTest,
        BaseKgridConvergenceSequencerTest):
    """Base test class for kgrid phonon convergence sequencers.
    """
    _kgrids = None

    def setUp(self):
        BasePhononConvergenceSequencerTest.setUp(self)
        if self._kgrids is None:
            raise DevError("Need to set '_kgrids'.")
        self.sequencer.scf_kgrids = self._kgrids

    def _check_scf_written(self):
        BaseKgridConvergenceSequencerTest._check_scf_written(self)


class BaseSmearingPhononConvergenceSequencerTest(
        BaseKgridPhononConvergenceSequencerTest,
        BaseSmearingConvergenceSequencerTest):
    """Base test class for smearing phonon convergence sequencers.
    """

    def setUp(self):
        BaseKgridPhononConvergenceSequencerTest.setUp(self)
        BaseSmearingConvergenceSequencerTest.setUp(self)

    def test_whole_sequence(self):
        BaseKgridPhononConvergenceSequencerTest.test_whole_sequence(self)
        self.assertIn(
                self.sequencer.scf_converged_parameter[
                    self.sequencer.smearings_input_variable_name],
                self.sequencer.scf_smearings)
        self.assertIn(
                self.sequencer.scf_converged_parameter[
                    self.sequencer.kgrids_input_variable_name],
                self.sequencer.scf_kgrids)

    def _check_phonons_written(self):
        nwritten = 0
        for subdir in os.listdir(self.phonons_workdir):
            nwritten += len(
                    os.listdir(os.path.join(self.phonons_workdir, subdir)))
        self.assertEqual(
                nwritten,
                len(self.sequencer.scf_smearings) *
                len(self.sequencer.scf_kgrids))

    def _check_plot_written(self):
        # one graph for each smearings tests
        self.assertEqual(
                len([x for x in os.listdir(self.scf_workdir)
                     if x.endswith(".pdf")]),
                len(self.sequencer.scf_smearings),
                msg=self.sequencer.plot_save)

    def _check_scf_written(self):
        self.assertEqual(
                len(os.listdir(self.scf_workdir)),
                len(self.sequencer.scf_smearings))
        for subdir in os.listdir(self.scf_workdir):
            self.assertEqual(
                    len(os.listdir(os.path.join(self.scf_workdir, subdir))),
                    len(self._kgrids))

    def _set_phonons_done(self, **kwargs):
        dirname = os.path.dirname(self._phonons_example)
        for subdir in os.listdir(dirname):
            for calc in os.listdir(os.path.join(dirname, subdir)):
                dest = os.path.join(
                        self.phonons_workdir, subdir, calc)
                copy_calculation(
                    os.path.join(dirname, subdir, calc),
                    dest,
                    new_parents=[
                        os.path.join(self.scf_workdir, subdir, calc)],
                    **kwargs
                    )

    def _set_scf_done(self, **kwargs):
        BaseSmearingConvergenceSequencerTest._set_scf_done(self, **kwargs)


class BaseBandStructureSequencerTest(BaseSCFSequencerTest):
    """Base class for a sequencer that creates a band structure plot.
    """
    _band_structure_example = None
    _band_structure_input_variables = None
    _band_structure_kpoint_path = None
    _band_structure_kpoint_path_density = None

    def setUp(self):
        if self._band_structure_example is None:
            raise DevError("Need to set '_band_structure_example'.")
        if self._band_structure_input_variables is None:
            raise DevError("Need to set '_band_structure_input_variables'.")
        if self._band_structure_kpoint_path is None:
            raise DevError("Need to set '_band_structure_kpoint_path'.")
        if self._band_structure_kpoint_path_density is None:
            raise DevError("Need to set _band_structure_kpoint_path_density'.")
        super().setUp()
        self._set_band_structure_tempdir()
        self.band_structure_workdir = os.path.join(
                self.band_structure_tempdir.name, "band_structure_run")
        self.sequencer.band_structure_workdir = self.band_structure_workdir
        self.sequencer.band_structure_queuing_system = "local"
        self.sequencer.band_structure_command = self.scf_command
        self.sequencer.plot_show = False
        self.sequencer.plot_save = None

    def tearDown(self):
        super().tearDown()
        if os.path.isdir(self.band_structure_workdir):
            self.band_structure_tempdir.cleanup()

    def test_starting_from_scratch(self):
        """Test that starting from scratch works.
        """
        self._set_scf()
        self._set_band_structure()
        self.sequencer.write()
        self._check_scf_written()
        self.assertFalse(self.sequencer.sequence_completed)
        # cleanup and set done
        self.scf_tempdir.cleanup()
        self._set_scf_done()
        self.sequencer.write()
        self._check_band_structure_written()
        self.assertFalse(self.sequencer.sequence_completed)
        # check everything done
        self.band_structure_tempdir.cleanup()
        self._set_band_structure_done()
        self.sequencer.launch()
        self.assertTrue(self.sequencer.sequence_completed)

    def _check_band_structure(self, func):
        func(CalculationDirectory.is_calculation_directory(
            self.band_structure_workdir))

    def _check_band_structure_written(self):
        self._check_band_structure(self.assertTrue)

    def _check_band_structure_not_written(self):
        self._check_band_structure(self.assertFalse)

    def _set_band_structure(self):
        self.sequencer.band_structure_kpoint_path = (
                self._band_structure_kpoint_path
                )
        self.sequencer.band_structure_kpoint_path_density = (
                self._band_structure_kpoint_path_density)
        self.sequencer.band_structure_input_variables = (
                self._band_structure_input_variables)

    def _set_band_structure_done(self):
        copy_calculation(
                self._band_structure_example,
                self.band_structure_workdir,
                new_parents=[self.scf_workdir],
                new_pseudos=self._pseudos)

    def _set_band_structure_tempdir(self):
        if not hasattr(self, "band_structure_tempdir"):
            self.band_structure_tempdir = tempfile.TemporaryDirectory()


class BasePositiveKpointsNSCFSequencerTest(BaseSCFSequencerTest):
    """Base class for a calculation that requires both a scf and a positive
    kpts nscf run.
    """
    _nscf_example = None
    _nscf_input_variables = None
    _nscf_kgrid = None

    def setUp(self):
        if self._nscf_example is None:
            raise DevError("Need to set '_nscf_example'.")
        if self._nscf_kgrid is None:
            raise DevError("Need to set '_nscf_kgrid'.")
        if self._nscf_input_variables is None:
            raise DevError("Need to set '_nscf_input_variables'.")
        super().setUp()
        self._set_nscf_tempdir()
        self.nscf_workdir = os.path.join(self.nscf_tempdir.name, "nscf_run")
        self.sequencer.nscf_workdir = self.nscf_workdir
        self.sequencer.nscf_queuing_system = "local"
        self.sequencer.nscf_command = self.scf_command

    def tearDown(self):
        super().tearDown()
        if os.path.isdir(self.nscf_workdir):
            self.nscf_tempdir.cleanup()

    def _check_nscf(self, func):
        func(CalculationDirectory.is_calculation_directory(self.nscf_workdir))

    def _check_nscf_written(self):
        self._check_nscf(self.assertTrue)

    def _check_nscf_not_written(self):
        self._check_nscf(self.assertFalse)

    def _set_nscf(self):
        self.sequencer.nscf_kgrid = self._nscf_kgrid
        self.sequencer.nscf_input_variables = self._nscf_input_variables

    def _set_nscf_done(self, **kwargs):
        copy_calculation(
                self._nscf_example, self.nscf_workdir,
                new_parents=[self.scf_workdir],
                new_pseudos=self._pseudos,
                **kwargs)

    def _set_nscf_tempdir(self):
        if not hasattr(self, "nscf_tempdir"):
            self.nscf_tempdir = tempfile.TemporaryDirectory()
