import os
import tempfile

from ..routines_for_tests import TemporaryCalculation
from ...exceptions import DevError
from ...handlers import CalculationDirectory
from ...launchers import __ALL_LAUNCHERS__
from ...routines import is_list_like
from ...status_checkers.exceptions import CalculationNotFinishedError


class BaseCleanerTest:
    """Base class for all Cleaners unittests.
    """
    _cleaner_cls = None
    _calculation_example = None
    _calculation_example_input_variables = None
    _calculation_example_parents = None
    _script_name = None

    def setUp(self):
        if self._cleaner_cls is None:
            raise DevError("Need to set '_cleaner_cls'.")
        if self._calculation_example is None:
            raise DevError("Need to set '_calculation_example'.")
        if self._calculation_example_input_variables is None:
            raise DevError(
                    "Need to set '_calculation_example_input_variables'.")
        self.calc_parents = []
        if self._calculation_example_parents is not None:
            if not is_list_like(self._calculation_example_parents):
                raise DevError(
                        "'_calculation_example_parents' must be a list.")
        # copy example calculation into a temp dir
        self.calc = TemporaryCalculation(
                self._calculation_example, copy_on_creation=False)
        self.cleaner = self._cleaner_cls()
        if self._script_name is None:
            raise DevError("Need to set '_script_name'.")
        self.command_file = tempfile.NamedTemporaryFile(
                suffix=self._script_name)

    def tearDown(self):
        self.calc.cleanup()
        for parent in self.calc_parents:
            parent.cleanup()

    def test_clean(self):
        # check that cleaning actually works
        self.calc.copy_calculation()
        self.cleaner.calculation_directory = self.calc.path
        to_delete = [x.path for x in self.cleaner.get_files_to_delete()]
        self.cleaner.clean()
        # check that all files have been deleted
        for path in CalculationDirectory.from_calculation(
                self.calc.path).walk(paths_only=True):
            if path in to_delete:
                self.assertFalse(os.path.exists(path))

    def test_clean_abort_if_calculation_not_started(self):
        # write a qe_pw calc using launcher and check that
        # cleaning does not work
        launcher = self._get_launcher()
        if self._calculation_example_parents is not None:
            for parent in self._calculation_example_parents:
                parent_calc = TemporaryCalculation(
                        parent, copy_on_creation=True)
                # this list will be cleaned on tearDown()
                self.calc_parents.append(parent_calc)
                launcher.link_calculation(parent_calc.path)
        launcher.command = self.command_file.name
        launcher.write()
        self.cleaner.calculation_directory = self.calc.path
        with self.assertRaises(CalculationNotFinishedError):
            self.cleaner.clean()
        # check that force cleaning does not complain
        self.cleaner.clean(force=True)

    def _get_launcher(self):
        """Creates a launcher object
        """
        launcher_cls = __ALL_LAUNCHERS__[self.cleaner.calctype]
        launcher = launcher_cls("test")
        launcher.workdir = self.calc.path
        launcher.queuing_system = "local"
        launcher.input_variables = (
                self._calculation_example_input_variables.copy())
        return launcher
