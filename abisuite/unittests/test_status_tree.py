import os
import tempfile

from .bases import TestCase
from .variables_for_tests import abinit_vars, abinit_AlAs_pseudos
from ..launchers import AbinitMassLauncher
from ..status_checkers import StatusTree


here = os.path.dirname(os.path.abspath(__file__))
# remove ecut from input vars
abinit_vars = abinit_vars.copy()
abinit_vars.pop("ecut")


class TestStatusTree(TestCase):
    jobnames = ["ecut5", "ecut10"]

    def setUp(self):
        self.tempdir = tempfile.TemporaryDirectory()
        self.path = self.tempdir.name
        # need to mock the abinit script
        self.abinit = tempfile.NamedTemporaryFile(suffix="abinit")
        self.launcher = AbinitMassLauncher(self.jobnames)
        self.launcher.workdir = self.path
        self.launcher.common_pseudos = abinit_AlAs_pseudos
        self.launcher.common_input_variables = abinit_vars
        self.launcher.specific_input_variables = [{"ecut": 5.0},
                                                  {"ecut": 10.0}]
        self.launcher.command = self.abinit.name
        self.launcher.queuing_system = "local"
        self.launcher.write()
        self.status_tree = StatusTree(self.path, loglevel=1)

    def tearDown(self):
        self.abinit.close()
        self.tempdir.cleanup()
        del self.launcher
        del self.tempdir

    def test_status_check(self):
        statuses = self.status_tree.status
        for status in statuses:
            self.assertFalse(status["calculation_started"])
            self.assertFalse(status["calculation_finished"])

    def test_print_status(self):
        # test that printing status does not raise an error
        self.status_tree.print_status()

    def test_print_ecut_convergence(self):
        # test that printing the ecut convergence does not raise an error
        self.status_tree.print_attributes("status", "convergence_reached",
                                          "ecut", "etotal",
                                          delta="etotal",
                                          sortby="ecut")
        # test that forgetting the sortby, it raises an error
        with self.assertRaises(ValueError):
            self.status_tree.print_attributes("status", "convergence_reached",
                                              "ecut", "etotal",
                                              delta="etotal")
