import os
import tempfile

from .bases import BaseWritableParserTest
from ..bases import TestCase
from ...handlers.file_parsers import SymLinkParser
from ...handlers.file_writers import SymLinkWriter
from ...linux_tools import touch


class TestSymLinkParser(BaseWritableParserTest, TestCase):
    _parser_class = SymLinkParser
    _writer_class = SymLinkWriter

    def setUp(self):
        self.tempdir = tempfile.TemporaryDirectory()
        self.path = os.path.join(self.tempdir.name, "test")
        self.writer = self._writer_class()
        self.writer.path = self.path
        self.source = os.path.join(self.tempdir.name, "source_file")
        touch(self.source)
        self.writer.source = self.source
        self.writer.write()

    def test_from_calculation(self):
        # irrelevant
        with self.assertRaises((FileNotFoundError, NotImplementedError)):
            self._parser_class.from_calculation(self.tempdir.name)
