import os
import tempfile
import unittest

from .bases import BaseWritableParserTest
from ..routines_for_tests import create_pbs_test_writer
from ...handlers.file_parsers import PBSParser
from ...handlers.file_writers import PBSWriter


class TestPBSParser(BaseWritableParserTest, unittest.TestCase):
    _parser_class = PBSParser
    _writer_class = PBSWriter
    _file_suffix = ".sh"

    def setUp(self):
        self.tempdir = tempfile.TemporaryDirectory()
        self.path = os.path.join(self.tempdir.name, "test")
        if self._file_suffix is not None:
            self.path += self._file_suffix
        self.writer = self._writer_class()
        self.writer.path = self.path
        create_pbs_test_writer(self.writer)
        self.writer.write()

    def test_from_calculation(self):
        # TODO: implement this test
        pass
