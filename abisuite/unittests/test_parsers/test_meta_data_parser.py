import unittest

from .bases import BaseWritableParserTest
from ...handlers.file_parsers import MetaDataParser
from ...handlers.file_writers import MetaDataWriter


class TestMetaDataParser(BaseWritableParserTest, unittest.TestCase):
    _file_suffix = MetaDataParser._expected_ending
    _parser_class = MetaDataParser
    _writer_class = MetaDataWriter

    def test_from_calculation(self):
        self.parser = self._parser_class.from_calculation(self.tempdir.name)
        self.parser.read()
        self.assertEqual(self.parser.structure, self.writer.structure)
