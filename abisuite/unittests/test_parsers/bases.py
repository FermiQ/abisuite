import os
import tempfile

from ...exceptions import DevError


class BaseParserTest:
    """Base class for parser classes for non-writable files.
    """
    _example_file = None
    _parser_class = None

    def setUp(self):
        if self._example_file is None:
            raise DevError(
                    f"Need to set '_example_file' in '{self.__class__}'.")
        if self._parser_class is None:
            raise DevError(
                    f"Need to set '_parser_class' in '{self.__class__}'.")
        self.parser = self._parser_class.from_file(self._example_file)
        self.parser.read()

    def test_parsing(self):
        # nothing to do just make sure there is no errors
        self.assertEqual(self._example_file, self.parser.path)


class BaseWritableParserTest:
    """base test for file parsers which can be written.
    """
    _file_suffix = None
    _parser_class = None
    _writer_class = None

    def setUp(self, write=True, initialize_attributes=True):
        if self._parser_class is None:
            raise DevError("Need to setup '_parser_class'")
        if self._writer_class is None:
            raise DevError("Need to setup '_writer_class'")
        self.tempdir = tempfile.TemporaryDirectory()
        self.path = os.path.join(self.tempdir.name, "test")
        if self._file_suffix is not None:
            self.path += self._file_suffix
        self.writer = self._writer_class()
        if initialize_attributes:
            for prop in self.writer.structure.all_attributes:
                if prop in self.writer.structure.set_attributes:
                    setattr(self.writer, prop, set())
                    continue
                if prop in self.writer.structure.list_attributes:
                    setattr(self.writer, prop, [])
                    continue
                setattr(self.writer, prop, prop)
        self.writer.path = self.path
        if write:
            self.writer.write()

    def tearDown(self):
        self.tempdir.cleanup()
        del self.tempdir

    def test_parsing(self):
        self.parser = self._parser_class.from_file(self.path)
        self.parser.read()
        self.assertEqual(
                self.parser.structure, self.writer.structure,
                msg=f"{self.parser.structure} != {self.writer.structure}")


class BaseInputParserTestNoInputVariables(BaseWritableParserTest):
    _file_ending = ".in"

    def setUp(self, write=True):
        super().setUp(write=False, initialize_attributes=False)
        if write:
            self.writer.write()


class BaseInputParserTest(BaseInputParserTestNoInputVariables):
    _input_vars = None

    def setUp(self):
        if self._input_vars is None:
            raise DevError(
                    f"Need to set '_input_vars' in '{self.__class__}'.")
        super().setUp(write=False)
        self.writer.input_variables = self._input_vars
        self.writer.write()
