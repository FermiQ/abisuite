import os
import tempfile

from .bases import TestCase
from ..exceptions import DirectoryExistsError
from ..linux_tools import mkdir, which, ln_s, touch


class TestLinuxTools(TestCase):
    """Test case for the linux tools.
    """

    def test_mkdir(self):
        # check that directory creation works recursively too
        with tempfile.TemporaryDirectory() as tempdir:
            newdir = os.path.join(tempdir, "newdir", "newdir_2")
            mkdir(newdir)
            self.assertTrue(os.path.isdir(newdir))
            self.assertTrue(os.path.isdir(os.path.dirname(newdir)))
            # check that creating a directory when it already exists does
            # nothing (like raising an error)
            mkdir(newdir)
        # check that an error is raised when attempting to create dir on a file
        with tempfile.NamedTemporaryFile() as temporaryfile:
            with self.assertRaises(FileExistsError):
                mkdir(temporaryfile.name)

    def test_touch(self):
        # test that creating a file in a temporary directory works
        with tempfile.TemporaryDirectory() as tempdir:
            filename = os.path.join(tempdir, "file")
            touch(filename)
            self.assertTrue(os.path.exists(filename))
            self.assertTrue(os.path.isfile(filename))
            # test that creating a file at the same place will raise an error
            with self.assertRaises(FileExistsError):
                touch(filename)
            # test that creating a file a the directory's place will raise
            with self.assertRaises(DirectoryExistsError):
                touch(tempdir)

    def test_which(self):
        # python should be accessible everywhere!
        which_result = which("python")
        self.assertIn("python", which_result, msg=which_result)
        # check for a non-existent script
        self.assertIs(which("non_existent_script"), None)

    def test_ln_s(self):
        # create 2 temporary directory. inside the first create a file
        TD = tempfile.TemporaryDirectory
        with TD() as tempdir1, TD() as tempdir2:
            filename = os.path.join(tempdir1, "file")
            touch(filename)
            target = os.path.join(tempdir2, "file_link")
            ln_s(filename, target)
            self.assertTrue(os.path.exists(target))
            self.assertTrue(os.path.islink(target))
            self.assertTrue(os.path.samefile(os.readlink(target), filename))
            # test the symlink to a directory
            target = os.path.join(tempdir2, "dir_link")
            ln_s(tempdir1, target)
            self.assertTrue(os.path.exists(target))
            self.assertTrue(os.path.islink(target))
            self.assertTrue(os.path.samefile(os.readlink(target), tempdir1))
            # check that if directory needs to be created, it is
            target = os.path.join(tempdir2, "new_dir", "file_link")
            ln_s(filename, target)
            self.assertTrue(os.path.exists(target))
            self.assertTrue(os.path.islink(target))
            self.assertTrue(os.path.samefile(os.readlink(target), filename))
