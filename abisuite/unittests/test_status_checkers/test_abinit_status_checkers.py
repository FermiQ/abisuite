import unittest

from .bases import BaseStatusCheckerTest, BaseSCFStatusCheckerTest
from ..variables_for_tests import (
        AbinitCalc, AbinitAnaddbCalc, AbinitMrgddbCalc,
        )


class TestAbinitStatusChecker(BaseSCFStatusCheckerTest, unittest.TestCase):
    """Test case for an abinit calculation status checker.
    """
    _calculation = AbinitCalc


class TestAbinitAnaddbStatusChecker(BaseStatusCheckerTest, unittest.TestCase):
    """Test case for an anaddb calculation status checker.
    """
    _calculation = AbinitAnaddbCalc


class TestAbinitMrgddbStatusChecker(BaseStatusCheckerTest, unittest.TestCase):
    """Test case for a mrgddb calculation status checker.
    """
    _calculation = AbinitMrgddbCalc
