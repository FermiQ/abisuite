from .bases import BaseMassLauncherTest
from ..variables_for_tests import (
        qe_dosx_vars, qe_dynmatx_vars,
        qe_epsilonx_vars, qe_fsx_vars, qe_ld1x_vars,
        qe_matdynx_vars, qe_phx_vars, qe_ppx_vars, qe_projwfcx_vars,
        qe_pwx_vars,
        qe_q2rx_vars,
        QEPHCalc, QEPWCalc, QEQ2RCalc)
from ...launchers import (
        QEDOSMassLauncher, QEDynmatMassLauncher,
        QEEpsilonMassLauncher, QEFSMassLauncher, QELD1MassLauncher,
        QEMatdynMassLauncher, QEPHMassLauncher, QEPPMassLauncher,
        QEProjwfcMassLauncher,
        QEPWMassLauncher, QEQ2RMassLauncher,
        )
import unittest


class BaseRequiredInputMassLauncherTest(BaseMassLauncherTest):
    """Class that test when masslauncher misses a required input file.
    """
    def test_unlinked_calculation_raise_error(self):
        self.masslauncher.clear_input_files()
        with self.assertRaises(FileNotFoundError):
            self.masslauncher.validate()


class TestQEDOSMassLauncher(BaseRequiredInputMassLauncherTest,
                            unittest.TestCase):
    _calctype = "dos.x"
    _common_vars = qe_dosx_vars.copy()
    _link_calculation = QEPWCalc
    _jobnames = ["deltaE1", "deltaE2"]
    _mass_launcher_class = QEDOSMassLauncher
    _specific_vars = [{"DeltaE": 0.1}, {"DeltaE": 0.2}]


class TestQEDynmatMassLauncher(BaseRequiredInputMassLauncherTest,
                               unittest.TestCase):
    _calctype = "dynmat.x"
    _common_vars = qe_dynmatx_vars.copy()
    _link_calculation = QEPHCalc
    _jobnames = ["q1", "q2"]
    _mass_launcher_class = QEDynmatMassLauncher
    _specific_vars = [{"q(1)": 0.1}, {"q(1)": 0.2}]


class TestQEEpsilonMassLauncher(BaseRequiredInputMassLauncherTest,
                                unittest.TestCase):
    _calctype = "epsilon.x"
    _common_vars = qe_epsilonx_vars.copy()
    _link_calculation = QEPWCalc
    _jobnames = ["intersmear1", "intersmear2"]
    _mass_launcher_class = QEEpsilonMassLauncher
    _specific_vars = [{"intersmear": 0.1}, {"intersmear": 0.2}]


# usually there's no point of using this class with different input vars
# only with different input calculations
# TODO: implement this test with different input calculations
class TestQEFSMassLauncher(BaseRequiredInputMassLauncherTest,
                           unittest.TestCase):
    _calctype = "fs.x"
    _common_vars = qe_fsx_vars.copy()
    _jobnames = ["fs1", "fs2"]
    _link_calculation = QEPWCalc
    _mass_launcher_class = QEFSMassLauncher
    _specific_vars = [{}, {}]


class TestQELD1MassLauncher(BaseMassLauncherTest, unittest.TestCase):
    _mass_launcher_class = QELD1MassLauncher
    _calctype = "ld1.x"
    _jobnames = ["ecutmax1", "ecutmax2"]
    _common_vars = qe_ld1x_vars.copy()
    _specific_vars = [{"ecutmax": 100}, {"ecutmax": 200}]


class TestQEMatdynMassLauncher(BaseRequiredInputMassLauncherTest,
                               unittest.TestCase):
    _calctype = "matdyn.x"
    _common_vars = qe_matdynx_vars.copy()
    _jobnames = ["asr1", "asr2"]
    _link_calculation = QEQ2RCalc
    _mass_launcher_class = QEMatdynMassLauncher
    _specific_vars = [{"asr": "no"}, {"asr": "simple"}]


class TestQEPHMassLauncher(BaseRequiredInputMassLauncherTest,
                           unittest.TestCase):
    _calctype = "ph.x"
    _common_vars = qe_phx_vars.copy()
    _jobnames = ["mass1", "mass2"]
    _link_calculation = QEPWCalc
    _mass_launcher_class = QEPHMassLauncher
    _specific_vars = [{"amass(1)": 1}, {"amass(2)": 2}]


class TestQEPPMassLauncher(BaseRequiredInputMassLauncherTest,
                           unittest.TestCase):
    _calctype = "pp.x"
    _common_vars = qe_ppx_vars.copy()
    _jobnames = ["kpt1", "kpt2"]
    _link_calculation = QEPWCalc
    _mass_launcher_class = QEPPMassLauncher
    _specific_vars = [{"kpoint": 1}, {"kpoint": 2}]


class TestQEProjwfcMassLauncher(BaseRequiredInputMassLauncherTest,
                                unittest.TestCase):
    _calctype = "projwfc.x"
    _common_vars = qe_projwfcx_vars.copy()
    _jobnames = ["pdos1", "pdos2"]
    _link_calculation = QEPWCalc
    _mass_launcher_class = QEProjwfcMassLauncher
    _specific_vars = [{"degauss": 0.01}, {"degauss": 0.02}]


class TestQEPWMassLauncher(BaseMassLauncherTest, unittest.TestCase):
    _mass_launcher_class = QEPWMassLauncher
    _calctype = "pw.x"
    _jobnames = ["ecutwfc5", "ecutwfc10"]
    _common_vars = qe_pwx_vars.copy()
    _specific_vars = [{"ecutwfc": 5}, {"ecutwfc": 10}]


class TestQEQ2RMassLauncher(BaseRequiredInputMassLauncherTest,
                            unittest.TestCase):
    _calctype = "q2r.x"
    _common_vars = qe_q2rx_vars.copy()
    _jobnames = ["zasr1", "zasr2"]
    _link_calculation = QEPHCalc
    _mass_launcher_class = QEQ2RMassLauncher
    _specific_vars = [{"zasr": "no"}, {"zasr": "simple"}]
