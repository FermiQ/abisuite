import os
import pytest
import shutil
import tempfile

from abisuite import AbinitWorkflow
from .bases import BaseWorkflowTest, __TEST_WORKFLOW_ROOT__
from ..bases import TestCase
from ..routines_for_tests import copy_calculation


# ############################  GS ############################################
ABINIT_GS_INPUT_VARS = {
        "acell": [7.2078778121] * 3,
        "rprim": [[0.0, 0.70710678119, 0.70710678119],
                  [0.70710678119, 0.0, 0.70710678119],
                  [0.70710678119, 0.70710678119, 0.0]],
        "ntypat": 1,
        "znucl": [14],
        "natom": 2,
        "typat": [1, 1],
        "xred": [[0.0, 0.0, 0.0], [1/4, 1/4, 1/4]],
        "ecut": 10.0,
        "kptopt": 1,
        "ngkpt": [6, 6, 6],
        "nshiftk": 4,
        "shiftk": [[0.5, 0.5, 0.5], [0.5, 0.0, 0.0],
                   [0.0, 0.5, 0.0], [0.0, 0.0, 0.5]],
        "nstep": 10,
        "toldfe": 1.0e-6,
        "diemac": 12.0,
        }
ABINIT_PSEUDOS = [os.path.join(
    __TEST_WORKFLOW_ROOT__, "test_files", "pseudos", "Si.psp8")
    ]
ABINIT_GS_EXAMPLE = os.path.join(
        __TEST_WORKFLOW_ROOT__,
        "test_files", "abinit_si", "nc", "gs_run")

# ############################# GS ECUT CONVERGENCE ###########################
# Make the test with PAW since there is 2 convergence to do
ABINIT_GS_ECUT_CONVERGENCE_INPUT_VARIABLES = {
        "acell": [10.18] * 3,
        "rprim": [[0.0, 0.5, 0.5], [0.5, 0.0, 0.5], [0.5, 0.5, 0.0]],
        "ntypat": 1,
        "znucl": [14],
        "natom": 2,
        "typat": [1, 1],
        "xred": [[0.0, 0.0, 0.0], [1/4, 1/4, 1/4]],
        "kptopt": 1,
        "pawecutdg": 30.0,
        "ngkpt": [2, 2, 2],
        "nshiftk": 4,
        "shiftk": [[0.5, 0.5, 0.5], [0.5, 0.0, 0.0],
                   [0.0, 0.5, 0.0], [0.0, 0.0, 0.5]],
        "nstep": 50,
        "tolvrs": 1.0e-10,
        "diemac": 12.0,
        }
ABINIT_GS_ECUT_CONVERGENCE_ECUTS = [10, 20, 30]
ABINIT_GS_ECUT_CONVERGENCE_PAWECUTS = [20, 30, 50]
ABINIT_GS_ECUT_CONVERGENCE_CRITERION = 10
ABINIT_GS_PAWECUT_CONVERGENCE_CRITERION = 10
ABINIT_GS_ECUT_CONVERGENCE_EXAMPLE = os.path.join(
        __TEST_WORKFLOW_ROOT__,
        "test_files", "abinit_si", "paw", "gs_ecut_convergence")
ABINIT_GS_PAWECUT_CONVERGENCE_EXAMPLE = os.path.join(
        __TEST_WORKFLOW_ROOT__,
        "test_files", "abinit_si", "paw", "gs_ecut_convergence", "pawecutdg")

# ######################## GS KGRID CONVERGENCE ###############################
ABINIT_GS_KGRID_CONVERGENCE_INPUT_VARS = {
         "acell": [10.18] * 3,
         "rprim": [[0.0, 0.5, 0.5], [0.5, 0.0, 0.5], [0.5, 0.5, 0.0]],
         "ntypat": 1,
         "znucl": [14],
         "natom": 2,
         "typat": [1, 1],
         "xred": [[0.0, 0.0, 0.0], [1/4, 1/4, 1/4]],
         "kptopt": 1,
         "nshiftk": 4,
         "shiftk": [[0.5, 0.5, 0.5], [0.5, 0.0, 0.0],
                    [0.0, 0.5, 0.0], [0.0, 0.0, 0.5]],
         "nstep": 10,
         "ecut": 20,
         "toldfe": 1.0e-6,
         "diemac": 12.0,
         }
ABINIT_GS_KGRID_CONVERGENCE_CRITERION = 1
ABINIT_GS_KGRID_CONVERGENCE_KGRIDS = [[2, 2, 2], [4, 4, 4],
                                      [6, 6, 6], [8, 8, 8]]
ABINIT_GS_KGRID_CONVERGENCE_VARNAME = "ngkpt"
ABINIT_GS_KGRID_CONVERGENCE_EXAMPLE = os.path.join(
        __TEST_WORKFLOW_ROOT__, "test_files", "abinit_si", "nc",
        "kgrid_convergence")

# ######################## SMEARING CONVERGENCE ###############################
ABINIT_GS_SMEARING_CONVERGENCE_INPUT_VARS = {
        "acell": [7.6] * 3,
        "rprim": [[0, 0.5, 0.5], [0.5, 0, 0.5], [0.5, 0.5, 0]],
        "ntypat": 1,
        "ecut": 20,
        "znucl": [13],
        "natom": 1,
        "typat": 1,
        "xred": [0, 0, 0],
        "nshiftk": 4,
        "shiftk": [[0.5, 0.5, 0.5], [0.5, 0.0, 0.0], [0, 0.5, 0], [0, 0, 0.5]],
        "nstep": 10,
        "toldfe": 1e-6,
        "occopt": 4,
        }
ABINIT_GS_SMEARING_CONVERGENCE_CRITERION = 10
ABINIT_GS_SMEARING_CONVERGENCE_KGRIDS = [[x, x, x] for x in [2, 4, 8]]
ABINIT_GS_SMEARING_CONVERGENCE_VARNAME = "ngkpt"
ABINIT_GS_SMEARING_CONVERGENCE_SMEARINGS = [0.1, 0.01, 0.001]
ABINIT_GS_SMEARING_CONVERGENCE_EXAMPLE = os.path.join(
        __TEST_WORKFLOW_ROOT__, "test_files", "abinit_Al",
        "gs_smearing_convergence")
ABINIT_GS_SMEARING_CONVERGENCE_PSEUDOS = [os.path.join(
    __TEST_WORKFLOW_ROOT__, "test_files", "pseudos", "Al.psp8")
    ]

# ############################ RELAXATION #####################################
ABINIT_RELAXATION_INPUT_VARIABLES = {
        "acell": [10.18] * 3,
        "rprim": [[0.0, 0.5, 0.5], [0.5, 0.0, 0.5],
                  [0.5, 0.5, 0.0]],
        "ntypat": 1,
        "znucl": [14],
        "natom": 2,
        "typat": [1, 1],
        "xred": [[0.0, 0.0, 0.0], [1/4, 1/4, 1/4]],
        "kptopt": 1,
        "nshiftk": 4,
        "shiftk": [[0.5, 0.5, 0.5], [0.5, 0.0, 0.0],
                   [0.0, 0.5, 0.0], [0.0, 0.0, 0.5]],
        "ngkpt": [6, 6, 6],
        "ecut": 20.0,
        "nstep": 10,
        "tolvrs": 1.0e-10,
        "diemac": 12.0,
        "optcell": 1,  # only modify acell
        "ionmov": 22,
        "ntime": 20,
        "tolmxf": 5e-04,
        "ecutsm": 0.5,
        "dilatmx": 1.1,
        }
ABINIT_RELAXATION_EXAMPLE = os.path.join(
        __TEST_WORKFLOW_ROOT__, "test_files", "abinit_si", "nc", "relax_run")


# ######################### PHONON ECUT CONVERGENCE PART ######################
# Al example
ABINIT_PHONON_ECUT_CONVERGENCE_PSEUDOS = [os.path.join(
    __TEST_WORKFLOW_ROOT__, "test_files", "pseudos", "Al.psp8")
    ]
ABINIT_PHONON_ECUT_CONVERGENCE_CRITERION = 10
ABINIT_PHONON_ECUT_CONVERGENCE_SCF_INPUT_VARS = {
            "acell": [5.326968652] * 3,
            "rprim": [[0.0, 0.70710678119, 0.70710678119],
                      [0.70710678119, 0.0, 0.70710678119],
                      [0.70710678119, 0.70710678119, 0.0]],
            "ntypat": 1,
            "occopt": 4,
            "znucl": [13],
            "natom": 1,
            "typat": [1],
            "xred": [[0.0, 0.0, 0.0]],
            "tsmear": 0.1,
            "nshiftk": 4,
            "shiftk": [[0.5, 0.5, 0.5], [0.5, 0.0, 0.0],
                       [0.0, 0.5, 0.0], [0.0, 0.0, 0.5]],
            "nstep": 10,
            "toldfe": 1.0e-6,
            "ngkpt": [4, 4, 4],
            }
ABINIT_PHONON_ECUT_CONVERGENCE_PHONONS_INPUT_VARS = {
            "rfatpol": [1, 1],
            "rfdir": [1, 1, 1],
            "tolvrs": 1e-8,
            "nstep": 25,
            }
ABINIT_PHONON_ECUT_CONVERGENCE_ECUTS = [10, 30, 50]
ABINIT_PHONON_ECUT_CONVERGENCE_PHONON_QPT = [0.0, 0.0, 0.0]
ABINIT_PHONON_ECUT_CONVERGENCE_EXAMPLE = os.path.join(
        __TEST_WORKFLOW_ROOT__, "test_files", "abinit_Al",
        "phonon_ecut_convergence")
ABINIT_PHONON_ECUT_CONVERGENCE_OTHER_KWARGS = {
        "compute_electric_field_response": False,
        }
# ########################## PHONON DISPERSION ################################
ABINIT_PHONON_DISPERSION_QPOINT_PATH = [
            {"L": [0.5, 0.0, 0.0]},
            {r"$\Gamma$": [0.0, 0.0, 0.0]},
            {"X": [0.0, 0.5, 0.5]},
            {r"$\Gamma_2$": [1.0, 1.0, 1.0]}]
ABINIT_PHONON_DISPERSION_QPOINT_PATH_DENSITY = 20
ABINIT_PHONON_DISPERSION_PHONONS_INPUT_VARS = {
            "rfatpol": [1, 1],
            "tolvrs": 1e-8,
            "diemac": 12.0,
            "nstep": 25,
            }
ABINIT_PHONON_DISPERSION_QGRID = [6] * 3
ABINIT_PHONON_DISPERSION_EXAMPLE = os.path.join(
        __TEST_WORKFLOW_ROOT__, "test_files", "abinit_si", "nc",
        "phonon_dispersion")
ABINIT_PHONON_DISPERSION_PSEUDOS = [
    os.path.join(__TEST_WORKFLOW_ROOT__, "test_files", "pseudos", "Si.psp8"),
    ]


# ################### PHONON SMEARING CONVERGENCE #############################
ABINIT_PHONON_SMEARING_CONVERGENCE_SCF_INPUT_VARS = {
            "nshiftk": 4,
            "shiftk": [[0.5, 0.5, 0.5], [0.5, 0.0, 0.0],
                       [0, 0.5, 0], [0, 0, 0.5]],
            "nstep": 10,
            "toldfe": 1e-6,
            "occopt": 4,
            "acell": [5.326968652] * 3,
            "natom": 1,
            "ntypat": 1,
            "rprim": [[0.0, 0.70710678119, 0.70710678119],
                      [0.70710678119, 0.0, 0.70710678119],
                      [0.70710678119, 0.70710678119, 0.0]],
            "typat": 1,
            "ecut": 30,
            "xred": [0, 0, 0],
            "znucl": [13.0],
            }
ABINIT_PHONON_SMEARING_CONVERGENCE_PHONONS_INPUT_VARS = {
            "rfatpol": [1, 1],
            "rfdir": [1, 1, 1],
            "tolvrs": 1e-8,
            "nstep": 25}
ABINIT_PHONON_SMEARING_CONVERGENCE_EXAMPLE = os.path.join(
        __TEST_WORKFLOW_ROOT__, "test_files", "abinit_Al",
        "phonon_smearing_convergence")
ABINIT_PHONON_SMEARING_CONVERGENCE_SMEARINGS = [0.1, 0.01, 0.001]
ABINIT_PHONON_SMEARING_CONVERGENCE_KGRIDS = [[x, x, x] for x in [2, 4, 8]]
ABINIT_PHONON_SMEARING_CONVERGENCE_OTHER_KWARGS = {
        "scf_kgrids_input_variable_name": "ngkpt"}
ABINIT_PHONON_SMEARING_CONVERGENCE_CRITERION = 10
ABINIT_PHONON_SMEARING_CONVERGENCE_PHONONS_QPT = [0.0, 0.0, 0.0]


# ########################### BAND STRUCTURE ##################################
ABINIT_BAND_STRUCTURE_EXAMPLE = os.path.join(
        __TEST_WORKFLOW_ROOT__, "test_files", "abinit_si", "nc",
        "band_structure", "band_structure")
ABINIT_BAND_STRUCTURE_INPUT_VARIABLES = {
        "ecut": 20,
        "iscf": -2,
        "nband": 8,
        "tolwfr": 1e-12,
        "autoparal": 1,
        "diemac": 12.0,
        }
ABINIT_BAND_STRUCTURE_KPOINT_PATH = [
        {"L": [0.5, 0.0, 0.0]},
        {r"$\Gamma$": [0.0, 0.0, 0.0]},
        {"X": [0.0, 0.5, 0.5]},
        {r"$\Gamma_2$": [1.0, 1.0, 1.0]}]
ABINIT_BAND_STRUCTURE_KPOINT_PATH_DENSITY = 20


@pytest.mark.order("first")
class TestAbinitWorkflow(BaseWorkflowTest, TestCase):
    """Test case for AbinitWorkflow class.
    """
    _band_structure_example = ABINIT_BAND_STRUCTURE_EXAMPLE
    _band_structure_input_variables = ABINIT_BAND_STRUCTURE_INPUT_VARIABLES
    _band_structure_kpoint_path = ABINIT_BAND_STRUCTURE_KPOINT_PATH
    _band_structure_kpoint_path_density = (
            ABINIT_BAND_STRUCTURE_KPOINT_PATH_DENSITY)
    _gs_ecut_convergence_criterion = ABINIT_GS_ECUT_CONVERGENCE_CRITERION
    _gs_ecut_convergence_input_vars = (
            ABINIT_GS_ECUT_CONVERGENCE_INPUT_VARIABLES)
    _gs_ecut_convergence_ecuts = ABINIT_GS_ECUT_CONVERGENCE_ECUTS
    _gs_ecut_convergence_example = ABINIT_GS_ECUT_CONVERGENCE_EXAMPLE
    _gs_ecut_convergence_other_kwargs = {
            "scf_pawecutdg": ABINIT_GS_ECUT_CONVERGENCE_PAWECUTS,
            "scf_pawecutdg_convergence_criterion": (
                ABINIT_GS_ECUT_CONVERGENCE_CRITERION),
            }
    _gs_kgrid_convergence_criterion = ABINIT_GS_KGRID_CONVERGENCE_CRITERION
    _gs_kgrid_convergence_example = ABINIT_GS_KGRID_CONVERGENCE_EXAMPLE
    _gs_kgrid_convergence_input_vars = ABINIT_GS_KGRID_CONVERGENCE_INPUT_VARS
    _gs_kgrid_convergence_kgrids = ABINIT_GS_KGRID_CONVERGENCE_KGRIDS
    _gs_kgrid_convergence_other_kwargs = {
            "scf_kgrids_input_variable_name": (
                ABINIT_GS_KGRID_CONVERGENCE_VARNAME),
            }
    _gs_smearing_convergence_criterion = (
            ABINIT_GS_SMEARING_CONVERGENCE_CRITERION)
    _gs_smearing_convergence_example = ABINIT_GS_SMEARING_CONVERGENCE_EXAMPLE
    _gs_smearing_convergence_input_vars = (
            ABINIT_GS_SMEARING_CONVERGENCE_INPUT_VARS.copy())
    _gs_smearing_convergence_kgrids = ABINIT_GS_SMEARING_CONVERGENCE_KGRIDS
    _gs_smearing_convergence_smearings = (
            ABINIT_GS_SMEARING_CONVERGENCE_SMEARINGS)
    _gs_smearing_convergence_other_kwargs = {
            "scf_kgrids_input_variable_name": (
                ABINIT_GS_SMEARING_CONVERGENCE_VARNAME),
            }
    _gs_example = ABINIT_GS_EXAMPLE
    _gs_input_vars = ABINIT_GS_INPUT_VARS
    _phonon_ecut_convergence_criterion = (
            ABINIT_PHONON_ECUT_CONVERGENCE_CRITERION)
    _phonon_ecut_convergence_scf_input_vars = (
            ABINIT_PHONON_ECUT_CONVERGENCE_SCF_INPUT_VARS)
    _phonon_ecut_convergence_phonons_input_vars = (
            ABINIT_PHONON_ECUT_CONVERGENCE_PHONONS_INPUT_VARS)
    _phonon_ecut_convergence_ecuts = (
            ABINIT_PHONON_ECUT_CONVERGENCE_ECUTS)
    _phonon_ecut_convergence_example = (
            ABINIT_PHONON_ECUT_CONVERGENCE_EXAMPLE)
    _phonon_ecut_convergence_phonon_qpt = (
            ABINIT_PHONON_ECUT_CONVERGENCE_PHONON_QPT)
    _phonon_ecut_convergence_other_kwargs = (
            ABINIT_PHONON_ECUT_CONVERGENCE_OTHER_KWARGS)
    _phonon_dispersion_example = ABINIT_PHONON_DISPERSION_EXAMPLE
    _phonon_dispersion_phonons_input_vars = (
            ABINIT_PHONON_DISPERSION_PHONONS_INPUT_VARS)
    _phonon_dispersion_qgrid = ABINIT_PHONON_DISPERSION_QGRID
    _phonon_dispersion_qpoint_path = ABINIT_PHONON_DISPERSION_QPOINT_PATH
    _phonon_dispersion_qpoint_path_density = (
            ABINIT_PHONON_DISPERSION_QPOINT_PATH_DENSITY)
    _phonon_smearing_convergence_criterion = (
            ABINIT_PHONON_SMEARING_CONVERGENCE_CRITERION)
    _phonon_smearing_convergence_scf_input_vars = (
            ABINIT_PHONON_SMEARING_CONVERGENCE_SCF_INPUT_VARS)
    _phonon_smearing_convergence_phonons_input_vars = (
            ABINIT_PHONON_SMEARING_CONVERGENCE_PHONONS_INPUT_VARS)
    _phonon_smearing_convergence_example = (
            ABINIT_PHONON_SMEARING_CONVERGENCE_EXAMPLE)
    _phonon_smearing_convergence_other_kwargs = (
            ABINIT_PHONON_SMEARING_CONVERGENCE_OTHER_KWARGS)
    _phonon_smearing_convergence_phonon_qpt = (
            ABINIT_PHONON_SMEARING_CONVERGENCE_PHONONS_QPT)
    _phonon_smearing_convergence_smearings = (
            ABINIT_PHONON_SMEARING_CONVERGENCE_SMEARINGS)
    _phonon_smearing_convergence_kgrids = (
            ABINIT_PHONON_SMEARING_CONVERGENCE_KGRIDS)
    _phonons_script_name = "abinit"
    _relaxation_input_vars = ABINIT_RELAXATION_INPUT_VARIABLES
    _set_gs_calculation_kwargs = None
    _scf_script_name = "abinit"
    _workflow_cls = AbinitWorkflow
    _workflow_init_kwargs = {"pseudos": ABINIT_PSEUDOS}

    def setUp(self):
        super().setUp()
        self.mrgddb_command_file = tempfile.NamedTemporaryFile(
                suffix="mrgddb")
        self.anaddb_command_file = tempfile.NamedTemporaryFile(
                suffix="anaddb")

    def test_band_structure_convergence(self):
        workflow = self._workflow_cls(
                band_structure_convergence=True,
                **self._workflow_init_kwargs)
        self._set_band_structure_convergence(workflow)
        workflow.write()
        # make sure scf calcs were written
        seq = workflow.band_structure_convergence_sequencer
        ncalcs = len(seq.scf_specific_input_variables)
        dir_ = os.path.dirname(seq.scf_workdir)
        self.assertEqual(len(os.listdir(dir_)), ncalcs)
        # set done and check workflow completed
        shutil.rmtree(dir_)
        self._set_band_structure_convergence_done(workflow)
        workflow.run()
        # make sure there are 2ncalcs + 2 files in results dir
        results_dir = os.path.join(
                os.path.dirname(os.path.dirname(seq.scf_workdir)), "results")
        self.assertEqual(len(os.listdir(results_dir)), 2 * ncalcs + 2)
        self.assertTrue(workflow.workflow_completed)

    def _check_phonon_dispersion_post_phonons(self, workflow):
        dir_ = os.path.join(
                    self.workflow_dir.name,
                    "phonon_dispersion")
        # make sure q2r calculation is written
        self.assertIn("mrgddb_run", os.listdir(dir_))
        shutil.rmtree(os.path.join(dir_, "mrgddb_run"))
        self._set_phonon_dispersion_done(workflow, mrgddb=True)
        workflow.write()
        # make sure the matdyn calc was written
        self.assertIn("anaddb_run", os.listdir(dir_))
        shutil.rmtree(os.path.join(dir_, "anaddb_run"))
        self._set_phonon_dispersion_done(workflow, anaddb=True)

    def _set_band_structure_convergence(self, workflow):
        calculation_parameters = {
                "command": self.scf_command_file.name,
                "queuing_system": "local",
                }
        workflow.set_band_structure_convergence(
                root_workdir=os.path.join(
                    self.workflow_dir.name, "band_structure_convergence"),
                scf_specific_input_variables=[
                    {"ecut": ecut} for ecut in [10, 30]],
                scf_input_variables={
                    "acell": [7.2078778121] * 3,
                    "rprim": [[0.0, 0.70710678119, 0.70710678119],
                              [0.70710678119, 0.0, 0.70710678119],
                              [0.70710678119, 0.70710678119, 0.0]],
                    "ntypat": 1,
                    "znucl": [14],
                    "natom": 2,
                    "typat": [1, 1],
                    "xred": [
                        [0.0, 0.0, 0.0],
                        [1/4, 1/4, 1/4]],
                    "kptopt": 1,
                    "ngkpt": [6, 6, 6],
                    "nshiftk": 4,
                    "shiftk": [[0.5, 0.5, 0.5], [0.5, 0.0, 0.0],
                               [0.0, 0.5, 0.0], [0.0, 0.0, 0.5]],
                    "nstep": 50,
                    "tolvrs": 1.0e-10,
                    "diemac": 12.0,
                    },
                scf_calculation_parameters=calculation_parameters,
                band_structure_input_variables={
                    "iscf": -2,
                    "nband": 8,
                    "tolwfr": 1e-12,
                    "diemac": 12.0,
                    "autoparal": 1,
                    },
                band_structure_calculation_parameters=calculation_parameters,
                band_structure_kpoint_path=[
                    {"L": [0.5, 0.0, 0.0]},
                    {r"$\Gamma$": [0.0, 0.0, 0.0]},
                    {"X": [0.0, 0.5, 0.5]},
                    {r"$\Gamma_2$": [1.0, 1.0, 1.0]}],
                band_structure_kpoint_path_density=20,
                plot_calculation_parameters={"show": False},
                )

    def _set_band_structure_convergence_done(self, workflow):
        # there are scf calculations and band structure calculations to copy
        seq = workflow.band_structure_convergence_sequencer
        scf_root = os.path.dirname(seq.scf_workdir)
        bs_root = os.path.dirname(seq.band_structure_workdir)
        SCF_ROOT_EXAMPLES = os.path.join(
                __TEST_WORKFLOW_ROOT__, "test_files", "abinit_si", "nc",
                "band_structure_ecut_convergence", "scf_runs")
        BS_ROOT_EXAMPLES = os.path.join(
                __TEST_WORKFLOW_ROOT__, "test_files", "abinit_si", "nc",
                "band_structure_ecut_convergence", "band_structure_runs")
        for scf_calc, bs_calc in zip(
                sorted(os.listdir(SCF_ROOT_EXAMPLES)),
                sorted(os.listdir(BS_ROOT_EXAMPLES))):
            target_scf = os.path.join(scf_root, scf_calc)
            copy_calculation(
                    os.path.join(SCF_ROOT_EXAMPLES, scf_calc),
                    target_scf,
                    new_pseudos=workflow.pseudos)
            copy_calculation(
                    os.path.join(BS_ROOT_EXAMPLES, bs_calc),
                    os.path.join(bs_root, bs_calc),
                    new_parents=[target_scf],
                    new_pseudos=workflow.pseudos)

    def _set_band_structure_for_workflow(self, *args):
        super()._set_band_structure_for_workflow(*args, fatbands=True)

    def _set_band_structure_done(self, workflow):
        super()._set_band_structure_done(
                workflow, new_pseudos=workflow.pseudos)

    def _set_gs_done(self, workflow):
        copy_calculation(
                self._gs_example, workflow.gs_sequencer.scf_workdir,
                new_pseudos=workflow.pseudos)

    def _set_gs_ecut_convergence_done(self, workflow, varname):
        # 2 sets of calculations to copy: 'ecut' and 'pawecutdg'
        # start with ECUT example
        super()._set_gs_ecut_convergence_done(
                workflow, varname, new_pseudos=workflow.pseudos)
        # reset workflow
        self._set_gs_ecut_convergence(workflow)
        seq = workflow.gs_ecut_convergence_sequencer[1]
        varname = seq.ecuts_input_variable_name
        for calcdir in os.listdir(ABINIT_GS_PAWECUT_CONVERGENCE_EXAMPLE):
            copy_calculation(
                    os.path.join(
                        ABINIT_GS_PAWECUT_CONVERGENCE_EXAMPLE, calcdir),
                    os.path.join(
                        workflow.gs_ecut_convergence_sequencer[1].scf_workdir,
                        varname, calcdir),
                    new_pseudos=workflow.pseudos)

    def _set_gs_kgrid_convergence_done(self, workflow, *args, **kwargs):
        super()._set_gs_kgrid_convergence_done(
                workflow, *args, new_pseudos=workflow.pseudos)

    def _set_gs_smearing_convergence(self, workflow, *args, **kwargs):
        # for this test we use a different material and thus different pseudo
        workflow.pseudos = ABINIT_GS_SMEARING_CONVERGENCE_PSEUDOS
        super()._set_gs_smearing_convergence(workflow, *args, **kwargs)

    def _set_gs_smearing_convergence_done(self, *args, **kwargs):
        # for this test we use a different material and thus different pseudo
        super()._set_gs_smearing_convergence_done(
                *args, new_pseudos=ABINIT_GS_SMEARING_CONVERGENCE_PSEUDOS,
                **kwargs)

    def _set_phonon_ecut_convergence(self, workflow, *args, **kwargs):
        # use the Al example
        workflow.pseudos = ABINIT_PHONON_ECUT_CONVERGENCE_PSEUDOS
        super()._set_phonon_ecut_convergence(workflow, *args, **kwargs)

    def _set_phonon_ecut_convergence_done(self, workflow, *args, **kwargs):
        super()._set_phonon_ecut_convergence_done(
                workflow, *args,
                _copy_scf_extra_kwargs={
                    "new_pseudos": ABINIT_PHONON_ECUT_CONVERGENCE_PSEUDOS},
                _copy_phonons_extra_kwargs={
                    "new_pseudos": ABINIT_PHONON_ECUT_CONVERGENCE_PSEUDOS},
                **kwargs)

    def _set_phonon_dispersion(self, *args, **kwargs):
        super()._set_phonon_dispersion(
                *args,
                compute_electric_field_response=True,
                ddk_input_variables={
                    "rfdir": [1, 1, 1],
                    "tolwfr": 1e-16,
                    "nstep": 25,
                    "diemac": 12.0,
                    },
                ddk_calculation_parameters={
                    "command": self.phonons_command_file.name,
                    "queuing_system": "local"},
                mrgddb_calculation_parameters={
                    "command": self.mrgddb_command_file.name,
                    "queuing_system": "local",
                    },
                anaddb_input_variables={
                    "ifcflag": 1, "ifcout": 0, "brav": 2,
                    "q1shft": [0.0] * 3, "nqshft": 1, "chneut": 1, "dipdip": 1,
                    "eivec": 4, "nph2l": 1, "qph2l": [1.0, 0.0, 0.0, 0.0],
                    },
                anaddb_calculation_parameters={
                    "command": self.anaddb_command_file.name,
                    "queuing_system": "local",
                    },
                **kwargs)

    def _set_phonon_dispersion_done(
            self, workflow,
            all_phonons=False, first_phonon=False, mrgddb=False,
            ddk=False,
            anaddb=False, **kwargs):
        super()._set_phonon_dispersion_done(
                workflow,
                new_pseudos=ABINIT_PHONON_DISPERSION_PSEUDOS,
                **kwargs)
        seq = workflow.phonon_dispersion_sequencer
        root = self._phonon_dispersion_example
        if first_phonon:
            copy_calculation(
                    os.path.join(
                        root, "phonons_runs", "ph_q_qgrid_generation"),
                    os.path.join(
                        os.path.dirname(seq.phonons_workdir),
                        "ph_q_qgrid_generation"),
                    new_pseudos=ABINIT_PHONON_DISPERSION_PSEUDOS)
            # also copy ddk
            self._set_phonon_dispersion_done(workflow, ddk=True)
        if ddk:
            copy_calculation(
                    os.path.join(root, "ddk_runs"),
                    seq.ddk_workdir,
                    new_pseudos=ABINIT_PHONON_DISPERSION_PSEUDOS,
                    new_parents=[seq.scf_workdir])
        if all_phonons:
            # also copy again ddk
            self._set_phonon_dispersion_done(workflow, ddk=True)
            for calcdir in os.listdir(os.path.join(root, "phonons_runs")):
                new_parents = [seq.scf_workdir]
                if calcdir == "ph_q1":
                    new_parents.append(seq.ddk_workdir)
                copy_calculation(
                    os.path.join(root, "phonons_runs", calcdir),
                    os.path.join(
                        os.path.dirname(seq.phonons_workdir), calcdir),
                    new_parents=new_parents,
                    new_pseudos=ABINIT_PHONON_DISPERSION_PSEUDOS,
                    **kwargs)
        if mrgddb:
            phroot = os.path.dirname(seq.phonons_workdir)
            copy_calculation(
                    os.path.join(root, "mrgddb_run"),
                    seq.mrgddb_workdir,
                    new_parents=[
                        os.path.join(phroot, calc)
                        for calc in os.listdir(phroot)
                        if "qgrid_gen" not in calc],
                    )
        if anaddb:
            copy_calculation(
                    os.path.join(root, "anaddb_run"),
                    seq.anaddb_workdir,
                    new_parents=[seq.mrgddb_workdir])

    def _set_phonon_smearing_convergence(self, workflow, *args, **kwargs):
        # use the Al example
        workflow.pseudos = ABINIT_PHONON_ECUT_CONVERGENCE_PSEUDOS
        super()._set_phonon_smearing_convergence(workflow, *args, **kwargs)

    def _set_phonon_smearing_convergence_done(self, workflow, *args, **kwargs):
        super()._set_phonon_smearing_convergence_done(
                workflow, *args,
                copy_scf_extra_kwargs={
                    "new_pseudos": ABINIT_PHONON_ECUT_CONVERGENCE_PSEUDOS},
                copy_phonons_extra_kwargs={
                    "new_pseudos": ABINIT_PHONON_ECUT_CONVERGENCE_PSEUDOS},
                **kwargs)

    def _set_relaxation_done(self, workflow):
        dirlist = os.listdir(ABINIT_RELAXATION_EXAMPLE)
        for icalc, calcdir in enumerate(dirlist):
            new_parents = []
            if calcdir == os.path.basename(
                    workflow.relaxation_sequencer._get_relax_cell_workdir()):
                # need to link the other calc
                basename = dirlist[0] if icalc == 1 else dirlist[1]
                new_parents = [
                        os.path.join(
                            workflow.relaxation_sequencer.scf_workdir,
                            basename)]
            copy_calculation(
                    os.path.join(ABINIT_RELAXATION_EXAMPLE, calcdir),
                    os.path.join(
                        workflow.relaxation_sequencer.scf_workdir, calcdir),
                    new_pseudos=workflow.pseudos,
                    new_parents=new_parents)
