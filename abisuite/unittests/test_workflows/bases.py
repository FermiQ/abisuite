import abc
import os
import shutil
import tempfile

from ..routines_for_tests import copy_calculation
from ...exceptions import DevError


__TEST_WORKFLOW_ROOT__ = os.path.dirname(os.path.abspath(__file__))


class BaseWorkflowTest:
    """Base class for workflow unittests. Defines common unittests and helper
    functions.
    """
    _band_structure_example = None
    _band_structure_input_variables = None
    _band_structure_kpoint_path = None
    _band_structure_kpoint_path_density = None
    _gs_ecut_convergence_criterion = None
    _gs_ecut_convergence_input_vars = None
    _gs_ecut_convergence_ecuts = None
    _gs_ecut_convergence_example = None
    _gs_ecut_convergence_other_kwargs = None
    _gs_kgrid_convergence_criterion = None
    _gs_kgrid_convergence_input_vars = None
    _gs_kgrid_convergence_kgrids = None
    _gs_kgrid_convergence_example = None
    _gs_kgrid_convergence_other_kwargs = None
    _gs_smearing_convergence_criterion = None
    _gs_smearing_convergence_input_vars = None
    _gs_smearing_convergence_kgrids = None
    _gs_smearing_convergence_smearings = None
    _gs_smearing_convergence_example = None
    _gs_smearing_convergence_other_kwargs = None
    _gs_example = None
    _gs_input_vars = None
    _phonon_ecut_convergence_criterion = None
    _phonon_ecut_convergence_scf_input_vars = None
    _phonon_ecut_convergence_phonons_input_vars = None
    _phonon_ecut_convergence_ecuts = None
    _phonon_ecut_convergence_example = None
    _phonon_ecut_convergence_phonon_qpt = None
    _phonon_ecut_convergence_other_kwargs = None
    _phonon_dispersion_example = None
    _phonon_dispersion_phonons_input_vars = None
    _phonon_dispersion_qgrid = None
    _phonon_dispersion_qpoint_path = None
    _phonon_dispersion_qpoint_path_density = None
    _phonon_smearing_convergence_example = None
    _phonon_smearing_convergence_scf_input_vars = None
    _phonon_smearing_convergence_phonons_input_vars = None
    _phonon_smearing_convergence_kgrids = None
    _phonon_smearing_convergence_phonon_qpt = None
    _phonon_smearing_convergence_smearings = None
    _phonon_smearing_convergence_criterion = None
    _phonon_smearing_convergence_other_kwargs = {}
    _relaxation_input_vars = None
    _relaxation_other_kwargs = None
    _scf_script_name = None
    _phonons_script_name = None
    _set_gs_calculation_kwargs = None
    _workflow_cls = None
    _workflow_init_kwargs = None

    def setUp(self):
        # GENERAL PART
        if self._workflow_cls is None:
            raise DevError("Need to set '_workflow_cls'.")
        if self._workflow_init_kwargs is None:
            self._workflow_init_kwargs = {}
        if self._scf_script_name is None:
            raise DevError("Need to set '_scf_script_name'.")
        if self._phonons_script_name is None:
            raise DevError("Need to set '_phonons_script_name'.")
        # GS PART
        if self._gs_example is None:
            raise DevError("Need to set '_gs_example'.")
        if self._gs_input_vars is None:
            raise DevError("Need to set '_gs_input_vars'.")
        if self._set_gs_calculation_kwargs is None:
            self._set_gs_calculation_kwargs = {}
        # GS ECUT CONVERGENCE PART
        if self._gs_ecut_convergence_criterion is None:
            raise DevError("Need to set '_gs_ecut_convergence_criterion'.")
        if self._gs_ecut_convergence_input_vars is None:
            raise DevError("Need to set '_gs_ecut_convergence_input_vars'.")
        if self._gs_ecut_convergence_ecuts is None:
            raise DevError("Need to set '_gs_ecut_convergence_ecuts'.")
        if self._gs_ecut_convergence_example is None:
            raise DevError("Need to set '_gs_ecut_convergence_example'.")
        if self._gs_ecut_convergence_other_kwargs is None:
            self._gs_ecut_convergence_other_kwargs = {}
        # GS KGRID CONVERGENCE PART
        if self._gs_kgrid_convergence_criterion is None:
            raise DevError("Need to set '_gs_kgrid_convergence_criterion'.")
        if self._gs_kgrid_convergence_input_vars is None:
            raise DevError("Need to set '_gs_kgrid_convergence_input_vars'.")
        if self._gs_kgrid_convergence_kgrids is None:
            raise DevError("Need to set '_gs_kgrid_convergence_kgrids'.")
        if self._gs_kgrid_convergence_example is None:
            raise DevError("Need to set '_gs_kgrid_convergence_example'.")
        if self._gs_kgrid_convergence_other_kwargs is None:
            self._gs_kgrid_convergence_other_kwargs = {}
        # GS SMEARING CONVERGENCE PART
        if self._gs_smearing_convergence_criterion is None:
            raise DevError("Need to set '_gs_smearing_convergence_criterion'.")
        if self._gs_smearing_convergence_input_vars is None:
            raise DevError(
                    "Need to set '_gs_smearing_convergence_input_vars'.")
        if self._gs_smearing_convergence_kgrids is None:
            raise DevError("Need to set '_gs_smearing_convergence_kgrids'.")
        if self._gs_smearing_convergence_example is None:
            raise DevError("Need to set '_gs_smearing_convergence_example'.")
        if self._gs_smearing_convergence_smearings is None:
            raise DevError("Need to set '_gs_smearing_convergence_smearings'.")
        if self._gs_smearing_convergence_other_kwargs is None:
            self._gs_smearing_convergence_other_kwargs = {}
        # RELAXATION PART
        if self._relaxation_other_kwargs is None:
            self._relaxation_other_kwargs = {}
        if self._relaxation_input_vars is None:
            raise DevError("Need to set '_relaxation_input_vars'.")
        # PHONON ECUT CONVERGENCE PART
        if self._phonon_ecut_convergence_criterion is None:
            raise DevError(
                    "Need to set '_phonon_ecut_convergence_criterion'.")
        if self._phonon_ecut_convergence_scf_input_vars is None:
            raise DevError(
                    "Need to set '_phonon_ecut_convergence_scf_input_vars'.")
        if self._phonon_ecut_convergence_phonons_input_vars is None:
            raise DevError(
                    "Need to set "
                    "'_phonon_ecut_convergence_phonons_input_vars'.")
        if self._phonon_ecut_convergence_ecuts is None:
            raise DevError(
                    "Need to set '_phonon_ecut_convergence_ecuts'.")
        if self._phonon_ecut_convergence_example is None:
            raise DevError(
                    "Need to set '_phonon_ecut_convergence_example'.")
        if self._phonon_ecut_convergence_phonon_qpt is None:
            raise DevError(
                    "Need to set '_phonon_ecut_convergence_phonon_qpt'.")
        if self._phonon_ecut_convergence_other_kwargs is None:
            self._phonon_ecut_convergence_other_kwargs = {}
        # PHONON DISPERSION PART
        if self._phonon_dispersion_example is None:
            raise DevError("Need to set ')phonon_dispersion_example'.")
        if self._phonon_dispersion_phonons_input_vars is None:
            raise DevError(
                    "Need to set '_phonon_dispersion_phonons_input_vars'.")
        if self._phonon_dispersion_qgrid is None:
            raise DevError(
                    "Need to set '_phonon_dispersion_qgrid'.")
        if self._phonon_dispersion_qpoint_path is None:
            raise DevError("Need to set '_phonon_dispersion_qpoint_path'.")
        if self._phonon_dispersion_qpoint_path_density is None:
            raise DevError(
                    "Need to set '_phonon_dispersion_qpoint_path_density'.")
        # PHONON SMEARING CONVERGENCE PART
        if self._phonon_smearing_convergence_criterion is None:
            raise DevError(
                    "Need to set '_phonon_smearing_convergence_criterion'.")
        if self._phonon_smearing_convergence_scf_input_vars is None:
            raise DevError(
                    "Need to set "
                    "'_phonon_smearing_convergence_scf_input_vars'.")
        if self._phonon_smearing_convergence_phonons_input_vars is None:
            raise DevError(
                    "Need to set "
                    "'_phonon_smearing_convergence_phonons_input_vars'.")
        if self._phonon_smearing_convergence_smearings is None:
            raise DevError(
                    "Need to set '_phonon_smearing_convergence_smearings'.")
        if self._phonon_smearing_convergence_kgrids is None:
            raise DevError(
                    "Need to set '_phonon_smearing_convergence_kgrids'.")
        if self._phonon_smearing_convergence_example is None:
            raise DevError(
                    "Need to set '_phonon_smearing_convergence_example'.")
        if self._phonon_smearing_convergence_phonon_qpt is None:
            raise DevError(
                    "Need to set '_phonon_smearing_convergence_phonon_qpt'.")
        if self._phonon_smearing_convergence_other_kwargs is None:
            self._phonon_smearing_convergence_other_kwargs = {}
        # BAND STRUCTURE PART
        if self._band_structure_example is None:
            raise DevError("Need to set '_band_structure_example'.")
        if self._band_structure_kpoint_path is None:
            raise DevError("Need to set '_band_structure_kpoint_path'.")
        if self._band_structure_kpoint_path_density is None:
            raise DevError(
                    "Need to set '_band_structure_kpoint_path_density'.")
        # setup temporary directory in order to setup calculations from
        self.workflow_dir = tempfile.TemporaryDirectory()
        # also create a fake executable for the software
        self.scf_command_file = tempfile.NamedTemporaryFile(
                suffix=self._scf_script_name)
        self.phonons_command_file = tempfile.NamedTemporaryFile(
                suffix=self._phonons_script_name)

    def tearDown(self):
        if os.path.exists(self.workflow_dir.name):
            self.workflow_dir.cleanup()

    def test_band_structure(self):
        workflow = self._workflow_cls(
                gs=True,
                band_structure=True,
                **self._workflow_init_kwargs)
        self._set_gs(workflow)
        self._set_gs_done(workflow)
        self._set_band_structure_for_workflow(workflow)
        workflow.write()
        self.assertTrue(
                os.path.isdir(
                    workflow.band_structure_sequencer.band_structure_workdir))
        shutil.rmtree(workflow.band_structure_sequencer.band_structure_workdir)
        self._set_band_structure_done(workflow)
        workflow.run()
        # make sure band structure plots were written
        res_dir = os.path.join(
                os.path.dirname(
                    workflow.band_structure_sequencer.band_structure_workdir),
                "results")
        # many files created since this is a fatband run
        self.assertGreaterEqual(len(os.listdir(res_dir)), 2)

    def test_gs_ecut_convergence(self):
        workflow = self._workflow_cls(
                gs_ecut_convergence=True,
                **self._workflow_init_kwargs)
        self._set_gs_ecut_convergence(workflow)
        workflow.write()
        # make sure scf calcs were written
        ncalcs = len(self._gs_ecut_convergence_ecuts)
        # find the varname
        seq = workflow.gs_ecut_convergence_sequencer
        varname = seq.ecuts_input_variable_name
        dir_ = os.path.join(
                    self.workflow_dir.name,
                    "gs_ecut_convergence", "scf_runs", varname)
        self.assertEqual(len(os.listdir(dir_)), ncalcs)
        # set done and check workflow completed
        shutil.rmtree(dir_)
        self._set_gs_ecut_convergence_done(workflow, varname)
        workflow.run()
        # make sure there are >= 2 files in results dir
        results_dir = os.path.join(
                os.path.dirname(os.path.dirname(dir_)), "results")
        self.assertGreaterEqual(len(os.listdir(results_dir)), 2)
        self.assertTrue(workflow.workflow_completed)

    def test_gs_kgrid_convergence(self):
        workflow = self._workflow_cls(
                gs_kgrid_convergence=True,
                **self._workflow_init_kwargs)
        self._set_gs_kgrid_convergence(workflow)
        workflow.write()
        # make sure scf calcs were written
        ncalcs = len(self._gs_kgrid_convergence_kgrids)
        seq = workflow.gs_kgrid_convergence_sequencer
        varname = seq._parameter_to_converge_input_variable_name
        dirs = [x for x in os.listdir(
            os.path.join(seq.scf_workdir, varname)) if "results" not in x]
        self.assertEqual(len(dirs), ncalcs)
        shutil.rmtree(
                workflow.gs_kgrid_convergence_sequencer.scf_workdir)
        self._set_gs_kgrid_convergence_done(workflow)
        workflow.run()
        results_dir = os.path.join(
                os.path.dirname(
                    workflow.gs_kgrid_convergence_sequencer.scf_workdir),
                "results")
        self.assertEqual(len(os.listdir(results_dir)), 2)
        self.assertTrue(workflow.workflow_completed)

    def test_gs_smearing_convergence(self):
        workflow = self._workflow_cls(
                gs_smearing_convergence=True,
                **self._workflow_init_kwargs)
        self._set_gs_smearing_convergence(workflow)
        workflow.write()
        ncalcs = (len(self._gs_smearing_convergence_kgrids) *
                  len(self._gs_smearing_convergence_smearings))
        dirs = os.listdir(
                workflow.gs_smearing_convergence_sequencer.scf_workdir)
        nwritten = 0
        for dir_ in dirs:
            nwritten += len(os.listdir(
                os.path.join(
                    workflow.gs_smearing_convergence_sequencer.scf_workdir,
                    dir_)))
        self.assertEqual(ncalcs, nwritten)
        shutil.rmtree(
                workflow.gs_smearing_convergence_sequencer.scf_workdir)
        self._set_gs_smearing_convergence_done(workflow)
        workflow.run()
        results_dir = os.path.join(
                workflow.gs_smearing_convergence_sequencer.scf_workdir,
                "results")
        self.assertEqual(len(os.listdir(results_dir)), 6)
        self.assertTrue(workflow.workflow_completed)

    def test_gs(self):
        workflow = self._workflow_cls(gs=True, **self._workflow_init_kwargs)
        self._set_gs(workflow)
        workflow.write()
        # make sure gs run is written
        self.assertTrue(os.path.isdir(workflow.gs_sequencer.scf_workdir))
        # check that workflow is not completed
        self.assertFalse(workflow.workflow_completed)
        # delete what was writte and copy example calculation
        shutil.rmtree(workflow.gs_sequencer.scf_workdir)
        self._set_gs_done(workflow)
        # check that the workflow is now completed
        self.assertTrue(workflow.workflow_completed)

    def test_phonon_ecut_convergence(self):
        workflow = self._workflow_cls(
                phonon_ecut_convergence=True,
                **self._workflow_init_kwargs)
        self._set_phonon_ecut_convergence(workflow)
        workflow.write()
        # make sure scf calcs were written
        ncalcs = len(self._phonon_ecut_convergence_ecuts)
        dir_ = os.path.join(
                    self.workflow_dir.name,
                    "phonon_ecut_convergence")
        seq = workflow.phonon_ecut_convergence_sequencer
        varname = seq.ecuts_input_variable_name
        self.assertEqual(len(os.listdir(
            os.path.join(dir_, "scf_runs", varname))), ncalcs)
        # set scf_runs done
        shutil.rmtree(dir_)
        self._set_phonon_ecut_convergence_done(workflow, scf=True)
        workflow.write()
        # check phonon calcs have been written
        self.assertEqual(
                len(os.listdir(os.path.join(seq.phonons_workdir, varname))),
                ncalcs)
        shutil.rmtree(dir_)
        self._set_phonon_ecut_convergence_done(
                workflow, scf=True, phonons=True)
        workflow.run()
        # make sure there are >= 2 files in results dir
        results_dir = os.path.join(dir_, "results")
        self.assertEqual(len(os.listdir(results_dir)), 2)
        self.assertTrue(workflow.workflow_completed)

    def test_phonon_dispersion(self):
        workflow = self._workflow_cls(
                gs=True,
                phonon_dispersion=True,
                **self._workflow_init_kwargs)
        self._set_phonon_dispersion(workflow)
        workflow.write()
        # make sure the first phonon calc was written
        dir_ = os.path.join(
                    self.workflow_dir.name,
                    "phonon_dispersion")
        self.assertEqual(
                len(os.listdir(
                    os.path.join(dir_, "phonons_runs"))), 1)
        shutil.rmtree(dir_)
        self._set_phonon_dispersion_done(
                workflow, first_phonon=True)
        workflow.write()
        # check all phonon calcs have been written
        seq = workflow.phonon_dispersion_sequencer
        all_phs = os.listdir(os.path.dirname(
            workflow.phonon_dispersion_sequencer.phonons_workdir))
        all_phs = [x for x in all_phs if "qgrid_gen" not in x]
        self.assertEqual(len(all_phs), seq.nphonons)
        shutil.rmtree(dir_)
        self._set_phonon_dispersion_done(
                workflow, all_phonons=True)
        workflow.write()
        # make sure post phonons calcs have been written
        self._check_phonon_dispersion_post_phonons(workflow)
        workflow.run()
        # make sure there are >= 2 files in results dir
        results_dir = os.path.join(dir_, "results")
        self.assertEqual(len(os.listdir(results_dir)), 2)
        self.assertTrue(workflow.workflow_completed)

    def test_phonon_smearing_convergence(self):
        workflow = self._workflow_cls(
                phonon_smearing_convergence=True,
                **self._workflow_init_kwargs)
        self._set_phonon_smearing_convergence(workflow)

        workflow.write()
        # make sure scf calcs were written
        ncalcs = (len(self._phonon_smearing_convergence_kgrids) *
                  len(self._phonon_smearing_convergence_smearings))
        dir_ = os.path.join(
                    self.workflow_dir.name,
                    "phonon_smearing_convergence", "scf_runs")
        self.assertEqual(
                len(os.listdir(dir_)),
                len(self._phonon_smearing_convergence_smearings))
        n_written = 0
        for subdir in os.listdir(dir_):
            n_written += len(os.listdir(os.path.join(dir_, subdir)))
        self.assertEqual(n_written, ncalcs)
        # set scf_runs done
        shutil.rmtree(dir_)
        self._set_phonon_smearing_convergence_done(workflow, scf=True)
        workflow.write()
        # check phonon calcs have been written
        dir_ = os.path.join(
                    self.workflow_dir.name,
                    "phonon_smearing_convergence", "phonons_runs")
        self.assertEqual(
                len(os.listdir(dir_)),
                len(self._phonon_smearing_convergence_smearings))
        n_written = 0
        for subdir in os.listdir(dir_):
            n_written += len(os.listdir(os.path.join(dir_, subdir)))
        self.assertEqual(n_written, ncalcs)
        shutil.rmtree(dir_)
        self._set_phonon_smearing_convergence_done(
                workflow, phonons=True)
        workflow.run()
        # make sure there are >= 2 files in results dir
        results_dir = os.path.join(
                self.workflow_dir.name, "phonon_smearing_convergence",
                "results")
        self.assertGreaterEqual(len(os.listdir(results_dir)), 2)
        self.assertTrue(workflow.workflow_completed)

    def test_relaxation_only(self):
        workflow = self._workflow_cls(
                relaxation=True,
                **self._workflow_init_kwargs)
        self._set_relaxation(workflow)
        workflow.write()
        # check that first calc has been written
        self.assertEqual(
                len(os.listdir(workflow.relaxation_sequencer.scf_workdir)), 1)
        shutil.rmtree(workflow.relaxation_sequencer.scf_workdir)
        self._set_relaxation_done(workflow)
        workflow.run()
        self.assertTrue(workflow.workflow_completed)

    def _set_band_structure_for_workflow(self, workflow, **kwargs):
        calculation_parameters = {
                "command": self.scf_command_file.name,
                "queuing_system": "local",
                }
        workflow.set_band_structure(
                root_workdir=os.path.join(
                    self.workflow_dir.name, "band_structure"),
                band_structure_input_variables=(
                    self._band_structure_input_variables),
                band_structure_calculation_parameters=calculation_parameters,
                band_structure_kpoint_path=self._band_structure_kpoint_path,
                band_structure_kpoint_path_density=(
                    self._band_structure_kpoint_path_density),
                plot_calculation_parameters={"show": False},
                use_gs=True,
                **kwargs
                )

    def _set_band_structure_done(self, workflow, **kwargs):
        copy_calculation(
                self._band_structure_example,
                workflow.band_structure_sequencer.band_structure_workdir,
                new_parents=[workflow.band_structure_sequencer.scf_workdir],
                **kwargs)

    def _set_gs_ecut_convergence(self, workflow):
        workflow.set_gs_ecut_convergence(
            root_workdir=os.path.join(
                    self.workflow_dir.name,
                    "gs_ecut_convergence"),
            scf_input_variables=self._gs_ecut_convergence_input_vars,
            scf_calculation_parameters={"command": self.scf_command_file.name,
                                        "queuing_system": "local"},
            scf_ecuts=self._gs_ecut_convergence_ecuts,
            scf_convergence_criterion=self._gs_ecut_convergence_criterion,
            plot_calculation_parameters={"show": False},
            **self._gs_ecut_convergence_other_kwargs)

    def _set_gs_ecut_convergence_done(self, workflow, varname, **kwargs):
        seq = workflow.gs_ecut_convergence_sequencer
        varname = seq.ecuts_input_variable_name
        for calcdir in os.listdir(
                os.path.join(
                    self._gs_ecut_convergence_example, varname)):
            copy_calculation(
                    os.path.join(
                        self._gs_ecut_convergence_example, varname, calcdir),
                    os.path.join(
                        seq.scf_workdir, varname, calcdir), **kwargs)

    def _set_gs_kgrid_convergence(self, workflow):
        workflow.set_gs_kgrid_convergence(
            root_workdir=os.path.join(
                    self.workflow_dir.name,
                    "gs_kgrid_convergence"),
            scf_input_variables=self._gs_kgrid_convergence_input_vars,
            scf_calculation_parameters={"command": self.scf_command_file.name,
                                        "queuing_system": "local"},
            scf_kgrids=self._gs_kgrid_convergence_kgrids,
            scf_convergence_criterion=self._gs_kgrid_convergence_criterion,
            plot_calculation_parameters={"show": False},
            **self._gs_kgrid_convergence_other_kwargs)

    def _set_gs_kgrid_convergence_done(self, workflow, **kwargs):
        dir_ = os.path.join(
                    self.workflow_dir.name,
                    "gs_kgrid_convergence", "scf_runs")
        seq = workflow.gs_kgrid_convergence_sequencer
        varname = seq.kgrids_input_variable_name
        for calcdir in os.listdir(
                os.path.join(self._gs_kgrid_convergence_example, varname)):
            if "results" in calcdir:
                continue
            copy_calculation(
                    os.path.join(
                        self._gs_kgrid_convergence_example, varname, calcdir),
                    os.path.join(dir_, varname, calcdir), **kwargs)

    def _set_gs_smearing_convergence(self, workflow):
        workflow.set_gs_smearing_convergence(
                root_workdir=os.path.join(
                    self.workflow_dir.name,
                    "gs_smearing_convergence"),
                scf_input_variables=self._gs_smearing_convergence_input_vars,
                scf_calculation_parameters={
                    "command": self.scf_command_file.name,
                    "queuing_system": "local",
                    },
                scf_kgrids=self._gs_smearing_convergence_kgrids,
                scf_smearings=self._gs_smearing_convergence_smearings,
                scf_convergence_criterion=(
                    self._gs_smearing_convergence_criterion),
                plot_calculation_parameters={"show": False},
                **self._gs_smearing_convergence_other_kwargs,
                )

    def _set_gs_smearing_convergence_done(self, workflow, **kwargs):
        dir_ = os.path.join(
                    self.workflow_dir.name,
                    "gs_smearing_convergence")
        for subdir in os.listdir(self._gs_smearing_convergence_example):
            if "results" in subdir:
                continue
            for calcdir in os.listdir(
                    os.path.join(self._gs_smearing_convergence_example,
                                 subdir)):
                copy_calculation(
                    os.path.join(
                        self._gs_smearing_convergence_example,
                        subdir, calcdir),
                    os.path.join(dir_, subdir, calcdir),
                    **kwargs)

    def _set_gs_done(self, workflow, **kwargs):
        copy_calculation(
                self._gs_example, workflow.gs_sequencer.scf_workdir,
                **kwargs)

    def _set_gs(self, workflow):
        calculation_parameters = {"command": self.scf_command_file.name,
                                  "queuing_system": "local"}
        calculation_parameters.update(self._set_gs_calculation_kwargs)
        workflow.set_gs(
                scf_workdir=os.path.join(self.workflow_dir.name, "gs_run"),
                scf_input_variables=self._gs_input_vars.copy(),
                scf_calculation_parameters=calculation_parameters)

    def _set_phonon_ecut_convergence(self, workflow, **kwargs):
        workflow.set_phonon_ecut_convergence(
            root_workdir=os.path.join(
                    self.workflow_dir.name,
                    "phonon_ecut_convergence"),
            scf_input_variables=self._phonon_ecut_convergence_scf_input_vars,
            scf_calculation_parameters={"command": self.scf_command_file.name,
                                        "queuing_system": "local"},
            scf_ecuts=self._phonon_ecut_convergence_ecuts,
            phonons_qpt=self._phonon_ecut_convergence_phonon_qpt,
            phonons_input_variables=(
                self._phonon_ecut_convergence_phonons_input_vars),
            phonons_calculation_parameters={
                "command": self.phonons_command_file.name,
                "queuing_system": "local"},
            phonons_convergence_criterion=(
                self._phonon_ecut_convergence_criterion),
            plot_calculation_parameters={"show": False},
            **self._phonon_ecut_convergence_other_kwargs)

    def _set_phonon_ecut_convergence_done(
            self, workflow, scf=False, phonons=False,
            _copy_phonons_extra_kwargs=None,
            _copy_scf_extra_kwargs=None):
        if _copy_phonons_extra_kwargs is None:
            _copy_phonons_extra_kwargs = {}
        if _copy_scf_extra_kwargs is None:
            _copy_scf_extra_kwargs = {}
        seq = workflow.phonon_ecut_convergence_sequencer
        varname = seq.ecuts_input_variable_name
        if scf:
            root = os.path.join(
                    self._phonon_ecut_convergence_example,
                    "scf_runs", varname)
            for calcdir in os.listdir(root):
                copy_calculation(
                    os.path.join(root, calcdir),
                    os.path.join(seq.scf_workdir, varname, calcdir),
                    **_copy_scf_extra_kwargs
                    )
        if phonons:
            root = os.path.join(
                    self._phonon_ecut_convergence_example,
                    "phonons_runs", varname)
            for calcdir in os.listdir(root):
                copy_calculation(
                    os.path.join(root, calcdir),
                    os.path.join(seq.phonons_workdir, varname, calcdir),
                    new_parents=[
                        os.path.join(seq.scf_workdir, varname, calcdir)],
                    **_copy_phonons_extra_kwargs
                    )

    def _set_phonon_dispersion(self, workflow, **kwargs):
        self._set_gs(workflow)
        self._set_gs_done(workflow)
        workflow.set_phonon_dispersion(
            root_workdir=os.path.join(
                    self.workflow_dir.name,
                    "phonon_dispersion"),
            phonons_input_variables=self._phonon_dispersion_phonons_input_vars,
            phonons_qpoint_grid=self._phonon_dispersion_qgrid,
            phonons_calculation_parameters={
                "command": self.phonons_command_file.name,
                "queuing_system": "local"},
            plot_calculation_parameters={"show": False},
            qpoint_path=self._phonon_dispersion_qpoint_path,
            qpoint_path_density=self._phonon_dispersion_qpoint_path_density,
            use_gs=True,
            **kwargs
            )

    def _set_phonon_dispersion_done(
            self, workflow, first_phonon=False, all_phonons=False,
            **kwargs):
        seq = workflow.phonon_dispersion_sequencer
        root = self._phonon_dispersion_example
        if all_phonons:
            for calcdir in os.listdir(os.path.join(root, "phonons_runs")):
                copy_calculation(
                    os.path.join(root, "phonons_runs", calcdir),
                    os.path.join(
                        os.path.dirname(seq.phonons_workdir), calcdir),
                    new_parents=[seq.scf_workdir],
                    **kwargs)

    def _set_phonon_smearing_convergence(self, workflow):
        workflow.set_phonon_smearing_convergence(
            root_workdir=os.path.join(
                    self.workflow_dir.name,
                    "phonon_smearing_convergence"),
            scf_input_variables=(
                self._phonon_smearing_convergence_scf_input_vars),
            scf_calculation_parameters={"command": self.scf_command_file.name,
                                        "queuing_system": "local"},
            scf_smearings=self._phonon_smearing_convergence_smearings,
            scf_kgrids=self._phonon_smearing_convergence_kgrids,
            phonons_input_variables=(
                self._phonon_smearing_convergence_phonons_input_vars),
            phonons_qpt=self._phonon_smearing_convergence_phonon_qpt,
            phonons_calculation_parameters={
                "command": self.phonons_command_file.name,
                "queuing_system": "local"},
            phonons_convergence_criterion=(
                self._phonon_smearing_convergence_criterion),
            plot_calculation_parameters={"show": False},
            **self._phonon_smearing_convergence_other_kwargs)

    def _set_phonon_smearing_convergence_done(
            self, workflow, scf=False, phonons=False,
            copy_phonons_extra_kwargs=None,
            copy_scf_extra_kwargs=None):
        if copy_phonons_extra_kwargs is None:
            copy_phonons_extra_kwargs = {}
        if copy_scf_extra_kwargs is None:
            copy_scf_extra_kwargs = {}
        seq = workflow.phonon_smearing_convergence_sequencer
        if scf:
            root = os.path.join(
                    self._phonon_smearing_convergence_example,
                    "scf_runs")
            for subdir in os.listdir(root):
                for calcdir in os.listdir(os.path.join(root, subdir)):
                    copy_calculation(
                        os.path.join(root, subdir, calcdir),
                        os.path.join(seq.scf_workdir, subdir, calcdir),
                        **copy_scf_extra_kwargs)
        if phonons:
            root = os.path.join(
                    self._phonon_smearing_convergence_example,
                    "phonons_runs")
            for subdir in os.listdir(root):
                for calcdir in os.listdir(os.path.join(root, subdir)):
                    copy_calculation(
                        os.path.join(root, subdir, calcdir),
                        os.path.join(seq.phonons_workdir, subdir, calcdir),
                        new_parents=[
                            os.path.join(seq.scf_workdir, subdir, calcdir)],
                        **copy_phonons_extra_kwargs
                        )

    def _set_relaxation(
            self, workflow, relax_cell=True, relax_atoms=True):
        workflow.set_relaxation(
            root_workdir=os.path.join(
                    self.workflow_dir.name, "relax_run"),
            scf_input_variables=self._relaxation_input_vars,
            relax_atoms=relax_atoms,
            relax_cell=relax_cell,
            scf_calculation_parameters={"command": self.scf_command_file.name,
                                        "queuing_system": "local"},
            **self._relaxation_other_kwargs,
            )

    @abc.abstractmethod
    def _set_relaxation_done(self, *args, **kwargs):
        pass
