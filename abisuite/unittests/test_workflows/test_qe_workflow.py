import os
import pytest
import shutil
import tempfile

from abisuite import QEWorkflow
from .bases import BaseWorkflowTest, __TEST_WORKFLOW_ROOT__
from ..bases import TestCase
from ..routines_for_tests import copy_calculation
from ...handlers import CalculationDirectory


# QEWorkflow unittest with Pb
# GENERAL
__PSEUDO_DIR__ = os.path.join(
        __TEST_WORKFLOW_ROOT__, "test_files", "pseudos")
# GS ECUT CONVERGENCE #########################################################
QE_GS_ECUT_CONVERGENCE_INPUT_VARS = {
        "calculation": "scf",
        "ibrav": 2,
        "ntyp": 1,
        "nat": 1,
        "celldm(1)": 9.2225583816,
        "pseudo_dir": __PSEUDO_DIR__,
        "occupations": "smearing",
        "degauss": 0.1,
        "conv_thr": 1.0e-8,
        "atomic_species": [
            {"atom": "Pb",
             "atomic_mass": 207.2,
             "pseudo": "pb_s.UPF",
             }],
        "k_points": {"parameter": "automatic",
                     "k_points": [8, 8, 8, 1, 1, 1],
                     },
        "atomic_positions": {"parameter": "crystal",
                             "positions": {"Pb": [0.0, 0.0, 0.0]}},
        }
QE_GS_ECUT_CONVERGENCE_ECUTS = [10, 20, 30]
QE_GS_ECUT_CONVERGENCE_EXAMPLE = os.path.join(
        __TEST_WORKFLOW_ROOT__, "test_files", "qe_Pb", "gs_ecut_convergence",
        "ecutwfc")
QE_GS_ECUT_CONVERGENCE_CRITERION = 100

# ######################### GS KGRID CONVERGENCE PART #########################

QE_GS_KGRID_CONVERGENCE_INPUT_VARS = {
         "calculation": "scf",
         "ibrav": 2,
         "ntyp": 1,
         "nat": 1,
         "celldm(1)": 9.2225583816,
         "pseudo_dir": __PSEUDO_DIR__,
         "occupations": "smearing",
         "degauss": 0.1,
         "conv_thr": 1.0e-8,
         "ecutwfc": 50,
         "atomic_species": [
             {"atom": "Pb",
              "atomic_mass": 207.2,
              "pseudo": "pb_s.UPF",
              }],
         "k_points": {"parameter": "automatic",
                      "k_points": [8, 8, 8, 1, 1, 1],
                      },
         "atomic_positions": {"parameter": "crystal",
                              "positions": {"Pb": [0.0, 0.0, 0.0]}},
         }
QE_GS_KGRID_CONVERGENCE_KGRIDS = [[x, x, x, 1, 1, 1] for x in [2, 4, 8]]
QE_GS_KGRID_CONVERGENCE_CRITERION = 10
QE_GS_KGRID_CONVERGENCE_EXAMPLE = os.path.join(
        __TEST_WORKFLOW_ROOT__, "test_files", "qe_Pb", "gs_kgrid_convergence")

# ######################## GS SMEARING CONVERGENCE PART #######################
QE_GS_SMEARING_CONVERGENCE_INPUT_VARS = {
        "calculation": "scf",
        "ibrav": 2,
        "ntyp": 1,
        "nat": 1,
        "celldm(1)": 9.2225583816,
        "pseudo_dir": __PSEUDO_DIR__,
        "occupations": "smearing",
        "conv_thr": 1.0e-8,
        "ecutwfc": 50.0,
        "atomic_species": [
            {"atom": "Pb",
             "atomic_mass": 207.2,
             "pseudo": "pb_s.UPF",
             }],
        "atomic_positions": {"parameter": "crystal",
                             "positions": {"Pb": [0.0, 0.0, 0.0]}},
        }
QE_GS_SMEARING_CONVERGENCE_KGRIDS = [[x, x, x, 1, 1, 1] for x in [2, 4, 8]]
QE_GS_SMEARING_CONVERGENCE_SMEARINGS = [0.1, 0.01]
QE_GS_SMEARING_CONVERGENCE_CRITERION = 10
QE_GS_SMEARING_CONVERGENCE_EXAMPLE = os.path.join(
        __TEST_WORKFLOW_ROOT__, "test_files", "qe_Pb",
        "gs_smearing_convergence")

# ######################### PHONON ECUT CONVERGENCE PART ######################
QE_PHONON_ECUT_CONVERGENCE_SCF_INPUT_VARS = {
        "calculation": "scf",
        "ibrav": 2,
        "ntyp": 1,
        "nat": 1,
        "celldm(1)": 9.13469970266919,
        "pseudo_dir": __PSEUDO_DIR__,
        "occupations": "smearing",
        "degauss": 0.1,
        "conv_thr": 1.0e-8,
        "atomic_species": [
            {"atom": "Pb",
             "atomic_mass": 207.2,
             "pseudo": "pb_s.UPF",
             }],
        "k_points": {"parameter": "automatic",
                     "k_points": [4, 4, 4, 1, 1, 1],
                     },
        "atomic_positions": {"parameter": "crystal",
                             "positions": {"Pb": [0.0, 0.0, 0.0]}},
        }
QE_PHONON_ECUT_CONVERGENCE_PHONONS_INPUT_VARS = {}
QE_PHONON_ECUT_CONVERGENCE_ECUTS = [10, 20, 30]
QE_PHONON_ECUT_CONVERGENCE_PHONON_QPT = [0.0, 0.0, 0.0]
QE_PHONON_ECUT_CONVERGENCE_EXAMPLE = os.path.join(
        __TEST_WORKFLOW_ROOT__, "test_files", "qe_Pb",
        "phonon_ecut_convergence")

# ########################### PHONON SMEARING CONVERGENCE PART ################
QE_PHONON_SMEARING_CONVERGENCE_INPUT_VARS = {
        "calculation": "scf",
        "ibrav": 2,
        "ntyp": 1,
        "nat": 1,
        "celldm(1)": 9.13469970266919,
        "pseudo_dir": __PSEUDO_DIR__,
        "occupations": "smearing",
        "conv_thr": 1.0e-8,
        "ecutwfc": 20,
        "atomic_species": [
            {"atom": "Pb",
             "atomic_mass": 207.2,
             "pseudo": "pb_s.UPF",
             }],
        "k_points": {"parameter": "automatic",
                     "k_points": [10, 10, 10, 1, 1, 1],
                     },
        "atomic_positions": {"parameter": "crystal",
                             "positions": {"Pb": [0.0, 0.0, 0.0]}},
        }
QE_PHONON_SMEARING_CONVERGENCE_PHONON_QPT = [0.0, 0.0, 0.0]
QE_PHONON_SMEARING_CONVERGENCE_SMEARINGS = [0.1, 0.01]
QE_PHONON_SMEARING_CONVERGENCE_KGRIDS = [[x, x, x, 1, 1, 1] for x in [2, 4, 8]]
QE_PHONON_SMEARING_CONVERGENCE_EXAMPLE = os.path.join(
        __TEST_WORKFLOW_ROOT__, "test_files", "qe_Pb",
        "phonon_smearing_convergence")


# ################################ GS PART ####################################
QE_GS_INPUT_VARS = {
        "calculation": "scf",
        "ibrav": 2,
        "ntyp": 1,
        "nat": 1,
        "celldm(1)": 9.134699723318054,
        "ecutwfc": 50,
        "pseudo_dir": __PSEUDO_DIR__,
        "occupations": "smearing",
        "degauss": 0.1,
        "conv_thr": 1.0e-8,
        "atomic_species": [
            {"atom": "Pb",
             "atomic_mass": 207.2,
             "pseudo": "pb_s.UPF",
             }],
        "k_points": {"parameter": "automatic",
                     "k_points": [8, 8, 8, 1, 1, 1],
                     },
        "atomic_positions": {"parameter": "crystal",
                             "positions": {"Pb": [0.0, 0.0, 0.0]}},
        }
QE_GS_EXAMPLE = os.path.join(
        __TEST_WORKFLOW_ROOT__, "test_files", "qe_Pb", "gs_run")


# ######################### RELAXATION PART ###################################
QE_RELAXATION_INPUT_VARIABLES = {
        "calculation": "scf",
        "ibrav": 2,
        "ntyp": 1,
        "nat": 1,
        "celldm(1)": 9.2225583816,
        "ecutwfc": 50,
        "pseudo_dir": __PSEUDO_DIR__,
        "occupations": "smearing",
        "press_conv_thr": 0.001,
        "forc_conv_thr": 1e-6,
        "etot_conv_thr": 1e-6,
        "degauss": 0.1,
        "conv_thr": 1.0e-8,
        "atomic_species": [
            {"atom": "Pb",
             "atomic_mass": 207.2,
             "pseudo": "pb_s.UPF",
             }],
        "k_points": {"parameter": "automatic",
                     "k_points": [8, 8, 8, 1, 1, 1],
                     },
        "atomic_positions": {"parameter": "crystal",
                             "positions": {"Pb": [0.0, 0.0, 0.0]}},
        }
QE_RELAXATION_EXAMPLE = os.path.join(
        __TEST_WORKFLOW_ROOT__, "test_files", "qe_Pb", "relax_run")


# ########################### BAND STRUCTURE PART #############################
QE_BAND_STRUCTURE_EXAMPLE = os.path.join(
        __TEST_WORKFLOW_ROOT__, "test_files", "qe_Pb", "band_structure",
        "band_structure")
QE_BAND_STRUCTURE_INPUT_VARIABLES = None
GAMMA = {r"$\Gamma$": [0.0, 0.0, 0.0]}   # Gamma
X = {"X": [0.0, 0.5, 0.5]}   # X
K = {"K": [3/8, 3/8, 3/4]}
L = {"L": [1/2, 1/2, 1/2]}
W = {"W": [1/4, 1/2, 3/4]}

QE_BAND_STRUCTURE_KPOINT_PATH = [GAMMA, X, W, L, K, GAMMA, W]
QE_BAND_STRUCTURE_KPOINT_PATH_DENSITY = 100


# ######################## FERMI SURFACE PART #################################
QE_FERMI_SURFACE_EXAMPLE = os.path.join(
        __TEST_WORKFLOW_ROOT__, "test_files", "qe_Pb", "fermi_surface")
QE_FERMI_SURFACE_NSCF_INPUT_VARIABLES = {}
QE_FERMI_SURFACE_NSCF_KGRID = [15] * 3


# ####################### PHONON DISPERSION PART ##############################

KPATH = [GAMMA, X, W, L, K, GAMMA, W]
QE_PHONON_DISPERSION_QPATH = KPATH
QE_PHONON_DISPERSION_QPATH_DENSITY = 100
QE_PHONON_DISPERSION_PHONONS_INPUT_VARIABLES = {
        "tr2_ph": 1e-12,
        }
QE_PHONON_DISPERSION_PHONONS_QPOINT_GRID = [2, 2, 2]
QE_PHONON_DISPERSION_PHONONS_EXAMPLE = os.path.join(
        __TEST_WORKFLOW_ROOT__, "test_files", "qe_Pb",
        "phonon_dispersion")


# #################### EPW INTERPOLATION PART #################################
QE_EPW_INTERPOLATION_EXAMPLE = os.path.join(
        __TEST_WORKFLOW_ROOT__, "test_files", "qe_Pb",
        "epw_interpolation")
QE_EPW_INTERPOLATION_NSCF_KGRID = [6, 6, 6]
QE_EPW_INTERPOLATION_NSCF_INPUT_VARIABLES = {
         "tprnfor": True,
         "tstress": True,
         "conv_thr": 1e-10,
         "diagonalization": "cg",
         "diago_full_acc": True,
         "noncolin": False,
         "lspinorb": False,
         "nbnd": 15}
QE_EPW_INTERPOLATION_EPW_INPUT_VARIABLES = {
         "amass(1)": 207.2,
         "elph": True,
         "band_plot": True,
         "etf_mem": 0,
         "kmaps": False,
         "epbwrite": True,
         "epbread": False,
         "epwwrite": True,
         "epwread": False,
         "nbndsub": 4,   # number of wannier functions to use
         # "nbndskip": 10,  # nbands under the disentanglement window
         "bands_skipped": "exclude_bands = 1-5",
         "wannierize": True,
         "num_iter": 1000,
         "wdata(1)": "dis_num_iter = 1000",
         "wdata(2)": "guiding_centres = True",
         "wdata(3)": "num_print_cycles = 100",
         "wdata(4)": "conv_window = 10",
         "dis_win_max": 21,
         "dis_win_min": -3,
         "dis_froz_min": -3,
         "dis_froz_max": 13.5,
         "proj(1)": "Pb:sp3",
         "iverbosity": 0,
         "elecselfen": False,
         "phonselfen": False,
         "fsthick": 6,  # fermi surface thickness
         "degaussw": 0.0,
         "ngaussw": -99,  # force FD distribution from the start
         "a2f": False,
         "vme": True,
         }


# ############################## IBTE PART ####################################
QE_IBTE_EXAMPLE = os.path.join(
        __TEST_WORKFLOW_ROOT__, "test_files", "qe_Pb", "ibte", "ibte_run")
QE_IBTE_INPUT_VARIABLES = {
         "assume_metal": True,
         "restart_step": 100,
         "degaussw": 0.0,
         "etf_mem": 0,
         "fsthick": 0.1,
         "ngaussw": -99,
         "vme": True,
         "temps": [50, 100, 300, 600],
         "nstemp": 4,
         "nkf1": 10, "nkf2": 10, "nkf3": 10,
         "nqf1": 10, "nqf2": 10, "nqf3": 10}


# #################### PHONON DISPERSION QGRID CONVERGENCE PART ###############
QE_PHONON_DISPERSION_QGRID_CONVERGENCE_PHONONS_QGRIDS = [
        [2, 2, 2], [4, 4, 4]]
QE_PHONON_DISPERSION_QGRID_CONVERGENCE_PHONONS_INPUT_VARIABLES = {
        "tr2_ph": 1e-12,
        }
QE_PHONON_DISPERSION_QGRID_CONVERGENCE_QPATH = KPATH
QE_PHONON_DISPERSION_QGRID_CONVERGENCE_QPATH_DENSITY = 100
QE_PHONON_DISPERSION_QGRID_CONVERGENCE_EXAMPLE = os.path.join(
        __TEST_WORKFLOW_ROOT__, "test_files", "qe_Pb",
        "phonon_dispersion_qgrid_convergence")


# ######################### LATTICE THERMAL EXPANSION PART ####################
QE_LATTICE_EXPANSION_DELTAS_VOLUMES = [0, -2, 2, -4, 4, -6, 6]
QE_LATTICE_EXPANSION_SCF_EXAMPLES = os.path.join(
        __TEST_WORKFLOW_ROOT__, "test_files", "qe_Pb", "lattice_expansion",
        "scf_runs")
QE_LATTICE_EXPANSION_PHONONS_EXAMPLES = os.path.join(
        __TEST_WORKFLOW_ROOT__, "test_files", "qe_Pb", "lattice_expansion",
        "phonons_runs")
QE_LATTICE_EXPANSION_Q2R_EXAMPLES = os.path.join(
        __TEST_WORKFLOW_ROOT__, "test_files", "qe_Pb", "lattice_expansion",
        "q2r_runs")
QE_LATTICE_EXPANSION_MATDYN_EXAMPLES = os.path.join(
        __TEST_WORKFLOW_ROOT__, "test_files", "qe_Pb", "lattice_expansion",
        "matdyn_runs")

# ###############  EPW INTERPOLATION WITH THERMAL EXPANSION PART ##############
__QE_EPW_WITH_THERMAL_EXPANSION_EXAMPLE_ROOT__ = os.path.join(
        __TEST_WORKFLOW_ROOT__, "test_files", "qe_Pb",
        "epw_with_lattice_expansion")
__QE_EPW_WITH_THERMAL_EXPANSION_SCF_EXAMPLES__ = os.path.join(
        __QE_EPW_WITH_THERMAL_EXPANSION_EXAMPLE_ROOT__, "scf_runs")
__QE_EPW_WITH_THERMAL_EXPANSION_NSCF_EXAMPLES__ = os.path.join(
        __QE_EPW_WITH_THERMAL_EXPANSION_EXAMPLE_ROOT__, "nscf_runs")
__QE_EPW_WITH_THERMAL_EXPANSION_PHONONS_EXAMPLES__ = os.path.join(
        __QE_EPW_WITH_THERMAL_EXPANSION_EXAMPLE_ROOT__, "phonons_runs")
__QE_EPW_WITH_THERMAL_EXPANSION_Q2R_EXAMPLES__ = os.path.join(
        __QE_EPW_WITH_THERMAL_EXPANSION_EXAMPLE_ROOT__, "q2r_runs")
__QE_EPW_WITH_THERMAL_EXPANSION_BAND_STRUCTURE_EXAMPLES__ = os.path.join(
        __QE_EPW_WITH_THERMAL_EXPANSION_EXAMPLE_ROOT__, "band_structure_runs")
__QE_EPW_WITH_THERMAL_EXPANSION_MATDYN_EXAMPLES__ = os.path.join(
        __QE_EPW_WITH_THERMAL_EXPANSION_EXAMPLE_ROOT__, "matdyn_runs")
__QE_EPW_WITH_THERMAL_EXPANSION_EPW_EXAMPLES__ = os.path.join(
        __QE_EPW_WITH_THERMAL_EXPANSION_EXAMPLE_ROOT__,
        "epw_interpolation_runs")

__QE_IBTE_WITH_THERMAL_EXPANSION_EXAMPLES__ = os.path.join(
        __TEST_WORKFLOW_ROOT__, "test_files", "qe_Pb",
        "ibte_with_thermal_expansion", "ibte_runs")


@pytest.mark.order("first")
class TestQEWorkflow(BaseWorkflowTest, TestCase):
    """Test case for the QEWorkflow.
    """
    _band_structure_example = QE_BAND_STRUCTURE_EXAMPLE
    _band_structure_input_variables = QE_BAND_STRUCTURE_INPUT_VARIABLES
    _band_structure_kpoint_path = QE_BAND_STRUCTURE_KPOINT_PATH
    _band_structure_kpoint_path_density = QE_BAND_STRUCTURE_KPOINT_PATH_DENSITY
    _gs_ecut_convergence_criterion = QE_GS_ECUT_CONVERGENCE_CRITERION
    _gs_ecut_convergence_input_vars = QE_GS_ECUT_CONVERGENCE_INPUT_VARS
    _gs_ecut_convergence_ecuts = QE_GS_ECUT_CONVERGENCE_ECUTS
    _gs_ecut_convergence_example = QE_GS_ECUT_CONVERGENCE_EXAMPLE
    _gs_kgrid_convergence_criterion = QE_GS_KGRID_CONVERGENCE_CRITERION
    _gs_kgrid_convergence_input_vars = QE_GS_KGRID_CONVERGENCE_INPUT_VARS
    _gs_kgrid_convergence_kgrids = QE_GS_KGRID_CONVERGENCE_KGRIDS
    _gs_kgrid_convergence_example = QE_GS_KGRID_CONVERGENCE_EXAMPLE
    _gs_smearing_convergence_criterion = QE_GS_SMEARING_CONVERGENCE_CRITERION
    _gs_smearing_convergence_input_vars = QE_GS_SMEARING_CONVERGENCE_INPUT_VARS
    _gs_smearing_convergence_kgrids = QE_GS_SMEARING_CONVERGENCE_KGRIDS
    _gs_smearing_convergence_smearings = QE_GS_SMEARING_CONVERGENCE_SMEARINGS
    _gs_smearing_convergence_example = QE_GS_SMEARING_CONVERGENCE_EXAMPLE
    _gs_input_vars = QE_GS_INPUT_VARS
    _gs_example = QE_GS_EXAMPLE
    _init_kwargs = {"gs": True, "lattice_expansion": True}
    _phonon_ecut_convergence_ecuts = QE_PHONON_ECUT_CONVERGENCE_ECUTS
    _phonon_ecut_convergence_example = QE_PHONON_ECUT_CONVERGENCE_EXAMPLE
    _phonon_ecut_convergence_scf_input_vars = (
            QE_PHONON_ECUT_CONVERGENCE_SCF_INPUT_VARS)
    _phonon_ecut_convergence_phonons_input_vars = (
            QE_PHONON_ECUT_CONVERGENCE_PHONONS_INPUT_VARS)
    _phonon_ecut_convergence_phonon_qpt = QE_PHONON_ECUT_CONVERGENCE_PHONON_QPT
    _phonon_ecut_convergence_criterion = 10
    _phonon_dispersion_example = QE_PHONON_DISPERSION_PHONONS_EXAMPLE
    _phonon_dispersion_phonons_input_vars = (
                QE_PHONON_DISPERSION_PHONONS_INPUT_VARIABLES
                )
    _phonon_dispersion_qgrid = QE_PHONON_DISPERSION_PHONONS_QPOINT_GRID
    _phonon_dispersion_qpoint_path = QE_PHONON_DISPERSION_QPATH
    _phonon_dispersion_qpoint_path_density = QE_PHONON_DISPERSION_QPATH_DENSITY
    _phonon_smearing_convergence_example = (
            QE_PHONON_SMEARING_CONVERGENCE_EXAMPLE)
    _phonon_smearing_convergence_scf_input_vars = (
            QE_PHONON_SMEARING_CONVERGENCE_INPUT_VARS)
    _phonon_smearing_convergence_phonons_input_vars = {}
    _phonon_smearing_convergence_kgrids = QE_PHONON_SMEARING_CONVERGENCE_KGRIDS
    _phonon_smearing_convergence_phonon_qpt = (
            QE_PHONON_SMEARING_CONVERGENCE_PHONON_QPT)
    _phonon_smearing_convergence_smearings = (
            QE_PHONON_SMEARING_CONVERGENCE_SMEARINGS)
    _phonon_smearing_convergence_criterion = 10
    _relaxation_input_vars = QE_RELAXATION_INPUT_VARIABLES
    _scf_script_name = "pw.x"
    _phonons_script_name = "ph.x"
    _relaxation_other_kwargs = {"maximum_relaxations": 10}
    _workflow_cls = QEWorkflow

    def setUp(self):
        super().setUp()
        self.q2r_command_file = tempfile.NamedTemporaryFile(
                suffix="q2r.x")
        self.matdyn_command_file = tempfile.NamedTemporaryFile(
                suffix="matdyn.x")
        self.epw_command_file = tempfile.NamedTemporaryFile(suffix="epw.x")

    def test_epw_interpolation(self):
        workflow = self._workflow_cls(
                band_structure=True,
                gs=True, epw_interpolation=True,
                phonon_dispersion=True,
                )
        self._set_gs(workflow)
        self._set_phonon_dispersion(workflow)
        self._set_phonon_dispersion_done(
                workflow, all_phonons=True, q2r=True, matdyn=True)
        self._set_band_structure_for_workflow(workflow)
        self._set_band_structure_done(workflow)
        self._set_epw_interpolation(workflow)
        workflow.write()
        # make sure nscf calc was written
        nscf = workflow.epw_interpolation_sequencer.nscf_workdir
        self.assertTrue(os.path.isdir(nscf))
        shutil.rmtree(nscf)
        self._set_epw_interpolation_done(workflow, nscf=True)
        workflow.write()
        # make sure epw part has been written
        epw = workflow.epw_interpolation_sequencer.epw_workdir
        self.assertTrue(os.path.isdir(epw))
        shutil.rmtree(epw)
        self._set_epw_interpolation_done(workflow, epw=True)
        workflow.run()
        # make sure results have been printed
        results = os.path.join(
                self.workflow_dir.name, "epw_interpolation", "results")
        self.assertGreaterEqual(len(os.listdir(results)), 2)

    def test_epw_interpolation_with_lattice_expansion(self):
        workflow = self._workflow_cls(
                gs=True, lattice_expansion=True,
                epw_interpolation_with_thermal_expansion=True)
        self._set_gs(workflow)
        self._set_gs_calculation_done(workflow)
        self._set_lattice_expansion_for_workflow(workflow)
        self._set_epw_with_lattice_expansion_for_workflow(workflow)
        workflow.write()
        # make sure no calculations for the epw run are written
        root_workdir = os.path.join(
                self.workflow_dir.name, "epw_with_lattice_expansion")
        self.assertFalse(os.path.exists(root_workdir))
        # copy calculation for lattice expansion
        lat_seq = workflow.lattice_expansion_sequencer
        # copy scf calculations
        # rm all calcs that are not a symlink
        dirname = os.path.dirname(lat_seq.scf_workdir)
        for path in os.listdir(dirname):
            join = os.path.join(dirname, path)
            if os.path.islink(join):
                continue
            shutil.rmtree(join)
        shutil.rmtree(
                os.path.dirname(
                    os.path.dirname(
                        workflow.lattice_expansion_sequencer.phonons_workdir)))

        self._set_lattice_expansion_done(workflow)
        # reset this workflow part
        sequencer = workflow.lattice_expansion_sequencer
        sequencer.clear_sequence()
        workflow.run_lattice_expansion()
        # reset epw setup
        workflow.stop_at_workflow = None
        self._set_epw_with_lattice_expansion_for_workflow(workflow)
        workflow.write()
        self.assertTrue(os.path.exists(root_workdir), msg=root_workdir)
        # set all calculations done and check plots are made
        shutil.rmtree(root_workdir)
        self._set_epw_with_lattice_expansion_done(workflow)
        workflow.run()
        # check that a results directory has been created with results in it
        results = os.path.join(root_workdir, "results")
        self.assertTrue(os.path.exists(results))
        self.assertTrue(len(os.listdir(results)) > 2)
        self.assertTrue(workflow.workflow_completed)

    def test_epw_with_manual_lattice_expansion(self):
        # same as epw_with_lattice_expansion but we don't compute lattice exp
        workflow = self._workflow_cls(
                epw_interpolation_with_thermal_expansion=True)
        self._set_epw_with_manual_lattice_expansion_for_workflow(workflow)
        root_workdir = os.path.join(
                self.workflow_dir.name, "epw_with_lattice_expansion")
        self._set_epw_with_lattice_expansion_done(workflow)
        workflow.run()
        # check that a results directory has been created with results in it
        results = os.path.join(root_workdir, "results")
        self.assertTrue(os.path.exists(results))
        self.assertTrue(len(os.listdir(results)) > 2)
        self.assertTrue(workflow.workflow_completed)

    def test_fermi_surface(self):
        workflow = self._workflow_cls(
                gs=True, fermi_surface=True,
                **self._workflow_init_kwargs)
        self._set_gs(workflow)
        self._set_gs_done(workflow)
        self._set_fermi_surface(workflow)
        workflow.write()
        # check nscf calc has been written
        nscf = workflow.fermi_surface_sequencer.nscf_workdir
        self.assertTrue(
                CalculationDirectory.is_calculation_directory(nscf))
        shutil.rmtree(nscf)
        self._set_fermi_surface_done(workflow)
        workflow.run()
        self.assertEqual(
                len(os.listdir(
                    os.path.join(os.path.dirname(nscf), "results"))), 6
                )

    def test_ibte(self):
        workflow = self._workflow_cls(
                gs=True, band_structure=True, phonon_dispersion=True,
                epw_interpolation=True, ibte=True)
        self._set_gs(workflow)
        self._set_phonon_dispersion(workflow)
        self._set_phonon_dispersion_done(
                workflow, all_phonons=True, q2r=True, matdyn=True)
        self._set_band_structure_for_workflow(workflow)
        self._set_band_structure_done(workflow)
        self._set_epw_interpolation(workflow)
        self._set_epw_interpolation_done(workflow, nscf=True, epw=True)
        self._set_ibte(workflow)
        workflow.write()
        self.assertTrue(
                os.path.isdir(
                    workflow.ibte_sequencer.ibte_workdir))
        shutil.rmtree(workflow.ibte_sequencer.ibte_workdir)
        self._set_ibte_done(workflow)
        workflow.run()
        self.assertEqual(
                len(os.listdir(
                    os.path.dirname(workflow.ibte_sequencer.plot_save))),
                8)

    def test_ibte_with_lattice_expansion(self):
        workflow = self._workflow_cls(
                gs=True, lattice_expansion=True,
                epw_interpolation_with_thermal_expansion=True,
                ibte_with_thermal_expansion=True)
        self._set_gs(workflow)
        self._set_gs_calculation_done(workflow)
        self._set_lattice_expansion_for_workflow(workflow)
        self._set_lattice_expansion_done(workflow)
        self._set_epw_with_lattice_expansion_for_workflow(workflow)
        self._set_epw_with_lattice_expansion_done(workflow)
        self._set_ibte_with_lattice_expansion_for_workflow(workflow)
        self._set_ibte_with_lattice_expansion_done(workflow)
        # check that results have been written
        workflow.run()
        results = os.path.dirname(
                workflow.ibte_with_thermal_expansion_sequencer[0].plot_save)
        self.assertTrue(os.path.exists(results))
        self.assertGreater(
                len(os.listdir(results)), 4, msg=os.listdir(results))

    def test_ibte_with_manual_lattice_expansion(self):
        # samething as with lattice expansion but with manual settings
        workflow = self._workflow_cls(
                epw_interpolation_with_thermal_expansion=True,
                ibte_with_thermal_expansion=True,
                )
        self._set_epw_with_manual_lattice_expansion_for_workflow(workflow)
        self._set_epw_with_lattice_expansion_done(workflow)
        self._set_ibte_with_lattice_expansion_for_workflow(workflow)
        self._set_ibte_with_lattice_expansion_done(workflow)
        # check that results have been written
        workflow.run()
        results = os.path.dirname(
                workflow.ibte_with_thermal_expansion_sequencer[0].plot_save)
        self.assertTrue(os.path.exists(results))
        self.assertGreater(
                len(os.listdir(results)), 4, msg=os.listdir(results))

    def test_lattice_expansion(self):
        workflow = self._workflow_cls(gs=True, lattice_expansion=True)
        self._set_gs(workflow)
        # copy gs calculation
        self._set_gs_calculation_done(workflow)
        self._set_lattice_expansion_for_workflow(workflow)
        # write workflow
        workflow.write()
        # this should have written all the scf calcs.
        scf_rootdir = os.path.dirname(
                workflow.lattice_expansion_sequencer.scf_workdir)
        self.assertEqual(
                len(os.listdir(scf_rootdir)),
                len(QE_LATTICE_EXPANSION_DELTAS_VOLUMES),
                msg=os.listdir(scf_rootdir))
        self.assertFalse(workflow.workflow_completed)

        lat_seq = workflow.lattice_expansion_sequencer
        # copy scf calculations
        # rm all calcs that are not a symlink
        dirname = os.path.dirname(lat_seq.scf_workdir)
        for path in os.listdir(dirname):
            join = os.path.join(dirname, path)
            if os.path.islink(join):
                continue
            shutil.rmtree(join)
        self._set_lattice_expansion_scf_calculations_done(workflow)
        workflow.write()
        self.assertEqual(
                len(os.listdir(os.path.dirname(
                    workflow.lattice_expansion_sequencer.phonons_workdir))),
                len(QE_LATTICE_EXPANSION_DELTAS_VOLUMES))
        self.assertFalse(workflow.workflow_completed)
        # copy phonon calculations
        shutil.rmtree(
                os.path.dirname(
                    workflow.lattice_expansion_sequencer.phonons_workdir))
        self._set_lattice_expansion_phonons_calculations_done(workflow)
        workflow.write()
        self.assertEqual(
                len(os.listdir(os.path.dirname(
                    workflow.lattice_expansion_sequencer.q2r_workdir))),
                len(QE_LATTICE_EXPANSION_DELTAS_VOLUMES))
        self.assertFalse(workflow.workflow_completed)
        # copy the q2r calculations
        shutil.rmtree(
                os.path.dirname(
                    workflow.lattice_expansion_sequencer.q2r_workdir))
        self._set_lattice_expansion_q2r_calculations_done(workflow)
        workflow.write()
        self.assertEqual(
                len(os.listdir(os.path.dirname(lat_seq.matdyn_workdir))),
                len(QE_LATTICE_EXPANSION_DELTAS_VOLUMES))
        self.assertFalse(workflow.workflow_completed)
        # copy matdyn calculations
        shutil.rmtree(
                os.path.dirname(
                    workflow.lattice_expansion_sequencer.matdyn_workdir))
        self._set_lattice_expansion_matdyn_calculations_done(workflow)
        workflow.run()
        # make sure plots were done there should be 3 pdf 3 pickles
        self.assertEqual(
                len(os.listdir(os.path.dirname(lat_seq.plot_save))), 6)
        self.assertTrue(workflow.workflow_completed)

    def test_phonon_dispersion_qgrid_convergence(self):
        workflow = self._workflow_cls(
                gs=True,
                phonon_dispersion_qgrid_convergence=True,
                **self._workflow_init_kwargs)
        self._set_phonon_dispersion_qgrid_convergence_for_workflow(workflow)
        workflow.write()
        # make sure the first phonon calc was written for each qgrids
        dir_ = os.path.join(
                    self.workflow_dir.name,
                    "phonon_dispersion_qgrid_convergence")
        self.assertEqual(
                len(os.listdir(dir_)),
                len(QE_PHONON_DISPERSION_QGRID_CONVERGENCE_PHONONS_QGRIDS)
                )
        for subdir in os.listdir(dir_):
            self.assertEqual(
                    len(os.listdir(
                        os.path.join(dir_, subdir, "phonons_runs"))),
                    1)
        shutil.rmtree(dir_)
        self._set_phonon_dispersion_qgrid_convergence_done(
                workflow, first_phonon=True)
        workflow.write()
        # check all phonon calcs have been written
        for seq in workflow.phonon_dispersion_qgrid_convergence_sequencer:
            self.assertEqual(
                len(os.listdir(
                        os.path.dirname(seq.phonons_workdir))),
                seq.nphonons)
        shutil.rmtree(dir_)
        self._set_phonon_dispersion_qgrid_convergence_done(
                workflow, all_phonons=True)
        workflow.write()
        # make sure q2r calculation is written
        for seq in workflow.phonon_dispersion_qgrid_convergence_sequencer:
            dir_ = os.path.dirname(seq.q2r_workdir)
            self.assertIn("q2r_run", os.listdir(dir_))
            shutil.rmtree(os.path.join(dir_, "q2r_run"))
        self._set_phonon_dispersion_qgrid_convergence_done(workflow, q2r=True)
        workflow.write()
        # make sure the matdyn calc was written
        for seq in workflow.phonon_dispersion_qgrid_convergence_sequencer:
            dir_ = os.path.dirname(seq.q2r_workdir)
            self.assertIn("matdyn_run", os.listdir(dir_))
            shutil.rmtree(os.path.join(dir_, "matdyn_run"))
        self._set_phonon_dispersion_qgrid_convergence_done(
                workflow, matdyn=True)
        workflow.run()
        # make sure there are >= 2 files in results dir
        dir_ = os.path.join(
                    self.workflow_dir.name,
                    "phonon_dispersion_qgrid_convergence")
        results_dir = os.path.join(dir_, "results")
        self.assertEqual(len(os.listdir(results_dir)), 2)
        self.assertTrue(workflow.workflow_completed)

    def _check_phonon_dispersion_post_phonons(self, workflow):
        dir_ = os.path.join(
                    self.workflow_dir.name,
                    "phonon_dispersion")
        # make sure q2r calculation is written
        self.assertIn("q2r_run", os.listdir(dir_))
        shutil.rmtree(os.path.join(dir_, "q2r_run"))
        self._set_phonon_dispersion_done(workflow, q2r=True)
        workflow.write()
        # make sure the matdyn calc was written
        self.assertIn("matdyn_run", os.listdir(dir_))
        shutil.rmtree(os.path.join(dir_, "matdyn_run"))
        self._set_phonon_dispersion_done(workflow, matdyn=True)

    def _set_band_structure_done(self, *args, **kwargs):
        super()._set_band_structure_done(
                *args, **kwargs, pseudo_dir=__PSEUDO_DIR__)

    def _set_epw_interpolation(self, workflow):
        workflow.set_epw_interpolation(
                root_workdir=os.path.join(
                    self.workflow_dir.name, "epw_interpolation"),
                use_gs=True,
                nscf_kgrid=QE_EPW_INTERPOLATION_NSCF_KGRID,
                nscf_input_variables=QE_EPW_INTERPOLATION_NSCF_INPUT_VARIABLES,
                nscf_calculation_parameters={
                    "command": self.scf_command_file.name,
                    "queuing_system": "local"},
                use_band_structure=True,
                use_phonon_dispersion=True,
                epw_input_variables=(
                    QE_EPW_INTERPOLATION_EPW_INPUT_VARIABLES.copy()),
                epw_calculation_parameters={
                    "command": self.epw_command_file.name,
                    "queuing_system": "local",
                    },
                plot_calculation_parameters={"show": False},
                )

    def _set_epw_interpolation_done(self, workflow, nscf=False, epw=False):
        if nscf:
            copy_calculation(
                    os.path.join(QE_EPW_INTERPOLATION_EXAMPLE, "nscf_run"),
                    workflow.epw_interpolation_sequencer.nscf_workdir,
                    pseudo_dir=__PSEUDO_DIR__,
                    new_parents=[
                        workflow.epw_interpolation_sequencer.scf_workdir])
        if epw:
            new_parents = [
                workflow.epw_interpolation_sequencer.nscf_workdir,
                workflow.epw_interpolation_sequencer.q2r_workdir,
                workflow.epw_interpolation_sequencer.matdyn_workdir,
                workflow.epw_interpolation_sequencer.band_structure_workdir,
                ]
            for path in os.listdir(
                    os.path.dirname(
                        workflow.phonon_dispersion_sequencer.phonons_workdir)):
                seq = workflow.phonon_dispersion_sequencer
                new_parents.append(
                        os.path.join(
                            os.path.dirname(seq.phonons_workdir),
                            path))
            copy_calculation(
                    os.path.join(QE_EPW_INTERPOLATION_EXAMPLE,
                                 "epw_interpolation_run"),
                    workflow.epw_interpolation_sequencer.epw_workdir,
                    new_parents=new_parents,
                    )

    def _set_fermi_surface(self, workflow):
        workflow.set_fermi_surface(
                root_workdir=os.path.join(
                    self.workflow_dir.name, "fermi_surface"),
                use_gs=True,
                nscf_kgrid=QE_FERMI_SURFACE_NSCF_KGRID,
                nscf_calculation_parameters={
                    "queuing_system": "local",
                    "command": self.scf_command_file.name},
                plot_calculation_parameters={"show": False},
                nscf_input_variables=(
                    QE_FERMI_SURFACE_NSCF_INPUT_VARIABLES.copy()),
                )

    def _set_fermi_surface_done(self, workflow):
        copy_calculation(
                os.path.join(QE_FERMI_SURFACE_EXAMPLE, "nscf_run"),
                workflow.fermi_surface_sequencer.nscf_workdir,
                pseudo_dir=__PSEUDO_DIR__,
                new_parents=[workflow.gs_sequencer.scf_workdir],
                )

    def _set_gs_done(self, *args, **kwargs):
        super()._set_gs_done(*args, **kwargs, pseudo_dir=__PSEUDO_DIR__)

    def _set_gs_calculation_done(self, workflow):
        copy_calculation(
                self._gs_example, workflow.gs_sequencer.scf_workdir,
                pseudo_dir=__PSEUDO_DIR__)

    def _set_gs_ecut_convergence_done(self, *args, **kwargs):
        super()._set_gs_ecut_convergence_done(
                *args, pseudo_dir=__PSEUDO_DIR__, **kwargs)

    def _set_gs_kgrid_convergence_done(self, *args, **kwargs):
        super()._set_gs_kgrid_convergence_done(
                *args, pseudo_dir=__PSEUDO_DIR__, **kwargs)

    def _set_gs_smearing_convergence_done(self, workflow):
        super()._set_gs_smearing_convergence_done(
                workflow,
                pseudo_dir=__PSEUDO_DIR__,
                )

    def _set_ibte(self, workflow):
        workflow.set_ibte(
                root_workdir=os.path.join(self.workflow_dir.name, "ibte"),
                ibte_input_variables=QE_IBTE_INPUT_VARIABLES,
                ibte_calculation_parameters={
                    "command": self.epw_command_file.name,
                    "queuing_system": "local",
                    },
                plot_calculation_parameters={"show": False})

    def _set_ibte_done(self, workflow):
        copy_calculation(
                QE_IBTE_EXAMPLE,
                workflow.ibte_sequencer.ibte_workdir,
                new_parents=[workflow.epw_interpolation_sequencer.epw_workdir],
                )

    def _set_ibte_with_lattice_expansion_done(self, workflow):
        for seq in workflow.ibte_with_thermal_expansion_sequencer:
            ibte_workdir = os.path.dirname(seq.ibte_workdir)
            calc = os.path.basename(seq.ibte_workdir)
            # fix here for legacy, source has its dirname in integer form
            calc_int = str(int(float(calc.split("K")[0]))) + "K"
            source = os.path.join(
                        __QE_IBTE_WITH_THERMAL_EXPANSION_EXAMPLES__, calc_int)
            target = os.path.join(ibte_workdir, calc)
            copy_calculation(source, target,
                             new_parents=[seq.epw_interpolation_calculation],
                             title=calc, prefix=calc,
                             )

    def _set_epw_with_lattice_expansion_done(self, workflow):
        # set scf calculations
        seq = workflow.epw_interpolation_with_thermal_expansion_sequencer[0]
        scf_workdir = os.path.dirname(seq.scf_workdir)
        for calc in os.listdir(__QE_EPW_WITH_THERMAL_EXPANSION_SCF_EXAMPLES__):
            copy_calculation(
                    os.path.join(
                        __QE_EPW_WITH_THERMAL_EXPANSION_SCF_EXAMPLES__, calc),
                    os.path.join(scf_workdir, calc),
                    pseudo_dir=__PSEUDO_DIR__,
                    )
        # set phonons calculations
        ph_workdir = os.path.dirname(os.path.dirname(seq.phonons_workdir))
        for temp_dir in os.listdir(
                __QE_EPW_WITH_THERMAL_EXPANSION_PHONONS_EXAMPLES__):
            for calc in os.listdir(
                    os.path.join(
                        __QE_EPW_WITH_THERMAL_EXPANSION_PHONONS_EXAMPLES__,
                        temp_dir)):
                source = os.path.join(
                        __QE_EPW_WITH_THERMAL_EXPANSION_PHONONS_EXAMPLES__,
                        temp_dir, calc)
                new_parent = os.path.join(scf_workdir, temp_dir)
                copy_calculation(
                        source,
                        os.path.join(ph_workdir, temp_dir, calc),
                        new_parents=[new_parent])
        # set band structures
        ex = __QE_EPW_WITH_THERMAL_EXPANSION_BAND_STRUCTURE_EXAMPLES__
        band_structure_workdir = os.path.dirname(seq.band_structure_workdir)
        for calc in os.listdir(
                __QE_EPW_WITH_THERMAL_EXPANSION_BAND_STRUCTURE_EXAMPLES__):
            copy_calculation(
                    os.path.join(ex, calc),
                    os.path.join(band_structure_workdir, calc),
                    new_parents=[os.path.join(scf_workdir, calc)],
                    pseudo_dir=__PSEUDO_DIR__,
                    )
        # set nscf calcs
        nscf_workdir = os.path.dirname(seq.nscf_workdir)
        for calc in os.listdir(
                __QE_EPW_WITH_THERMAL_EXPANSION_NSCF_EXAMPLES__):
            copy_calculation(
                    os.path.join(
                        __QE_EPW_WITH_THERMAL_EXPANSION_NSCF_EXAMPLES__, calc),
                    os.path.join(nscf_workdir, calc),
                    new_parents=[os.path.join(scf_workdir, calc)],
                    pseudo_dir=__PSEUDO_DIR__,
                    )
        # set q2r calcs
        q2r_workdir = os.path.dirname(seq.q2r_workdir)
        for calc in os.listdir(__QE_EPW_WITH_THERMAL_EXPANSION_Q2R_EXAMPLES__):
            phdir = os.path.join(ph_workdir, calc)
            copy_calculation(
                    os.path.join(
                        __QE_EPW_WITH_THERMAL_EXPANSION_Q2R_EXAMPLES__, calc),
                    os.path.join(q2r_workdir, calc),
                    new_parents=[
                        os.path.join(phdir, ph) for ph in os.listdir(phdir)],
                    )
        # set matdyn calcs
        matdyn_workdir = os.path.dirname(seq.matdyn_workdir)
        for calc in os.listdir(
                __QE_EPW_WITH_THERMAL_EXPANSION_MATDYN_EXAMPLES__):
            copy_calculation(
                    os.path.join(
                        __QE_EPW_WITH_THERMAL_EXPANSION_MATDYN_EXAMPLES__,
                        calc),
                    os.path.join(matdyn_workdir, calc),
                    new_parents=[os.path.join(q2r_workdir, calc)],
                    )
        # set epw calc
        epw_workdir = os.path.dirname(seq.epw_workdir)
        for calc in os.listdir(__QE_EPW_WITH_THERMAL_EXPANSION_EPW_EXAMPLES__):
            phdir = os.path.join(ph_workdir, calc)
            copy_calculation(
                    os.path.join(
                        __QE_EPW_WITH_THERMAL_EXPANSION_EPW_EXAMPLES__, calc),
                    os.path.join(epw_workdir, calc),
                    new_parents=[
                        os.path.join(nscf_workdir, calc),
                        os.path.join(band_structure_workdir, calc),
                        os.path.join(matdyn_workdir, calc),
                        os.path.join(q2r_workdir, calc),
                        ] + [os.path.join(phdir, ph)
                             for ph in os.listdir(phdir)],
                    )

    def _set_epw_with_manual_lattice_expansion_for_workflow(self, workflow):
        # set epw calculations
        GAMMA = {r"$\Gamma$": [0.0, 0.0, 0.0]}   # Gamma
        X = {"X": [0.0, 0.5, 0.5]}   # X
        K = {"K": [3/8, 3/8, 3/4]}
        L = {"L": [1/2, 1/2, 1/2]}
        W = {"W": [1/4, 1/2, 3/4]}
        NQPTS = 100  # number of points between each symmetry points
        NKPTS = 100
        KPATH = [GAMMA, X, W, L, K, GAMMA, W]
        QPATH = KPATH
        root_workdir = os.path.join(
                self.workflow_dir.name, "epw_with_lattice_expansion")
        workflow.set_epw_interpolation_with_thermal_expansion(
                root_workdir=root_workdir,
                lattice_parameters=[9.28673089708421,
                                    9.293005653458401,
                                    9.302776496754362,
                                    9.321717194356948],
                temperatures=[50.0, 150.0, 300.0, 600.0],
                asr="crystal",
                scf_input_variables=QE_GS_INPUT_VARS,
                scf_calculation_parameters={
                    "command": self.scf_command_file.name,
                    "queuing_system": "local",
                    },
                nscf_kgrid=[6, 6, 6],
                nscf_input_variables={
                    "tprnfor": True,
                    "tstress": True,
                    "ecutwfc": 20,
                    "occupations": "smearing",
                    "pseudo_dir": __PSEUDO_DIR__,
                    "degauss": 0.1,
                    "conv_thr": 1e-10,
                    "diagonalization": "cg",
                    "diago_full_acc": True,
                    "noncolin": False,
                    "lspinorb": False,
                    "nbnd": 15},
                nscf_calculation_parameters={
                    "queuing_system": "local",
                    "command": self.scf_command_file.name},
                band_structure_kpoint_path=KPATH,
                band_structure_kpoint_path_density=NKPTS,
                band_structure_calculation_parameters={
                    "command": self.scf_command_file.name,
                    "queuing_system": "local"},
                phonons_input_variables={
                    "tr2_ph": 1e-12,
                    },
                phonons_qpoint_grid=[2, 2, 2],
                phonons_calculation_parameters={
                    "queuing_system": "local",
                    "command": self.phonons_command_file.name,
                    },
                q2r_calculation_parameters={
                    "command": self.q2r_command_file.name,
                    "queuing_system": "local",
                    },
                qpoint_path=QPATH,
                qpoint_path_density=NQPTS,
                matdyn_calculation_parameters={
                    "command": self.matdyn_command_file.name,
                    "queuing_system": "local",
                    },
                epw_input_variables={
                    "amass(1)": 207.2,
                    "elph": True,
                    "band_plot": True,
                    "etf_mem": 0,
                    "kmaps": False,
                    "epbwrite": True,
                    "epbread": False,
                    "epwwrite": True,
                    "epwread": False,
                    "nbndsub": 4,   # number of wannier functions to use
                    "bands_skipped": "exclude_bands = 1-5",
                    "wannierize": True,
                    "num_iter": 1000,
                    "wdata(1)": "dis_num_iter = 1000",
                    "wdata(2)": "guiding_centres = True",
                    "wdata(3)": "num_print_cycles = 100",
                    "wdata(4)": "conv_window = 10",
                    "dis_win_max": 21,
                    "dis_win_min": -3,
                    "dis_froz_min": -3,
                    "dis_froz_max": 13.5,
                    "proj(1)": "Pb:sp3",
                    "iverbosity": 0,
                    "elecselfen": False,
                    "phonselfen": False,
                    "fsthick": 6,  # fermi surface thickness
                    "degaussw": 0.0,
                    "ngaussw": -99,  # force FD distribution from the start
                    "a2f": False,
                    "vme": True,
                    },
                epw_calculation_parameters={
                        "queuing_system": "local",
                        "command": self.epw_command_file.name,
                        },
                plot_parameters={"show": False},
                )

    def _set_epw_with_lattice_expansion_for_workflow(self, workflow):
        # set epw calculations
        GAMMA = {r"$\Gamma$": [0.0, 0.0, 0.0]}   # Gamma
        X = {"X": [0.0, 0.5, 0.5]}   # X
        K = {"K": [3/8, 3/8, 3/4]}
        L = {"L": [1/2, 1/2, 1/2]}
        W = {"W": [1/4, 1/2, 3/4]}
        NQPTS = 100  # number of points between each symmetry points
        NKPTS = 100
        KPATH = [GAMMA, X, W, L, K, GAMMA, W]
        QPATH = KPATH
        root_workdir = os.path.join(
                self.workflow_dir.name, "epw_with_lattice_expansion")
        workflow.set_epw_interpolation_with_thermal_expansion(
                root_workdir=root_workdir,
                nscf_kgrid=[6, 6, 6],
                nscf_input_variables={
                    "tprnfor": True,
                    "tstress": True,
                    "ecutwfc": 20,
                    "occupations": "smearing",
                    "degauss": 0.1,
                    "conv_thr": 1e-10,
                    "diagonalization": "cg",
                    "diago_full_acc": True,
                    "pseudo_dir": __PSEUDO_DIR__,
                    "noncolin": False,
                    "lspinorb": False,
                    "nbnd": 15},
                nscf_calculation_parameters={
                    "queuing_system": "local",
                    "command": self.scf_command_file.name},
                band_structure_kpoint_path=KPATH,
                band_structure_kpoint_path_density=NKPTS,
                band_structure_calculation_parameters={
                    "command": self.scf_command_file.name,
                    "queuing_system": "local"},
                qpoint_path=QPATH,
                qpoint_path_density=NQPTS,
                epw_input_variables={
                    "amass(1)": 207.2,
                    "elph": True,
                    "band_plot": True,
                    "etf_mem": 0,
                    "kmaps": False,
                    "epbwrite": True,
                    "epbread": False,
                    "epwwrite": True,
                    "epwread": False,
                    "nbndsub": 4,   # number of wannier functions to use
                    "bands_skipped": "exclude_bands = 1-5",
                    "wannierize": True,
                    "num_iter": 1000,
                    "wdata(1)": "dis_num_iter = 1000",
                    "wdata(2)": "guiding_centres = True",
                    "wdata(3)": "num_print_cycles = 100",
                    "wdata(4)": "conv_window = 10",
                    "dis_win_max": 21,
                    "dis_win_min": -3,
                    "dis_froz_min": -3,
                    "dis_froz_max": 13.5,
                    "proj(1)": "Pb:sp3",
                    "iverbosity": 0,
                    "elecselfen": False,
                    "phonselfen": False,
                    "fsthick": 6,  # fermi surface thickness
                    "degaussw": 0.0,
                    "ngaussw": -99,  # force FD distribution from the start
                    "a2f": False,
                    "vme": True,
                    },
                epw_calculation_parameters={
                        "queuing_system": "local",
                        "command": self.epw_command_file.name,
                        },
                phonons_qpoint_grid=[2, 2, 2],
                plot_parameters={"show": False},
                )

    def _set_ibte_with_lattice_expansion_for_workflow(self, workflow):
        root_workdir = os.path.join(
                self.workflow_dir.name, "ibte_with_lattice_expansion")
        workflow.relaxed_geometry = {"celldm(1)": 9.134699723318054}
        workflow.set_ibte_with_thermal_expansion(
                ibte_workdir=root_workdir,
                ibte_input_variables={
                    "assume_metal": True,
                    "restart_step": 100,
                    "degaussw": 0.0,
                    "etf_mem": 0,
                    "fsthick": 0.1,
                    "ngaussw": -99,
                    "vme": True,
                    "nkf1": 10, "nkf2": 10, "nkf3": 10,
                    "nqf1": 10, "nqf2": 10, "nqf3": 10},
                ibte_calculation_parameters={
                    "command": self.epw_command_file.name,
                    "queuing_system": "local",
                    },
                plot_parameters={"show": False, "elements": [[0, 0]], },
                )

    def _set_lattice_expansion_for_workflow(self, workflow):
        # setup lattice_expansion calculations
        workflow.relaxed_geometry = {"celldm(1)": 9.134699723318054}
        workflow.set_lattice_expansion(
                deltas_volumes=QE_LATTICE_EXPANSION_DELTAS_VOLUMES,
                asr="crystal",
                temperatures=[50, 150, 300, 600],
                bulk_modulus_initial_guess=37,
                root_workdir=os.path.join(
                    self.workflow_dir.name, "lattice_expansion"),
                phonons_input_variables={
                    "tr2_ph": 1e-12},
                phonons_qpoint_grid=[2, 2, 2],
                phonons_calculation_parameters={
                    "queuing_system": "local",
                    "command": self.phonons_command_file.name,
                    },
                q2r_input_variables={},
                q2r_calculation_parameters={
                    "queuing_system": "local",
                    "command": self.q2r_command_file.name},
                matdyn_calculation_parameters={
                    "queuing_system": "local",
                    "command": self.matdyn_command_file.name},
                plot_calculation_parameters={"plot_show": False},
                )

    def _set_lattice_expansion_scf_calculations_done(self, workflow):
        lat_seq = workflow.lattice_expansion_sequencer
        for calc in os.listdir(QE_LATTICE_EXPANSION_SCF_EXAMPLES):
            # if "+0percent" in calc:
            #     # will be symlinked
            #     continue
            copy_calculation(
                    os.path.join(QE_LATTICE_EXPANSION_SCF_EXAMPLES, calc),
                    os.path.join(
                        os.path.dirname(lat_seq.scf_workdir), calc),
                    pseudo_dir=__PSEUDO_DIR__,
                    )

    def _set_lattice_expansion_phonons_calculations_done(self, workflow):
        lat_seq = workflow.lattice_expansion_sequencer
        for vol_dir in os.listdir(QE_LATTICE_EXPANSION_PHONONS_EXAMPLES):
            for calc in os.listdir(
                    os.path.join(
                        QE_LATTICE_EXPANSION_PHONONS_EXAMPLES, vol_dir)):
                source = os.path.join(
                        QE_LATTICE_EXPANSION_PHONONS_EXAMPLES, vol_dir, calc)
                suffix = "_" + os.path.dirname(source).split("_")[-1]
                new_parent = lat_seq.scf_workdir + suffix
                copy_calculation(
                        source,
                        os.path.join(
                            os.path.dirname(
                                lat_seq.phonons_workdir), vol_dir, calc),
                        new_parents=[new_parent])

    def _set_lattice_expansion_q2r_calculations_done(self, workflow):
        lat_seq = workflow.lattice_expansion_sequencer
        for calc in os.listdir(QE_LATTICE_EXPANSION_Q2R_EXAMPLES):
            suffix = "_" + calc.split("_")[-1]
            parent_root = lat_seq.phonons_workdir + suffix
            copy_calculation(
                    os.path.join(QE_LATTICE_EXPANSION_Q2R_EXAMPLES, calc),
                    os.path.join(os.path.dirname(lat_seq.q2r_workdir), calc),
                    new_parents=[
                        os.path.join(parent_root, x)
                        for x in os.listdir(parent_root)])

    def _set_lattice_expansion_matdyn_calculations_done(self, workflow):
        lat_seq = workflow.lattice_expansion_sequencer
        for calc in os.listdir(QE_LATTICE_EXPANSION_MATDYN_EXAMPLES):
            suffix = "_" + calc.split("_")[-1]
            parent = workflow.lattice_expansion_sequencer.q2r_workdir + suffix
            copy_calculation(
                    os.path.join(QE_LATTICE_EXPANSION_MATDYN_EXAMPLES, calc),
                    os.path.join(
                        os.path.dirname(lat_seq.matdyn_workdir), calc),
                    new_parents=[parent])

    def _set_lattice_expansion_done(self, workflow):
        self._set_lattice_expansion_scf_calculations_done(workflow)
        self._set_lattice_expansion_phonons_calculations_done(workflow)
        self._set_lattice_expansion_q2r_calculations_done(workflow)
        self._set_lattice_expansion_matdyn_calculations_done(workflow)

    def _set_phonon_dispersion(self, workflow):
        super()._set_phonon_dispersion(
                workflow,
                q2r_input_variables={},
                q2r_calculation_parameters={
                    "command": self.q2r_command_file.name,
                    "queuing_system": "local"},
                matdyn_input_variables={
                    "asr": "crystal", "q_in_cryst_coord": True},
                matdyn_calculation_parameters={
                    "command": self.matdyn_command_file.name,
                    "queuing_system": "local"},
                )

    def _set_phonon_dispersion_done(
            self, workflow, first_phonon=False, q2r=False, matdyn=False,
            **kwargs):
        super()._set_phonon_dispersion_done(workflow, **kwargs)
        seq = workflow.phonon_dispersion_sequencer
        root = self._phonon_dispersion_example
        if first_phonon:
            copy_calculation(
                os.path.join(root, "phonons_runs", "ph_q1"),
                os.path.join(
                    os.path.dirname(seq.phonons_workdir), "ph_q1"),
                new_parents=[seq.scf_workdir],
                **kwargs)
        if q2r:
            phroot = os.path.dirname(seq.phonons_workdir)
            copy_calculation(
                    os.path.join(root, "q2r_run"),
                    seq.q2r_workdir,
                    new_parents=[
                        os.path.join(phroot, calcdir)
                        for calcdir in os.listdir(phroot)],
                    )
        if matdyn:
            copy_calculation(
                    os.path.join(root, "matdyn_run"),
                    seq.matdyn_workdir,
                    new_parents=[seq.q2r_workdir],
                    )

    def _set_phonon_dispersion_qgrid_convergence_for_workflow(self, workflow):
        self._set_gs(workflow)
        self._set_gs_done(workflow)
        workflow.set_phonon_dispersion_qgrid_convergence(
            root_workdir=os.path.join(
                    self.workflow_dir.name,
                    "phonon_dispersion_qgrid_convergence"),
            phonons_input_variables=(
                QE_PHONON_DISPERSION_QGRID_CONVERGENCE_PHONONS_INPUT_VARIABLES
                ),
            phonons_qpoint_grids=(
                QE_PHONON_DISPERSION_QGRID_CONVERGENCE_PHONONS_QGRIDS
                ),
            phonons_calculation_parameters={
                "command": self.phonons_command_file.name,
                "queuing_system": "local"},
            plot_calculation_parameters={"show": False},
            q2r_input_variables={},
            q2r_calculation_parameters={
                "command": self.q2r_command_file.name,
                "queuing_system": "local"},
            matdyn_input_variables={"asr": "crystal"},
            matdyn_calculation_parameters={
                "command": self.matdyn_command_file.name,
                "queuing_system": "local"},
            qpoint_path=QE_PHONON_DISPERSION_QGRID_CONVERGENCE_QPATH,
            qpoint_path_density=(
                QE_PHONON_DISPERSION_QGRID_CONVERGENCE_QPATH_DENSITY),
            use_gs=True,
            )

    def _set_phonon_dispersion_qgrid_convergence_done(
            self, workflow, first_phonon=False, all_phonons=False, q2r=False,
            matdyn=False):
        for seq in workflow.phonon_dispersion_qgrid_convergence_sequencer:
            root = os.path.join(
                    QE_PHONON_DISPERSION_QGRID_CONVERGENCE_EXAMPLE,
                    os.path.basename(os.path.dirname(
                        os.path.dirname(seq.phonons_workdir))))
            if first_phonon:
                copy_calculation(
                    os.path.join(root, "phonons_runs", "ph_q1"),
                    os.path.join(
                        os.path.dirname(seq.phonons_workdir), "ph_q1"),
                    new_parents=[seq.scf_workdir])
            if all_phonons:
                phroot = os.path.dirname(seq.phonons_workdir)
                for calcdir in os.listdir(os.path.join(root, "phonons_runs")):
                    copy_calculation(
                        os.path.join(root, "phonons_runs", calcdir),
                        os.path.join(
                            os.path.dirname(seq.phonons_workdir), calcdir),
                        new_parents=[seq.scf_workdir],
                        )
            if q2r:
                phroot = os.path.dirname(seq.phonons_workdir)
                copy_calculation(
                        os.path.join(root, "q2r_run"),
                        seq.q2r_workdir,
                        new_parents=[
                            os.path.join(phroot, calcdir)
                            for calcdir in os.listdir(phroot)],
                        )
            if matdyn:
                copy_calculation(
                        os.path.join(root, "matdyn_run"),
                        seq.matdyn_workdir,
                        new_parents=[seq.q2r_workdir],
                        )

    def _set_phonon_ecut_convergence_done(self, *args, **kwargs):
        super()._set_phonon_ecut_convergence_done(
                *args, _copy_scf_extra_kwargs={"pseudo_dir": __PSEUDO_DIR__},
                **kwargs)

    def _set_phonon_smearing_convergence_done(self, *args, **kwargs):
        super()._set_phonon_smearing_convergence_done(
                *args,
                copy_scf_extra_kwargs={"pseudo_dir": __PSEUDO_DIR__}, **kwargs)

    def _set_relaxation(self, *args, **kwargs):
        super()._set_relaxation(
                *args, relax_atoms=False, **kwargs)

    def _set_relaxation_done(self, workflow):
        dirlist = os.listdir(QE_RELAXATION_EXAMPLE)
        for calcdir in dirlist:
            new_parents = []
            if not calcdir.endswith("1"):
                # add dependencies
                end = calcdir.split("_")[-1]
                new = str(int(end) - 1)
                new_parents = [
                        os.path.join(
                            workflow.relaxation_sequencer.scf_workdir,
                            calcdir.replace("_" + end, "_" + new))]
            copy_calculation(
                    os.path.join(QE_RELAXATION_EXAMPLE, calcdir),
                    os.path.join(
                        workflow.relaxation_sequencer.scf_workdir, calcdir),
                    pseudo_dir=__PSEUDO_DIR__,
                    new_parents=new_parents)
