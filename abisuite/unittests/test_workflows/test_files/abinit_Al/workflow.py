# AbinitWorkflow example for a metal (FCC Al)

from abisuite import AbinitWorkflow


workflow = AbinitWorkflow(
        gs_ecut_convergence=True,
        gs_smearing_convergence=True,
        relaxation=True,
        phonon_ecut_convergence=True,
        phonon_smearing_convergence=True,
        gs=True,
        phonon_dispersion=True,
        pseudos=["../pseudos/Al.psp8"],
        )
workflow.set_gs_ecut_convergence(
        root_workdir="gs_ecut_convergence",
        scf_input_variables={
            "acell": [7.6] * 3,
            "rprim": [[0, 0.5, 0.5], [0.5, 0, 0.5], [0.5, 0.5, 0]],
            "ntypat": 1,
            "znucl": 13,
            "natom": 1,
            "typat": 1,
            "xred": [0, 0, 0],
            "nshiftk": 4,
            "shiftk": [[0.5, 0.5, 0.5], [0.5, 0.0, 0.0],
                       [0, 0.5, 0], [0, 0, 0.5]],
            "nstep": 10,
            "toldfe": 1e-6,
            "occopt": 4,
            "tsmear": 0.1,
            "ngkpt": [4, 4, 4],
            },
        scf_calculation_parameters={
            "mpi_command": "mpirun -np 4",
            },
        scf_ecuts=[10, 20, 30],
        scf_convergence_criterion=10,
        )
workflow.set_gs_smearing_convergence(
        root_workdir="gs_smearing_convergence",
        scf_input_variables={
            "acell": [7.6] * 3,
            "rprim": [[0, 0.5, 0.5], [0.5, 0, 0.5], [0.5, 0.5, 0]],
            "ntypat": 1,
            "znucl": 13,
            "natom": 1,
            "typat": 1,
            "xred": [0, 0, 0],
            "nshiftk": 4,
            "shiftk": [[0.5, 0.5, 0.5], [0.5, 0.0, 0.0],
                       [0, 0.5, 0], [0, 0, 0.5]],
            "nstep": 10,
            "toldfe": 1e-6,
            "occopt": 4,
            },
        scf_calculation_parameters={
            "mpi_command": "mpirun -np 4",
            },
        scf_convergence_criterion=10,
        scf_kgrids=[[x, x, x] for x in [2, 4, 8]],
        scf_kgrids_input_variable_name="ngkpt",
        scf_smearings=[0.1, 0.01, 0.001],
        use_gs_converged_ecut=True,
        )
workflow.set_relaxation(
        root_workdir="relaxation",
        relax_atoms=False, relax_cell=True,
        scf_input_variables={
            "acell": [7.6] * 3,
            "ionmov": 22,
            "ntime": 20,
            "ecutsm": 0.5,
            "rprim": [[0, 0.5, 0.5], [0.5, 0, 0.5], [0.5, 0.5, 0]],
            "ntypat": 1,
            "znucl": 13,
            "natom": 1,
            "typat": 1,
            "xred": [0, 0, 0],
            "nshiftk": 4,
            "shiftk": [[0.5, 0.5, 0.5], [0.5, 0.0, 0.0],
                       [0, 0.5, 0], [0, 0, 0.5]],
            "nstep": 10,
            "toldfe": 1e-6,
            "occopt": 4,
            },
        scf_calculation_parameters={
            "mpi_command": "mpirun -np 4",
            },
        use_gs_converged_ecut=True,
        use_gs_converged_smearing=True,
        )
workflow.set_phonon_ecut_convergence(
        root_workdir="phonon_ecut_convergence",
        scf_input_variables={
            "nshiftk": 4,
            "shiftk": [[0.5, 0.5, 0.5], [0.5, 0.0, 0.0],
                       [0, 0.5, 0], [0, 0, 0.5]],
            "nstep": 10,
            "toldfe": 1e-6,
            "occopt": 4,
            },
        scf_calculation_parameters={
            "mpi_command": "mpirun -np 4",
            },
        scf_ecuts=[10, 30, 50],
        phonons_qpt=[0.0, 0.0, 0.0],
        phonons_convergence_criterion=10,  # cm^-1
        use_gs_converged_smearing=True,
        use_relaxed_geometry=True,
        phonons_input_variables={
            "rfatpol": [1, 1],
            "rfdir": [1, 1, 1],
            "tolvrs": 1e-8,
            "nstep": 25},
        phonons_calculation_parameters={
            "mpi_command": "mpirun -np 4",
            },
        compute_electric_field_response=False,
        )
workflow.set_phonon_smearing_convergence(
        root_workdir="phonon_smearing_convergence",
        scf_input_variables={
            "nshiftk": 4,
            "shiftk": [[0.5, 0.5, 0.5], [0.5, 0.0, 0.0],
                       [0, 0.5, 0], [0, 0, 0.5]],
            "nstep": 10,
            "toldfe": 1e-6,
            "occopt": 4,
            },
        scf_calculation_parameters={
            "mpi_command": "mpirun -np 4",
            },
        scf_kgrids=[[x, x, x] for x in [2, 4, 8]],
        scf_kgrids_input_variable_name="ngkpt",
        scf_smearings=[0.1, 0.01, 0.001],
        phonons_input_variables={
            "rfatpol": [1, 1],
            "rfdir": [1, 1, 1],
            "tolvrs": 1e-8,
            "nstep": 25},
        phonons_calculation_parameters={
            "mpi_command": "mpirun -np 4",
            },
        phonons_qpt=[0.0, 0.0, 0.0],
        phonons_convergence_criterion=10,
        use_phonon_converged_ecut=True,
        use_relaxed_geometry=True,
        )
workflow.set_gs(
        scf_workdir="gs_run",
        scf_input_variables={
            "nshiftk": 4,
            "shiftk": [[0.5, 0.5, 0.5], [0.5, 0.0, 0.0],
                       [0, 0.5, 0], [0, 0, 0.5]],
            "nstep": 10,
            "toldfe": 1e-6,
            "occopt": 4,
            },
        scf_calculation_parameters={
            "mpi_command": "mpirun -np 4"},
        use_phonon_converged_ecut=True,
        use_phonon_converged_smearing=True,
        use_relaxed_geometry=True,
        )
workflow.set_phonon_dispersion(
        root_workdir="phonon_dispersion",
        use_gs=True,
        compute_electric_field_response=False,  # Set to False for metals
        phonons_input_variables={
            "rfatpol": [1, 1],
            "tolvrs": 1e-8,
            "nstep": 25,
            },
        # watchout: the qgrid should be commensurate with the kgrid
        # (user's responsability to check it does)
        phonons_qpoint_grid=[2, 2, 2],
        phonons_calculation_parameters={"mpi_command": "mpirun -np 4"},
        qpoint_path=[
            {"L": [0.5, 0.0, 0.0]},
            {r"$\Gamma$": [0.0, 0.0, 0.0]},
            {"X": [0.0, 0.5, 0.5]},
            {r"$\Gamma_2$": [1.0, 1.0, 1.0]}],
        qpoint_path_density=20,
        mrgddb_calculation_parameters={},
        anaddb_input_variables={
            "ifcflag": 1, "ifcout": 0, "brav": 2,
            "q1shft": [0.0] * 3, "nqshft": 1, "chneut": 1, "dipdip": 1,
            "eivec": 4,
            },
        anaddb_calculation_parameters={},
        )
workflow.run()
