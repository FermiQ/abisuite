#!/bin/bash

MPIRUN="mpirun -np 4"
EXECUTABLE="/home/fgoudreault/Workspace/abinit/abinit/build/develop/2021-04-16/src/98_main/abinit"
INPUT=/home/fgoudreault/Workspace/abisuite/abisuite/unittests/test_workflows/test_files/abinit_Al/phonon_ecut_convergence/phonons_runs/ecut/ecut_30/run/ecut_30.files
LOG=/home/fgoudreault/Workspace/abisuite/abisuite/unittests/test_workflows/test_files/abinit_Al/phonon_ecut_convergence/phonons_runs/ecut/ecut_30/ecut_30.log
STDERR=/home/fgoudreault/Workspace/abisuite/abisuite/unittests/test_workflows/test_files/abinit_Al/phonon_ecut_convergence/phonons_runs/ecut/ecut_30/ecut_30.stderr


$MPIRUN $EXECUTABLE < $INPUT > $LOG 2> $STDERR
