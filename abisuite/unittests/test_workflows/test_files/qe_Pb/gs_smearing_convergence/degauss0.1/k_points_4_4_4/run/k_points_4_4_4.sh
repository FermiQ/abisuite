#!/bin/bash

MPIRUN="mpirun -np 4"
EXECUTABLE="/home/fgoudreault/Workspace/q-e/bin/pw.x -npool 4"
INPUT=/home/fgoudreault/Workspace/abisuite/abisuite/unittests/test_workflows/test_files/qe_Pb/gs_smearing_convergence/degauss0.1/k_points_4_4_4/k_points_4_4_4.in
LOG=/home/fgoudreault/Workspace/abisuite/abisuite/unittests/test_workflows/test_files/qe_Pb/gs_smearing_convergence/degauss0.1/k_points_4_4_4/k_points_4_4_4.log
STDERR=/home/fgoudreault/Workspace/abisuite/abisuite/unittests/test_workflows/test_files/qe_Pb/gs_smearing_convergence/degauss0.1/k_points_4_4_4/k_points_4_4_4.stderr


$MPIRUN $EXECUTABLE -input $INPUT > $LOG 2> $STDERR
