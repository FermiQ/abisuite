#!/bin/bash

MPIRUN="mpirun -np 4"
EXECUTABLE="/home/fgoudreault/Workspace/q-e/bin/epw.x -npool 4"
INPUT=/home/fgoudreault/Workspace/abisuite/abisuite/unittests/test_workflows/test_files/qe_Pb/epw_interpolation/epw_interpolation_run/epw_interpolation_run.in
LOG=/home/fgoudreault/Workspace/abisuite/abisuite/unittests/test_workflows/test_files/qe_Pb/epw_interpolation/epw_interpolation_run/epw_interpolation_run.log
STDERR=/home/fgoudreault/Workspace/abisuite/abisuite/unittests/test_workflows/test_files/qe_Pb/epw_interpolation/epw_interpolation_run/epw_interpolation_run.stderr


$MPIRUN $EXECUTABLE -input $INPUT > $LOG 2> $STDERR
