set style data dots
set nokey
set xrange [0: 6.49674]
set yrange [ -1.15520 : 20.73240]
set arrow from  1.29982,  -1.15520 to  1.29982,  20.73240 nohead
set arrow from  1.94974,  -1.15520 to  1.94974,  20.73240 nohead
set arrow from  2.86885,  -1.15520 to  2.86885,  20.73240 nohead
set arrow from  3.66483,  -1.15520 to  3.66483,  20.73240 nohead
set arrow from  5.04350,  -1.15520 to  5.04350,  20.73240 nohead
set xtics ("G"  0.00000,"X"  1.29982,"W"  1.94974,"L"  2.86885,"K"  3.66483,"G"  5.04350,"W"  6.49674)
 plot "epw_interpolation_run_band.dat"
