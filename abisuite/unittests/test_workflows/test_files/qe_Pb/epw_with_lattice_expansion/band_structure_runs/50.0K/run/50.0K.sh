#!/bin/bash

MPIRUN="mpirun -np 4"
EXECUTABLE="/home/fgoudreault/Workspace/q-e/bin/pw.x -npool 4"
INPUT=/home/fgoudreault/Workspace/abisuite/abisuite/unittests/test_workflows/test_files/qe_Pb/epw_with_lattice_expansion/band_structure_runs/50.0K/50.0K.in
LOG=/home/fgoudreault/Workspace/abisuite/abisuite/unittests/test_workflows/test_files/qe_Pb/epw_with_lattice_expansion/band_structure_runs/50.0K/50.0K.log
STDERR=/home/fgoudreault/Workspace/abisuite/abisuite/unittests/test_workflows/test_files/qe_Pb/epw_with_lattice_expansion/band_structure_runs/50.0K/50.0K.stderr


$MPIRUN $EXECUTABLE -input $INPUT > $LOG 2> $STDERR
