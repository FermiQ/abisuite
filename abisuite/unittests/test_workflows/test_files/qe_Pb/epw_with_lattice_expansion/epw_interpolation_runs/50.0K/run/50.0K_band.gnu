set style data dots
set nokey
set xrange [0: 6.39039]
set yrange [  1.01293 : 19.56389]
set arrow from  1.27854,   1.01293 to  1.27854,  19.56389 nohead
set arrow from  1.91782,   1.01293 to  1.91782,  19.56389 nohead
set arrow from  2.82188,   1.01293 to  2.82188,  19.56389 nohead
set arrow from  3.60483,   1.01293 to  3.60483,  19.56389 nohead
set arrow from  4.96093,   1.01293 to  4.96093,  19.56389 nohead
set xtics ("G"  0.00000,"X"  1.27854,"W"  1.91782,"L"  2.82188,"K"  3.60483,"G"  4.96093,"W"  6.39039)
 plot "50.0K_band.dat"
