set style data dots
set nokey
set xrange [0: 6.37937]
set yrange [  0.69436 : 19.52418]
set arrow from  1.27634,   0.69436 to  1.27634,  19.52418 nohead
set arrow from  1.91451,   0.69436 to  1.91451,  19.52418 nohead
set arrow from  2.81702,   0.69436 to  2.81702,  19.52418 nohead
set arrow from  3.59861,   0.69436 to  3.59861,  19.52418 nohead
set arrow from  4.95237,   0.69436 to  4.95237,  19.52418 nohead
set xtics ("G"  0.00000,"X"  1.27634,"W"  1.91451,"L"  2.81702,"K"  3.59861,"G"  4.95237,"W"  6.37937)
 plot "300.0K_band.dat"
