set style data dots
set nokey
set xrange [0: 6.36640]
set yrange [ -0.40567 : 19.32541]
set arrow from  1.27375,  -0.40567 to  1.27375,  19.32541 nohead
set arrow from  1.91062,  -0.40567 to  1.91062,  19.32541 nohead
set arrow from  2.81129,  -0.40567 to  2.81129,  19.32541 nohead
set arrow from  3.59130,  -0.40567 to  3.59130,  19.32541 nohead
set arrow from  4.94231,  -0.40567 to  4.94231,  19.32541 nohead
set xtics ("G"  0.00000,"X"  1.27375,"W"  1.91062,"L"  2.81129,"K"  3.59130,"G"  4.94231,"W"  6.36640)
 plot "600.0K_band.dat"
