set style data dots
set nokey
set xrange [0: 6.38607]
set yrange [  0.83928 : 19.52736]
set arrow from  1.27768,   0.83928 to  1.27768,  19.52736 nohead
set arrow from  1.91652,   0.83928 to  1.91652,  19.52736 nohead
set arrow from  2.81998,   0.83928 to  2.81998,  19.52736 nohead
set arrow from  3.60240,   0.83928 to  3.60240,  19.52736 nohead
set arrow from  4.95758,   0.83928 to  4.95758,  19.52736 nohead
set xtics ("G"  0.00000,"X"  1.27768,"W"  1.91652,"L"  2.81998,"K"  3.60240,"G"  4.95758,"W"  6.38607)
 plot "150.0K_band.dat"
