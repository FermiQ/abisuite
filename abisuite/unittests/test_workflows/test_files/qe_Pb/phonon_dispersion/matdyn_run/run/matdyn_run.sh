#!/bin/bash

MPIRUN=""
EXECUTABLE="/home/fgoudreault/Workspace/q-e/bin/matdyn.x"
INPUT=/home/fgoudreault/Workspace/abisuite/abisuite/unittests/test_workflows/test_files/qe_Pb/phonon_dispersion/matdyn_run/matdyn_run.in
LOG=/home/fgoudreault/Workspace/abisuite/abisuite/unittests/test_workflows/test_files/qe_Pb/phonon_dispersion/matdyn_run/matdyn_run.log
STDERR=/home/fgoudreault/Workspace/abisuite/abisuite/unittests/test_workflows/test_files/qe_Pb/phonon_dispersion/matdyn_run/matdyn_run.stderr


$MPIRUN $EXECUTABLE -input $INPUT > $LOG 2> $STDERR
