#!/bin/bash

MPIRUN="mpirun -np 4"
EXECUTABLE="/home/fgoudreault/Workspace/q-e/bin/ph.x -npool 4"
INPUT=/home/fgoudreault/Workspace/abisuite/abisuite/unittests/test_workflows/test_files/qe_Pb/phonon_smearing_convergence/phonons_runs/degauss0.1/k_points2_2_2/k_points2_2_2.in
LOG=/home/fgoudreault/Workspace/abisuite/abisuite/unittests/test_workflows/test_files/qe_Pb/phonon_smearing_convergence/phonons_runs/degauss0.1/k_points2_2_2/k_points2_2_2.log
STDERR=/home/fgoudreault/Workspace/abisuite/abisuite/unittests/test_workflows/test_files/qe_Pb/phonon_smearing_convergence/phonons_runs/degauss0.1/k_points2_2_2/k_points2_2_2.stderr


$MPIRUN $EXECUTABLE -input $INPUT > $LOG 2> $STDERR
