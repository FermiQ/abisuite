#!/bin/bash

MPIRUN=""
EXECUTABLE="/home/fgoudreault/Workspace/q-e/bin/q2r.x"
INPUT=/home/fgoudreault/Workspace/abisuite/abisuite/unittests/test_workflows/test_files/qe_Pb/lattice_expansion/q2r_runs/q2r_vol+4percent/q2r_vol+4percent.in
LOG=/home/fgoudreault/Workspace/abisuite/abisuite/unittests/test_workflows/test_files/qe_Pb/lattice_expansion/q2r_runs/q2r_vol+4percent/q2r_vol+4percent.log
STDERR=/home/fgoudreault/Workspace/abisuite/abisuite/unittests/test_workflows/test_files/qe_Pb/lattice_expansion/q2r_runs/q2r_vol+4percent/q2r_vol+4percent.stderr


$MPIRUN $EXECUTABLE -input $INPUT > $LOG 2> $STDERR
