#!/bin/bash

MPIRUN=""
EXECUTABLE="/home/fgoudreault/Workspace/q-e/bin/q2r.x"
INPUT=/home/fgoudreault/Workspace/abisuite/abisuite/unittests/test_workflows/test_files/qe_Pb/lattice_expansion/q2r_runs/q2r_vol-6percent/q2r_vol-6percent.in
LOG=/home/fgoudreault/Workspace/abisuite/abisuite/unittests/test_workflows/test_files/qe_Pb/lattice_expansion/q2r_runs/q2r_vol-6percent/q2r_vol-6percent.log
STDERR=/home/fgoudreault/Workspace/abisuite/abisuite/unittests/test_workflows/test_files/qe_Pb/lattice_expansion/q2r_runs/q2r_vol-6percent/q2r_vol-6percent.stderr


$MPIRUN $EXECUTABLE -input $INPUT > $LOG 2> $STDERR
