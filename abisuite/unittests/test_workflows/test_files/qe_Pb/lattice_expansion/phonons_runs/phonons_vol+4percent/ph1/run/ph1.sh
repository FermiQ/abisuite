#!/bin/bash

MPIRUN="mpirun -np 4"
EXECUTABLE="/home/fgoudreault/Workspace/q-e/bin/ph.x -npool 4"
INPUT=/home/fgoudreault/Workspace/abisuite/abisuite/unittests/test_workflows/test_files/qe_Pb/lattice_expansion/phonons_runs/phonons_vol+4percent/ph1/ph1.in
LOG=/home/fgoudreault/Workspace/abisuite/abisuite/unittests/test_workflows/test_files/qe_Pb/lattice_expansion/phonons_runs/phonons_vol+4percent/ph1/ph1.log
STDERR=/home/fgoudreault/Workspace/abisuite/abisuite/unittests/test_workflows/test_files/qe_Pb/lattice_expansion/phonons_runs/phonons_vol+4percent/ph1/ph1.stderr


$MPIRUN $EXECUTABLE -input $INPUT > $LOG 2> $STDERR
