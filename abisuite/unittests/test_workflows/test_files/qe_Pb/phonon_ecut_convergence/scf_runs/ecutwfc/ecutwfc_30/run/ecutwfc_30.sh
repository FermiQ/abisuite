#!/bin/bash

MPIRUN="mpirun -np 4"
EXECUTABLE="/home/fgoudreault/Workspace/q-e/bin/pw.x -npool 4"
INPUT=/home/fgoudreault/Workspace/abisuite/abisuite/unittests/test_workflows/test_files/qe_Pb/phonon_ecut_convergence/scf_runs/ecutwfc/ecutwfc_30/ecutwfc_30.in
LOG=/home/fgoudreault/Workspace/abisuite/abisuite/unittests/test_workflows/test_files/qe_Pb/phonon_ecut_convergence/scf_runs/ecutwfc/ecutwfc_30/ecutwfc_30.log
STDERR=/home/fgoudreault/Workspace/abisuite/abisuite/unittests/test_workflows/test_files/qe_Pb/phonon_ecut_convergence/scf_runs/ecutwfc/ecutwfc_30/ecutwfc_30.stderr


$MPIRUN $EXECUTABLE -input $INPUT > $LOG 2> $STDERR
