#!/bin/bash

MPIRUN="mpirun -np 4"
EXECUTABLE="/home/fgoudreault/Workspace/abinit/abinit/build/develop/2021-04-16/src/98_main/abinit"
INPUT=/home/fgoudreault/Workspace/abisuite/abisuite/unittests/test_workflows/test_files/abinit_si/paw/gs_ecut_convergence/pawecutdg/pawecutdg_30/run/pawecutdg_30.files
LOG=/home/fgoudreault/Workspace/abisuite/abisuite/unittests/test_workflows/test_files/abinit_si/paw/gs_ecut_convergence/pawecutdg/pawecutdg_30/pawecutdg_30.log
STDERR=/home/fgoudreault/Workspace/abisuite/abisuite/unittests/test_workflows/test_files/abinit_si/paw/gs_ecut_convergence/pawecutdg/pawecutdg_30/pawecutdg_30.stderr


$MPIRUN $EXECUTABLE < $INPUT > $LOG 2> $STDERR
