#!/bin/bash

MPIRUN="mpirun -np 4"
EXECUTABLE="/home/fgoudreault/Workspace/abinit/abinit/build/develop/2021-04-16/src/98_main/abinit"
INPUT=/home/fgoudreault/Workspace/abisuite/abisuite/unittests/test_workflows/test_files/abinit_si/paw/phonon_ecut_convergence/phonons_runs/pawecutdg/pawecutdg_10/run/pawecutdg_10.files
LOG=/home/fgoudreault/Workspace/abisuite/abisuite/unittests/test_workflows/test_files/abinit_si/paw/phonon_ecut_convergence/phonons_runs/pawecutdg/pawecutdg_10/pawecutdg_10.log
STDERR=/home/fgoudreault/Workspace/abisuite/abisuite/unittests/test_workflows/test_files/abinit_si/paw/phonon_ecut_convergence/phonons_runs/pawecutdg/pawecutdg_10/pawecutdg_10.stderr


$MPIRUN $EXECUTABLE < $INPUT > $LOG 2> $STDERR
