from abisuite import AbinitWorkflow


workflow = AbinitWorkflow(
        gs=True, band_structure=True, band_structure_convergence=True,
        optic=True, relaxation=True, gs_ecut_convergence=True,
        gs_kgrid_convergence=True,
        phonon_ecut_convergence=True,
        phonon_dispersion=True,
        pseudos=["../../pseudos/Si.psp8"],  # NC
        )

workflow.set_gs_ecut_convergence(
        root_workdir="ecut_convergence",
        scf_input_variables={
                "acell": [10.18] * 3,
                "rprim": [[0.0, 0.5, 0.5], [0.5, 0.0, 0.5], [0.5, 0.5, 0.0]],
                "ntypat": 1,
                "znucl": 14,
                "natom": 2,
                "typat": [1, 1],
                "xred": [[0.0, 0.0, 0.0], [1/4, 1/4, 1/4]],
                "kptopt": 1,
                "ngkpt": [2, 2, 2],
                "nshiftk": 4,
                "shiftk": [[0.5, 0.5, 0.5], [0.5, 0.0, 0.0],
                           [0.0, 0.5, 0.0], [0.0, 0.0, 0.5]],
                "nstep": 50,
                "tolvrs": 1.0e-10,
                "diemac": 12.0,
                },
        scf_calculation_parameters={
            "mpi_command": "mpirun -np 4",
            },
        scf_ecuts=[10, 20, 30],
        scf_convergence_criterion=1,
        plot_calculation_parameters={"show": False},
        )

workflow.set_gs_kgrid_convergence(
        scf_workdir="kgrid_convergence",
        scf_input_variables={
                "acell": [10.18] * 3,
                "rprim": [[0.0, 0.5, 0.5], [0.5, 0.0, 0.5], [0.5, 0.5, 0.0]],
                "ntypat": 1,
                "znucl": 14,
                "natom": 2,
                "typat": [1, 1],
                "xred": [[0.0, 0.0, 0.0], [1/4, 1/4, 1/4]],
                "kptopt": 1,
                "nshiftk": 4,
                "shiftk": [[0.5, 0.5, 0.5], [0.5, 0.0, 0.0],
                           [0.0, 0.5, 0.0], [0.0, 0.0, 0.5]],
                "nstep": 10,
                "toldfe": 1.0e-6,
                "diemac": 12.0,
                },
        scf_kgrids_input_variable_name="ngkpt",
        scf_kgrids=[[2, 2, 2], [4, 4, 4], [6, 6, 6], [8, 8, 8]],
        scf_calculation_parameters={
            "mpi_command": "mpirun -np 4",
            },
        scf_convergence_criterion=1,
        use_gs_converged_ecut=True,
        plot_calculation_parameters={"show": False},
        )

workflow.set_relaxation(
        root_workdir="relax_run",
        scf_input_variables={
                "acell": [10.18] * 3,
                "rprim": [[0.0, 0.5, 0.5], [0.5, 0.0, 0.5], [0.5, 0.5, 0.0]],
                "ntypat": 1,
                "znucl": 14,
                "natom": 2,
                "typat": [1, 1],
                "xred": [[0.0, 0.0, 0.0], [1/4, 1/4, 1/4]],
                "kptopt": 1,
                "nshiftk": 4,
                "shiftk": [[0.5, 0.5, 0.5], [0.5, 0.0, 0.0],
                           [0.0, 0.5, 0.0], [0.0, 0.0, 0.5]],
                "nstep": 10,
                "tolvrs": 1.0e-10,
                "diemac": 12.0,
                "optcell": 1,  # only modify acell
                "ionmov": 22,
                "ntime": 20,
                "tolmxf": 5e-04,
                "ecutsm": 0.5,
                "dilatmx": 1.1,
                },
        relax_atoms=True,
        relax_cell=True,
        scf_calculation_parameters={
            "mpi_command": "mpirun -np 4"},
        use_gs_converged_ecut=True,
        use_gs_converged_kgrid=True,
        )
workflow.set_phonon_ecut_convergence(
        root_workdir="phonon_ecut_convergence",
        compute_electric_field_response=True,
        ddk_input_variables={
            "rfatpol": [1, 2],
            "tolwfr": 1e-22,
            "nstep": 25,
            "diemac": 9.0,
            "rfdir": [1, 1, 1],
            },
        ddk_calculation_parameters={
            "mpi_command": "mpirun -np 4",
            },
        phonons_input_variables={
            "rfatpol": [1, 2],
            "rfdir": [1, 1, 1],
            "tolvrs": 1e-8,
            "diemac": 9.0,
            "nstep": 25,
            },
        scf_input_variables={
                "acell": [10.18] * 3,
                "rprim": [[0.0, 0.5, 0.5], [0.5, 0.0, 0.5], [0.5, 0.5, 0.0]],
                "ntypat": 1,
                "znucl": 14,
                "natom": 2,
                "typat": [1, 1],
                "xred": [[0.0, 0.0, 0.0], [1/4, 1/4, 1/4]],
                "kptopt": 1,
                "nshiftk": 4,
                "shiftk": [[0.5, 0.5, 0.5], [0.5, 0.0, 0.0],
                           [0.0, 0.5, 0.0], [0.0, 0.0, 0.5]],
                "nstep": 10,
                "toldfe": 1.0e-6,
                "diemac": 12.0,
                },
        scf_calculation_parameters={
            "mpi_command": "mpirun -np 4",
            },
        scf_ecuts=[10, 20, 30],
        phonons_convergence_criterion=10,  # cm^-1
        phonons_qpt=[0, 0, 0],  # test convergence at Gamma
        phonons_calculation_parameters={
            "mpi_command": "mpirun -np 4",
            },
        plot_calculation_parameters={"show": False},
        )

workflow.set_gs(
        scf_workdir="gs_run",
        scf_input_variables={
                "kptopt": 1,
                "nshiftk": 4,
                "shiftk": [[0.5, 0.5, 0.5], [0.5, 0.0, 0.0],
                           [0.0, 0.5, 0.0], [0.0, 0.0, 0.5]],
                "nstep": 10,
                "toldfe": 1.0e-6,
                "diemac": 12.0,
                },
        scf_calculation_parameters={
            "mpi_command": "mpirun -np 4"},
        use_phonon_converged_ecut=True,
        use_gs_converged_kgrid=True,
        use_relaxed_geometry=True,
        )
workflow.set_band_structure(
        root_workdir="band_structure",
        band_structure_input_variables={
            "iscf": -2,
            "nband": 8,
            "diemac": 12.0,
            "tolwfr": 1e-12,
            "autoparal": 1,
            },
        band_structure_calculation_parameters={
            "mpi_command": "mpirun -np 4",
            },
        band_structure_kpoint_path=[
            {"L": [0.5, 0.0, 0.0]},
            {r"$\Gamma$": [0.0, 0.0, 0.0]},
            {"X": [0.0, 0.5, 0.5]},
            {r"$\Gamma_2$": [1.0, 1.0, 1.0]}],
        band_structure_kpoint_path_density=20,
        fatbands=True,
        use_gs=True,
        plot_calculation_parameters={"show": False},
        )
workflow.set_band_structure_convergence(
        root_workdir="band_structure_ecut_convergence",
        scf_input_variables={
                "acell": [10.18] * 3,
                "kptopt": 1,
                "nshiftk": 4,
                "shiftk": [[0.5, 0.5, 0.5], [0.5, 0.0, 0.0],
                           [0.0, 0.5, 0.0], [0.0, 0.0, 0.5]],
                "nstep": 50,
                "tolvrs": 1.0e-10,
                "diemac": 12.0,
                },
        scf_specific_input_variables=[{"ecut": ecut} for ecut in [10, 30]],
        scf_calculation_parameters={
            "mpi_command": "mpirun -np 4"},
        plot_calculation_parameters={
            "show_bandgap": True, "adjust_axis": False, "show": False},
        band_structure_kpoint_path=[
            {"L": [0.5, 0.0, 0.0]},
            {r"$\Gamma$": [0.0, 0.0, 0.0]},
            {"X": [0.0, 0.5, 0.5]},
            {r"$\Gamma_2$": [1.0, 1.0, 1.0]}],
        band_structure_kpoint_path_density=20,
        band_structure_input_variables={
            "iscf": -2,
            "nband": 8,
            "diemac": 12.0,
            "tolwfr": 1e-12,
            "autoparal": 1,
            },
        band_structure_calculation_parameters={
            "mpi_command": "mpirun -np 4",
            },
        use_gs_converged_ecut=True,
        use_gs_converged_kgrid=True,
        use_relaxed_geometry=True,
        )
workflow.set_optic(
        root_workdir="optic",
        experimental_bandgap=1.14,
        compute_non_linear_optical_response=False,
        nscf_input_variables={
            "nstep": 20,
            "tolwfr": 1e-14, "ngkpt": [6, 6, 6], "nbdbuf": 2, "nband": 10},
        nscf_calculation_parameters={
            "mpi_command": "mpirun -np 4", "queuing_system": "local"},
        nscffbz_calculation_parameters={
            "mpi_command": "mpirun -np 4", "queuing_system": "local"},
        ddk_input_variables={"rfdir": [1, 1, 1]},
        ddk_calculation_parameters={
            "mpi_command": "mpirun -np 4", "queuing_system": "local"},
        optic_input_variables={
            "num_lin_comp": 6, "lin_comp": [11, 12, 13, 22, 23, 33],
            "num_nonlin_comp": 0, "num_nonlin2_comp": 0, "num_linel_comp": 0,
            "broadening": 1e-3, "domega": 0.1, "maxomega": 1,
            "tolerance": 1e-3},
        optic_calculation_parameters={
            "mpi_command": "mpirun -np 4", "queuing_system": "local"},
        use_gs=True,
        plot_calculation_parameters={"show": False},
        )
workflow.set_phonon_dispersion(
        root_workdir="phonon_dispersion",
        use_gs=True,
        compute_electric_field_response=True,
        phonons_input_variables={
            "rfatpol": [1, 1],
            "tolvrs": 1e-8,
            "diemac": 12.0,
            "nstep": 25,
            },
        phonons_qpoint_grid=[6, 6, 6],
        phonons_calculation_parameters={"mpi_command": "mpirun -np 4"},
        ddk_input_variables={
            "rfdir": [1, 1, 1],
            "tolwfr": 1e-16,
            "nstep": 25,
            "diemac": 12.0,
            },
        ddk_calculation_parameters={"mpi_command": "mpirun -np 4"},
        qpoint_path=[
            {"L": [0.5, 0.0, 0.0]},
            {r"$\Gamma$": [0.0, 0.0, 0.0]},
            {"X": [0.0, 0.5, 0.5]},
            {r"$\Gamma_2$": [1.0, 1.0, 1.0]}],
        qpoint_path_density=20,
        anaddb_input_variables={
            "ifcflag": 1, "ifcout": 0, "brav": 2, "ngqpt": [6, 6, 6],
            "q1shft": [0.0] * 3, "nqshft": 1, "chneut": 1, "dipdip": 1,
            "eivec": 4, "nph2l": 1, "qph2l": [1.0, 0.0, 0.0, 0.0],
            },
        anaddb_calculation_parameters={},
        )
workflow.run()
