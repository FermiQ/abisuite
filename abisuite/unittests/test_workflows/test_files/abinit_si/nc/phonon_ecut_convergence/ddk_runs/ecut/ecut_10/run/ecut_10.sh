#!/bin/bash

MPIRUN="mpirun -np 4"
EXECUTABLE="/home/fgoudreault/Workspace/abinit/abinit/build/develop/2021-04-16/src/98_main/abinit"
INPUT=/home/fgoudreault/Workspace/abisuite/abisuite/unittests/test_workflows/test_files/abinit_si/nc/phonon_ecut_convergence/ddk_runs/ecut/ecut_10/run/ecut_10.files
LOG=/home/fgoudreault/Workspace/abisuite/abisuite/unittests/test_workflows/test_files/abinit_si/nc/phonon_ecut_convergence/ddk_runs/ecut/ecut_10/ecut_10.log
STDERR=/home/fgoudreault/Workspace/abisuite/abisuite/unittests/test_workflows/test_files/abinit_si/nc/phonon_ecut_convergence/ddk_runs/ecut/ecut_10/ecut_10.stderr


$MPIRUN $EXECUTABLE < $INPUT > $LOG 2> $STDERR
