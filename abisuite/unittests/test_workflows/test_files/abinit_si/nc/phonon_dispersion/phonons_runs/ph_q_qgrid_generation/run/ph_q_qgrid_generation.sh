#!/bin/bash

MPIRUN=""
EXECUTABLE="/home/fgoudreault/Workspace/abinit/abinit/build/develop/2021-04-20/src/98_main/abinit"
INPUT=/home/fgoudreault/Workspace/abisuite/abisuite/unittests/test_workflows/test_files/abinit_si/nc/phonon_dispersion/phonons_runs/ph_q_qgrid_generation/run/ph_q_qgrid_generation.files
LOG=/home/fgoudreault/Workspace/abisuite/abisuite/unittests/test_workflows/test_files/abinit_si/nc/phonon_dispersion/phonons_runs/ph_q_qgrid_generation/ph_q_qgrid_generation.log
STDERR=/home/fgoudreault/Workspace/abisuite/abisuite/unittests/test_workflows/test_files/abinit_si/nc/phonon_dispersion/phonons_runs/ph_q_qgrid_generation/ph_q_qgrid_generation.stderr


$MPIRUN $EXECUTABLE < $INPUT > $LOG 2> $STDERR
