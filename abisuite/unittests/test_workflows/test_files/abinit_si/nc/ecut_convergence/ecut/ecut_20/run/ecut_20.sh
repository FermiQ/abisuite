#!/bin/bash

MPIRUN="mpirun -np 4"
EXECUTABLE="/home/fgoudreault/Workspace/abinit/abinit/build/develop/2021-04-16/src/98_main/abinit"
INPUT=/home/fgoudreault/Workspace/abisuite/abisuite/unittests/test_workflows/test_files/abinit_si/nc/ecut_convergence/ecut/ecut_20/run/ecut_20.files
LOG=/home/fgoudreault/Workspace/abisuite/abisuite/unittests/test_workflows/test_files/abinit_si/nc/ecut_convergence/ecut/ecut_20/ecut_20.log
STDERR=/home/fgoudreault/Workspace/abisuite/abisuite/unittests/test_workflows/test_files/abinit_si/nc/ecut_convergence/ecut/ecut_20/ecut_20.stderr


$MPIRUN $EXECUTABLE < $INPUT > $LOG 2> $STDERR
