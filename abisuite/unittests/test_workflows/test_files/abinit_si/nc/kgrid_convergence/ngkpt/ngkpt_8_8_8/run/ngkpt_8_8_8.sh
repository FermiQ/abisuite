#!/bin/bash

MPIRUN="mpirun -np 4"
EXECUTABLE="/home/fgoudreault/Workspace/abinit/abinit/build/develop/2021-04-16/src/98_main/abinit"
INPUT=/home/fgoudreault/Workspace/abisuite/abisuite/unittests/test_workflows/test_files/abinit_si/nc/kgrid_convergence/ngkpt/ngkpt_8_8_8/run/ngkpt_8_8_8.files
LOG=/home/fgoudreault/Workspace/abisuite/abisuite/unittests/test_workflows/test_files/abinit_si/nc/kgrid_convergence/ngkpt/ngkpt_8_8_8/ngkpt_8_8_8.log
STDERR=/home/fgoudreault/Workspace/abisuite/abisuite/unittests/test_workflows/test_files/abinit_si/nc/kgrid_convergence/ngkpt/ngkpt_8_8_8/ngkpt_8_8_8.stderr


$MPIRUN $EXECUTABLE < $INPUT > $LOG 2> $STDERR
