import os


# #############################################################################
# ############################## COMMON(WEALTH) ###############################
# #############################################################################


BASIC_MPI_VARIABLES = {"ppn": 12,
                       "mpi_command": "mpirun -npernode 12",
                       "nodes": "3:m48G"}
here = os.path.dirname(os.path.abspath(__file__))
pseudo_dir = os.path.join(here, "files/pseudos")


# #############################################################################
# ################################# ABINIT ####################################
# #############################################################################


Hpseudo = os.path.join(here, "pseudos", "01h.pspgth")
ddk1 = os.path.join(here, "../../examples/example_data/odat_example_1WF7")
ddk2 = os.path.join(here, "../../examples/example_data/odat_example_1WF8")
ddk3 = os.path.join(here, "../../examples/example_data/odat_example_1WF9")
wfk = os.path.join(here, "../../examples/example_data/odat_example_WFK")
abinit_optic_vars = {
        "ddkfile_1": ddk1,
        "ddkfile_2": ddk2,
        "ddkfile_3": ddk3,
        "wfkfile": wfk,
        "broadening": 0.002,
        "maxomega": 0.3,
        "domega": 0.0003,
        "tolerance": 0.002,
        "num_lin_comp": 1,
        "lin_comp": 11,
        "num_nonlin_comp": 2,
        "nonlin_comp": [123, 222],
        "num_linel_comp": 0,
        "num_nonlin2_comp": 0,
        }

# Abinit GS example (AlAs)
abinit_vars = {
        "nband": 4, "ecut": 3.0, "ngkpt": [4, 4, 4], "nshiftk": 1,
        "shiftk": ([0.5, 0.5, 0.5], ),
        "ixc": 1,
        "diemac": 9.0, "nstep": 25, "tolvrs": 1e-18, "acell": [10.61] * 3,
        "natom": 2, "ntypat": 2,
        "rprim": [[0.0, 0.5, 0.5], [0.5, 0.0, 0.5], [0.5, 0.5, 0.0]],
        "typat": [1, 2],
        "xred": [[0.0, 0.0, 0.0], [0.25, 0.25, 0.25]],
        "znucl": [13, 33],
        }
abinit_AlAs_pseudos = [
        os.path.join(here, "files", "pseudos", "13al.981214.fhi"),
        os.path.join(here, "files", "pseudos", "33as.pspnc"),
        ]
AbinitCalc = os.path.join(here, "files/abinit_AlAs/gs")
abinit_out_example = os.path.join(AbinitCalc, "gs.out")


# optic lincomp file example
abinit_optic_lincomp_example = os.path.join(here,
                                            ("../../examples/parsers/"
                                             "abinit_parsers/"
                                             "optic_lin_comp_parser/"
                                             "optic_lin_comp_"
                                             "example.out"))
# DMFT file examples
abinit_dmft_projectors_example = os.path.join(here,
                                              ("../../examples/parsers/"
                                               "abinit_parsers/"
                                               "dmft_parsers/projectors/"
                                               "projectors_example.ovlp"))
abinit_dmft_hamiltonian_example = os.path.join(
        here,
        ("../../examples/parsers/abinit_parsers/dmft_parsers/hamiltonian/"
         "forlb.eig")
        )

# anaddb input variables example (based on trf2)
abinit_anaddb_vars = {
        "ifcflag": 1,
        "brav": 2,
        "ngqpt": [4, 4, 4],
        "nqshft": 1,
        "q1shft": [0.0, 0.0, 0.0],
        "chneut": 1,
        "dipdip": 1,
        "ifcana": 1,
        "ifcout": 20,
        "natifc": 1,
        "atifc": 1,
        }
AbinitAnaddbCalc = os.path.join(here, "files/abinit_AlAs/anaddb")
abinit_anaddb_log_example = os.path.join(AbinitAnaddbCalc, "anaddb.log")
abinit_anaddb_phfrq_example = os.path.join(
        AbinitAnaddbCalc, "anaddb.out_PHFRQ")

# mrgddb calculation example
AbinitMrgddbCalc = os.path.join(here, "files/abinit_AlAs/mrgddb")
abinit_mrgddb_log_example = os.path.join(AbinitMrgddbCalc, "mrgddb.log")


# #############################################################################
# ########################## QUANTUM ESPRESSO #################################
# #############################################################################

# very simple QE examples
# QE dos.x example
qe_dosx_vars = {
        "ngauss": 0,
        "degauss": 0.005,
        "Emin": -10.0,
        "Emax": 30.0,
        "DeltaE": 0.01,
        "outdir": "test_outdir",
        "prefix": "test_prefix",
        "fildos": "test_fildos",
        }

QEDOSCalc = os.path.join(here, "files", "qe_silicon", "dos_run")
QEDOSDOSExample = os.path.join(
        here, "files", "qe_silicon", "dos_run", "run", "output_data",
        "dos.dos"
        )
QEDOSLogExample = os.path.join(
        here, "files", "qe_silicon", "dos_run", "dos.log"
        )


# simple dynmat calc
qe_dynmatx_vars = {"asr": "simple",
                   "fildyn": "test_fildyn",
                   "lperm": True,
                   "q(1)": 1.0,
                   "q(2)": 0.0,
                   "q(3)": 0.0}


# QE epsilon.x example
qe_epsilonx_vars = {
        "calculation": "eps",
        "outdir": "test_outdir",
        "prefix": "test_prefix",
        "smeartype": "gauss",
        "intersmear": 0.02,
        "wmin": 0.0,
        "wmax": 30.0,
        "nw": 500,
        }

# QE epw.x example
qe_epwx_vars = {
        "amass(1)": 207.2,
        "elph": True,
        "kmaps": False,
        "epbwrite": True,
        "epbread": False,
        "epwwrite": True,
        "epwread": False,
        "nbndsub": 4,
        "bands_skipped": "exclude_bands = 1:5",
        "wannierize": True,
        "num_iter": 300,
        "dis_win_max": 21,
        "dis_win_min": -3,
        "dis_froz_min": -3,
        "dis_froz_max": 13.5,
        "proj(1)": "PB:sp3",
        "wdata(1)": "bands_plot = True",
        "wdata(2)": "begin kpoint_path",
        "wdata(3)": "G 0.0 0.0 0.0 X 0.0 0.5 0.5",
        "wdata(4)": "X 0.0 0.5 0.5 W 0.25 0.5 0.75",
        "wdata(5)": "W 0.25 0.5 0.75 L 0.5 0.5 0.5",
        "wdata(6)": "L 0.5 0.5 0.5 K 0.375 0.375 0.75",
        "wdata(7)": "K 0.375 0.375 0.75 G 0.0 0.0 0.0",
        "wdata(8)": "G 0.0 0.0 0.0 L 0.5 0.5 0.5",
        "wdata(9)": "end kpoint_path",
        "wdata(10)": "bands_plot_format = gnuplot",
        "iverbosity": 0,
        "elecselfen": False,
        "phonselfen": True,
        "fsthick": 6,
        "degaussw": 0.1,
        "a2f": True,
        "nkf1": 10,
        "nkf2": 10,
        "nkf3": 10,
        "nqf1": 10,
        "nqf2": 10,
        "nqf3": 10,
        "nk1": 8,
        "nk2": 8,
        "nk3": 8,
        "title": "epw",
        "q_points": {
            "parameter": "cartesian",
            "q_points": [[0.0, 0.0, 0.0], [0.5, -0.5, 0.5], [0.0, -1.0, 0.0]],
            "weights": [1, 1, 1],
            },
        "prefix": "epw_test",
        "outdir": "outdir_test",
        }
QEEPWCalc = os.path.join(here, "test_sequencers/files/qe_pb/epw_run")

# QE fs.x example
qe_fsx_vars = {
        "outdir": "test_outdir",
        "prefix": "test_prefix",
        }

# QE ld1.x example
qe_ld1x_vars = {
        "atom": "Ti",
        "dft": "PBE",
        "config": "[Ar] 3d2 4s2 4p0",
        "iswitch": 2,
        "file_pseudo": "Ti_pseudo_file.UPF",
        "nconf": 1,
        "configts(1)": "3d2 4s2 4p0",
        "ecutmin": 50,
        "ecutmax": 200,
        "decut": 50,
        }

# simple matdyn calc for silicon
qe_matdynx_vars = {
        "asr": "simple",
        "flfrc": "test_flfrc",
        "flfrq": "test_flfrq",
        "q_in_band_form": True,
        "q_points": {"qpts": [[0.5, 0.5, 0.5],
                              [0.0, 0.0, 0.0],
                              [1.0, 0.0, 0.0]],
                     "nqpts": [20, 20, 20]},
        }
QEMatdynCalc = os.path.join(here, "files/qe_silicon/matdyn_run")
QEMatdynfreq = os.path.join(QEMatdynCalc, "run/output_data/matdyn.freq")


# simple ph calc for silicon
qe_phx_vars = {
        "amass(1)": 28.086,
        "q_points": [0.0, 0.0, 0.0],
        "title": "silicon",
        "fildyn": "test_fildyn",
        "fildvscf": "dvscf",
        "outdir": "test_outfir",
        "prefix": "test_prefix",
        }
QEPHCalc = os.path.join(here, "files/qe_silicon/ph_run")
qe_phx_dyn0_example = os.path.join(
        QEPHCalc, "run", "output_data", "ph_run.dyn0")
qe_phx_output_data_dir_example = os.path.join(QEPHCalc, "run", "output_data")
qe_phxlog_example = os.path.join(here, "files/qe_silicon/ph_run/ph_run.log")


# simple pp.x example
qe_ppx_vars = {
        "plot_num": 7,
        "lsign": True,
        "kpoint": 21,
        "kband": 4,
        "outdir": "test_outdir",
        "prefix": "test_prefix",
        "filplot": "test_filplot",
        "iflag": 3,
        "output_format": 5,
        "fileout": "test_fileout",
        }

# simple projwfc.x example
qe_projwfcx_vars = {
        "ngauss": 0,
        "degauss": 0.005,
        "Emin": -10.0,
        "Emax": 30.0,
        "DeltaE": 0.001,
        "filpdos": "test_filpdos",
        "filproj": "test_filproj",
        "outdir": "test_outdir",
        "prefix": "test_prefix",
        }

QEProjwfcCalc = os.path.join(here, "files", "qe_silicon", "pdos_run")
QEProjwfcPDOSExample = os.path.join(
        here, "files", "qe_silicon", "pdos_run", "run", "output_data",
        "pdos.pdos_atm#1(Si)_wfc#2(p)"
        )
QEProjwfcLogExample = os.path.join(
        here, "files", "qe_silicon", "pdos_run", "pdos.log")


# QE pw.x example
qe_pwx_vars = {
        "calculation": "scf",
        "ibrav": 2,
        "prefix": "test_prefix",
        "outdir": "test_outdir",
        "pseudo_dir": pseudo_dir,
        "celldm(1)": 10.28,
        "nat": 2,
        "ntyp": 1,
        "ecutwfc": 18.0,
        "atomic_species": [{"atom": "Si", "atomic_mass": 28.086,
                            "pseudo": "Si.upf"}],
        "atomic_positions": {"parameter": "alat",
                             "positions": {"Si": [[0.00, 0.00, 0.00],
                                                  [0.25, 0.25, 0.25]],
                                           }},
        "k_points": {"parameter": "automatic",
                     "k_points": [4, 4, 4, 1, 1, 1]
                     }
        }

QEPWCalc = os.path.join(here, "files/qe_silicon/pw_run")
qe_pwxlog_example = os.path.join(here, ("files/qe_silicon/pw_run/pw.log"))
QEPWNscfPositiveKptsCalc = os.path.join(
        here, "files", "qe_silicon", "pw_run_nscf_positive_ks")
qe_pwx_output_data_dir_example = os.path.join(QEPWCalc, "run", "output_data")
PWCalc = QEPWCalc  # legacy + lot of laziness haha

# QE pw.x bandstructure vars example
qe_pwx_bandstructure_vars = {
        "calculation": "bands",
        "nbnd": 10,
        "ecutwfc": 50.0,
        "k_points": {"parameter": "crystal_b",
                     "nks": 2,
                     "k_points": [[0.0, 0.0, 0.0], [-0.5, 0.0, 0.0]],
                     "weights": [10, 10],
                     },
        "verbosity": "high",
        }
QEPWBandCalc = os.path.join(here, "files/qe_silicon/bandstructure_run")
QEPWBandCalclog = os.path.join(QEPWBandCalc, "bandstructure.log")

# QE pw2wannier90.x example
qe_pw2wannier90x_vars = {
        "outdir": "test_outdir",
        "prefix": "test_prefix",
        "seedname": "test_seedname",
        "spin_component": "none",
        "write_mmn": True,
        "write_amn": True,
        "write_unk": True,
        "reduce_unk": True,
        }

# simple q2r calc for silicon
qe_q2rx_vars = {
        "fildyn": "test_fildyn",
        "flfrc": "test_flfrc",
        }
QEQ2RCalc = os.path.join(here, "files/qe_silicon/q2r_run")


# TODO: to remove (below)??
# Already made calculations
POSITIVE_KS_NSCF_CALC = os.path.join(
        here, "files", "qe_silicon",
        "pw_run_nscf_positive_ks")

# #############################################################################
# ############################# Wannier 90 ####################################
# #############################################################################


wannier90_vars = {
        "num_bands": 16,
        "num_wann": 8,
        "num_iter": 200,
        "conv_window": 3,
        "dis_win_max": 17.0,
        "dis_froz_max": 6.5,
        "dis_mix_ratio": 1.0,
        "bands_plot": True,
        "guiding_centres": True,
        "dis_num_iter": 1000,
        "projections": ["Si:sp3"],
        "mp_grid": [4] * 3,
        "atomic_positions": {
            "parameter": "atoms_frac",
            "positions": {"Si": [[0.0, 0.0, 0.0],
                                 [0.25, 0.25, 0.25]]}},
        "unit_cell": {
            "parameter": "unit_cell_cart",
            "vectors": [[-5.14, 0.0, 5.14],
                        [0.0, 5.14, 5.14],
                        [-5.14, 5.14, 0.0]], },
        }

Gamma = [0.0, 0.0, 0.0]
X = [-0.5, 0.0, 0.0]  # [-0.5, 1.0, 0.0]
W = [-0.75, -0.5, -0.25]  # [-0.75, 1.5, -0.25]
K = [-0.75, -0.5, -3/8]  # [-0.75, 1.5, -3/8]
L = [-0.5, -0.5, -0.5]  # [-0.5, 1.5, -0.5]
U = [-5/8, -0.5, -0.25]  # [-5/8, 1.5, -0.25]

# PATH
kpts = [Gamma, X, W, K, Gamma, L, U, W, L, K, U, X]
labels = ["G", "X", "W", "K", "G", "L", "U", "W", "L", "K", "U", "X"]
kpt_path = []
Npts = 4
for kpt1, kpt2, label1, label2 in zip(
        kpts[:-1], kpts[1:], labels[:-1], labels[1:]):
    kpt_path.append([{label1: kpt1}, {label2: kpt2}])

wannier90_vars["kpoint_path"] = kpt_path
for i in range(Npts):
    for j in range(Npts):
        for k in range(Npts):
            wannier90_vars.setdefault("kpoints", [])
            wannier90_vars["kpoints"].append([i / Npts, j / Npts, k / Npts])
