#!/bin/bash

MPIRUN="mpirun -np 4"
EXECUTABLE="/home/fgoudreault/Workspace/q-e/bin/ph.x -npool 4"
INPUT=/home/fgoudreault/Workspace/abisuite/abisuite/unittests/files/qe_silicon/ph_run/ph_run.in
LOG=/home/fgoudreault/Workspace/abisuite/abisuite/unittests/files/qe_silicon/ph_run/ph_run.log
STDERR=/home/fgoudreault/Workspace/abisuite/abisuite/unittests/files/qe_silicon/ph_run/ph_run.stderr


$MPIRUN $EXECUTABLE < $INPUT > $LOG 2> $STDERR
