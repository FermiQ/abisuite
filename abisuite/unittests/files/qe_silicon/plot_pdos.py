from abisuite import MassDOSPlot


path = "pdos_run"
dos = MassDOSPlot.from_calculation(path, atoms="Si", atom_nos=1)
plot = dos.get_plot(
        vertical=True,
        xlabel="DOS",
        xunits="states/eV",
        ylabel="Energy",
        yunits="eV",
        )
plot.curves_color = ["r", "b", "k", "g"]
plot.plot()
