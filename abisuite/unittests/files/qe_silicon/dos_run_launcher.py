from abisuite import QEDOSLauncher as Launcher


variables = {
        "ngauss": 0,
        "degauss": 0.05,
        "Emin": -20,
        "Emax": 20,
        "DeltaE": 0.1,
        }

launcher = Launcher("dos")
launcher.workdir = "dos_run"
launcher.input_variables = variables
launcher.link_calculation("pw_run_nscf")
launcher.write()
launcher.run()
