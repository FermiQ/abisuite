#!/bin/bash

MPIRUN=""
EXECUTABLE="/home/fgoudreault/Workspace/q-e/bin/dos.x"
INPUT=/home/fgoudreault/Workspace/abisuite/abisuite/unittests/files/qe_silicon/dos_run/dos.in
LOG=/home/fgoudreault/Workspace/abisuite/abisuite/unittests/files/qe_silicon/dos_run/dos.log
STDERR=/home/fgoudreault/Workspace/abisuite/abisuite/unittests/files/qe_silicon/dos_run/dos.stderr


$MPIRUN $EXECUTABLE < $INPUT > $LOG 2> $STDERR
