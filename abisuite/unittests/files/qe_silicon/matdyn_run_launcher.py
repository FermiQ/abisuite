from abisuite import QEMatdynLauncher as Launcher


# PATH POINTS
Gamma = [0.0, 0.0, 0.0]
X = [-0.5, 0.0, 0.0]  # [-0.5, 1.0, 0.0]
W = [-0.75, -0.5, -0.25]  # [-0.75, 1.5, -0.25]
K = [-0.75, -0.5, -3/8]  # [-0.75, 1.5, -3/8]
L = [-0.5, -0.5, -0.5]  # [-0.5, 1.5, -0.5]
U = [-5/8, -0.5, -0.25]  # [-5/8, 1.5, -0.25]

# PATH
qpts = [Gamma, X, W, K, Gamma, L, U, W, L, K, U, X]
Npts = 20

variables = {
        "asr": "simple",
        "q_in_band_form": True,
        "q_points": {
            "qpts": qpts,
            "nqpts": [Npts] * len(qpts)},
        }

launcher = Launcher("matdyn")
launcher.workdir = "matdyn_run"
launcher.input_variables = variables
launcher.link_calculation("q2r_run")
launcher.command_arguments = "-npool 4"
launcher.mpi_command = "mpirun -np 4"
launcher.write()
launcher.run()
