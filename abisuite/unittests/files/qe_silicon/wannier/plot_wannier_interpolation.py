from abisuite import BandStructure


k_path = [r"$\Gamma$", "X", "W", "K", r"$\Gamma$",
          "L", "U", "W", "L", "K", "W", "X"]
npts = 20
k_labels = [(label, coordinate)
            for label, coordinate in zip(k_path,
                                         range(0, len(k_path) * (npts + 1),
                                               npts))]

path = "../bandstructure_run"
bs = BandStructure.from_calculation(path)
plot = bs.get_plot(ylabel="Energy",
                   yunits="eV",
                   symmetry="none",
                   linewidth=2,
                   color="k",
                   all_k_labels=k_labels)
plot.curves[0].label = "dft"
# plot.plot()
wannier_bs = BandStructure.from_calculation("wannier_run")
plot_wannier = wannier_bs.get_plot(
        ylabel="Energy",
        yunits="eV",
        symmetry="none",
        all_k_labels="none",
        linewidth=2,
        color="r")
plot_wannier.curves[0].label = "wannier"
final_plot = plot + plot_wannier
final_plot.plot()
