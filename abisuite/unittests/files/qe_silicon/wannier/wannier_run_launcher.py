from abisuite import Wannier90Launcher as Launcher


# wannier
launcher = Launcher("wannier_run")
launcher.workdir = "wannier_run"
launcher.link_calculation("pp_wannier_run")
launcher.link_calculation("pw2wannier90_run")
launcher.mpi_command = "mpirun -np 4"
launcher.write()
launcher.run()
