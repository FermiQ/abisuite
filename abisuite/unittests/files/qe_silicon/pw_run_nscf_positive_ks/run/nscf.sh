#!/bin/bash

MPIRUN="mpirun -np 4"
EXECUTABLE="/home/fgoudreault/Workspace/q-e/bin/pw.x -npool 4"
INPUT=/home/fgoudreault/Workspace/abisuite/abisuite/unittests/files/qe_silicon/pw_run_nscf_positive_ks/nscf.in
LOG=/home/fgoudreault/Workspace/abisuite/abisuite/unittests/files/qe_silicon/pw_run_nscf_positive_ks/nscf.log
STDERR=/home/fgoudreault/Workspace/abisuite/abisuite/unittests/files/qe_silicon/pw_run_nscf_positive_ks/nscf.stderr


$MPIRUN $EXECUTABLE < $INPUT > $LOG 2> $STDERR
