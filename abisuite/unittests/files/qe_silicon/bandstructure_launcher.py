from abisuite import QEPWLauncher as Launcher


# PATH POINTS
Gamma = [0.0, 0.0, 0.0]
X = [-0.5, 0.0, 0.0]  # [-0.5, 1.0, 0.0]
W = [-0.75, -0.5, -0.25]  # [-0.75, 1.5, -0.25]
K = [-0.75, -0.5, -3/8]  # [-0.75, 1.5, -3/8]
L = [-0.5, -0.5, -0.5]  # [-0.5, 1.5, -0.5]
U = [-5/8, -0.5, -0.25]  # [-5/8, 1.5, -0.25]

# PATH
kpts = [Gamma, X, W, K, Gamma, L, U, W, L, K, U, X]
Npts = 20

# very simple example
qe_input_vars = {
        "calculation": "bands",
        "nbnd": 12,
        "ecutwfc": 50.0,
        # "conv_thr": 1e-12,
        # "diagonalization": "cg",
        # "diago_full_acc": True,
        "k_points": {"parameter": "crystal_b",
                     "nks": len(kpts),
                     "k_points": kpts,
                     "weights": [Npts] * len(kpts),
                     },
        "verbosity": "high",
        }

launcher = Launcher("bandstructure")
launcher.command_arguments = "-pool 4"
launcher.workdir = "bandstructure_run"
launcher.mpi_command = "mpirun -np 4"
launcher.input_variables = qe_input_vars
launcher.load_geometry_from("pw_run")
launcher.link_calculation("pw_run")
launcher.write()
launcher.run()
