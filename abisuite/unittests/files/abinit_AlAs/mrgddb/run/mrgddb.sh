#!/bin/bash

MPIRUN=""
EXECUTABLE="/Users/fgoudreault/Workspace/abinit/build/mpi/src/98_main/mrgddb"
INPUT=/Users/fgoudreault/Workspace/abisuite/abisuite/unittests/files/abinit_AlAs/mrgddb/mrgddb.in
LOG=/Users/fgoudreault/Workspace/abisuite/abisuite/unittests/files/abinit_AlAs/mrgddb/mrgddb.log
STDERR=/Users/fgoudreault/Workspace/abisuite/abisuite/unittests/files/abinit_AlAs/mrgddb/mrgddb.stderr


$MPIRUN $EXECUTABLE < $INPUT > $LOG 2> $STDERR
