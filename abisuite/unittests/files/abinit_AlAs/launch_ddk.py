from abisuite import AbinitLauncher as Launcher


launcher = Launcher("ddk")
launcher.workdir = "ddk"
launcher.input_variables = {
        "irdwfk": 1,
        "nqpt": 1,
        "qpt": [0.0, 0.0, 0.0],
        "iscf": -3,
        "kptopt": 2,
        "rfphon": 0,
        "rfelfd": 2,
        "tolwfr": 1.0e-22,
        "nband": 4,
        "ixc": 1,
        "ecut": 3.0,
        "ngkpt": [4, 4, 4],
        "nshiftk": 4,
        "shiftk": [[0.0, 0.0, 0.5], [0.0, 0.5, 0.0],
                   [0.5, 0.0, 0.0], [0.5, 0.5, 0.5]],
        "nstep": 25,
        "diemac": 9.0,
        }
launcher.load_geometry_from("gs")
launcher.link_wfk_from("gs")
launcher.mpi_command = "mpirun -np 4"
launcher.write()
launcher.launch()
