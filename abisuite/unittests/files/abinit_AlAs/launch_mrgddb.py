from abisuite import AbinitMrgddbLauncher as Launcher


launcher = Launcher("mrgddb")
launcher.workdir = "mrgddb"
launcher.link_calculation("phonons/*")
launcher.write()
launcher.launch()
