import os

from abisuite import AbinitLauncher as Launcher


qpts = [[0.25, 0.0, 0.0],
        [0.5, 0.0, 0.0],
        [0.25, 0.25, 0.0],
        [0.5, 0.25, 0.0],
        [0.5, 0.25, 0.0],
        [-0.25, 0.25, 0.0],
        [0.5, 0.5, 0.0],
        [-0.25, 0.5, 0.25],
        ]

for i, qpt in enumerate(qpts):
    launcher = Launcher(f"ph{i + 1}")
    launcher.workdir = os.path.join("phonons", f"ph{i + 1}")
    launcher.input_variables = {
            "nqpt": 1,
            "irdwfk": 1,
            "kptopt": 3,
            "rfphon": 1,
            "rfatpol": [1, 2],
            "rfdir": [1, 1, 1],
            "tolvrs": 1.0e-8,
            "diemac": 9.0,
            "ixc": 1,
            "nstep": 25,
            "qpt": qpt,
            }
    launcher.load_geometry_from("gs")
    launcher.link_wfk_from("gs")
    launcher.mpi_command = "mpirun -np 4"
    launcher.write()
    launcher.launch()
