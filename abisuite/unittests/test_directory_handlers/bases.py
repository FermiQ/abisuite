import os
import tempfile

from ...exceptions import DevError
from ...linux_tools import touch


class BaseDirectoryHandlerTest:
    _directory_handler_class = None
    _directory_handler_init_args = []

    def setUp(self):
        if self._directory_handler_class is None:
            raise DevError("Test requires a directory handler class.")
        # create a directory with some content in it
        self._dir = tempfile.TemporaryDirectory()
        # create file inside
        self._file = os.path.join(self._dir.name, "tempfile")
        touch(self._file)
        # create sub directory
        self._subdir = tempfile.TemporaryDirectory(dir=self._dir.name)
        # create subfile in subdirectory
        self._subfile = os.path.join(self._subdir.name, "tempsubfile")
        touch(self._subfile)
        self.handler = self._directory_handler_class(
                *self._directory_handler_init_args, loglevel=1
                )
        self.handler.path = self._dir.name

    def tearDown(self):
        if os.path.isdir(self._dir.name):
            self._dir.cleanup()
            del self._dir

    def test_read(self):
        # test that reading gets all the content and subcontent
        self.handler.read()
        # check all paths and subpaths
        allpaths = [x.path for x in self.handler.content]
        self.assertIn(self._file, allpaths)
        self.assertIn(self._subdir.name, allpaths)
        allsubpaths = self.handler.walk(paths_only=True)
        self.assertIn(
                self._subfile,
                allsubpaths,
                msg=f"{str(self.handler)}")

    def test_copy(self):
        copy = self.handler.copy()
        self.assertEqual(self.handler, copy,
                         msg=f"{self.handler.content} != {copy.content}")

    def test_copy_directory(self):
        # find other place
        newdir = tempfile.TemporaryDirectory()
        self.handler.copy_directory(newdir.name)
        newhandler = self._directory_handler_class(
                *self._directory_handler_init_args, loglevel=1)
        newhandler.path = newdir.name
        newhandler.read()
        # direct comparision is hard to do since not same path for each objects
        # self.assertEqual(newhandler.content, self.handler.content)
        self.assertEqual(len(newhandler.content), len(self.handler.content),
                         msg=(f"\n{os.listdir(newhandler.path)}\n!=\n"
                              f"{os.listdir(self.handler.path)}"))

    def test_delete(self):
        self.handler.delete()
        self.assertFalse(os.path.exists(self.handler.path))

    def test_move(self):
        self.handler.read()  # read now to make sure everything is included
        olddir = self.handler.copy()
        newdir = tempfile.TemporaryDirectory()
        self.handler.move(newdir.name)
        self.assertFalse(os.path.exists(olddir.path))
        self.assertTrue(os.path.isdir(newdir.name))
        self.assertEqual(len(self.handler.content), len(olddir.content),
                         msg=(f"\n{[x.path for x in self.handler.content]}\n!="
                              f"\n{[x.path for x in olddir.content]}"))
        # direct comparision is hard to do since not same path for each objects
        # self.assertTrue(all([x == y for x, y in zip(self.handler.content,
        #                                             olddir.content)]),
        #                 msg=f"{self.handler.content} != {olddir.content}")
        self.assertEqual(self.handler.path, newdir.name)


class BaseDirectoryHandlerWithPBSTest(BaseDirectoryHandlerTest):
    def setUp(self):
        super().setUp()
        # create random pbs file
        self.handler.pbs_file.path = os.path.join(self.handler.path, "pbs.sh")
        self.handler.pbs_file.queuing_system = "local"
        self.handler.pbs_file.input_file_path = "input"
        self.handler.pbs_file.stderr_path = "stderr"
        self.handler.pbs_file.log_path = "log"
        self.handler.pbs_file.command = "command"

    def test_copy(self):
        self.handler.write()
        super().test_copy()

    def test_copy_directory(self):
        self.handler.write()
        super().test_copy_directory()

    def test_move(self):
        self.handler.write()
        super().test_move()

    def test_write(self):
        self.handler.write()
        self.assertTrue(os.path.isfile(self.handler.pbs_file.path))
        self.assertTrue(os.path.isdir(self.handler.path))
