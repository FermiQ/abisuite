from .bases import BaseDirectoryHandlerWithPBSTest
from ...handlers import RunDirectory
import unittest


class TestRunDirectory(BaseDirectoryHandlerWithPBSTest, unittest.TestCase):
    _directory_handler_class = RunDirectory
