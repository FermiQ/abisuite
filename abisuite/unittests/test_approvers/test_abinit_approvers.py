import unittest

from .bases import BaseApproverTest, BaseInputApproverTest
from ..variables_for_tests import (
        abinit_anaddb_vars, abinit_vars, abinit_optic_vars,
        abinit_AlAs_pseudos,
        )
from ...approvers import (
        AbinitAnaddbInputApprover, AbinitInputApprover,
        AbinitMrgddbInputApprover, AbinitOpticInputApprover,
        )
from ...approvers.exceptions import InputFileError
from ...approvers.abinit_approvers.abinit_input_approver import (
        TOLERANCES_VARS, ATOMIC_POSITION_VARS)
from ...variables.abinit_variables import (
        ALL_ABINIT_VARIABLES, ALL_ABINIT_ANADDB_VARIABLES,
        ALL_ABINITOPTIC_VARIABLES,
        )


class TestAbinitAnaddbInputApprover(BaseInputApproverTest, unittest.TestCase):
    _approver_class = AbinitAnaddbInputApprover
    _variables = abinit_anaddb_vars.copy()
    _variables_db = ALL_ABINIT_ANADDB_VARIABLES


class TestAbinitInputApprover(BaseInputApproverTest, unittest.TestCase):
    _approver_class = AbinitInputApprover
    _variables = abinit_vars
    _variables_db = ALL_ABINIT_VARIABLES
    _pseudos = abinit_AlAs_pseudos

    def test_tolwfr_if_nscf(self):
        self.approver.structure.add_input_variable("iscf", -2)
        # self.approver.input_variables.pop("toldfe")
        self.approver.validate()
        self.assertFalse(self.approver.is_valid)
        self.assertEqual(len(self.approver.errors), 1,
                         msg=self.approver.errors)
        with self.assertRaises(InputFileError):
            self.approver.raise_errors()

    def test_one_tolerances(self):
        """Check that if more than one tolerance var is there, an error is
        raised.

        Also check that if no tolerance is there, an error is raised.
        """
        for tolerance in TOLERANCES_VARS:
            if tolerance in self.variables:
                continue
            inputs = self.variables.copy()
            inputs[tolerance] = 10e-6
            approver = self._approver_class()
            approver.input_variables = inputs
            approver.pseudos = abinit_AlAs_pseudos
            approver.validate()
            self.assertFalse(approver.is_valid)
            self.assertEqual(len(approver.errors), 1,
                             msg=approver.errors)
            with self.assertRaises(InputFileError):
                approver.raise_errors()

        # check at least one tolerance is there
        for tolerance in TOLERANCES_VARS:
            if tolerance in self.approver.input_variables:
                self.approver.input_variables.pop(tolerance)
        self.approver.validate()
        self.assertFalse(self.approver.is_valid)
        self.assertEqual(len(self.approver.errors), 1)
        with self.assertRaises(InputFileError):
            self.approver.raise_errors()

    def test_one_atomic_position(self):
        # test that when there are more than 1 atomic position variable present
        # in input file, the approver raises an error.
        for atomic in ATOMIC_POSITION_VARS:
            if atomic in self.variables:
                continue
            inputs = self.variables.copy()
            inputs[atomic] = [0.0, 0.0, 0.0]
            approver = self._approver_class()
            approver.input_variables = inputs
            approver.pseudos = abinit_AlAs_pseudos
            approver.validate()
            self.assertFalse(approver.is_valid)
            self.assertEqual(len(approver.errors), 1,
                             msg=approver.errors)
            with self.assertRaises(InputFileError):
                approver.raise_errors()

        # check that if no atomic position is present, an error is also raised
        for atomic in ATOMIC_POSITION_VARS:
            if atomic in self.variables:
                self.variables.pop(atomic)
                break
        self.approver.input_variables = inputs
        self.approver.validate()
        self.assertFalse(self.approver.is_valid)
        self.assertEqual(len(self.approver.errors), 1)
        with self.assertRaises(InputFileError):
            self.approver.raise_errors()

    def test_wrong_znucl_vs_pseudo(self):
        """For AlAs, check that setting a wrong znucl compared to pseudo
        raises an error.

        Actually 2 errors: one for each znucl
        """
        self.variables["znucl"] = [1, 2]
        self.approver.input_variables = self.variables
        self.approver.validate()
        self.assertEqual(len(self.approver.errors), 2)
        with self.assertRaises(InputFileError):
            self.approver.raise_errors()

    def test_not_enough_bands(self):
        self.variables["nspinor"] = 2
        self.variables["nband"] = 1
        self.approver.input_variables = self.variables
        self.approver.validate()
        self.assertFalse(self.approver.is_valid)
        with self.assertRaises(InputFileError):
            self.approver.raise_errors()

    def test_shiftk_not_consistent(self):
        # test nshiftk not well set
        self.variables["nshiftk"] = 0
        approver = self._approver_class()
        approver.input_variables = self.variables
        approver.pseudos = abinit_AlAs_pseudos
        approver.validate()
        self.assertFalse(approver.is_valid)
        with self.assertRaises(InputFileError):
            approver.raise_errors()
        # test len(shiftk) != nshift
        self.variables["nshiftk"] = 2
        self.variables["shiftk"] = [0.5, 0.5, 0.5]
        approver = self._approver_class()
        approver.input_variables = self.variables
        approver.pseudos = abinit_AlAs_pseudos
        approver.validate()
        self.assertFalse(approver.is_valid)
        with self.assertRaises(InputFileError):
            approver.raise_errors()
        self.variables["shiftk"] = [[0.5, 0.5, 0.5], [0.6, 0.6, 0.6],
                                    [0.7, 0.7, 0.7]]
        approver = self._approver_class()
        approver.input_variables = self.variables
        approver.pseudos = abinit_AlAs_pseudos
        approver.validate()
        self.assertFalse(approver.is_valid)
        with self.assertRaises(InputFileError):
            approver.raise_errors()


class TestAbinitMrgddbInputApprover(BaseApproverTest, unittest.TestCase):
    """Test case for the abinit mrgddb input approver class.
    """
    _approver_class = AbinitMrgddbInputApprover
    _exception = InputFileError

    def setUp(self):
        super().setUp()
        self.approver.output_file_path = "test_output_file"
        self.approver.ddb_paths = ["test_file1", "test_file2", ]

    def test_not_enough_ddb_paths(self):
        """Test that when there is less than 2 ddb paths, the approver raises
        an error.
        """
        self.approver.ddb_paths = []
        self.approver.validate()
        self.assertEqual(len(self.approver.errors), 1)
        with self.assertRaises(self._exception):
            self.approver.raise_errors()


class TestAbinitOpticInputApprover(BaseInputApproverTest, unittest.TestCase):
    _approver_class = AbinitOpticInputApprover
    _variables = abinit_optic_vars.copy()
    _variables_db = ALL_ABINITOPTIC_VARIABLES
