from .bases import BaseApproverTest
from ..variables_for_tests import (
                abinit_vars, BASIC_MPI_VARIABLES, qe_phx_vars, qe_pwx_vars
                )
from ...approvers import (AbinitInputParalApprover, AbinitInputApprover,
                          MPIApprover,
                          QEPHInputApprover, QEPHInputParalApprover,
                          QEPWInputParalApprover,
                          QEPWInputApprover)
from ...approvers.exceptions import InputFileError
import unittest


abinit_vars = abinit_vars.copy()
abinit_vars["autoparal"] = 1


class BaseInputParalApproverTest(BaseApproverTest):
    _input_vars = None
    _input_approver_class = None
    # _approver is now the input paral approver

    def setUp(self):
        self.variables = self._input_vars.copy()
        self.mpi_variables = BASIC_MPI_VARIABLES.copy()
        self.approver = self._approver_class()
        self.approver.input_variables = self.variables
        self.approver.mpi_command = self.mpi_variables["mpi_command"]
        self.approver.nodes = self.mpi_variables["nodes"]
        self.approver.ppn = self.mpi_variables["ppn"]

    def test_from_approvers_ok(self):
        # check that it works when giving corresponding approvers
        inputapprover = self._input_approver_class()
        inputapprover.input_variables = self.variables
        mpiapprover = MPIApprover()
        mpiapprover.mpi_command = self.mpi_variables["mpi_command"]
        mpiapprover.nodes = self.mpi_variables["nodes"]
        mpiapprover.ppn = self.mpi_variables["ppn"]
        mpiapprover.queuing_system = "torque"
        mpiapprover.command = ""
        approver = self._approver_class.from_approvers(inputapprover,
                                                       mpiapprover)
        approver.validate()
        self.assertTrue(approver.is_valid)
        self.assertEqual(len(approver.errors), 0)

    def test_raise_init_from_approvers(self):
        inputapprover = self._input_approver_class()
        inputapprover.input_variables = self._input_vars
        mpiapprover = MPIApprover()
        mpiapprover.queuing_system = "torque"
        mpiapprover.ppn = self.mpi_variables["ppn"]
        mpiapprover.nodes = self.mpi_variables["nodes"]
        with self.assertRaises(TypeError):
            self._approver_class.from_approvers(1, mpiapprover)
        with self.assertRaises(TypeError):
            self._approver_class.from_approvers(inputapprover, 1)
        with self.assertRaises(TypeError):
            self._approver_class.from_approvers(1, 1)


class BaseTestQEInputParalApprover(BaseInputParalApproverTest):
    _script = None

    def setUp(self):
        # this modifies the class attribute it's weird...
        super().setUp()
        self.approver.command = f"{self._script} -npool 3"

    def test_npool_too_large_for_total_ncpus(self):
        self.approver.command = f"{self._script} -npool 100000"
        self.approver.mpi_command = ""
        self.approver.validate()
        self.assertFalse(self.approver.is_valid)
        self.assertEqual(len(self.approver.errors), 2)
        with self.assertRaises(InputFileError):
            self.approver.raise_errors()


class TestQEPHInputParalApprover(BaseTestQEInputParalApprover,
                                 unittest.TestCase):
    _approver_class = QEPHInputParalApprover
    _input_approver_class = QEPHInputApprover
    _input_vars = qe_phx_vars.copy()
    _script = "ph.x"


class TestQEPWInputParalApprover(BaseTestQEInputParalApprover,
                                 unittest.TestCase):
    _approver_class = QEPWInputParalApprover
    _input_approver_class = QEPWInputApprover
    _input_vars = qe_pwx_vars.copy()
    _script = "pw.x"


class TestAbinitInputParalApprover(BaseInputParalApproverTest,
                                   unittest.TestCase):
    _approver_class = AbinitInputParalApprover
    _input_approver_class = AbinitInputApprover
    _input_vars = abinit_vars.copy()

    def test_fail_if_more_proc_asked_than_avail(self):
        self.variables["npkpt"] = 100
        self.approver.input_variables = self.variables
        self.approver.validate()
        self.assertFalse(self.approver.is_valid)
        self.assertEqual(len(self.approver.errors), 1)
        with self.assertRaises(InputFileError):
            self.approver.raise_errors()
