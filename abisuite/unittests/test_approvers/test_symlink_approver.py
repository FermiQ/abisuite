from .bases import BaseApproverTest
from ...approvers import SymLinkApprover
from ...approvers.exceptions import SymLinkError
import tempfile
import unittest


class TestSymLinkApprover(BaseApproverTest, unittest.TestCase):
    _approver_class = SymLinkApprover
    _exception = SymLinkError

    def setUp(self):
        super().setUp()
        # create file
        self.source = tempfile.NamedTemporaryFile()
        self.approver.source = self.source.name

    def test_non_existant_source(self):
        self.approver.source = "non_existant_source"
        self.approver.validate()
        self.assertFalse(self.approver.is_valid)
        self.assertEqual(len(self.approver.errors), 1)
        with self.assertRaises(SymLinkError):
            self.approver.raise_errors()
