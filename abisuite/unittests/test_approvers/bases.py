from ...approvers.exceptions import InputFileError
from ...exceptions import DevError


class BaseApproverTest:
    _approver_class = None
    _exception = None

    def setUp(self):
        if self._approver_class is None:
            raise DevError(
                    f"Need to set '_approver_class' in '{self.__class__}'.")
        self.approver = self._approver_class()

    def test_approver_works(self):
        # check that an error is raised looking of approver is valid before
        # being validated
        with self.assertRaises(self._approver_class._exception):
            self.approver.is_valid
        # check that it works as expected using the variables
        self.approver.validate()
        self.assertTrue(self.approver.is_valid,
                        msg=self.approver.errors)
        self.assertEqual(len(self.approver.errors), 0,
                         msg=self.approver.errors)
        # raising errors should not raise anything
        self.approver.raise_errors()
        # check that if we manually append an error, it acts as expected
        self.approver.errors.append("error")
        self.assertFalse(self.approver.is_valid)
        with self.assertRaises(self._approver_class._exception):
            self.approver.raise_errors()


class BaseInputApproverTest(BaseApproverTest):
    _specific_mandatory_vars = None
    _variables = None
    _variables_db = None
    _pseudos = None

    def setUp(self):
        if self._variables is None:
            raise DevError(
                    f"Need to set '_variables' for '{self.__class__}'.")
        if self._variables_db is None:
            raise DevError(
                    f"Need to set '_variables_db' for '{self.__class__}'.")
        super().setUp()
        self.variables = self._variables.copy()
        # get mandatory variables
        man = []
        for var, params in self._variables_db.items():
            if params["mandatory"]:
                man.append(var)
        self._mandatory_vars = man
        self.approver.input_variables = self.variables
        if self._pseudos is not None:
            self.approver.pseudos = self._pseudos

    def test_no_dict_raise(self):
        with self.assertRaises(TypeError):
            app = self._approver_class()
            app.input_variables = 1

    def test_missing_mandatory_variable(self):
        manvars = list(self._mandatory_vars)
        if self._specific_mandatory_vars is not None:
            manvars += list(self._specific_mandatory_vars)
        for var in self._mandatory_vars:
            inputs = self._variables.copy()
            if "*" in var:
                # a wild card mandatory variable.
                splitted = var.split("*")
                start = splitted[0]
                end = splitted[1]
                topop = []
                for varname in inputs:
                    # pop them all!
                    if varname.startswith(start) and varname.endswith(end):
                        topop.append(varname)
                for varname in topop:
                    inputs.pop(varname, None)
            else:
                inputs.pop(var, None)
            approver = self._approver_class()
            approver.input_variables = inputs
            if self._pseudos is not None:
                approver.pseudos = self._pseudos
            approver.validate()
            self.assertFalse(approver.is_valid,
                             msg=f"missing {var} should raise an error.")
            self.assertGreaterEqual(len(approver.errors), 1)
            with self.assertRaises(InputFileError):
                approver.raise_errors()
            del approver
