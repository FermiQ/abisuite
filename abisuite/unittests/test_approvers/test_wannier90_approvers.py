from .bases import BaseInputApproverTest
from ..variables_for_tests import (
        wannier90_vars,
        )
from ...approvers import (
        Wannier90InputApprover,
        )
from ...variables.wannier90_variables import (
        ALL_WANNIER90_VARIABLES,
        )
import unittest


class TestWannier90InputApprover(BaseInputApproverTest, unittest.TestCase):
    _approver_class = Wannier90InputApprover
    _variables = wannier90_vars.copy()
    _variables_db = ALL_WANNIER90_VARIABLES
