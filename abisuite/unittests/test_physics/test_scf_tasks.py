# import unittest
#
# from abisuite import SCFTask
#
#
#
# class TestSCFTaskUsingQE(unittest.TestCase):
#     """TestCase for a SCF Task that uses Q-E.
#     """
#     def setUp(self):
#         self.task = SCFTask()
#         self.task.software = "qe"
#         self.task.host = "local"
#         self.task.queuing_system = "local"
#         self.task.set_geometry(
#                 bravais_lattice="fcc",
#                 natoms=2,
#                 atom_types=("Si", ),
#                 atom_positions=
