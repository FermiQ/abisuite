import os

# from tqdm import tqdm

from abisuite.handlers import CalculationDirectory, GenericFile
from abisuite.linux_tools import mkdir
from abisuite.routines import full_abspath
from abisuite.scripts.bases import BaseAbisuiteArgParser


class AbimoveArgParser(BaseAbisuiteArgParser):
    """Argparser for abimove script."""

    def create_parser(self):
        parser = super().create_parser()
        parser.add_argument(
                "to_move", action="store", type=str,
                help="Calculation(s) to move.",
                nargs="*")
        parser.add_argument(
                "output", action="store", type=str,
                help=("Where to move the calculation(s). If dir does not exist"
                      " it is created"))
        parser.add_argument(
                "--dry-run", "-d", action="store_true",
                help="Dry run mode, don't do nothing. Just print what happens")
        parser.add_argument(
                "--completed-only", "-c", action="store_true",
                help="Move only completed calculations.")
        parser.add_argument(
                "--recursive", "-r", action="store_true",
                help="Recursive mode.")
        return parser


def move_this(to_move, target, completed_only, recursive, dry_run):
    """Actually moves one directory to another place.
    """
    if os.path.basename(target) != os.path.basename(to_move):
        # add basename to target
        target = os.path.join(target, os.path.basename(to_move))
    if CalculationDirectory.is_calculation_directory(to_move):
        with CalculationDirectory.from_calculation(to_move) as calc:
            try:
                calc.connect_to_database()
            except Exception:
                pass
            if not calc.status["calculation_finished"] and completed_only:
                # don't move since calculation is not completed
                return
            print(f"Moving '{to_move}' -> '{target}'.")
            if dry_run:
                return
            calc.move(target, _display_progress_bar=True)
            print("Done")
        return
    if os.path.isfile(to_move):
        print(f"Moving '{to_move}' -> '{target}'.")
        if dry_run:
            return
        # move the file
        with GenericFile.from_file(to_move) as file_:
            file_.move(target)
        print("Done")
        return
    if os.path.isdir(to_move) and recursive:
        # move content of directory by keeping same basename
        basename = os.path.basename(to_move)
        for name in os.listdir(to_move):
            move_this(
                    os.path.join(to_move, name),
                    os.path.join(target, basename),
                    completed_only,
                    recursive)


if __name__ == "__main__":
    parser = AbimoveArgParser()
    args = parser.parse_args()
    to_move = args.to_move
    output = full_abspath(args.output)
    dry_run = args.dry_run
    recursive = args.recursive
    completed_only = args.completed_only
    if len(to_move) > 1:
        # if more than one calc dir to move, move them all in output with same
        # basename
        if not os.path.exists(output):
            mkdir(output)
        if not os.path.isdir(output):
            raise NotADirectoryError(output)

    for path in to_move:
        path = full_abspath(path)
        move_this(path, output, completed_only, recursive, dry_run)
    # TODO: find a way to reintegrate the progress bar
    # if not dry_run:
    #     # show a progress bar
    #     with tqdm(calcs, unit=" calculations", desc="abimove progress",
    #               unit_scale=False, dynamic_ncols=True) as progress_bar:
    #         for calc, dest in zip(calcs, dests):
    #             try:
    #                 abimove(calc, dest, display_progress_bar=True)
    #                 del calc
    #             except Exception as e:
    #                 warnings.warn(
    #                         f"An error occured while moving '{calc.path}'")
    #                 warnings.warn(traceback.print_tb(e.__traceback__))
    #             finally:
    #                 progress_bar.update()
