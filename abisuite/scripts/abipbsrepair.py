import glob
import os
import warnings

from abisuite.handlers import MetaDataFile, PBSFile
from abisuite.routines import full_abspath
from abisuite.scripts.bases import BaseAbisuiteArgParser


class AbipbsrepairArgParser(BaseAbisuiteArgParser):
    """Argparser for abipbsrepair script."""

    def create_parser(self):
        parser = super().create_parser()
        parser.add_argument(
                "to_repair", action="store", type=str,
                default=".",
                help=("Calculation directory(ies) containing pbs"
                      " files to repair."),
                nargs="*")
        parser.add_argument(
                "--dry-run", "-d", action="store_true",
                help="Dry run mode, do nothing. Just print what happens")
        return parser


if __name__ == "__main__":
    parser = AbipbsrepairArgParser()
    args = parser.parse_args()
    to_repairs = args.to_repair
    dry_run = args.dry_run
    loglevel = args.loglevel
    all_paths = []
    for to_repair in to_repairs:
        if "*" in to_repair:
            all_paths += glob.glob(to_repair)
        else:
            all_paths.append(to_repair)
    for to_repair in all_paths:
        here = full_abspath(to_repair)
        if dry_run:
            print(f"Trying to repair pbs file in: '{here}'")  # noqa: T001
            continue
        try:
            with PBSFile.from_calculation(
                            here, loglevel=loglevel) as pbs:
                print(f"Trying to repair pbs file in: '{here}'")  # noqa: T001
                # we need to get the new working directory and hope its
                # basename is the same as the old one
                with MetaDataFile.from_calculation(
                                here, loglevel=loglevel) as meta:
                    workdir = meta.calc_workdir
                    basename = os.path.basename(workdir)
                    if basename not in pbs.path:
                        warnings.warn("Unable to repair in: '{here}'.")
                        continue
                    # ok can repair from here. get new relative paths
                    for attr in ("log_path", "stderr_path", "input_file_path"):
                        old = getattr(pbs, attr)
                        # add the sep. here to make sure the split goes well
                        new = os.path.join(
                                workdir, old.split(basename + os.path.sep)[-1])
                        setattr(pbs, attr, new)
                print("Repairing attempt done.")  # noqa: T001
        except FileNotFoundError:
            # this is not a calculation directory, skip it
            continue
