import argparse
import logging

from ..bases import BaseRemoteClient
from ..routines import is_list_like


class BaseAbisuiteArgParser:
    """Baseclass for any argparser for the abisuite package.
    """
    description = None

    def __init__(self):
        self.parser = self.create_parser()

    def parse_args(self):
        return self.parser.parse_args()

    def create_parser(self):
        parser = argparse.ArgumentParser(description=self.description)
        parser.add_argument("--loglevel", "-l", action="store", type=int,
                            default=logging.INFO,
                            help=("Sets the logging level."))
        return parser


class BaseAbisuiteScriptRemoteClient(BaseRemoteClient):
    """Base class for remote clients defined only in the scripts.
    """

    def remote_script_calculations(self, script, calculations):
        """Use the script remotely on given calculations.

        Parameters
        ----------
        script: str
            The script to use.
        calculations: str or list-like
            The calculations to send to the script. Can be a list.
        """
        assert script == self._check_script_exists_on_connection
        if self.client is None:
            raise RuntimeError(
                    "Need to enter with statement before relaunching remote "
                    "calculations.")
        if not is_list_like(calculations):
            self.remote_script_calculations(script, (calculations, ))
            return
        cmd = f"{script} " + " ".join(calculations)
        stdin, stdout, stderr = self.client.exec_command(cmd)
        stderr = stderr.read().decode("utf-8")
        stdout = stdout.read().decode("utf-8")
        if stdout:
            print(stdout)
        if stderr:
            print(stderr)
