import sys

from abisuite import USER_CONFIG
from abisuite.databases import AbiDB
from abisuite.databases.exceptions import (
        CalculationDoesNotExistError, CachedAbiDBFileNotFoundError,
        )
from abisuite.routines import full_abspath
from abisuite.scripts.bases import BaseAbisuiteArgParser


class AbiDBArgParser(BaseAbisuiteArgParser):
    """Argparser for the abidb script.
    """

    def create_parser(self):
        parser = super().create_parser()
        parser.add_argument(
                "calculation", action="store", type=str,
                help="Calculation(s) to handle.",
                default=".",
                nargs="*")
        parser.add_argument(
                "--add", action="store_true",
                help="Add the calculation(s) to the database.")
        parser.add_argument(
                "--all", action="store_true",
                help="Show all calculations from the database.")
        parser.add_argument(
                "--all-monitored", action="store_true",
                help="Show all monitored calculations from the database.")
        parser.add_argument(
                "--download", action="store_true",
                help=("Download remote database. If not set, I'll try to use "
                      "a cached previously downloaded database."))
        parser.add_argument(
                "--force", action="store_true",
                help=("Forces abidb operations and bypass integrity checks."))
        # get arguments to get or set data from database
        parser.add_argument(
                "--get-id", action="store_true",
                help="Get the calculation(s) id(s) from the database.")
        parser.add_argument(
                "--get-status", action="store_true",
                help="Get calculation status from database.")
        parser.add_argument(
                "--get-monitored", action="store_true",
                help="Returns True if calculation is monitored.")
        parser.add_argument(
                "--get-project", action="store_true",
                help="Returns the project tag of the calculation.")
        parser.add_argument(
                "--set-monitored-false", action="store_true",
                help="Set the 'monitored' attribute to False.")
        parser.add_argument(
                "--set-monitored-true", action="store_true",
                help="Set the 'monitored' attribute to True.")
        parser.add_argument(
                "--set-project", action="store", type=str, default="",
                help="Set the project tag of a given calculation to this.")
        parser.add_argument(
                "--database", action="store", type=str,
                default="default",
                help="The database to use. The default is in config file.")
        parser.add_argument(
                "--delete", action="store_true",
                help="Delete the calculation(s) from the database.")
        parser.add_argument(
                "--remote", action="store", type=str,
                default="none",
                help="Use the default database from given remote.")
        parser.add_argument(
                "--init-db", action="store", default="none",
                help=(
                    "If this flag is activated, a database is created here "
                    "with the name given by this flag. And then it exit."))
        parser.add_argument(
                "--refresh", action="store_true",
                help=("Refreshes the calculation status database entry for "
                      "selected calculations. If remote is set, all "
                      "monitored calculations will be refreshed on the remote "
                      "machine."))
        parser.add_argument(
                "--show-db", action="store_true",
                help="Print the database file path and exit.")
        return parser


if __name__ == "__main__":
    parser = AbiDBArgParser()
    args = parser.parse_args()
    calculations = args.calculation
    add = args.add
    all_ = args.all
    all_monitored = args.all_monitored
    if all_ and all_monitored:
        raise ValueError(
                "Make up your mind! Do I fetch all calculations or just "
                "the monitored ones?!?")
    download = args.download
    delete = args.delete
    if add and delete:
        raise ValueError(
                "Cannot delete and add a calculation at the same time.")
    force = args.force
    get_id = args.get_id
    get_status = args.get_status
    get_monitored = args.get_monitored
    get_project = args.get_project
    set_monitored_false = args.set_monitored_false
    set_monitored_true = args.set_monitored_true
    set_project = args.set_project
    show_db = args.show_db
    if set_monitored_true and set_monitored_false:
        raise ValueError(
                "Make up your mind about the 'monitored' attribute: "
                "I cannot set it to both True and False at the same time!")
    db = args.database
    remote = args.remote
    refresh = args.refresh
    loglevel = args.loglevel
    init_db = args.init_db
    if init_db != "none":
        # init a db at this place
        print(  # noqa: T001
                f"Creating a SQLite database here: '{init_db}'.")
        abidb = AbiDB(loglevel=loglevel)
        abidb.create_database(init_db)
        sys.exit()
    get = get_id or get_status or get_monitored or get_project
    set_ = set_monitored_true or set_monitored_false or set_project
    if remote == "none":
        if db == "default":
            db = USER_CONFIG.DATABASE.path
        # initialize DB
        abidb = AbiDB.from_file(db, loglevel=loglevel)
    else:
        if download:
            print(f"Downloading database: '{remote}'.")
            abidb = AbiDB.from_remote(
                        remote, download=True, refresh=refresh,
                        loglevel=loglevel)
        else:
            try:
                # use remote db but don't download yet
                print(f"Using cached remote database: '{remote}'.")
                abidb = AbiDB.from_remote(
                            remote, download=False, refresh=refresh,
                            loglevel=loglevel)
            except CachedAbiDBFileNotFoundError:
                print("Set call --download to download db.")
                sys.exit()
        # if not all_ and not all_monitored:
        #     print("Need to specify either --all or --all-monitored "
        #           "when using remotes.")
        #     sys.exit()
    if show_db:
        print(f"Transacting with Database: '{db}'.")  # noqa: T001
        sys.exit()
    if all_:
        # get all calculations from database and discard the ones given as args
        calculations = abidb.get_all_calculations()
    elif all_monitored:
        # get all monitored calculations from database and discard the ones
        # given in the args
        calculations = abidb.get_all_monitored_calculations()
    # if not a remote, get full abspaths
    if not abidb.is_remote:
        calculations = [full_abspath(calc) for calc in calculations]
    # if a set is done do it all at once
    if set_:
        if set_monitored_true:
            abidb.update_calculation_monitored(calculations, True)
            set_monitored_true = False
            for calculation in calculations:
                print(f"Now monitoring: '{calculation}'")
        elif set_monitored_false:
            abidb.update_calculation_monitored(calculations, False)
            set_monitored_false = False
            for calculation in calculations:
                print(f"Not monitoring: '{calculation}'")
        if set_project:
            # set project
            abidb.update_calculation_project(calculations, set_project)
            print(f"Project '{set_project}' now contains these calculations:")
            for calculation in calculations:
                print(calculation)
    for calculation in calculations:
        if add:
            print(f"Adding to database '{calculation}'.")  # noqa: T001
            abidb.add_calculation(calculation)
        # get or set data from db
        # at least print calculation path
        try:
            if delete:
                print(
                    f"Removing from database: '{calculation}'.")  # noqa: T001
                abidb.delete_calculation(
                        calculation, check_integrity=not force)
                continue
            toprint = f"'{calculation}'"
            if get:
                toprint += " ->"
            if get_id:
                toprint += f" ID: {abidb[calculation]}"
            if get_status:
                toprint += f" status: '{abidb.get_status(calculation)}'"
            if get_monitored:
                toprint += f" monitored: '{abidb.get_monitored(calculation)}'"
            if get_project:
                toprint += f" project: '{abidb.get_project(calculation)}'."
            if refresh and remote == "none":
                # refresh locally if needed
                abidb.refresh(calculation)
                toprint += " <Refreshed> "
            if get or not set_:
                # print if we get something or don't set anything
                print(toprint)  # noqa: T001
        except CalculationDoesNotExistError:
            print(f"Not in database: '{calculation}'.")  # noqa: T001
