import os

from ..handlers import CalculationDirectory
from ..routines import is_list_like
from ..status_checkers import StatusTree


def get_all_calculations_recursively(
        source, return_path=False, **kwargs):
    """Get the list of all calculations from a source tree recursively.

    Parameters
    ----------
    source: path, list
        The source of everything. If a list of sources is given,
        the concatenation of all calculations is returned.
    return_path: bool, optional
        If True, only paths are returned. Otherwise, CalculationDirectory
        objects are returned.
    other_kwargs are passed to the StatusTree object that build the tree.

    Returns
    -------
    list: The list of all calculations.
    """
    if is_list_like(source):
        # many sources. concatenate everything
        calcs = []
        for src in source:
            calcs += get_all_calculations_recursively(
                    src, return_path=return_path)
        return calcs
    if os.path.isfile(source):
        return []
    calculations = StatusTree(source, **kwargs).tree.calculations
    if return_path:
        return [x["path"] for x in calculations]
    return [CalculationDirectory.from_calculation(x["path"])
            for x in calculations]
