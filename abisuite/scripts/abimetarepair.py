import glob

from abisuite.handlers import MetaDataFile
from abisuite.routines import full_abspath
from abisuite.scripts.bases import BaseAbisuiteArgParser


class AbimetarepairArgParser(BaseAbisuiteArgParser):
    """Argparser for abimetarepair script."""

    def create_parser(self):
        parser = super().create_parser()
        parser.add_argument(
                "to_repair", action="store", type=str,
                default=".",
                help=("Calculation directory(ies) containing meta"
                      " data files to repair."),
                nargs="*")
        parser.add_argument(
                "--dry-run", "-d", action="store_true",
                help="Dry run mode, do nothing. Just print what happens")
        return parser


if __name__ == "__main__":
    parser = AbimetarepairArgParser()
    args = parser.parse_args()
    to_repairs = args.to_repair
    dry_run = args.dry_run
    loglevel = args.loglevel
    all_paths = []
    for to_repair in to_repairs:
        if "*" in to_repair:
            all_paths += glob.glob(to_repair)
        else:
            all_paths.append(to_repair)
    for to_repair in all_paths:
        here = full_abspath(to_repair)
        if dry_run:
            print(f"Trying to repair MetaDataFile in: '{here}'")  # noqa: T001
            continue
        try:
            with MetaDataFile.from_calculation(
                            here, loglevel=loglevel) as meta:
                print(  # noqa: T001
                 f"Trying to repair MetaDataFile in: '{here}'")
                meta.calc_workdir = here
                print("Repairing attempt done.")  # noqa: T001
        except FileNotFoundError:
            # this is not a calculation directory, skip it
            continue
