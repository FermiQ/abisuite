import os
import subprocess

from abisuite.exceptions import NotACalculationDirectoryError
from abisuite.handlers import is_calculation_directory, CalculationDirectory
from abisuite.launchers import AbinitCut3DLauncher
from abisuite.linux_tools import which
from abisuite.routines import full_abspath
from abisuite.scripts.bases import BaseAbisuiteArgParser


class AbiVisualizeStructureArgParser(BaseAbisuiteArgParser):
    """Arg parser class for the abivisualizestructure script.
    """
    def create_parser(self):
        parser = super().create_parser()
        parser.add_argument(
                "calculation", action="store", type=str,
                help="Calculation path to visualize.",
                default=".", nargs="*")
        return parser


if __name__ == "__main__":
    parser = AbiVisualizeStructureArgParser()
    args = parser.parse_args()
    calculation = full_abspath(args.calculation[0])
    if not is_calculation_directory(calculation):
        raise NotACalculationDirectoryError(calculation)
    with CalculationDirectory.from_calculation(calculation) as calc:
        if calc.calctype != "abinit":
            raise TypeError(
                    f"Not an 'abinit' calculation: '{calculation}'.")
        # check if an xsf file already exists
        with calc.output_data_directory as output:
            for filehandler in output:
                if filehandler.path.endswith(".xsf"):
                    # xsf file already exists
                    xsf = filehandler.path
                    break
            else:
                # xsf does not exist => generate it using cut3d
                # create cut3d launcher
                print("Launching 'cut3d' to generate 'xsf' file.")
                launcher = AbinitCut3DLauncher(loglevel=args.loglevel)
                launcher.workdir = calculation
                launcher.write()
                launcher.run()
                xsf = os.path.join(calc.output_data_directory.path,
                                   launcher.input_file.output_file_path)
    # launch vesta if it is available
    if which("vesta") is not None:
        print("Launching 'vesta' to visualize structure.")
        subprocess.run(["vesta", xsf])
    else:
        print("'vesta' not available in 'PATH'.")
