class AbiDBRuntimeError(RuntimeError):
    pass


class CalculationDoesNotExistError(FileNotFoundError):
    pass


class CachedAbiDBFileNotFoundError(FileNotFoundError):
    pass
