import configparser
import json
import os

from .bases import BaseUtility
from .exceptions import DevError
from .linux_tools import mkdir
from .routines import full_abspath


__ALL_REMOTES_KEYS__ = ["hostname", "address", "username", "port"]
CONFIG_PATH = os.path.join(os.path.expanduser("~"), ".config", "abisuite")
CONFIG_ATTRIBUTES = {
        "DEFAULTS": {
                "default_pseudos_dir": None,
                 },
        "SYSTEM": {
                "default_project_code": None,
                "default_project_account": None,
                "default_memory": None,
                "default_mpi_command": None,
                "default_lines_before": [],
                "queuing_system": "local",
                "max_jobs_in_queue": None,
                "max_jobname_len": None,
                "username": None,
                },
        "DATABASE": {
            "path": None,
            "package_dest_root": None,
            "package_src_root": None,
            },
        "REMOTES": {
            "remotes": [
                {"hostname": "example", "address": "example",
                 "username": "example", "port": 22},
                ],
            "cache_directory": full_abspath(
                os.path.join("~", ".cache", "abisuite")),
            },
        "ABINIT": {
                "abinit_path": "abinit",
                "anaddb_path": "anaddb",
                "cut3d_path": "cut3d",
                "optic_path": "optic",
                "mrgddb_path": "mrgddb",
                "default_build": "abinit_default_build",
                },
        "QUANTUM_ESPRESSO": {
                "dos_path": "dos.x",
                "dynmat_path": "dynmat.x",
                "matdyn_path": "matdyn.x",
                "epsilon_path": "epsilon.x",
                "epw_path": "epw.x",
                "fs_path": "fs.x",
                "ld1_path": "ld1.x",
                "ph_path": "ph.x",
                "pp_path": "pp.x",
                "projwfc_path": "projwfc.x",
                "pw_path": "pw.x",
                "pw2wannier90_path": "pw2wannier90.x",
                "q2r_path": "q2r.x",
                "default_build": None,
                },
        "WANNIER90": {
                "wannier90_path": "wannier90.x",
                "default_build": "wannier90_default_build",
                },
        "abinit_default_build": {
                "build_path": "path/to/build/with/executables",
                "modules_to_load": [],
                "modules_to_swap": [],
                "modules_to_unload": [],
                "modules_to_use": [],
                "default_lines_before": [],
                "has_mpi": False,
                },
        "qe_default_build": {
                "build_path": "path/to/build/with/executables",
                "modules_to_load": [],
                "modules_to_swap": [],
                "modules_to_unload": [],
                "modules_to_use": [],
                "default_lines_before": [],
                "has_mpi": False,
                },
        "wannier90_default_build": {
                "build_path": "path/to/build/with/executables",
                "modules_to_load": [],
                "modules_to_swap": [],
                "modules_to_unload": [],
                "modules_to_use": [],
                "default_lines_before": [],
                "has_mpi": False,
                },
        }


class ConfigValue(BaseUtility):
    _loggername = "ConfigValue"

    def __init__(self, entry, **kwargs):
        super().__init__(**kwargs)
        self._entry = entry

    def __contains__(self, item):
        return item in self.keys()

    def __getattr__(self, item):
        return self[item]

    def __getitem__(self, item):
        value = self._entry[item]
        if value == "none":
            return None
        if value == "false":
            return False
        if value == "true":
            return True
        if value.startswith("[") and value.endswith("]"):
            return self._extract_list(value)
        # check if we can convert value to a int or float otherwise return str
        try:
            return int(value)
        except ValueError:
            # not an integer
            pass
        try:
            return float(value)
        except ValueError:
            # not a float
            pass
        # just return the string then
        return value

    def keys(self):
        return self._entry.keys()

    def values(self):
        return [self[item] for item in self.keys()]

    def items(self):
        return [(i, v) for i, v in zip(self.keys(), self.values())]

    def __repr__(self):
        string = f"{self._entry.name}:\n"
        for item, value in self.items():
            string += f"{item} = {value}\n"
        return string

    def _extract_list(self, value):
        # json only works with double quotation marks.
        if "'" in value:
            # replace all "'" by '"'
            value = value.replace("'", '"')
        return json.loads(value)
        # strip = value.strip("[").strip("]").strip(" ")
        # if not len(strip):
        #     return []
        # if "[" in strip or "]" in strip:
        #     # it was a list of a list
        #     toreturn = []
        #     split = strip.split("[")
        # # separate element of the list
        # split = strip.split(",")
        # # remove spaces between elements if any
        # split = [x.strip() for x in split]
        # # reformat strings if needed
        # return [x.strip("'").strip('"') for x in split]


class BaseConfig(BaseUtility):
    """Base class for a config file handler.
    """
    config_path = None
    default_values = None

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        # create the file if it does not exists
        if self.config_path is None:
            raise DevError("Need to set 'config_path' class attribute.")
        if self.default_values is None:
            raise DevError("Need to set 'default_values' class attribute.")
        self._config = configparser.ConfigParser()
        self._config.read(self.config_path)
        self._path = self.config_path
        dirname = os.path.dirname(self.config_path)
        if not os.path.isdir(dirname):
            mkdir(dirname)
        if not os.path.isfile(self.config_path):
            self.set_all_defaults()
            self._logger.info(f"A config file has been created at "
                              f"{self.config_path}")
        self.check_content_integrity()

    def __contains__(self, item):
        return item in self._config

    def __getitem__(self, item):
        return ConfigValue(self._config[item])

    def __getattr__(self, item):
        return self[item]

    def check_content_integrity(self):
        """Check that for all config sections, the config section exists and
        that all its entries are present.
        """
        for sectionname, entries in CONFIG_ATTRIBUTES.items():
            if sectionname not in self:
                # set default for all its entries
                for entry in entries:
                    self.set_default(entry, sectionname)
            for entry in entries:
                if entry not in self[sectionname]:
                    # set default for missing entries
                    self.set_default(entry, sectionname)

    def write(self, key, value, repo, _force=False):
        """Write a config file entry.

        Parameters
        ----------
        key : str
            Attribute name.
        value : attribute value.
        repo : str
            The config file section of the attribute.
        _force : bool, optional
            If False, an error is raised if the attribute is already defined
            (prevent overwrite).
        """
        # _force = True => bypass the raise of an error
        if repo in self and not _force:
            if key in self[repo]:
                self._logger.error(f"Trying to modify config on: [{repo}], "
                                   f"{key} to {value}")
                self._logger.error(f"Config file on: {self._path}.")
                raise KeyError("Modifying an already existing parameter.")
        if value is None:
            value = "none"
        elif value is False:
            value = "false"
        elif value is True:
            value = "true"
        elif not isinstance(value, str):
            value = str(value)
        try:
            self._config[repo][key] = value
        except KeyError:
            # this other try is for when the section doesn't exist
            self._config[repo] = {key: value}
        with open(self._path, "w") as f:
            self._config.write(f)

    def get_build(self, build):
        """Returns the config entry for a build. Makes some check to see
        if the config file section has been well written by user.
        """
        if build not in self:
            raise ValueError(f"Build entry {build} not in config file!")
        entry = self[build]
        return entry

    def set_all_defaults(self):
        for reponame, repo in self.default_values.items():
            for attr in repo:
                self.set_default(attr, reponame)

    def set_default(self, attr, entry):
        # find its default value
        default = self.default_values[entry][attr]
        self.write(attr, default, entry)


class ConfigFileParser(BaseConfig):
    """Class that manages the abisuite config file.
    """
    _loggername = "ConfigFileParser"
    config_path = CONFIG_PATH
    default_values = CONFIG_ATTRIBUTES.copy()  # copy just to make sure
