from ..bases import BaseInputVariable
from ...routines import is_list_like


__SPECIAL_VARIABLE_ENDINGS__ = (":", "+", "*", "?")


class AbinitInputVariable(BaseInputVariable):
    """Class that represents an abinit input variable.
    """
    _loggername = "AbinitInputVariable"

    def __init__(self, *args, **kwargs):
        """Abinit input variable init method.
        """
        self._basename = None
        super().__init__(*args, **kwargs)

    @property
    def basename(self):
        if self._basename is not None:
            return self._basename
        clean = self.name.strip("".join(__SPECIAL_VARIABLE_ENDINGS__))
        while clean[-1].isdigit():
            # remove datasets labels
            clean = clean[:-1]
        self._basename = clean
        return clean

    def __str__(self):
        # compute string representation of the variable
        string = f" {self.name}"
        # if value is a single string, int or float just append it
        if self.is_value_a_scalar_or_str():
            return string + f" {str(self.value)}\n"
        if is_list_like(self.value):
            if self.is_value_a_vector():
                return (string + self._spaces +
                        self.convert_vector_to_str(self.value)
                        + "\n")
            # else, we are a 2d array
            if self.is_value_a_2d_array():
                startspace = len(string)
                return (string + "\n" +
                        self.convert_2d_arr_to_str(self.value,
                                                   startspace=startspace) +
                        "\n")
        else:
            raise TypeError(f"Unsupported data type for input file:"
                            f" {self.name}, typ={type(self.value)}")
