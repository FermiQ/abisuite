from ..bases import (BaseInputVariable, variableDBIntegrityChecker,
                     MANDATORY_VARIABLE_KEYS, OPTIONAL_VARIABLE_KEYS)


class AbinitOpticInputVariable(BaseInputVariable):
    """Class that represents an optic input variable.
    """
    _loggername = "AbinitOpticInputVariable"

    def __str__(self):
        string = f" {self.name} = "
        if isinstance(self.value, str):
            return string + "'" + self.value + "',\n"
        elif isinstance(self.value, float) or isinstance(self.value, int):
            return string + str(self.value) + ",\n"
        # else it is a list/tuple
        val = ",".join([str(x) for x in self.value])
        return string + val + ",\n"


ALLOWED_ABINITOPTIC_BLOCKS = ("files", "parameters", "computations")
RAW_ABINITOPTIC_VARIABLES = {
        "broadening": {
            "block": "parameters",
            "class": AbinitOpticInputVariable,
            "mandatory": False,
            "min": 0.0,
            "type": float,
            },
        "ddkfile_1": {
            "block": "files",
            "class": AbinitOpticInputVariable,
            "mandatory": True,
            "type": str,
            },
        "ddkfile_2": {
            "block": "files",
            "class": AbinitOpticInputVariable,
            "mandatory": True,
            "type": str,
            },
        "ddkfile_3": {
            "block": "files",
            "class": AbinitOpticInputVariable,
            "mandatory": True,
            "type": str,
            },
        "domega": {
            "block": "parameters",
            "class": AbinitOpticInputVariable,
            "mandatory": False,
            "min": 0.0,
            "type": float,
            },
        "lin_comp": {
            "block": "computations",
            "class": AbinitOpticInputVariable,
            "mandatory": False,
            "type": "vector",
            },
        "maxomega": {
            "block": "parameters",
            "class": AbinitOpticInputVariable,
            "mandatory": False,
            "min": 0.0,
            "type": float,
            },
        "nonlin_comp": {
            "block": "computations",
            "class": AbinitOpticInputVariable,
            "mandatory": False,
            "type": "vector",
            },
        "num_lin_comp": {
            "block": "computations",
            "class": AbinitOpticInputVariable,
            "mandatory": False,
            "type": int,
            },
        "num_linel_comp": {
            "block": "computations",
            "class": AbinitOpticInputVariable,
            "mandatory": False,
            "type": int,
            },
        "num_nonlin_comp": {
            "block": "computations",
            "class": AbinitOpticInputVariable,
            "mandatory": False,
            "type": int,
            },
        "num_nonlin2_comp": {
            "block": "computations",
            "class": AbinitOpticInputVariable,
            "mandatory": False,
            "type": int,
            },
        "scissor": {
            "block": "parameters",
            "class": AbinitOpticInputVariable,
            "mandatory": False,
            "type": float,
            },
        "tolerance": {
            "block": "parameters",
            "class": AbinitOpticInputVariable,
            "mandatory": False,
            "min": 0.0,
            "type": float,
            },
        "wfkfile": {
            "block": "files",
            "class": AbinitOpticInputVariable,
            "mandatory": True,
            "type": str,
            },
        }

ALL_ABINITOPTIC_VARIABLES = variableDBIntegrityChecker(
        RAW_ABINITOPTIC_VARIABLES,
        ALLOWED_ABINITOPTIC_BLOCKS,
        MANDATORY_VARIABLE_KEYS,
        [AbinitOpticInputVariable],
        optional_keys=OPTIONAL_VARIABLE_KEYS)
