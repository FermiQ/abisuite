from .bases import AbinitInputVariable
from ..bases import (
        variableDBIntegrityChecker,
        MANDATORY_VARIABLE_KEYS, OPTIONAL_VARIABLE_KEYS
        )


ALLOWED_ABINIT_ANADDB_BLOCKS = (
        "flags", "Wavevector grid", "Effective charges",
        "Interatomic force constants", "Phonon band structure",
        )

RAW_ABINIT_ANADDB_INPUT_VARIABLES = {
        "asr": {
            "allowed": (0, 1, 2),
            "block": "Interatomic force constants",
            "class": AbinitInputVariable,
            "mandatory": False,
            "type": int,
            },
        "atifc": {
            "block": "Interatomic force constants",
            "class": AbinitInputVariable,
            "mandatory": False,
            "type": "vector",
            },
        "brav": {
            "allowed": (-1, 1, 2, 3, 4),
            "block": "Wavevector grid",
            "class": AbinitInputVariable,
            "mandatory": False,
            "type": int,
            },
        "chneut": {
            "allowed": (0, 1, 2),
            "block": "Effective charges",
            "class": AbinitInputVariable,
            "mandatory": False,
            "type": int,
            },
        "dipdip": {
            "allowed": (0, 1),
            "block": "Interatomic force constants",
            "class": AbinitInputVariable,
            "mandatory": False,
            "type": int,
            },
        "eivec": {
            "allowed": (0, 1, 2, 4),
            "block": "Phonon band structure",
            "class": AbinitInputVariable,
            "mandatory": False,
            "type": int,
            },
        "ifcana": {
            "allowed": (0, 1),
            "block": "Interatomic force constants",
            "class": AbinitInputVariable,
            "mandatory": False,
            "type": int,
            },
        "ifcflag": {
            "allowed": (0, 1),
            "block": "flags",
            "class": AbinitInputVariable,
            "mandatory": False,
            "type": int,
            },
        "ifcout": {
            "block": "Interatomic force constants",
            "class": AbinitInputVariable,
            "mandatory": False,
            "min": 0,
            "type": int,
            },
        "natifc": {
            "block": "Interatomic force constants",
            "class": AbinitInputVariable,
            "mandatory": False,
            "min": 0,
            "type": int,
            },
        "ngqpt": {
            "block": "Wavevector grid",
            "class": AbinitInputVariable,
            "mandatory": False,
            "type": "vector",
            },
        "nph1l": {
            "block": "Phonon band structure",
            "class": AbinitInputVariable,
            "mandatory": False,
            "min": 0,
            "type": int,
            },
        "nph2l": {
            "block": "Phonon band structure",
            "class": AbinitInputVariable,
            "mandatory": False,
            "min": 0,
            "type": int,
            },
        "nqpath": {
            "block": "Phonon band structure",
            "class": AbinitInputVariable,
            "mandatory": False,
            "min": 1,
            "type": int,
            },
        "nqshft": {
            "block": "Wavevector grid",
            "class": AbinitInputVariable,
            "mandatory": False,
            "min": 0,
            "type": int,
            },
        "q1shft": {
            "block": "Wavevector grid",
            "class": AbinitInputVariable,
            "mandatory": False,
            "type": "vector",
            },
        "qpath": {
            "block": "Phonon band structure",
            "class": AbinitInputVariable,
            "mandatory": False,
            "type": "2darr",
            },
        "qph1l": {
            "block": "Phonon band structure",
            "class": AbinitInputVariable,
            "mandatory": False,
            "type": "2darr",
            },
        "qph2l": {
            "block": "Phonon band structure",
            "class": AbinitInputVariable,
            "mandatory": False,
            "type": "2darr",
            },
        }


ALL_ABINIT_ANADDB_VARIABLES = variableDBIntegrityChecker(
        RAW_ABINIT_ANADDB_INPUT_VARIABLES,
        ALLOWED_ABINIT_ANADDB_BLOCKS,
        MANDATORY_VARIABLE_KEYS,
        [AbinitInputVariable],
        optional_keys=OPTIONAL_VARIABLE_KEYS)
