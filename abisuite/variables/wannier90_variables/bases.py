from ..bases import BaseInputVariable


class Wannier90InputVariable(BaseInputVariable):
    """The class for a regular wannier90.x input variable.
    """
    _loggername = "Wannier90InputVariable"

    def __str__(self):
        if self.is_value_a_vector():
            return self.name + " = " + " ".join([str(x) for x in self.value])
        return self.name + " = " + str(self.value) + "\n"
