from .bases import (
        BaseInputVariable,
        variableDBIntegrityChecker, MANDATORY_VARIABLE_KEYS,
        OPTIONAL_VARIABLE_KEYS
        )


class GenericInputVariable(BaseInputVariable):
    """Generic input variable."""
    _loggername = "GenericInputVariable"


GENERIC_ALLOWED_BLOCKS = ("generic", )
GENERIC_ALLOWED_CLASSES = (GenericInputVariable, )
RAW_GENERIC_VARIABLES = {
        "generic_variable": {
            "block": "generic",
            "mandatory": False,
            "class": GenericInputVariable,
            }
    }

GENERIC_INPUT_VARIABLES_DB = variableDBIntegrityChecker(
        RAW_GENERIC_VARIABLES,
        GENERIC_ALLOWED_BLOCKS,
        MANDATORY_VARIABLE_KEYS,
        GENERIC_ALLOWED_CLASSES,
        optional_keys=OPTIONAL_VARIABLE_KEYS
        )
