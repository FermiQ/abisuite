from .bases import CommonQEInputVariable
from ..bases import (MANDATORY_VARIABLE_KEYS, OPTIONAL_VARIABLE_KEYS,
                     variableDBIntegrityChecker)


# #############################################################################
# ############## Quantum Espresso projwfc.x script variables ##################
# #############################################################################


QEPROJWFC_ALLOWED_BLOCKS = ("projwfc", )
QEPROJWFC_ALLOWED_CLASSES = (CommonQEInputVariable, )
RAW_QEPROJWFC_VARIABLES = {
        "degauss": {
            "block": "projwfc",
            "class": CommonQEInputVariable,
            "mandatory": True,
            "type": float,
            },
        "DeltaE": {
            "block": "projwfc",
            "class": CommonQEInputVariable,
            "mandatory": True,
            "type": float,
            },
        "Emax": {
            "block": "projwfc",
            "class": CommonQEInputVariable,
            "mandatory": True,
            "type": float,
            },
        "Emin": {
            "block": "projwfc",
            "class": CommonQEInputVariable,
            "mandatory": True,
            "type": float,
            },
        "filpdos": {
            "block": "projwfc",
            "class": CommonQEInputVariable,
            "mandatory": True,
            "type": str,
            },
        "filproj": {
            "block": "projwfc",
            "class": CommonQEInputVariable,
            "mandatory": True,
            "type": str,
            },
        "irmax": {
            "block": "projwfc",
            "class": CommonQEInputVariable,
            "mandatory": False,
            "type": "2darr",
            },
        "irmin": {
            "block": "projwfc",
            "class": CommonQEInputVariable,
            "mandatory": False,
            "type": "2darr",
            },
        "kresolveddos": {
            "block": "projwfc",
            "class": CommonQEInputVariable,
            "mandatory": False,
            "type": bool,
            },
        "lbinary_data": {
            "block": "projwfc",
            "class": CommonQEInputVariable,
            "mandatory": False,
            "type": bool,
            },
        "lsym": {
            "block": "projwfc",
            "class": CommonQEInputVariable,
            "mandatory": False,
            "type": bool,
            },
        "lwrite_overlaps": {
            "block": "projwfc",
            "class": CommonQEInputVariable,
            "mandatory": False,
            "type": bool,
            },
        "ngauss": {
            "allowed": (0, 1, -1, -99),
            "block": "projwfc",
            "class": CommonQEInputVariable,
            "mandatory": False,
            "type": int,
            },
        "n_proj_boxes": {
            "block": "projwfc",
            "class": CommonQEInputVariable,
            "mandatory": False,
            "min": 1,
            "type": int,
            },
        "outdir": {
            "block": "projwfc",
            "class": CommonQEInputVariable,
            "mandatory": True,
            "type": str
            },
        "pawproj": {
            "block": "projwfc",
            "class": CommonQEInputVariable,
            "mandatory": False,
            "type": bool,
            },
        "plotboxes": {
            "block": "projwfc",
            "class": CommonQEInputVariable,
            "mandatory": False,
            "type": bool,
            },
        "prefix": {
            "block": "projwfc",
            "class": CommonQEInputVariable,
            "mandatory": True,
            "type": str
            },
        "tdosinboxes": {
            "block": "projwfc",
            "class": CommonQEInputVariable,
            "mandatory": False,
            "type": bool,
            },
        }

ALL_QEPROJWFC_VARIABLES = variableDBIntegrityChecker(
        RAW_QEPROJWFC_VARIABLES,
        QEPROJWFC_ALLOWED_BLOCKS,
        MANDATORY_VARIABLE_KEYS,
        QEPROJWFC_ALLOWED_CLASSES,
        optional_keys=OPTIONAL_VARIABLE_KEYS)
