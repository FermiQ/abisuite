from .bases import CommonQEInputVariable
from ..bases import (MANDATORY_VARIABLE_KEYS, OPTIONAL_VARIABLE_KEYS,
                     variableDBIntegrityChecker)
from ...routines import is_list_like, is_scalar_or_str, is_vector
import numpy as np


class QPointsMatdynVar(CommonQEInputVariable):
    """Special variable class for q_points.

    Expected syntax:
    'q_points' = {'qpts': [list of qpoitns coordinates],
                  'nqpts': [list of nqs between each qpts]}
    """
    @CommonQEInputVariable.value.setter
    def value(self, value):
        if not isinstance(value, dict):
            raise TypeError(f"Expected dict for q_points but got: {value}.")
        if "qpts" not in value:
            raise ValueError("Specify 'qpts' for q_points (list of qpts).")
        if "nqpts" not in value:
            raise ValueError("Specify 'nqpts' for q_points (list of # of qpts"
                             " between each qpts.")
        if not is_list_like(value["qpts"]):
            raise TypeError(f"Expected a list for 'qpts' but got "
                            f"{value['qpts']}")
        if is_vector(value["qpts"]):
            # only one qpt convert into 2D array
            value["qpts"] = np.array([value["qpts"]], dtype=float)
        if is_scalar_or_str(value["nqpts"]):
            # only one element, convert into a vector
            value["nqpts"] = np.array([value["nqpts"]])
        if not is_vector(value["nqpts"]):
            raise TypeError("Expected a vector for 'nqpts' but got "
                            f"{value['nqpts']}")
        if len(value["qpts"]) != len(value["nqpts"]):
            raise ValueError("'nqpts' and 'qpts' must have same length.")
        CommonQEInputVariable.value.fset(self, value)

    def __str__(self):
        # must convert into:
        # total number of qpts
        # qpt1 nqpt1
        # qpt2 nqpt2
        # ...
        # don't use convert_2d_arr_to_str here cause numpy arrays
        # will force the last column to be floats instead of ints
        string = f"{len(self.value['qpts'])}\n"
        for qpt, nqpt in zip(self.value["qpts"], self.value["nqpts"]):
            towrite = qpt + [nqpt]
            string += self.convert_vector_to_str(towrite) + "\n"
        return string


# #############################################################################
# ############### Quantum Espresso matdyn.x script variables ##################
# #############################################################################

# there is no doc on the input variables on the QE website.
# need to go directly into the source code...

QEMATDYN_ALLOWED_BLOCKS = ("input", None)
QEMATDYN_ALLOWED_CLASSES = (CommonQEInputVariable, QPointsMatdynVar)
RAW_QEMATDYN_VARIABLES = {
            "asr": {
                "allowed": ("no", "simple", "crystal",
                            "one-dim", "zero-dim"),
                "block": "input",
                "class": CommonQEInputVariable,
                "mandatory": False,
                "type": str
                },
            "dos": {
                "block": "input",
                "class": CommonQEInputVariable,
                "mandatory": False,
                "type": bool,
                },
            "fleig": {
                "block": "input",
                "class": CommonQEInputVariable,
                "mandatory": False,
                "path_convertible": True,
                "type": str
                },
            "flfrc": {
                "block": "input",
                "class": CommonQEInputVariable,
                "mandatory": True,
                "path_convertible": True,
                "type": str
                },
            "flfrq": {
                "block": "input",
                "class": CommonQEInputVariable,
                "mandatory": True,
                "path_convertible": True,
                "type": str
                },
            "flvec": {
                "block": "input",
                "class": CommonQEInputVariable,
                "mandatory": False,
                "path_convertible": True,
                "type": str
                },
            "nk1": {
                "block": "input",
                "class": CommonQEInputVariable,
                "mandatory": False,
                "min": 1,
                "type": int,
                },
            "nk2": {
                "block": "input",
                "class": CommonQEInputVariable,
                "mandatory": False,
                "min": 1,
                "type": int,
                },
            "nk3": {
                "block": "input",
                "class": CommonQEInputVariable,
                "mandatory": False,
                "min": 1,
                "type": int,
                },
            "q_in_band_form": {
                "block": "input",
                "class": CommonQEInputVariable,
                "mandatory": False,
                "type": bool
                },
            "q_in_cryst_coord": {
                "block": "input",
                "class": CommonQEInputVariable,
                "mandatory": False,
                "type": bool,
                },
            "q_points": {
                "block": None,
                "class": QPointsMatdynVar,
                "mandatory": False,
                },
           }

ALL_QEMATDYN_VARIABLES = variableDBIntegrityChecker(
        RAW_QEMATDYN_VARIABLES,
        QEMATDYN_ALLOWED_BLOCKS,
        MANDATORY_VARIABLE_KEYS,
        QEMATDYN_ALLOWED_CLASSES,
        optional_keys=OPTIONAL_VARIABLE_KEYS)
