from .bases import CommonQEInputVariable
from ..bases import (MANDATORY_VARIABLE_KEYS, OPTIONAL_VARIABLE_KEYS,
                     variableDBIntegrityChecker)


# #############################################################################
# ################ Quantum Espresso fs.x script variables #####################
# #############################################################################


QEFS_ALLOWED_BLOCKS = ("fermi", )
QEFS_ALLOWED_CLASSES = (CommonQEInputVariable, )
RAW_QEFS_VARIABLES = {
        "outdir": {
            "block": "fermi",
            "class": CommonQEInputVariable,
            "mandatory": True,
            "type": str
            },
        "prefix": {
            "block": "fermi",
            "class": CommonQEInputVariable,
            "mandatory": True,
            "type": str
            },
        }

ALL_QEFS_VARIABLES = variableDBIntegrityChecker(
        RAW_QEFS_VARIABLES,
        QEFS_ALLOWED_BLOCKS,
        MANDATORY_VARIABLE_KEYS,
        QEFS_ALLOWED_CLASSES,
        optional_keys=OPTIONAL_VARIABLE_KEYS)
