from .bases import CommonQEInputVariable
from ..bases import (
        MANDATORY_VARIABLE_KEYS, OPTIONAL_VARIABLE_KEYS,
        variableDBIntegrityChecker,
        )


# #############################################################################
# ########### Quantum Espresson pw2wannier90.x script variables ###############
# #############################################################################

QEPW2WANNIER90_ALLOWED_BLOCKS = ("inputpp", )
QEPW2WANNIER90_ALLOWED_CLASSES = (CommonQEInputVariable, )
RAW_QEPW2WANNIER90_VARIABLES = {
        "outdir": {
            "block": "inputpp",
            "class": CommonQEInputVariable,
            "path_convertible": True,
            "mandatory": True,
            "type": str,
            },
        "prefix": {
            "block": "inputpp",
            "class": CommonQEInputVariable,
            "mandatory": True,
            "type": str,
            },
        "reduce_unk": {
            "block": "inputpp",
            "class": CommonQEInputVariable,
            "mandatory": False,
            "type": bool,
            },
        "seedname": {
            "block": "inputpp",
            "class": CommonQEInputVariable,
            "mandatory": True,
            "type": str,
            },
        "spin_component": {
            "allowed": ("none", "up", "down"),
            "block": "inputpp",
            "class": CommonQEInputVariable,
            "mandatory": True,
            "type": str,
            },
        "write_amn": {
            "block": "inputpp",
            "class": CommonQEInputVariable,
            "mandatory": False,
            "type": bool,
            },
        "write_mmn": {
            "block": "inputpp",
            "class": CommonQEInputVariable,
            "mandatory": False,
            "type": bool,
            },
        "write_spn": {
            "block": "inputpp",
            "class": CommonQEInputVariable,
            "mandatory": False,
            "type": bool,
            },
        "write_unk": {
            "block": "inputpp",
            "class": CommonQEInputVariable,
            "mandatory": False,
            "type": bool,
            },
        }


ALL_QEPW2WANNIER90_VARIABLES = variableDBIntegrityChecker(
        RAW_QEPW2WANNIER90_VARIABLES,
        QEPW2WANNIER90_ALLOWED_BLOCKS,
        MANDATORY_VARIABLE_KEYS,
        QEPW2WANNIER90_ALLOWED_CLASSES,
        optional_keys=OPTIONAL_VARIABLE_KEYS)
