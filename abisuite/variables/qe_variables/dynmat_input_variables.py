from .bases import CommonQEInputVariable
from ..bases import (MANDATORY_VARIABLE_KEYS, OPTIONAL_VARIABLE_KEYS,
                     variableDBIntegrityChecker)


# #############################################################################
# ############### Quantum Espresso dynmat.x script variables ##################
# #############################################################################

QEDYNMAT_ALLOWED_BLOCKS = ("input", )
QEDYNMAT_ALLOWED_CLASSES = (CommonQEInputVariable, )
RAW_QEDYNMAT_VARIABLES = {"asr": {"allowed": ("crystal", "no", "one-dim",
                                              "simple", "zero-dim"),
                                  "block": "input",
                                  "class": CommonQEInputVariable,
                                  "mandatory": False,
                                  "type": str},
                          "fildyn": {"block": "input",
                                     "class": CommonQEInputVariable,
                                     "mandatory": True,
                                     "type": str},
                          "lperm": {"block": "input",
                                    "class": CommonQEInputVariable,
                                    "mandatory": False,
                                    "type": bool},
                          "q(1)": {"block": "input",
                                   "class": CommonQEInputVariable,
                                   "mandatory": False,
                                   "type": float},
                          "q(2)": {"block": "input",
                                   "class": CommonQEInputVariable,
                                   "mandatory": False,
                                   "type": float},
                          "q(3)": {"block": "input",
                                   "class": CommonQEInputVariable,
                                   "mandatory": False,
                                   "type": float},
                          }

ALL_QEDYNMAT_VARIABLES = variableDBIntegrityChecker(
        RAW_QEDYNMAT_VARIABLES,
        QEDYNMAT_ALLOWED_BLOCKS,
        MANDATORY_VARIABLE_KEYS,
        QEDYNMAT_ALLOWED_CLASSES,
        optional_keys=OPTIONAL_VARIABLE_KEYS)
