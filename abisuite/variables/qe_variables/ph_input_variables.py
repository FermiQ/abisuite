from .bases import CommonQEInputVariable
from ..bases import (variableDBIntegrityChecker, MANDATORY_VARIABLE_KEYS,
                     OPTIONAL_VARIABLE_KEYS)


class QPointsVar(CommonQEInputVariable):
    """Special variable class for q_points.
    """
    def __str__(self):
        if self.is_value_a_vector():
            return self.convert_vector_to_str(self.value) + "\n"
        if self.is_value_a_2d_array():
            return "\n".join([self.convert_vector_to_str(x)
                              for x in self.value]) + "\n"
        raise TypeError(f"Expected vector or 2d arr got: {self.value}")


# #############################################################################
# ################# Quantum Espresso ph.x script variables ####################
# #############################################################################

QEPH_ALLOWED_BLOCKS = (None, "inputph")
QEPH_ALLOWED_CLASSES = (CommonQEInputVariable, QPointsVar)
RAW_QEPH_VARIABLES = {
        "alpha_mix(*)": {
            "block": "inputph",
            "class": CommonQEInputVariable,
            "mandatory": False,
            "min": 0.0,
            "max": 1.0,
            "type": float,
            },
        "amass(*)": {
            "block": "inputph",
            "class": CommonQEInputVariable,
            "mandatory": False,
            "type": float
            },
        "epsil": {
            "block": "inputph",
            "class": CommonQEInputVariable,
            "mandatory": False,
            "type": bool,
            },
        "fildvscf": {
           "block": "inputph",
           "class": CommonQEInputVariable,
           "mandatory": True,
           "type": str},
        "fildyn": {
           "block": "inputph",
           "class": CommonQEInputVariable,
           "mandatory": True,
           "path_convertible": True,
           "type": str},
        "last_irr": {
            "block": "inputph",
            "class": CommonQEInputVariable,
            "mandatory": False,
            "min": 1,
            "type": int,
            },
        "last_q": {
            "block": "inputph",
            "class": CommonQEInputVariable,
            "mandatory": False,
            "min": 1,
            "type": int,
            },
        "ldisp": {
            "block": "inputph",
            "class": CommonQEInputVariable,
            "mandatory": False,
            "type": bool,
            },
        "niter_ph": {
            "block": "inputph",
            "class": CommonQEInputVariable,
            "mandatory": False,
            "min": 1,
            "max": 100,  # this limit is hardcoded in QE
            "type": int,
            },
        "nmix_ph": {
            "block": "inputph",
            "class": CommonQEInputVariable,
            "mandatory": False,
            "min": 1,  # See q-e/PHonon/PH/phq_readin.f90
            "max": 4,  # https://gitlab.com/QEF/q-e/blob/develop/PHonon/PH/phq_readin.f90#L368  # noqa
            "type": int,
            },
        "nq1": {
            "block": "inputph",
            "class": CommonQEInputVariable,
            "mandatory": False,
            "min": 1,
            "type": int
            },
        "nq2": {
            "block": "inputph",
            "class": CommonQEInputVariable,
            "mandatory": False,
            "min": 1,
            "type": int,
            },
        "nq3": {
            "block": "inputph",
            "class": CommonQEInputVariable,
            "mandatory": False,
            "min": 1,
            "type": int
            },
        "outdir": {
            "block": "inputph",
            "class": CommonQEInputVariable,
            "mandatory": True,
            "path_convertible": True,
            "type": str
            },
        "prefix": {
            "block": "inputph",
            "class": CommonQEInputVariable,
            "mandatory": True,
            "type": str
            },
        "q_points": {
            "block": None,
            "class": QPointsVar,
            "mandatory": False
            },
        "recover": {
            "block": "inputph",
            "class": CommonQEInputVariable,
            "mandatory": False,
            "type": bool,
            },
        "start_irr": {
            "block": "inputph",
            "class": CommonQEInputVariable,
            "mandatory": False,
            "min": 1,
            "type": int,
            },
        "start_q": {
            "block": "inputph",
            "class": CommonQEInputVariable,
            "mandatory": False,
            "min": 1,
            "type": int,
            },
        "title": {
            "block": None,
            "class": CommonQEInputVariable,
            "mandatory": True,
            "type": str,
            },
        "tr2_ph": {
            "block": "inputph",
            "class": CommonQEInputVariable,
            "mandatory": False,
            "min": 0.0,
            "type": float
            },
        "verbosity": {
            "allowed": ("debug", "default", "low",
                        "medium", "minimal", "high"),
            "block": "inputph",
            "class": CommonQEInputVariable,
            "mandatory": False,
            "type": str,
            },
        "zeu": {
            "block": "inputph",
            "class": CommonQEInputVariable,
            "mandatory": False,
            "type": bool,
             },
        }


ALL_QEPH_VARIABLES = variableDBIntegrityChecker(
        RAW_QEPH_VARIABLES,
        QEPH_ALLOWED_BLOCKS,
        MANDATORY_VARIABLE_KEYS,
        QEPH_ALLOWED_CLASSES,
        optional_keys=OPTIONAL_VARIABLE_KEYS)
