from ..bases import BaseInputVariable
from ...routines import is_list_like


class CommonQEInputVariable(BaseInputVariable):
    """Base class for a Quantum Espresso input variable.
    """
    _spaces = " "  # space between array elements
    _loggername = "CommonQEInputVariable"

    def __str__(self):
        string = self._spaces + self.name + " = "
        if isinstance(self.value, str):
            # surround value with quotation marks
            string += "'" + self.value + "',\n"
        elif is_list_like(self.value):
            string += " ".join([str(x) for x in self.value]) + ",\n"
        else:
            string += str(self.value) + ",\n"
        return string
