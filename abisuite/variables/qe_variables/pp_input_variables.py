from .bases import CommonQEInputVariable
from ..bases import (MANDATORY_VARIABLE_KEYS, OPTIONAL_VARIABLE_KEYS,
                     variableDBIntegrityChecker)


# #############################################################################
# ################# Quantum Espresso pp.x script variables ####################
# #############################################################################


QEPP_ALLOWED_BLOCKS = ("inputpp", "plot")
QEPP_ALLOWED_CLASSES = (CommonQEInputVariable, )
RAW_QEPP_VARIABLES = {
        "fileout": {
            "block": "plot",
            "class": CommonQEInputVariable,
            "mandatory": True,
            "type": str,
            },
        "filplot": {
            "block": "inputpp",
            "class": CommonQEInputVariable,
            "mandatory": True,
            "type": str
            },
        "iflag": {
            "block": "plot",
            "class": CommonQEInputVariable,
            "mandatory": True,
            "type": int,
            },
        "kband": {
            "block": "inputpp",
            "class": CommonQEInputVariable,
            "mandatory": True,
            "type": int,
            },
        "kpoint": {
            "block": "inputpp",
            "class": CommonQEInputVariable,
            "mandatory": True,
            "type": int,
            },
        "lsign": {
            "block": "inputpp",
            "class": CommonQEInputVariable,
            "mandatory": True,
            "type": bool,
            },
        "outdir": {
            "block": "inputpp",
            "class": CommonQEInputVariable,
            "mandatory": True,
            "type": str
            },
        "output_format": {
            "block": "plot",
            "class": CommonQEInputVariable,
            "mandatory": True,
            "type": int,
            },
        "plot_num": {
            "block": "inputpp",
            "class": CommonQEInputVariable,
            "mandatory": True,
            "type": int,
            },
        "prefix": {
            "block": "inputpp",
            "class": CommonQEInputVariable,
            "mandatory": True,
            "type": str
            },
        }

ALL_QEPP_VARIABLES = variableDBIntegrityChecker(
        RAW_QEPP_VARIABLES,
        QEPP_ALLOWED_BLOCKS,
        MANDATORY_VARIABLE_KEYS,
        QEPP_ALLOWED_CLASSES,
        optional_keys=OPTIONAL_VARIABLE_KEYS)
