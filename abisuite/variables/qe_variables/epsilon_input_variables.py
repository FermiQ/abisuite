from .bases import CommonQEInputVariable
from ..bases import (MANDATORY_VARIABLE_KEYS, OPTIONAL_VARIABLE_KEYS,
                     variableDBIntegrityChecker)


# #############################################################################
# ############## Quantum Espresso epsilon.x script variables ##################
# #############################################################################


QEEPSILON_ALLOWED_BLOCKS = ("inputpp", "energy_grid")
QEEPSILON_ALLOWED_CLASSES = (CommonQEInputVariable, )
RAW_QEEPSILON_VARIABLES = {
        "calculation": {
            "allowed": ("eps", ),
            "block": "inputpp",
            "class": CommonQEInputVariable,
            "mandatory": True,
            "type": str,
            },
        "intersmear": {
            "block": "energy_grid",
            "class": CommonQEInputVariable,
            "mandatory": True,
            "type": float,
            },
        "nw": {
            "block": "energy_grid",
            "class": CommonQEInputVariable,
            "mandatory": True,
            "type": int,
            },
        "outdir": {
            "block": "inputpp",
            "class": CommonQEInputVariable,
            "mandatory": True,
            "type": str
            },
        "prefix": {
            "block": "inputpp",
            "class": CommonQEInputVariable,
            "mandatory": True,
            "type": str
            },
        "smeartype": {
            "allowed": ("gauss", ),
            "block": "energy_grid",
            "class": CommonQEInputVariable,
            "mandatory": True,
            "type": str,
            },
        "wmax": {
            "block": "energy_grid",
            "class": CommonQEInputVariable,
            "mandatory": True,
            "type": float,
            },
        "wmin": {
            "block": "energy_grid",
            "class": CommonQEInputVariable,
            "mandatory": True,
            "type": float,
            },
        }

ALL_QEEPSILON_VARIABLES = variableDBIntegrityChecker(
        RAW_QEEPSILON_VARIABLES,
        QEEPSILON_ALLOWED_BLOCKS,
        MANDATORY_VARIABLE_KEYS,
        QEEPSILON_ALLOWED_CLASSES,
        optional_keys=OPTIONAL_VARIABLE_KEYS)
