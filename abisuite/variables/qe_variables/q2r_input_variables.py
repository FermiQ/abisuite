from .bases import CommonQEInputVariable
from ..bases import (MANDATORY_VARIABLE_KEYS, OPTIONAL_VARIABLE_KEYS,
                     variableDBIntegrityChecker)


# #############################################################################
# ################# Quantum Espresso q2r.x script variables ###################
# #############################################################################

# No doc on the input variables on the QE website. Need to go directly
# into the source code for the docs.

QEQ2R_ALLOWED_BLOCKS = ("input", )
QEQ2R_ALLOWED_CLASSES = (CommonQEInputVariable, )
RAW_QEQ2R_VARIABLES = {
                "fildyn": {
                    "block": "input",
                    "class": CommonQEInputVariable,
                    "mandatory": True,
                    "path_convertible": True,
                    "type": str
                    },
                "flfrc": {
                   "block": "input",
                   "class": CommonQEInputVariable,
                   "mandatory": True,
                   "path_convertible": True,
                   "type": str
                   },
                "zasr": {
                   "allowed": ["no", "simple", "crystal", "one-dim",
                               "zero-dim"],
                   "block": "input",
                   "class": CommonQEInputVariable,
                   "mandatory": False,
                   "type": str,
                   },
              }

ALL_QEQ2R_VARIABLES = variableDBIntegrityChecker(
        RAW_QEQ2R_VARIABLES,
        QEQ2R_ALLOWED_BLOCKS,
        MANDATORY_VARIABLE_KEYS,
        QEQ2R_ALLOWED_CLASSES,
        optional_keys=OPTIONAL_VARIABLE_KEYS)
