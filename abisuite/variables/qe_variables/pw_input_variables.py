from .bases import CommonQEInputVariable
from ..bases import (variableDBIntegrityChecker, MANDATORY_VARIABLE_KEYS,
                     OPTIONAL_VARIABLE_KEYS)
from ...routines import is_list_like, is_2d_arr
import numpy as np


# TODO: put all checks into the value.setter method instead of the
# __str__ method!


class CellParametersVar(CommonQEInputVariable):
    """Special variable class for cell_parameters.
    """
    def __str__(self):
        string = self.name.upper() + "\n"
        # should be a 2D array
        if not self.is_value_a_2d_array():
            raise ValueError("cell_parameters should be a 2D array but"
                             f" received {self.value}")
        string += self.convert_2d_arr_to_str(self.value)
        return string


class KPointsVar(CommonQEInputVariable):
    """Special variable for k_points.
    """

    def __getitem__(self, item):
        return self.value[item]

    def __setitem__(self, key, item):
        self.value[key] = item

    @property
    def parameter(self):
        return self.value["parameter"]

    @parameter.setter
    def parameter(self, parameter):
        self.value["parameter"] = parameter

    @property
    def k_points(self):
        return self.value["k_points"]

    @k_points.setter
    def k_points(self, k_points):
        self.value["k_points"] = k_points

    @CommonQEInputVariable.value.setter
    def value(self, value):
        parameter = value["parameter"]
        if parameter not in ("automatic", "gamma"):
            if "weights" not in value:
                raise ValueError("Need to specify 'weights' in k-points")
            # need to set nks if not set
            nks = len(value["k_points"])
            coordinates = np.array(value["k_points"])
            weights = value["weights"]
            if self.is_value_a_vector(value=coordinates):
                # if only one k-point still make a list of it
                coordinates = np.array([coordinates])
            if self.is_value_a_scalar_or_str(value=weights):
                # only one element
                weights = [weights]
            if len(weights) != nks:
                raise ValueError(f"len(weights) ({len(weights)}) != "
                                 f"nks ({nks})")
            if len(coordinates) != nks:
                raise ValueError(f"len(coordinates) ({len(coordinates)}) !="
                                 f" nks ({nks})")
            if "nks" not in value:
                value["nks"] = nks
        elif parameter == "automatic":
            kpts = value["k_points"]
            if len(kpts) != 6:
                # 6 is 3 ints for kgrid + 3 ints for kgrid shift
                raise ValueError(
                        "If k_points parameter is 'automatic', must give 3 int"
                        " for kgrid + 3 ints for kshift. But instead we got: "
                        f"'{kpts}'")
        CommonQEInputVariable.value.fset(self, value)

    def __str__(self):
        string = self.name.upper()
        if self.value["parameter"] == "automatic":
            # automatic generation of kpts
            return self._convert_automatic_to_str(string)
        elif self.value["parameter"] == "gamma":
            return self._convert_gamma_to_str(string)
        # else, it is normal k-point parameter
        return self._convert_to_str_normal_param(string)

    def _convert_automatic_to_str(self, string):
        value = self.value["k_points"]
        string += self._spaces + "automatic" + "\n"
        string += (self._spaces +
                   self.convert_vector_to_str(value) + "\n")
        return string

    def _convert_gamma_to_str(self, string):
        return string + self._spaces + "gamma" + "\n"

    def _convert_to_str_normal_param(self, string):
        parameter = self.value["parameter"]
        coordinates = np.array(self.value["k_points"])
        weights = self.value["weights"]
        nks = self.value["nks"]
        # concatenante weights and coordinates into a single 2D table
        arr_to_write = coordinates.tolist().copy()
        for row, w in zip(arr_to_write, weights):
            if parameter in ("tpiba_b", "crystal_b"):
                if not isinstance(w, int):
                    raise TypeError(f"For k-points param: '{parameter}', "
                                    "weights must be int, otherwise it might "
                                    "fail depending on compiler. Got: "
                                    f"{weights}")
            row.append(w)
        # arr_to_write = np.concatenate((coordinates, np.array([weights]).T),
        #                              axis=1)
        string += self._spaces + parameter + "\n"
        string += str(nks) + "\n"
        string += self.convert_2d_arr_to_str(arr_to_write,
                                             add_common_spaces=False)
        return string


class AtomicSpeciesVar(CommonQEInputVariable):
    """Special variable for atomic_species variable.
    """

    @CommonQEInputVariable.value.setter
    def value(self, value):
        if isinstance(value, dict):
            value = [value]
        for specie in value:
            if "atom" not in specie:
                raise ValueError("'atom' keyword should be present"
                                 " in all atomic species")
            if "atomic_mass" not in specie:
                raise ValueError("'atomic_mass' keyword should be present"
                                 " in all atomic species")
            if "pseudo" not in specie:
                raise ValueError("'pseudo' keyword should be present in"
                                 " all atomic species")
        CommonQEInputVariable.value.fset(self, value)

    def __str__(self):
        string = self.name.upper() + "\n"
        # value should be a list of dict like {'atom': 'Si',
        # 'atomic_mass': 28.086, 'pseudo': 'Si.pz-vbc.UPF}
        if not self.is_value_a_vector():
            raise TypeError("ATOMIC_SPECIES must be a vector of dict.")
        arr_2_write = []
        for specie in self.value:
            element = [specie["atom"], str(specie["atomic_mass"]),
                       specie["pseudo"]]
            arr_2_write.append(element)
            # string += self._spaces + specie["atom"]
            # string += self._spaces * 2 + str(specie["atomic_mass"])
            # string += self._spaces * 2 + specie["pseudo"] + "\n"
        return (string +
                self.convert_2d_arr_to_str(np.array(arr_2_write),
                                           add_common_spaces=False,
                                           startspace=1) + "\n")


class AtomicPositionsVar(CommonQEInputVariable):
    """Special variable class for atomic_positions

    This variable should be a dict with the following keys:
    'parameter' and 'positions'. The 'positions' key should also be a dict
    with whose keys are the atom symbols and values are the list of atomic
    positions for that atom.
    """
    _allowed_parameters = ("alat", "bohr", "angstrom", "crystal", "crystal_sg")

    def __getitem__(self, item):
        return self.value[item]

    @CommonQEInputVariable.value.setter
    def value(self, value):
        if not isinstance(value, dict):
            raise TypeError("atomic_positions should be a dict but"
                            f" got {value}")
        for key in ("parameter", "positions"):
            if key not in value:
                raise ValueError(f"'{key}' must be set in the"
                                 " atomic_positions.")
        if value["parameter"] not in self._allowed_parameters:
            raise ValueError("atomic_positions parameter not in allowed "
                             f" parameters: {self._allowed_parameters}")
        pos = value["positions"]
        if not isinstance(pos, dict):
            raise TypeError("'positions' must be a dict in 'atomic_positions'"
                            f" but got: {pos}")
        for key, val in pos.items():
            if not is_list_like(val):
                raise TypeError(f"positions must be a list but got {val}")
            if not is_2d_arr(val):
                val = [list(val)]
                pos[key] = val
            if np.shape(val)[-1] != 3:
                raise ValueError(f"positions must be 3-comp vectors but got:"
                                 f" {val}")
        CommonQEInputVariable.value.fset(self, value)

    def __str__(self):
        string = self.name.upper()
        parameter = self.value["parameter"]
        all_positions = self.value["positions"]
        string += self._spaces + parameter + "\n"
        # all_positions should be dict whose keys are the elements
        # and values are the positions of all this element positions
        arr_2_write = []
        for atom, positions in all_positions.items():
            positions = np.array(positions)
            for position in positions:
                arr_2_write.append([atom] + list(position))
        string += (self.convert_2d_arr_to_str(np.array(arr_2_write),
                                              add_common_spaces=False,
                                              startspace=1) + "\n")
        return string


# #############################################################################
# ################# Quantum Espresso pw.x script variables ####################
# #############################################################################

QEPW_ALLOWED_BLOCKS = (None, "cell", "control", "electrons", "system")
QEPW_ALLOWED_CLASSES = (AtomicPositionsVar, AtomicSpeciesVar,
                        CellParametersVar,
                        CommonQEInputVariable, KPointsVar)
RAW_QEPW_VARIABLES = {
        "atomic_positions": {
            "allowed": {"parameter": ("alat", "bohr",
                                      "angstrom", "crystal",
                                      "crystal_sg")},
            "block": None,
            "class": AtomicPositionsVar,
            "mandatory": True,
            "mandatory_keys": ("parameter", "positions"),
            "type": dict
            },
        "atomic_species": {
            "block": None,
            "class": AtomicSpeciesVar,
            "mandatory": True,
            "type": "vector",
            },
        "calculation": {
            "allowed": ("scf", "nscf", "bands",
                        "relax", "md",
                        "vc-relax", "vc-md"),
            "block": "control",
            "class": CommonQEInputVariable,
            "mandatory": False,  # default = 'scf'
            "type": str},
        "cell_parameters": {
            "block": None,
            "class": CellParametersVar,
            "mandatory": False},
        "celldm(1)": {
            "block": "system",
            "class": CommonQEInputVariable,
            "mandatory": True,
            "type": float},
        "celldm(2)": {
            "block": "system",
            "class": CommonQEInputVariable,
            "mandatory": False,
            "type": float},
        "celldm(3)": {
            "block": "system",
            "class": CommonQEInputVariable,
            "mandatory": False,
            "type": float},
        "conv_thr": {
            "block": "electrons",
            "class": CommonQEInputVariable,
            "mandatory": False,
            "type": float,
            "min": 0.0},
        "degauss": {
            "block": "system",
            "class": CommonQEInputVariable,
            "mandatory": False,
            "type": float
            },
        "diagonalization": {
            "allowed": ("david", "cg", "cg-serial", "david-serial"),
            "block": "electrons",
            "class": CommonQEInputVariable,
            "mandatory": False,
            "type": str,
                },
        "diago_cg_maxiter": {  # default is 100 as read from source code
            "block": "electrons",
            "class": CommonQEInputVariable,
            "mandatory": False,
            "min": 1,
            "type": int,
            },
        "diago_full_acc": {
            "block": "electrons",
            "class": CommonQEInputVariable,
            "mandatory": False,
            "type": bool,
            },
        "diago_thr_init": {
            "block": "electrons",
            "class": CommonQEInputVariable,
            "mandatory": False,
            "min": 0.0,
            "type": float,
            },
        "ecutrho": {
            "block": "system",
            "class": CommonQEInputVariable,
            "mandatory": False,
            "min": 0.0,
            "type": float
            },
        "ecutwfc": {
            "block": "system",
            "class": CommonQEInputVariable,
            "mandatory": True,
            "min": 0.0,
            "type": float
            },
        "electron_maxstep": {
            "block": "electrons",
            "class": CommonQEInputVariable,
            "mandatory": False,
            "type": int
            },
        "etot_conv_thr": {
            "block": "control",
            "class": CommonQEInputVariable,
            "mandatory": False,
            "min": 0.0,
            "type": float
            },
        "exx_fraction": {
            "block": "system",
            "class": CommonQEInputVariable,
            "mandatory": False,
            "max": 1.0,
            "min": 0.0,
            "type": float,
            },
        "forc_conv_thr": {
            "block": "control",
            "class": CommonQEInputVariable,
            "mandatory": False,
            "min": 0.0,
            "type": float
            },
        "ibrav": {
            "block": "system",
            "class": CommonQEInputVariable,
            "mandatory": True,
            "type": int
            },
        "input_dft": {
            # there are plenty more allowed values (see Modules/funct.f90)
            # (to implement later when needed I guess)
            "allowed": ("hse", ),
            "block": "system",
            "class": CommonQEInputVariable,
            "mandatory": False,
            "type": str,
            },
        "k_points": {
            "allowed": {
                "parameter": ("tpiba", "automatic", "crystal",
                              "gamma", "tpiba_b", "crystal_b",
                              "tpiba_c", "crystal_c")
                       },
            "block": None,
            "class": KPointsVar,
            "mandatory": True,
            "mandatory_keys": ("k_points", "parameter", ),
            "type": dict,
            },
        "lspinorb": {
            "block": "system",
            "class": CommonQEInputVariable,
            "mandatory": False,
            "type": bool
            },
        "mixing_beta": {
            "block": "electrons",
            "class": CommonQEInputVariable,
            "mandatory": False,
            "min": 0.0,
            "type": float,
            },
        "nat": {
            "block": "system",
            "class": CommonQEInputVariable,
            "mandatory": True,
            "min": 1,
            "type": int
            },
        "nbnd": {
            "block": "system",
            "class": CommonQEInputVariable,
            "mandatory": False,
            "type": int,
            },
        "noinv": {
            "block": "system",
            "class": CommonQEInputVariable,
            "mandatory": False,
            "type": bool
            },
        "noncolin": {
            "block": "system",
            "class": CommonQEInputVariable,
            "mandatory": False,
            "type": bool
            },
        "nosym": {
            "block": "system",
            "class": CommonQEInputVariable,
            "mandatory": False,
            "type": bool
            },
        "nqx1": {
            "block": "system",
            "class": CommonQEInputVariable,
            "mandatory": False,
            "min": 1,
            "type": int,
            },
        "nqx2": {
            "block": "system",
            "class": CommonQEInputVariable,
            "mandatory": False,
            "min": 1,
            "type": int,
            },
        "nqx3": {
            "block": "system",
            "class": CommonQEInputVariable,
            "mandatory": False,
            "min": 1,
            "type": int,
            },
        "nspin": {
            "block": "system",
            "class": CommonQEInputVariable,
            "mandatory": False,
            "type": int
            },
        "ntyp": {
            "block": "system",
            "class": CommonQEInputVariable,
            "mandatory": True,
            "min": 1,
            "type": int
            },
        "occupations": {
            "allowed": ("smearing", "tetrahedra",
                        "tetrahedra_lin",
                        "tetrahedra_opt",
                        "fixed", "from_input"),
            "block": "system",
            "class": CommonQEInputVariable,
            "mandatory": False,
            "type": str
             },
        "prefix": {
            "block": "control",
            "class": CommonQEInputVariable,
            "mandatory": True,
            "type": str
            },
        "press_conv_thr": {
            "block": "cell",
            "class": CommonQEInputVariable,
            "mandatory": False,
            "min": 0.0,
            "type": float,
            },
        "pseudo_dir": {
            "block": "control",
            "class": CommonQEInputVariable,
            "mandatory": False,
            "type": str
            },
        "outdir": {
            "block": "control",
            "class": CommonQEInputVariable,
            "mandatory": True,
            "path_convertible": True,
            "type": str
            },
        "smearing": {
            "allowed": ("gauss", "gaussian", "mp", "m-p",
                        "methfessel-paxton", "cold", "m-v", "mv",
                        "marzari-vanderbilt", "fermi-dirac", "f-d", "fd"),
            "block": "system",
            "class": CommonQEInputVariable,
            "mandatory": False,
            "type": str,
            },
        "tot_magnetization": {
            "block": "system",
            "class": CommonQEInputVariable,
            "mandatory": False,
            "type": float
            },
        "tprnfor": {
            "block": "control",
            "class": CommonQEInputVariable,
            "mandatory": False,
            "type": bool,
            },
        "tstress": {
            "block": "control",
            "class": CommonQEInputVariable,
            "mandatory": False,
            "type": bool,
            },
        "verbosity": {
            "allowed": ("default", "high", "low"),
            "block": "control",
            "class": CommonQEInputVariable,
            "mandatory": False,
            "type": str,
            },
        "wf_collect": {
            "block": "control",
            "class": CommonQEInputVariable,
            "mandatory": False,
            "type": bool,
            },
        }

ALL_QEPW_VARIABLES = variableDBIntegrityChecker(
        RAW_QEPW_VARIABLES,
        QEPW_ALLOWED_BLOCKS,
        MANDATORY_VARIABLE_KEYS,
        QEPW_ALLOWED_CLASSES,
        optional_keys=OPTIONAL_VARIABLE_KEYS)
