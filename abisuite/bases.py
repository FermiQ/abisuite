import logging
import time

try:
    import coloredlogs
    coloredlogs.install()
except ImportError:
    # not a mandatory module
    pass

from paramiko.client import SSHClient

from .exceptions import DevError


class BaseUtility:
    """The mother of all classes.
    """
    _loggername = None

    def __init__(self, loglevel=logging.INFO):
        if self._loggername is None:
            raise DevError(
                    "_loggername class attribute should be set in "
                    f"{self.__class__}")
        logging.basicConfig()
        self._logger = logging.getLogger(self._loggername)
        self._logger.setLevel(loglevel)

    @property
    def _loglevel(self):
        return self._logger.level

    @_loglevel.setter
    def _loglevel(self, loglevel):
        self._logger.level = loglevel


class BaseCalctypedUtility(BaseUtility):
    """Base class for any object that applies to a specific calctype.
    """
    _calctype = None
    """The calctype of the object."""

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        from . import __IMPLEMENTED_CALCTYPES__
        if self._calctype is None:
            raise DevError(
                    f"Need to set '_calctype' for '{self.__class__}' object.")
        if self._calctype not in __IMPLEMENTED_CALCTYPES__:
            raise NotImplementedError(
                    f"Calctype '{self._calctype}' not implemented.")

    @property
    def calctype(self):
        return self._calctype

    @property
    def software(self):
        if self.calctype.startswith("qe"):
            return "quantum_espresso"
        elif self.calctype.startswith("abinit"):
            return "abinit"
        elif self.calctype.startswith("wannier90"):
            return "wannier90"
        raise DevError(f"Unrecognized calculation type {self.calctype}")


class BaseStructuredObject(BaseUtility):
    """Base class for all objects that supports a 'Structure' object.
    """
    _structure_class = None
    _mpi_structure_class = None

    def __init__(self, *args, **kwargs):
        if self._structure_class is None:
            raise DevError(
                    "_structure_class attribute should be set in "
                    f"{self.__class__}.")
        self._structure = self._structure_class()
        self._mpi_structure = None
        if self._mpi_structure_class is not None:
            self._mpi_structure = self._mpi_structure_class()
        super().__init__(*args, **kwargs)

    def __getattr__(self, attr):
        struc = self.structure
        mpistruc = self.mpi_structure
        if attr in struc.all_attributes or attr in dir(struc):
            return getattr(self.structure, attr)
        if self.mpi_structure is not None:
            if attr in mpistruc.all_attributes or attr in dir(mpistruc):
                return getattr(self.mpi_structure, attr)
        raise AttributeError(f"'{self.__class__}' object has no attribute "
                             f"'{attr}'.")

    def __delattr__(self, attr):
        if attr in self.structure.all_attributes:
            delattr(self.structure, attr)
            return
        if self.mpi_structure is not None:
            if attr in self.mpi_structure.all_attributes:
                delattr(self.mpi_structure, attr)
                return
        super().__delattr__(attr)

    def __setattr__(self, attr, value):
        # check this first to prevent infinite recursions
        if attr.endswith("structure") or attr in dir(self):
            super().__setattr__(attr, value)
            return
        if attr in self.structure.all_attributes:
            setattr(self.structure, attr, value)
            # nest here because same attribute can be defined in both structurs
            if self.mpi_structure is not None:
                if attr in self.mpi_structure.all_attributes:
                    setattr(self.mpi_structure, attr, value)
            return
        # in case attr is only defined in mpi structure
        if self.mpi_structure is not None:
            if attr in self.mpi_structure.all_attributes:
                setattr(self.mpi_structure, attr, value)
                return
        super().__setattr__(attr, value)

    @property
    def structure(self):
        return self._structure

    @structure.setter
    def structure(self, structure):
        if not isinstance(structure, self._structure_class):
            raise TypeError(f"Expected '{self._structure_class}' instance but "
                            f"got '{structure}'")
        self._structure = structure

    @property
    def mpi_structure(self):
        return self._mpi_structure

    @mpi_structure.setter
    def mpi_structure(self, structure):
        if not isinstance(structure, self._mpi_structure_class):
            raise TypeError(f"Expected '{self._mpi_structure_class}' "
                            f"instance but got: '{structure}'")
        self._mpi_structure = structure


class BaseRemoteClient(BaseUtility):
    """Base class for remote clients for abisuite.

    Parameters
    ----------
    remote_data: dict
        The dictionary containing the remote data.
    """
    _check_script_exists_on_connection = None

    def __init__(self, remote_data, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self._check_remote_data(remote_data)
        if self._check_script_exists_on_connection is not None:
            if not isinstance(self._check_script_exists_on_connection, str):
                raise DevError(
                        "'_check_script_exists_on_connection' should be a str."
                        f" but got '{self._check_script_exists_on_connection}'"
                        ".")
        self.remote_data = remote_data
        self.client = None
        self._script_exists = None

    def __enter__(self):
        # create client
        self.client = SSHClient()
        self.client.load_system_host_keys()
        self.client.connect(
                self.remote_data["address"],
                username=self.remote_data["username"],
                port=self.remote_data["port"])
        if self._check_script_exists_on_connection is not None:
            if self._script_exists is None:
                # check only once if script exists
                self._script_exists = self.check_script_exists(
                        self._check_script_exists_on_connection)
            if self._script_exists is False:
                self.close()  # close connection
                raise RuntimeError(
                        f"'{self._check_script_exists_on_connection}' does not"
                        f"seem to be installed on remote '{self.hostname}'.")

        return self

    def __exit__(self, exc_type, exc_value, traceback):
        self.close()

    @property
    def hostname(self):
        return self.remote_data["hostname"]

    def check_script_exists(self, script):
        """Checks if a given script is directly accessible on remote using the
        'which' command.

        Parameters
        ----------
        script: str
            The script to check if it exists.
        """
        stdin, stdout, stderr = self.exec_command(f"which {script}")
        # stdin, stdout, stderr = self.client.exec_command("which abidb")
        if not stdout.read().decode("utf-8").strip("\n"):
            # nothing in stdout => script is not accessible
            return False
        # everything seems fine
        return True

    def close(self):
        """Closes the SSH client.
        """
        self.client.close()
        del self.client
        self.client = None

    def exec_command(self, command):
        """Executes a command on the remote server through the SSH client.

        This method will wait for the command to finish before returning
        the outputs.

        Parameters
        ----------
        command: str
            The command to execute.
        """
        if self.client is None:
            raise RuntimeError("Need to enter with statement to get client.")
        stdin, stdout, stderr = self.client.exec_command(command)
        # don't need stdin
        stdin.close()
        # don't need to write to stdout
        stdout.channel.shutdown_write()
        while not stdout.channel.exit_status_ready():
            time.sleep(0.1)  # wait 0.1 second
        return stdin, stdout, stderr

    def open_sftp(self):
        return self.client.open_sftp()

    @classmethod
    def from_hostname(cls, hostname, *args, **kwargs):
        """Create a RemoteClient from the hostname of a remote defined in
        the user config file.

        Parameters
        ----------
        hostname: str
            The hostname of the remote.
        """
        if not isinstance(hostname, str):
            raise TypeError(
                    f"Expected string for 'hostname' but got '{hostname}'.")
        from . import USER_CONFIG
        for remote in USER_CONFIG.REMOTES.remotes:
            if remote["hostname"] == hostname:
                return cls(remote, *args, **kwargs)
        raise ValueError(f"Hostname not defined in config file: '{hostname}'.")

    def _check_remote_data(self, remote_data):
        # checks that remote data is good
        if not isinstance(remote_data, dict):
            raise TypeError(
                    f"Expected dict for 'remote_data' but got '{remote_data}'."
                    )
        from .config import __ALL_REMOTES_KEYS__
        for key in __ALL_REMOTES_KEYS__:
            if key not in remote_data:
                raise ValueError(f"Missing key in remote_data: '{key}'.")
        from . import USER_CONFIG
        for remote in USER_CONFIG.REMOTES.remotes:
            # if hostnames matches, chek other keys
            if remote["hostname"] == remote_data["hostname"]:
                for key in __ALL_REMOTES_KEYS__:
                    if remote[key] != remote_data[key]:
                        raise ValueError(
                                f"Unrecognized remote: '{remote_data}'.")
                else:
                    break
        else:
            raise ValueError(
                    f"Remote not defined in config file: '{remote_data}'.")
