# taken from:
# https://stackoverflow.com/a/17303428/6362595
class Colors:
    """Colors container. Each class attributes are constant representing
    colors for the terminal.
    """
    PURPLE = '\033[95m'
    CYAN = '\033[96m'
    DARKCYAN = '\033[36m'
    BLUE = '\033[94m'
    GREEN = '\033[92m'
    YELLOW = '\033[93m'
    RED = '\033[91m'
    BOLD = '\033[1m'
    UNDERLINE = '\033[4m'
    END = '\033[0m'

    @classmethod
    def bold_text(cls, text):
        """Embeds the given text with the bolding tags.

        Parameters
        ----------
        text: str
            The text to make bold.

        Returns
        -------
        str: The bolded text.
        """
        return cls.BOLD + text + cls.END

    @classmethod
    def color_text(cls, text, *attributes):
        """Embed the given text with the given color.

        Parameters
        ----------
        text: str
            The text to colorize.
        attributes: all other args
            The attributes to add to the given text.

        Raises
        ------
        ValueError: If given color is not defined.

        Returns
        -------
        str: The embedded text.
        """
        for attribute in attributes:
            try:
                code = getattr(cls, attribute.upper())
            except AttributeError:
                raise ValueError(f"Attribute not defined '{attribute}'.")
            text = code + text
        return text + cls.END
