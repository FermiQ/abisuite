import matplotlib.pyplot as plt
import numpy as np

from ..nscf_sequencers import PositiveKpointsNSCFSequencer as NSCFSequencer
from ...physics import Lattice
from ...plotters import Plot
from ...routines import is_scalar, is_list_like, suppress_warnings


class QEFermiSurfaceSequencer(NSCFSequencer):
    """A QE nscf sequencer that can plot the Fermi Surface of a material.

    For now, only supports a single 2D fermi surface along a given k_z.
    """
    _all_sequencer_prefixes = ("scf_", "nscf_", "plot_", )
    _loggername = "QEFermiSurfaceSequencer"

    def __init__(self, *args, **kwargs):
        NSCFSequencer.__init__(self, "qe", *args, **kwargs)
        self._fermi_surface_k_z = None

    @property
    def nscf_input_variables(self):
        return NSCFSequencer.nscf_input_variables.fget(self)

    @nscf_input_variables.setter
    def nscf_input_variables(self, nscf):
        if not isinstance(nscf, dict):
            raise TypeError(
                    "'nscf_input_variables' should be a dict.")
        self._set_input_var_to(nscf, "calculation", "nscf")
        self._set_input_var_to(nscf, "verbosity", "high")
        # self._set_input_var_to(nscf, "nosym", True)
        # these variables are used to update the dict of scf input vars
        scf = self.scf_input_variables.copy()
        scf.update(nscf)
        NSCFSequencer.nscf_input_variables.fset(self, scf)

    @property
    def fermi_surface_k_z(self):
        return self._fermi_surface_k_z

    @fermi_surface_k_z.setter
    def fermi_surface_k_z(self, k_z):
        list_kzs = k_z
        if not is_list_like(k_z):
            list_kzs = (k_z, )
        for k_z in list_kzs:
            if not is_scalar(k_z):
                raise TypeError(k_z)
            if k_z < -0.5 or k_z > 0.5:
                raise ValueError("k_z should be between [-0.5, 0.5]")
        self._fermi_surface_k_z = k_z

    def post_sequence(self):
        self.plot_fermi_surface()

    @suppress_warnings
    def plot_fermi_surface(self):
        """Uses log file eigenvalues to plot fermi surface.
        """
        nscf_calc = self.get_sequence_calculation("nscf")
        with nscf_calc.calculation_directory.log_file as log:
            kpts = np.array(log.eigenvalues["k_points"])
            eigs = np.array(log.eigenvalues["eigenvalues"])
            e_F = log.fermi_energy
        kxs = np.unique(kpts[:, 0])
        kys = np.unique(kpts[:, 1])
        kzs = np.unique(kpts[:, 2])
        self.plot_grid = False
        lattice = Lattice.from_calculation(
                nscf_calc.calculation_directory.path)
        if self.fermi_surface_k_z is not None:
            # select kpts at given k_z
            fskzs = self.fermi_surface_k_z
            if not is_list_like(self.fermi_surface_k_z):
                fskzs = (self.fermi_surface_k_z, )
            for fskz in fskzs:
                if fskz not in kzs:
                    raise ValueError(f"k_z={fskz} not in computed values!")
                bands = eigs[kpts[:, 2] == fskz].T
                plot = Plot(loglevel=self._loglevel)
                # for each bands that crosses the fermi level,
                # plot a contour line
                for band in bands:
                    if min(band) > e_F:
                        continue
                    if max(band) < e_F:
                        continue
                    # this band has its min < eF and max > eF
                    # it thus cross the fermi level
                    energy_surface = band.reshape(len(kxs), len(kys)) - e_F
                    if self.fermi_surface_k_z is not None:
                        plot.add_contour(
                                kxs, kys, energy_surface, (0.0, ),
                                linewidths=2)
                        plot.add_contour(
                                kxs, -kys, energy_surface, (0.0, ),
                                linewidths=2)
                        plot.add_contour(
                                -kxs, kys, energy_surface, (0.0, ),
                                linewidths=2)
                        plot.add_contour(
                                -kxs, -kys, energy_surface, (0.0, ),
                                linewidths=2)

                plot.xlims = [-0.5, 0.5]
                plot.ylims = [-0.5, 0.5]
                plot.xlabel = r"$k_x$"
                plot.ylabel = r"$k_y$"
                plot.grid = False
                plot.aspect = "equal"
                plot.add_hline(0)
                plot.add_vline(0)
                self._post_process_plot(
                        plot, name_extension=f"k_z={fskz}")
        # now plot whole volume
        # get all points that would be on a contour for each kz
        nbnds = eigs.shape[-1]
        fermi_surfaces = [[] for i in range(nbnds)]
        for kx, ky, kz in zip(kxs, kys, kzs):
            # if kpt out of BZ, skip it
            if not self._kpt_in_bz(kx, ky, kz, lattice):
                continue
            eigs_this_kz = eigs[kpts[:, 2] == kz].T
            for iband, band in enumerate(eigs_this_kz):
                if min(band) > e_F:
                    continue
                if max(band) < e_F:
                    continue
                fig = plt.figure()
                ax = fig.add_subplot(111)
                energy_surface = band.reshape(len(kxs), len(kys)) - e_F
                lines = ax.contour(kxs, kys, energy_surface, (0.0, ))
                segments = lines.allsegs[0]
                for segment in segments:
                    for pt in segment:
                        fermi_surfaces[iband].append([pt[0], pt[1], kz])
                plt.close(fig)
                del fig, ax, lines, segments
        # finished collecting all points on the fermi surfaces
        # now plot surface
        colors = ["r", "b", "g", "orange"]  # if more than that...
        icolor = 0
        bz_plot = lattice.get_brillouin_zone_plot()
        plots = []
        for ibnd, fermi_surface in enumerate(fermi_surfaces):
            if not fermi_surface:
                continue
            # coordinates are in cartesian coords already (tpiba)
            # change to cartesian
            # fs = np.array(fermi_surface).dot(lattice.reciprocal_vectors)
            fs = np.array(fermi_surface)
            plot = Plot(loglevel=self._loglevel)
            plot.add_scatter(
                    fs[:, 0], fs[:, 1], fs[:, 2], color=colors[icolor])
            plot.add_scatter(
                    -fs[:, 0], fs[:, 1], fs[:, 2], color=colors[icolor])
            plot.add_scatter(
                    fs[:, 0], -fs[:, 1], fs[:, 2], color=colors[icolor])
            plot.add_scatter(
                    fs[:, 0], fs[:, 1], -fs[:, 2], color=colors[icolor])
            plot.add_scatter(
                    -fs[:, 0], -fs[:, 1], fs[:, 2], color=colors[icolor])
            plot.add_scatter(
                    -fs[:, 0], fs[:, 1], -fs[:, 2], color=colors[icolor])
            plot.add_scatter(
                    fs[:, 0], -fs[:, 1], -fs[:, 2], color=colors[icolor])
            plot.add_scatter(
                    -fs[:, 0], -fs[:, 1], -fs[:, 2], color=colors[icolor])
            plot.xlabel = r"$k_x$"
            plot.ylabel = r"$k_y$"
            plot.zlabel = r"$k_z$"
            bz_plot.synchronize_labels(plot)
            plot += bz_plot
            icolor += 1
            plot.xlims = [-0.5, 0.5]
            plot.ylims = [-0.5, 0.5]
            plot.aspect = "auto"
            plot.zlims = [-0.5, 0.5]
            plot.grid = False
            self._post_process_plot(plot, name_extension=f"ibnd{ibnd}")
            plots.append(plot)
        if len(plots) > 1:
            all_plots = sum(plots[1:], plots[0])
            self._post_process_plot(
                    all_plots, name_extension="all_fermi_sheets")
        return

    def _get_nscf_k_points(self):
        kpts = NSCFSequencer._get_nscf_k_points(self)
        kpts["parameter"] = "tpiba"
        kpts["k_points"] = (np.array(kpts["k_points"]) / 2).tolist()
        return kpts

    def _kpt_in_bz(self, kx, ky, kz, lattice):
        for rec in lattice.reciprocal_vectors:
            # k.rec <= 0.5 rec ** 2 for it to be inside BZ
            dot = abs(rec.dot([kx, ky, kz]))
            norm = rec.dot(rec)
            if dot > 0.5 * norm:
                return False
        return True
