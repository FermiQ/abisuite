import os

from .scf_sequencer import QESCFSequencer
from ..bases import SequenceCalculation
from ..exceptions import SequenceNotConvergedError
from ..relaxation_sequencers import RelaxationSequencer
from ...handlers import QEPWLogFile
from ...launchers import QEPWLauncher
from ...launchers.restarters.qe_restarters.pw_restarter import (
        get_qe_pw_final_geometry,
        )


class QERelaxationSequencer(QESCFSequencer, RelaxationSequencer):
    """The relaxation sequencer executes relaxation calculations
    (which are basically a series of SCF calculations) until the pressure
    reaches 0. An upper limit of calculations needs to be specified in order
    to not waste ressources in case of problems.
    """
    _loggername = "QERelaxationSequencer"

    def __init__(self, *args, **kwargs):
        QESCFSequencer.__init__(self, *args, **kwargs)
        RelaxationSequencer.__init__(self, "qe", *args, **kwargs)
        self._maximum_relaxations = None
        # use special flag for sequence completed to return True
        # since we add calculations only once they every calcs
        # are finished
        self._truly_completed = False

    @property
    def maximum_relaxations(self):
        if self._maximum_relaxations is not None:
            return self._maximum_relaxations
        raise ValueError("Need to set 'maximum_relaxations'.")

    @maximum_relaxations.setter
    def maximum_relaxations(self, max_):
        if not isinstance(max_, int):
            raise TypeError(
                    f"Expected int for 'maximum_relaxations' but got '{max_}'."
                    )
        self._maximum_relaxations = max_

    @property
    def scf_input_variables(self):
        return self._scf_input_variables

    @scf_input_variables.setter
    def scf_input_variables(self, invars):
        if self.relax_cell:
            if "press_conv_thr" not in invars:
                raise ValueError(
                    "'press_conv_thr' must be defined for cell optimization.")
            self._set_input_var_to(invars, "tprnfor", True)
            self._set_input_var_to(invars, "tstress", True)
        for varname in ("forc_conv_thr", "etot_conv_thr", ):
            if varname not in invars:
                raise ValueError(
                    f"'{varname}' must be defined for relaxation.")
        self._scf_input_variables = invars

    @property
    def sequence_completed(self):
        if not self.sequence:
            self.init_sequence()
        if not self._truly_completed:
            return False
        return RelaxationSequencer.sequence_completed.fget(self)

    def init_sequence(self):
        # in any case, start by doing atomic relaxations only
        if not self.relax_atoms and not self.relax_cell:
            raise ValueError(
                    "'relax_atoms' and 'relax_cell' cannot be False "
                    "at same time.")
        invars = self.scf_input_variables.copy()
        if self.relax_atoms:
            invars["calculation"] = "relax"
            workdir = self._get_relaxation_workdir(0)
        else:
            invars["calculation"] = "vc-relax"
            workdir = self._get_relaxation_workdir(1)
        seq = SequenceCalculation(
                "qe_pw", workdir,
                invars, self.scf_calculation_parameters,
                loglevel=self._loglevel)
        self.sequence.append(seq)
        if not self.relax_cell:
            # nothing else to do
            self._truly_completed = True
            return
        # up to a maximum of 'self.maximum_relaxations' must be done
        # to relax cell (in order to reach pressure of exactly 0)
        # first check how many calcs are written
        n_relaxed = 0
        while os.path.exists(self._get_relaxation_workdir(n_relaxed + 1)):
            n_relaxed += 1
        # there are n_relaxed calculations written build sequence up to there
        for icalc in range(1, n_relaxed + 1):
            self._add_calc_to_sequence(icalc)
        # check if sequence is completed. If not, launch another calc if we can
        if not RelaxationSequencer.sequence_completed.fget(self):
            # wait for sequence to complete
            return
        # sequence completed. check if last calculation converged to 0 pressure
        with QEPWLogFile.from_calculation(
                self.sequence[-1].workdir, loglevel=self._loglevel) as log:
            assert log.pressure is not None, "Couldn't extract pressure..."
            if log.pressure == 0.0:
                # we're done
                self._truly_completed = True
                return
        # pressure not yet 0. add calculation if we can
        if n_relaxed >= self.maximum_relaxations:
            # raise error we should have reached convergence normally
            raise SequenceNotConvergedError(self, sequence_part="scf_")
        # append a calc
        self._add_calc_to_sequence(n_relaxed + 1)

    def _add_calc_to_sequence(self, icalc):
        if icalc == 1 and not self.relax_atoms:
            # nothing to do
            return
        # add the calculations following the first one
        # pop out the geometry variables
        # so that they can be loaded from last calc without being overwritten
        invars = self.scf_input_variables.copy()
        for varname in QEPWLauncher._geometry_variables:
            if "*" in varname:
                begin = varname.split("*")[0]
                end = varname.split("*")[1]
                topop = []
                for invar in invars:
                    if invar.startswith(begin) and invar.endswith(end):
                        topop.append(invar)
                for invar in topop:
                    invars.pop(invar)
            else:
                invars.pop(varname, None)
        seq = SequenceCalculation(
                "qe_pw", self._get_relaxation_workdir(icalc),
                invars, self.scf_calculation_parameters,
                loglevel=self._loglevel)
        seq.recover_from = self.sequence[-1]
        seq.recover_from_other_kwargs["use_last_geometry"] = True
        seq.recover_from_update_variables = {"calculation": "vc-relax"}
        seq.dependencies.append(self.sequence[-1])
        self.sequence.append(seq)

    def _get_relaxation_workdir(self, icalc):
        if icalc == 0 or icalc == "0":
            return os.path.join(self.scf_workdir, "relax_atoms_only")
        return os.path.join(
                self.scf_workdir, f"relax_full_cell_geometry_{icalc}")

    def _get_relaxed_geometry_variables(self):
        # return variables as if we're defining them in a launcher
        # get ibrav from input file
        return get_qe_pw_final_geometry(self.sequence[-1].workdir)
