from ..band_structure_sequencer import BandStructureSequencer


class QEBandStructureSequencer(BandStructureSequencer):
    """Sequencer class to make a Band Structure with Quantum Espresso.
    """
    _loggername = "QEBandStructureSequencer"

    def __init__(self, *args, **kwargs):
        BandStructureSequencer.__init__(self, "qe", *args, **kwargs)

    @property
    def band_structure_input_variables(self):
        return BandStructureSequencer.band_structure_input_variables.fget(self)

    @band_structure_input_variables.setter
    def band_structure_input_variables(self, input_vars):
        if not isinstance(input_vars, dict):
            raise TypeError(
                    f"Expected dict for band structure input vars got instead"
                    f": {input_vars}")
        self._set_input_var_to(
                input_vars, "calculation", "bands")
        # set verbosity to 'high' whatever it was before
        self._set_input_var_to(
                input_vars, "verbosity", "high")
        # set kpoint path
        kpath = [list(k.values())[0]
                 for k in self.band_structure_kpoint_path]
        self._set_input_var_to(
                input_vars, "k_points",
                {"parameter": "crystal_b",
                 "k_points": kpath,
                 "weights": ([self.band_structure_kpoint_path_density] *
                             len(kpath)),
                 })
        BandStructureSequencer.band_structure_input_variables.fset(
                self, input_vars)
