from ..scf_sequencer import SCFSequencer


class QESCFSequencer(SCFSequencer):
    """SCF sequencer for Quantum Espresso.
    """
    _loggername = "QESCFSequencer"

    def __init__(self, *args, **kwargs):
        super().__init__("qe", *args, **kwargs)
