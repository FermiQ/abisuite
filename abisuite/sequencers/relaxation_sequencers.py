import abc

from .exceptions import SequenceNotCompletedError
from .scf_sequencer import SCFSequencer


class RelaxationSequencer(SCFSequencer):
    """Base class for any relaxation sequencers.
    """

    def __init__(self, *args, **kwargs):
        SCFSequencer.__init__(self, *args, **kwargs)
        self._relax_atoms = None
        self._relax_cell = None

    @property
    def relax_atoms(self):
        if self._relax_atoms is not None:
            return self._relax_atoms
        raise ValueError("Need to set 'relax_atoms'.")

    @relax_atoms.setter
    def relax_atoms(self, relax):
        self._relax_atoms = relax

    @property
    def relax_cell(self):
        if self._relax_cell is not None:
            return self._relax_cell
        raise ValueError("Need to set 'relax_cell'.")

    @relax_cell.setter
    def relax_cell(self, relax):
        self._relax_cell = relax

    @property
    def relaxed_geometry_variables(self):
        if not self.sequence_completed:
            raise SequenceNotCompletedError(self)
        return self._get_relaxed_geometry_variables()

    @abc.abstractmethod
    def _get_relaxed_geometry_variables(self):
        pass
