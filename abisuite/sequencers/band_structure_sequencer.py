from .bases import SequenceCalculation
from .scf_sequencer import SCFSequencer, SCFComparatorSequencer
from ..exceptions import DevError
from ..plotters import rand_cmap
from ..post_processors import BandStructure
from ..routines import is_list_like


class BandStructureSequencer(SCFSequencer):
    """A sequencer for a Band Structure calculation.
    """
    _all_sequencer_prefixes = ("scf_", "band_structure_", "plot_")

    def __init__(self, *args, **kwargs):
        SCFSequencer.__init__(self, *args, **kwargs)
        if "band_structure_" not in self._all_sequencer_prefixes:
            raise DevError(
                    "Need to set 'band_structure_' in "
                    "'_all_sequencer_prefixes'")
        if "plot_" not in self._all_sequencer_prefixes:
            raise DevError(
                    "Need to set 'plot_' in "
                    "'_all_sequencer_prefixes'")
        # BANDSTRUCTURE EXTRA PARAMETERS
        self._band_structure_kpoint_path = None
        self._band_structure_kpoint_path_density = None
        self._bandgap = None
        self._dft_plot = None  # in order to save time later on

    @property
    def all_k_labels(self):
        labels = self._get_band_structure_labels()
        n = self.band_structure_kpoint_path_density
        return [(label, coord)
                for label, coord in zip(
                    labels, range(0, len(labels) * (n + 1), n))]

    @property
    def bandgap(self):
        """The material's band gap.
        """
        if self._bandgap is not None:
            return self._bandgap
        if not self.sequence_completed:
            raise RuntimeError(
                    "Cannot compute 'bandgap' if sequence not complete.")
        self.compute_bandgap()
        return self.bandgap

    @property
    def band_structure_kpoint_path(self):
        if self._band_structure_kpoint_path is not None:
            return self._band_structure_kpoint_path
        raise ValueError("Need to set 'band_structure_kpoint_path'.")

    @band_structure_kpoint_path.setter
    def band_structure_kpoint_path(self, kpoint_path):
        if not is_list_like(kpoint_path):
            raise TypeError("Expected a list for 'kpoint_path' but got: "
                            f"{kpoint_path}")
        if len(kpoint_path) < 2:
            raise ValueError("'kpoint_path' must have at least 2 points.")
        for kpt in kpoint_path:
            if not isinstance(kpt, dict):
                raise TypeError("Expected a dict for a kpt but got: "
                                f"{kpt}.")
            if len(kpt) > 1:
                raise SyntaxError(
                        "A kpt in kpt_path is a dict whose 'only' key is the "
                        "kpt label and value its coordinates in crystal coords"
                        )
            label = list(kpt.keys())[0]
            coords = kpt[label]
            if not is_list_like(coords):
                raise TypeError(
                        "Expected list for k_point_path coords but got: "
                        f"{coords}")
            if len(coords) != 3:
                raise ValueError("kpoint path coordinates must be of len 3!")
        self._band_structure_kpoint_path = kpoint_path

    @property
    def band_structure_kpoint_path_density(self):
        if self._band_structure_kpoint_path_density is not None:
            return self._band_structure_kpoint_path_density
        raise ValueError("Need to set 'band_structure_kpoint_path_density'.")

    @band_structure_kpoint_path_density.setter
    def band_structure_kpoint_path_density(self, den):
        if not isinstance(den, int):
            raise TypeError(
                    "kpt density should be an integer.")
        self._band_structure_kpoint_path_density = den

    @property
    def band_structure_input_variables(self):
        return self._band_structure_input_variables

    @band_structure_input_variables.setter
    def band_structure_input_variables(self, input_vars):
        if not isinstance(input_vars, dict):
            raise TypeError(
                    f"Expected dict for band structure input vars got instead"
                    f": {input_vars}")
        self._band_structure_input_variables = input_vars

    @property
    def band_structure_calctype(self):
        if self.software == "qe":
            return "qe_pw"
        elif self.software == "abinit":
            return "abinit"
        raise NotImplementedError(self.software)

    def compute_bandgap(self):
        # getting the dft plot will also compute the bandgap
        if self._dft_plot is None:
            self._dft_plot = self._get_dft_plot(self.band_structure_workdir)

    def init_sequence(self, *args, **kwargs):
        # SCF calculation done in mother class
        SCFSequencer.init_sequence(self, *args, **kwargs)
        bscalc = SequenceCalculation(
                self.band_structure_calctype, self.band_structure_workdir,
                self.band_structure_input_variables,
                self.band_structure_calculation_parameters,
                loglevel=self._loglevel)
        # bandstructure depends on SCF calculation
        for calc in self.sequence:
            if calc.workdir == self.scf_workdir:
                scfcalc = calc
                break
        else:
            raise LookupError("Could not find SCF calculation.")
        bscalc.dependencies.append(scfcalc)
        bscalc.load_geometry_from = scfcalc.workdir
        # bscalc.link_wfk_from = scfcalc.workdir
        self.sequence.append(bscalc)

    def post_sequence(self, *args, **kwargs):
        # Call when sequence is completed only
        super().post_sequence(*args, **kwargs)
        self.plot_band_structure()

    # TODO: rebase this with the SCFSequencer.plot_convergence method
    def plot_band_structure(self, name_extension=""):
        """Create the plot object for the band structure.
        """
        if self._dft_plot is None:
            self._dft_plot = self._get_dft_plot(self.band_structure_workdir)
        self._post_process_plot(self._dft_plot, name_extension=name_extension)
        return self._dft_plot

    def _get_band_structure_labels(self):
        return [list(x.keys())[0] for x in self.band_structure_kpoint_path]

    def _get_dft_plot(self, calcdir):
        # returns a dft plot for a given band structure calc
        dft_bs = BandStructure.from_calculation(
                    calcdir, loglevel=self._loglevel)
        fermi_at_zero = self.plot_calculation_parameters.get(
                "fermi_at_zero", True)
        dft_plot = dft_bs.get_plot(
                yunits="eV",
                ylabel="Energy",
                color="k",
                linewidth=2,
                symmetry="none",
                all_k_labels=self.all_k_labels,
                fermi_at_zero=fermi_at_zero,
                # draw line at 0 only if fermi at 0
                line_at_zero=fermi_at_zero,
                adjust_axis=self.plot_calculation_parameters.get(
                    "adjust_axis", True),
                show_bandgap=self.plot_calculation_parameters.get(
                    "show_bandgap", False)
                )
        # computing the plot computes the bandgap as well
        self._bandgap = dft_bs.bandgap
        self._apply_plot_parameters(dft_plot)
        return dft_plot

    def _apply_plot_parameters(self, plot):
        # apply other properties to plot
        for param, value in self.plot_calculation_parameters.items():
            if param in ("show", "save", "save_plot"):
                # postfix parameters
                continue
            setattr(plot, param, value)


class BandStructureComparatorSequencer(
        BandStructureSequencer, SCFComparatorSequencer):
    """Band structure comparator sequecner. It should be used to compare
    band structures against changes of a single scf parameter.
    """

    def __init__(self, *args, **kwargs):
        BandStructureSequencer.__init__(self, *args, **kwargs)
        SCFComparatorSequencer.__init__(self, *args, **kwargs)

    def init_sequence(self, *args, **kwargs):
        SCFComparatorSequencer.init_sequence(self, *args, **kwargs)
        bs_calcs = []
        for scf_calc, specific_vars in zip(
                self.sequence, self.scf_specific_input_variables):
            bs_dir = self._get_bs_workdir_single_scf(scf_calc)
            # since bs is a gs calculation, we update the bs vars
            # with the scf specific vars as well
            ivars = self.band_structure_input_variables.copy()
            ivars.update(specific_vars)
            bs_calc = SequenceCalculation(
                        self.band_structure_calctype, bs_dir, ivars,
                        self.band_structure_calculation_parameters,
                        )
            bs_calc.load_geometry_from = scf_calc
            bs_calc.dependencies.append(scf_calc)
            bs_calcs.append(bs_calc)
        self.sequence += bs_calcs

    def plot_band_structure(self):
        """Makes a joint plot and a multiplot to compare the different
        band structures.
        """
        workdirs = [self._get_bs_workdir_single_scf(i)
                    for i in range(len(self.scf_specific_input_variables))]
        extensions = [workdir.split("_")[-1] for workdir in workdirs]
        dft_plots = [self._get_dft_plot(workdir) for workdir in workdirs]
        title = self.plot_calculation_parameters.get("title", "")
        for ext, dft_plot in zip(extensions, dft_plots):
            # save each plots individually
            dft_plot.title = title + " " + ext
            self._post_process_plot(
                    dft_plot,
                    name_extension=ext)
        if len(extensions) <= 1:
            # no comparison to show
            return
        # make joint plot
        # first change colors for each plot
        ncolors = len(extensions)
        cmap = rand_cmap(ncolors)
        colors = [cmap(i / ncolors) for i in range(ncolors)]
        for dft_plot, color, ext in zip(dft_plots, colors, extensions):
            if dft_plot.curves[0].label is None:
                dft_plot.curves[0].label = ""
            else:
                dft_plot.curves[0].label += " "
            dft_plot.curves[0].label += ext
            for curve in dft_plot.curves:
                curve.color = color
            for scatter in dft_plot.scatters:
                scatter.color = color
        sumplot = sum(dft_plots[1:], dft_plots[0])
        sumplot.title = title
        sumplot.legend = True
        sumplot.legend_outside = True
        self._post_process_plot(sumplot, name_extension="combined")
        return sumplot

    def plot_comparision(self):
        """This method does nothing since the comparision is done in the
        plot_band_structure method.
        """
        pass

    def _get_bs_workdir_single_scf(self, calc):
        if isinstance(calc, int):
            calc = self.sequence[calc]
        scf_ext = "_" + calc.workdir.split("_")[-1]
        return self.band_structure_workdir + scf_ext
