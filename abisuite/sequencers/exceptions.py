from .bases import BaseSequencer
from ..exceptions import DevError


class SequenceNotConvergedError(Exception):
    """Custom exception to raise when a Sequence hasn't converge.
    """
    def __init__(self, item, *args, sequence_part=None, **kwargs):
        """Custom init method that can create a custom message in case a sequencer
        object is given as arg.

        Parameters
        ----------
        item: str or BaseSequencer instance
            If a str is given, this error message will be raised.
            If a Sequence object is given instead, a custom error message will
            be raised. If this is the case, 'sequence_part' must be given in
            order to build the error message.
        sequence_part: str, optional
            The sequence part of the Sequencer that raised the error.
        """
        if isinstance(item, str):
            super().__init__(item, *args, **kwargs)
            return
        if not isinstance(item, BaseSequencer):
            raise TypeError(
                    "'item' should be either a str or Sequencer. "
                    f"Got '{item}'.")
        if sequence_part is None or not isinstance(sequence_part, str):
            raise DevError("'sequence_part' should be set.")
        if sequence_part not in item._all_sequencer_prefixes:
            raise DevError(
                    f"'sequence_part'='{sequence_part}' not in all sequencer's"
                    f"prefixes ({item._all_sequencers_prefixes}).")
        workdir = getattr(item, sequence_part + "workdir")
        msg = (f"Sequencer part: '{sequence_part.strip('_')} of '{item}' at "
               f"'{workdir}' makes the sequence not converged.")
        super().__init__(msg, *args, **kwargs)


class SequenceNotCompletedError(Exception):
    """Custom error raised to signify that a sequencer is not completed yet.
    """
    def __init__(self, item, *args, **kwargs):
        """Custom init method. If given a string as input, the
        normal exception behavior is requested. Otherwise, a custom
        error message is printed.

        Parameters
        ----------
        item: str or Sequencer object
            If a Sequencer object is used, a custom error message is
            printed. Otherwise, if it is a string, the normal exception
            behavior is used.
        """
        if isinstance(item, str):
            super().__init__(item, *args, **kwargs)
            return
        if not isinstance(item, BaseSequencer):
            raise TypeError(
                    f"Expected Sequencer object but got: '{item}'.")
        msg = f"Sequence not completed: '{item.__class__}'."
        super().__init__(msg, *args, **kwargs)
