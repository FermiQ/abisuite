import abc
import os

import numpy as np

from .scf_sequencer import (
        SCFSequencer, SCFComparatorSequencer, SCFConvergenceSequencer,
        )
from ..exceptions import DevError
from ..plotters import Plot, rand_cmap, MultiPlot
from ..routines import is_list_like, is_vector


class BaseIndividualPhononSequencer(SCFSequencer):
    """A sequencer that launches phonon calculations one phonon at a time
    based on one SCF GS calculation.
    """
    def __init__(self, software, *args, **kwargs):
        SCFSequencer.__init__(self, software, *args, **kwargs)
        if "phonons_" not in self._all_sequencer_prefixes:
            raise DevError(
                "Need to put 'phonons_' in '_all_sequencer_prefixes'.")
        self._nphonons = None
        self._phonons_specific_input_variables = None
        # this variable can be used to force phonons workdir to have a zfill
        # value. Use this for legacy only...
        self._phonons_workdir_zfill = None

    @property
    def nphonons(self):
        if self._nphonons is not None:
            return self._nphonons
        raise ValueError("Need to set 'nphonons'.")

    @nphonons.setter
    def nphonons(self, nphonons):
        if not isinstance(nphonons, int):
            raise TypeError("Need an integer for 'nphonons'.")
        self._nphonons = nphonons

    @property
    def phonons_input_variables(self):
        return self._phonons_input_variables

    @phonons_input_variables.setter
    def phonons_input_variables(self, input_vars):
        if not isinstance(input_vars, dict):
            raise TypeError(
                    f"Expected dict for input vars but got: {input_vars}")
        self._phonons_input_variables = input_vars

    @property
    def phonons_specific_input_variables(self):
        """Specific input variables for each different phonon calculations.
        """
        if self._phonons_specific_input_variables is None:
            raise ValueError("Need to set 'phonons_specific_input_variables'.")
        return self._phonons_specific_input_variables

    @phonons_specific_input_variables.setter
    def phonons_specific_input_variables(self, spec):
        if not is_list_like(spec):
            raise TypeError(
                    f"Expected list-like but got: '{spec}'.")
        if self._nphonons is None:
            raise ValueError(
                    "Need to set nphonons before setting specific phonons "
                    "input variables.")
        if len(spec) != self.nphonons:
            raise ValueError(
                    "Specific input variables must be same length as number of"
                    " q-points.")
        for item in spec:
            if not isinstance(item, dict):
                raise TypeError(
                        f"ph_spec_inputs must be a list of dicts. "
                        f"Got: '{spec}'.")
        self._phonons_specific_input_variables = spec

    # @property
    # def _zfill_ph_number(self):
    #     """zfill value for phonon calculation paths.
    #     """
    #     # compute the number of zeros needed for all the individual phonon
    #     # workdir names. we want all repository to have same number of digits
    #     # so we need to zfill if needed.
    #     ph_ends = [str(x + 1) for x in range(self.nphonons)]
    #     return max([len(x) for x in ph_ends])

    def init_sequence(self, *args, **kwargs):
        self.init_scf_sequence(*args, **kwargs)
        self.init_phonons_sequence(*args, **kwargs)

    def init_scf_sequence(self, *args, **kwargs):
        # SCF calculation done in mother's class
        SCFSequencer.init_sequence(self, *args, **kwargs)

    def init_phonons_sequence(self, *args, **kwargs):
        scf_calc = self.sequence[0]
        for iph in range(self.nphonons):
            self.init_phonons_qpoint(iph, scf_calc)

    def init_phonons_qpoint(self, iqpt, scf_calc):
        """Init a single phonon qpoint calculation, based on a given SCF calc.

        Parameters
        ----------
        iqpt: int
            The qpt index to do.
        scf_calc: SequenceCalculation object
            The SequenceCalculation object of the SCF calculation
        """
        self.sequence.append(self._get_phonon_calculation(
            iqpt, scf_calc))

    def _get_phonon_workdir(self, iph, *args, base_workdir=None):
        """Returns the phonon workdir for a given phonon index.

        Parameters
        ----------
        iph: int
            The phonon index (python index starts at 0).
        base_workdir: str, optional
            If not None, gives the root dir of the phonon calcs.
        """
        if base_workdir is None:
            base_workdir = self.phonons_workdir
        if self._phonons_workdir_zfill is not None:
            return base_workdir + str(iph + 1).zfill(
                    self._phonons_workdir_zfill)
        return base_workdir + str(iph + 1)

    @abc.abstractmethod
    def _get_phonon_calculation(self, *args, **kwargs):
        pass


class BasePhononComparatorSequencer(
        BaseIndividualPhononSequencer, SCFComparatorSequencer):
    """Phonon comparator sequencer. It should be used to compare phonon
    frequencies against changes of a single scf parameter.
    """
    _all_sequencer_prefixes = ("scf_", "phonons_", "plot_", )

    def __init__(self, *args, **kwargs):
        BaseIndividualPhononSequencer.__init__(self, *args, **kwargs)
        SCFComparatorSequencer.__init__(self, *args, **kwargs)

    def init_sequence(self, *args, **kwargs):
        BaseIndividualPhononSequencer.init_sequence(self, *args, **kwargs)

    def init_scf_sequence(self, *args, **kwargs):
        SCFComparatorSequencer.init_scf_sequence(self, *args, **kwargs)

    def init_phonons_sequence(self):
        # from here, self.sequence contains only scf calcs.
        scf_calcs = self.get_sequence_calculation("scf_")
        if not is_list_like(scf_calcs):
            scf_calcs = [scf_calcs]
        for scf_calc in scf_calcs:
            # for each scf calc, we do a full spectrum of phonon calcs
            # phonon base dir is appended with the same number as the scf calc
            self.init_phonons_single_scf(scf_calc)

    def init_phonons_single_scf(self, scf_calc):
        """Inits the phonon calculations that are based on a single
        scf calculation.
        """
        for iqpt in range(self.nphonons):
            self.init_phonons_qpoint(iqpt, scf_calc)

    def _get_phonon_workdir(self, iph, scf_calc, base_workdir=None):
        """Returns the phonon base dir from the base scf calc and qpt number.

        Parameters
        ----------
        iph: int
            The qpt number to do.
        scf_calc: SequenceCalculation object
            The SCF calculation on which the phonon calculation is linked.
        base_workdir: str
            The basename for the phonons workdir.
        """
        if base_workdir is None:
            base_workdir = self.phonons_workdir
        scf_ext = "_" + scf_calc.workdir.split("_")[-1]
        return super()._get_phonon_workdir(
                iph, base_workdir=base_workdir + scf_ext)


class BasePhononConvergenceSequencer(
        BasePhononComparatorSequencer,
        SCFConvergenceSequencer):
    """Phonon convergence sequencer for an individual qpt.

    Should be used to converge SCF parameters against phonon frequencies of a
    given qpt.
    """
    _all_sequencer_prefixes = ("scf_", "phonons_", "plot_", )

    def __init__(self, *args, **kwargs):
        SCFConvergenceSequencer.__init__(self, *args, **kwargs)
        BasePhononComparatorSequencer.__init__(self, *args, **kwargs)
        # artificially sets this to 1 (easier for plotting)
        self._nphonons = 1
        self._phonons_qpt = None
        self._phonons_convergence_criterion = None

    @property
    def phonons_convergence_criterion(self):
        if self._phonons_convergence_criterion is not None:
            return self._phonons_convergence_criterion
        raise ValueError("Need to set 'phonons_convergence_criterion'.")

    @phonons_convergence_criterion.setter
    def phonons_convergence_criterion(self, criterion):
        self._phonons_convergence_criterion = criterion

    @property
    def phonons_qpt(self):
        if self._phonons_qpt is not None:
            return self._phonons_qpt
        raise ValueError("Need to set 'phonons_qpt'.")

    @phonons_qpt.setter
    def phonons_qpt(self, qpt):
        if not is_vector(qpt):
            raise TypeError(
                    "'phonons_qpt' should be a vector but got: '{qpt}'.")
        if len(qpt) != 3:
            raise TypeError(
                    "'phonons_qpt' should be a vector of length 3 but got: "
                    f"'{qpt}'.")
        self._phonons_qpt = qpt

    @BaseIndividualPhononSequencer.nphonons.setter
    def nphonons(self, nphonons):
        self._logger.warning(
                "Cannot set nphonons for convergence study. Nothing is done.")

    @property
    def phonons_specific_input_variables(self):
        return [{"q_points": self.phonons_qpt}]

    @phonons_specific_input_variables.setter
    def phonons_specific_input_variables(self, spec):
        raise ValueError(
                "Only 1 phonon so no specific variables to set.")

    @property
    def scf_converged_parameter(self):
        if self._scf_converged_parameter is not None:
            return self._scf_converged_parameter
        self.compute_convergence()
        return self.scf_converged_parameter

    def compute_convergence(self, *args, **kwargs):
        """Computes the converged scf parameters that converges the phonons.

        Returns
        -------
        frequencies: the list of phonon frequencies for each parameter rooted
                     from the given rootdir.
        deltas: the list of deltas for each phonon branch.
        """
        frequencies, deltas = self._get_phonon_frequencies(*args, **kwargs)
        criterion = self.phonons_convergence_criterion
        for iparam, deltas_this_param in enumerate(deltas):
            # all deltas must be below threshold
            if not all(deltas_this_param <= criterion):
                continue
            self._scf_converged_parameter = {
                    self._parameter_to_converge_input_variable_name: (
                        self._parameters_to_converge[iparam])}
            break
        return frequencies, deltas

    def post_sequence(self):
        SCFConvergenceSequencer.post_sequence(self)

    def plot_convergence(
            self, *args, post_process=True, **kwargs):
        """Plot the convergence of the phonon frequencies with respect to the
        SCF input parameter to converge.

        Parameters
        ----------
        post_process: bool, optional
            If True, this method will post process the plot before
            returning it.
        """
        self._logger.info("Plotting phonon frequencies convergence.")
        frequencies, deltas = self.compute_convergence(**kwargs)
        # directly plot all frequencies on same graph for each param to conv
        plot = Plot(loglevel=self._loglevel)
        # should already be ordered
        xdata = self._add_xdata_to_plot(plot)
        ydata = np.transpose(frequencies)
        # frequencies is a [nparams_to_test x n_phonon_branches] array
        assert len(xdata) == len(frequencies), (str(np.shape(xdata)) +
                                                str(np.shape(frequencies)))
        colors = rand_cmap(len(ydata))
        colors = [colors(i / len(ydata))
                  for i in range(len(ydata))]
        for curve, color in zip(ydata, colors):
            plot.add_curve(
                    xdata, curve, linewidth=2, linestyle="-", marker="s",
                    markersize=5, markerfacecolor=color, color=color)
        plot.xlabel = f"{self._parameter_to_converge_input_variable_name} "
        if self._parameter_to_converge_units is not None:
            plot.xlabel += "[ " + self._parameter_to_converge_units + "]"
        plot.ylabel = r"$\omega$ [cm$^{-1}$]"
        plot.xlims = [xdata[0], xdata[-1]]
        # make a second plot for the deltas
        plot2 = Plot(loglevel=self._loglevel)
        if len(xdata) > 1:
            xdata = xdata[:-1]
        for curve, color in zip(np.transpose(deltas), colors):
            plot2.add_curve(
                    xdata, curve, linewidth=2, linestyle="-", marker="s",
                    markersize=5, markerfacecolor=color, color=color)
        if len(xdata) > 2:
            plot2.xlims = [xdata[0], xdata[-1]]
        self._add_xdata_to_plot(plot2)
        plot2.ylabel = r"$\Delta$" + plot.ylabel
        plot2.xlabel = f"{self._parameter_to_converge_input_variable_name} ["
        plot2.add_hline(
                self.phonons_convergence_criterion,
                linestyle="--", linewidth=2,
                label=(
                    r"$\Delta\omega$=" +
                    str(self.phonons_convergence_criterion) +
                    r"cm$^{-1}$"))
        if self._parameter_to_converge_units is not None:
            plot2.xlabel += "[ " + self._parameter_to_converge_units + "]"
        # create multiplot
        multiplot = MultiPlot(loglevel=self._loglevel)
        for pl in [plot, plot2]:
            multiplot.add_plot(pl, 0)
        # add title
        multiplot.title = (
                f"{self._parameter_to_converge_input_variable_name} phonon "
                + f"convergence at $q = ${self.phonons_qpt}")
        if post_process:
            return self._post_process_plot(multiplot)
        return multiplot

    @abc.abstractmethod
    def _get_phonon_frequencies_from_log(self, log):
        pass

    def _get_phonon_workdir(self, iph, scf_calc, **kwargs):
        """Computes the phonn workdir for the given scf calculation.
        """
        icalc = self.sequence.index(scf_calc)
        return os.path.join(
                self.phonons_workdir,
                self._parameter_to_converge_input_variable_name,
                (self._parameter_to_converge_input_variable_name + "_" +
                 self._convert_param_to_str(
                     self._parameters_to_converge[icalc], icalc)))

    def _get_phonon_frequencies(self, rootdir=None):
        if rootdir is None:
            rootdir = self.phonons_workdir
        frequencies = []
        for ph_calc in self.sequence:
            if rootdir not in ph_calc.workdir:
                # this is not a phonon calc
                continue
            with ph_calc.calculation_directory as calcdir:
                with calcdir.log_file as log:
                    frequencies.append(
                            self._get_phonon_frequencies_from_log(log))
        if len(frequencies) > 1:
            deltas = np.abs(
                    np.array(frequencies[:-1]) - np.array(frequencies[-1]))
        else:
            # no deltas to show
            deltas = np.zeros(np.shape(frequencies))
        return frequencies, deltas
