from ..band_structure_sequencer import (
        BandStructureSequencer,
        BandStructureComparatorSequencer,
        )
from ...plotters import MultiPlot
from ...post_processors import FatBand


class AbinitBandStructureSequencer(BandStructureSequencer):
    """Sequencer for the abinit script that is in charge to produce a band
    structure. Also handles Fatbands when the computation asked for it.
    """
    _loggername = "AbinitBandStructureSequencer"

    def __init__(self, *args, **kwargs):
        BandStructureSequencer.__init__(self, "abinit", *args, **kwargs)

    @property
    def band_structure_input_variables(self):
        return BandStructureSequencer.band_structure_input_variables.fget(self)

    @band_structure_input_variables.setter
    def band_structure_input_variables(self, input_vars):
        if not isinstance(input_vars, dict):
            raise TypeError(
                    f"Expected dict for band structure input vars got instead"
                    f": {input_vars}")
        self._set_input_var_to(input_vars, "irdden", 1)
        if input_vars.get("iscf", 7) > 0:
            self._set_input_var_to(input_vars, "iscf", -2)
        if input_vars.get("kptopt", 1) > 0:
            self._set_input_var_to(
                    input_vars, "kptopt",
                    -(len(self.band_structure_kpoint_path) - 1))
        self._set_input_var_to(
                input_vars, "ndivk",
                ([self.band_structure_kpoint_path_density] *
                 -input_vars["kptopt"]))
        self._set_input_var_to(
                input_vars, "kptbounds",
                [list(k.values())[0]
                 for k in self.band_structure_kpoint_path])
        BandStructureSequencer.band_structure_input_variables.fset(
                self, input_vars)

    def init_sequence(self):
        BandStructureSequencer.init_sequence(self)
        bscalc = self.get_sequence_calculation("band_structure")
        bscalc.link_den_from = self.scf_workdir

    def plot_band_structure(self, **kwargs):
        """Create the fat band structure alongside the usual band structure.
        """
        # make band structure
        BandStructureSequencer.plot_band_structure(
                self, name_extension="band_structure")
        # make fatbands if they were computed
        if not self._check_fatbands_computed():
            return self._dft_plot
        fatbands = FatBand.from_calculation(self.band_structure_workdir)
        fatband_multiplots = []
        ncols = 4
        for fatband in fatbands:
            fatband_plot = fatband.get_plot(
                    yunits="eV", ylabel="Energy", color="k", linewidth=2,
                    all_k_labels=self.all_k_labels,
                    fat_band_color="r",
                    symmetry=None,
                    fermi_at_zero=self.plot_calculation_parameters.get(
                        "fermi_at_zero", True),
                    line_at_zero=self.plot_calculation_parameters.get(
                        "fermi_at_zero", True),
                    adjust_axis=self.plot_calculation_parameters.get(
                        "adjust_axis", True),
                    show_bandgap=self.plot_calculation_parameters.get(
                        "show_bandgap", False),
                    )
            self._apply_plot_parameters(fatband_plot)
            if fatband.orbital is None:
                name_ext = f"fatband_{fatband.atom_name}{fatband.atom_number}"
                name_ext += f"_l={fatband.l_number}_m={fatband.m}"
            else:
                name_ext = f"fatband_atom{fatband.atom_number}_"
                name_ext += f"{fatband.orbital}"
            fatband_plot.title = name_ext
            self._post_process_plot(fatband_plot, name_extension=name_ext)
            # put all fatbands of the same atom into a multiplot
            if fatband.atom_number + 1 > len(fatband_multiplots):
                # create multiplot
                multiplot = MultiPlot()
                multiplot.title = f"Fatband atom #{fatband.atom_number}"
                fatband_multiplots.append(multiplot)
            else:
                multiplot = fatband_multiplots[fatband.atom_number]
            multiplot.add_plot(
                    fatband_plot, len(multiplot.plots) // ncols)
        for i, fatband_multiplot in enumerate(fatband_multiplots):
            self._post_process_plot(
                fatband_multiplot,
                name_extension=f"all_fatbands_atom{i + 1}")

    def _check_fatbands_computed(self):
        if self.band_structure_input_variables.get("pawfatbnd", 0) > 0:
            return True
        if self.band_structure_input_variables.get("prtprocar", 0) > 0:
            return True
        return False


class AbinitBandStructureComparatorSequencer(
        AbinitBandStructureSequencer,
        BandStructureComparatorSequencer):
    """Sequencer to make comparisons of band structures using variations
    in SCF parameters with Abinit.
    """
    _loggername = "AbinitBandStructureComparatorSequencer"

    def __init__(self, *args, **kwargs):
        AbinitBandStructureSequencer.__init__(self, *args, **kwargs)
        BandStructureComparatorSequencer.__init__(
                self, "abinit", *args, **kwargs)

    def init_sequence(self, *args, **kwargs):
        BandStructureComparatorSequencer.init_sequence(self, *args, **kwargs)
        # for all bs calc, link the DEN from their respective SCF calcs
        for calc in self.get_sequence_calculation("band_structure"):
            calc.link_den_from = calc.dependencies[0]

    def plot_band_structure(self, *args, **kwargs):
        BandStructureComparatorSequencer.plot_band_structure(
                self, *args, **kwargs)
