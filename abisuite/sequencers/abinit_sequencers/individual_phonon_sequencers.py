import os

import numpy as np

from .ddk_sequencer import DDKSequencer
from ..bases import SequenceCalculation
from ..individual_phonon_sequencers import (
        BaseIndividualPhononSequencer, BasePhononConvergenceSequencer,
        )
from ...constants import HARTREE_TO_CM
from ...routines import is_2d_arr


class AbinitIndividualPhononSequencer(
        BaseIndividualPhononSequencer, DDKSequencer):
    """Individual phonon sequencer for abinit. The full sequence will depend
    if the electric field response is needed or not or if the dynamical matrix
    is needed or not at the end.
    """
    _all_sequencer_prefixes = ("scf_", "ddk_", "phonons_")
    _loggername = "AbinitIndividualPhononSequencer"

    def __init__(self, *args, **kwargs):
        BaseIndividualPhononSequencer.__init__(self, "abinit", *args, **kwargs)
        DDKSequencer.__init__(self, *args, **kwargs)
        self._phonons_qpoint_grid = None
        # use a None init parameter to force user to tell to compute electric
        # field response or not.
        self._compute_electric_field_response = None

    @property
    def compute_electric_field_response(self):
        if self._compute_electric_field_response is not None:
            return self._compute_electric_field_response
        raise ValueError("Need to set 'compute_electric_field_response'.")

    @compute_electric_field_response.setter
    def compute_electric_field_response(self, compute):
        self._compute_electric_field_response = compute

    @property
    def phonons_qpoint_grid(self):
        if self._phonons_qpoint_grid is not None:
            return self._phonons_qpoint_grid
        raise ValueError("Need to set 'phonons_qpoint_grid'.")

    @phonons_qpoint_grid.setter
    def phonons_qpoint_grid(self, qpts):
        if not is_2d_arr(qpts):
            if len(qpts) == 4:
                # only one qpt
                qpts = [qpts]
            else:
                raise ValueError(
                        f"Expected a list of qpts coordinates but got: "
                        f"'{qpts}'."
                        )
        if self._nphonons is None:
            self.nphonons = len(qpts)
        else:
            if len(qpts) != self.nphonons:
                raise ValueError(
                        f"Expected '{self.nphonons}' qpts but got "
                        f"'{len(qpts)}' instead.")
        self._phonons_qpoint_grid = qpts

    def init_scf_sequence(self, *args, **kwargs):
        """init the sequences before the phonons calculations.
        """
        BaseIndividualPhononSequencer.init_scf_sequence(self, *args, **kwargs)
        if self.compute_electric_field_response:
            self.init_ddk_sequence()

    def init_sequence(self):
        BaseIndividualPhononSequencer.init_sequence(self)
        # if Gamma and if electric field response, link ddk calc
        if self._nphonons is None:
            return
        for i in range(self.nphonons):
            qpt = self._get_phonons_specific_input_variables(i)["qpt"]
            if self.compute_electric_field_response and qpt == [0.0, 0.0, 0.0]:
                self.link_ddk_to_phonons()

    def link_ddk_to_phonons(self):
        ddk_calc = None
        # find ddk calc
        for calc in self.sequence:
            if self.ddk_workdir in calc.workdir:
                # there should be only one so break after
                ddk_calc = calc
                break
        else:
            raise LookupError()
        # link to phonon calcs
        if self.phonons_input_variables is None:
            # nothing to link
            return
        for calc in self.sequence:
            # only gamma phonon need to be linked
            if self.phonons_workdir in calc.workdir:
                if "qpt" not in calc.input_variables:
                    continue
                if list(calc.input_variables["qpt"]) == [0.0, 0.0, 0.0]:
                    calc.dependencies.append(ddk_calc)
                    break
        else:
            raise LookupError()

    def _get_phonon_calculation(
            self, iph, scf_calculation):
        # create a phonon calculation to the sequence based on the phonon index
        # base_workdir is the root directory
        if self.phonons_input_variables is not None:
            phvars = self.phonons_input_variables.copy()
            phvars.update(self._get_phonons_specific_input_variables(iph))
        else:
            phvars = None
        workdir = self._get_phonon_workdir(iph, scf_calculation)
        phon_calc = SequenceCalculation(
                        "abinit", workdir, phvars,
                        self.phonons_calculation_parameters,
                        loglevel=self._loglevel)
        phon_calc.dependencies.append(scf_calculation)  # gs calculation
        phon_calc.load_geometry_from = scf_calculation
        phon_calc.load_kpts_from = scf_calculation
        return phon_calc

    def _get_phonons_specific_input_variables(self, iph):
        # returns specific input variable for phonon number 'iph'.
        qpt = self.phonons_qpoint_grid[iph]
        if not isinstance(qpt, list):
            qpt = list(qpt)
        spec = {"kptopt": 3, "rfdir": [1, 1, 1], "rfphon": 1, "irdwfk": 1,
                "irdddk": 0, "qpt": qpt, "nqpt": 1}
        if qpt == [0.0, 0.0, 0.0]:
            # read ddk if electric field response
            if self.compute_electric_field_response:
                spec["irdddk"] = 1
                # compute electric field response only for gamma
                spec["rfelfd"] = 3
            # set kptopt to 2 instead for Gamma because we can use TR syms
            spec["kptopt"] = 2
        try:
            spec.update(self.phonons_specific_input_variables[iph])
        except ValueError:
            # don't mind if phonons specific input vars are not set
            pass
        return spec


class AbinitPhononConvergenceSequencer(
        BasePhononConvergenceSequencer,
        AbinitIndividualPhononSequencer,
        DDKSequencer):
    """Base class for abinit phonon convergence sequencers.
    """
    _all_sequencer_prefixes = ("scf_", "ddk_", "phonons_", "plot_", )

    def __init__(self, *args, **kwargs):
        BasePhononConvergenceSequencer.__init__(
                self, "abinit", *args, **kwargs)
        AbinitIndividualPhononSequencer.__init__(
                self, *args, **kwargs)
        DDKSequencer.__init__(self, *args, **kwargs)
        self._nphonons = 1

    @property
    def phonons_specific_input_variables(self):
        aips = AbinitIndividualPhononSequencer
        return aips.phonons_specific_input_variables.fget(self)

    @phonons_specific_input_variables.setter
    def phonons_specific_input_variables(self, spec):
        aips = AbinitIndividualPhononSequencer
        aips.phonons_specific_input_variables.fset(self, spec)

    @property
    def phonons_qpoint_grid(self):
        return [self.phonons_qpt]

    def init_sequence(self):
        BasePhononConvergenceSequencer.init_sequence(self)
        # if we need to compute electric field response, we need to add
        # ddk calculations for gamma phonons conly
        if list(self.phonons_qpt) != [0.0, 0.0, 0.0]:
            return
        if not self.compute_electric_field_response:
            return
        self.init_ddk_sequence()

    def init_ddk_sequence(self):
        # adds ddk calculations
        n_scf_calcs = len(self._parameters_to_converge)
        scf_calcs = self.sequence[:n_scf_calcs]
        # phonon calcs should have been prepared already
        ph_calcs = self.sequence[n_scf_calcs:]
        ddk_calcs = []
        for icalc, (scf_calc, ph_calc, param) in enumerate(zip(
                scf_calcs, ph_calcs, self._parameters_to_converge)):
            # arg of 0 is because we only do 1 phonon for each scf calc
            workdir = os.path.join(
                    self.ddk_workdir,
                    self._parameter_to_converge_input_variable_name,
                    (self._parameter_to_converge_input_variable_name + "_" +
                     self._convert_param_to_str(param, icalc)))
            if self.ddk_input_variables is None:
                ddk_input_vars = {}
            else:
                ddk_input_vars = self.ddk_input_variables.copy()
            ddk_input_vars.update(
                    {"qpt": self.phonons_qpt,
                     self._parameter_to_converge_input_variable_name: param}
                    )
            seq = SequenceCalculation(
                    "abinit", workdir, ddk_input_vars,
                    self.ddk_calculation_parameters, loglevel=self._loglevel)
            seq.dependencies.append(scf_calc)
            seq.load_geometry_from = scf_calc
            seq.load_kpts_from = scf_calc
            self.sequence.append(seq)
            ddk_calcs.append(seq)
            # then add ddk calc as dependency to phonon calc
            ph_calc.dependencies.append(seq)
        self.sequence += ddk_calcs

    def _get_phonon_frequencies_from_log(self, log):
        eigs = log.dtsets[0]["phonon_frequencies"]["eigenvalues"]
        return np.array(eigs) * HARTREE_TO_CM
