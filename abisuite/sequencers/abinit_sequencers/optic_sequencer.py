from .ddk_sequencer import DDKSequencer
from ..bases import SequenceCalculation
from ..nscf_sequencers import NSCFSequencer
from ...post_processors import DielectricTensor
from ...routines import is_list_like


class AbinitOpticSequencer(DDKSequencer, NSCFSequencer):
    """Sequencer for an optic run. The sequence goes as follows:

    - GS SCF run
    - NSCF run for dense kmesh and lots of bands in IBZ using DEN from SCF run
    - Same NSCF calc but in complete BZ from WFK of first NSCF run
      (Full BZ or half if using TRS)
    - DDK
    - optic
    """
    _all_sequencer_prefixes = (
            "scf_", "nscf_", "nscffbz_", "ddk_", "optic_", "plot_", )
    _loggername = "AbinitOpticSequencer"

    def __init__(self, *args, **kwargs):
        DDKSequencer.__init__(self, *args, **kwargs)
        NSCFSequencer.__init__(self, "abinit", *args, **kwargs)
        self._compute_non_linear_optical_response = None
        self._force_kptopt = False

    @property
    def compute_non_linear_optical_response(self):
        if self._compute_non_linear_optical_response is not None:
            return self._compute_non_linear_optical_response
        raise ValueError(
                "Need to set 'compute_non_linear_optical_response' to True or "
                "False. This will decide if we ise TRS or not.")

    @compute_non_linear_optical_response.setter
    def compute_non_linear_optical_response(self, compute):
        self._compute_non_linear_optical_response = compute

    @property
    def force_kptopt(self):
        return self._force_kptopt

    @force_kptopt.setter
    def force_kptopt(self, force):
        self._force_kptopt = force

    @property
    def nscffbz_input_variables(self):
        return self._nscffbz_input_variables

    @nscffbz_input_variables.setter
    def nscffbz_input_variables(self, input_vars):
        self._nscffbz_input_variables = input_vars.copy()

    @NSCFSequencer.nscf_input_variables.setter
    def nscf_input_variables(self, input_vars):
        # iscf must be set to -2
        # kptopt must be set to 1
        # make copy to not modify dict from user in case it is used elsewhere
        input_vars_copy = input_vars.copy()
        occopt = input_vars_copy.get("occopt", 1)
        if input_vars_copy.get("iscf", 1) > 0:
            self._set_input_var_to(
                    input_vars_copy, "iscf",
                    -3 if occopt in (0, 2) else -2)
        if self.force_kptopt:
            # only modify kptopt if it is not set
            if "kptopt" not in input_vars_copy:
                self._set_input_var_to(input_vars_copy, "kptopt", 1)
        else:
            # kptopt must be set to 1
            if input_vars_copy.get("kptopt", 1) != 1:
                self._set_input_var_to(input_vars_copy, "kptopt", 1)
        # remove irdden and irdwfk from input vars
        # this is done to prevent calculation linking to automatically link
        # files we don't want because second NSCF run requires DEN from SCF
        # run but wfk from first nscf run (not sure why?!?)
        # these variables will be automatically set when we link the files
        # we want in the init_sequence function.
        input_vars_copy.pop("irdden", None)
        input_vars_copy.pop("irdwfk", None)
        NSCFSequencer.nscf_input_variables.fset(self, input_vars_copy)
        # set nscffbz_input_variable at the same time
        fbz = input_vars_copy.copy()
        if self.force_kptopt:
            # only modify kptopt if not present:
            if "kptopt" not in fbz:
                if self.compute_non_linear_optical_response:
                    fbz["kptopt"] = 3
                else:
                    # we can use TRS (ref: abinit tutorial)
                    fbz["kptopt"] = 2
        else:
            if self.compute_non_linear_optical_response:
                fbz["kptopt"] = 3
            else:
                # we can use TRS (ref: abinit tutorial)
                fbz["kptopt"] = 2
        self.nscffbz_input_variables = fbz

    @DDKSequencer.ddk_input_variables.setter
    def ddk_input_variables(self, input_vars):
        # start from nscffbz input vars and update
        # kptopt should have been set to 3 from these
        copy = self.nscffbz_input_variables.copy()
        copy.update(input_vars)
        input_vars = copy
        DDKSequencer.ddk_input_variables.fset(self, input_vars)
        # for optic run, set nstep to 1
        nstep = self.ddk_input_variables.get("nstep", 30)
        if nstep != 1:
            self._set_input_var_to(
                    self.ddk_input_variables, "nstep", 1)
        # set nline to 0
        nline = self.ddk_input_variables.get("nline", 4)
        if nline != 0:
            self._set_input_var_to(
                    self.ddk_input_variables, "nline", 0)
        # set prtwf to 3 for size reduced ddk files for optic
        self._set_input_var_to(
                self.ddk_input_variables, "prtwf", 3)
        # set kptopt to whatever kptopt was set in the 2nd nscf run
        self._set_input_var_to(
                self.ddk_input_variables, "kptopt",
                self.nscffbz_input_variables["kptopt"])

    @property
    def optic_input_variables(self):
        return self._optic_input_variables

    @optic_input_variables.setter
    def optic_input_variables(self, input_vars):
        # check that we are consisten
        # if we ask for non lin comp, we should have fun the 2nd nscf run
        # with kptopt = 3
        for varname in ("num_nonlin_comp", "num_nonlin2_comp"):
            cnlor = self.compute_non_linear_optical_response
            if varname not in input_vars:
                # automatic 0
                continue
            if input_vars[varname] > 0 and not cnlor:
                raise ValueError(
                        "You're asking for a non linear comp but the wfk was "
                        "computed only on hald the FBZ using TRS (kptopt = 2)."
                        )
        self._optic_input_variables = input_vars

    def init_nscf_sequence(self):
        # do the same as usual but manually link the DEN and WFK files
        NSCFSequencer.init_nscf_sequence(self)
        nscf = self.get_sequence_calculation("nscf")
        nscf.link_den_from = self.scf_workdir
        nscf.link_wfk_from = self.scf_workdir

    def init_second_nscf_sequence(self):
        seq = SequenceCalculation(
                "abinit",
                self.nscffbz_workdir,
                self.nscffbz_input_variables,
                self.nscffbz_calculation_parameters,
                loglevel=self._loglevel)
        seq.load_geometry_from = self.scf_workdir
        seq.dependencies.append(self.get_sequence_calculation("scf"))
        seq.dependencies.append(self.get_sequence_calculation("nscf"))
        seq.link_den_from = self.scf_workdir
        seq.link_wfk_from = self.nscf_workdir
        self.sequence.append(seq)

    def init_ddk_sequence(self):
        DDKSequencer.init_ddk_sequence(self)
        # change dependancy from SCF to last NSCF calc
        ddk = self.get_sequence_calculation("ddk")
        if not is_list_like(ddk):
            ddk = [ddk]
        for ddkcalc in ddk:
            last_nscf = self.get_sequence_calculation("nscffbz")
            ddkcalc.dependencies.clear()
            ddkcalc.dependencies.append(last_nscf)
            ddkcalc.load_geometry_from = last_nscf
            ddkcalc.link_wfk_from = last_nscf

    def init_sequence(self):
        # make GS run and first nscf fun
        NSCFSequencer.init_sequence(self)
        # add second nscf sequence calculation
        self.init_second_nscf_sequence()
        # add second nscf run
        DDKSequencer.init_sequence(self)
        self.init_optic_sequence()

    def init_optic_sequence(self):
        seq = SequenceCalculation(
                    "abinit_optic", self.optic_workdir,
                    self.optic_input_variables,
                    self.optic_calculation_parameters,
                    loglevel=self._loglevel)
        # the optic run has the DDK run as dependencies and the last nscf run
        # (to get the WFK in the FBZ)
        ddk = self.get_sequence_calculation("ddk")
        if not is_list_like(ddk):
            ddk = [ddk]
        nscffbz = self.get_sequence_calculation("nscffbz")
        seq.dependencies += ddk + [nscffbz]
        self.sequence.append(seq)

    def plot_dielectric_tensor(self):
        """Plots the dielectric tensor.
        """
        # first check if we need to plot the dielectric tensor at all
        # check in the optic input variables if we need
        lincomp = self.optic_input_variables.get("num_lin_comp", 0)
        if not lincomp:
            self._logger.info("Don't need to plot dielectric tensor.")
            return
        diel = DielectricTensor.from_calculation(
                self.optic_workdir, loglevel=self._loglevel)
        multi_diel_plot = diel.get_plot(multiplot=True)
        diel_plots = diel.get_plot(multiplot=False)
        all_plots = list(diel_plots) + [multi_diel_plot]
        # apply other properties to plot
        for param, value in self.plot_calculation_parameters.items():
            if param in ("show", "save", "save_plot", "show_legend_on"):
                # postfix parameters
                continue
            for plot in all_plots:
                setattr(plot, param, value)
        self._post_process_plot(multi_diel_plot, name_extension="combined")
        self._post_process_plot(diel_plots[0], name_extension="real_part")
        self._post_process_plot(diel_plots[1], name_extension="imag_part")

    def post_sequence(self, *args, **kwargs):
        super().post_sequence(*args, **kwargs)
        # plot all the optic data
        # for now only the dielectric tensor is implemented
        self.plot_dielectric_tensor()
