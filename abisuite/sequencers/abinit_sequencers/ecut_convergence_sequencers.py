from .individual_phonon_sequencers import (
        AbinitPhononConvergenceSequencer,
        )
from ..ecut_convergence_sequencers import (
        BaseEcutConvergenceSequencer,
        BaseEcutPhononConvergenceSequencer,
        )


class AbinitEcutConvergenceSequencer(BaseEcutConvergenceSequencer):
    """Convergence sequencer for ecut parameter with abinit.
    """
    _loggername = "AbinitEcutConvergenceSequencer"

    def __init__(self, *args, **kwargs):
        BaseEcutConvergenceSequencer.__init__(self, "abinit", *args, **kwargs)
        self._ecuts_input_variable_name = None

    @property
    def ecuts_input_variable_name(self):
        if self._ecuts_input_variable_name is not None:
            return self._ecuts_input_variable_name
        raise ValueError("Need to set 'ecuts_input_variable_name'.")

    @ecuts_input_variable_name.setter
    def ecuts_input_variable_name(self, name):
        if name not in ("ecut", "pawecutdg", ):
            raise ValueError(f"'{name}'.")
        self._ecuts_input_variable_name = name

    @property
    def ecuts_units(self):
        return "Ha"

    @property
    def _parameter_to_converge_input_variable_name(self):
        return self.ecuts_input_variable_name


class AbinitEcutPhononConvergenceSequencer(
        AbinitEcutConvergenceSequencer,
        AbinitPhononConvergenceSequencer,
        BaseEcutPhononConvergenceSequencer,
        ):
    """Convergence sequencer for the ecut parameter vs phonon frequencies.
    """
    _all_sequencer_prefixes = ("scf_", "ddk_", "phonons_", "plot_", )
    _loggername = "AbinitEcutPhononConvergenceSequencer"

    def __init__(self, *args, **kwargs):
        AbinitPhononConvergenceSequencer.__init__(self, *args, **kwargs)
        AbinitEcutConvergenceSequencer.__init__(self, *args, **kwargs)
        BaseEcutPhononConvergenceSequencer.__init__(
                self, "abinit", *args, **kwargs)

    def init_sequence(self):
        AbinitPhononConvergenceSequencer.init_sequence(self)
