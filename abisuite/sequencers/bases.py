import abc
import logging
import os

from ..bases import BaseUtility
from ..exceptions import DevError
from ..handlers import CalculationDirectory, CALCTYPES_TO_INPUT_FILE_CLS
from ..launchers import __ALL_LAUNCHERS__
from ..launchers.exceptions import TooManyCalculationsInQueueError
from ..plotters import MultiPlot, Plot
from ..routines import full_abspath, is_list_like
from ..status_checkers import (
        CalculationNotConvergedError, CalculationNotFinishedError,
        )


# list of input variables that are exceptions to differences between
# an old and a new calculation (e.g.: variables that do not influence
# the outcome of a calculation like 'prefix' in qe_pw and can change
# from a old to new calculation because we symlinked a calculation for
# some reason)
__EXCEPTION_VARIABLES__ = {
        "qe_ph": ["prefix", "outdir", "fildyn", "title"],
        "qe_pw": ["diago_cg_maxiter", "prefix", "outdir"],
        "qe_epw": ["dvscf_dir", ],
        "abinit": ["iomode", "istwfk", "nstep", ],
        }


class BaseSequencer(BaseUtility, abc.ABC):
    """Base class for any Sequencer object.
    """
    _all_sequencer_prefixes = None

    def __init__(
            self, software, *args, bypass_convergence_check=False,
            bypass_sequence_comparison=False, **kwargs):
        """BaseSequencer init method.

        Parameters
        ----------
        software: str
            The software used for the sequence. Either 'abinit' or 'qe' or
            'wannier90' for now.
        bypass_convergence_check: bool, optional
            If True, the sequencer won't care if a calculation is converged or
            not when it is finished before continuing the sequence.
        bypass_sequence_comparison: bool, optional
            If True, the sequence validation is bypassed. That means we don't
            check if all the calculations are coherent when we rerun the
            sequencer. Use this if all calculations were successfull and you
            don't change any parameters but want to get the results faster.
        """
        super().__init__(*args, **kwargs)
        self.software = software
        self.sequence = Sequence(loglevel=self._loglevel)
        self.bypass_sequence_comparison = bypass_sequence_comparison
        self.bypass_convergence_check = bypass_convergence_check
        if self._all_sequencer_prefixes is None:
            raise DevError("Need to specify '_all_sequencer_prefixes'.")
        for seq in self._all_sequencer_prefixes:
            # set up all seq_calculation_parameters
            setattr(self, "_" + seq + "calculation_parameters", {})
            # set all workdirs
            setattr(self, "_" + seq + "workdir", None)
            # set all input variables
            setattr(self, "_" + seq + "input_variables", None)
        self._has_been_run = False

    def __getattr__(self, attr):
        for seq_prefix in self._all_sequencer_prefixes:
            if attr.startswith(seq_prefix):
                if attr.endswith("workdir"):
                    return getattr(self, "_" + seq_prefix + "workdir")
                # I don't remember why this needs to be commented out...
                # find out why! (FG: 03/08/2020)
                # Answer: because each sequencers defines the property for the
                # input variables which returns the '_{seq}_input_variables'
                # attribute (FG: 03/08/2020).
                # Wow that was a quick self answered question haha
                # if attr.endswith("input_variables"):
                #    return getattr(self, "_" + seq_prefix + "input_variables")
                if attr.endswith("calculation_parameters"):
                    return getattr(
                            self, "_" + seq_prefix + "calculation_parameters")
                attr_name = attr.split(seq_prefix)[-1]
                if attr_name not in getattr(
                        self, seq_prefix + "calculation_parameters"):
                    raise AttributeError(
                            f"'{self.__class__}': No attr named: '{attr}'")
                return getattr(
                        self,
                        "_" + seq_prefix + "calculation_parameters")[attr_name]
        else:
            raise AttributeError(
                    f"'{self.__class__}': No attr named: '{attr}'")

    def __repr__(self):
        if not self.sequence:
            self.init_sequence()
        return (str(self.__class__) + ": [\n  " + "\n  ".join(
            [repr(x) for x in self.sequence]) + "]")

    def __setattr__(self, attr, value):
        if attr in dir(self) or attr.startswith("_") or attr in (
                "sequence", "software"):
            super().__setattr__(attr, value)
            return
        # check if a 'workdir' attribute
        if attr in ([prefix + "workdir"
                     for prefix in self._all_sequencer_prefixes]):
            if value is not None:
                value = full_abspath(value)
            super().__setattr__(attr, value)
            return
        # check if a 'calculation_parameter' attribute:
        if attr in ([prefix + "calculation_parameters"
                     for prefix in self._all_sequencer_prefixes]):
            super().__setattr__(attr, value)
            return
        # check if a 'parameter' for calculation_parameters dict
        for seq_prefix in self._all_sequencer_prefixes:
            if attr.startswith(seq_prefix):
                attr_name = attr.split(seq_prefix)[-1]
                getattr(
                    self, seq_prefix + "calculation_parameters"
                    )[attr_name] = value
                return
        else:
            raise AttributeError(f"No attr named: {attr}")

    @property
    def bypass_sequence_comparison(self):
        return self.sequence.bypass_sequence_comparison

    @bypass_sequence_comparison.setter
    def bypass_sequence_comparison(self, bypass):
        self.sequence.bypass_sequence_comparison = bypass

    @property
    def bypass_convergence_check(self):
        return self.sequence.bypass_convergence_check

    @bypass_convergence_check.setter
    def bypass_convergence_check(self, bypass):
        self.sequence.bypass_convergence_check = bypass

    @property
    def has_been_run(self):
        """True if the calculation has been run/launched at least once.
        """
        return self._has_been_run

    @property
    def sequence_completed(self):
        # check that all calculations are completed
        # self.clear_sequence()
        # self.init_sequence()
        # sequence is completed if all calculations from sequence are completed
        if not self.sequence:
            # init the sequence and rerun
            try:
                self.init_sequence()
                return self.sequence_completed
            except (TypeError, ValueError, KeyError):
                # some variables are not set
                return False
            except AttributeError as err:
                self._logger.exception(
                        f"An AttributeError occured within sequencer '{self}'."
                        )
                raise err
        for seq in self.sequence:
            calc = seq.calculation_directory
            if not calc.exists:
                self._logger.debug("{seq} does not exists.")
                return False
            calc.read()
            if calc.status["calculation_finished"] is not True:
                self._logger.warning(
                        f"{calc.path} is not finished.")
                return False
            if "calculation_converged" in calc.status:
                conv = calc.status["calculation_converged"]
                if conv is False and not self.bypass_convergence_check:
                    self._logger.error(
                            f"{calc.path} has not converged.")
                    raise CalculationNotConvergedError(seq.workdir)
            # calculation completed and converged. But is it the same as the
            # calculation we want?
            try:
                # this will raise an error if there's something different
                seq.is_ready_to_launch
            except Exception:
                # calculation is different
                self._logger.debug("{seq} is different.")
                return False
        # all calculations are completed/converged + they are the same as the
        # one we want. everything is done then!
        return True

    @abc.abstractmethod
    def init_sequence(self, *args, **kwargs):
        # this method is the backbone of the sequencer. it creates the
        # sequence of calculations and the corresponding dependencies.
        pass

    def clear_sequence(self):
        bypass_convergence = self.bypass_convergence_check
        bypass_comparison = self.bypass_sequence_comparison
        del self.sequence
        self.sequence = Sequence(
                bypass_convergence_check=bypass_convergence,
                bypass_sequence_comparison=bypass_comparison,
                loglevel=self._loglevel)

    def get_sequence_calculation(self, prefix):
        """Gets (all/the) sequence calculation object(s) corresponding to a
        given prefix.

        Parameters
        ----------
        prefix: str
            The prefix to get the sequence calculation from.

        Returns
        -------
        SequenceCalculation objec / list:
            The calculation object or the list of these objects (if more than
            1) corresponding to the given prefix.
        """
        if not len(self.sequence):
            raise RuntimeError(
                "Call 'init_sequence' before getting a Sequence"
                " Calulation.")
        if not prefix.endswith("_"):
            prefix += "_"
        if prefix not in self._all_sequencer_prefixes:
            raise KeyError(prefix)
        seq_workdir = full_abspath(getattr(self, prefix + "workdir"))
        allcalcs = []
        for calc in self.sequence:
            if calc.workdir.startswith(seq_workdir):
                allcalcs.append(calc)
        if not len(allcalcs):
            raise LookupError(
                    f"Could not find calculation: '{prefix}' in sequence.")
        if len(allcalcs) == 1:
            return allcalcs[0]
        return allcalcs

    def launch(self, *args, **kwargs):
        """Launches the sequence of calculations.
        """
        need_to_launch = True
        self._has_been_run = True
        while need_to_launch:
            calcs_to_launch = self._do_launch(*args, **kwargs)
            if self.sequence_completed:
                self._logger.info("This sequence is done!")
                self.post_sequence()
                return
            # if the one of the last calculation was local, relaunch
            need_to_launch = any([x.is_local_launch
                                 for x in calcs_to_launch])
            if need_to_launch:
                self._logger.debug(
                        "Automatically continuing sequence since one of "
                        "the last launched calculation was a local one.")
                continue
            else:
                break
        # we had to stop here
        self._logger.info(
                "Sequence interrupted: need to relaunch after calculations"
                " are completed.")

    def post_sequence(self, *args, **kwargs):
        """This method executes post sequence tasks when sequence is completed.

        By default there is nothing else to do.
        """
        pass

    def run(self, *args, **kwargs):
        self.launch(*args, **kwargs)

    def write(self, *args, **kwargs):
        """Like launch but only write calculations.
        """
        self.launch(*args, run=False, **kwargs)

    def _do_launch(self, *args, run=True, **kwargs):
        # actually launch the sequence and return what have been launched
        self.clear_sequence()
        self.init_sequence()
        calcs_to_launch = []
        # compute calculations to launch now
        for calc in self.sequence:
            if calc.is_ready_to_launch:
                self._logger.debug(f"{calc} needs to be launched.")
                calcs_to_launch.append(calc)
            else:
                self._logger.debug(f"{calc} already launched/done.")
        # launch all calculations that need to be launched
        for i, calc in enumerate(calcs_to_launch):
            if run:
                self._logger.info(f"Launching calculation: {calc}")
            else:
                self._logger.info(f"Writing calculation: {calc}")
            try:
                calc.launch(*args, run=run, **kwargs)
            except TooManyCalculationsInQueueError:
                # already too many calculations
                self._logger.error(
                    "Too many calculations in queue, cannot launch "
                    f"{calc}")
                return calcs_to_launch[:i]
        return calcs_to_launch

    def _post_process_plot(self, plot, name_extension=""):
        """Method used to post process plots in a generic way.

        Parameters
        ----------
        plot: Plot object to handle
        name_extension: str, optional
            String extension to the filenames used to save the plot.
        """
        if "plot_" not in self._all_sequencer_prefixes:
            raise RuntimeError(
                    "Not supposed to handle plots if it's not part of the "
                    "Sequencer.")
        if "grid" not in self.plot_calculation_parameters:
            # always add grid if not specified
            plot.grid = True
        # show and save
        show = self.plot_calculation_parameters.get("show", True)
        save = self.plot_calculation_parameters.get("save", None)
        if isinstance(plot, MultiPlot):
            show_legend_on = self.plot_calculation_parameters.get(
                    "show_legend_on", None)
            plot.plot(show=show, show_legend_on=show_legend_on)
        elif isinstance(plot, Plot):
            plot.plot(show=show)
        else:
            raise TypeError(plot)
        if name_extension and not name_extension.startswith("_"):
            name_extension = "_" + name_extension
        if save is not None:
            if not save.endswith(".pdf"):
                save = save + name_extension + ".pdf"
            else:
                save = save[:-4] + name_extension + ".pdf"
            if save is not None:
                self._logger.info(
                        f"Saving plot at '{save}'.")
                plot.save(save, overwrite=True)
        save_pickle = self.plot_calculation_parameters.get(
                "save_pickle", None)
        if save_pickle is not None:
            if not save_pickle.endswith(".pickle"):
                save_pickle = save_pickle + name_extension + ".pickle"
            else:
                save_pickle = (
                        save_pickle[:-7] +
                        name_extension + ".pickle")
            self._logger.info(
                    f"Saving plot pickle at: '{save_pickle}'.")
            plot.save_plot(save_pickle, overwrite=True)  # overwrite by default
        return plot

    def _set_input_var_to(self, input_vars, varname, value):
        # method to change an input variable and warning user at same time
        if varname not in input_vars:
            self._logger.debug(f"Setting '{varname}' to '{value}'.")
            input_vars[varname] = value
        if input_vars[varname] != value:
            self._logger.debug(f"Setting '{varname}' to '{value}'.")
            input_vars[varname] = value


class SequenceCalculation(BaseUtility):
    """Class that represents a calculation to do for a Sequencer object.
    """
    _default_calcdir_loglevel = logging.ERROR
    _loggername = "SequenceCalculation"

    def __init__(self, calctype, workdir, input_variables,
                 calculation_parameters, *args,
                 bypass_sequence_comparison=False,
                 bypass_convergence_check=False, **kwargs):
        """SequenceCalculation init method.

        Parameters
        ----------
        calctype: str
            The calculation calctype.
        workdir: str
            The calculation workdir.
        input_variables: dict
            The input variables dict.
        calculation_parameters: dict
            The calculation parameters dict.
        bypass_convergence_check: bool, optional
            If True, the sequence will continue if a calculation is
            finished but not converged. If False, an error is raised
            if a calculation in the sequence is not converged.
        bypass_sequence_calculation: bool, optional
            If True, the sequence comparison is skipped. In other words,
            if a calculation exists at the same workdir, it is assumed it
            is the same calculation without comparing input variables
            and dependencies.
        """
        super().__init__(*args, **kwargs)
        self.workdir = full_abspath(workdir)
        self._bypass_sequence_comparison = bypass_sequence_comparison
        self._bypass_convergence_check = bypass_convergence_check
        self.dependencies = Sequence(
                bypass_convergence_check=bypass_convergence_check,
                bypass_sequence_comparison=bypass_sequence_comparison,
                loglevel=self._loglevel)
        self.calctype = calctype
        self.input_variables = input_variables
        self.load_geometry_from = None
        self.recover_from = None
        self.recover_from_update_variables = None
        self.recover_from_pop_variables = None
        self.recover_from_other_kwargs = {}
        # introducing the following property to keep some variables after
        # loading geometry since this is done after applying input variables
        # sometimes we want some geometry variables to be kept (like acell
        # in thermal expansion sequencers).
        self.keep_variables_after_loading_geometry = None
        self.load_kpts_from = None
        # for abinit only
        self.link_den_from = None
        self.link_wfk_from = None
        self.load_ddk_from = None
        self.calculation_parameters = calculation_parameters
        self.jobname = os.path.basename(self.workdir)
        if "jobname" in self.calculation_parameters:
            self.jobname = self.calculation_parameters.pop("jobname")
        else:
            self._logger.debug(
                f"Taking default jobname as: '{self.jobname}' for '{workdir}'")
        self._launcher = None
        self._is_ready_to_launch = None
        self._calculation_directory = None

    def __eq__(self, other):
        if self.workdir != other.workdir:
            return False
        for dependency in self.dependencies:
            if dependency not in other.dependencies:
                return False
        for dependency in other.dependencies:
            if dependency not in self.dependencies:
                return False
        if self.calctype != other.calctype:
            return False
        if self.input_variables != other.input_variables:
            return False
        if self.load_geometry_from != other.load_geometry_from:
            return False
        if self.calculation_parameters != other.calculation_parameters:
            return False
        return True

    def __repr__(self):
        return f"< class SequenceCalculation: {self.workdir} >"

    @property
    def bypass_sequence_comparison(self):
        return self._bypass_sequence_comparison

    @bypass_sequence_comparison.setter
    def bypass_sequence_comparison(self, bypass):
        self._bypass_sequence_comparison = bypass
        self.dependencies.bypass_sequence_comparison = bypass

    @property
    def bypass_convergence_check(self):
        return self._bypass_convergence_check

    @bypass_convergence_check.setter
    def bypass_convergence_check(self, bypass):
        self._bypass_convergence_check = bypass
        self.dependencies.bypass_convergence_check = bypass

    @property
    def calculation_directory(self):
        if self._calculation_directory is not None:
            return self._calculation_directory
        loglevel = self._default_calcdir_loglevel
        if self._loglevel <= logging.DEBUG:
            loglevel = self._loglevel
        if CalculationDirectory.is_calculation_directory(
                self.workdir, loglevel=loglevel):
            with CalculationDirectory.from_calculation(
                    self.workdir, loglevel=self._loglevel) as calc:
                self._calculation_directory = calc
                if calc.calctype != self.calctype:
                    raise TypeError(
                            f"A different calculation already exists here:"
                            f" '{self}', '{self.calctype}'")
                return calc
        # else, just create normal calc dir
        self._calculation_directory = CalculationDirectory(
                CALCTYPES_TO_INPUT_FILE_CLS[self.calctype],
                loglevel=loglevel)
        self._calculation_directory.path = self.workdir
        # check if directory exists. if it does, raise an error cause
        # it's weird
        if self._calculation_directory.exists:
            raise IsADirectoryError(
                    f"{self.workdir} exists but it's not a calcdir.")
        return self.calculation_directory

    @property
    def launcher(self):
        if self._launcher is not None:
            return self._launcher
        self._launcher = self.get_launcher()
        return self.launcher

    def get_launcher(self, link_dependencies=True):
        launcher_cls = __ALL_LAUNCHERS__[self.calctype]
        launcher = launcher_cls(self.jobname, loglevel=logging.ERROR)
        launcher.workdir = self.workdir
        # start by recovering the calculation if needed
        if self.recover_from is not None:
            recover_from = self.recover_from
            if isinstance(self.recover_from, SequenceCalculation):
                recover_from = self.recover_from.workdir
            launcher.recover_from(
                    recover_from,
                    update_variables=self.recover_from_update_variables,
                    pop_variables=self.recover_from_pop_variables,
                    keep_pbs_file_parameters=False,
                    **self.recover_from_other_kwargs)
            # I think the line below is wrong and we should use the update
            # kwargs above instead (FG: 27/01/2021)
            # e.g.: when recovering to launch a new relaxation once the
            # previous one is done, we don't want to cancel the optimization!
            # After thinking about it for the night, I think it is good
            # but to prevent overwriting the updated variables, we should
            # just pop them from the input variables.
            invars = self.input_variables.copy()
            for var in self.recover_from_update_variables:
                invars.pop(var, None)
            launcher.input_variables.update(invars)
        else:
            launcher.input_variables = self.input_variables
        # set all the other optional attributes
        for attr, value in self.calculation_parameters.items():
            setattr(launcher, attr, value)
        # all dependencies should be finished and converged if relevant
        if self.load_geometry_from is not None:
            load_geometry_from = self.load_geometry_from
            if isinstance(load_geometry_from, SequenceCalculation):
                load_geometry_from = load_geometry_from.workdir
            launcher.load_geometry_from(load_geometry_from)
            # if we need to keep variables, keep them
            if self.keep_variables_after_loading_geometry is not None:
                launcher.input_variables.update(
                        self.keep_variables_after_loading_geometry)
        if self.load_kpts_from is not None:
            load_kpts_from = self.load_kpts_from
            if isinstance(load_kpts_from, SequenceCalculation):
                load_kpts_from = load_kpts_from.workdir
            launcher.load_kpts_from(load_kpts_from)
        if self.dependencies and link_dependencies:
            for link in self.dependencies:
                launcher.link_calculation(link.workdir)
        # Specific to abinit!
        if self.calctype.startswith("abinit") and link_dependencies:
            if self.link_den_from is not None:
                if isinstance(self.link_den_from, SequenceCalculation):
                    launcher.link_den_from(self.link_den_from.workdir)
                else:
                    launcher.link_den_from(self.link_den_from)
            if self.link_wfk_from is not None:
                if isinstance(self.link_wfk_from, SequenceCalculation):
                    launcher.link_wfk_from(self.link_wfk_from.workdir)
                else:
                    launcher.link_wfk_from(self.link_wfk_from)
            if self.load_ddk_from is not None:
                launcher.link_ddk_from(self.load_ddk_from)
        self._launcher = launcher
        return self.launcher

    @property
    def is_local_launch(self):
        # check if it was a local launch
        # read the rundirectory beforehand to be sure pbs file exists
        rundir = self.calculation_directory.run_directory
        if rundir.exists and not rundir.has_been_read:
            rundir.read()
        with self.calculation_directory.pbs_file as pbs:
            if pbs.queuing_system == "local":
                return True
        return False

    @property
    def is_ready_to_launch(self):
        if self._is_ready_to_launch is not None:
            return self._is_ready_to_launch
        # check if calculation is ready to be launched.
        # first of all check its dependencies: they should be done and
        # converged if necessary
        self._logger.debug("Checking if calculation is ready to launch.")
        for dependency in self.dependencies:
            # dependencies should be SequenceCalculation objects
            if not dependency.calculation_directory.exists:
                # dependency not even written
                self._logger.debug(
                        f"Dependency not written: {dependency}.")
                self._is_ready_to_launch = False
                return self.is_ready_to_launch
            # dependency written. check its status
            calc = dependency.calculation_directory
            # check status
            finished = calc.status["calculation_finished"]
            if finished is not True:
                self._logger.debug(
                        f"Dependency not finished: {dependency}")
                self._is_ready_to_launch = False
                return self.is_ready_to_launch
            if "calculation_converged" in calc.status:
                converged = calc.status["calculation_converged"]
                if converged is False:
                    self._logger.debug(
                            f"Dependency not converged: {dependency}")
                    self._is_ready_to_launch = False
                    return self.is_ready_to_launch
            # dependency exists and is converged (if relevant) => continue
        # all dependencies are good, check calculation in itself
        if not self.calculation_directory.exists:
            # calculation does not exists => it's ready to launch
            self._logger.debug(
                    f"Calculation not written: {self}")
            self._is_ready_to_launch = True
            return self.is_ready_to_launch
        # calculation already exist. compare OLD vs NEW calc to see if
        # something is different. if not, don't need to relaunch
        if self.bypass_sequence_comparison:
            # bypass checking
            self._is_ready_to_launch = False
            return self.is_ready_to_launch
        try:
            self._compare_old_vs_new_calculation()
        except CalculationNotFinishedError:
            # calculation is not finished => calculation not ready to launch
            self._logger.warning(
                    f"Calculation not finished: {self}")
            self._is_ready_to_launch = False
            return self.is_ready_to_launch
        except CalculationNotConvergedError as e:
            if self.bypass_convergence_check:
                # we don't care if calculation is not converged
                self._is_ready_to_launch = True
                return self.is_ready_to_launch
            # calculation has not converged but is finished => something wring
            # happened
            self._logger.error(
                    f"Something happened to OLD calculation: "
                    f"{self}")
            raise e
        except Exception:
            # calculations are different. raise an error to tell that
            self._logger.exception(
                f"There are differences with already existing calculation: "
                f"'{self}'.")
            raise IsADirectoryError(self.workdir)
        # no error has been detected, calculation is the same and it's
        # finished and converged (if relevant). Thus, no need to launch.
        # OR: there was nothing to compared (no input variables given)
        self._logger.debug("Calculation successfully completed: "
                           f"{self}")
        self._is_ready_to_launch = False
        return self.is_ready_to_launch

    def launch(self, *args, run=True, **kwargs):
        # launch a particular calculation (should be a SequenceCalculation obj)
        # at this stage, the calculation should be ready to launch
        self.launcher.write(*args, **kwargs)
        if run:
            self.launcher.run()

    def _compare_old_vs_new_calculation(self):
        # read the calculation that exists already
        self.calculation_directory.read()
        calc = self.calculation_directory
        # check the calculation has finished and, if relevant, converged
        if calc.status["calculation_finished"] is not True:
            raise CalculationNotFinishedError(self.workdir)
        if "calculation_converged" in calc.status:
            if calc.status["calculation_converged"] is False:
                if not self.bypass_convergence_check:
                    raise CalculationNotConvergedError(self.workdir)
        # check links are the same
        have_links = len(self.dependencies) > 0
        have_input_vars = self.input_variables is not None
        if have_links and have_input_vars:
            # don't compare links if no variables given.
            # check the same parents have been given.
            with calc.meta_data_file as meta:
                new_parents_raw = [x.workdir for x in self.dependencies]
                # if in the new parents, the parents are symlinks to other
                # calcs, we need to point directly to the good dependency
                # because in the meta file it is the TRUE parent (the one
                # the symlink points to) which is written
                new_parents = []
                for parent in new_parents_raw:
                    if os.path.islink(parent):
                        # if not an absolute path raise an error
                        # (cannot assume where it links to exactly)
                        source = os.readlink(parent)
                        if not os.path.isabs(source):
                            # try making it absolute
                            new_source = full_abspath(
                                    os.path.join(
                                        os.path.dirname(parent),
                                        source))
                            if not os.path.isdir(new_source):
                                msg = (
                                    "symlink to another calculation is not "
                                    f"absolute: '{parent}'. Make it absolute!")
                                raise FileNotFoundError(msg)
                            else:
                                source = new_source
                        new_parents.append(source)
                    else:
                        new_parents.append(parent)
                # now check that all dependencies are good
                for parent in meta.parents:  # loop over old parents
                    if os.path.islink(parent):
                        parent = os.readlink(parent)
                    # call normpath because in windows,
                    parent = os.path.normpath(parent)
                    if parent not in new_parents:
                        toprint = "\n".join(new_parents)
                        raise NotADirectoryError(
                            f"Calc: '{self}' has parent '{parent}' "
                            f"but it's not found in calcs to link: "
                            f"'{toprint}'.")
                for link in new_parents:
                    old_parents = meta.parents
                    old_parents = [
                            x if not os.path.islink(x) else os.readlink(x)
                            for x in old_parents]
                    if link not in old_parents:
                        raise NotADirectoryError(
                                f"Calc to link: '{link}' not found in "
                                f"parents of already existing calculation:"
                                f" '{self}'")
        # check input variables are the same
        if self.input_variables is not None:
            # compare input variables to make sure it's the same thing
            # to make sure it works out, we need to call the same launcher
            # and link all the calculations
            if self.calctype in (
                    "abinit", "abinit_optic", "qe_epw", "qe_matdyn",
                    "wannier90",
                    ):
                # for these calculations we need to link dependencies to
                # compare input variables (since the input variables depend
                # on what have been linked).
                link = True
            else:
                link = False
            launcher = self.get_launcher(link_dependencies=link)
            # check if variables are the same
            with calc.input_file as input_file:
                self._compare_input_variables(
                                input_file.input_variables,
                                launcher.input_variables,
                                is_calc_a_symlink=os.path.islink(self.workdir),
                                )
        # everything matches, return the calc directory
        return calc

    def _compare_input_variables(
            self, old_vars_dict, new_vars_dict, is_calc_a_symlink=False):
        """Compare input variables from an old calculation with the new one
        computed from the sequencer.

        Parameters
        ----------
        old_vars_dict: dict
            The dictionary of input variables for the old calculation.
        new_vars_dict: dict
            The dictionary of input variables for the new calculation.
        is_calc_a_symlink: bool, optional
            Defaults to False. If True, when we use a symlink to
            an old calculation, sometimes there are variables
            that do not influence the calculation
            but can differ from the the new ones. For these we skip the check.
        """
        if old_vars_dict == new_vars_dict:
            return
        # we want a detailed analysis
        errors = []
        if self.calctype in __EXCEPTION_VARIABLES__:
            exc = __EXCEPTION_VARIABLES__[self.calctype]
        else:
            exc = []
        for k, v in old_vars_dict.items():
            if k not in new_vars_dict:
                if k in exc:
                    continue
                errors.append(
                        f"'{k}'='{v}' is present in OLD calc but not "
                        "in NEW.")
                continue
            if v != new_vars_dict[k]:
                if k in exc:
                    # skip the check since this variable does
                    # not influence calc.
                    continue
                errors.append(
                        f"'{k}'='{v.value}' in OLD but\n"
                        f"'{k}'='"
                        f"{new_vars_dict[k].value}' in"
                        " NEW.")
        for k, v in new_vars_dict.items():
            if k not in old_vars_dict:
                if k in exc:
                    continue
                errors.append(
                        f"'{k}'='{v}' is present in NEW calc but not "
                        "in OLD.")
                continue
            if v != old_vars_dict[k]:
                if k in exc:
                    # skip the check since this variable does not
                    # influence calc.
                    continue
                errors.append(
                        f"'{k}'='{v.value}' in NEW but\n"
                        f"'{k}'='"
                        f"{old_vars_dict[k].value}'"
                        " in OLD.")
        if errors:
            for error in errors:
                self._logger.error(error)
            raise ValueError(
                    "There are differences between input variables"
                    " between already existing calc "
                    "vs input variables given.")


class Sequence(BaseUtility):
    """A utility class that stores sequence calculation. Basically acts as an
    ordered mutable set.
    """
    _loggername = "Sequence"

    def __init__(self, *args, bypass_sequence_comparison=False,
                 bypass_convergence_check=False, **kwargs):
        super().__init__(*args, **kwargs)
        self._bypass_sequence_comparison = bypass_sequence_comparison
        self._bypass_convergence_check = bypass_convergence_check
        self._container = []

    def __add__(self, obj):
        if not is_list_like(obj):
            self._logger.error(f"Tried to add: '{obj}'.")
            raise TypeError("Cannot add a non-list like obj.")
        for item in obj:
            self.append(item)
        return self

    def __bool__(self):
        return bool(self._container)

    def __contains__(self, obj):
        return obj in self._container

    def __iter__(self):
        for calc in self._container:
            yield calc

    def __len__(self):
        return len(self._container)

    def __getitem__(self, index):
        return self._container[index]

    def __setitem__(self, index, value):
        if not isinstance(value, SequenceCalculation):
            raise TypeError(
                    "Can only set 'SequenceCalculation' objects but got: "
                    f"{value}")
        value.bypass_sequence_comparison = self.bypass_sequence_comparison
        value.bypass_convergence_check = self.bypass_convergence_check
        self._container[index] = value

    def __repr__(self):
        return f"[{', '.join([repr(x) for x in self])}]"

    @property
    def bypass_convergence_check(self):
        return self._bypass_convergence_check

    @bypass_convergence_check.setter
    def bypass_convergence_check(self, bypass):
        self._bypass_convergence_check = bypass
        for calc in self:
            calc.bypass_convergence_check = bypass

    @property
    def bypass_sequence_comparison(self):
        return self._bypass_sequence_comparison

    @bypass_sequence_comparison.setter
    def bypass_sequence_comparison(self, bypass):
        self._bypass_sequence_comparison = bypass
        for calc in self:
            calc.bypass_sequence_comparison = bypass

    def append(self, obj):
        if not isinstance(obj, SequenceCalculation):
            raise TypeError(
                    "Can only append 'SequenceCalculation' object but got: "
                    f"{obj}")
        if obj in self:
            # already in self, nevermind this item
            return
        obj.bypass_sequence_comparison = self.bypass_sequence_comparison
        obj.bypass_convergence_check = self.bypass_convergence_check
        self._container.append(obj)

    def clear(self):
        del self._container
        self._container = []

    def index(self, calc):
        """Returns the index of a given calculation in the sequence.

        If the calc appears more than one time in the Sequence, the first
        index is returned.

        Parameters
        ----------
        calc: SequenceCalculation object
            The object to get the index from.

        Returns
        -------
        int: The index of the SequenceCalculation object in the sequence.
        """
        if not isinstance(calc, SequenceCalculation):
            raise TypeError(
                    "Expected a 'SequenceCalculation' object but got "
                    f"'{calc}'.")
        for icalc, incalc in enumerate(self):
            if calc is incalc:
                return icalc
        raise KeyError(
                f"'{calc}' was not found in sequence: '{self}'.")
