from .bases import SequenceCalculation
from .scf_sequencer import SCFSequencer
from ..exceptions import DevError
from ..routines import is_list_like


class NSCFSequencer(SCFSequencer):
    """A sequencer for a nscf calculation following a scf calculation.
    """
    _all_sequencer_prefixes = (
            "scf_", "nscf_", )
    _loggername = "NSCFSequencer"

    def __init__(self, *args, **kwargs):
        SCFSequencer.__init__(self, *args, **kwargs)
        if "nscf_" not in self._all_sequencer_prefixes:
            raise DevError(
                    "'nscf_' should be set in '_all_sequencer_prefixes'.")

    @property
    def nscf_input_variables(self):
        return self._nscf_input_variables

    @nscf_input_variables.setter
    def nscf_input_variables(self, input_vars):
        if not isinstance(input_vars, dict):
            raise TypeError(
                    f"Expected a dict for nscf input vars got instead: "
                    f"{input_vars}")
        if self.software == "qe":
            if input_vars.get("calculation", "scf") != "nscf":
                self._logger.debug(
                        "Overwritting 'calculation' var for 'nscf'.")
                input_vars["calculation"] = "nscf"
        elif self.software == "abinit":
            if input_vars.get("iscf", 7) >= 0:
                raise ValueError(
                        "For an NSCF run, 'iscf' must be negative.")
        self._nscf_input_variables = input_vars

    def init_nscf_sequence(self):
        scfcalc = self.get_sequence_calculation("scf")
        nscfcalc = SequenceCalculation(
                    scfcalc.calctype, self.nscf_workdir,
                    self.nscf_input_variables,
                    self.nscf_calculation_parameters,
                    loglevel=self._loglevel)
        # nscf depend on scf
        nscfcalc.dependencies.append(scfcalc)
        nscfcalc.load_geometry_from = scfcalc.workdir
        self.sequence.append(nscfcalc)

    def init_sequence(self, *args, **kwargs):
        # SCF calculation is set in mother class
        SCFSequencer.init_sequence(self, *args, **kwargs)
        self.init_nscf_sequence()


class PositiveKpointsNSCFSequencer(NSCFSequencer):
    """A sequencer for a nscf calculation following a scf calculation
    with kpoints disposed on a regular kpoint grid with only positive
    coordinates (useful for calculations with wannier90).
    """
    _loggername = "PositiveKpointsNSCFSequencer"

    def __init__(self, *args, **kwargs):
        NSCFSequencer.__init__(self, *args, **kwargs)
        # define kgrid parameters
        self._nscf_kgrid = None

    @NSCFSequencer.nscf_input_variables.setter
    def nscf_input_variables(self, input_vars):
        NSCFSequencer.nscf_input_variables.fset(self, input_vars)
        if self.software == "qe":
            self.nscf_input_variables["k_points"] = self._get_nscf_k_points()

    @property
    def nscf_kgrid(self):
        if self._nscf_kgrid is not None:
            return self._nscf_kgrid
        raise ValueError("Need to set 'nscf_kgrid'")

    @nscf_kgrid.setter
    def nscf_kgrid(self, nscf_kgrid):
        if not is_list_like(nscf_kgrid):
            nscf_kgrid = [nscf_kgrid] * 3
        if not all([isinstance(x, int) for x in nscf_kgrid]):
            raise TypeError("Need integers for nscf kgrid.")
        self._nscf_kgrid = nscf_kgrid

    def _get_nscf_k_points(self):
        kpts = []
        for i in range(self.nscf_kgrid[0]):
            for j in range(self.nscf_kgrid[1]):
                for k in range(self.nscf_kgrid[2]):
                    kpts.append([i / self.nscf_kgrid[0],
                                 j / self.nscf_kgrid[1],
                                 k / self.nscf_kgrid[2],
                                 ])
        N = self.nscf_kgrid[0] * self.nscf_kgrid[1] * self.nscf_kgrid[2]
        return {"parameter": "crystal",
                "k_points": kpts,
                "weights": [1 / N] * N}
