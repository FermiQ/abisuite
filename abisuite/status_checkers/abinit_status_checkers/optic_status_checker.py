from ..bases import BaseCalculationStatusChecker


class AbinitOpticCalculationStatusChecker(BaseCalculationStatusChecker):
    """Status checker for an optic calculation.
    """
    _calculation_completed_keyword_trigger = (
            "Calculation completed."
            )
    _error_keyword_trigger = "ERROR"
    _loggername = "AbinitOpticCalculationStatusChecker"
