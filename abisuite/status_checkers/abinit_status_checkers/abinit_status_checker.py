from ..bases import BaseSCFCalculationStatusChecker


class AbinitCalculationStatusChecker(BaseSCFCalculationStatusChecker):
    """Status checker for an Abinit calculation.
    """
    _calculation_completed_keyword_trigger = "Calculation completed."
    _error_keyword_trigger = "ERROR"
    _loggername = "AbinitCalculationStatusChecker"
    _scf_converged_keywords = "converged"
    _scf_non_converged_keywords = "not enough SCF cycles to converge"

    def _is_calculation_converged(self):
        # check the calculation type before deciding which are the
        # triggering keywords
        # check the input variables
        with self.calculation_directory.input_file as input_file:
            # based on the definitions of the variable in abinit,
            # we can deduce the default
            # by default it is always positive thus a SCF calc
            # put 1 for the sake of it
            iscf = input_file.input_variables.get("iscf", 1).value
            nstep = input_file.input_variables.get("nstep", 30).value
        if nstep == 0:
            return "non-scf"
        if iscf < 0:
            # NON SCF CALCULATION => cannot tell if convergence is reached
            self._logger.debug(f"'iscf'={iscf}<0 cannot tell if convergence"
                               " is reached.")
            return "non-scf"
        # check ionmov
        ionmov = input_file.input_variables.get("ionmov", 0)
        if ionmov > 0:
            # ionmov calculation, change the (non-)converged keywords
            converged = "gradients are converged"
            nonconverged = "not enough Broyd/MD steps to converge gradients"
            return super()._is_calculation_converged(
                    scf_converged_keywords=converged,
                    scf_non_converged_keywords=nonconverged
                    )
        # else, just a normal GS calculation
        return super()._is_calculation_converged()
