from ..bases import BaseCalculationStatusChecker


class AbinitMrgddbCalculationStatusChecker(BaseCalculationStatusChecker):
    """Status checker for a mrgddb calculation.
    """
    _calculation_completed_keyword_trigger = (
            "mrgddb : the run completed successfully"
            )
    _error_keyword_trigger = "ERROR"
    _loggername = "AbinitMrgddbCalculationStatusChecker"
