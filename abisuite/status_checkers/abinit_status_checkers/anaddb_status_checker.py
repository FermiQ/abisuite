from ..bases import BaseCalculationStatusChecker


class AbinitAnaddbCalculationStatusChecker(BaseCalculationStatusChecker):
    """Status checker for an anaddb calculation.
    """
    _calculation_completed_keyword_trigger = (
            "anaddb : the run completed succesfully."
            )
    _error_keyword_trigger = "ERROR"
    _loggername = "AbinitAnaddbCalculationStatusChecker"
