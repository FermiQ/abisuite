import abc
import os
import subprocess

from ..bases import BaseUtility
from ..exceptions import DevError
from ..routines import is_list_like


class BaseCalculationStatusChecker(BaseUtility, abc.ABC):
    """Base class for status analysis tool of a calculation.
    """
    _calculation_completed_keyword_trigger = None
    _error_keyword_trigger = None

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self._calc_dir = None
        if self._calculation_completed_keyword_trigger is None:
            raise DevError("Need to set '_calculation_keyword_trigger'")
        if self._error_keyword_trigger is None:
            raise DevError("Need to set '_error_keyword_trigger'")

    @property
    def calculation_directory(self):
        if self._calc_dir is not None:
            return self._calc_dir
        raise AttributeError("Need to set 'calculation_directory'")

    @calculation_directory.setter
    def calculation_directory(self, calcdir):
        from ..handlers import CalculationDirectory
        if isinstance(calcdir, CalculationDirectory):
            # just set it as it is
            self._calc_dir = calcdir
            return
        elif isinstance(calcdir, str):
            # initiate calculation directory from path
            self._calc_dir = CalculationDirectory.from_calculation(
                    calcdir, loglevel=self._loglevel)
            return
        # else raise type error
        raise TypeError(f"Was expecting either CalculationDirectory instance "
                        f" or path but got instead: {calcdir}")

    @property
    def calculation_status(self):
        """Dictionary containing information on the calculation status.

        Keys: 'calculation_started', 'calculation_finished'
        """
        # check if calcdir is connected to a database. If it's the case
        # check if status have been already computed
        if self.calculation_directory.is_connected_to_database:
            status = self.calculation_directory._database.get_status(
                    self.calculation_directory.path)
            if status is not None:
                # status has been computed before. Check if it needs to be
                # updated
                finished = status["calculation_finished"]
                if finished is True or finished == "error":
                    # don't need to update
                    return status
                # else recompute status
        status = self.get_initial_status_dict()
        # check if log file exists
        if self._is_calculation_started():
            # only update if log file exists. otherwise calculation is just
            # not started
            self._update_status_dict(status)
        else:
            self._logger.debug("Calculation not started.")
        # status has been computed, append to database if one exists
        if self.calculation_directory.is_connected_to_database:
            db = self.calculation_directory._database
            db.update_calculation_status(
                    self.calculation_directory.path,
                    status)
        # return the status dict
        return status

    @property
    def input_file(self):
        return self.calculation_directory.input_file

    @property
    def is_in_queue(self):
        try:
            with self.calculation_directory as calc:
                with calc.pbs_file as pbs:
                    if pbs.queuing_system == "local":
                        return False
                rundir = calc.run_directory
                try:
                    from abisuite import SYSTEM_QUEUE
                    with SYSTEM_QUEUE as queue:
                        if rundir.path in [x.workdir for x in queue.jobs]:
                            return True
                except subprocess.CalledProcessError as err:
                    # something happened while checking out the queue...
                    self._logger.error(
                            "Checking out queue errored somehow...")
                    self._logger.exception(err)
                    return False
        except ValueError as e:
            self._logger.exception(e)
        return False

    @property
    def log_file(self):
        return self.calculation_directory.log_file

    @property
    def stderr_file(self):
        with self.calculation_directory as calc:
            return calc.stderr_file

    def get_initial_status_dict(self):
        return {"calculation_started": False,
                "calculation_finished": False}

    def _is_calculation_started(self):
        logpresent = self.log_file.exists
        errpresent = self.stderr_file.exists
        if errpresent and not logpresent:
            # stderr file could be present even though log file is not
            # check if stuff has been written into err file
            # that would mean that calculation has started
            return True
        if not logpresent:
            self._logger.debug(
                    f"Log file not found: {self.log_file.path} => calculation"
                    " not started.")
            self._logger.debug(str(self.calculation_directory))
            self._logger.debug(os.listdir(self.calculation_directory.path))
        return logpresent

    # TODO: Put that in parser object??
    # NOTE: maybe not since some calculations can have more than 1 log file
    def _is_calculation_finished(self):
        # open file and read for a specific keyword
        finished = self._dig_log_file_for_status()
        # if job errors right before starting of script (like a bad pbs)
        # _dig_log_file will return False because it reached the end of file
        # check if a stderr file exists and if so, check if something is
        # written in it. if it's the case, return "error" instead.
        if finished is True or finished == "error":
            return finished
        # also a calculation might have been interrupted by queuing system
        # (e.g. if walltime is exceeded.)
        # in that case, it might appear as not finished with no errors
        # but no calculation is actually running. check if calculation
        # is still in queue. if not, an error occured
        if self.is_in_queue is False:
            return "error"
        with self.calculation_directory as calc:
            with calc.stderr_file:
                if calc.stderr_file.nlines > 0:
                    self._logger.debug(
                            "Stderr files contains lines => it errored.")
                    return "error"
        return finished

    def _dig_log_file_for_status(
            self, error_keys=None, completed_keys=None,
            line_limit=200, log_file=None):
        """Dig the log file for a specific keyword that tells the status
        of the calculation.

        Parameters
        ----------
        error_keys: str or list, optional
            The list of str or the str that, if encountered, says the calc has
            errored. If None, the class attributes will be taken.
        completed_keys: str or list, optional
            The list of str or the str that, if encountered, says the calc has
            completed. If None, the class attributes will be taken.
        line_limit: int, optional
            If not None, if no keywords have been detected within
            the line_limit
            number of lines from the end of the file, we assume the calculation
            has not finished yet.
        log_file: FileHandler object, optional
            If None, the log_file attribute is used instead of this. If
            specified, it is the log file to dig to get the status.

        Returns
        -------
        True: If any of the 'completed_keys' have been detected.
        False: If nothing have been detected within the 'line_limit'.
        "error": If any of the 'error_keys' have been detected.
        """
        # reversed search for a quicker answer
        if error_keys is None:
            error_keys = self._get_error_keys()
        if not is_list_like(error_keys):
            error_keys = (error_keys, )
        if completed_keys is None:
            completed_keys = self._get_completed_keys()
        if not is_list_like(completed_keys):
            completed_keys = (completed_keys, )
        if log_file is None:
            log_file = self.log_file
        # if log file does not exists and calc not in queue, return error
        if not log_file.exists:
            if not self.is_in_queue:
                return "error"
            # calculation started but still in queue
            return False
        with open(log_file.path, "r") as f:
            lines = f.readlines()
        if line_limit is None:
            line_limit = len(lines)
        for i, line in enumerate(reversed(lines)):
            # if we arive at more than, say, 200 lines, probably calculation
            # is not finished
            if i == line_limit:
                self._logger.debug(
                        f"Reached {line_limit} lines from bottom => "
                        "calculation not finished.")
                return False
            for error_key in error_keys:
                if error_key in line:
                    self._logger.debug(
                            "Found an 'error' keyword => calculation errored.")
                    return "error"
            for completed_key in completed_keys:
                if completed_key in line:
                    self._logger.debug(
                            "Found completed keyword => calculation finished.")
                    return True
        else:
            # if we arrive at end of file, return False
            self._logger.debug("Arrived at end of file => calc not finished.")
            return False

    # TODO: check if this function is still relevent
    def _get_completed_keys(self):
        completed_keys = self._calculation_completed_keyword_trigger
        if not is_list_like(completed_keys):
            completed_keys = (completed_keys, )
        return completed_keys

    # TODO: check if this function is still relevent
    def _get_error_keys(self):
        error_keys = self._error_keyword_trigger
        if not is_list_like(error_keys):
            error_keys = (error_keys, )
        return error_keys

    def _update_status_dict(self, status):
        # assuming log exists, calculation already started
        status["calculation_started"] = True
        # log file for ending status
        status["calculation_finished"] = self._is_calculation_finished()


class BaseSCFCalculationStatusChecker(BaseCalculationStatusChecker, abc.ABC):
    """Base class for calculation that have a SCF cycle of some sort.
    These kind of checkers checks if the convergence have been reached.
    """
    _scf_converged_keywords = None
    _scf_non_converged_keywords = None

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        if self._scf_converged_keywords is None:
            raise DevError("Must set '_scf_converged_keywords'")
        if self._scf_non_converged_keywords is None:
            raise DevError("Must set '_scf_non_converged_keywords'")

    # TODO: DRY this with the 'dig' method of the base class
    def _is_calculation_converged(self, scf_converged_keywords=None,
                                  scf_non_converged_keywords=None):
        if scf_converged_keywords is None:
            scf_converged_keywords = self._scf_converged_keywords
        if scf_non_converged_keywords is None:
            scf_non_converged_keywords = self._scf_non_converged_keywords
        with open(self.log_file.path) as f:
            lines = f.readlines()
        for line in lines[::-1]:
            if scf_converged_keywords in line:
                return True
            elif scf_non_converged_keywords in line:
                return False
        # if we are here, computation is not finished => return False
        # self._logger.warning("Could not find the convergence status in "
        #                      f"{self.log_file.path}")
        return False

    def get_initial_status_dict(self):
        dic = super().get_initial_status_dict()
        dic["calculation_converged"] = False
        return dic

    def _update_status_dict(self, status):
        super()._update_status_dict(status)
        finished = status["calculation_finished"]
        if finished is False or finished == "error":
            # if calculation is not finished, it is surely not converged
            # do not change anything else
            return
        # check if calculation is converged
        status["calculation_converged"] = self._is_calculation_converged()
