from ..bases import BaseUtility
from ..colors import Colors
from ..routines import full_abspath
from ..utils import TerminalTable
import os
import time
import traceback


TIME_BETWEEN_UPDATES = 300  # seconds


class StatusTree(BaseUtility):
    """Object that starts at the top of a directory tree and scrolls down
    in all its subdirectories in order to get the status of each
    calculations. The status is checked by using a StatusChecker object
    depending of the calculation type.

    When looking down in the tree, we assume that a calculation directory
    does not have subdirectories containing other calculations
    (this may be a future feature to look down everywhere).
    """
    _loggername = "StatusTree"

    def __init__(self, top_directory,
                 ignore=None,
                 time_between_updates=TIME_BETWEEN_UPDATES,
                 follow_symlinks=True, **kwargs):
        """StatusTree init method.

        Parameters
        ----------
        top_directory : str
                        The top directory path.
        ignore : list, optional
                 If not None, ignore these directories and its subdirectories.
        time_between_updates : float, optional
                               Gives the minimal time before updating
                               the status tree.
        follow_symlinks: bool, optional
            If True, symlinks are followed. If False, they are ignored.
        """
        super().__init__(**kwargs)
        self.top_directory = full_abspath(top_directory)
        self.ignore = ignore
        if ignore is None:
            self.ignore = []
        if isinstance(ignore, str):
            self.ignore = [ignore, ]
        self.build_tree(follow_symlinks=follow_symlinks)
        self._status = None
        self._last_update = time.time() - time_between_updates
        self.time_between_updates = time_between_updates

    def __add__(self, tree):
        # defines the addition between two trees. Just return the same tree
        # with the concatenated calculationtreee of the other Statustree
        self.tree += tree.tree
        return self

    def __iter__(self):
        for calcdir in self.tree.calculations:
            yield calcdir["calculation_dir"]

    def build_tree(self, **kwargs):
        """Builds the calculation tree. All kwargs are passed to the
        CalculationTree utility.
        """
        from ..handlers import CalculationTree
        self._logger.info(f"Building calculation tree from"
                          f" {self.top_directory}")
        self.tree = CalculationTree(loglevel=self._loglevel)
        self.tree.path = self.top_directory
        self.tree.build_tree(**kwargs)
        if not len(self.tree):
            self._logger.info(f"Found no calculations starting from: "
                              f"{self.top_directory}")
        self.tree.sort_tree()

    @property
    def status(self):
        if time.time() - self._last_update < self.time_between_updates:
            self._logger.debug("Don't need to reupdate tree: too soon.")
            return self._status
        self._logger.info("Computing status of calculation tree.")
        self._status = self.tree.status
        self._last_update = time.time()
        return self._status

    # FIXME: fix this method that probably doesn't work anymore or delete it...
    def print_attributes(self, *args, shortpath=True, delta=None, sortby=None,
                         delta_type="percent",
                         precision=2):
        """Print a table of the arguments from each calculation in the tree.
        Can print computation status as well as output variables.

        Parameters
        ----------
        args : str
               All args are either 'status' and/or input variable names.
               All args are shown in the printed table at the end.
               If the arg is 'status', prints the state of the calculation.
               If the arg is 'convergence_reached', prints the convergence
               status.
               Anything else will print the value of the output variable.
        shortpath : bool, optional
                    If False, the full path to the calculation is shown.
        delta : str, optional
                A column will be added
                to show the variations of the parameter. It should be
                an abinit output variable (like 'etotal').
        delta_type : str, optional {'percent', 'absolute', 'absolute_per_atom'}
                     Gives the 'units' of the deltas;
                     - percent: delta is given in percentage of difference
                     - absolute: delta is just the difference
                                 between the values.
                     - absolute_per_atom: same as absolute but divided by
                                          the number of atoms.
        sortby : str, optional
                 Sorts the calculations by the name of this variable.
                 E.g.: if you give 'ecut' in args and in sortby, calculations
                 will be sorted by increasing ecut.
        precision : int, optional
                    Number of decimals for delta columns.
        """
        if delta is not None and sortby is None:
            raise ValueError("Delta is not None but values need to be compared"
                             " to something. Use the sortby argument to tell"
                             " which value to compare to.")
        args = list(args)
        self._logger.info(f"Getting tree attributes for {args}.")
        # create first column which tells the calculation title
        table = TerminalTable(["calculations"])
        table.column_alignments[0] = "left"
        calcs = self._get_calculations()
        for calc in calcs:
            table.add_row([calc])

        # print calculation status if needed
        if "status" in args:
            args.remove("status")
            table.add_column("status",
                             self._get_calculation_status(),
                             alignment="center")
            # if at least one calc has convergence_reached prop in status
            # then print a column
            for calc in self:
                if "calculation_converged" in calc.status:
                    table.add_column("convergence", self._get_convergence(),
                                     alignment="center")
                    break
        # print each attribute column
        for arg in args:
            table.add_column(arg, self._get_attribute(arg), alignment="right")

        # sort table if needed
        if sortby is not None:
            table.sortby(sortby)
        # finish the first loop to make sure the sorted value is retrieved
        # and table has been sorted already.
        if delta is not None:
            if isinstance(delta, str):
                delta = (delta, )
            for d in delta:
                if not table.is_column_sortable(d):
                    # column cannot be sorted, do not compute deltas
                    self._logger.error(f"{d} is not sortable => cannot compute"
                                       f" its delta.")
                    continue
                data = table.get_column(d)
                index = table.get_column_index(d) + 1
                natoms = None
                if delta_type == "absolute_per_atom":
                    natoms = []
                    for c in self:
                        try:
                            natom = c.get_output_var("natom")
                        except LookupError:
                            # calculation not finished => return none
                            natom = None
                        natoms.append(natom)
                name, values = self._get_delta_attribute(d, data, precision,
                                                         delta_type,
                                                         natoms=natoms)
                table.add_column(name, values, index=index, alignment="right")
        table.print()

    # FIXME: see FIXME for method above
    def _get_delta_attribute(self, delta, data, precision, delta_type,
                             natoms=None):
        # compute delta column from data. Print result with 'precision'
        # decimals
        # WE ASSUME HERE THE TABLE HAS ALREADY BEEN SORTED
        # check that name is ok
        name = "delta_" + delta
        if delta_type == "percent":
            name += " (%)"
        elif delta_type == "absolute":
            name += " (abs)"
        elif delta_type == "absolute_per_atom":
            name += " (abs/atom)"
        else:
            ValueError(f"Invalid delta type: {delta_type}.")
        deltas = []  # delta column values
        # if column is sortable => at least one value is an int or a float
        # indices of values which we can extract a delta
        can_work_with_indices = [i for i, x in enumerate(data)
                                 if type(x) in (int, float)
                                 and natoms[i] is not None]
        last_can_work = 0
        for i, x in enumerate(data):
            if i in can_work_with_indices:
                # can compute a delta
                if last_can_work == 0:
                    # first value => no delta to compute
                    deltas.append("--")
                    last_can_work += 1
                    continue
                lastval = data[can_work_with_indices[last_can_work - 1]]
                if delta_type == "percent":
                    delta_val = (x - lastval) / abs(lastval) * 100
                elif (delta_type == "absolute" or
                      delta_type == "absolute_per_atom"):
                    delta_val = x - lastval
                    if delta_type == "absolute_per_atom":
                        delta_val /= natoms[i]
                delta_val = round(delta_val, precision)
                deltas.append(delta_val)
                last_can_work += 1
            else:
                # cannot work with this value
                deltas.append(Colors.color_text("NOT AVAILABLE", "bold"))
        self._logger.debug(f"deltas to add {delta}: {deltas}.")
        return name, deltas

    def print_status(self, shortpath=True):
        """Prints the status of each calculation in the calculation tree.

        Parameters
        ----------
        shortpath : bool, optional
                    If False, the full path to the calculation directories are
                    given instead of relative paths.
                    If True, the relative path to the current directory (where
                    the script is executed) is given instead.
        """
        self.print_attributes("status", "convergence_reached",
                              shortpath=shortpath)

    def _get_attribute(self, attribute):
        if attribute == "walltime":
            # this one is different
            return self._get_walltimes()
        values = []
        for i, calc in enumerate(self):
            status = self.status[i]["calculation_finished"]
            # if computation not finished; attribute is not available
            if status is False or status == "error":
                values.append(Colors.color_text("NOT AVAILABLE", "bold"))
                continue
            instruct = attribute in dir(calc.log_file.structure)
            inattributes = attribute in calc.log_file.structure.all_attributes

            if not inattributes and not instruct:
                # attribute not available
                values.append(
                        Colors.color_text("NOT AVAILABLE", "bold"))
                continue
            try:
                # if we need an attribute from the content of the log file, we
                # we need to parse it.
                if not calc.log_file.has_been_read:
                    calc.log_file.read()
                values.append(
                        self._post_process_attribute(
                                attribute, getattr(calc.log_file, attribute)))
            except Exception as e:
                # any exception, print ERROR in logger and but error in table
                values.append(
                        Colors.color_text("BUG", "red", "bold"))
                self._logger.error(
                    f"Can't get attribute '{attribute}' from {calc.path}")
                self._logger.error(e)
                # print traceback
                self._logger.error(f"{traceback.print_tb(e.__traceback__)}")
        self._logger.debug(f"Values for {attribute} found are: {values}.")
        return values

    def _get_calculations(self, shortpath=True):
        paths = []
        for calc in self.tree.calculations:
            path = calc["path"]
            if shortpath:
                path = os.path.relpath(path)
            paths.append(path)
        if not paths:
            return paths
        # resweep for symlinks
        # we resweep just to have symlinks pointers at the same horizontal
        # position for prettiness
        maxlen = max([len(x) for x in paths])
        for i, calc in enumerate(self.tree.calculations):
            if calc["is_symlink"]:
                path = paths[i]
                nspaces = maxlen - len(path)
                # point to actual calcdir
                where_actual = os.path.relpath(calc["calculation_dir"].path)
                path += " " + "-" * nspaces + f"-> {where_actual}"
                paths[i] = path
        return paths

    def _get_calculation_status(self):
        # get calculation status for each calculation.
        statuses = []
        for stat in self.status:
            started = stat["calculation_started"]
            finished = stat["calculation_finished"]
            if not started:
                status = Colors.color_text("NOT STARTED", "cyan", "bold")
            elif started and (finished is False or finished == "error"):
                if finished is False:
                    status = Colors.color_text(
                            "NOT FINISHED", "yellow", "bold")
                elif finished == "error":
                    status = Colors.color_text("ERROR", "red", "bold")
            elif started and finished is True:
                status = Colors.color_text("COMPLETED", "green", "bold")
            statuses.append(status)
        return statuses

    def _get_convergence(self):
        # return the 'convergence_reached' text if available
        statuses = []
        for stat in self.status:
            started = stat["calculation_started"]
            finished = stat["calculation_finished"]
            # default convergence status is 'non-scf'
            conv = stat.get("calculation_converged", "non-scf")
            if finished == "error":
                status = Colors.color_text("ERROR", "red", "bold")
            if not started and finished is False:
                status = Colors.color_text("NOT AVAILABLE", "bold")
            if started and finished is False:
                status = Colors.color_text("NOT AVAILABLE", "bold")
            if finished is True:
                if conv is False:
                    status = Colors.color_text("NOT REACHED", "red", "bold")
                elif conv is True:
                    status = Colors.color_text("REACHED", "green", "bold")
                else:
                    # anything else, print blue, should be a string
                    status = Colors.color_text(conv.upper(), "blue", "bold")
            statuses.append(status)
        return statuses

    def _get_walltimes(self):
        # returns the list of strings corresponding to walltimes
        maxwalltimedisplay = "s"
        # compare all walltimes and returns strings of same lengths
        walltimes = []
        for calc in self:
            if calc.status["calculation_finished"] is not True:
                # append none
                walltimes.append(None)
                continue
            with calc.log_file as log:
                all_attr = log.structure.all_attributes
                if "walltime" in all_attr or "walltime" in dir(log.structure):
                    walltimes.append(log.walltime)
                else:
                    walltimes.append(None)
        for walltime in walltimes:
            if walltime is None:
                continue
            # walltimes are in seconds
            if walltime >= 60:
                # display at least minutes
                if maxwalltimedisplay == "s":
                    maxwalltimedisplay = "m"
            if walltime >= 3600:
                # display at least hours
                if maxwalltimedisplay in ("s", "m"):
                    maxwalltimedisplay = "h"
            if walltime >= 86400:
                # display at least days
                if maxwalltimedisplay != "d":
                    maxwalltimedisplay = "d"
        strings = []
        for walltime in walltimes:
            if walltime is None:
                strings.append(
                        Colors.color_text("NOT AVAILABLE", "bold"))
                continue
            strings.append(
                    self._post_process_walltime(
                        walltime, show_up_to=maxwalltimedisplay))
        return strings

    def _post_process_attribute(self, attribute, value):
        # this method post process an attribute before adding it to a column
        if attribute != "walltime" or value is None:
            return value
        else:
            return self._post_process_walltime(value)

    def _post_process_walltime(self, value, show_up_to="s"):
        # walltime is given in seconds, convert it to a useful string
        # show_up_to says uop to which units we show up.
        assert show_up_to in ("s", "m", "h", "d")
        ndays = int(value // (24 * 3600))
        value = value % (24 * 3600)
        nhours = int(value // 3600)
        value = value % 3600
        nmins = int(value // 60)
        nsecs = round(value % 60, 1)
        string = ""
        if ndays or show_up_to == "d":
            string += f"{ndays}d "
        if nhours or show_up_to in ("d", "h"):
            string += f"{nhours:02}h "
        if nmins or show_up_to in ("d", "h", "m"):
            string += f"{nmins:02}m "
        # always display seconds
        string += f"{nsecs:.1f}s".zfill(5)
        return string
