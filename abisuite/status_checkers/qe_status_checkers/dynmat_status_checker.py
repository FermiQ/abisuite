from .bases import BaseQECalculationStatusChecker


class QEDynmatCalculationStatusChecker(BaseQECalculationStatusChecker):
    """Status checker for a dynmat.x calculation from Quantum Espresso.
    """
    _loggername = "QEDynmatCalculationStatusChecker"
