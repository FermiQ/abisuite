from .bases import BaseQECalculationStatusChecker


class QEFSCalculationStatusChecker(BaseQECalculationStatusChecker):
    """Status checker for a fs.x calculation from Quantum Espresso.
    """
    _loggername = "QEFSCalculationStatusChecker"
