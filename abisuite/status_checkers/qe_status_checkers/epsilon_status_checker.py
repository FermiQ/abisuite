from .bases import BaseQECalculationStatusChecker


class QEEpsilonCalculationStatusChecker(BaseQECalculationStatusChecker):
    """Status checker for a epsilon.x calculation from Quantum Espresso.
    """
    _loggername = "QEEpsilonCalculationStatusChecker"
