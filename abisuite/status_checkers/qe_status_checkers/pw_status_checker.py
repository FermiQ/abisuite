from .bases import BaseSCFQECalculationStatusChecker


class QEPWCalculationStatusChecker(BaseSCFQECalculationStatusChecker):
    """Status checker for a pw.x calculation from Quantum Espresso.
    """
    _loggername = "QEPWCalculationStatusChecker"
    _scf_converged_keywords = "convergence has been achieved"
    _scf_non_converged_keywords = "convergence NOT achieved after"

    def _is_calculation_converged(self, *args, **kwargs):
        with self.input_file as inf:
            calculation = inf.input_variables.get("calculation", "scf")
        if calculation in ("nscf", "bands"):
            return "non-scf"
        elif calculation.endswith("relax"):
            return self._is_vc_relax_calculation_converged()
        elif calculation == "scf":
            return super()._is_calculation_converged()
        raise NotImplementedError(calculation)

    def _is_vc_relax_calculation_converged(self):
        return self._dig_log_file_for_status(
                completed_keys="bfgs converged in", line_limit=None)
