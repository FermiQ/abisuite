from .bases import BaseQECalculationStatusChecker


class QEMatdynCalculationStatusChecker(BaseQECalculationStatusChecker):
    """Status checker for a Quantum Espresso matdyn.x calculation.
    """
    _loggername = "QEMatdynCalculationStatusChecker"
