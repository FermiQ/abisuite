from .bases import BaseSCFQECalculationStatusChecker
import os


class QEPHCalculationStatusChecker(BaseSCFQECalculationStatusChecker):
    """Status checker for a ph.x calculation from Quantum Espresso.
    """
    _loggername = "QEPHCalculationStatusChecker"
    _scf_converged_keywords = "convergence has been achieved"
    _scf_non_converged_keywords = "no convergence has been achieved"

    def _dig_log_file_for_convergence(self, log_file):
        # need to check every representation.
        # in other words, count total number of times a representation
        # is computed vs total number of times 'convergence achieved'
        # appears
        with open(log_file.path, "r") as f:
            lines = f.readlines()
        n_repr = 0
        n_conv = 0
        for line in lines:
            if "Representation #" in line:
                n_repr += 1
                continue
            # put that first since both keywords are exactly the same (almost)
            if self._scf_non_converged_keywords in line.lower():
                return False
            if self._scf_converged_keywords in line.lower():
                n_conv += 1
                continue
        self._logger.debug(f"Found {n_repr} Repr calculations with {n_conv} "
                           "calculation converged keywords")
        return n_repr == n_conv

    def _get_all_log_files(self):
        log_files = [self.calculation_directory.log_file]
        # all other log files are listed in the 'run' directory
        # and the all start with 'out.1_0' or something like this
        # append them if they exist
        logcls = log_files[0].__class__
        for subfile in os.listdir(
                self.calculation_directory.run_directory.path):
            if not subfile.startswith("out."):
                continue
            log = logcls.from_file(
                    os.path.join(
                        self.calculation_directory.run_directory.path,
                        subfile))
            log_files.append(log)
        return log_files

    # special case since calculation can be split into many images
    # that need to be recovered afterwards if it's the case.
    # when it's split, there is > 1 log file and one needs to check em all
    # to see if calculation is actually finished since the split could be
    # not equal between images (depends on parallelization scheme also).
    def _is_calculation_finished(self):
        # do the same as base class but compute ending for all log files.
        log_files = self._get_all_log_files()
        statuses = [self._dig_log_file_for_status(log_file=log)
                    for log in log_files]
        if "error" in statuses:
            self._logger.debug("One of the logs has errors.")
            return "error"
        if not all([status is True for status in statuses]):
            # some of the logs don't seem finished
            # check if calc is in queue. if not and it appears to be not
            # finished, then an error occured
            if not self.is_in_queue:
                self._logger.debug(
                    f"'{self.calculation_directory.path}' not in queue.")
                return "error"
            # calc truly not finished
            self._logger.debug(
                    f"'{self.calculation_directory.path}' is still in queue.")
            return False
        else:
            # all log files are finished
            return True

    def _is_calculation_converged(self):
        return all([x is True for x in [self._dig_log_file_for_convergence(x)
                    for x in self._get_all_log_files()]])
