from ..bases import (
        BaseSCFCalculationStatusChecker, BaseCalculationStatusChecker,
        )


class Wannier90StatusChecker(BaseSCFCalculationStatusChecker):
    """Status checker class for a wannier90.x calculation.
    """
    _calculation_completed_keyword_trigger = ("All done", )
    _error_keyword_trigger = ("Error", "ERROR")
    _loggername = "Wannier90StatusChecker"
    _scf_non_converged_keywords = "Warning: Maximum number of disentanglement iterations reached"  # noqa
    _scf_converged_keywords = "Disentanglement convergence criteria satisfied"

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.is_pp = False

    @property
    def calculation_directory(self):
        return super().calculation_directory

    @calculation_directory.setter
    def calculation_directory(self, calculation_directory):
        BaseSCFCalculationStatusChecker.calculation_directory.fset(
                self, calculation_directory)
        # make sure run directory has been read
        rundir = self.calculation_directory.run_directory
        if rundir.exists and not rundir.has_been_read:
            rundir.read()
        with self.calculation_directory.pbs_file as pbs:
            self.is_pp = "-pp" in pbs.command_arguments
            if self.is_pp:
                self._logger.debug("Wannier90 calculation is a '-pp' one.")

    @property
    def _werr_exists(self):
        for item in self.calculation_directory:
            if item.path.endswith(".werr"):
                return True

    def _is_calculation_started(self):
        if self._werr_exists:
            # even if there is an error which is indicated
            # by the presence of this file
            return True
        return super()._is_calculation_started()

    def _is_calculation_finished(self):
        if self._werr_exists:
            return "error"
        if self.is_pp:
            # a '-pp' wannier90 calculation will write an initial guess of the
            # overlap matrix and then exit. It won't print the usual 'All done'
            with open(self.log_file.path, "r") as f:
                lines = f.readlines()
            final_line = lines[-1]
            if "Exiting..." in final_line:
                self._logger.debug(
                        "Wannier90 -pp calculation found the 'Exiting' flag"
                        " => calculation finished.")
                return True
        return super()._is_calculation_finished()

    def _get_completed_keys(self):
        completed_keys = super()._get_completed_keys()
        if self.is_pp:
            completed_keys = list(completed_keys)
            completed_keys.append("Exiting...")
        return tuple(completed_keys)

    def get_initial_status_dict(self):
        if self.is_pp:
            return BaseCalculationStatusChecker.get_initial_status_dict(self)
        return super().get_initial_status_dict()

    def _update_status_dict(self, *args, **kwargs):
        if self.is_pp:
            return BaseCalculationStatusChecker._update_status_dict(
                    self, *args, **kwargs)
        return super()._update_status_dict(*args, **kwargs)
