# TOP LEVEL ROUTINES
from math import sqrt  # noqa
import os
import warnings

import numpy as np


def suppress_warnings(func):
    """Decorator to suppress warnings that would be created during the
    function's call.
    """
    def suppressed_func(*args, **kwargs):
        with warnings.catch_warnings():
            warnings.simplefilter("ignore")
            return func(*args, **kwargs)
    return suppressed_func


def check_abspath(path, join_before=None):
    """Check if a path is absolute. If not, make it absolute
    and if needed, join a path to it.

    Parameters
    ----------
    path : str
           The path to check if it is absolute.
    join_before : str, optional
                  If path is not absolute, join this path before
                  making it absolute.
    """
    # check if path is absolute, if not, join workdir and return absolute
    if not os.path.isabs(path):
        if join_before is not None:
            path = os.path.join(join_before, path)
        return full_abspath(path)
    return path


def decompose_line(line, float_dtype=float, split=" "):
    """Decompose a line of text / string into its fundamental constituants
    based on a given separator.

    Parameters
    ----------
    line: str
        The line to decompose.
    float_dtype: callable, optional
        The float type in which floats will be converted.
        Default is the builtin float class from python.
    split: str, optional
        Separator on which we split the line. By default it's an empty space.

    Returns
    -------
    A tuple of 3 lists:
        - First list is the strings (or non-floats and non-integers).
        - Second is the list of integers in the line.
        - Third is the list of floats in the line casted using the
          'float_dtype' argument.
    """
    # from here we have the lines from either format
    # varname         value
    # value
    splitted = line.split(split)
    strings = []
    ints = []
    floats = []
    for element in splitted:
        try:
            if element == "":
                # drop the empty strings
                continue
            # check if element is an integer
            try:
                i = int(element)
            except ValueError:
                # not an integer
                pass
            else:
                ints.append(i)
                continue
            # check if element is a float
            try:
                f = float_dtype(element)
            except ValueError:
                # not a float
                pass
            else:
                floats.append(f)
                continue
            # check if it is a fraction or radical exposed as a string
            try:
                if element in ("None", ):
                    strings.append("None")  # return as a string
                    continue
                f = eval(element)
            except (NameError, SyntaxError):
                # not a fraction-like string
                pass
            else:
                # append only if it is a float
                if callable(f):
                    # return it to a string. This means the word was eval
                    # as a valid python function like "max" or "min" or etc.
                    strings.append(f.__name__)
                else:
                    floats.append(f)
                continue
            if "E-" in element and "." in element:
                # maybe there is a bug with abinit and two floats are
                # glued together. try to separate them
                f1, f2 = _try_debug_2floats(element, float_dtype)
                if f1 is not None:
                    # it worked !
                    floats.append(f1)
                    floats.append(f2)
                    continue
            # from here, element is neither a float nor an int => string
            strings.append(element)
        except SyntaxError as e:
            # if we're here, something unexpected happened in the file.
            # return None and alert user
            warnings.warn("Can't parse line for some reason: %s" % line)
            raise e
    return strings, ints, floats


def full_abspath(path):
    """abspath + expanduser
    """
    return os.path.abspath(os.path.expanduser(path))


def full_path_split(path):
    """Returns a tuple of all the part of a path.

    E.g.: /path/to/a/file will return ('path', 'to', 'a', 'file').
    """
    toreturn = []
    split = os.path.split(path)
    while split[0]:
        toreturn.append(split[-1])
        split = os.path.split(split[0])
    toreturn.append(split[-1])
    return tuple(reversed(toreturn))


def is_list_like(obj):
    if isinstance(obj, list) or (isinstance(obj, tuple) or
                                 isinstance(obj, np.ndarray) or
                                 isinstance(obj, set)):
        return True
    return False


def is_scalar(obj):
    """Tells if an object is a scalar (int or float).

    Parameters
    ----------
    obj : The object to classify.

    Returns
    -------
    True : If the object is an int or a float.
    False : if not.
    """
    if isinstance(obj, int):
        return True
    if isinstance(obj, float):
        return True
    if isinstance(obj, np.number):
        return True
    return False


def is_scalar_or_str(obj):
    """Tells if an object is a scalar (int or float) or a str.

    Parameters
    ----------
    obj : The object to classify.

    Returns
    -------
    True : If the object is an int, a float or a str
    False : if not.
    """
    if isinstance(obj, bool):
        # bool are valued as being an instance of int
        return False
    if isinstance(obj, str):
        return True
    return is_scalar(obj)


def is_vector(obj, length=None):
    """Returns True if 'obj' is a vector (i.e.: a 1D list-like obj).

    Parameters
    ----------
    obj: The object to test out.
    length: int, optional
       If not None, adds a check on the length of the vector. If
       vector does not have this length, False is returned.

    Returns
    -------
    bool: True if obj respects all the abovementioned conditions.
    """
    if not is_list_like(obj):
        return False
    shape = np.shape(obj)
    if len(shape) != 1:
        return False
    if length is not None:
        if not isinstance(length, int):
            raise TypeError("Length must be an integer!")
        if len(obj) != length:
            return False
    return True


def is_2d_arr(obj):
    """Tells if an object is a 2d array or not.

    Parameters
    ----------
    obj : The object to test.

    Returns
    -------
    True : If the object is 2-dimensionnal array.
    False : If not.
    """
    if not is_list_like(obj):
        raise TypeError("To be a 2D arr, obj must be at least list-like"
                        f" but received: {obj}")
    if len(np.shape(obj)) == 2:
        return True
    return False


is_2d_array = is_2d_arr  # alias


def return_list(arg):
    """Given the argument, returns a list.

    If it is a single string, returns a list of the string.
    If it is None, returns an empty list.
    """
    if arg is None:
        return []
    if isinstance(arg, str):
        return [arg]
    return arg


def sort_data(tosort, *args):
    """Sort all args according to the first array.

    Parameters
    ----------
    tosort : array
             The array to sort. All other arrays will be sorted the
             same as this one.
    """
    # check all arrays has the same len
    length = len(tosort)
    for array in args:
        if len(array) != length:
            raise ValueError("Arrays should have the same length.")
    sorted_indices = np.argsort(tosort)
    sorted_arrays = []
    for array in [tosort] + list(args):
        # manual sort to avoid numpy arrays
        newarr = []
        for index in sorted_indices:
            newarr.append(array[index])
        sorted_arrays.append(newarr)
    # sorted_args = [x for _, *x in sorted(zip(tosort, *args))]
    # sorted_args = [arr for arr in sorted_args.T]
    # tosort = sorted(tosort)
    # return [tosort] + sorted_args
    return sorted_arrays


def _try_debug_2floats(string, float_dtype):
    # try to separate two floats glued in a string e.g. 1.52E-0210.000
    E = string.index("E")
    # there is always two numbers in the exponent
    split = E + 4
    f1 = string[:split]
    f2 = string[split:]
    try:
        f1 = float_dtype(f1)
        f2 = float_dtype(f2)
    except Exception:
        warnings.warn("%s could not be decomposed into 2 floats." % string)
        return None, None
    else:
        # it worked
        return f1, f2
