from .bases import PathConvertibleBaseStructure


class MetaDataStructure(PathConvertibleBaseStructure):
    """Container for properties needed for a Meta Data File.
    """
    all_attributes = ("calctype", "calc_workdir", "children", "copied_files",
                      "input_data_dir", "input_file_path", "jobname",
                      "last_modified", "linked_files", "output_data_dir",
                      "parents", "pbs_file_path", "rundir",
                      # in case program (like Abinit) requires a
                      # different input file on execution that points to the
                      # file containing the input
                      # variables.  (executable input file)
                      "script_input_file_path",
                      "submit_time")
    attributes_not_relevant_for_equal_check = ("last_modified", )
    convertible_path_attributes = ("input_data_dir", "input_file_path",
                                   "output_data_dir", "pbs_file_path",
                                   "rundir", "script_input_file_path")
    relative_to_path_attribute = "calc_workdir"
    list_attributes = ("children", "copied_files", "linked_files", "parents")
