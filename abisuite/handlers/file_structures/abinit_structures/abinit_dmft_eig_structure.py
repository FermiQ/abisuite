from ..bases import BaseStructure


class AbinitDMFTEigStructure(BaseStructure):
    """Structure for a '.eig' file created by the dmft part of Abinit.
    """
    all_attributes = ("dmftbandf", "dmftbandi", "eigenvalues", "nband",
                      "nbandtot", "nkpt", "nspins")
