from ..bases import BaseSCFLogStructure


class AbinitLogStructure(BaseSCFLogStructure):
    """Structure class for an abinit log file. Looks very
    similar to the abinit output structure.
    """

    all_attributes = (
            "dtsets", "input_variables", "output_variables",
            "walltime", "occ",
            )
    dict_attributes = ("input_variables", "output_variables", )
    list_attributes = ("dtsets", )
    optional_attributes = ("occ", )

    @property
    def etot(self):
        return self.output_variables["etotal"]

    @etot.setter
    def etot(self, etot):
        self._etot = etot
