from ..bases import BaseInputStructure
from ....variables import ALL_ABINITOPTIC_VARIABLES


class AbinitOpticInputStructure(BaseInputStructure):
    """Structure for an optic input file.
    """
    _variables_db = ALL_ABINITOPTIC_VARIABLES
