from ..bases import BaseStructure


class AbinitGSRStructure(BaseStructure):
    """Structure for a '_GSR' file created by Abinit.
    """
    all_attributes = ("occupations", )
