from ..bases import PathConvertibleBaseStructure


class BaseAbinitFilesStructure(PathConvertibleBaseStructure):
    """base class for files file structures for ABINIT software.
    """
    relative_to_path_attribute = "path"
