from ..bases import BaseStructure


class AbinitProcarStructure(BaseStructure):
    """Structure of a PROCAR_XXXX file created by abinit.
    """
    all_attributes = ("characters", "eigenvalues", "nbands", "nkpts")
