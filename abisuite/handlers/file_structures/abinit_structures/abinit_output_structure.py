from ..bases import BaseSCFLogStructure


class AbinitOutputStructure(BaseSCFLogStructure):
    """Structure class for an output file created by Abinit.
    """
    all_attributes = (
            "dtsets", "input_variables", "output_variables",
            "walltime",
            )
    dict_attributes = ("input_variables", "output_variables", )
    list_attributes = ("dtsets", )

    @property
    def etot(self):
        return self.output_variables["etotal"]

    @etot.setter
    def etot(self, etot):
        self._etot = etot
