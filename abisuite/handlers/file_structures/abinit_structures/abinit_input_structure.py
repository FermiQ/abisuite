from ..bases import BaseInputStructure
from ....variables import ALL_ABINIT_VARIABLES


class AbinitInputStructure(BaseInputStructure):
    """Structure for an abinit input file.
    """
    _variables_db = ALL_ABINIT_VARIABLES
