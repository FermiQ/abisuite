from ..bases import BaseLogStructure


class AbinitAnaddbLogStructure(BaseLogStructure):
    """Structure class for an anaddb log file.
    """
    all_attributes = ("non_analytical_gamma_corrections", "timing", )
    optional_attributes = (
            "non_analytical_gamma_corrections",
            )
