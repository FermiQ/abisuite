from ..bases import BaseStructure


class AbinitOpticLogStructure(BaseStructure):
    """Structure class for an optic log file.
    """
    all_attributes = tuple([])
