from ..bases import BaseStructure


class AbinitMrgddbLogStructure(BaseStructure):
    """Structure class for a mrgddb log file.
    """
    all_attributes = ("walltime", )
