from .bases import BaseAbinitFilesStructure


class AbinitAnaddbFilesStructure(BaseAbinitFilesStructure):
    """Structure for an anaddb input file.
    """
    all_attributes = (
            "input_file_path", "output_file_path", "ddb_file_path",
            "band_structure_file_path", "gkk_file_path", "eph_data_prefix",
            "ddk_file_path",
            )
    convertible_path_attributes = (
            "input_file_path", "output_file_path", "ddb_file_path",
            "band_structure_file_path", "gkk_file_path", "eph_data_prefix",
            "ddk_file_path",
            )
