from ..bases import BaseStructure


class AbinitOpticLincompStructure(BaseStructure):
    """Structure class for a linear component of the dielectric tensor file
    produced by the optic script.
    """
    all_attributes = ("absolute", "absorption", "broadening",
                      "component", "epsilon", "frequencies",
                      "refractive_index", "reflectivity", "scissor_shift",
                      "window")
