from ..bases import BaseStructure


class AbinitCut3DInputStructure(BaseStructure):
    """Structure class for the (custom) input file for the 'cut3d' utility
    of abinit.
    """
    all_attributes = (
            "data_file_path", "option", "output_file_path", "post_option")
    list_attributes = ("post_option", )
