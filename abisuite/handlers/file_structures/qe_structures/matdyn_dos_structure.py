from ..bases import BaseStructure


class QEMatdynDOSStructure(BaseStructure):
    """Structure class for a dos file produced by the matdyn.x script from the
    Quantum Espresso package.
    """
    all_attributes = (
            "energies", "dos",
            )
