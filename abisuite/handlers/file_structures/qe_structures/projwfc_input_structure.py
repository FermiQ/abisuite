from ..bases import BaseInputStructure
from ....variables.qe_variables import ALL_QEPROJWFC_VARIABLES


class QEProjwfcInputStructure(BaseInputStructure):
    """Structure class for a Quantum Espresso projwfc.x input file.
    """
    _variables_db = ALL_QEPROJWFC_VARIABLES
