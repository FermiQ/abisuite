from ..bases import BaseInputStructure
from ....variables.qe_variables import ALL_QEPH_VARIABLES


class QEPHInputStructure(BaseInputStructure):
    """Structure class for a QuantumEspresso ph.x input file.
    """
    _variables_db = ALL_QEPH_VARIABLES
