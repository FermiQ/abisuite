from .bases import BaseQELogStructure


class QEPW2Wannier90LogStructure(BaseQELogStructure):
    """Structure for a pw2wannier90.x log file from Quantum Espresso.
    """
    all_attributes = ("timing", )
