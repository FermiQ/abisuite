from .bases import BaseQEBandFreqStructure


class QEEPWBandEigStructure(BaseQEBandFreqStructure):
    """Structure for a 'band.eig' file created by the epw.x script of
    Quantum Espresso.
    """
