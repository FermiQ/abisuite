from .bases import BaseQELogStructure


class QEEpsilonLogStructure(BaseQELogStructure):
    """Structure for a epsilon.x log file from Quantum Espresso.
    """
    all_attributes = tuple([])
