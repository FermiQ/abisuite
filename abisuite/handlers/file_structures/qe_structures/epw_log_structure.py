from .bases import BaseQELogStructure


class QEEPWLogStructure(BaseQELogStructure):
    """Structure for a epw.x log file from Quantum Espresso.
    """
    all_attributes = ("conductivities", "serta_conductivities", "timing", )
    optional_attributes = (
            "conductivities", "serta_conductivities", "timing",
            )
