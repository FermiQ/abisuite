from ..bases import BaseInputStructure
from ....variables.qe_variables import ALL_QEQ2R_VARIABLES


class QEQ2RInputStructure(BaseInputStructure):
    """Structure class for a Quantum Espresso q2r.x input file.
    """
    _variables_db = ALL_QEQ2R_VARIABLES
