from ..bases import BaseInputStructure
from ....variables.qe_variables import ALL_QEMATDYN_VARIABLES


class QEMatdynInputStructure(BaseInputStructure):
    """Structure class for a QuantumEspresso matdyn.x input file.
    """
    _variables_db = ALL_QEMATDYN_VARIABLES
