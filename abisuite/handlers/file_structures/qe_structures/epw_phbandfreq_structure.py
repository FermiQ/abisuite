from .bases import BaseQEBandFreqStructure


class QEEPWPHBandFreqStructure(BaseQEBandFreqStructure):
    """Structure for a 'phband.freq' file created by the epw.x script of
    Quantum Espresso.
    """
