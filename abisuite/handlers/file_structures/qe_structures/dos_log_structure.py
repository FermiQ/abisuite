from .bases import BaseQELogStructure


class QEDOSLogStructure(BaseQELogStructure):
    """Structure for a dos.x log file from Quantum Espresso.
    """
    all_attributes = ("timing", )
