from ..bases import BaseInputStructure
from ....variables.qe_variables import ALL_QEEPW_VARIABLES


class QEEPWInputStructure(BaseInputStructure):
    """Structure class for a QuantumEspresso epw.x input file.
    """
    _variables_db = ALL_QEEPW_VARIABLES
