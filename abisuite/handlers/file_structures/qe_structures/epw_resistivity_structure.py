from ..bases import BaseStructure


class QEEPWResistivityStructure(BaseStructure):
    """Structure class for a resistivity file produced by the epw.x executable
    from Quantum Espresso.
    """
    all_attributes = ("temperatures", "resistivity", )
