from .bases import BaseQELogStructure


class QELD1LogStructure(BaseQELogStructure):
    """Structure for a ld1.x log file from Quantum Espresso.
    """
    all_attributes = tuple([])
