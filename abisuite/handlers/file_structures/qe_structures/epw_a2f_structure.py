from ..bases import BaseStructure


class QEEPWa2FStructure(BaseStructure):
    """Structure class for a a2F file produced by the epw.x executable from
    Quantum Espresso.
    """
    all_attributes = ("frequencies", "a2F")
