from ..bases import BaseStructure


class QEProjwfcPDOSStructure(BaseStructure):
    """Structure class for a pdos file produced by the Quantum Espresso
    projwfc.x script.
    """
    all_attributes = (
            "energies", "ldos", "s", "pz", "px", "py", "dz2", "dzx", "dzy",
            "dx2-y2", "dxy",
            )
    optional_attributes = (
            "s", "pz", "px", "py", "dz2", "dzx", "dzy", "dx2-y2", "dxy",
            )
