from ..bases import BaseInputStructure
from ....variables.qe_variables import ALL_QEPP_VARIABLES


class QEPPInputStructure(BaseInputStructure):
    """Structure class for a Quantum Espresso pp.x input file.
    """
    _variables_db = ALL_QEPP_VARIABLES
