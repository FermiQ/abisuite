from ..bases import BaseInputStructure
from ....variables.qe_variables import ALL_QEPW2WANNIER90_VARIABLES


class QEPW2Wannier90InputStructure(BaseInputStructure):
    """Structure class for a Quantum Espresso pw2wannier90.x input file.
    """
    _variables_db = ALL_QEPW2WANNIER90_VARIABLES
