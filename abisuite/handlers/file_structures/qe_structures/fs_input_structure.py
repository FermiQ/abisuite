from ..bases import BaseInputStructure
from ....variables.qe_variables import ALL_QEFS_VARIABLES


class QEFSInputStructure(BaseInputStructure):
    """Structure class for a QuantumEspresso fs.x input file.
    """
    _variables_db = ALL_QEFS_VARIABLES
