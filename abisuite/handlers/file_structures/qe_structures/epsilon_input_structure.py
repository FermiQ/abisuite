from ..bases import BaseInputStructure
from ....variables.qe_variables import ALL_QEEPSILON_VARIABLES


class QEEpsilonInputStructure(BaseInputStructure):
    """Structure class for a QuantumEspresso epsilon.x input file.
    """
    _variables_db = ALL_QEEPSILON_VARIABLES
