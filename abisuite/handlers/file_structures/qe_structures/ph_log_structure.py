from .bases import BaseQELogStructure


class QEPHLogStructure(BaseQELogStructure):
    """Structure for a ph.x log file.
    """
    all_attributes = ("phonon_frequencies", "timing", "nphonons", )
    # diagonalization might not be done in a run
    optional_attributes = ("phonon_frequencies", "nphonons", )
