import os

from ..bases import BaseInputStructure
from ....variables.qe_variables import ALL_QEPW_VARIABLES


class QEPWInputStructure(BaseInputStructure):
    """Structure for a Quantum Espresso pw.x input file.
    """
    _variables_db = ALL_QEPW_VARIABLES

    def _get_pseudos_from_input_variables(self):
        # combine all pseudos from the input variable
        if "atomic_species" not in self.input_variables:
            raise KeyError("Cannot access pseudos if 'atomic_species' is not"
                           " in the input variables")
        if "pseudo_dir" not in self.input_variables:
            raise KeyError("Cannot access pseudos if 'pseudo_dir' is not"
                           " in the input variables.")
        atomic_species = self.input_variables["atomic_species"].value
        return [os.path.join(self.input_variables["pseudo_dir"].value,
                             x["pseudo"])
                for x in atomic_species]
