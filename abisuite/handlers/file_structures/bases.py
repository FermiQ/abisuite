import abc
import numpy as np
import os

from ...exceptions import DevError
from ...routines import is_list_like
from ...variables import InputVariableDict


# don't rebase this with BaseUtility we don't need logging for that
# also it might interfere when setting attributes for loggers with the
# file handlers structure so be wary of rebasing this with BaseUtility
class BaseStructure(abc.ABC):
    """Base class for file structures.
    """
    all_attributes = None
    # attributes with restricted values
    allowed_attributes = {}
    # defaul values of some attributes
    default_values = {}
    # attributes declared as dictionaries
    dict_attributes = tuple([])
    # attributes declared as lists
    list_attributes = tuple([])
    # optional attributes that can stay none
    optional_attributes = tuple([])
    # attributes declared as sets
    set_attributes = tuple([])
    # atributes not relevant for equality check
    attributes_not_relevant_for_equal_check = tuple([])

    def __init__(self):
        if self.all_attributes is None:
            raise DevError("Need to set all_attributes")
        # check everything is consistent
        containers = (self.dict_attributes, self.list_attributes,
                      self.set_attributes, self.optional_attributes,
                      self.attributes_not_relevant_for_equal_check, )
        for container in containers:
            if not isinstance(container, tuple):
                raise DevError("All vars containers must be tuples.")
            for var in container:
                if var not in self.all_attributes:
                    raise DevError(f"{var} not in 'all_attributes'")
            for other_container in containers:
                if other_container == container:
                    continue
                for var in container:
                    if var in other_container:
                        raise DevError(f"Specific var {var} cannot be in two "
                                       f"different containers.")
        for attr, allowed in self.allowed_attributes.items():
            if attr not in self.all_attributes:
                raise DevError(f"'{attr}' not in all_attributes.")
            if not is_list_like(allowed):
                raise TypeError(f"Allowed attributes for {attr} not"
                                " list-like")
        for attr in self.default_values:
            if attr not in self.all_attributes:
                raise DevError(f"'{attr}' not in all_attributes.")
        # everything ok => set defaults
        self._set_defaults()

    def __eq__(self, struc):
        if not isinstance(struc, self.__class__):
            raise TypeError("Cannot compare two different classes: "
                            f"{self.__class__}, {struc.__class__}")
        for attr in self.get_relevant_properties():
            if attr in self.attributes_not_relevant_for_equal_check:
                continue
            if isinstance(getattr(self, attr), np.ndarray):
                # special case for numpy arrays
                if (getattr(self, attr) != getattr(struc, attr)).all():
                    return False
            else:
                if getattr(self, attr) != getattr(struc, attr):
                    return False
        return True

    def __getattr__(self, attr):
        if attr not in self.all_attributes:
            raise ValueError(f"'{self.__class__} has no attribute "
                             f"'{attr}'")
        try:
            val = getattr(self, "_" + attr)
        except AttributeError:
            raise AttributeError(f"'{self.__class__} has no attribute "
                                 f"'{attr}'")
        if val is not None or attr in self.optional_attributes:
            # optional attributes are allowed to be None
            return val
        raise ValueError(f"Need to set '{attr}'.")

    def __delattr__(self, attr):
        if attr.startswith("_") or attr in dir(self):
            super().__delattr__(attr)
            return
        if attr not in self.all_attributes:
            raise ValueError(f"'{self.__class__} has no attribute "
                             f"'{attr}'")
        try:
            super().__delattr__("_" + attr)
        except AttributeError:
            raise AttributeError(f"'{self.__class__} has no attribute "
                                 f"'{attr}'")
        self._set_default(attr)

    def __setattr__(self, attr, value):
        # do not override properties setter
        if attr.startswith("_") or attr in dir(self):
            super().__setattr__(attr, value)
            return
        if attr not in self.all_attributes:
            raise AttributeError(f"Cannot set attribute '{attr}' in "
                                 f"'{self.__class__}'.")
        if attr in self.list_attributes or attr in self.set_attributes:
            if not is_list_like(value):
                raise TypeError(f"Expected list like but got '{attr}'")
        if attr in self.list_attributes and not isinstance(value, list):
            # try to put it as a list
            value = list(value)
        if attr in self.set_attributes and not isinstance(value, set):
            # try to cast as set
            value = set(value)
        if attr in self.allowed_attributes:
            # restricted values for this attribute
            if value not in self.allowed_attributes[attr]:
                raise ValueError(f"'{value}' not in list of allowed attributes"
                                 f" for '{attr}':"
                                 f"'{self.allowed_attributes[attr]}'")
        setattr(self, "_" + attr, value)

    def __str__(self):
        string = f"=== {self.__class__} ===\n\n"
        for attr in self.get_relevant_properties():
            try:
                string += f"{attr} = {getattr(self, '_' +  attr)}\n"
            except ValueError:
                string += f"{attr} = ??? ERROR\n"
        return string

    def copy(self):
        # return a copy of self
        new = self.__class__()
        for prop in self.get_relevant_properties():
            attr = getattr(self, prop)
            try:
                attr = attr.copy()
            except AttributeError:
                # no copy method
                pass
            setattr(new, prop, attr)
        return new

    # just an alias
    def get_relevant_attributes(self):
        return self.get_relevant_properties()

    def get_relevant_properties(self):
        return self.all_attributes

    def _set_defaults(self):
        for attr in self.all_attributes:
            self._set_default(attr)

    def _set_default(self, attr):
        if attr in self.set_attributes:
            setattr(self, "_" + attr, set())
            return
        elif attr in self.list_attributes:
            setattr(self, "_" + attr, [])
            return
        elif attr in self.dict_attributes:
            setattr(self, "_" + attr, {})
            return
        elif attr in self.default_values:
            setattr(self, "_" + attr, self.default_values[attr])
            return
        else:
            setattr(self, "_" + attr, None)


class BaseSCFLogStructure(BaseStructure, abc.ABC):
    """Base structure for log/output files from SCF calculations.
    """
    @property
    @abc.abstractmethod
    def etot(self):
        pass

    @etot.setter
    @abc.abstractmethod
    def etot(self, etot):
        pass


class PathConvertibleBaseStructure(BaseStructure):
    """Base structure for files that have convertible paths from relative
    paths to absolute paths and vice-versa.
    """
    # attributes declared as convertible paths
    convertible_path_attributes = tuple([])
    # attribute defined as the 'source' attribute
    relative_to_path_attribute = None

    def __init__(self, *args, **kwargs):
        if self.relative_to_path_attribute is None:
            raise DevError("Need to set the relative_to_path_attribute")
        super().__init__(*args, **kwargs)
        for attr in self.convertible_path_attributes:
            if attr not in self.all_attributes:
                raise DevError(f"{attr} in convertible path attributes not in "
                               "'all_attributes'")
        rel = self.relative_to_path_attribute
        if rel is None:
            raise DevError("Need to set 'relative_to_path_attribute'")
        if rel != "path" and rel not in self.all_attributes:
            raise DevError(f"Invalid 'relative_to_path_attribute'={rel}")


class BaseInputStructure(PathConvertibleBaseStructure):
    """Base class for Input structures.
    """
    all_attributes = ("input_variables", "pseudos", )
    dict_attributes = ("input_variables", )
    convertible_path_attributes = ("input_variables", )
    optional_attributes = ("pseudos", )
    relative_to_path_attribute = "path"
    _variables_db = None

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        if "input_variables" not in self.all_attributes:
            raise DevError("'input_variables' should be in structure.")
        if self._variables_db is None:
            raise DevError("Need to set '_variables_db'.")
        self._input_variables = InputVariableDict(self._variables_db)

    @property
    def input_variables(self):
        if not len(self._input_variables):
            raise ValueError("input vars empty => set them.")
        return self._input_variables

    @input_variables.setter
    def input_variables(self, input_variables):
        if isinstance(input_variables, InputVariableDict):
            # just reset everything if same _variables_db
            iv = input_variables
            if iv._variables_db != self._input_variables._variables_db:
                raise ValueError("Incompatible variable dict.")
            self._input_variables = iv
            return
        else:
            if not isinstance(input_variables, dict):
                raise TypeError(f"Expected dict but got: {input_variables}")
        del self._input_variables
        self._input_variables = InputVariableDict(self._variables_db)
        self.add_input_variables(input_variables)

    @input_variables.deleter
    def input_variables(self):
        self.input_variables.clear()

    @property
    def pseudos(self):
        if self._pseudos is not None:
            return self._pseudos
        self._pseudos = self._get_pseudos_from_input_variables()
        return self._pseudos

    @pseudos.setter
    def pseudos(self, pseudos):
        if pseudos is not None and not is_list_like(pseudos):
            self._pseudos = (pseudos, )
            return
        # allow None values as not all input files need to know the pseudos
        self._pseudos = pseudos

    def add_input_variables(self, new_variables):
        """Adds many input variables to the input variable dictionary.
        """
        self._input_variables.update(new_variables)

    def add_input_variable(self, name, value):
        """Adds an input variable to the input variable dictionary.
        """
        self._input_variables.add_variable(name, value)

    def clear_input_variables(self):
        """Remove all input variables to restart from nothing.
        """
        del self.input_variables

    def _get_pseudos_from_input_variables(self):
        # by default return none
        return None


class BaseStructureWithMPISupport(BaseStructure):
    """Base class for structures with mpi attributes.
    """
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        if "command" not in self.all_attributes:
            raise DevError("'command' must be an attribute.")
        if "command_arguments" not in self.dict_attributes:
            raise DevError("'command_arguments' must be a dict attribute.")
        if "mpi_command" not in self.all_attributes:
            raise DevError("'mpi_command' must be an attribute.")
        if "mpi_command_arguments" not in self.dict_attributes:
            raise DevError("'mpi_command_arguments' must be a dict attribute.")
        if "nodes" not in self.all_attributes:
            raise DevError("'nodes' must be an attribute.")
        if "ntasks" not in self.all_attributes:
            raise DevError("'ntasks' must be an attribute.")
        if "ppn" not in self.all_attributes:
            raise DevError("'ppn' must be an attribute.")
        if "total_ncpus" not in self.all_attributes:
            raise DevError("'total_ncpus' must be an attribute.")
        if "queuing_system" not in self.all_attributes:
            raise DevError("'queuing_system' must be an attribute.")

    @property
    def command(self):
        # if there are arguments, add them to the command string here
        if self._command is None:
            raise ValueError("Need to set 'command'")
        return self._add_args_to_cmd(self._command, self.command_arguments)

    @command.setter
    def command(self, cmd):
        cmd, args = self._split_cmd_from_args(cmd)
        self._command = cmd
        self.command_arguments = args

    @property
    def command_arguments(self):
        return self._command_arguments

    @command_arguments.setter
    def command_arguments(self, args):
        self._command_arguments = self._convert_str_to_args(args)

    @property
    def mpi_command(self):
        if self._mpi_command is None:
            raise ValueError("Need to set 'mpi_command'.")
        if not self._mpi_command and self.mpi_command_arguments:
            raise ValueError(
                    "mpi command is not set while mpi command arguments is...")
        return self._add_args_to_cmd(self._mpi_command,
                                     self.mpi_command_arguments)

    @mpi_command.setter
    def mpi_command(self, mpi_cmd):
        mpi_cmd, args = self._split_cmd_from_args(mpi_cmd)
        self._mpi_command = mpi_cmd
        self.mpi_command_arguments = args

    @property
    def mpi_command_arguments(self):
        return self._mpi_command_arguments

    @mpi_command_arguments.setter
    def mpi_command_arguments(self, mpi_args):
        self._mpi_command_arguments = self._convert_str_to_args(mpi_args)

    # special case for nodes which can be a special string
    @property
    def nodes(self):
        if self._queuing_system is not None and self._nodes is None:
            if self.queuing_system == "local":
                self.nodes = 1
                return self.nodes
        # if self._nodes is not None:
        #     return self._nodes
        # raise ValueError("Need to set 'nodes'")
        return self._nodes

    @nodes.setter
    def nodes(self, nodes):
        if isinstance(nodes, str):
            if nodes.isdigit():
                nodes = int(nodes)
            # else just set the string
        self._nodes = nodes

    @property
    def ntasks(self):
        return self._ntasks

    @ntasks.setter
    def ntasks(self, n):
        self._ntasks = n

    @property
    def ppn(self):
        if self._queuing_system is not None and self._ppn is None:
            if self.queuing_system == "local":
                self.ppn = os.cpu_count()
                return self.ppn
        # if self._ppn is not None:
        #     return self._ppn
        # raise ValueError("Need to set 'ppn'")
        return self._ppn

    @ppn.setter
    def ppn(self, ppn):
        if isinstance(ppn, str):
            ppn = int(ppn)
        self._ppn = ppn

    @property
    def total_ncpus(self):
        # special because we can compute it
        if self._total_ncpus is not None:
            return self._total_ncpus
        try:
            nodes = self.nodes
            ppn = self.ppn
        except ValueError:
            raise ValueError("Cannot set automatically 'total_ncpus' because "
                             "either 'nodes' or 'ppn' is None.")
        # this is for Torque systems
        if isinstance(nodes, str):
            if ":" in nodes:
                nodes = nodes.split(":")[0]
        try:
            self.total_ncpus = int(nodes) * int(ppn)
            return self.total_ncpus
        except (TypeError, ValueError):
            # one of the properties does not exists
            pass
        # try with ntasks
        try:
            self.total_ncpus = int(self.ntasks)
            return self.total_ncpus
        except (ValueError, TypeError):
            raise ValueError("Could not compute 'total_ncpus'.")

    @total_ncpus.setter
    def total_ncpus(self, total_ncpus):
        if isinstance(total_ncpus, str):
            total_ncpus = int(total_ncpus)
        self._total_ncpus = total_ncpus

    def _add_args_to_cmd(self, cmd, args):
        tojoin = [cmd]
        for arg, value in args.items():
            if arg.endswith("="):
                if not len(str(value)):
                    raise ValueError("Arg with an '=' have no value...?")
                # make sure there are no spaces after the '='
                tojoin.append(f"{arg}{value}")
                continue
            if not len(str(value)):
                # no value
                tojoin.append(f"{arg}")
                continue
            else:
                tojoin.append(f"{arg} {value}")
                continue
        return " ".join(tojoin)

    def _convert_str_to_args(self, args):
        # if needed, convert str args to a dict. else return samething
        # /path/to/command -arg1 1 --arg2=2 -arg3
        if isinstance(args, dict):
            return args
        # need to split the arguements into a dict
        # first strip surrounding spaces if any
        if not isinstance(args, list):
            args = args.strip(" ")
            split = args.split(" ")
        else:
            split = args
        toreturn = {}
        for j, item in enumerate(split):
            if item.startswith("-"):
                if "=" in item:
                    splitted = item.split("=")
                    if len(splitted) != 2:
                        raise LookupError(f"arg makes no sense: {item}")
                    # the split removes the '='
                    value = self._return_arg_val(splitted[1])
                    toreturn[splitted[0] + "="] = value
                    continue
                # check following item for arg value
                if j == len(split) - 1:
                    # last argument with nothing after
                    toreturn[item] = ""
                    continue
                if split[j + 1].startswith("-"):
                    # next item is also an arg
                    toreturn[item] = ""
                    continue
                # else, next item is the value of the arg
                toreturn[item] = self._return_arg_val(split[j + 1])
        return toreturn

    def _return_arg_val(self, string):
        # try to return a float or int if possible
        try:
            return int(string)
        except ValueError:
            pass
        try:
            return float(string)
        except ValueError:
            pass
        return string

    def _split_cmd_from_args(self, cmd):
        # returns the cmd separated from the command arguments if any
        split = cmd.split(" ")
        if len(split) == 1:
            # no command args
            return split[0], {}
        return split[0], self._convert_str_to_args(split[1:])


class BaseLogStructure(BaseStructure):
    """Base class for log file structures.
    """
    # only define some aliases
    @property
    def cputime(self):
        if "timing" not in self.all_attributes:
            raise AttributeError("'cputime is not an attribute because "
                                 "'timing' is not either.")
        return self.timing["cputime"]

    @property
    def walltime(self):
        if "timing" not in self.all_attributes:
            raise AttributeError("'walltime is not an attribute because "
                                 "'timing' is not either.")
        return self.timing["walltime"]
