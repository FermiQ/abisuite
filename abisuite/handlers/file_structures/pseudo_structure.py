from .bases import BaseStructure


class PseudoStructure(BaseStructure):
    """Pseudo potential file structure.
    """
    all_attributes = (
            "has_so", "is_full_relativistic", "is_paw", "is_ultrasoft", "ixc",
            "zatom", "zion",
            )
