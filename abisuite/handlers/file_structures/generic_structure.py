from .bases import BaseStructure, BaseInputStructure
from ...variables import GENERIC_INPUT_VARIABLES_DB


class GenericStructure(BaseStructure):
    """A dummy structure with no attributes.
    """
    all_attributes = tuple([])


class GenericInputStructure(BaseInputStructure):
    """A dummy structure for input input files.
    """
    _variables_db = GENERIC_INPUT_VARIABLES_DB
