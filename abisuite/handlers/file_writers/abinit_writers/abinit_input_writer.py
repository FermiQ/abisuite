from .bases import BaseAbinitInputWriter
from ...file_structures import AbinitInputStructure


class AbinitInputWriter(BaseAbinitInputWriter):
    """Class that can write an Abinit Input file."""
    _loggername = "AbinitInputWriter"
    _structure_class = AbinitInputStructure
