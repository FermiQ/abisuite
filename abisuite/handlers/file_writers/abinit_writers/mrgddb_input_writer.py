from ..bases import BaseInputWriter
from ...file_structures import AbinitMrgddbInputStructure


class AbinitMrgddbInputWriter(BaseInputWriter):
    """Class that writes a mrgddb input file.
    """
    _loggername = "AbinitMrgddbInputWriter"
    _structure_class = AbinitMrgddbInputStructure

    @property
    def lines(self):
        lines = [
            self.output_file_path + "\n",
            self.title + "\n",
            str(self.nddb) + "\n",
            ]
        for ddb in self.ddb_paths:
            if ddb.endswith("\n"):
                lines.append(ddb)
            else:
                lines.append(ddb + "\n")
        return lines
