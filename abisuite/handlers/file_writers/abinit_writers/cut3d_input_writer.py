from ..bases import BaseWriter
from ...file_structures import AbinitCut3DInputStructure


class AbinitCut3DInputWriter(BaseWriter):
    """Writer class for the 'cut3d' utility of abinit.
    """
    _files_suffix = ".in"
    _loggername = "AbinitCut3DInputWriter"
    _structure_class = AbinitCut3DInputStructure

    @property
    def lines(self):
        lines = []
        lines.append(self.data_file_path + "\n")
        lines.append(str(self.option) + "\n")
        lines.append(self.output_file_path + "\n")
        for post in self.post_option:
            lines.append(str(post) + "\n")
        return lines
