from ..bases import BaseInputWriter
from ...file_structures import Wannier90InputStructure


class Wannier90InputWriter(BaseInputWriter):
    """Class that can write wannier90.x input files.
    """
    _loggername = "Wannier90InputWriter"
    _structure_class = Wannier90InputStructure

    def _append_var_block(self, lines, varblock):
        lines.append(f"! {varblock.upper()}\n")
        for variable in self.input_variables.values():
            if variable.block == varblock:
                lines.append(str(variable))
        lines.append("\n")

    @property
    def lines(self):
        lines = []
        self._append_var_block(lines, "control")
        self._append_var_block(lines, "system")
        self._append_var_block(lines, "kpoints")
        return lines
