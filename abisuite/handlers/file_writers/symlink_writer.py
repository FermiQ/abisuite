import os

from .bases import BaseStreamLineWriter
from ..file_structures import SymLinkStructure


class SymLinkWriter(BaseStreamLineWriter):
    """Writer class for a symlink file.
    """
    _loggername = "SymLinkWriter"
    _structure_class = SymLinkStructure

    def _do_write(self):
        os.symlink(self.source, self.path,
                   target_is_directory=self.source_is_directory)
