from .bases import BaseQEInputWriter
from ...file_structures import QEEPWInputStructure


class QEEPWInputWriter(BaseQEInputWriter):
    """Class that can write Quantum Espresso epw.x input files.
    """
    _loggername = "QEEPWInputWriter"
    _structure_class = QEEPWInputStructure

    @property
    def lines(self):
        # start with control variables
        lines = []
        # start with title line
        if "title" not in self.input_variables:
            raise ValueError("'title' must be specified for a ph.w calc.")
        # pop title because we don't want to write it twice
        title = self.input_variables.pop("title")
        lines.append(str(title.value) + "\n")
        self._append_var_block(lines, "inputepw")
        self._append_others(lines)
        # reset input variables to keep 'title' in structure
        self.input_variables["title"] = title
        return lines
