from .bases import BaseQEInputWriter
from ...file_structures import QELD1InputStructure


class QELD1InputWriter(BaseQEInputWriter):
    """Class that can write Quantum Espresso pw.x input files.
    """
    _loggername = "QELD1InputWriter"
    _structure_class = QELD1InputStructure

    @property
    def lines(self):
        # start with control variables
        lines = []
        self._append_var_block(lines, "input")
        self._append_var_block(lines, "test")
        return lines
