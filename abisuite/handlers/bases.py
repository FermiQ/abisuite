import os

from ..routines import full_abspath


class BaseHandler:
    """Base class for any handler object."""
    _is_writable = False

    def __init__(self, *args, **kwargs):
        self._path = None
        self.has_been_read = False

    def __enter__(self):
        if not self._is_writable and not self.exists:
            raise FileNotFoundError(
                    f"File/directory does not exist: '{self.path}'.")
        if not self.has_been_read and self.exists:
            self.read()
        return self

    def __exit__(self, exc_type, exc_value, traceback):
        pass

    @property
    def basename(self):
        return os.path.basename(self.path)

    @property
    def exists(self):
        try:
            return os.path.exists(self.path)
        except (ValueError, TypeError):
            # path no set => return False
            return False

    @property
    def islink(self):
        return os.path.islink(self.path)

    @property
    def path(self):
        if self._path is not None:
            return self._path
        raise ValueError("Need to set 'path'.")

    @path.setter
    def path(self, path):
        self._path = full_abspath(path)

    def read(self):
        self.has_been_read = True
