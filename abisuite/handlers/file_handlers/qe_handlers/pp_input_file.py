from ..bases import BaseInputFileHandler
from ....approvers import QEPPInputApprover
from ...file_parsers import QEPPInputParser
from ...file_structures import QEPPInputStructure
from ...file_writers import QEPPInputWriter


class QEPPInputFile(BaseInputFileHandler):
    """File handler for a Quantum Espresso pp.x input file.
    """
    _approver_class = QEPPInputApprover
    _loggername = "QEPPInputFile"
    _parser_class = QEPPInputParser
    _structure_class = QEPPInputStructure
    _writer_class = QEPPInputWriter
