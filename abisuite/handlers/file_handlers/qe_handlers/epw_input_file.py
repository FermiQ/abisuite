from ..bases import BaseInputFileHandler
from ....approvers import QEEPWInputApprover
from ...file_parsers import QEEPWInputParser
from ...file_structures import QEEPWInputStructure
from ...file_writers import QEEPWInputWriter


class QEEPWInputFile(BaseInputFileHandler):
    """File handler for a Quantum Espresso epw.x input file.
    """
    _approver_class = QEEPWInputApprover
    _loggername = "QEEPWInputFile"
    _parser_class = QEEPWInputParser
    _structure_class = QEEPWInputStructure
    _writer_class = QEEPWInputWriter
