from ..bases import BaseInputFileHandler
from ....approvers import QEProjwfcInputApprover
from ...file_parsers import QEProjwfcInputParser
from ...file_structures import QEProjwfcInputStructure
from ...file_writers import QEProjwfcInputWriter


class QEProjwfcInputFile(BaseInputFileHandler):
    """File handler for a Quantum Espresso projwfc.x input file.
    """
    _approver_class = QEProjwfcInputApprover
    _loggername = "QEProjwfcInputFile"
    _parser_class = QEProjwfcInputParser
    _structure_class = QEProjwfcInputStructure
    _writer_class = QEProjwfcInputWriter
