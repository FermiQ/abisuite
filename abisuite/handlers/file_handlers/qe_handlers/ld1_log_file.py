from ..bases import BaseLogFileHandler
from ...file_parsers import QELD1LogParser
from ...file_structures import QELD1LogStructure


class QELD1LogFile(BaseLogFileHandler):
    """Log file handler for a Quantum Espresso ld1.x log file.
    """
    _loggername = "QELD1LogFile"
    _parser_class = QELD1LogParser
    _structure_class = QELD1LogStructure
