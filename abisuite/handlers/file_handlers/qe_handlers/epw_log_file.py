from ..bases import BaseLogFileHandler
from ...file_parsers import QEEPWLogParser
from ...file_structures import QEEPWLogStructure


class QEEPWLogFile(BaseLogFileHandler):
    """Log file handler for a Quantum Espresso epw.x log file.
    """
    _loggername = "QEEPWLogFile"
    _parser_class = QEEPWLogParser
    _structure_class = QEEPWLogStructure
