from ..bases import BaseFileHandler
from ...file_parsers import QEPHDyn0Parser
from ...file_structures import QEPHDyn0Structure


class QEPHDyn0File(BaseFileHandler):
    """Class that represents a .dyn0 file produced by a ph.x calculation
    with more than 1 qpt.
    """
    _loggername = "QEPHDyn0File"
    _parser_class = QEPHDyn0Parser
    _structure_class = QEPHDyn0Structure
