from ..bases import BaseFileHandler
from ...file_parsers import QEEPWResistivityParser
from ...file_structures import QEEPWResistivityStructure


class QEEPWResistivityFile(BaseFileHandler):
    """Class that represents a resistivity function file produced by a epw.x
    calculation from Quantum Espresso.
    """
    _loggername = "QEEPWResistivityFile"
    _parser_class = QEEPWResistivityParser
    _structure_class = QEEPWResistivityStructure
