from ..bases import BaseInputFileHandler
from ....approvers import QEMatdynInputApprover
from ...file_parsers import QEMatdynInputParser
from ...file_structures import QEMatdynInputStructure
from ...file_writers import QEMatdynInputWriter


class QEMatdynInputFile(BaseInputFileHandler):
    """File handler for a Quantum Espresso matdyn.x input file.
    """
    _approver_class = QEMatdynInputApprover
    _loggername = "QEMatdynInputFile"
    _parser_class = QEMatdynInputParser
    _structure_class = QEMatdynInputStructure
    _writer_class = QEMatdynInputWriter
