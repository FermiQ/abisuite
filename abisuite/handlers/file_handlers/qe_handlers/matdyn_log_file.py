from ..bases import BaseLogFileHandler
from ...file_parsers import QEMatdynLogParser
from ...file_structures import QEMatdynLogStructure


class QEMatdynLogFile(BaseLogFileHandler):
    """Log file handler for a Quantum Espresso matdyn.x log file.
    """
    _loggername = "QEMatdynLogFile"
    _parser_class = QEMatdynLogParser
    _structure_class = QEMatdynLogStructure
