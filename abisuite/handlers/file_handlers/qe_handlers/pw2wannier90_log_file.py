from ..bases import BaseLogFileHandler
from ...file_parsers import QEPW2Wannier90LogParser
from ...file_structures import QEPW2Wannier90LogStructure


class QEPW2Wannier90LogFile(BaseLogFileHandler):
    """Log file handler for a Quantum Espresso pw2wannier90.x log file.
    """
    _loggername = "QEPW2Wannier90LogFile"
    _parser_class = QEPW2Wannier90LogParser
    _structure_class = QEPW2Wannier90LogStructure
