from ..bases import BaseLogFileHandler
from ...file_parsers import QEPPLogParser
from ...file_structures import QEPPLogStructure


class QEPPLogFile(BaseLogFileHandler):
    """Log file handler for a Quantum Espresso pp.x log file.
    """
    _loggername = "QEPPLogFile"
    _parser_class = QEPPLogParser
    _structure_class = QEPPLogStructure
