from ..bases import BaseFileHandler
from ...file_parsers import QEEPWBandEigParser
from ...file_structures import QEEPWBandEigStructure


class QEEPWBandEigFile(BaseFileHandler):
    """File handler class for a 'band.eig' file produced by epw.x script
    from Quantum Espresso.
    """
    _loggername = "QEEPWBandEigFile"
    _parser_class = QEEPWBandEigParser
    _structure_class = QEEPWBandEigStructure
