from ..bases import BaseInputFileHandler
from ....approvers import QEDOSInputApprover
from ...file_parsers import QEDOSInputParser
from ...file_structures import QEDOSInputStructure
from ...file_writers import QEDOSInputWriter


class QEDOSInputFile(BaseInputFileHandler):
    """File handler for a Quantum Espresso dos.x input file."""
    _approver_class = QEDOSInputApprover
    _loggername = "QEDOSInputFile"
    _parser_class = QEDOSInputParser
    _structure_class = QEDOSInputStructure
    _writer_class = QEDOSInputWriter
