from ..bases import BaseLogFileHandler
from ...file_parsers import QEDynmatLogParser
from ...file_structures import QEDynmatLogStructure


class QEDynmatLogFile(BaseLogFileHandler):
    """Log file handler for a Quantum Espresso dynmat.x log file.
    """
    _loggername = "QEDynmatLogFile"
    _parser_class = QEDynmatLogParser
    _structure_class = QEDynmatLogStructure
