from ..bases import BaseLogFileHandler
from ...file_parsers import QEPWLogParser
from ...file_structures import QEPWLogStructure


class QEPWLogFile(BaseLogFileHandler):
    """File handler class for the pw.x log file.
    """
    _loggername = "QEPWLogFile"
    _parser_class = QEPWLogParser
    _structure_class = QEPWLogStructure
