from ..bases import BaseFileHandler
from ...file_parsers import QEMatdynFreqParser
from ...file_structures import QEMatdynFreqStructure


class QEMatdynFreqFile(BaseFileHandler):
    """File handler class for a '.freq' file produced by the matdyn.x script
    of Quantum Espresso.
    """
    _loggername = "QEMatdynFreqFile"
    _parser_class = QEMatdynFreqParser
    _structure_class = QEMatdynFreqStructure
