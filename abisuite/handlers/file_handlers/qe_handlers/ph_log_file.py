from ..bases import BaseLogFileHandler
from ...file_parsers import QEPHLogParser
from ...file_structures import QEPHLogStructure


class QEPHLogFile(BaseLogFileHandler):
    """File handler class for a ph.x log file.
    """
    _loggername = "QEPHLogFile"
    _parser_class = QEPHLogParser
    _structure_class = QEPHLogStructure
