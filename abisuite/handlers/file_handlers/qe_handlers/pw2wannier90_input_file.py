from ..bases import BaseInputFileHandler
from ....approvers import QEPW2Wannier90InputApprover
from ...file_parsers import QEPW2Wannier90InputParser
from ...file_structures import QEPW2Wannier90InputStructure
from ...file_writers import QEPW2Wannier90InputWriter


class QEPW2Wannier90InputFile(BaseInputFileHandler):
    """File handler for a Quantum Espresso pw2wannier90.x input file.
    """
    _approver_class = QEPW2Wannier90InputApprover
    _loggername = "QEPW2Wannier90InputFile"
    _parser_class = QEPW2Wannier90InputParser
    _structure_class = QEPW2Wannier90InputStructure
    _writer_class = QEPW2Wannier90InputWriter
