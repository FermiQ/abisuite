from ..bases import BaseLogFileHandler
from ...file_parsers import QEProjwfcLogParser
from ...file_structures import QEProjwfcLogStructure


class QEProjwfcLogFile(BaseLogFileHandler):
    """Log file handler for a Quantum Espresso projwfc.x log file.
    """
    _loggername = "QEProjwfcLogFile"
    _parser_class = QEProjwfcLogParser
    _structure_class = QEProjwfcLogStructure
