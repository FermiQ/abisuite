from .bases import BaseFilesFile
from ...file_parsers import AbinitAnaddbFilesParser
from ...file_structures import AbinitAnaddbFilesStructure
from ...file_writers import AbinitAnaddbFilesWriter


class AbinitAnaddbFilesFile(BaseFilesFile):
    """File handler for an anaddb files file.
    """
    _loggername = "AbinitAnaddbFilesFile"
    _parser_class = AbinitAnaddbFilesParser
    _structure_class = AbinitAnaddbFilesStructure
    _writer_class = AbinitAnaddbFilesWriter
