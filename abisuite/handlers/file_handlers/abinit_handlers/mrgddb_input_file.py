from ..bases import BaseInputFileHandlerNoInputVariables
from ...file_parsers import AbinitMrgddbInputParser
from ...file_structures import AbinitMrgddbInputStructure
from ...file_writers import AbinitMrgddbInputWriter
from ....approvers import AbinitMrgddbInputApprover


class AbinitMrgddbInputFile(BaseInputFileHandlerNoInputVariables):
    """File handler class for a mrgddb input file.
    """
    _approver_class = AbinitMrgddbInputApprover
    _loggername = "AbinitMrgddbInputFile"
    _parser_class = AbinitMrgddbInputParser
    _structure_class = AbinitMrgddbInputStructure
    _writer_class = AbinitMrgddbInputWriter
