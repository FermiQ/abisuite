from ..bases import BaseInputFileHandler, BaseFileWithPseudosHandler
from ....approvers import AbinitInputApprover
from ...file_parsers import AbinitInputParser
from ...file_structures import AbinitInputStructure
from ...file_writers import AbinitInputWriter


class AbinitInputFile(BaseInputFileHandler, BaseFileWithPseudosHandler):
    """A file handler for the input file of the abinit script.
    """
    _approver_class = AbinitInputApprover
    _loggername = "AbinitInputFile"
    _parser_class = AbinitInputParser
    _structure_class = AbinitInputStructure
    _writer_class = AbinitInputWriter
