from ..bases import BaseLogFileHandler
from ...file_parsers import AbinitMrgddbLogParser
from ...file_structures import AbinitMrgddbLogStructure


class AbinitMrgddbLogFile(BaseLogFileHandler):
    """File handler class for a mrgddb log file.
    """
    _loggername = "AbinitMrgddbLogFile"
    _parser_class = AbinitMrgddbLogParser
    _structure_class = AbinitMrgddbLogStructure
