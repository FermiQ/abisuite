from ..bases import BaseInputFileHandler
from ...file_parsers import AbinitAnaddbInputParser
from ...file_structures import AbinitAnaddbInputStructure
from ...file_writers import AbinitAnaddbInputWriter
from ....approvers import AbinitAnaddbInputApprover


class AbinitAnaddbInputFile(BaseInputFileHandler):
    """A file handler for the input file of the abinit anaddb script.
    """
    _approver_class = AbinitAnaddbInputApprover
    _loggername = "AbinitAnaddbInputFile"
    _parser_class = AbinitAnaddbInputParser
    _structure_class = AbinitAnaddbInputStructure
    _writer_class = AbinitAnaddbInputWriter
