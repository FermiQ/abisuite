from ..bases import BaseFileHandler
from ...file_parsers import AbinitEIGParser
from ...file_structures import AbinitEIGStructure


class AbinitEIGFile(BaseFileHandler):
    """File Handler class for an abinit EIG file.
    """
    _loggername = "AbinitEIGFile"
    _parser_class = AbinitEIGParser
    _structure_class = AbinitEIGStructure
