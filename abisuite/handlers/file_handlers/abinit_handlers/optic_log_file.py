from ..bases import BaseLogFileHandler
from ...file_parsers import AbinitOpticLogParser
from ...file_structures import AbinitOpticLogStructure


class AbinitOpticLogFile(BaseLogFileHandler):
    """File handler class for an optic log file.
    """
    _loggername = "AbinitOpticLogFile"
    _parser_class = AbinitOpticLogParser
    _structure_class = AbinitOpticLogStructure
