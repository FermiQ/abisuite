from ..bases import BaseFileHandler
from ...file_parsers import AbinitDMFTProjectorsParser
from ...file_structures import AbinitDMFTProjectorsStructure


class AbinitDMFTProjectorsFile(BaseFileHandler):
    """File Handler class for an abinit DMFT projectors file.
    """
    _loggername = "AbinitDMFTProjectorsFile"
    _parser_class = AbinitDMFTProjectorsParser
    _structure_class = AbinitDMFTProjectorsStructure
