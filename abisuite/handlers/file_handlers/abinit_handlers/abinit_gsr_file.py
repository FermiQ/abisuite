from ..bases import BaseFileHandler
from ...file_parsers import AbinitGSRParser
from ...file_structures import AbinitGSRStructure


class AbinitGSRFile(BaseFileHandler):
    """File Handler class for an abinit GSR file.
    """
    _loggername = "AbinitGSRFile"
    _parser_class = AbinitGSRParser
    _structure_class = AbinitGSRStructure
