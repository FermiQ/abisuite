from .bases import BaseFilesFile
from ..bases import BaseFileWithPseudosHandler
from ...file_parsers import AbinitFilesParser
from ...file_structures import AbinitFilesStructure
from ...file_writers import AbinitFilesWriter


class AbinitFilesFile(BaseFilesFile, BaseFileWithPseudosHandler):
    """File handler for an ABINIT files file.
    """
    _loggername = "AbinitFilesFile"
    _parser_class = AbinitFilesParser
    _structure_class = AbinitFilesStructure
    _writer_class = AbinitFilesWriter
