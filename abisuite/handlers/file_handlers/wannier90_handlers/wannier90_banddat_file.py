from .bases import BaseWannier90FileHandler
from ...file_parsers import Wannier90BandDatParser
from ...file_structures import Wannier90BandDatStructure


class Wannier90BandDatFile(BaseWannier90FileHandler):
    """File handler for a wannier90.x seedname_band.dat file produced when
    bands_plot = True.
    """
    _loggername = "Wannier90BandDatFile"
    _parser_class = Wannier90BandDatParser
    _structure_class = Wannier90BandDatStructure
