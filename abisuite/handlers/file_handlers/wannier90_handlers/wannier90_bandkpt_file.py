from .bases import BaseWannier90FileHandler
from ...file_parsers import Wannier90BandkptParser
from ...file_structures import Wannier90BandkptStructure


class Wannier90BandkptFile(BaseWannier90FileHandler):
    """File handler for a wannier90.x seedname_band.kpt file produced when
    bands_plot = True.
    """
    _loggername = "Wannier90BandkptFile"
    _parser_class = Wannier90BandkptParser
    _structure_class = Wannier90BandkptStructure
