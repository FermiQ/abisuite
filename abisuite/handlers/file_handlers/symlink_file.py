from .bases import BaseApprovableFileHandler, PathConvertibleFileHandler
from ..file_parsers import SymLinkParser
from ..file_structures import SymLinkStructure
from ...approvers import SymLinkApprover
from ..file_writers import SymLinkWriter


class SymLinkFile(BaseApprovableFileHandler, PathConvertibleFileHandler):
    """File handler for all symlink files.
    """
    _approver_class = SymLinkApprover
    _loggername = "SymLinkFile"
    _parser_class = SymLinkParser
    _structure_class = SymLinkStructure
    _writer_class = SymLinkWriter

    @property
    def is_broken_link(self):
        if not self.exists:
            return True
        try:
            self.read()
        except ValueError:
            # broken link
            return True
        return False

    def move(self, path, overwrite=False):
        # need to override this since its a particular type of file
        path = self._prepare_path_for_file_transfer(path, overwrite=overwrite)
        # delete file and redo symlink pointing at same source
        self.delete()
        self.path = path
        self.write(overwrite=overwrite)
