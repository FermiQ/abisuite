import abc
import os
import logging
import shutil

from ..bases import BaseHandler
from ..file_structures import MPIStructure
from ...approvers import MPIApprover
from ...bases import BaseStructuredObject
from ...exceptions import DevError
from ...linux_tools import mkdir
from ...routines import full_abspath, is_list_like
from ...variables import InputVariableDict


class BaseFileHandler(BaseHandler, BaseStructuredObject, abc.ABC):
    """Base class for a file handler.

    A file handler is an object that handles files (Duh!).
    It can load the properties from an existing file using the
    appropriate Parser object.
    It can also move the file, copy it or delete it.
    """
    isdir = False
    _parser_class = None

    def __init__(self, *args, **kwargs):
        # do this first because of the 'smart' __getattr__ and __setattr__
        BaseStructuredObject.__init__(self, *args, **kwargs)
        BaseHandler.__init__(self, *args, **kwargs)
        self._parser = None

    def __eq__(self, handler):
        # only compare same class objects
        if not isinstance(handler, self.__class__):
            return False
        if self.path != handler.path:
            return False
        if self.structure != handler.structure:
            return False
        return True

    def __str__(self):
        string = f"=== {self._loggername} ===\n\n"
        string += f"path = {self.path}\n"
        string += f"structure = {self.structure}\n"
        return string

    @property
    def isfile(self):
        return os.path.isfile(self.path)

    @property
    def path(self):
        return BaseHandler.path.fget(self)

    @path.setter
    def path(self, path):
        if not path.endswith(
                self._parser_class._expected_ending
                ) and not os.path.isfile(path):
            self._logger.warning("Automatically adding suffix "
                                 f"'{self._parser_class._expected_ending}' "
                                 "to path.")
            path += self._parser_class._expected_ending
        BaseHandler.path.fset(self, path)
        if self._parser is not None:
            # reset parser
            self._parser.path = path

    @property
    def parser(self):
        if self._parser is not None:
            return self._parser
        # set parser if it does not exist
        self._parser = self._parser_class(loglevel=self._loglevel)
        self._parser.path = self.path
        return self.parser

    @property
    def workdir(self):
        return os.path.dirname(self.path)

    @property
    def isdir(self):
        return False

    def copy(self):
        """Copy the File Handler object.

        Returns
        -------
        New file handler object.
        """
        if self.exists and not self.has_been_read:
            self.read()
        new = self.__class__(loglevel=self._loglevel)
        new.path = self.path
        # copy structure
        new.structure = self.structure.copy()
        return new

    def copy_file(self, path, overwrite=False):
        """Copy the file to another location. The file must already exist
        prior to being copied. Call the 'write' method if necessary.

        If 'path' is a directory, the file will be written inside it with
        the same name.

        Parameters
        ----------
        path : str
               The path where the file should be copied.
        overwrite : bool, optional
                    If True, if there is already a file there, it will
                    be overwritten.

        Raises
        ------
        FileExistsError : If a file with same name already exists and
                          'overwrite' is False.
        FileNotFoundError : If the file to be copied does not exist (yet).
                            Use the 'write' method to write it if possible.
        """
        path = self._prepare_path_for_file_transfer(path, overwrite=overwrite)
        dir_ = os.path.dirname(path)
        if not os.path.exists(dir_):
            mkdir(dir_)
        # copy file
        self._logger.debug(f"Copying: {self.path} -> {path}")
        shutil.copy2(self.path, path, follow_symlinks=False)
        # follow_synlink = False will create a new synlink instead of copying
        # source file which link points to
        self._logger.debug(f"Copying done: {self.path} -> {path}")

    def delete(self):
        """Delete the file.
        """
        self._logger.debug(f"Deleting '{self.path}'")
        if not os.path.isfile(self.path) and not os.path.islink(self.path):
            # file already doesn't exists
            return
        os.remove(self.path)
        self._logger.debug("Deleting done.")

    def move(self, path, overwrite=False):
        """Move the file to another location.

        Equivalent to copy_file, then delete, then change the path variable.
        Parameters
        ----------
        path : str
               The path where to move the file. If it's a directory,
               the file will be moved to this directory with the same name.
        """
        path = self._prepare_path_for_file_transfer(path, overwrite=overwrite)
        self._logger.debug(f"Moving: {self.path} -> {path}")
        # # equivalent to copy and delete, in addition to changin the path prop
        # self.copy_file(path, overwrite=overwrite)
        # self.delete()
        shutil.move(self.path, path)
        self.path = path

    def read(self, force=False):
        """Read file set on the 'path' attribute.

        Parameters
        ----------
        force: bool, optional
               If True, the file is read even if it has been already read.
        """
        if self.has_been_read and not force:
            # file already read, do nothing
            return
        # set data to structure
        self.parser.read()
        self.structure = self.parser.structure.copy()

    def _prepare_path_for_file_transfer(self, path, overwrite=False):
        # if path is a dir, change it to a dir + current filename
        # if path already exists, depending of overwrite, either raise an
        # error or delete already existing file.
        # if self.path doesn't exist, raise an error.
        path = full_abspath(path)
        if os.path.isdir(path):
            path = os.path.join(path, os.path.basename(self.path))
        if not os.path.isfile(self.path) and not os.path.islink(self.path):
            # need to create file first
            raise FileNotFoundError(f"File {self.path} doesn't exist.")
        if os.path.isfile(path) or os.path.islink(path):
            # a file already exist at the destination
            if not overwrite:
                raise FileExistsError(
                        f"A file already exists at destination: '{path}'.")
            else:
                os.remove(path)
        return path

    @classmethod
    def from_calculation(cls, path, *args, **kwargs):
        """Initialize the file handler from a calculation.

        This method works by locating the meta file in the calculation
        directory.

        Parameters
        ----------
        path : str
               The path to the calculation directory or a CalculationDirectory
               instance.
        """
        from ..directory_handlers import CalculationDirectory
        if isinstance(path, CalculationDirectory):
            meta = path.meta_data_file
        else:
            # import here to prevent loop imports
            from .meta_data_file import MetaDataFile
            meta = MetaDataFile.from_calculation(
                    path, loglevel=kwargs.get("loglevel", logging.INFO))
        with meta:
            return cls.from_meta_data_file(meta, *args, **kwargs)

    @classmethod
    def from_file(cls, path, *args, **kwargs):
        """Initialize the file handler directly from a file that exists.

        Parameters
        ----------
        path : str
               The path to the file.
        """
        instance = cls(*args, **kwargs)
        # instance._logger.debug(f"Creating file handler from file: {path}")
        instance.path = path
        # instance.read()
        return instance

    @classmethod
    def from_meta_data_file(cls, metadatafile, *args, **kwargs):
        """Initialize the file handler from a meta data file.
        """
        # import here to prevent loop imports
        from .meta_data_file import MetaDataFile
        if not isinstance(metadatafile, MetaDataFile):
            raise TypeError("Expected a MetaDataFile object but got: "
                            f"{metadatafile}")
        with metadatafile:
            paths = cls._get_file_from_meta_data_file(metadatafile)
        if is_list_like(paths):
            if len(paths) > 1:
                return [cls.from_file(x, *args, **kwargs) for x in paths]
            paths = paths[0]
        if isinstance(paths, dict):
            return {x: cls.from_file(y, *args, **kwargs)
                    for x, y in paths.items()}
        return cls.from_file(paths, *args, **kwargs)

    @classmethod
    def from_parser(cls, parser, *args, **kwargs):
        """Init a file handler from its parser object.
        """
        if not isinstance(parser, cls._parser_class):
            raise TypeError(f"Expected a '{cls._parser_class}' object but got "
                            f"'{parser}'")
        instance = cls(*args, **kwargs)
        instance.path = parser.path
        instance.structure = parser.structure.copy()
        return instance

    @classmethod
    def _get_file_from_meta_data_file(cls, metadatafile):
        return cls._parser_class._filepath_from_meta(metadatafile)


class BaseLogFileHandler(BaseFileHandler):
    """Base file handler class for log files.
    """
    pass


class BaseWritableFileHandler(BaseFileHandler):
    """Base file handler class for writable files.

    Can be used as a context manager (recommended). If a modification occurs
    to the file while it is in the context manager, the file will be rewritten
    (if possible).
    """
    _is_writable = True
    _writer_class = None

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self._writer = None
        self._previous_state = None

    def __enter__(self):
        self = super().__enter__()
        self._previous_state = None
        if os.path.isfile(self.path):
            # save previous state if file exists
            self.set_state()
        return self

    def __exit__(self, exc_type, exc_value, traceback):
        # write itself if there are any changes
        # except if an error occurs!
        if exc_type is not None:
            return
        if not os.path.isfile(self.path) or self._previous_state != self:
            # self._logger.debug("(Re)writing file upon context manager exit.")
            self.write(overwrite=True)

    @property
    def writer(self):
        if self._writer is not None:
            return self._writer
        if self._writer_class is None:
            raise DevError(f"Writer class not set for {self._loggername}")
        writer = self._writer_class(loglevel=self._loglevel)
        writer.path = self.path
        self._writer = writer
        return self.writer

    def set_state(self):
        """Can be used inside a context manager to reset the state of a file
        after being modified. Using this will prevent the writing of a file
        after leaving the context manager if no more modifications are done.
        """
        self._previous_state = self.copy()

    def write(self, *args, **kwargs):
        """Write the file. All the arguments goes to the 'write' methods
        of the appropriate writers.

        Raises
        ------
        CannotBeWrittenError : If the file can't be written (for exemple if
                               it is an output file handler or something like
                               that).
        """
        self._logger.debug(f"Writing file at: {self.path}")
        # reset writer's path
        self.writer.path = self.path
        self.writer.structure = self.structure.copy()
        self.writer.write(*args, **kwargs)

    @classmethod
    def from_writer(cls, writer, *args, **kwargs):
        """Initialize the file handler from a writer object.
        """
        if not isinstance(writer, cls._writer_class):
            raise TypeError(f"Expected a '{cls._writer_class}' object but got "
                            f"'{writer}'")
        instance = cls(*args, **kwargs)
        instance.path = writer.path
        instance.structure = writer.structure
        return instance


class BaseApprovableFileHandler(BaseWritableFileHandler):
    """Special case of file handlers which can be validated through
    the relevant approver class.
    """
    _approver_class = None

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        if self._approver_class is None:
            raise DevError("Need to set '_approver_class' attribute.")
        self._approver = None

    @property
    def approver(self):
        if self._approver is not None:
            return self._approver
        self.approver = self._approver_class(loglevel=self._loglevel)
        return self.approver

    @approver.setter
    def approver(self, approver):
        if not isinstance(approver, self._approver_class):
            raise TypeError(f"Expected '{self._approver_class}' object but got"
                            f" '{approver}'")
        self._approver = approver

    def validate(self):
        self.approver.structure = self.structure.copy()
        self.approver.validate()
        if not self.approver.is_valid:
            self.approver.raise_errors()


class BaseFileWithPseudosHandler(BaseWritableFileHandler):
    """File handler for files with pseudos entries in the structure.
    """
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        if "pseudos" not in self.structure.all_attributes:
            raise DevError("No 'pseudos' in the structure...")

    @property
    def pseudos(self):
        return self.structure.pseudos

    @pseudos.setter
    def pseudos(self, pseudos):
        # # do pseudo file import here to prevent import loops
        # from .pseudo_file import PseudoFile
        if isinstance(pseudos, str):
            pseudos = (pseudos, )
        from .pseudo_file import get_true_pseudo_path
        pseudos = tuple([get_true_pseudo_path(x) for x in pseudos])
        # pseudos = tuple([PseudoFile.from_file(x) for x in pseudos])
        # # read pseudos
        # for pseudo in pseudos:
        #     pseudo.read()
        # peudos should be sets
        self.structure.pseudos = pseudos


class BaseMPIApprovableFileHandler(BaseApprovableFileHandler):
    """Special case of file handler which can be validated through
    an approver and the MPI approver.
    """
    # need to support mpi structure as well
    _mpi_structure_class = MPIStructure

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self._mpi_approver = MPIApprover(loglevel=self._logger.level)

    def __eq__(self, obj):
        if not super().__eq__(obj):
            return False
        if self.mpi_structure != obj.mpi_structure:
            return False
        return True

    def __str__(self):
        string = super().__str__()
        string += f"mpi_structure = {self.mpi_structure}\n"
        return string

    @property
    def mpi_approver(self):
        return self._mpi_approver

    def copy(self):
        new = super().copy()
        new.mpi_structure = self.mpi_structure.copy()
        return new

    def read(self, *args, **kwargs):
        super().read(*args, **kwargs)
        # also create mpi structure
        # only copy relevant properties
        for attr in self.structure.get_relevant_properties():
            if attr in self.mpi_structure.all_attributes:
                setattr(self.mpi_structure, attr, getattr(self, attr))

    def validate(self):
        super().validate()
        # TODO: find a better way to update data (find a way such that its not
        # necessary to call this)
        # need to do this cause if we change a dict properties in an attribute
        # the same attribute (if it exist) in the mpi structure might not be
        # updated. (e.g.: mpi_command_arguments in PBSFile).
        self._update_mpi_structure()
        self.mpi_approver.structure = self.mpi_structure.copy()
        self.mpi_approver.validate()
        if not self.mpi_approver.is_valid:
            self.mpi_approver.raise_errors()

    def _update_mpi_structure(self):
        for attr in self.mpi_structure.get_relevant_properties():
            if attr in self.structure.all_attributes:
                setattr(self, attr, getattr(self, attr))


class PathConvertibleFileHandler(BaseWritableFileHandler):
    """Handler that can convert some of its path properties relative to
    something or to absolute path. Useful for files that needs changing
    when they are moved.
    """

    def __eq__(self, obj):
        if not isinstance(obj, PathConvertibleFileHandler):
            return False
            # raise TypeError("Cannot compare with an object which paths"
            #                 " cannot be converted.")
        self.convert_to_absolute_paths()
        obj.convert_to_absolute_paths()
        return super().__eq__(obj)

    def convert_to_relative_paths(self):
        """Convert all paths attribute to relative paths in respect to
        the calc_workdir.
        """
        # call an absolute conversion before in case relative conversion was
        # just called because of a dumb/ignorang dev. because calling twice
        # relative conversion breaks everything...
        self._convert_paths(absolute=True, _log=False)
        self._convert_paths(absolute=False)

    def convert_to_absolute_paths(self):
        """Convert all paths attribute to absolute paths.
        """
        self._convert_paths(absolute=True)

    def move(self, *args, **kwargs):
        # convert paths to relative before moving
        # FIXME: don't enter context manager here cause it breaks conversions
        # not sure why though (23/07/2020)...
        # with self:
        if not self.has_been_read and self.exists:
            self.read()
        self.write(absolute=False, overwrite=True)
        super().move(*args, **kwargs)
        self.write(absolute=True, overwrite=True)

    def _convert_paths(self, absolute=True, _log=True, **kwargs):
        # convert all paths that needs to be converted
        # if absolute and _log:
        #     self._logger.debug(f"Converting to absolute paths: {self.path}")
        # elif not absolute and _log:
        #     self._logger.debug(f"Converting to relative paths: {self.path}")
        rel = getattr(self, self.structure.relative_to_path_attribute)
        for attr in self.structure.convertible_path_attributes:
            try:
                setattr(self, attr,
                        self._convert_path(getattr(self, attr),
                                           absolute=absolute,
                                           relative_to=rel,
                                           **kwargs))
            except ValueError:
                # value was not set, don't need to worry about that
                continue

    def _convert_path(self, path, absolute=True, relative_to=None):
        # convert an absolute path to a path relative to the calc_workdir
        # if absolute=False => relative paths
        if absolute:
            return full_abspath(os.path.join(relative_to, path))
        if relative_to is None:
            raise DevError("Need to state which path it is relative to...")
        return os.path.relpath(path, start=relative_to)

    def write(self, *args, absolute=True, **kwargs):
        """Write method the file.

        Parameters
        ----------
        absolute : bool, optional
                   If True, all paths apprearing in the file are made
                   absolute. If False, they are made relative instead.
        Other args and kwargs are passed to the super class write method.
        """
        # make sure that everything is in absolute paths before writing
        if absolute:
            self.convert_to_absolute_paths()
        else:
            self.convert_to_relative_paths()
        super().write(*args, **kwargs)


class BaseInputFileHandlerNoInputVariables(
        BaseApprovableFileHandler, PathConvertibleFileHandler):
    """Base file handler for input files without input variables.
    """

    def move(self, *args, **kwargs):
        PathConvertibleFileHandler.move(self, *args, **kwargs)


class BaseInputFileHandler(BaseInputFileHandlerNoInputVariables):
    """Base file handler for input files with input variables.

    It is path convertible as some of the input variables could define
    paths.
    """

    def add_input_variables(self, input_variables):
        self.structure.add_input_variables(input_variables)

    def clear_input_variables(self):
        self.structure.clear_input_variables()

    def _convert_path(self, path, **kwargs):
        # override this method to take into account that convertible paths in
        # input files are stored in the 'input_variables' dictionaries
        # thus we must handle it as a whole
        if not isinstance(path, InputVariableDict):
            return super()._convert_path(path, **kwargs)
        newdict = path.copy()
        for var, value in newdict.items():
            if self.structure._variables_db[var].get(
                    "path_convertible", False) is True:
                newdict[var] = super()._convert_path(value.value, **kwargs)
        return newdict
