from .bases import BaseDirectoryHandler, BaseWritableDirectoryHandler


class GenericDirectory(BaseDirectoryHandler):
    """Class that represents a generic directory.
    """
    _loggername = "GenericDirectory"


class GenericWritableDirectory(BaseWritableDirectoryHandler):
    """Class that represents a generic writable directory.
    """
    _loggername = "GenericWritableDirectory"
