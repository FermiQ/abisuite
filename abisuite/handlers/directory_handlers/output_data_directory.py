from .bases import BaseWritableDirectoryHandler


# For now, this class is not too specialized.
class OutputDataDirectory(BaseWritableDirectoryHandler):
    """Class that represents an output data directory.
    """
    _loggername = "OutputDataDirectory"
