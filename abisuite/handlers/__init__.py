from .directory_handlers import (
        CalculationDirectory, CalculationTree,
        GenericDirectory,
        InputDataDirectory,
        OutputDataDirectory,
        RunDirectory,
        )
from .file_handlers import (
        AbinitAnaddbFilesFile, AbinitAnaddbInputFile, AbinitAnaddbLogFile,
        AbinitAnaddbPhfrqFile, AbinitCut3DInputFile, AbinitDMFTEigFile,
        AbinitDMFTProjectorsFile,
        AbinitEIGFile, AbinitFatbandFile, AbinitFilesFile, AbinitGSRFile,
        AbinitInputFile,
        AbinitLogFile, AbinitMrgddbInputFile,
        AbinitMrgddbLogFile, AbinitOpticFilesFile, AbinitOpticInputFile,
        AbinitOpticLincompFile,
        AbinitOutputFile, AbinitProcarFile,
        CALCTYPES_TO_INPUT_FILE_CLS,
        CALCTYPES_TO_LOG_FILE_CLS,
        GenericFile, GenericInputFile,
        MetaDataFile,
        PBSFile,
        PseudoFile,
        QEDOSDOSFile, QEDOSInputFile, QEDOSLogFile,
        QEDynmatInputFile,
        QEEpsilonInputFile,
        QEEPWa2FFile, QEEPWBandEigFile, QEEPWConductivityTensorFile,
        QEEPWInputFile,
        QEEPWLogFile, QEEPWPHBandFreqFile, QEEPWPhononSelfEnergyFile,
        QEEPWResistivityFile, QEEPWSpecFunPhonFile,
        QEFSInputFile,
        QELD1InputFile,
        QEMatdynDOSFile, QEMatdynEigFile, QEMatdynFreqFile, QEMatdynInputFile,
        QEMatdynLogFile,
        QEPHDyn0File, QEPHInputFile, QEPHLogFile,
        QEPPInputFile,
        QEProjwfcInputFile, QEProjwfcLogFile, QEProjwfcPDOSFile,
        QEPWInputFile, QEPWLogFile,
        QEPW2Wannier90InputFile, QEPW2Wannier90LogFile,
        QEQ2RInputFile, QEQ2RLogFile,
        SymLinkFile,
        Wannier90BandDatFile, Wannier90BandkptFile,
        Wannier90InputFile, Wannier90OutputFile,
        )


def is_calculation_directory(*args, **kwargs):
    """Returns True if a directory is a calculation directory. False otherwise.
    """
    return CalculationDirectory.is_calculation_directory(*args, **kwargs)
