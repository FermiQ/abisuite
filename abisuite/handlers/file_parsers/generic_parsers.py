from .bases import BaseStreamLineParser, BaseInputParser
from ..file_structures import GenericStructure, GenericInputStructure


class GenericParser(BaseStreamLineParser):
    """Generic parser for a generic file. Doesn't actually parse the file.
    """
    _loggername = "GenericParser"
    _expected_ending = ""
    _structure_class = GenericStructure

    def _extract_data(self):
        # nothing to do
        pass

    @classmethod
    def _filepath_from_meta(cls, *args, **kwargs):
        # just a generic file parser for a generic file
        raise NotImplementedError("Generic file parser.")


class GenericInputParser(BaseInputParser):
    """Generic Parser for an input file. It doesn't actually parse anything.

    Useful for tests cause it has all the properties of an actual input
    file parser.
    """
    _loggername = "GenericInputParser"
    _structure_class = GenericInputStructure

    def _extract_data_from_lines(self, *args, **kwargs):
        # do nothing and return one inpute var
        return {"input_variables": {"generic_variable": "generic_value"}}
