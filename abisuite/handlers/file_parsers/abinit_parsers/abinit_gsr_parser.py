from .bases import BaseAbinitNCParser
from ...file_structures import AbinitGSRStructure


class AbinitGSRParser(BaseAbinitNCParser):
    """Parser class for a '_GSR' file produced by Abinit.
    """
    _expected_ending = "_GSR"
    _loggername = "AbinitGSRParser"
    _structure_class = AbinitGSRStructure

    def _extract_data_from_lines(self, lines):
        raise NotImplementedError()

    def _extract_data_from_netcdf_dataset(self, dataset):
        return {"occupations": dataset.variables["occupations"][:]}
