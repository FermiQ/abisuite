from ..bases import BaseParser
from ...file_structures import AbinitCut3DInputStructure


class AbinitCut3DInputParser(BaseParser):
    """Parser class for the cut3d input file.
    """
    _expected_ending = ".in"
    _loggername = "AbinitCut3DInputParser"
    _structure_class = AbinitCut3DInputStructure

    def _extract_data_from_lines(self, lines):
        # first line is input data file path
        return {
            "data_file_path": lines[0],
            "option": int(lines[1]),
            "output_file_path": lines[2],
            "post_option": lines[3:],
            }

    def _clean_lines(self, lines):
        return [line.strip("\n") for line in lines]
