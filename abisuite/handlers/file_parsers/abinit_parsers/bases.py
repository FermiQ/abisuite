import abc
import os

from .subparsers import VariableSubParser
from ..bases import BaseParser, BaseInputParser


class BaseFilesParser(BaseParser, abc.ABC):
    """Base class for files file parsers.
    """
    _expected_ending = ".files"

    @classmethod
    def _filepath_from_meta(cls, meta):
        # files file usually in rundir
        return os.path.join(meta.rundir, meta.jobname + cls._expected_ending)

    def _clean_lines(self, lines):
        lines = [line.strip().strip("\n") for line in lines]
        lines = [line for line in lines
                 if not (line.startswith("#") or len(line) == 0)]
        return lines


class BaseAbinitDMFTParser(BaseParser):
    """Base class for an Abinit DMFT file parser.
    """

    @classmethod
    def _filepath_from_meta(cls, meta):
        out = meta.output_data_dir
        # look for an .eig file
        for outfile in os.listdir(out):
            if outfile.endswith(cls._expected_ending):
                return os.path.join(out, outfile)
        raise FileNotFoundError(f"Could not locate DMFT file in {out}")


class BaseAbinitInputParser(BaseInputParser):
    """Base class for some of the abinit input parser classes.
    """
    def _clean_lines(self, lines):
        clean = []
        for line in lines:
            if line.strip().startswith("#"):
                continue
            strip_nl = line.strip("\n")
            strip_spaces = strip_nl.strip(" ")
            if not strip_spaces:
                continue
            clean.append(strip_spaces)
        return clean

    def _extract_data_from_lines(self, lines):
        data = {}
        next_end = 0
        for i, line in enumerate(lines):
            if i < next_end:
                continue
            if VariableSubParser.variable_def_start_here(line):
                var = VariableSubParser(lines[i:],
                                        loglevel=self._logger.level)
                next_end += var.ending_relative_index
                data[var.name] = var.value
        return {"input_variables": data}


class BaseAbinitNCParser(BaseParser, abc.ABC):
    """Base parser class for abinit '.nc' files.

    A particularity of abinit is that some files can be written with netcdf or
    not (user's choice). We thus need to check if they are written that way
    or not. Files written by netcdf will be happended by the .nc extension.

    Note: the '_expected_ending' class attribute should not contain the '.nc'
          extention.
    """
    @classmethod
    def _filepath_from_meta(cls, meta, *args, **kwargs):
        # the EIG file is located inside the output data dir.
        for filename in os.listdir(meta.output_data_dir):
            if filename.endswith(cls._expected_ending) or filename.endswith(
                    cls._expected_ending + ".nc"):
                return os.path.join(meta.output_data_dir, filename)
        raise FileNotFoundError(
                f"Could not find the {cls._expected_ending} "
                f"file for '{meta.workdir}'.")

    def _extract_data(self):
        if self.path.endswith(self._expected_ending):
            return super()._extract_data()
        # else we need to use netcdf module
        self._set_attributes_from_data(
                self._extract_data_from_netcdf())

    def _extract_data_from_netcdf(self):
        try:
            from netCDF4 import Dataset
        except ImportError as e:
            self._logger.error(
                "netCDF4 module must be installed in order to read"
                "netCDF4 files. Please do so in order to continue.")
            self._logger.exception(e)
            raise e
        with Dataset(self.path, "r", format="NETCDF4") as dtset:
            return self._extract_data_from_netcdf_dataset(dtset)

    @abc.abstractmethod
    def _extract_data_from_netcdf_dataset(self, dataset):
        pass
