from ...bases import BaseSubParser
from .....routines import decompose_line


class AbinitLogOccSubParser(BaseSubParser):
    """Subparser that gets the occupations from the log file.
    """
    subject = "occ"
    trigger = "chkneu: initialized the occupation numbers"
    _loggername = "AbinitLogOccSubParser"

    def _extract_data_from_lines(self, lines):
        # data looks like this
        # chkneu: initialized the occupation numbers for occopt= 1, spin-unpolarized or antiferromagnetic case:  # noqa: E501
        # 2.00  2.00  2.00  2.00  2.00  2.00  2.00  2.00  2.00  2.00  0.00  0.00  # noqa: E501
        # 0.00  0.00
        occ = []
        for iline, line in enumerate(lines[1:]):
            s, i, f = decompose_line(line)
            if s:
                # end here
                self.ending_relative_index = iline + 1
                return occ
            occ += f
        # if we here reached the end of the file...
        raise LookupError("Occupations...")


class AbinitLogWalltimeSubParser(BaseSubParser):
    """Subparser for an Abinit log file (or out file) to get the walltime.
    """
    subject = "walltime"
    trigger = "Proc.   0 individual time (sec):"
    _loggername = "AbinitLogWalltimeSubParer"

    def _extract_data_from_lines(self, lines):
        s, i, f = decompose_line(lines[0])
        self.ending_relative_index = 0
        return f[-1]
