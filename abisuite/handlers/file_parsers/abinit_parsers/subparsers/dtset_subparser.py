from .eig_subparser import EIGSubParser
from .dtset_subparsers import (
        AbinitDtsetFermiEnergySubParser, AbinitDtsetPhononFrequenciesSubParser,
        )
from ...bases import BaseSubParser, BaseParserWithSubParsers
from ....file_structures import GenericStructure


# TODO: I felt long time ago that this was a good idea to make a subparser with
# subparsers. Perhaps this would need to be cleaned out to make it more like
# the other subparsers. FG 03/08/2020
class DtsetSubParser(BaseSubParser, BaseParserWithSubParsers):
    """Class that parses a dtset from an abinit output file.
    """
    _expected_ending = ""  # need to do this because of multiple inheritance
    _loggername = "DtsetSubParser"
    _subparsers = (
            EIGSubParser, AbinitDtsetPhononFrequenciesSubParser,
            AbinitDtsetFermiEnergySubParser,
            )
    can_be_recalled = True
    recalled_behavior = "append"
    subject = "dtsets"
    trigger = "== DATASET"
    _structure_class = GenericStructure  # since not directly a file

    def __init__(self, *args, **kwargs):
        BaseParserWithSubParsers.__init__(self, *args, **kwargs)
        BaseSubParser.__init__(self, *args, **kwargs)

    def _extract_data_from_lines(self, lines):
        # self._logger.debug("=== Parsing DATASET ===")
        if self.trigger in lines[0]:
            lines = lines[1:]
        return BaseParserWithSubParsers._extract_data_from_lines(self, lines)

    def _stop_parsing_at(self, line, index):
        if self.trigger in line or "== END DATASET(S)" in line:
            # dataset ends here
            self._ending_relative_index = index - 1
            return True
        return False

    @classmethod
    def from_string(cls, string, **kwargs):
        """Parses a dtset from a single string instead of a list of lines.
        """
        lines = string.split("\n")
        return cls(lines, **kwargs)

    @classmethod
    def _filepath_from_meta(cls, *args, **kwargs):
        # irrelevent here
        raise IOError("This parser cannot be instanciated with a meta file.")
