from .bases import BaseAbinitInputParser
from ...file_structures import AbinitInputStructure


class AbinitInputParser(BaseAbinitInputParser):
    """Parser for an abinit input file.
    """
    _loggername = "AbinitInputParser"
    _structure_class = AbinitInputStructure
