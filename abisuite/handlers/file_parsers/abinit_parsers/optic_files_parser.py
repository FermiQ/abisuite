from .bases import BaseFilesParser
from ...file_structures import AbinitOpticFilesStructure
import os


class AbinitOpticFilesParser(BaseFilesParser):
    """Parser for the optic files file.
    """
    _loggername = "AbinitOpticFilesParser"
    _structure_class = AbinitOpticFilesStructure

    def _extract_data_from_lines(self, lines):
        data = {}
        data["input_file_path"] = lines[0]
        data["output_file_path"] = lines[1]
        out_data_path = lines[2]
        data["output_data_dir"] = os.path.dirname(out_data_path)
        data["output_data_prefix"] = os.path.basename(out_data_path)
        return data
