import os

import numpy as np

from ..bases import BaseParser
from ...file_structures import AbinitAnaddbPhfrqStructure
from ....routines import decompose_line


class AbinitAnaddbPhfrqParser(BaseParser):
    """Parser class for an anaddb PHFRQ file.
    """
    _expected_ending = "PHFRQ"
    _loggername = "AbinitAnaddbPhfrqParser"
    _structure_class = AbinitAnaddbPhfrqStructure

    @classmethod
    def _filepath_from_meta(cls, meta, *args, **kwargs):
        # the PHFRQ file is located next to the batch file
        for filename in os.listdir(meta.rundir):
            if filename.endswith(cls._expected_ending):
                return os.path.join(meta.rundir, filename)
        # previously it was also found next to the input file.
        # try to locate it there as well if it is not found next to batch file
        for filename in os.listdir(meta.workdir):
            if filename.endswith(cls._expected_ending):
                return os.path.join(meta.workdir, filename)
        raise FileNotFoundError(
                f"Could not find PHFRQ file in '{meta.workdir}'.")

    def _extract_data_from_lines(self, lines):
        # data looks like this
        # # ABINIT generated phonon band structure file. All in Ha atomic units
        # #
        # # number_of_qpoints 71
        # # number_of_phonon_modes 6
        # #
        # 1    0.0000000000E+00    0.0000000000E+00    0.0000000000E+00    0.1568553720E-02    0.1568553720E-02    0.1568553720E-02  # noqa: E501
        # 2    0.7795285041E-04    0.9980229125E-04    0.1577695327E-03    0.1562165714E-02    0.1567661756E-02    0.1724378844E-02  # noqa: E501
        # 3    0.1510315695E-03    0.1997060281E-03    0.3074081400E-03    0.1546884050E-02    0.1564208088E-02    0.1706321336E-02  # noqa: E501
        # 4    0.2159182044E-03    0.2972208589E-03    0.4421728371E-03    0.1531171075E-02    0.1556374316E-02    0.1676684528E-02  # noqa: E501
        # 5    0.2717020496E-03    0.3869205466E-03    0.5580035748E-03    0.1520602645E-02    0.1542537905E-02    0.1638665645E-02  # noqa: E501
        # 6    0.3194810969E-03    0.4630448988E-03    0.6538201033E-03    0.1510670393E-02    0.1522530166E-02    0.1600929727E-02  # noqa: E501
        # 7    0.3607518540E-03    0.5231522870E-03    0.7299392460E-03    0.1488838330E-02    0.1498259413E-02    0.1576447318E-02  # noqa: E501
        # 8    0.3956083917E-03    0.5691329398E-03    0.7861710274E-03    0.1457986662E-02    0.1473367444E-02    0.1564636285E-02  # noqa: E501
        # ...
        data = {}
        data.update(self._extract_header(lines))
        data["frequencies"] = self._extract_frequencies(lines)
        # make some checkes
        assert data["frequencies"].shape == (data["nqpts"], data["nbranches"])
        return data

    def _extract_frequencies(self, lines):
        # skip first column as it is just the qpt index
        return np.loadtxt(lines)[:, 1:]

    def _extract_header(self, lines):
        data = {}
        for line in lines:
            if "number_of_qpoints" in line:
                s, i, f = decompose_line(line)
                data["nqpts"] = i[0]
                continue
            elif "number_of_phonon_modes" in line:
                s, i, f = decompose_line(line)
                data["nbranches"] = i[0]
                continue
            elif not line.startswith("#"):
                # return what we have
                return data
        raise LookupError()
