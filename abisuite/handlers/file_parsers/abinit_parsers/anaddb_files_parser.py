from .bases import BaseFilesParser
from ...file_structures import AbinitAnaddbFilesStructure


class AbinitAnaddbFilesParser(BaseFilesParser):
    """Parser that parses an Abinit anaddb files file.
    """
    _loggername = "AbinitAnaddbFilesParser"
    _structure_class = AbinitAnaddbFilesStructure

    def _extract_data_from_lines(self, lines):
        data = {}
        data["input_file_path"] = lines[0]
        data["output_file_path"] = lines[1]
        data["ddb_file_path"] = lines[2]
        data["band_structure_file_path"] = lines[3]
        data["gkk_file_path"] = lines[4]
        data["eph_data_prefix"] = lines[5]
        data["ddk_file_path"] = lines[6]
        return data
