import os

import numpy as np

from ..bases import BaseStreamLineParser
from ...file_structures import AbinitProcarStructure
from ....linux_tools import cat
from ....routines import decompose_line


class AbinitProcarParser(BaseStreamLineParser):
    """Parser class for the abinit PROCAR files.
    """
    _expected_ending = ""
    _loggername = "AbinitProcarParser"
    _structure_class = AbinitProcarStructure

    def _extract_data(self):
        # data looks like this:
        # # of k-points:         61 # of bands:         8 # of ions:         2
        #
        #  k-point       1 :     0.500000    0.000000    0.000000 weight =     1.000000  # noqa
        #
        # band       1 # energy    -0.177786 # occ.     2.000000
        #
        # ion       s      py     pz     px    dxy    dyz    dz2    dxz    dx2    tot   # noqa
        #      1  0.231  0.005  0.005  0.005  0.001  0.001  0.000  0.001  0.001  0.249  # noqa
        #      2  0.231  0.005  0.005  0.005  0.001  0.001  0.000  0.001  0.001  0.249  # noqa
        # tot     0.461  0.010  0.010  0.010  0.001  0.003  0.000  0.003  0.001  0.499  # noqa
        # band       2 # energy    -0.081197 # occ.     2.000000
        #
        # ion       s      py     pz     px    dxy    dyz    dz2    dxz    dx2    tot    # noqa
        #      1  0.098  0.033  0.033  0.033  0.000  0.000  0.000  0.000  0.000  0.197  # noqa
        #      2  0.098  0.033  0.033  0.033  0.000  0.000  0.000  0.000  0.000  0.197  # noqa
        # tot     0.196  0.066  0.066  0.066  0.000  0.001  0.000  0.001  0.000  0.395  # noqa
        # band       3 # energy     0.134617 # occ.     2.000000
        #
        # ion       s      py     pz     px    dxy    dyz    dz2    dxz    dx2    tot    # noqa
        #      1  0.000  0.113  0.000  0.113  0.001  0.006  0.000  0.006  0.001  0.241  # noqa
        #      2  0.000  0.113  0.000  0.113  0.001  0.006  0.000  0.006  0.001  0.241  # noqa
        # tot     0.000  0.226  0.000  0.226  0.002  0.012  0.000  0.012  0.002  0.482  # noqa
        # band       4 # energy     0.134617 # occ.     2.000000

        # I want to return a character array which will have a shape of
        # nbands x nkpts x natoms x norbitals
        characters = []
        eigenvalues = []
        with open(self.path, "r") as f:
            lines = [line.rstrip("\n").strip(" ") for line in f.readlines()]
        skip = 0
        for iline, line in enumerate(lines):
            if iline < skip:
                continue
            if "k-point" in line:
                (kpoint_block, eigs,
                 relative_ending) = self._extract_kpoint_block(
                        lines[iline:])
                skip += relative_ending
                # kpoint_block is an array of shape nband x natoms x norbital
                # eigs is a nband vector
                eigenvalues.append(eigs)
                # we will transpose at the end
                characters.append(kpoint_block)
        self.nkpts = len(characters)
        self.characters = np.array(characters).transpose((1, 0, 2, 3))
        self.nbands = len(characters)
        # we set eigenvalues to be nband x nkpts
        self.eigenvalues = np.array(eigenvalues).T

    def _extract_kpoint_block(self, lines):
        # a kpoint block looks like this:
        #  k-point       1 :     0.500000    0.000000    0.000000 weight =     1.000000  # noqa
        #
        # band       1 # energy    -0.177786 # occ.     2.000000
        #
        # ion       s      py     pz     px    dxy    dyz    dz2    dxz    dx2    tot    # noqa
        #      1  0.231  0.005  0.005  0.005  0.001  0.001  0.000  0.001  0.001  0.249  # noqa
        #      2  0.231  0.005  0.005  0.005  0.001  0.001  0.000  0.001  0.001  0.249  # noqa
        # tot     0.461  0.010  0.010  0.010  0.001  0.003  0.000  0.003  0.001  0.499  # noqa
        # band       2 # energy    -0.081197 # occ.     2.000000
        #
        # ion       s      py     pz     px    dxy    dyz    dz2    dxz    dx2    tot    # noqa
        #      1  0.098  0.033  0.033  0.033  0.000  0.000  0.000  0.000  0.000  0.197  # noqa
        #      2  0.098  0.033  0.033  0.033  0.000  0.000  0.000  0.000  0.000  0.197  # noqa
        # tot     0.196  0.066  0.066  0.066  0.000  0.001  0.000  0.001  0.000  0.395  # noqa
        # band       3 # energy     0.134617 # occ.     2.000000  # noqa
        #
        # ion       s      py     pz     px    dxy    dyz    dz2    dxz    dx2    tot    # noqa
        #      1  0.000  0.113  0.000  0.113  0.001  0.006  0.000  0.006  0.001  0.241  # noqa
        #      2  0.000  0.113  0.000  0.113  0.001  0.006  0.000  0.006  0.001  0.241  # noqa
        # tot     0.000  0.226  0.000  0.226  0.002  0.012  0.000  0.012  0.002  0.482  # noqa
        # band       4 # energy     0.134617 # occ.     2.000000
        # ...
        #  k-point       2 :     0.475000    0.000000    0.000000 weight =     1.000000  # noqa
        data_chars = []
        eigs = []
        skip = 0
        for iline, line in enumerate(lines):
            if iline < skip:
                continue
            if "k-point" in line and len(data_chars) > 0:
                return data_chars, eigs, iline
            if "band" in line:
                # band block starting here
                band_block, eig, relative_end = self._extract_band_block(
                        lines[iline:])
                skip += relative_end
                data_chars.append(band_block)
                eigs.append(eig)
        # if we're here we reached the end of file
        if not eigs or not data_chars:
            raise LookupError()
        return data_chars, eigs, iline

    def _extract_band_block(self, lines):
        # a band block looks like this
        # band       1 # energy    -0.177786 # occ.     2.000000
        #
        # ion       s      py     pz     px    dxy    dyz    dz2    dxz    dx2    tot    # noqa
        #      1  0.231  0.005  0.005  0.005  0.001  0.001  0.000  0.001  0.001  0.249  # noqa
        #      2  0.231  0.005  0.005  0.005  0.001  0.001  0.000  0.001  0.001  0.249  # noqa
        # tot     0.461  0.010  0.010  0.010  0.001  0.003  0.000  0.003  0.001  0.499  # noqa
        # band       2 # energy    -0.081197 # occ.     2.000000
        data_chars = []
        eig = None
        for iline, line in enumerate(lines):
            if "band " in line:
                if eig is not None:
                    return data_chars, eig, iline - 1
                else:
                    s, i, f = decompose_line(line)
                    eig = f[0]
                    continue
            if "ion" in line or "tot" in line or not line or "k-point" in line:
                continue
            # if we're here, we're in a characters line
            s, i, f = decompose_line(line)
            # there are 10 floats, take only the 9 firsts since last is tot
            data_chars.append(f[:-1])
        # if we're here we reached end of file
        if eig is None:
            raise LookupError()
        return data_chars, eig, iline - 1

    @classmethod
    def _filepath_from_meta(cls, meta, *args, **kwargs):
        # PROCAR files are located besides the PBS file and named PROCAR_XXXX
        # where XXXX is the cpu number. Thus we need to merge them before
        # parsing them.
        procar_files = []
        for path in os.listdir(meta.rundir):
            if "PROCAR" in path:
                procar_files.append(os.path.join(meta.rundir, path))
            if path == "PROCAR_COMBINED":
                # already combined
                return os.path.join(meta.rundir, path)
        if len(procar_files) > 1:
            # merge them into a single file called PROCAR_COMBINED
            cat(*sorted(procar_files),
                pipe=os.path.join(meta.rundir, "PROCAR_COMBINED"))
            return os.path.join(meta.rundir, "PROCAR_COMBINED")
        return procar_files[0]
