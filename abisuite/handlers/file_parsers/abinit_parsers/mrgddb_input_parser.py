from ..bases import BaseInputParserNoInputVariables
from ...file_structures import AbinitMrgddbInputStructure


class AbinitMrgddbInputParser(BaseInputParserNoInputVariables):
    """Parser class for a mrgddb input file.
    """
    _loggername = "AbinitMrgddbInputParser"
    _structure_class = AbinitMrgddbInputStructure

    def _extract_data_from_lines(self, lines):
        data = {}
        data["output_file_path"] = lines[0]
        data["title"] = lines[1]
        data["nddb"] = int(lines[2])
        data["ddb_paths"] = lines[3:]
        return data
