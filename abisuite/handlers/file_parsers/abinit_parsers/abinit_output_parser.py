import os

from .subparsers import (
        AbinitLogWalltimeSubParser, DtsetSubParser, HeaderParser,
        FooterParser,
        )
from ..bases import BaseParserWithSubParsers
from ...file_structures import AbinitOutputStructure


class AbinitOutputParser(BaseParserWithSubParsers):
    """An ABINIT output file parser that gets data from an output file.

    Parameters
    ----------
    filepath : str
               The path to the output file.
    """
    _expected_ending = ".out"
    _loggername = "AbinitOutputParser"
    _structure_class = AbinitOutputStructure
    _subparsers = (
            DtsetSubParser, HeaderParser, FooterParser,
            AbinitLogWalltimeSubParser,
            )

    @classmethod
    def _filepath_from_meta(cls, meta):
        # usually, output file in workdir
        return os.path.join(meta.calc_workdir,
                            meta.jobname + cls._expected_ending)
