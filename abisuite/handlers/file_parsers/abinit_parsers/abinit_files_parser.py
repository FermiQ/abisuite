import os

from .bases import BaseFilesParser
from ...file_structures import AbinitFilesStructure


class AbinitFilesParser(BaseFilesParser):
    """Parser that parses an ABINIT files file.
    """
    _loggername = "AbinitFilesParser"
    _structure_class = AbinitFilesStructure

    def _extract_data_from_lines(self, lines):
        data = {}
        data["input_file_path"] = lines[0]
        data["output_file_path"] = lines[1]
        input_data_path = lines[2]
        data["input_data_prefix"] = os.path.basename(input_data_path)
        data["input_data_dir"] = os.path.dirname(input_data_path)
        output_data_path = lines[3]
        data["output_data_prefix"] = os.path.basename(output_data_path)
        data["output_data_dir"] = os.path.dirname(output_data_path)
        tmp_data_path = lines[4]
        data["tmp_data_prefix"] = os.path.basename(tmp_data_path)
        data["tmp_data_dir"] = os.path.dirname(tmp_data_path)
        data["pseudos"] = tuple([x for x in lines[5:]])
        return data
