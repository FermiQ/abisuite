import os

from ..bases import BaseParser
from ...file_structures import Wannier90BandDatStructure
from ....routines import decompose_line


class Wannier90BandDatParser(BaseParser):
    """Parser class for a seedname_band.dat file produced by wannier90 when
    bands_plot=True.
    """
    _expected_ending = "_band.dat"
    _loggername = "Wannier90BandDatParser"
    _structure_class = Wannier90BandDatStructure

    def _clean_lines(self, lines):
        return [line.strip("\n").strip() for line in lines]

    def _extract_data_from_lines(self, lines):
        # the file is just two columns of floats.
        # I don't know what the first float mean but the second one looks like
        # a band eigenvalue. The two columns are split after nkpts by an empty
        # line.
        bands = []
        band = []  # first band
        for line in lines:
            if not len(line):
                # empty line found => a new band starts
                # append the band
                bands.append(band)
                # start a new band list
                band = []
                continue
            s, i, f = decompose_line(line)
            band.append(f[-1])
        else:
            # append the last band if the file does not end with an empty line
            if len(band):
                bands.append(band)
        # make some checks
        nkpts = len(bands[0])
        for band in bands:
            if len(band) != nkpts:
                raise AssertionError("Not all bands have same nb of kpts")
        return {"nkpt": nkpts, "bands": bands}

    @classmethod
    def _filepath_from_meta(cls, meta, *args, **kwargs):
        # this file is located next to the '.wout' file in main calc dir.
        return os.path.join(
                meta.calc_workdir, meta.jobname + cls._expected_ending)
