import os

from .subparsers.wannier90_output_subparsers import (
        Wannier90OutputTimingSubParser,
        )
from ..bases import BaseParserWithSubParsers
from ...file_structures import Wannier90OutputStructure


class Wannier90OutputParser(BaseParserWithSubParsers):
    """Parser class for a wannier90 output file.
    """
    _expected_ending = ".wout"
    _loggername = "Wannier90OutputParser"
    _structure_class = Wannier90OutputStructure
    _subparsers = (Wannier90OutputTimingSubParser, )

    @classmethod
    def _filepath_from_meta(cls, meta):
        # output file = meta.jobname + _expected_eding
        return os.path.join(
                meta.calc_workdir,
                meta.jobname + cls._expected_ending)
