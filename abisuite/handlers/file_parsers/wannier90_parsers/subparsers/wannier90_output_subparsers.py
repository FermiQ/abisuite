from ...bases import BaseSubParser
from .....routines import decompose_line


class Wannier90OutputTimingSubParser(BaseSubParser):
    """Subparser class that parses the timing from the 'wout' file produced
    by the wannier90.x script.
    """
    subject = "walltime"
    _loggername = "Wannier90OutputTimingSubParser"
    trigger = "Total Execution Time"

    def _extract_data_from_lines(self, lines):
        # only one line e.g.:
        # Total Execution Time         121.441 (sec)
        s, i, f = decompose_line(lines[0])
        if not len(f) == 1:
            raise LookupError(f"Could not extract walltime from : {lines[0]}")
        self.ending_relative_index = 1
        return f[0]
