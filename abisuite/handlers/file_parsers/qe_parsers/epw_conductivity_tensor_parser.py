import numpy as np
import os

from .bases import BaseQEParser
from ...file_structures import QEEPWConductivityTensorStructure


class QEEPWConductivityTensorParser(BaseQEParser):
    """Parser for a conductivity tensor file produced by the epw.x script
    of Quantum Espresso.
    """
    _expected_ending = ""
    _loggername = "QEEPWConductivityTensorParser"
    _structure_class = QEEPWConductivityTensorStructure

    def _extract_data_from_lines(self, lines):
        data = np.loadtxt(lines)
        chemical_potentials = data[:, 0]
        temperatures = data[:, 1]
        # sigma is a 3x3 tensor. 1 tensor for each temperature
        # thus self.sigmas is a Ntemp x 3 x 3 array
        conductivity_tensor = np.zeros((len(self.temperatures), 3, 3))
        conductivity_tensor[:, 0, 0] = data[:, 2]  # xx
        conductivity_tensor[:, 0, 1] = data[:, 3]  # xy
        conductivity_tensor[:, 0, 2] = data[:, 4]  # xz
        conductivity_tensor[:, 1, 0] = data[:, 5]  # yx
        conductivity_tensor[:, 1, 1] = data[:, 6]  # yy
        conductivity_tensor[:, 1, 2] = data[:, 7]  # yz
        conductivity_tensor[:, 2, 0] = data[:, 8]  # zx
        conductivity_tensor[:, 2, 1] = data[:, 8]  # zy
        conductivity_tensor[:, 2, 2] = data[:, 10]  # zz
        return {"temperatures": temperatures,
                "chemical_potentials": chemical_potentials,
                "conductivity_tensor": conductivity_tensor,
                }

    @classmethod
    def _filepath_from_meta(cls, meta, *args, **kwargs):
        # there is two file: one for holes and one for electrons
        # both are in the run dir which is next to where the epw.x executable
        # has been called
        rd = meta.rundir
        paths = {}
        for path in os.listdir(rd):
            if path.endswith("_elcond_e"):
                paths["electrons"] = os.path.join(rd, path)
                continue
            elif path.endswith("_elcond_h"):
                paths["holes"] = os.path.join(rd, path)
                continue
        return paths
