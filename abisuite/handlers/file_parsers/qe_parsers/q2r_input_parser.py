from .bases import BaseQEInputParser
from ...file_structures import QEQ2RInputStructure


class QEQ2RInputParser(BaseQEInputParser):
    """Class that can parse a Quantum Espresso q2r.x input File.
    """
    _loggername = "QEQ2RInputParser"
    _structure_class = QEQ2RInputStructure
