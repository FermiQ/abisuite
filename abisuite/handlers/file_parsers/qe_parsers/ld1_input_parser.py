from .bases import BaseQEInputParser
from ...file_structures import QELD1InputStructure


class QELD1InputParser(BaseQEInputParser):
    """Class that can parse a Quantum Espresso ld1.x input File.
    """
    _loggername = "QELD1InputParser"
    _structure_class = QELD1InputStructure
