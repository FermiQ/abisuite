from .bases import BaseQEInputParser
from ...file_structures import QEEpsilonInputStructure


class QEEpsilonInputParser(BaseQEInputParser):
    """Class that can parse a Quantum Espresso epsilon.x input File.
    """
    _loggername = "QEEpsilonInputParser"
    _structure_class = QEEpsilonInputStructure
