import os

import numpy as np

from .bases import BaseQEParser
from ...file_structures import QEEPWSpecFunPhonStructure


class QEEPWSpecFunPhonParser(BaseQEParser):
    """Parser for a phonon spectral function file produced by the epw.x script
    from Quantum Espresso.
    """
    _expected_ending = "specfun.phon"
    _loggername = "QEEPWSpecFunPhonParser"
    _structure_class = QEEPWSpecFunPhonStructure

    def _extract_data_from_lines(self, lines):
        data = np.loadtxt(lines)
        # reshape data into a nqpt x nfreq matrix
        # for that we need to get the number of freq and qpts
        # first column of data is the qpt indices
        nfreqs = len(np.where(data[:, 0] == 1)[0])
        # for number of qpts, get first frequency
        first_freq = data[0, 1]
        nqpts = len(np.where(data[:, 1] == first_freq)[0])
        energies = data[:nfreqs, 1]
        # just keep spectral function
        data = data[:, 2]
        spectral_function = np.reshape(data, (nqpts, nfreqs))
        return {"energies": energies, "spectral_function": spectral_function}

    @classmethod
    def _filepath_from_meta(cls, meta, *args, **kwargs):
        # this file is written in the run directory where the batch file is
        # where the epw.x script has been called
        with meta:
            rd = meta.rundir
        return os.path.join(rd, "specfun.phon")
