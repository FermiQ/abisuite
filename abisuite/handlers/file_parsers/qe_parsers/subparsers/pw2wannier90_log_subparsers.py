from .bases import BaseQELogTimingSubParser


class QEPW2Wannier90LogTimingSubParser(BaseQELogTimingSubParser):
    _loggername = "QEPW2Wannier90LogTimingSubParser"
    trigger = "PW2WANNIER   :"
