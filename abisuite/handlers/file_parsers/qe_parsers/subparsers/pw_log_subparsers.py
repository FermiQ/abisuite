from .bases import BaseQELogTimingSubParser
from ...bases import BaseSubParser
from .....routines import decompose_line


class QEPWLogCrystalAxesSubParser(BaseSubParser):
    """Subparser to extract the crystal axes (primitive vectors) from the
    pw.x log file of Quantum Espresso.
    """
    _loggername = "QEPWLogCrystalAxesSubParser"
    subject = "crystal_axes"
    trigger = "crystal axes:"

    def _extract_data_from_lines(self, lines):
        data = []
        for j, line in enumerate(lines[1:]):
            if "a(" not in line:
                self.ending_relative_index = j + 1
                return data
            s, i, f = decompose_line(line)
            data.append(f)
        else:
            raise LookupError("Could not find the end of 'crystal_axes'.")


class QEPWLogLatticeParametersSubParser(BaseSubParser):
    """Subparser that extract the lattice parameters from the pw.x log
    file produced by Quantum Espresso.
    """
    _loggername = "QEPWLogLatticeParametersSubParser"
    subject = "lattice_parameters"
    trigger = "celldm(1)"

    def _extract_data_from_lines(self, lines):
        # celldm(1)=   7.518450  celldm(2)=   0.000000  celldm(3)=   0.000000
        # celldm(4)=   0.000000  celldm(5)=   0.000000  celldm(6)=   0.000000
        data = {}
        for j, line in enumerate(lines):
            if "celldm(" not in line:
                self.ending_relative_index = j
                return data
            s, i, f = decompose_line(line)
            celldms = [x.split("=")[0] for x in s]
            data.update({celldm: value for celldm, value in zip(celldms, f)})
        else:
            raise LookupError("Could not find the end of the lattice "
                              "parameters subsection of log file.")


class QEPWLogPressureStressSubParser(BaseSubParser):
    """Subparser used to parse the pressure and stress tensor
    from the pw.x log file produces by Quantum Espresso.
    """
    _loggername = "QEPWLogPressureSubParser"
    subject = ("pressure", "stress_tensor")
    trigger = "Computing stress (Cartesian axis) and pressure"
    can_be_recalled = True
    recalled_behavior = "overwrite"

    def _extract_data_from_lines(self, lines):
        #     Computing stress (Cartesian axis) and pressure
        #
        #       total   stress  (Ry/bohr**3)                   (kbar)     P=   -0.02  # noqa
        #   -0.00000014   0.00000000   0.00000000         -0.02      0.00      0.00   # noqa
        #    0.00000000  -0.00000014  -0.00000000          0.00     -0.02     -0.00   # noqa
        #    0.00000000  -0.00000000  -0.00000014          0.00     -0.00     -0.02   # noqa
        data = {"stress_tensor": []}
        for iline, line in enumerate(lines):
            s, i, f = decompose_line(line)
            if not f:
                # no data here
                continue
            if len(f) == 1:
                # global pressure
                data["pressure"] = f[0]
                continue
            if len(f) == 6:
                # append stress tensor
                data["stress_tensor"].append(f[:3])
            if len(data["stress_tensor"]) == 3:
                # we're done
                self.ending_relative_index = iline
                return data
        raise LookupError()


class ParallelizationSubParser(BaseSubParser):
    """Subparser for the parallelization info section.
    """
    _loggername = "ParallelizationSubParser"
    subject = "parallelization"
    trigger = "Parallelization info"

    def _extract_data_from_lines(self, lines):
        data = {}
        for j, line in enumerate(lines):
            if "Sum" in line:
                s, i, f = decompose_line(line)
                data["pw"] = i[-1]
                self.ending_relative_index = j
                break
        return data


class EnergySubParser(BaseSubParser):
    """Subparser for the energy info.
    """
    _loggername = "EnergySubParser"
    subject = "total_energy"  # extract only one energy for now
    trigger = "!"

    def _extract_data_from_lines(self, lines):
        # only one line for now
        self.ending_relative_index = 0
        s, i, f = decompose_line(lines[0])
        return f[0]


class QEPWLogFinalAtomicPositionsSubParser(BaseSubParser):
    """Subparser for the final atomic positions. We say final cause
    this subparser can be recalled if in a 'relax' mode.
    """
    can_be_recalled = True
    recalled_behavior = "overwrite"
    _loggername = "FinalGeometrySubParser"
    subject = "final_atomic_positions"
    trigger = "ATOMIC_POSITIONS"

    def _extract_data_from_lines(self, lines):
        # ATOMIC_POSITIONS (crystal)
        # Sr       0.000000000   0.353760507   0.353760507
        # Sr       0.000000000   0.646239493   0.646239493
        # Ru       0.000000000   0.000000000   0.000000000
        # O        0.500000000   0.000000000  -0.500000000
        # O        0.000000000   0.167036181   0.167036181
        # O       -0.500000000   0.500000000  -0.000000000
        # O        0.000000000   0.832963819   0.832963819
        data = {}
        for j, line in enumerate(lines):
            if j == 0:
                # skip title line
                continue
            s, i, f = decompose_line(line)
            if not len(s) or len(f) != 3:
                # end of data block
                self.ending_relative_index = j
                return data
            atom = s[0]
            if atom not in data:
                data[atom] = [f]
                continue
            data[atom].append(f)
        else:
            # reached end of file? return what we got
            self.ending_relative_index = j
            return data


class QEPWLogFinalGeometrySubParser(BaseSubParser):
    """Subparser for the final cell geometry. We say final
    but the subparser can be recalled if in a 'vc-relax' mode.
    In this case just take the last one.
    """
    can_be_recalled = True
    recalled_behavior = "overwrite"
    _loggername = "QEPWLogFinaleGeometrySubParser"
    subject = "final_geometry"
    trigger = "CELL_PARAMETERS"

    def _extract_data_from_lines(self, lines):
        # CELL_PARAMETERS (alat=  9.22255838)
        # -0.499356312   0.000000000   0.499356312
        # 0.000000000   0.499356312   0.499356312
        # -0.499356312   0.499356312   0.000000000
        data = {"primitive_vectors": []}
        for iline, line in enumerate(lines):
            if "alat" in line:
                data["alat"] = float(line.split(")")[0].split("=")[-1])
                continue
            s, i, f = decompose_line(line)
            data["primitive_vectors"].append(f)
            if len(data["primitive_vectors"]) == 3:
                self.ending_relative_index = iline
                return data
        raise LookupError()


class QEPWLogInputParametersSubParser(BaseSubParser):
    """SubParser for some input parameters.
    """
    _loggername = "QEPWLogInputParametersSubParser"
    subject = "input_variables"
    trigger = "bravais-lattice"

    def _extract_data_from_lines(self, lines):
        data = {}
        for j, line in enumerate(lines):
            if "number of Kohn-Sham states" in line:
                s, i, f = decompose_line(line)
                data["nbnd"] = i[0]
                continue
            if "kinetic-energy cutoff" in line:
                s, i, f = decompose_line(line)
                data["ecutwfc"] = f[0]
                continue
            if "number of atoms/cell" in line:
                s, i, f = decompose_line(line)
                data["nat"] = i[0]
                continue
            if "volume" in line:
                s, i, f = decompose_line(line)
                data["volume"] = f[0]
                continue
            if "Exchange-correlation" in line:
                # stop here
                self.ending_relative_index = j
                return data
        raise LookupError("Couldn't find end of Input parameter section.")


class QEPWLogTimingSubParser(BaseQELogTimingSubParser):
    """Subparser that parses time values.
    """
    _loggername = "QEPWLogTimingSubParser"
    trigger = "PWSCF        :"


class KGridSubParser(BaseSubParser):
    """Subparser that parses kgrids information.
    """
    _loggername = "KGridSubparser"
    subject = "k_points"
    trigger = "number of k points"

    def _extract_data_from_lines(self, lines):
        # for now only one data to extract
        self.ending_relative_index = 0
        s, i, f = decompose_line(lines[0])
        data = {"nkpts": i[0]}
        if "smearing" in lines[0]:
            # also smearing information e.g.:
            #  number of k points= 54  gaussian smearing, width (Ry)=  0.0001
            data["smearing"] = s[4]
            data["degauss"] = f[0]
        return data


class EigenvaluesSubParser(BaseSubParser):
    """Subparser that gets the eigenvalues at each kpts.
    """
    _loggername = "EigenvaluesSubParser"
    subject = "eigenvalues"
    trigger = "bands (ev)"

    def _extract_data_from_lines(self, lines):
        # data is organized as follows:
        #           k = 0.5000 0.5000 0.5000 (   568 PWs)   bands (ev):
        #
        #    -3.4470  -0.8438   4.9915   4.9915   7.7615   9.5415   9.5415  13.7948  # noqa
        # watchout there can be weird formatting like:
        # k =-0.0750-0.0750 0.0750
        coords = []
        eigs = []
        skip = 0
        for j, line in enumerate(lines):
            if j < skip:
                continue
            if self.trigger in line:
                # a kpts is defined here, watch out for particular cases
                coord = self._extract_kpt_coords(line)
                coords.append(coord)
                continue
            s, i, f = decompose_line(line)
            if not len(f) and not len(s):
                # no floats here, probably empty line
                continue
            if len(s):
                # words are written here so we must be at the end
                self.ending_relative_index = j
                break
            # eigenvalues are defined on the following lines
            eigs_this_kpt, nextblock = self._get_eigs_this_kpt(lines[j:])
            eigs.append(eigs_this_kpt)
            skip = j + nextblock
        else:
            raise LookupError("Reached end of file and wasn't suppose to...")
        # make some checks
        nks = len(coords)
        neigs = len(eigs)
        if nks != neigs:
            raise LookupError(f"nkpts ({nks}) != neigs ({neigs})")
        return {"k_points": coords,
                "eigenvalues": eigs}

    def _extract_kpt_coords(self, line):
        k_coord_region = line.split("=")[1].split("(")[0]
        if "-" in k_coord_region:
            # add a space before each '-' to make sure everything is ok
            newstr = ""
            for char in k_coord_region:
                if char == "-":
                    newstr += " -"
                else:
                    newstr += char
            k_coord_region = newstr
        s, i, f = decompose_line(k_coord_region)
        return f

    def _get_eigs_this_kpt(self, sublines):
        # get eigs for a block of eigs and return when a new block starts
        eigs = []
        for j, line in enumerate(sublines):
            if self.trigger in line:
                # new block starts here, return what we got
                return eigs, j
            s, i, f = decompose_line(line)
            if len(s):
                # words are written here so we must be at the end
                # or in an 'occupation numbers' block.
                if "occupation" not in s:
                    # no occupation block, return what we got
                    # return j-1 as next skip in order for the upper loop
                    # to see the end too
                    return eigs, j - 1
                # if we are here there is an 'occupation' block for some reason
                # we need to return a relative end which is further in order
                # to skip it
                for o, occline in enumerate(sublines[j:]):
                    s, i, f = decompose_line(occline)
                    if not len(f) and not len(s):
                        # found first empty line after the occ block, return
                        return eigs, j + o
                else:
                    # end of file????
                    raise LookupError("What happened?"
                                      " Cannot extract eigs block...")
            eigs += f
        else:
            raise LookupError("What happened? cannot extract eigs block...")


class AtomicPositionsSubParser(BaseSubParser):
    """Subparser that parses atomic positions.
    """
    _loggername = "AtomicPositionsSubParser"
    subject = "atomic_positions"
    trigger = "Cartesian axes"

    def _extract_data_from_lines(self, lines):
        data = {}
        for index, line in enumerate(lines):
            if "number of k points" in line:
                # reached the end
                self.ending_relative_index = index - 1
                return data
            if "tau" in line:
                atom, position = self._get_atom_pos(line)
                if atom not in data:
                    data[atom] = []
                data[atom].append(position)
                continue

    def _get_atom_pos(self, line):
        # line look like this:
        # 1           Cl  tau(   1) = (   0.0000000   0.0000000   0.0000000  )
        s, i, f = decompose_line(line)
        atom = s[0]
        pos = f[-3:]
        return atom, pos


class QEPWLogFermiEnergySubParser(BaseSubParser):
    """Subparser that parses the Fermi energy (or highest occupied level).
    """
    _loggername = "QEPWLogFermiEnergySubParser"
    subject = "fermi_energy"
    trigger = ("the Fermi energy is", "highest occupied level")

    def _extract_data_from_lines(self, lines):
        # for now only one data to extract
        self.ending_relative_index = 0
        s, i, f = decompose_line(lines[0])
        return f[0]
