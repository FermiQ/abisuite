from .bases import BaseQELogTimingSubParser
from ...bases import BaseSubParser
from .....routines import decompose_line


class QEPHLogNPhononsSubParser(BaseSubParser):
    """PH log subparser that gets the number of phonons in the qpoint grid.
    (when there is a phonon qpoint grid).
    """
    can_be_recalled = False
    subject = "nphonons"
    trigger = "Dynamical matrices"
    _loggername = "QEPHNPhononsSubParser"

    def _extract_data_from_lines(self, lines):
        # data looks like this:
        # Dynamical matrices for ( 2, 2, 2)  uniform grid of q-points
        # (   3 q-points):
        #   N         xq(1)         xq(2)         xq(3)
        #   1   0.000000000   0.000000000   0.000000000
        #   2   0.500000000  -0.500000000   0.500000000
        #   3   0.000000000  -1.000000000   0.000000000
        s, i, f = decompose_line(lines[1])
        self.ending_relative_index = 1
        return i[0]


class QEPHLogPhononFreqSubParser(BaseSubParser):
    """Subparser that can get the phonon frequencies written in a ph.x
    log file.
    """
    subject = "phonon_frequencies"
    trigger = "Diagonalizing"
    _loggername = "QEPHLogPhononFreqSubParser"

    def _extract_data_from_lines(self, lines):
        data = []
        # put end at 0 since the freq block appears for each qpoint
        # thus parse all freqs for each qpts at once
        self.ending_relative_index = 0
        blockstart = 0
        blockend = None
        starlinepassed = False
        for index, line in enumerate(lines):
            if self.trigger in line:
                # data block starts here
                blockstart = index
                continue
            if "*****" in line:
                if not starlinepassed:
                    starlinepassed = True
                    continue
                # end of block
                blockend = index
                data.append(self._parse_freq_block(lines[blockstart:blockend]))
                # reset flags
                starlinepassed = False
                blockstart = index + 1
                blockend = None
                continue
        # return what we got
        return data

    def _parse_freq_block(self, loi):
        # parse a block of lines containing the phonon frequencies
        # data looks as follows
        #     Diagonalizing the dynamical matrix
        #
        #     q = (    0.000000000  -1.000000000   0.000000000 )
        #
        #  ********************************************************************
        # freq (    1) =       4.383310 [THz] =     146.211487 [cm-1]
        # freq (    2) =       4.383310 [THz] =     146.211487 [cm-1]
        # freq (    3) =      11.935806 [THz] =     398.135639 [cm-1]
        # freq (    4) =      11.935806 [THz] =     398.135639 [cm-1]
        # freq (    5) =      13.247649 [THz] =     441.894010 [cm-1]
        # freq (    6) =      13.247649 [THz] =     441.894010 [cm-1]
        # *********************************************************************
        data = {"frequencies_THz": [], "frequencies_cm-1": []}
        for line in loi:
            if "q = (" in line:
                # qpoint coordinates
                s, i, f = decompose_line(line)
                data["q_point"] = f
                continue
            if "freq" in line and "-->" not in line:
                s, i, f = decompose_line(line)
                data["frequencies_THz"].append(f[0])
                data["frequencies_cm-1"].append(f[1])
                continue
        return data


class QEPHLogTimingSubParser(BaseQELogTimingSubParser):
    _loggername = "QEPHLogTimingSubParser"
    trigger = "PHONON       :"
    can_be_recalled = True  # only get the last one
    recalled_behavior = "overwrite"
