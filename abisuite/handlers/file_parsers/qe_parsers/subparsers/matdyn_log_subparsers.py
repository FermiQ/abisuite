from .bases import BaseQELogTimingSubParser


class QEMatdynLogTimingSubParser(BaseQELogTimingSubParser):
    _loggername = "QEMatdynLogTimingSubParser"
    trigger = "MATDYN       :"
