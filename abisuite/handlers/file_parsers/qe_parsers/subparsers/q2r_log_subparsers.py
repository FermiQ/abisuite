from .bases import BaseQELogTimingSubParser


class QEQ2RLogTimingSubParser(BaseQELogTimingSubParser):
    _loggername = "QEQ2RLogTimingSubParser"
    trigger = "Q2R          :"
