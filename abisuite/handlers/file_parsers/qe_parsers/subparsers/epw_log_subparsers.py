from .bases import BaseQELogTimingSubParser
from ...bases import BaseSubParser
from .....routines import decompose_line


class QEEPWLogTimingSubParser(BaseQELogTimingSubParser):
    can_be_recalled = True  # only get the last one
    recalled_behavior = "overwrite"
    trigger = "EPW          :"
    _loggername = "QEEPWLogTimingSubParser"


class BaseQEEPWLogConductivitiesSubParser(BaseSubParser):
    """Base class for conductivities subparsers. Essentially does all the job
    and the subclasses just change in the trigger.
    """
    can_be_recalled = False

    def _extract_data_from_lines(self, lines):
        # data is organised as follows, for each iteration:
        # there is a small difference for metals
        # Iteration number:         1
        #
        # =============================================================================================  # noqa: E501
        #   Temp     Fermi    Population SR                  Conductivity
        #    [K]      [eV]  [carriers per cell]              [V.s.cm]^-1
        # =============================================================================================  # noqa: E501
        #
        #   10.000   0.11645E+02  -0.28422E-13    0.446918E+03   -0.120917E-14    0.169284E-13           # noqa: E501
        #                                        -0.120917E-14    0.446918E+03    0.120917E-14           # noqa: E501
        #                                         0.278109E-13    0.120917E-14    0.446918E+03           # noqa: E501
        #   60.000   0.11642E+02   0.10663E-13    0.557159E+02   -0.151220E-15    0.743184E-16           # noqa: E501
        #                                         0.113360E-15    0.557159E+02    0.113360E-15           # noqa: E501
        #                                        -0.128474E-14    0.147604E-18    0.557159E+02           # noqa: E501
        # [...]
        data = {"holes": [], "electrons": [], "metals": [], "error": []}
        skip = 0
        for index, line in enumerate(lines):
            if index < skip:
                continue
            if "Iteration number" in line:
                self._logger.debug("Parsing iteration block.")
                iter_data, relskip = self._extract_iteration_block(
                        lines[index:])
                skip = index + relskip
                for typ in ("holes", "electrons", "metals", "error"):
                    if typ in iter_data:
                        data[typ].append(iter_data[typ])
                continue
            # continue until end of file (not sure where this block ends).
        self.ending_relative_index = 10  # move forward at least 10 lines
        # because Yoda said so
        return data

    def _extract_iteration_block(self, lines):
        # each iteration block has electrons+holes conductivities
        data = {}
        skip = 0
        for index, line in enumerate(lines):
            if index < skip:
                continue
            if "Hole conductivity" in line:
                self._logger.debug("Parsing holes conductivities block.")
                hole_data, relend = self._extract_conductivities(lines[
                    index + 3:])
                skip = index + relend + 3
                data["holes"] = hole_data
                continue
            elif "Elec conductivity" in line:
                self._logger.debug("Parsing electrons conductivities block.")
                elec_data, relend = self._extract_conductivities(
                        lines[index + 3:])
                skip = index + relend + 3
                data["electrons"] = elec_data
                continue
            elif "Conductivity" in line:
                # metal
                self._logger.debug("Parsing electrons conductivities block.")
                metal_data, relend = self._extract_conductivities(
                        lines[index + 3:])
                skip = index + relend + 3
                data["metals"] = metal_data
                continue
            elif "Max error" in line:
                self._logger.debug("Parsing error line.")
                s, i, f = decompose_line(line)
                data["error"] = f[0]
                return data, index + 1
        # if we are here something bad happened
        raise LookupError(
                "Error while parsing EPW log file for a conductivity "
                "iteration block.")

    def _extract_conductivities(self, lines):
        data = []
        seen_first_line = False
        for j, line in enumerate(lines):
            s, i, f = decompose_line(line)
            if len(f) > 3:
                # we are at the first line
                if len(f) == 7:
                    newdata = {
                            "temperature": f[0],
                            "chemical_potential": f[1],
                            "dos": f[2],
                            "population": f[3],
                            "xx": f[4],
                            "xy": f[5],
                            "xz": f[6]
                            }
                elif len(f) == 6:
                    # before DOS implementation
                    newdata = {
                            "temperature": f[0],
                            "chemical_potential": f[1],
                            "population": f[2],
                            "xx": f[3],
                            "xy": f[4],
                            "xz": f[5]
                            }
                seen_first_line = True
                data.append(newdata)
                continue
            elif len(f) == 3:
                if seen_first_line:
                    # we are at the second line now
                    newdata = {
                            "yx": f[0],
                            "yy": f[1],
                            "yz": f[2],
                            }
                    data[-1].update(newdata)
                    seen_first_line = False
                    continue
                else:
                    # we are at the third line now
                    newdata = {
                            "zx": f[0],
                            "zy": f[1],
                            "zz": f[2],
                            }
                    data[-1].update(newdata)
                    continue
            elif (
                    "Max error" in line or "Iteration number" in line or
                    "========" in line
                    ):
                # return what we got
                return data, j
        # else we got a problem
        raise LookupError(
                "An error occured when extracting conductivities of holes or "
                "electrons for a given iteration.")


class QEEPWLogConductivitiesSubParser(BaseQEEPWLogConductivitiesSubParser):
    """Subparser for the IBTE conductivities.
    """
    subject = "conductivities"
    trigger = "iterative Boltzmann Transport Equation"
    _loggername = "QEEPWLogConductivitiesSubParser"


class QEEPWLogSERTAConductivitiesSubParser(
        BaseQEEPWLogConductivitiesSubParser):
    """Subparser for the SERTA conductivities.
    """
    subject = "serta_conductivities"
    trigger = "SERTA"
    _loggername = "QEEPWLogSERTAConductivitiesSubParser"
