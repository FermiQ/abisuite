from ...bases import BaseSubParser
from .....routines import decompose_line


class BaseQELogTimingSubParser(BaseSubParser):
    """Base class for a subparser for cpu and walltime in Quantum Espresso
    log files.
    """
    subject = "timing"

    def _extract_data_from_lines(self, lines):
        data = {}
        # only one line
        # e.g.: PWSCF        :    1m 3.35s CPU       1m  3.62s WALL
        # if < 1m: minutes don't appear
        s, i, f = decompose_line(lines[0])
        s = s[2:-1]  # remove PWSCF, ':' and WALL
        # keep CPU as it splits the time measures
        # group cputime (first group)
        # so far s is a list of strings looking like:
        # ["1d", "5h", "1m", "3.35s", "CPU", "1m", "3.62s"]
        # or any other alteration
        intermediate_str = "".join(s)
        if "CPU" not in intermediate_str:
            raise LookupError(f"Was expecting a 'CPU' flag in the timing line:"
                              f" {lines[0]}")
        intermediate_split = intermediate_str.split("CPU")
        data["walltime"] = self._convert_str_to_sec(intermediate_split[1])
        data["cputime"] = self._convert_str_to_sec(intermediate_split[0])
        self.ending_relative_index = 0
        return data

    def _convert_str_to_sec(self, string):
        # e.g.: 1d3h1m11.81s, 1.82s
        # assume nothing more complicated
        has_seconds = "s" in string
        has_minutes = "m" in string
        has_hours = "h" in string
        has_days = "d" in string
        nsecs, nmins, nhours, ndays = 0, 0, 0, 0
        if has_seconds:
            string = string.split("s")[0]
            if has_minutes:
                splitby = "m"
            elif not has_minutes and has_hours:
                splitby = "h"
            elif not has_minutes and not has_hours and has_days:
                splitby = "d"
            else:
                splitby = "none"
            nsecs = float(string.split(splitby)[-1])
        if has_minutes:
            string = string.split("m")[0]
            if has_hours:
                splitby = "h"
            elif not has_hours and has_days:
                splitby = "d"
            else:
                splitby = "none"
            nmins = float(string.split(splitby)[-1])
        if has_hours:
            string = string.split("h")[0]
            if has_days:
                splitby = "d"
            else:
                splitby = "none"
            nhours = float(string.split(splitby)[-1])
        if has_days:
            string = string.split("d")[0]
            ndays = float(string)
        # RETURN TOTAL TIME IN SECONDS (DECIDED CONVENTION BECAUSE YOLO S.I.)
        return nsecs + nmins * 60 + nhours * 3600 + ndays * 24 * 3600
