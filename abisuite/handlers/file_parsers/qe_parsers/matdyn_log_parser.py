from .bases import BaseQELogParser
from .subparsers.matdyn_log_subparsers import (
                QEMatdynLogTimingSubParser,
                )
from ...file_structures import QEMatdynLogStructure


class QEMatdynLogParser(BaseQELogParser):
    """Parser class for a Quantum Espresso matdyn.x log file.
    """
    _loggername = "QEMatdynLogParser"
    _structure_class = QEMatdynLogStructure
    _subparsers = (
            QEMatdynLogTimingSubParser,
            )
