import os

import numpy as np

from .bases import BaseQEParser
from ...file_structures import QEEPWPhononSelfEnergyStructure


class QEEPWPhononSelfEnergyParser(BaseQEParser):
    """Parser for a phonon self energy file produced by the epw.x script
    from Quantum Espresso.
    """
    _expected_ending = "specfun_sup.phon"
    _loggername = "QEEPWPhononSelfEnergyParser"
    _structure_class = QEEPWPhononSelfEnergyStructure

    def _extract_data_from_lines(self, lines):
        to_return = {}
        data = np.loadtxt(lines)
        # reshape data into a nqpt x nbranch x nfreq matrix
        # for that we need to get the number of freq and qpts and branches
        # first column of data is the qpt indices
        iqpts = data[:, 0]
        nqpts = int(np.max(iqpts))
        # second column is the branch index
        branches = data[:, 1]
        nbranches = int(np.max(branches))
        # third columns are the eigenvalues for each qpts
        # create the eigenvalues tensor which is nqpts x nbranches
        eigs = data[:, 2]
        # the eigs data repeat itself every nbranches
        all_eigs = []
        for i in range(nqpts):
            where_i = np.where(iqpts == i + 1)[0]
            all_eigs += eigs[where_i][:nbranches].tolist()
        all_eigs = np.array(all_eigs)
        to_return["eigenvalues"] = all_eigs.reshape((nqpts, nbranches))
        # get frequencies
        freqs = data[:, 3]
        to_return["energies"] = np.unique(freqs)
        nfreqs = len(self.energies)
        # get self energies
        # real = np.reshape(data[:, 4], (nqpts, nbranches, nfreqs))
        # imag = np.reshape(data[:, 6], (nqpts, nbranches, nfreqs))
        # custom reshape since the file is very oddly written
        real = data[:, 4]
        imag = data[:, 6]
        self_energy = np.zeros((nqpts, nbranches, nfreqs), dtype=complex)
        for iq in range(nqpts):
            for ifreq in range(nfreqs):
                for inu in range(nbranches):
                    index = inu + ifreq * nbranches + iq * nfreqs
                    re, im = real[index], imag[index]
                    self_energy[iq, inu, ifreq] = re + 1.0j * im
        to_return["self_energy"] = self_energy
        return to_return

    @classmethod
    def _filepath_from_meta(cls, meta, *args, **kwargs):
        # this file is written in the run directory where the batch file is
        # where the epw.x script has been called
        with meta:
            rd = meta.rundir
        return os.path.join(rd, cls._expected_ending)
