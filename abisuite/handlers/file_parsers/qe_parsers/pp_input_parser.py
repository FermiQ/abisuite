from .bases import BaseQEInputParser
from ...file_structures import QEPPInputStructure


class QEPPInputParser(BaseQEInputParser):
    """Class that can parse a Quantum Espresso pp.x input File.
    """
    _loggername = "QEPPInputParser"
    _structure_class = QEPPInputStructure
