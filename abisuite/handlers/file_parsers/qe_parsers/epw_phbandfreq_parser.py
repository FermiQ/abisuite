import os

from .bases import BaseBandFreqParser
from ...file_structures import QEEPWPHBandFreqStructure


class QEEPWPHBandFreqParser(BaseBandFreqParser):
    """A Quantum Espresso .freq file parser that extracts data from
    a .freq file.
    """
    _expected_ending = "phband.freq"
    _loggername = "QEEPWPHBandFreqParser"
    _structure_class = QEEPWPHBandFreqStructure

    @classmethod
    def _filepath_from_meta(cls, meta, *args, **kwargs):
        return os.path.join(meta.rundir, cls._expected_ending)

    def _extract_data_many_atoms(self, lines):
        # energies are not grouped by 6 like for matdyn...
        return self._extract_data_1_atom(lines)
