from .bases import BaseQELogParser
from .subparsers.pw2wannier90_log_subparsers import (
                QEPW2Wannier90LogTimingSubParser,
                )
from ...file_structures import QEPW2Wannier90LogStructure


class QEPW2Wannier90LogParser(BaseQELogParser):
    """Parser class for a Quantum Espresso pw2wannier90.x log file.
    """
    _loggername = "QEPW2Wannier90LogParser"
    _structure_class = QEPW2Wannier90LogStructure
    _subparsers = (
            QEPW2Wannier90LogTimingSubParser,
            )
