from .bases import BaseQEInputParser
from ...file_structures import QEEPWInputStructure
from ....routines import decompose_line, is_list_like


class QEEPWInputParser(BaseQEInputParser):
    """Class that can parse a Quantum Espresso epw.x input File.
    """
    _loggername = "QEEPWInputParser"
    _structure_class = QEEPWInputStructure

    def _check_for_special_input_var(self, i, lines):
        lowered = lines[i].lower()
        if "&" in lowered or "/" in lowered or "=" in lowered:
            return super()._check_for_special_input_var()
        if i == 0:
            # title
            return {"title": lines[i].strip("\n")}, 1
        # else it's qpoints until the end of file
        lines = lines[i:]
        data = {}
        qpts = []
        weights = []
        for line in lines:
            s, ints, f = decompose_line(line)
            if len(s) == 1 and len(ints) == 1:
                # parameter line
                data["parameter"] = s[0]
                nqpts = ints[0]
                continue
            if len(f):
                # coordinates
                qpts.append(f[:3])
                if len(f) == 4:
                    weights.append(f[-1])
                    continue
                elif len(ints) == 1:
                    weights.append(ints[0])
                    continue
            # if we are here something was wrong
            raise LookupError(f"WTF is this line: {line}.")
        if "parameter" not in data:
            raise LookupError("Something happened while parsing qpts.")
        if nqpts != len(qpts):
            raise ValueError("Missed some qpts somehow.")
        data["q_points"] = qpts
        data["weights"] = weights
        return {"q_points": data}, len(lines) + 1

    def _extract_variable_from_line(self, line):
        # exception for 'temps' which could have a length of 1 and thus be
        # declared an integer only
        varname, varvalue = super()._extract_variable_from_line(line)
        if varname == "temps" and not is_list_like(varvalue):
            varvalue = [varvalue]
        return varname, varvalue
