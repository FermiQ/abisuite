import logging
import os

from .bases import BaseQEDOSParser
from ...file_structures import QEProjwfcPDOSStructure


class QEProjwfcPDOSParser(BaseQEDOSParser):
    """Class that parses a Partial DOS data file.
    """
    _expected_ending = ""
    _loggername = "QEProjwfcPDOSParser"
    _structure_class = QEProjwfcPDOSStructure

    def _extract_data_from_array(self, data):
        toreturn = {"energies": data[:, 0],
                    "ldos": data[:, 1]
                    }
        # for the following see:
        # https://www.quantum-espresso.org/Doc/INPUT_PROJWFC.html
        if data.shape[-1] == 3:
            # only s orbital
            toreturn["s"] = data[:, 2]
        elif data.shape[-1] == 5:
            # p orbitals => 3 types
            toreturn["pz"] = data[:, 2]
            toreturn["px"] = data[:, 3]
            toreturn["py"] = data[:, 4]
        elif data.shape[-1] == 7:
            # d orbitals => 5 types
            toreturn["dz2"] = data[:, 2]
            toreturn["dzx"] = data[:, 3]
            toreturn["dzy"] = data[:, 4]
            toreturn["dx2-y2"] = data[:, 5]
            toreturn["dxy"] = data[:, 6]
        else:
            raise LookupError(f"Unrecognized data format in {self.path}")
        return toreturn

    @classmethod
    def _filepath_from_meta(cls, meta, *args, atoms=None, orbitals=None,
                            wfcs=None,
                            atom_nos=None, **kwargs):
        """Creates a list of QEPDOSParser from a MetaData object.

        Parameters
        ----------
        atoms : list, optional
                If not None, gives the list of atoms to return.
                If None, returns parser for all atoms.
        orbitals : list, optional
                   For the atoms requested, if this is not None, orbitals
                   that are not in this list will not be considered.
                   Else, all orbitals are returned.
        wfcs : list, optional
               Samething as orbitals except that if there is more than one
               wfc per orbital, this parameter lets you choose it in
               advance.
        atom_nos : list, optional
                   Same thing as above but for atomic numbers (if more than
                   one atom of the same specie)
        """
        out = meta.output_data_dir
        to_return = {}
        if isinstance(atoms, str):
            atoms = (atoms, )
        if isinstance(orbitals, str):
            orbitals = (orbitals, )
        if isinstance(wfcs, int):
            wfcs = (str(wfcs), )
        if isinstance(atom_nos, int):
            atom_nos = (str(atom_nos), )
        atom_nos = [str(x) for x in atom_nos]  # in case it was a list of ints.
        for subfile in os.listdir(out):
            if "pdos_atm" not in subfile:
                continue
            # format: jobname.pdos_atm#1(Sr)_wfc#1(s)
            atom = subfile.split("(")[1].split(")")[0]
            orbital = subfile.split("(")[2].split(")")[0]
            atom_no = subfile.split("#")[1].split("(")[0]
            wfc = subfile.split("#")[2].split("(")[0]
            if atoms is not None:
                if atom not in atoms:
                    continue
            if orbitals is not None:
                if orbital not in orbitals:
                    continue
            if wfcs is not None:
                if wfc not in wfcs:
                    continue
            if atom_nos is not None:
                if atom_no not in atom_nos:
                    continue
            # return the path
            key = f"{atom}{atom_no}_{orbital}_w{wfc}"
            to_return[key] = os.path.join(out, subfile)
        if not len(to_return):
            raise ValueError("No pdos to return.")
        return to_return

    @classmethod
    def _instance_from_filepath(cls, filepath, *args, **kwargs):
        # ignore all args and kwargs, only get loglevel
        # from that point, filepath is a dict
        lvl = kwargs.get("loglevel", logging.INFO)
        return {k: cls(path, loglevel=lvl) for k, path in filepath.items()}
