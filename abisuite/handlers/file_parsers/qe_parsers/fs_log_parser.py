from .bases import BaseQELogParser
from ...file_structures import QEFSLogStructure


class QEFSLogParser(BaseQELogParser):
    """Parser class for a Quantum Espresso fs.x log file.
    """
    _loggername = "QEFSLogParser"
    _structure_class = QEFSLogStructure
