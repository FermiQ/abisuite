from .bases import BaseQEInputParser
from ...file_structures import QEPHInputStructure
from ....routines import decompose_line


class QEPHInputParser(BaseQEInputParser):
    """Class that can parse a Quantum Espresso ph.x input File.
    """
    _loggername = "QEPHInputParser"
    _structure_class = QEPHInputStructure

    def _check_for_special_input_var(self, i, lines):
        lowered = lines[i].lower()
        if "&" not in lowered and "/" not in lowered and "=" not in lowered:
            if i == 0:
                # title
                return {"title": lines[i].strip("\n")}, 1
            # else its qpoints until the end ot file
            if len(lines) - i > 1:
                # complicated q-points structure to implement later
                # FYI: this comment has been written on 14/12/2018
                # at 20:13 in Oxford / UK
                # maybe archeologist will uncover this code at some point
                # and they will be happy to not have to look at the file's meta
                # data to determine at which date this was written! :D

                # 11/04/2019: Is it possible to look into a file meta data and
                # know at what time a specific line has been written? (in
                # response to the previous comment)
                raise NotImplementedError()
            # only one qpoint that is supposed to be a single vector
            s, ints, f = decompose_line(lines[i])
            if len(ints) or len(s):
                raise LookupError("Expected only floats in the last line of "
                                  "this ph input file.")
            return {"q_points": f}, i
        return super()._check_for_special_input_var()
