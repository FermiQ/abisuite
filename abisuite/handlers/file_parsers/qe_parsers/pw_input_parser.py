from .bases import BaseQEInputParser
from ...file_structures import QEPWInputStructure
from ....routines import decompose_line


class QEPWInputParser(BaseQEInputParser):
    """Class that can parse a Quantum Espresso Input File.
    """
    _loggername = "QEPWInputParser"
    _structure_class = QEPWInputStructure

    def _check_for_special_input_var(self, i, lines):
        # check if special input var is defined on this line
        # if yes, return the related data
        lowered = lines[i].lower()
        if "atomic_species" in lowered:
            self._logger.debug("Found ATOMIC_SPECIES varblock.")
            # atomic species: the following lines are a list of atom types
            return self._extract_atomic_species(lines[i + 1:])
        elif "atomic_positions" in lowered:
            self._logger.debug("Found ATOMIC_POSITIONS varblock.")
            parameter = lines[i].split(" ")[-1]
            return self._extract_atomic_positions(lines[i + 1:], parameter)
        elif "k_points" in lowered:
            self._logger.debug("Found K_POINTS varblock.")
            parameter = lines[i].split(" ")[-1]
            return self._extract_kpts(lines[i + 1:], parameter)
        # else return super method
        return super()._check_for_special_input_var(i, lines)

    def _extract_kpts(self, lines, parameter):
        # lines should be starting with the list of kpts parameters
        if parameter not in ("automatic",
                             "tpiba", "crystal", "crystal_b"):
            raise NotImplementedError(f"kpt param: {parameter}")
        if parameter == "automatic":
            data = None
            for i, line in enumerate(lines):
                if "atomic" in line.lower():  # next data block
                    return {"k_points": {"parameter": parameter,
                                         "k_points": data}}, i
                s, i, f = decompose_line(line)
                data = i
            # end of file
            return {"k_points": {"parameter": parameter,
                                 "k_points": data}}, len(lines)
        elif parameter in ("tpiba", "crystal", "crystal_b"):
            data = {
                "k_points": {
                    "parameter": parameter,
                    }, }
            kpts = []
            weights = []
            nks = None
            for j, line in enumerate(lines):
                if "k_points" in line.lower():
                    continue
                if "atomic" in line.lower():
                    assert nks is not None
                    assert nks == len(kpts)
                    assert nks == len(weights)
                    data["k_points"]["k_points"] = kpts
                    data["k_points"]["nks"] = len(kpts)
                    data["k_points"]["weights"] = weights
                    return data, j
                s, i, f = decompose_line(line)
                if len(i) == 1 and len(f) == 0:
                    # nks
                    nks = i[0]
                    continue
                # else is a kpt + weight
                kpts.append(f[:3])
                if len(i) == 1:
                    # weight is an int
                    weights.append(i[0])
                    continue
                # else its a float
                assert len(f) == 4
                weights.append(f[-1])
            # if we reach end of file, return what we got
            assert nks is not None
            assert nks == len(kpts)
            assert nks == len(weights)
            data["k_points"]["k_points"] = kpts
            data["k_points"]["nks"] = len(kpts)
            data["k_points"]["weights"] = weights
            return data, len(lines)

    def _extract_atomic_positions(self, lines, parameter):
        # lines should be starting with a list of atomic positions
        data = {}
        for j, line in enumerate(lines):
            if "atomic" in line.lower() or "k_points" in line.lower():
                # Reached new section => return data gathered
                return {"atomic_positions": {"parameter": parameter,
                                             "positions": data}}, j
            s, i, f = decompose_line(line)
            atom_type = s[0]
            coordinates = f
            if atom_type not in data:
                data[atom_type] = [coordinates]
            else:
                data[atom_type].append(coordinates)
        # reached end of file, return everything
        return {"atomic_positions": {"parameter": parameter,
                                     "positions": data}}, len(lines)

    def _extract_atomic_species(self, lines):
        # lines should be starting with a list of atomic species
        data = []
        for j, line in enumerate(lines):
            if "atomic" in line.lower() or "k_points" in line.lower():
                # reached new section => return data gathered
                return {"atomic_species": data}, j
            s, i, f = decompose_line(line)
            atom = s[0]
            atomic_mass = f[0]
            pseudo = s[1]
            data.append({"atom": atom,
                         "atomic_mass": atomic_mass,
                         "pseudo": pseudo})
        # reached end of file
        return {"atomic_species": data}, len(lines)
