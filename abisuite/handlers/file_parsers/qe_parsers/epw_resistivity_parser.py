import numpy as np
import os

from .bases import BaseQEParser
from ...file_structures import QEEPWResistivityStructure


class QEEPWResistivityParser(BaseQEParser):
    """Parser for a resistivity file produced by the epw.x script from
    Quantum Espresso.
    """
    _expected_ending = ""
    _loggername = "QEEPWResistivityParser"
    _structure_class = QEEPWResistivityStructure

    def _extract_data_from_lines(self, lines):
        data = np.loadtxt(lines)
        temperatures = data[:, 0]
        resistivity = data[:, 1]
        return {"temperatures": temperatures, "resistivity": resistivity}

    @classmethod
    def _filepath_from_meta(cls, meta, *args, **kwargs):
        # this file is written in the run directory where the batch file is
        # where the epw.x script has been called
        rd = meta.rundir
        for path in os.listdir(rd):
            if path.endswith(".res.01"):
                return os.path.join(rd, path)
        raise FileNotFoundError(
                f"Could not find resistivity file in '{rd}'.")
