from .bases import BaseQELogParser
from ...file_structures import QEDynmatLogStructure


class QEDynmatLogParser(BaseQELogParser):
    """Parser class for a Quantum Espresso dynmat.x log file.
    """
    _loggername = "QEDynmatLogParser"
    _structure_class = QEDynmatLogStructure
