import os

from .bases import BaseQEParser
from ...file_structures import QEMatdynEigStructure
from ....routines import decompose_line


class QEMatdynEigParser(BaseQEParser):
    """A Quantum Espresso .freq file parser that extracts data from
    a .eig file produced by the matdyn.x script.
    """
    _expected_ending = ".eig"
    _loggername = "QEMatdynEigParser"
    _structure_class = QEMatdynEigStructure

    def _extract_data_from_lines(self, lines):
        # one eigenvector for each qpt, and branch
        # total nqpt x nbranch complex eigenvectors
        eig_per_qpt = []
        skip = 0
        for i, line in enumerate(lines):
            if i < skip:
                continue
            if "q = " in line:
                # one qpt block starts here
                self._logger.debug(f"Extracting qpt block of: {line}")
                data_this_qpt, rel_skip = self._extract_qpt_block(lines[i:])
                eig_per_qpt.append(data_this_qpt)
                skip = i + rel_skip
                continue
        # when end of file is reached, return what we got
        return {"eigenvectors": eig_per_qpt}

    def _extract_qpt_block(self, lines):
        # a qpt block starts with the qpt coordinates.
        # then all the branches are listed and for each, the eigenvector
        skip = 0
        data = {}
        freqs_cm = []
        freqs_thz = []
        eigen_vecs = []
        for i, line in enumerate(lines):
            if i < skip:
                continue
            if "diagonalizing" in line or i == len(lines) - 1:
                # stop here, fill data dict and return what we got
                data["frequencies_cm"] = freqs_cm
                data["frequencies_THz"] = freqs_thz
                data["eigenvectors"] = eigen_vecs
                return data, i + 1
            if "q =" in line:
                # qpt coordinates defined here
                s, ints, f = decompose_line(line)
                data["coordinates"] = f
                continue
            if "freq" in line:
                # phonon frequency for that qpt is written here
                s, ints, f = decompose_line(line)
                if len(f) != 2:
                    self._logger.error(
                            f"Could not extract eigenfrequencies on line: "
                            f"'{line}'")
                    raise LookupError(
                            "Could not determine phonon frequencies.")
                freqs_cm.append(f[1])
                freqs_thz.append(f[0])
                self._logger.debug(f"Extracting freq block for: {f[1]} cm-1")
                data_this_branch, rel_skip = self._extract_branch_block(
                                                lines[i:])
                eigen_vecs.append(data_this_branch)
                skip = i + rel_skip
                continue
        else:
            raise RuntimeError("Something weird happened :'(")

    def _extract_branch_block(self, lines):
        has_seen_freq = False
        eigenvector = []

        def _return(i):
            return eigenvector, i
        for i, line in enumerate(lines):
            if "freq" in line:
                if not has_seen_freq:
                    has_seen_freq = True
                    continue
                else:
                    # return what we got
                    return _return(i)
            if "********************" in line:
                return _return(i)
            # else we construct the eigenvector
            s, i, f = decompose_line(line)
            if len(f) != 6:
                self._logger.error(
                        f"Expected to extract 6 values 3 real and 3 imag in: "
                        f"'{line}'.")
                raise LookupError("Could not extract phonon eigenvector.")
            # separate real part and imaginary part
            real = [f[0], f[2], f[4]]
            imag = [f[1], f[3], f[5]]
            eigenvector.append(
                    [x + 1.0j * y for x, y in zip(real, imag)])

    @classmethod
    def _filepath_from_meta(cls, meta, *args, **kwargs):
        try1 = os.path.join(meta.output_data_dir,
                            meta.jobname + cls._expected_ending)
        if os.path.isfile(try1):
            return try1
        # if not there, look in the rundir for a file that ends with .eig
        for filename in os.listdir(meta.rundir):
            if filename.endswith(cls._expected_ending):
                return os.path.join(
                        meta.rundir, filename)
        # else cannot find file
        raise FileNotFoundError("Could not locate .eig file.")
