from .bases import BaseQEInputParser
from ...file_structures import QEDynmatInputStructure


class QEDynmatInputParser(BaseQEInputParser):
    """Class that can parse a Quantum Espresso dynmat.x input File.
    """
    _loggername = "QEDynmatInputParser"
    _structure_class = QEDynmatInputStructure
