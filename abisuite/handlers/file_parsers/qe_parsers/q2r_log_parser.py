from .bases import BaseQELogParser
from .subparsers.q2r_log_subparsers import (
                QEQ2RLogTimingSubParser,
                )
from ...file_structures import QEQ2RLogStructure


class QEQ2RLogParser(BaseQELogParser):
    """Parser class for a Quantum Espresso q2r.x log file.
    """
    _loggername = "QEQ2RLogParser"
    _structure_class = QEQ2RLogStructure
    _subparsers = (
            QEQ2RLogTimingSubParser,
            )
