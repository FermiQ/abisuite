from .bases import BaseQELogParser
from ...file_structures import QEEpsilonLogStructure


class QEEpsilonLogParser(BaseQELogParser):
    """Parser class for a Quantum Espresso epsilon.x log file.
    """
    _loggername = "QEEpsilonLogParser"
    _structure_class = QEEpsilonLogStructure
