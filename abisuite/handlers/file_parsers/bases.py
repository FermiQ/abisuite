import abc
import logging
import os

from ...bases import BaseUtility, BaseStructuredObject
from ...exceptions import DevError
from ...routines import is_list_like


class BaseStreamLineParser(BaseStructuredObject, abc.ABC):
    """Mainstream base class for parsers.
    """
    _calctype = None
    _expected_ending = None

    def __init__(self, *args, **kwargs):
        super().__init__(**kwargs)
        if self._expected_ending is None:
            raise DevError("Need to set _expected_ending.")
        self._path = None

    @property
    def path(self):
        return self._path

    @path.setter
    def path(self, path):
        # if not os.path.exists(path):
        #     raise FileNotFoundError(f"Nothing found here: {path}.")
        # only raise an error when reading
        # if not os.path.isfile(path):
        #     raise FileNotFoundError(f"Not a file: {path}.")
        if self._expected_ending is not None:
            if not path.endswith(self._expected_ending):
                self._logger.warning(f"file {path} has its extension different"
                                     f" from expected ({self._expected_ending}"
                                     ")")
        self._path = path

    def read(self):
        """Read and parse file set in the 'path' attribute.
        """
        if not os.path.exists(self.path) and not os.path.islink(self.path):
            raise FileNotFoundError(f"Nothing found here: {self.path}.")
        if not os.path.isfile(self.path) and not os.path.islink(self.path):
            raise FileNotFoundError(f"Not a file: {self.path}.")
        self._extract_data()

    @abc.abstractmethod
    def _extract_data(self):
        pass

    @classmethod
    def from_calculation(cls, path, *args, **kwargs):
        """Returns a Parser from a calculation directory by reading the
        '.meta' file.
        """
        # import here to prevent import loops
        from ..file_handlers import MetaDataFile
        lvl = kwargs.get("loglevel", logging.INFO)
        with MetaDataFile.from_calculation(path, loglevel=lvl) as meta:
            return cls.from_metadatafile(meta)

    @classmethod
    def from_file(cls, path, *args, **kwargs):
        """Initialize parser directly from a file.
        """
        instance = cls(*args, **kwargs)
        instance.path = path
        return instance

    @classmethod
    def from_metadatafile(cls, meta, *args, **kwargs):
        """Initialize parser from a meta data file object.
        """
        # import here to prevent import loops
        from ..file_handlers import MetaDataFile
        if not isinstance(meta, MetaDataFile):
            raise TypeError("Requires a Meta data file object.")
        if cls._calctype is not None:
            if meta.calctype != cls._calctype:
                raise ValueError(f"Meta calctype '{meta.calctype}' differs"
                                 f" from Parser calctype "
                                 f"'{cls._calctype}'")
        filepath = cls._filepath_from_meta(meta, *args, **kwargs)
        return cls._instance_from_filepath(filepath, *args, **kwargs)

    @classmethod
    def _instance_from_filepath(cls, filepath, *args, **kwargs):
        if is_list_like(filepath):
            # many files, return a list of parsers if needed
            if len(filepath) > 1:
                return [cls.from_file(x, *args, **kwargs) for x in filepath]
            filepath = filepath[0]
        elif isinstance(filepath, dict):
            # many files organized in a dict, return a dict
            return {k: cls.from_file(v, *args, **kwargs)
                    for k, v in filepath.items}
        return cls.from_file(filepath, *args, **kwargs)

    @abc.abstractclassmethod
    def _filepath_from_meta(cls, meta, *args, **kwargs):
        # returns the expected filepath from a meta file
        pass

    def _set_attributes_from_data(self, data):
        for attr, value in data.items():
            setattr(self, attr, value)


class BaseParser(BaseStreamLineParser, abc.ABC):
    """Base class for parsers that have a custom syntax (most parsers).
    """

    def _extract_data(self):
        with open(self.path, "r") as f:
            lines = f.readlines()
        lines = self._clean_lines(lines)
        self._set_attributes_from_data(
                self._extract_data_from_lines(lines))

    def _clean_lines(self, lines):
        return lines

    @abc.abstractmethod
    def _extract_data_from_lines(self, lines):
        pass


class BaseSubParser(BaseUtility, abc.ABC):
    """SubParser Base class.
    """
    can_be_recalled = False
    """If can_be_recalled is True, then the subparser will be updated
    each time the trigger is found in the file.
    """
    recalled_behavior = None
    """Specify what happens to the data when this sub parser is recalled.
    Can be either 'append' or 'overwrite'.
    """
    subject = None
    trigger = None

    def __init__(self, lines, **kwargs):
        super().__init__(**kwargs)
        self._ending_relative_index = None
        # TODO: check that this is not necessary!
        self.lines = self.preprocess_lines(lines)
        # self._logger.debug(f"SubParser triggered for: '{self.subject}'.")
        self.data = self._extract_data_from_lines(self.lines)
        if self.can_be_recalled and self.recalled_behavior is None:
            raise DevError(
                    "Need to set the 'recalled_behavior' for the subparser.")

    @abc.abstractmethod
    def _extract_data_from_lines(self, lines):
        pass

    @property
    def ending_relative_index(self):
        if hasattr(self, "_ending_relative_index"):
            if self._ending_relative_index is not None:
                return self._ending_relative_index
        # if we are here, there is a dev error
        raise ValueError("Dev Error: ending_relative_index must be set (%s)." %
                         self.subject)

    @ending_relative_index.setter
    def ending_relative_index(self, index):
        self._ending_relative_index = index

    @staticmethod
    def preprocess_lines(lines):
        # TODO: lines should already have been cleaned up. This is redundant!
        return [x.strip("\n").strip() for x in lines]


class BaseParserWithSubParsers(BaseParser, abc.ABC):
    """Base class for parsers that use subparsers to parse
    their data.
    """
    _subparsers = None

    def __init__(self, *args, **kwargs):
        if self._subparsers is None:
            raise DevError("Need to set subparsers list.")
        super().__init__(*args, **kwargs)
        self.subparsers = SubParsersList(self._subparsers)

    def _extract_data_from_lines(self, lines):
        skip = 0
        data = {}
        # copy subparsers to not erase from the class attribute
        subparsers = self.subparsers.copy()
        for index, line in enumerate(lines):
            if index < skip:
                # don't work on this line
                continue
            for triggers, subparser in subparsers.items():
                # there can be multiple triggers for one type of data
                # e.g.:fermi_level and highest_occupied_level for QEPWLog
                if not is_list_like(triggers):
                    triggers = (triggers, )
                for trigger in triggers:
                    if trigger in line:
                        # this line is a trigger for the subparser to work
                        # parse the rest of the lines from here
                        s = subparser(lines[index:],
                                      loglevel=self._logger.level)
                        self._set_data_from_subparser(data, s)
                        if not subparser.can_be_recalled:
                            # remove the subparser from the list
                            # to not parse again
                            subparsers.remove(subparser)
                        # modify the skip to not reparse the parsed lines
                        skip = index + s.ending_relative_index
                        # stop parsing the active line
                        break
            if self._stop_parsing_at(line, index):
                # TODO: this is weird, find a way to make this better
                break
        # if len(subparsers):
        #     # couldnt get some information
        #     missed = [s.subject
        #               for s in subparsers if not s.can_be_recalled]
        #     # if len(missed):
        #     #     self._logger.debug(
        #            f"Some info couldn't be parsed: {missed}")
        return data

    def _set_data_from_subparser(self, data, subparser):
        subjects = subparser.subject
        # support for many subjects in a single subparser
        # different subjects are ordered in the data dicts
        # as the keys
        if not is_list_like(subjects):
            subjects = [subjects]
            subparser.data = {subjects[0]: subparser.data}
        for subject, subdata in subparser.data.items():
            if not subparser.can_be_recalled:
                if subject not in data:
                    data[subject] = subdata
                    continue
            if subparser.recalled_behavior == "overwrite":
                data[subject] = subdata
            elif subparser.recalled_behavior == "append":
                if subject not in data:
                    data[subject] = [subdata]
                else:
                    data[subject].append(subdata)
            else:
                raise NotImplementedError(subparser.recalled_behavior)

    def _stop_parsing_at(self, line, index):
        # use this if there's a need to stop parsing at a certain line
        return False


class SubParsersList:
    """Class that takes a list of subparsers and manages it.
    """
    def __init__(self, slist):
        self.subparsers = list(slist)

    def copy(self):
        return SubParsersList(self.subparsers.copy())

    def items(self):
        for subparser in self.subparsers:
            yield subparser.trigger, subparser

    def remove(self, parser):
        self.subparsers.remove(parser)

    def __len__(self):
        return len(self.subparsers)

    def __iter__(self):
        for subparser in self.subparsers:
            yield subparser

    def __repr__(self):
        return str(self.subparsers)


class BaseInputParserNoInputVariables(BaseParser, abc.ABC):
    """Base class for an input file parser that does not have input variables.

    This class is more general than the 'BaseInputParser' class.
    """
    _expected_ending = ".in"

    def _clean_lines(self, lines):
        stripped = [x.strip("\n").strip() for x in lines]
        # remove lines with comments
        return [x for x in stripped if not x.startswith("!")
                and not x.startswith("#")]

    @classmethod
    def _filepath_from_meta(cls, meta):
        return meta.input_file_path


class BaseInputParser(BaseInputParserNoInputVariables, abc.ABC):
    """Base class for an input file parser.
    """
    pass


class BaseLogParser(BaseParserWithSubParsers):
    """Base class for log parsers.
    """
    _expected_ending = ".log"

    def _clean_lines(self, lines):
        return [line.strip().strip("\n").strip() for line in lines]

    def _extract_data_from_lines(self, *args, **kwargs):
        return BaseParserWithSubParsers._extract_data_from_lines(
                self, *args, **kwargs)

    @classmethod
    def _filepath_from_meta(cls, meta, *args, **kwargs):
        # log path is located in PBS files.
        # import here to prevent import loops.
        from .pbs_parser import PBSParser
        pbsparser = PBSParser.from_metadatafile(meta)
        pbsparser.read()
        return pbsparser.log_path
